class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :text
      t.string :language
      t.string :file
      t.belongs_to :area, index: true
      t.belongs_to :category, index: true
      t.timestamps null: false
    end
  end
end
