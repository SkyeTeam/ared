class AddOfficerIdToAgent < ActiveRecord::Migration[5.0]
  def change
    add_reference :agents, :officer, foreign_key: true
  end
end
