class AdsAddLink < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :link, :string
  end
end
