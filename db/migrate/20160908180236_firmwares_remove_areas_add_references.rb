class FirmwaresRemoveAreasAddReferences < ActiveRecord::Migration[5.0]
  def change
    remove_column :firmwares, :areas, :integer, default: []
  end
end
