class SurveysAddLanguage < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :language, :string
  end
end
