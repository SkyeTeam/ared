class CreateLoans < ActiveRecord::Migration[5.0]
  def change
    create_table :loans do |t|
      t.integer :user_id
      t.string :acc_type
      t.string :currency
      t.float :amount
      t.float :balance
      t.date :due_date

      t.timestamps
    end
  end
end
