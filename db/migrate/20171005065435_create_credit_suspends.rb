class CreateCreditSuspends < ActiveRecord::Migration[5.0]
  def change
    create_table :credit_suspends do |t|
    	t.integer :agent_id
    	t.integer :is_blocked
      	t.integer :status, default: 0
      	t.timestamps
    end
  end
end
