class CreateMaintenanceOfficers < ActiveRecord::Migration[5.0]
  def change
    create_table :maintenance_officers do |t|
      t.string :email
      t.string :phone
      t.integer :account
      t.string :password
      t.string :photo
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :state
      t.date :birth
      t.integer :gender
      t.references :country, foreign_key: true
      t.references :district, foreign_key: true
      t.references :town, foreign_key: true

      t.timestamps
    end
  end
end
