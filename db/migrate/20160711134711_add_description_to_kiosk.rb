class AddDescriptionToKiosk < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosks, :description, :string
  end
end
