class CreateGcmNotsNews < ActiveRecord::Migration[5.0]
  def change
    create_table :gcm_nots_news do |t|
		t.string :gcm_key
    	t.string :user_id
      t.timestamps
    end
  end
end
