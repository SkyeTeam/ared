class AddFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name,   :string               # Name:    Manish Rai
    add_column :users, :phone,  :string               # Phone:    9841990715
    add_column :users, :date_of_birth, :date          # Date of birth:    1990-12-12
    add_column :users, :address, :string              # Address:    basbari
    add_column :users, :city,    :string              # City:    Kathmandu
    add_column :users, :country, :string              # Country:    Nepal
    add_column :users, :zip_code, :string             # Zip Code:    00977
    add_column :users, :status, :integer, default: 1  # Status:    Disabled

    add_index  :users, :phone, unique: true           # unique and indexed
  end
end
