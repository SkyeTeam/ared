class CreateAnnouncements < ActiveRecord::Migration[5.0]
  def change
    create_table :announcements do |t|

    	t.string  :announcement
    	t.integer :created_by
      	t.datetime :txn_date
      	t.integer :status
      	t.timestamps
    end
  end
end
