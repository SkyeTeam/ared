class CreateSmsRegistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :sms_registrations do |t|
      t.string :mobile_no
      t.string :purpose
      t.string :message
      t.datetime :deliver_on
      t.integer :status

      t.timestamps
    end
  end
end
