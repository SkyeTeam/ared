class CreateAggregatorReconciliations < ActiveRecord::Migration[5.0]
  def change
    create_table :aggregator_reconciliations do |t|
    	t.string :service_type
      	t.string :sub_type
      	t.datetime :txn_date
      	t.float :txn_amount
      	t.float :net_amount
      	t.float :total_commission
      	t.float :previous_balance
      	t.float :post_balance
      	t.integer :status
     	t.timestamps
    end
  end
end
