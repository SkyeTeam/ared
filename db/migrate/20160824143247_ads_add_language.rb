class AdsAddLanguage < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :language, :string
  end
end
