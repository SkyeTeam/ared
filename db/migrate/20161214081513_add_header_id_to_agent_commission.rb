class AddHeaderIdToAgentCommission < ActiveRecord::Migration[5.0]
  def change
  	add_column :agent_commissions, :header_id, :integer, default: 0
  end
end
