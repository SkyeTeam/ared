class AddVersionIdColumnToUserAccouts < ActiveRecord::Migration[5.0]
  def change
  	add_column :user_accounts, :version_id, :int, default: 0
  end
end
