class AreasAddStorageSize < ActiveRecord::Migration[5.0]
  def change
    add_column :areas, :storage_size, :integer, default: 0
  end
end
