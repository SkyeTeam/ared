class CreateDeployments < ActiveRecord::Migration[5.0]
  def change
    create_table :deployments do |t|
      t.references :country, foreign_key: true
      t.references :district, foreign_key: true
      t.references :town, foreign_key: true
      t.references :officer, foreign_key: true

      t.timestamps
    end
  end
end
