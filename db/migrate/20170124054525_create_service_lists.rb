class CreateServiceLists < ActiveRecord::Migration[5.0]
  def change
    create_table :service_lists do |t|
    	t.string :service_name
    	t.string :service_type
      t.timestamps
    end
  end
end
