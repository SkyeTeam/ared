class CreateAreas < ActiveRecord::Migration[5.0]
  def change
    create_table :areas do |t|
      t.string :title
      t.string :ancestry
      t.timestamps null: false
    end
    add_index :areas, :ancestry
  end
end
