class AddColumnToTxnHeader < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :area, :string
  	add_column :txn_headers, :bill_no, :string
  end
end
