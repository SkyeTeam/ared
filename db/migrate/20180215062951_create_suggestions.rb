class CreateSuggestions < ActiveRecord::Migration[5.0]
  def change
    create_table :suggestions do |t|
    	t.integer 	:user_id
    	t.string    :type
    	t.string    :user_comments
    	t.string    :admin_comments
      t.integer 	:status, default: 1
      t.timestamps
    end
  end
end
