class CreateAgentCreditLimits < ActiveRecord::Migration[5.0]
  def change
    create_table :agent_credit_limits do |t|
      t.integer :user_id
      t.datetime :loan_capacity_date
      t.float :loan_amount
      t.integer :status
      t.timestamps
    end
  end
end
