class CreateKioskMasters < ActiveRecord::Migration[5.0]
  def change
    create_table :kiosk_masters do |t|
    	t.string :model_no
      	t.string :serial_no
      	t.datetime :purchase_date
      	t.string :qr_code_no
      	t.integer :functional_status
      	t.integer :status, default: 0
      	t.timestamps
    end
  end
end
