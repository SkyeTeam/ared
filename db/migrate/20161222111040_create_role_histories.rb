class CreateRoleHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :role_histories do |t|
    	t.integer :role_id
    	t.integer :user_id

      t.timestamps
    end
  end
end
