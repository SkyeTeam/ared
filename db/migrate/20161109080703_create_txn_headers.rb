class CreateTxnHeaders < ActiveRecord::Migration[5.0]
  def change
    create_table :txn_headers do |t|
      t.string :service_type
      t.string :sub_type
      t.datetime :txn_date
      t.string :currency_code
      t.float :amount
      t.string :feeId
      t.string :agent_id
      t.float :agent_commission
      t.float :opt_commission
      t.float :total_commission
      t.integer :status
      t.string :gateway
      t.integer :initated_by
      t.string :description
      t.string :account_id
      t.string :external_reference
      t.string :param1

      t.timestamps
    end
  end
end
