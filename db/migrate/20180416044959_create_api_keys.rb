class CreateApiKeys < ActiveRecord::Migration[5.0]
  def change
    create_table :api_keys do |t|
    	t.integer :user_id
      	t.string :api_key
      	t.integer :status, default: 0
      	t.timestamps
    end
  end
end
