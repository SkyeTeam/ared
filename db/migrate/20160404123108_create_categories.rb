class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    enable_extension "hstore"
    create_table :categories do |t|
      t.string :title
      t.hstore :translations, array: true, default: []
      t.timestamps null: false
    end
  end
end
