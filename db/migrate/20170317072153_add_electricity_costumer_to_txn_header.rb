class AddElectricityCostumerToTxnHeader < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :electricity_costumer, :string
  end
end
