class CreateAgentCommissions < ActiveRecord::Migration[5.0]
  def change
    create_table :agent_commissions do |t|
      t.integer :user_id
      t.string :service_type
      t.string :sub_type
      t.datetime :txn_date
      t.float :previous_balance
      t.float :post_balance

      t.timestamps
    end
  end
end
