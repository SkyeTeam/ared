class CreateUploads < ActiveRecord::Migration[5.0]
  def change
    create_table :uploads do |t|
      t.string :title
      t.string :file
      t.string :languages, default: [], array: true
      t.timestamps null: false
    end
  end
end
