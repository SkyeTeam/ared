class CreateServiceSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :service_settings do |t|
      t.string :service
      t.integer :status

      t.timestamps
    end
  end
end
