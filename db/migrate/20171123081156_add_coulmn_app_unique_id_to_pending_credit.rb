class AddCoulmnAppUniqueIdToPendingCredit < ActiveRecord::Migration[5.0]
  def change
  	add_column :pending_credits, :app_unique_id, :string
  end
end
