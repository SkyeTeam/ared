class CreateAgents < ActiveRecord::Migration[5.0]
  def change
    create_table :agents do |t|
      t.string :name
      t.string :phone
      t.string :location
      t.string :latitude
      t.string :longitude
      t.string :photo

      t.timestamps
    end
  end
end
