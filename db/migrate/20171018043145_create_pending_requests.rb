class CreatePendingRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :pending_requests do |t|
    	t.string  :txn_id
    	t.string  :service_type
    	t.string  :sub_type
    	t.string  :client_number
      	t.float   :amount 
      	t.integer :agent_id
      	t.integer :status
      	t.datetime :txn_date
      	t.timestamps
    end
  end
end
