class RemoveTableClientCommands < ActiveRecord::Migration[5.0]
  def change
    drop_table :client_commands
  end
end
