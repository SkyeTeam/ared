class CreateWifiLists < ActiveRecord::Migration[5.0]
  def change
    create_table :wifi_lists do |t|
    	t.string :duration
    	t.string :amount
      t.timestamps
    end
  end
end
