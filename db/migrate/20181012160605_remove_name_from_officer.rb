class RemoveNameFromOfficer < ActiveRecord::Migration[5.0]
  def change
    remove_column :officers, :name, :string
    add_column :officers, :first_name, :string
    add_column :officers, :middle_name, :string
    add_column :officers, :last_name, :string
  end
end
