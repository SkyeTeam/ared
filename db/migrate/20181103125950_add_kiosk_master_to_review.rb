class AddKioskMasterToReview < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :kiosk_master_id, :integer
    add_index :reviews, :kiosk_master_id
  end
end
