class AddColumnVatTypeToServiceFeeMaster < ActiveRecord::Migration[5.0]
  def change
  	add_column :service_fees_masters, :vat_type, :string
  end
end
