class AddStatusColumnToAgentCommission < ActiveRecord::Migration[5.0]
  def change
  	add_column :agent_commissions, :status, :int, default: 0
  end
end
