class CreateScores < ActiveRecord::Migration[5.0]
  def change
    create_table :scores do |t|
      t.string :metric
      t.string :target
      t.string :points
      t.string :scorable 

      t.timestamps
    end
  end
end
