class AddLxcTaskAndLxcInfoToKiosks < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosks, :lxc_task, :json
    add_column :kiosks, :lxc_info, :json
  end
end
