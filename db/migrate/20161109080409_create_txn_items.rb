class CreateTxnItems < ActiveRecord::Migration[5.0]
  def change
    create_table :txn_items do |t|
      t.integer :header_id
      t.integer :user_id
      t.float :debit_amount
      t.float :credit_amount
      t.integer :account_id
      t.integer :fee_id
      t.float :fee_amt
      t.float :previous_balance
      t.float :post_balance
      t.string :role

      t.timestamps
    end
  end
end
