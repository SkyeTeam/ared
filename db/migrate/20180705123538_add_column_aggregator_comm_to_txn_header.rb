class AddColumnAggregatorCommToTxnHeader < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :aggregator_comm, :float
  end
end
