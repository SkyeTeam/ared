class CreateAdStatistics < ActiveRecord::Migration[5.0]
  def change
    create_table :ad_statistics do |t|
      t.belongs_to  :ad,        foreign_key: true
      t.belongs_to  :kiosk,     foreign_key: true
      t.integer     :action,    nil: false
      t.datetime    :action_at, nil: false
    end
  end
end
