class AddSurveyResultAnswersJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_table :survey_result_answers do |t|
      t.integer :survey_result_id
      t.integer :answer_id
    end
  end
end
