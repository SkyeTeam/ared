class CreateWifiUserLists < ActiveRecord::Migration[5.0]
  def change
    create_table :wifi_user_lists do |t|
    	t.integer :user_id
    	t.integer :duration
    	t.integer :amount
    	t.string :refer_code
    	t.string :costumer_phone

      t.timestamps
    end
  end
end
