class CreateKioskAgents < ActiveRecord::Migration[5.0]
  def change
    create_table :kiosk_agents do |t|
		t.integer :kiosk_master_id
		t.integer :agent_id
   	  	t.integer :assign_status
   	  	t.integer :functional_status
   	  	t.string  :comments
      	t.integer :status, default: 0
      	t.timestamps
    end
  end
end
