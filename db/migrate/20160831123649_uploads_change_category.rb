class UploadsChangeCategory < ActiveRecord::Migration[5.0]
  def change
    remove_column :uploads, :category, :integer, default: 1
    add_reference :uploads, :category, index: true
  end
end
