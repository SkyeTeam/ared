class CreateTaxConfigurations < ActiveRecord::Migration[5.0]
  def change
    create_table :tax_configurations do |t|
      t.string :service
      t.float :percentage	
      t.datetime :effective_date
      t.integer :status, :default => 0
      t.timestamps
    end
  end
end
