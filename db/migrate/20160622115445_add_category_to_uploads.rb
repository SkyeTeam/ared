class AddCategoryToUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :uploads, :category, :integer, default: 1
    add_column :kiosks, :status, :json
  end
end
