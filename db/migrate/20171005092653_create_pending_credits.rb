class CreatePendingCredits < ActiveRecord::Migration[5.0]
  def change
    create_table :pending_credits do |t|
    	t.integer :agent_id
      	t.float :amount
      	t.datetime :txn_date
      	t.datetime :approval_date
      	t.integer :status
      	t.timestamps
    end
  end
end
