class AddColumnUserIdCreditFeeConfigs < ActiveRecord::Migration[5.0]
  def change
  	add_column :credit_fee_configs, :user_id, :integer, default: -1
  end
end
