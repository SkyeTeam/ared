class AddColumnCustomerFeeTxnHeader < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :customer_fee, :float, default: 0
  end
end
