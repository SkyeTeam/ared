class CreateOfficers < ActiveRecord::Migration[5.0]
  def change
    create_table :officers do |t|
      t.string :email
      t.string :phone
      t.integer :account
      t.string :password
      t.string :photo

      t.timestamps
    end
  end
end
