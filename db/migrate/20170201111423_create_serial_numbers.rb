class CreateSerialNumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :serial_numbers do |t|
    	t.string :serial_number
    	t.integer :user_id
    	t.integer :active_flag,default: 0

      t.timestamps
    end
  end
end
