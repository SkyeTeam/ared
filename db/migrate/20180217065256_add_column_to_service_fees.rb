class AddColumnToServiceFees < ActiveRecord::Migration[5.0]
  def change
  	add_column :service_fees, :rate, :float, default: 0
  end
end
