class AddMacAddressToWifiBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :wifi_bookings, :mac_address, :string
  end
end
