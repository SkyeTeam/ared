class AddColumnToServiceFeeMaster < ActiveRecord::Migration[5.0]
  def change
  	add_column :service_fees_slabs, :aggregator_comm, :float, default: 0
  end
end
