class AddColumnGenderToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :gender, :string
    add_column :users, :district, :string
    add_column :users, :national_id, :string
  end
end
