class CreateNotificationNews < ActiveRecord::Migration[5.0]
  def change
    create_table :notification_news do |t|
        t.string :notification_content
    	t.string :notification_title
    	t.integer :user_id
    	t.integer :status,default: 0
      t.timestamps
    end
  end
end
