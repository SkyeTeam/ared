class AddJsonFieldToKiosk < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosks, :gps_info, :json
    add_column :kiosks, :wifi_info, :json
    add_column :kiosks, :wifi_clients_info, :json
  end
end
