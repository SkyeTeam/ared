class KioskRemoveDeprecatedAddNew < ActiveRecord::Migration[5.0]
  def change
    remove_column :kiosks, :ip_address, :string
    add_column :kiosks, :device_info_content, :json
    add_column :kiosks, :device_info_db, :json
  end
end
