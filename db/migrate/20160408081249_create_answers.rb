class CreateAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :answers do |t|
      t.text :answer
      t.integer :order_id
      t.belongs_to :question, foreign_key: true
    end
  end
end
