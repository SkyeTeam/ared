class AddColumnBalanceToServiceSettings < ActiveRecord::Migration[5.0]
  def change
  	add_column :service_settings, :balance, :float, default: 0
  end
end
