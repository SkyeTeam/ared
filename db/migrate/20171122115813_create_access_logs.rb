class CreateAccessLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :access_logs do |t|
      	t.string    :access_id
      	t.integer   :user_id
      	t.integer   :role
      	t.integer	  :is_successful
      	t.string	  :remote_ip
        t.integer   :use_web  
      	t.integer  	:status
      	t.timestamps
    end
  end
end
