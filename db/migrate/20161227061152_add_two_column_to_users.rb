class AddTwoColumnToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :amount_deposit, :float
    add_column :users, :description, :string
  end
end
