class AddReadFlagNotificationNew < ActiveRecord::Migration[5.0]
  def change
  	add_column :notification_news, :read_flag, :int, default: 0
  end
end
