class CreateGatewayLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :gateway_logs do |t|
		t.string    :log_id
      	t.string    :agent_id
      	t.string    :header_id
      	t.string  	:aggregator_name
      	t.float 	:aggregator_balance
      	t.float 	:txn_amount
      	t.string  	:request
      	t.string  	:response
      	t.integer 	:status
      	t.timestamps
    end
  end
end
