class AddStateToOfficer < ActiveRecord::Migration[5.0]
  def change
    add_column :officers, :state, :string
  end
end
