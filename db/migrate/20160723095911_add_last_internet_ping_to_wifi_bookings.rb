class AddLastInternetPingToWifiBookings < ActiveRecord::Migration[5.0]
  def change
    add_column :wifi_bookings, :last_internet_ping, :datetime
  end
end
