class CreateSurveyResult < ActiveRecord::Migration[5.0]
  def change
    create_table :survey_results do |t|
      t.belongs_to :kiosk, foreign_key: true
      t.belongs_to :survey, foreign_key: true
      t.timestamps null: false
    end
  end
end
