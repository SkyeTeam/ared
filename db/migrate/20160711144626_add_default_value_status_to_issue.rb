class AddDefaultValueStatusToIssue < ActiveRecord::Migration[5.0]
  def change
    change_column_default :issues, :status, 1
  end
end
