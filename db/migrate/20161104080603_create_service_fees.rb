class CreateServiceFees < ActiveRecord::Migration[5.0]
  def change
    create_table :service_fees do |t|
      t.string :service
      t.string :company
      t.float :total_comm_per
      t.float :total_comm_fix
      t.float :our_comm_per
      t.float :our_comm_fix
      t.float :agent_comm_per
      t.float :agent_comm_fix
      t.string :effective_date
      t.string :who_will_pay

      t.timestamps
    end
  end
end
