class CreateTranscationCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :transcation_categories do |t|
    	t.string :category_name
    	t.string :category_sub_name

      t.timestamps
    end
  end
end
