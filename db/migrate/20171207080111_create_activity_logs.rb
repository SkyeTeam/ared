class CreateActivityLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :activity_logs do |t|
    	t.string    :action
      	t.string    :initiator
      	t.string    :description
      	t.string  	:params
      	t.timestamps
    end
  end
end
