class CreateFirmwares < ActiveRecord::Migration[5.0]
  def change
    create_table :firmwares do |t|
      t.string :title
      t.string :file
      t.string :areas, array: true, default: []
      t.string :version
      t.string :revision
      t.string :distribution
      t.timestamps
    end
  end
end
