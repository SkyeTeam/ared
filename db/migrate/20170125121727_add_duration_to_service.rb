class AddDurationToService < ActiveRecord::Migration[5.0]
  def change
  	add_column :service_fees, :duration, :int, default: 0
  end
end
