class AddDatesFlagsAndAreaToSurvey < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :area_id, :integer
    add_column :surveys, :start_date, :datetime
    add_column :surveys, :end_date, :datetime
    add_column :surveys, :active, :boolean
  end
end
