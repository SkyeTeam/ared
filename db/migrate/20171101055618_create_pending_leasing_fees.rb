class CreatePendingLeasingFees < ActiveRecord::Migration[5.0]
  def change
    create_table :pending_leasing_fees do |t|

    	t.integer   :agent_id
      	t.date 		:end_period
      	t.float		:payment_amount 
      	t.integer  	:status
      	t.timestamps
      
    end
  end
end
