class UploadsChangeLanguagesToString < ActiveRecord::Migration[5.0]
  def change
    remove_column :uploads, :languages, :integer, default: [], array: true
    add_column :uploads, :language, :string
  end
end
