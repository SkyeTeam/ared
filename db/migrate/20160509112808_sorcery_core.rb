class SorceryCore < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email,            :null => false
      t.integer :role,    default: 0
      t.string :crypted_password
      t.string :password_digest
      t.string :salt

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end
