class AddReportAndFixedTimestampToIssue < ActiveRecord::Migration[5.0]
  def change
    add_column :issues, :report, :string
    add_column :issues, :pending_at, :datetime
    add_column :issues, :closed_at, :datetime
  end
end
