class AddThresholdColumnToServiceSettings < ActiveRecord::Migration[5.0]
    def change
  		add_column :service_settings, :threshold_amount, :float, default: 0
 	end
end
