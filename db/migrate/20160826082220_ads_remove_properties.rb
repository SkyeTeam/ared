class AdsRemoveProperties < ActiveRecord::Migration[5.0]
  def change
    remove_column :ads, :link, :string
    remove_column :ads, :size, :integer
  end
end
