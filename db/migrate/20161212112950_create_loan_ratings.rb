class CreateLoanRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :loan_ratings do |t|
      t.integer :user_id
      t.string :parameter
      t.datetime :date
      t.float :rating

      t.timestamps
    end
  end
end
