class AddAreaToKiosks < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosks, :area_id, :integer, default: 1
    add_index  :kiosks, :area_id
  end
end
