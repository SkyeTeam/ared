class RemoveWifiStatusFromKiosk < ActiveRecord::Migration[5.0]
  def change
    remove_column :kiosks, :wifi_status, :integer
  end
end
