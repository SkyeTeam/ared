class AddColumnToServiceSettings < ActiveRecord::Migration[5.0]
  def change
  	add_column :service_settings, :emails, :string
  	add_column :service_settings, :notification_sent_date, :datetime
  end
end
