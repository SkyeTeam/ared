class CreateThirdPartyConfigs < ActiveRecord::Migration[5.0]
  def change
    create_table :third_party_configs do |t|
    	t.string    :party_name
      	t.string    :username
      	t.string    :password
      	t.float  	:connection_timeout
      	t.integer  	:status
      	t.timestamps
    end
  end
end
