class AddNameToTown < ActiveRecord::Migration[5.0]
  def change
    add_column :towns, :name, :string
  end
end
