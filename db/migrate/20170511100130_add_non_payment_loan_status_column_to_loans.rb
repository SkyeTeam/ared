class AddNonPaymentLoanStatusColumnToLoans < ActiveRecord::Migration[5.0]
  def change
  	add_column :loans, :non_payment_status, :int, default: 0
  end
end
