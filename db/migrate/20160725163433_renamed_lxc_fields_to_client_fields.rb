class RenamedLxcFieldsToClientFields < ActiveRecord::Migration[5.0]
  def change
    rename_column :kiosks, :lxc_task, :client_task
    rename_column :kiosks, :lxc_info, :os_info
    rename_table :lxc_commands, :client_commands
  end
end
