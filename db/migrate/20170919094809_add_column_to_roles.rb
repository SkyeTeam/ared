class AddColumnToRoles < ActiveRecord::Migration[5.0]
  def change
  	add_column :roles, :controller, :string
  	add_column :roles, :action, :string
  	add_column :roles, :route, :string
  end
end
