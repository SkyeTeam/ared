class CreateServiceFeesSlabs < ActiveRecord::Migration[5.0]
  def change
    create_table :service_fees_slabs do |t|
        t.integer :master_id
        t.string :comm_payer
      	t.float :min_amount
      	t.float :max_amount
      	t.string :comm_type
      	t.float :total_comm
      	t.float :our_comm
      	t.float :agent_comm
      	t.integer :sequence
        t.integer :status, default: 0
      	t.timestamps
    end
  end
end
