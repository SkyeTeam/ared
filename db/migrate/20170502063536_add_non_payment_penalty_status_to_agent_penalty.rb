class AddNonPaymentPenaltyStatusToAgentPenalty < ActiveRecord::Migration[5.0]
  def change
  	add_column :agent_penalty_details, :non_payment_status, :int, default: 0
  end
end
