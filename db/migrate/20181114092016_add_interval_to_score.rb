class AddIntervalToScore < ActiveRecord::Migration[5.0]
  def change
    add_column :scores, :interval, :string
  end
end
