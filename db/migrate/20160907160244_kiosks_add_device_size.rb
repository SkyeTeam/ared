class KiosksAddDeviceSize < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosks, :device_size_content, :integer
  end
end
