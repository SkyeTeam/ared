class AddRollbackstatusColumn < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :is_rollback, :int, default: 0
  end
end
