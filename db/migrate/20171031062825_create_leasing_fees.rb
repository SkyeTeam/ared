class CreateLeasingFees < ActiveRecord::Migration[5.0]
  	def change
	    create_table :leasing_fees do |t|
	    	t.float    :fee_amount
	      	t.datetime :applicable_from 
	      	t.integer  :status
	      	t.timestamps
	    end
  	end
end
