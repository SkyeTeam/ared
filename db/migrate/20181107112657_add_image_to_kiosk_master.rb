class AddImageToKioskMaster < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosk_masters, :image, :string
  end
end
