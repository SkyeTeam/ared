class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.references :agent, foreign_key: true
      t.references :officer, foreign_key: true
      t.boolean :agent_present
      t.boolean :uniform
      t.boolean :wifi
      t.boolean :replaced
      t.boolean :clean
      t.boolean :issue
      t.string :summary_type
      t.string :summary_text

      t.timestamps
    end
  end
end
