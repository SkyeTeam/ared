class CreateKiosk < ActiveRecord::Migration[5.0]
  def change
    create_table :kiosks do |t|
      t.string :identifier
      t.string :lon
      t.string :lat
      t.string :firmware
      t.integer :flags, null: false, default: 0
      t.string :internet_pin
      t.timestamps null: false
      t.datetime :last_online_at
    end
  end
end
