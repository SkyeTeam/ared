class CreateCreditFeeConfigs < ActiveRecord::Migration[5.0]
  def change
    create_table :credit_fee_configs do |t|
    	t.float   :amount
    	t.string  :approval_req
      t.float   :fee 
      t.integer :status
      t.timestamps
    end
  end
end
