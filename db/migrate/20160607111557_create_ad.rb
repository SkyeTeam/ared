class CreateAd < ActiveRecord::Migration[5.0]
  def change
    create_table :ads do |t|
      t.string :title
      t.string :image
      t.string :link
      t.integer :size
      t.integer :clicks
      t.boolean :active
      t.datetime :start_date
      t.datetime :end_date
      t.timestamps null: false
      t.belongs_to :area, index: true
    end
  end
end
