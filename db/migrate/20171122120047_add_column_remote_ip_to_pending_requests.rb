class AddColumnRemoteIpToPendingRequests < ActiveRecord::Migration[5.0]
  def change
  	add_column :pending_requests, :remote_ip, :string
  	add_column :pending_requests, :user_id, :integer
  	add_column :pending_requests, :role, :integer, default: 0
  end
end
