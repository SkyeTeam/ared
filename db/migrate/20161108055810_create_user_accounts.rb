class CreateUserAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :user_accounts do |t|
      t.integer :user_id
      t.string :acc_type
      t.string :currency
      t.float  :balance

      t.timestamps
    end
  end
end
