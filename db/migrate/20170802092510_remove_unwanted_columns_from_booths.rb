class RemoveUnwantedColumnsFromBooths < ActiveRecord::Migration[5.0]
  def change
  	remove_column :booths, :booth_name, :string
  	remove_column :booths, :booth_color, :string

  	#Code to add in a new column
  	add_column :booths, :user_id, :integer
  end
end
