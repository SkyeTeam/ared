class FirmwaresAreasCreateTable < ActiveRecord::Migration[5.0]
  def change
    create_table :areas_firmwares, id: false do |t|
      t.belongs_to  :area,  index: true
      t.belongs_to  :firmware, index: true
    end
  end
end
