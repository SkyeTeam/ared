class CreateMfPenaltyFees < ActiveRecord::Migration[5.0]
  def change
    create_table :mf_penalty_fees do |t|
    	t.string    :penalty_type
      	t.float 	:charges
      	t.integer 	:is_active
      	t.integer 	:status
      	t.timestamps
    end
  end
end
