class CreateQuickTellers < ActiveRecord::Migration[5.0]
  def change
    create_table :quick_tellers do |t|
    	t.integer :category_id
		  t.integer :biller_id
 	  	t.boolean :is_amount_fixed
 	  	t.integer :payment_item_id
 	  	t.string  :payment_item_name
 	  	t.integer :payment_code
    	t.integer :status, default: 0
    	t.timestamps
    end
  end
end
