class AddThreeColumnsToTxnHeader < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :agent_vat, :float
  	add_column :txn_headers, :ared_vat, :float
  	add_column :txn_headers, :vat_status, :integer, default: 0
  end
end