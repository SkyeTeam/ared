class CreateBooths < ActiveRecord::Migration[5.0]
  def change
    create_table :booths do |t|
      t.string :booth_name
      t.string :booth_model
      t.string :booth_color
      t.datetime :date_purchased

      t.timestamps
    end
  end
end
