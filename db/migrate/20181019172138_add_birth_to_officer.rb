class AddBirthToOfficer < ActiveRecord::Migration[5.0]
  def change
    add_column :officers, :birth, :date
    add_column :officers, :gender, :integer
    add_reference :officers, :country, foreign_key: true
    add_reference :officers, :district, foreign_key: true
    add_reference :officers, :town, foreign_key: true
  end
end
