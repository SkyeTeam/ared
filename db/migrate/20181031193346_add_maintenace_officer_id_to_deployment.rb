class AddMaintenaceOfficerIdToDeployment < ActiveRecord::Migration[5.0]
  def change
    add_reference :deployments, :maintenance_officer, foreign_key: true
  end
end
