class AddPaymentRefToAgentCommissions < ActiveRecord::Migration[5.0]
  def change
  	add_column :agent_commissions, :payment_ref, :int, default: 0
  end
end
