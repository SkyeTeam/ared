class CreateOneTimePasswords < ActiveRecord::Migration[5.0]
  def change
    create_table :one_time_passwords do |t|
    	t.string  :otp_id
    	t.string  :otp
    	t.string  :description
    	t.string  :comments
    	t.integer :status
      t.timestamps
    end
  end
end
