class AddColumnToUsers < ActiveRecord::Migration[5.0]
  def change
 	add_column :users, :business_name, :string
 	add_column :users, :tin_no, :integer, default: 0
  end
end
