class AddColumnToTxnHeaders < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :duration, :float, default: 0
  end
end
