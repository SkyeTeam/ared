class CreateWifiBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :wifi_bookings do |t|
      t.integer :duration
      t.integer :kind
      t.string :code
      t.integer :user_id
      t.integer :kiosk_id
      t.datetime :start
      t.timestamps
    end
  end
end
