class CreateVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :visits do |t|
      t.references :agent, foreign_key: true
      t.references :maintenance_officer, foreign_key: true
      t.string :reason
      t.boolean :fixed

      t.timestamps
    end
  end
end
