class AddColumnToAgentPenalty < ActiveRecord::Migration[5.0]
  def change
  	add_column :agent_penalty_details, :penalty_type_id, :integer, default: 0
  end
end
