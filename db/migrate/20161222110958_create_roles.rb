class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
    	t.string :role_name
    	t.string :role_type
    	t.string :role_sub_name
    	t.integer :status, :default => 0
      t.timestamps
    end
  end
end
