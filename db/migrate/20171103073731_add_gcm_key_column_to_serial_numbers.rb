class AddGcmKeyColumnToSerialNumbers < ActiveRecord::Migration[5.0]
  def change
  	add_column :serial_numbers, :gcm_key, :string
  end
end
