class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.belongs_to  :kiosk,       foreign_key: true
      t.string      :title
      t.integer     :priority
      t.integer     :status       #enum
      t.integer     :category     #enum
      t.text        :description

      t.timestamps
    end
  end
end
