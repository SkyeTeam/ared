class AddColumnsToAgentCommissions < ActiveRecord::Migration[5.0]
  def change
  	add_column :agent_commissions, :acc_balance, :float, default: 0
  	add_column :agent_commissions, :loan, :float, default: 0
  	add_column :agent_commissions, :penalty, :float, default: 0
  	add_column :agent_commissions, :total_comm, :float, default: 0
  	add_column :agent_commissions, :tax_percentage, :float, default: 0
  	add_column :agent_commissions, :total_tax, :float, default: 0
  end
end
