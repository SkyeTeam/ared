class CreateAgentPenaltyDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :agent_penalty_details do |t|
      t.integer :user_id
      t.float :loan_amount
      t.float :penalty_amount
      t.float :pending_penalty_amount

      t.timestamps
    end
  end
end
