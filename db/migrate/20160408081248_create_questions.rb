class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string :title
      t.integer :layout
      t.integer :order_id
      t.belongs_to :survey, foreign_key: true
    end
  end
end
