class AddUnitToTxnHeader < ActiveRecord::Migration[5.0]
  def change

  	add_column :txn_headers, :unit, :string
  	add_column :txn_headers, :vat, :string
  end
end
