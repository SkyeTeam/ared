class AddCaKeysToKiosk < ActiveRecord::Migration[5.0]
  def change
    add_column :kiosks, :trusted, :boolean, default: false
    add_column :kiosks, :ca, :string
    add_column :kiosks, :certificate, :string
    add_column :kiosks, :private_key, :string
    add_column :kiosks, :uuid, :string
  end
end
