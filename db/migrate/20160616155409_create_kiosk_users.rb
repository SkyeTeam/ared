class CreateKioskUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :kiosks_users, id: false do |t|
      t.belongs_to  :user,  index: true
      t.belongs_to  :kiosk, index: true
    end
  end
end
