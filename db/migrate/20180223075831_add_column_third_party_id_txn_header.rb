class AddColumnThirdPartyIdTxnHeader < ActiveRecord::Migration[5.0]
  def change
  	add_column :txn_headers, :third_party_txn_id, :string
  end
end
