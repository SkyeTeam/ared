class AddFeeColumnToLoans < ActiveRecord::Migration[5.0]
  def change
  	add_column :loans, :credit_fee, :float, default: 0
  end
end
