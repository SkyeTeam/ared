class ReplaceFlagsWithSingleFieldsInKiosk < ActiveRecord::Migration[5.0]
  def change
    remove_column :kiosks, :flags, :integer, default: 0
    add_column :kiosks, :wifi_status, :integer, default: 0
  end
end
