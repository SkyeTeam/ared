class FirmwaresAddActiveBoolean < ActiveRecord::Migration[5.0]
  def change
    add_column :firmwares, :active, :boolean
  end
end
