class CreateServiceFeesMasters < ActiveRecord::Migration[5.0]
  def change
    create_table :service_fees_masters do |t|
      	t.string :service_type
      	t.string :sub_type
      	t.datetime :effective_date
      	t.string :comm_payer
      	t.float :rate
      	t.integer :status, default: 0
      	t.timestamps
    end
  end
end
