class FirmwaresAddAreasAsIntegerArray < ActiveRecord::Migration[5.0]
  def change
    remove_column :firmwares, :areas, :string, default: [], array: true
    add_column :firmwares, :areas, :integer, default: [], array: true
  end
end
