# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181114092016) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "access_logs", force: :cascade do |t|
    t.string   "access_id"
    t.integer  "user_id"
    t.integer  "role"
    t.integer  "is_successful"
    t.string   "remote_ip"
    t.integer  "use_web"
    t.integer  "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "activity_logs", force: :cascade do |t|
    t.string   "action"
    t.string   "initiator"
    t.string   "description"
    t.string   "params"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ad_statistics", force: :cascade do |t|
    t.integer  "ad_id"
    t.integer  "kiosk_id"
    t.integer  "action"
    t.datetime "action_at"
    t.index ["ad_id"], name: "index_ad_statistics_on_ad_id", using: :btree
    t.index ["kiosk_id"], name: "index_ad_statistics_on_kiosk_id", using: :btree
  end

  create_table "ads", force: :cascade do |t|
    t.string   "title"
    t.string   "image"
    t.integer  "clicks"
    t.boolean  "active"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "area_id"
    t.string   "language"
    t.integer  "position"
    t.string   "link"
    t.index ["area_id"], name: "index_ads_on_area_id", using: :btree
  end

  create_table "agent_commissions", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "service_type"
    t.string   "sub_type"
    t.datetime "txn_date"
    t.float    "previous_balance"
    t.float    "post_balance"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "header_id",        default: 0
    t.integer  "status",           default: 0
    t.integer  "payment_ref",      default: 0
    t.float    "acc_balance",      default: 0.0
    t.float    "loan",             default: 0.0
    t.float    "penalty",          default: 0.0
    t.float    "total_comm",       default: 0.0
    t.float    "tax_percentage",   default: 0.0
    t.float    "total_tax",        default: 0.0
  end

  create_table "agent_credit_limits", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "loan_capacity_date"
    t.float    "loan_amount"
    t.integer  "status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "agent_penalty_details", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "loan_amount"
    t.float    "penalty_amount"
    t.float    "pending_penalty_amount"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "header_id",              default: 0
    t.integer  "non_payment_status",     default: 0
    t.integer  "penalty_type_id",        default: 0
  end

  create_table "agents", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "location"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "photo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "officer_id"
    t.index ["officer_id"], name: "index_agents_on_officer_id", using: :btree
  end

  create_table "aggregator_reconciliations", force: :cascade do |t|
    t.string   "service_type"
    t.string   "sub_type"
    t.datetime "txn_date"
    t.float    "txn_amount"
    t.float    "net_amount"
    t.float    "total_commission"
    t.float    "previous_balance"
    t.float    "post_balance"
    t.integer  "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "announcements", force: :cascade do |t|
    t.string   "announcement"
    t.integer  "created_by"
    t.datetime "txn_date"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "answers", force: :cascade do |t|
    t.text    "answer"
    t.integer "order_id"
    t.integer "question_id"
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
  end

  create_table "api_keys", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "api_key"
    t.integer  "status",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "applicants", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "areas", force: :cascade do |t|
    t.string   "title"
    t.string   "ancestry"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "storage_size", default: 0
    t.index ["ancestry"], name: "index_areas_on_ancestry", using: :btree
  end

  create_table "areas_firmwares", id: false, force: :cascade do |t|
    t.integer "area_id"
    t.integer "firmware_id"
    t.index ["area_id"], name: "index_areas_firmwares_on_area_id", using: :btree
    t.index ["firmware_id"], name: "index_areas_firmwares_on_firmware_id", using: :btree
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.string   "text"
    t.string   "language"
    t.string   "file"
    t.integer  "area_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["area_id"], name: "index_articles_on_area_id", using: :btree
    t.index ["category_id"], name: "index_articles_on_category_id", using: :btree
  end

  create_table "booths", force: :cascade do |t|
    t.string   "booth_model"
    t.datetime "date_purchased"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.hstore   "translations", default: [],              array: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "credit_fee_configs", force: :cascade do |t|
    t.float    "amount"
    t.string   "approval_req"
    t.float    "fee"
    t.integer  "status"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id",      default: -1
  end

  create_table "credit_suspends", force: :cascade do |t|
    t.integer  "agent_id"
    t.integer  "is_blocked"
    t.integer  "status",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "deployments", force: :cascade do |t|
    t.integer  "country_id"
    t.integer  "district_id"
    t.integer  "town_id"
    t.integer  "officer_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "maintenance_officer_id"
    t.index ["country_id"], name: "index_deployments_on_country_id", using: :btree
    t.index ["district_id"], name: "index_deployments_on_district_id", using: :btree
    t.index ["maintenance_officer_id"], name: "index_deployments_on_maintenance_officer_id", using: :btree
    t.index ["officer_id"], name: "index_deployments_on_officer_id", using: :btree
    t.index ["town_id"], name: "index_deployments_on_town_id", using: :btree
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_districts_on_country_id", using: :btree
  end

  create_table "editapplicantstatuses", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "firmwares", force: :cascade do |t|
    t.string   "title"
    t.string   "file"
    t.string   "version"
    t.string   "revision"
    t.string   "distribution"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "active"
  end

  create_table "gateway_logs", force: :cascade do |t|
    t.string   "log_id"
    t.string   "agent_id"
    t.string   "header_id"
    t.string   "aggregator_name"
    t.float    "aggregator_balance"
    t.float    "txn_amount"
    t.string   "request"
    t.string   "response"
    t.integer  "status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "gcm_nots", force: :cascade do |t|
    t.string   "gcm_key"
    t.string   "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gcm_nots_news", force: :cascade do |t|
    t.string   "gcm_key"
    t.string   "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "issues", force: :cascade do |t|
    t.integer  "kiosk_id"
    t.string   "title"
    t.integer  "priority"
    t.integer  "status",      default: 1
    t.integer  "category"
    t.text     "description"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "report"
    t.datetime "pending_at"
    t.datetime "closed_at"
    t.index ["kiosk_id"], name: "index_issues_on_kiosk_id", using: :btree
  end

  create_table "kiosk_agents", force: :cascade do |t|
    t.integer  "kiosk_master_id"
    t.integer  "agent_id"
    t.integer  "assign_status"
    t.integer  "functional_status"
    t.string   "comments"
    t.integer  "status",            default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "kiosk_masters", force: :cascade do |t|
    t.string   "model_no"
    t.string   "serial_no"
    t.datetime "purchase_date"
    t.string   "qr_code_no"
    t.integer  "functional_status"
    t.integer  "status",            default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "image"
  end

  create_table "kiosks", force: :cascade do |t|
    t.string   "identifier"
    t.string   "lon"
    t.string   "lat"
    t.string   "firmware"
    t.string   "internet_pin"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.datetime "last_online_at"
    t.integer  "area_id",             default: 1
    t.json     "status"
    t.string   "description"
    t.json     "client_task"
    t.json     "os_info"
    t.boolean  "trusted",             default: false
    t.string   "ca"
    t.string   "certificate"
    t.string   "private_key"
    t.string   "uuid"
    t.json     "gps_info"
    t.json     "wifi_info"
    t.json     "wifi_clients_info"
    t.json     "device_info_content"
    t.json     "device_info_db"
    t.integer  "device_size_content"
    t.index ["area_id"], name: "index_kiosks_on_area_id", using: :btree
  end

  create_table "kiosks_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "kiosk_id"
    t.index ["kiosk_id"], name: "index_kiosks_users_on_kiosk_id", using: :btree
    t.index ["user_id"], name: "index_kiosks_users_on_user_id", using: :btree
  end

  create_table "leasing_fees", force: :cascade do |t|
    t.float    "fee_amount"
    t.datetime "applicable_from"
    t.integer  "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "listquestions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loan_ratings", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "parameter"
    t.datetime "date"
    t.float    "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loans", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "acc_type"
    t.string   "currency"
    t.float    "amount"
    t.float    "balance"
    t.date     "due_date"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "non_payment_status", default: 0
    t.float    "credit_fee",         default: 0.0
  end

  create_table "maintenance_officers", force: :cascade do |t|
    t.string   "email"
    t.string   "phone"
    t.integer  "account"
    t.string   "password"
    t.string   "photo"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "state"
    t.date     "birth"
    t.integer  "gender"
    t.integer  "country_id"
    t.integer  "district_id"
    t.integer  "town_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["country_id"], name: "index_maintenance_officers_on_country_id", using: :btree
    t.index ["district_id"], name: "index_maintenance_officers_on_district_id", using: :btree
    t.index ["town_id"], name: "index_maintenance_officers_on_town_id", using: :btree
  end

  create_table "mf_penalty_fees", force: :cascade do |t|
    t.string   "penalty_type"
    t.float    "charges"
    t.integer  "is_active"
    t.integer  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "notification_news", force: :cascade do |t|
    t.string   "notification_content"
    t.string   "notification_title"
    t.integer  "user_id"
    t.integer  "status",               default: 0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "read_flag",            default: 0
  end

  create_table "officers", force: :cascade do |t|
    t.string   "email"
    t.string   "phone"
    t.integer  "account"
    t.string   "password"
    t.string   "photo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "state"
    t.date     "birth"
    t.integer  "gender"
    t.integer  "country_id"
    t.integer  "district_id"
    t.integer  "town_id"
    t.index ["country_id"], name: "index_officers_on_country_id", using: :btree
    t.index ["district_id"], name: "index_officers_on_district_id", using: :btree
    t.index ["town_id"], name: "index_officers_on_town_id", using: :btree
  end

  create_table "one_time_passwords", force: :cascade do |t|
    t.string   "otp_id"
    t.string   "otp"
    t.string   "description"
    t.string   "comments"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "payment_modes", force: :cascade do |t|
    t.string   "payment_mode_type"
    t.integer  "status",            default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "pending_credits", force: :cascade do |t|
    t.integer  "agent_id"
    t.float    "amount"
    t.datetime "txn_date"
    t.datetime "approval_date"
    t.integer  "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "app_unique_id"
  end

  create_table "pending_leasing_fees", force: :cascade do |t|
    t.integer  "agent_id"
    t.date     "end_period"
    t.float    "payment_amount"
    t.integer  "status"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "pending_requests", force: :cascade do |t|
    t.string   "txn_id"
    t.string   "service_type"
    t.string   "sub_type"
    t.string   "client_number"
    t.float    "amount"
    t.integer  "agent_id"
    t.integer  "status"
    t.datetime "txn_date"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "remote_ip"
    t.integer  "user_id"
    t.integer  "role",          default: 0
  end

  create_table "questions", force: :cascade do |t|
    t.string  "title"
    t.integer "layout"
    t.integer "order_id"
    t.integer "survey_id"
    t.index ["survey_id"], name: "index_questions_on_survey_id", using: :btree
  end

  create_table "quick_tellers", force: :cascade do |t|
    t.integer  "category_id"
    t.integer  "biller_id"
    t.boolean  "is_amount_fixed"
    t.integer  "payment_item_id"
    t.string   "payment_item_name"
    t.integer  "payment_code"
    t.integer  "status",            default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "agent_id"
    t.integer  "officer_id"
    t.boolean  "agent_present"
    t.boolean  "uniform"
    t.boolean  "wifi"
    t.boolean  "replaced"
    t.boolean  "clean"
    t.boolean  "issue"
    t.string   "summary_type"
    t.string   "summary_text"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "kiosk_master_id"
    t.index ["agent_id"], name: "index_reviews_on_agent_id", using: :btree
    t.index ["kiosk_master_id"], name: "index_reviews_on_kiosk_master_id", using: :btree
    t.index ["officer_id"], name: "index_reviews_on_officer_id", using: :btree
  end

  create_table "role_histories", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "role_name"
    t.string   "role_type"
    t.string   "role_sub_name"
    t.integer  "status",        default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "controller"
    t.string   "action"
    t.string   "route"
    t.integer  "order_view"
  end

  create_table "scores", force: :cascade do |t|
    t.string   "metric"
    t.string   "target"
    t.string   "points"
    t.string   "scorable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "option"
    t.string   "interval"
  end

  create_table "serial_numbers", force: :cascade do |t|
    t.string   "serial_number"
    t.integer  "user_id"
    t.integer  "active_flag",   default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "gcm_key"
  end

  create_table "service_fees", force: :cascade do |t|
    t.string   "service"
    t.string   "company"
    t.float    "total_comm_per"
    t.float    "total_comm_fix"
    t.float    "our_comm_per"
    t.float    "our_comm_fix"
    t.float    "agent_comm_per"
    t.float    "agent_comm_fix"
    t.string   "effective_date"
    t.string   "who_will_pay"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "duration",       default: 0
    t.float    "rate",           default: 0.0
  end

  create_table "service_fees_masters", force: :cascade do |t|
    t.string   "service_type"
    t.string   "sub_type"
    t.datetime "effective_date"
    t.string   "comm_payer"
    t.float    "rate"
    t.integer  "status",         default: 0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "vat_type"
  end

  create_table "service_fees_slabs", force: :cascade do |t|
    t.integer  "master_id"
    t.string   "comm_payer"
    t.float    "min_amount"
    t.float    "max_amount"
    t.string   "comm_type"
    t.float    "total_comm"
    t.float    "our_comm"
    t.float    "agent_comm"
    t.integer  "sequence"
    t.integer  "status",          default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.float    "aggregator_comm", default: 0.0
  end

  create_table "service_lists", force: :cascade do |t|
    t.string   "service_name"
    t.string   "service_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "service_settings", force: :cascade do |t|
    t.string   "service"
    t.integer  "status"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.float    "balance",                default: 0.0
    t.float    "threshold_amount",       default: 0.0
    t.string   "emails"
    t.datetime "notification_sent_date"
  end

  create_table "sms_registrations", force: :cascade do |t|
    t.string   "mobile_no"
    t.string   "purpose"
    t.string   "message"
    t.datetime "deliver_on"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suggestions", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "type"
    t.string   "user_comments"
    t.string   "admin_comments"
    t.integer  "status",         default: 1
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "survey_result_answers", force: :cascade do |t|
    t.integer "survey_result_id"
    t.integer "answer_id"
  end

  create_table "survey_results", force: :cascade do |t|
    t.integer  "kiosk_id"
    t.integer  "survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["kiosk_id"], name: "index_survey_results_on_kiosk_id", using: :btree
    t.index ["survey_id"], name: "index_survey_results_on_survey_id", using: :btree
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "title"
    t.integer  "area_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "active"
    t.string   "language"
  end

  create_table "tax_configurations", force: :cascade do |t|
    t.string   "service"
    t.float    "percentage"
    t.datetime "effective_date"
    t.integer  "status",         default: 0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "third_party_configs", force: :cascade do |t|
    t.string   "party_name"
    t.string   "username"
    t.string   "password"
    t.float    "connection_timeout"
    t.integer  "status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "towns", force: :cascade do |t|
    t.integer  "district_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.index ["district_id"], name: "index_towns_on_district_id", using: :btree
  end

  create_table "transcation_categories", force: :cascade do |t|
    t.string   "category_name"
    t.string   "category_sub_name"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "txn_headers", force: :cascade do |t|
    t.string   "service_type"
    t.string   "sub_type"
    t.datetime "txn_date"
    t.string   "currency_code"
    t.float    "amount"
    t.string   "feeId"
    t.string   "agent_id"
    t.float    "agent_commission"
    t.float    "opt_commission"
    t.float    "total_commission"
    t.integer  "status"
    t.string   "gateway"
    t.integer  "initated_by"
    t.string   "description"
    t.string   "account_id"
    t.string   "external_reference"
    t.string   "param1"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "unit"
    t.string   "vat"
    t.string   "electricity_costumer"
    t.integer  "is_rollback",          default: 0
    t.string   "area"
    t.string   "bill_no"
    t.float    "duration",             default: 0.0
    t.string   "third_party_txn_id"
    t.float    "aggregator_comm"
    t.float    "agent_vat"
    t.float    "ared_vat"
    t.integer  "vat_status",           default: 0
    t.float    "customer_fee",         default: 0.0
  end

  create_table "txn_items", force: :cascade do |t|
    t.integer  "header_id"
    t.integer  "user_id"
    t.float    "debit_amount"
    t.float    "credit_amount"
    t.integer  "account_id"
    t.integer  "fee_id"
    t.float    "fee_amt"
    t.float    "previous_balance"
    t.float    "post_balance"
    t.string   "role"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "uploads", force: :cascade do |t|
    t.string   "title"
    t.string   "file"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
    t.integer  "area_id"
    t.string   "language"
    t.integer  "category_id"
    t.index ["category_id"], name: "index_uploads_on_category_id", using: :btree
  end

  create_table "user_accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "acc_type"
    t.string   "currency"
    t.float    "balance"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "version_id", default: 0
  end

  create_table "user_govt_accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "acc_type"
    t.string   "currency"
    t.float    "balance"
    t.integer  "status",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                       null: false
    t.integer  "role",                            default: 0
    t.string   "crypted_password"
    t.string   "password_digest"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_me_token"
    t.datetime "remember_me_token_expires_at"
    t.string   "name"
    t.string   "phone"
    t.date     "date_of_birth"
    t.string   "address"
    t.string   "city"
    t.string   "country"
    t.string   "zip_code"
    t.integer  "status",                          default: 1
    t.string   "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.string   "business_name"
    t.integer  "tin_no",                          default: 0
    t.string   "image"
    t.string   "last_name"
    t.float    "amount_deposit"
    t.string   "description"
    t.string   "gender"
    t.string   "district"
    t.string   "national_id"
    t.integer  "email_flag",                      default: 0
    t.integer  "is_comm_include"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["phone"], name: "index_users_on_phone", unique: true, using: :btree
    t.index ["remember_me_token"], name: "index_users_on_remember_me_token", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

  create_table "visits", force: :cascade do |t|
    t.integer  "agent_id"
    t.integer  "maintenance_officer_id"
    t.string   "reason"
    t.boolean  "fixed"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["agent_id"], name: "index_visits_on_agent_id", using: :btree
    t.index ["maintenance_officer_id"], name: "index_visits_on_maintenance_officer_id", using: :btree
  end

  create_table "wifi_bookings", force: :cascade do |t|
    t.integer  "duration"
    t.integer  "kind"
    t.string   "code"
    t.integer  "user_id"
    t.integer  "kiosk_id"
    t.datetime "start"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.datetime "last_internet_ping"
    t.string   "mac_address"
  end

  create_table "wifi_lists", force: :cascade do |t|
    t.string   "duration"
    t.string   "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wifi_user_lists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "duration"
    t.integer  "amount"
    t.string   "refer_code"
    t.string   "costumer_phone"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_foreign_key "ad_statistics", "ads"
  add_foreign_key "ad_statistics", "kiosks"
  add_foreign_key "agents", "officers"
  add_foreign_key "answers", "questions"
  add_foreign_key "deployments", "countries"
  add_foreign_key "deployments", "districts"
  add_foreign_key "deployments", "maintenance_officers"
  add_foreign_key "deployments", "officers"
  add_foreign_key "deployments", "towns"
  add_foreign_key "districts", "countries"
  add_foreign_key "issues", "kiosks"
  add_foreign_key "maintenance_officers", "countries"
  add_foreign_key "maintenance_officers", "districts"
  add_foreign_key "maintenance_officers", "towns"
  add_foreign_key "officers", "countries"
  add_foreign_key "officers", "districts"
  add_foreign_key "officers", "towns"
  add_foreign_key "questions", "surveys"
  add_foreign_key "reviews", "agents"
  add_foreign_key "reviews", "officers"
  add_foreign_key "survey_results", "kiosks"
  add_foreign_key "survey_results", "surveys"
  add_foreign_key "towns", "districts"
  add_foreign_key "visits", "agents"
  add_foreign_key "visits", "maintenance_officers"
end
