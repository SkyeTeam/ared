# Preview all emails at http://localhost:3000/rails/mailers/officer_mailer
class OfficerMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/officer_mailer/send_notification
  def send_notification
    OfficerMailer.send_notification
  end

end
