require 'test_helper'

class InternetControllerTest < ActionDispatch::IntegrationTest
  test "should get tariff_settings" do
    get internet_tariff_settings_url
    assert_response :success
  end

  test "should get free_internet_settings" do
    get internet_free_internet_settings_url
    assert_response :success
  end

  test "should get free_website" do
    get internet_free_website_url
    assert_response :success
  end

  test "should get bandwidth" do
    get internet_bandwidth_url
    assert_response :success
  end

  test "should get internet_bandwidth_stats" do
    get internet_internet_bandwidth_stats_url
    assert_response :success
  end

  test "should get tokens" do
    get internet_tokens_url
    assert_response :success
  end

end
