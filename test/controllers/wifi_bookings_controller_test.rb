require 'test_helper'

class WifiBookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @wifi_booking = wifi_bookings(:one)
  end

  test "should get index" do
    get wifi_bookings_url
    assert_response :success
  end

  test "should get new" do
    get new_wifi_booking_url
    assert_response :success
  end

  test "should create wifi_booking" do
    assert_difference('WifiBooking.count') do
      post wifi_bookings_url, params: { wifi_booking: { code: @wifi_booking.code, duration: @wifi_booking.duration, kind: @wifi_booking.kind, kiosk_id: @wifi_booking.kiosk_id, user_id: @wifi_booking.user_id } }
    end

    assert_redirected_to wifi_booking_path(WifiBooking.last)
  end

  test "should show wifi_booking" do
    get wifi_booking_url(@wifi_booking)
    assert_response :success
  end

  test "should get edit" do
    get edit_wifi_booking_url(@wifi_booking)
    assert_response :success
  end

  test "should update wifi_booking" do
    patch wifi_booking_url(@wifi_booking), params: { wifi_booking: { code: @wifi_booking.code, duration: @wifi_booking.duration, kind: @wifi_booking.kind, kiosk_id: @wifi_booking.kiosk_id, user_id: @wifi_booking.user_id } }
    assert_redirected_to wifi_booking_path(@wifi_booking)
  end

  test "should destroy wifi_booking" do
    assert_difference('WifiBooking.count', -1) do
      delete wifi_booking_url(@wifi_booking)
    end

    assert_redirected_to wifi_bookings_path
  end
end
