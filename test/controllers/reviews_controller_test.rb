require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @review = reviews(:one)
  end

  test "should get index" do
    get reviews_url
    assert_response :success
  end

  test "should get new" do
    get new_review_url
    assert_response :success
  end

  test "should create review" do
    assert_difference('Review.count') do
      post reviews_url, params: { review: { agent_id: @review.agent_id, agent_present: @review.agent_present, clean: @review.clean, issue: @review.issue, officer_id: @review.officer_id, replaced: @review.replaced, summary_text: @review.summary_text, summary_type: @review.summary_type, uniform: @review.uniform, wifi: @review.wifi } }
    end

    assert_redirected_to review_url(Review.last)
  end

  test "should show review" do
    get review_url(@review)
    assert_response :success
  end

  test "should get edit" do
    get edit_review_url(@review)
    assert_response :success
  end

  test "should update review" do
    patch review_url(@review), params: { review: { agent_id: @review.agent_id, agent_present: @review.agent_present, clean: @review.clean, issue: @review.issue, officer_id: @review.officer_id, replaced: @review.replaced, summary_text: @review.summary_text, summary_type: @review.summary_type, uniform: @review.uniform, wifi: @review.wifi } }
    assert_redirected_to review_url(@review)
  end

  test "should destroy review" do
    assert_difference('Review.count', -1) do
      delete review_url(@review)
    end

    assert_redirected_to reviews_url
  end
end
