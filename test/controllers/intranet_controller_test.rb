require 'test_helper'

class IntranetControllerTest < ActionDispatch::IntegrationTest
  test "should get cms_content_uploader" do
    get intranet_cms_content_uploader_url
    assert_response :success
  end

  test "should get cms_surveys" do
    get intranet_cms_surveys_url
    assert_response :success
  end

  test "should get cms_categories" do
    get intranet_cms_categories_url
    assert_response :success
  end

  test "should get cms_client_feedback" do
    get intranet_cms_client_feedback_url
    assert_response :success
  end

  test "should get kms_kiosk_list" do
    get intranet_kms_kiosk_list_url
    assert_response :success
  end

end
