require 'test_helper'

class MaintenanceOfficersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @maintenance_officer = maintenance_officers(:one)
  end

  test "should get index" do
    get maintenance_officers_url
    assert_response :success
  end

  test "should get new" do
    get new_maintenance_officer_url
    assert_response :success
  end

  test "should create maintenance_officer" do
    assert_difference('MaintenanceOfficer.count') do
      post maintenance_officers_url, params: { maintenance_officer: { account: @maintenance_officer.account, birth: @maintenance_officer.birth, country_id: @maintenance_officer.country_id, district_id: @maintenance_officer.district_id, email: @maintenance_officer.email, first_name: @maintenance_officer.first_name, gender: @maintenance_officer.gender, last_name: @maintenance_officer.last_name, middle_name: @maintenance_officer.middle_name, password: @maintenance_officer.password, phone: @maintenance_officer.phone, photo: @maintenance_officer.photo, state: @maintenance_officer.state, town_id: @maintenance_officer.town_id } }
    end

    assert_redirected_to maintenance_officer_url(MaintenanceOfficer.last)
  end

  test "should show maintenance_officer" do
    get maintenance_officer_url(@maintenance_officer)
    assert_response :success
  end

  test "should get edit" do
    get edit_maintenance_officer_url(@maintenance_officer)
    assert_response :success
  end

  test "should update maintenance_officer" do
    patch maintenance_officer_url(@maintenance_officer), params: { maintenance_officer: { account: @maintenance_officer.account, birth: @maintenance_officer.birth, country_id: @maintenance_officer.country_id, district_id: @maintenance_officer.district_id, email: @maintenance_officer.email, first_name: @maintenance_officer.first_name, gender: @maintenance_officer.gender, last_name: @maintenance_officer.last_name, middle_name: @maintenance_officer.middle_name, password: @maintenance_officer.password, phone: @maintenance_officer.phone, photo: @maintenance_officer.photo, state: @maintenance_officer.state, town_id: @maintenance_officer.town_id } }
    assert_redirected_to maintenance_officer_url(@maintenance_officer)
  end

  test "should destroy maintenance_officer" do
    assert_difference('MaintenanceOfficer.count', -1) do
      delete maintenance_officer_url(@maintenance_officer)
    end

    assert_redirected_to maintenance_officers_url
  end
end
