Rails.application.routes.draw do

  
  
  
  resources :scores do 
    collection do 
      get :maintenance 
      get :franchisee
    end
  end

  resources :visits
  resources :maintenance_officers do 
     collection do 
      get :deployed
      get :suspended
      get :terminated
    end

    member do 
      get :accept 
      get :decline
      get :deploy
      post :deploy_maintenance_officer
      get :suspend
      get :terminate
      get :reactivate
      get :password_reset
    end
  end

  resources :countries do
    resources :districts do 
      resources :towns
    end
  end
  resources :officers do 
    collection do 
      get :deployed
      get :suspended
      get :terminated
    end

    member do 
      get :accept 
      get :decline
      get :deploy
      post :deploy_officer
      get :suspend
      get :terminate
      get :reactivate
      get :password_reset
    end

    resources :agents do 
      resources :reviews
    end
  end
  get '/reviews/:kiosk_id/officer/:id' => "officers#reviews" ,as: :show_review

 #FOR AJAX REQUESTS 
 get '/listingtowns' => 'officers#listings_towns' 
 get '/listingdistricts' => 'officers#listings_districts' 
  
  root 'sessions#new'

  match '/ChangePassword' => 'sessions#change_admin_pin', via: [:post]

  match '/ActivatePhone' => 'users#activatePhone', via: [:get, :post]
  match '/DeActivatePhone' => 'users#deActivatePhone', via: [:get, :post]
  match '/UserSearch' => 'users#searchByPhoneOrName', via: [:get, :post]
  
  match '/search_usr/update' => 'search_usr#new', :status =>"update", via: [:get, :post]
  match '/search_usr/suspend' => 'search_usr#new', :status =>"suspend", via: [:get, :post]
  match '/search_usr/delete' => 'search_usr#new', :status =>"delete", via: [:get, :post] 
  match '/search_usr/agent_profile' => 'search_usr#new', :status =>"agent_profile", via: [:get, :post] 
  
  match 'users/user_role' => 'users#user_role', via: [:get, :post]
  
  match '/ServiceSettings' => 'services_setting#index', via: [:get, :post]
  match '/EnableServiceSettings' => 'services_setting#enable', via: [:post]
  match '/DisableServicesSettings' => 'services_setting#disable', via: [:get, :post]
  match '/ViewThreshold' => 'services_setting#threshold', via: [:get, :post]
  match '/SetThreshold' => 'services_setting#setThreshold', via: [:get, :post]
  
  match '/CommissionPayment' => 'commission_payment#index', via: [:get, :post]
  match '/search_commission_payment' => 'commission_payment#search', via: [:get, :post]
  match '/pay_commission' => 'commission_payment#payCommission', via: [:get, :post]
  
  match '/TransactionHistory' => 'txn_history#index', via: [:get,:post]
 
  match '/PenaltyDetails' => 'penalty_report#searchPenaltyDetails', via: [:get,:post]

  match '/ProfitLoseDetails' => 'profit_lose_report#searchProfitLoseDetails', via: [:get,:post]

  match '/TaxDetails' => 'tax_report#searchTaxDeductDetails', via: [:get,:post]
  
  match '/CommissionPaymentDetails' => 'commission_payment_report#searchCommissionPaymentDetails', via: [:get,:post]

  match '/MFCashDeposit' => 'cash_deposit#index', via: [:get,:post]
  match '/MFCashDepositSearch' => 'cash_deposit#searchMF', via: [:get,:post]
  match '/MFCashDepositConfirm' => 'cash_deposit#depositFloat', via: [:post]

  match '/SearchNamePayment' => 'cash_payment#search_name', via: [:get,:post]
  match '/Reimbursement' => 'cash_payment#payment', via: [:post]

  match '/SearchUserName' => 'search_usr#search_name', via: [:get,:post]

  match '/CheckUserName' => 'reset_password#search_name', via: [:get,:post]
 
  match '/TaxConfiguration' => 'tax_configuration#index', via: [:get,:post]
  match '/TaxSetup' => 'tax_configuration#new',via: [:get,:post]
  match '/CreateTaxSetup' =>'tax_configuration#create', via: [:post]
  
  match '/PDFDownload' => 'pdf_generator#index', via: [:get,:post]

  match '/InitiateTxnRollback' => 'txn_rollback_initiate#index', via: [:get,:post]
  match '/InitiatedTxnRollback' => 'txn_rollback_initiate#initiateTxnRollback', via: [:get,:post] 
  match '/InitiatedTxnRollbackConfirm' => 'txn_rollback_initiate#initiatedTxnRollbackConfirm', via: [:get,:post]
  
  match '/ApproveTxnRollback' => 'txn_rollback_approve#index', via: [:get,:post]
  match '/ApproveSingleTxnRollback' => 'txn_rollback_approve#approveSingleTxn', via: [:get,:post]
  match '/ApprovedTxnRollbackConfirm' => 'txn_rollback_approve#approveTxnRollbackConfirm', via: [:get,:post]

  match '/CreditFeeSetup' => 'credit_fee_setup#index', via: [:get,:post]
  match '/CreditSetupDetails' => 'credit_fee_setup#getCreditDetails', via: [:get,:post]
  match '/CreditSetupConfirm' => 'credit_fee_setup#setupCredit', via: [:get,:post]

  match '/CreditSuspend' => 'credit_suspend#index', via: [:get,:post]
  match '/CreditSuspendPhone' => 'credit_suspend#searchByPhone', via: [:get,:post]
  match '/CreditSuspendName' => 'credit_suspend#searchByName', via: [:get,:post]
  match '/CreditSuspendConfirm' => 'credit_suspend#suspendCredit', via: [:get,:post]

  match '/CreditApproval' => 'credit_approval#index', via: [:get,:post]
  match '/ViewCredit' => 'credit_approval#viewSingleCredit', via: [:get,:post]
  match '/ApprovedCredit' => 'credit_approval#approvedCredit', via: [:get,:post]

  match '/CreditReport' => 'credit_report#index', via: [:get,:post]

  match '/MFCommissionReport' => 'mf_commission_report#index', via: [:get,:post]
  match '/SingleMFCommissionReport' => 'mf_commission_report#singleUserReport', via: [:get,:post]
  
  match '/AredRevenueReport' => 'revenue#index', via: [:get,:post]

  match '/TerminateUser' => 'users#destroy', via: [:get,:post]

  match '/ValidateOTP' => 'sessions#validateOTP', via: [:get,:post] 

  match '/Announcements' => 'announcements#index', via: [:get,:post]
  match '/CreateAnnouncement' => 'announcements#createAnnouncement', via: [:get,:post]

  match '/LeasingFeeSetup' => 'leasing_fee_setup#index', via: [:get,:post]
  match '/NewLeasingFeeSetup' => 'leasing_fee_setup#new', via: [:get,:post]
  match '/CreateLeasingFeeSetup' => 'leasing_fee_setup#createLeasingFeeSetup', via: [:get,:post]

  match '/TrackCustomer' => 'track_customer#index', via: [:get,:post]  

  match '/AggregatorSignup' => 'aggregator#index', via: [:get,:post]  
  match '/NewAggregatorSignup' => 'aggregator#new', via: [:get,:post]
  match '/NewAggregatorCreate' => 'aggregator#create', via: [:get,:post]

  match '/AggregatorDeposit' => 'aggregator_deposit#index', via: [:get,:post]  
  match '/AggregatorDepositAmt' => 'aggregator_deposit#deposit', via: [:get,:post]

  match '/AggregatorReport' => 'aggregator_report#index', via: [:get,:post]  
  
  match '/AuditLogs' => 'audit_logs#index', via: [:get, :post]

  match '/ThirdPartyConfig' => 'third_party_config#index', via: [:get, :post]
  match '/ThirdPartyConfigSave' => 'third_party_config#updateConfigs', via: [:get, :post]

  match '/MFFeedbacks' => 'suggestions#index', via: [:get, :post]

  match '/MFPenalties' => 'mf_penalty#index', via: [:get, :post]
  match '/MFPenaltiesPhone' => 'mf_penalty#searchByPhone', via: [:get, :post]
  match '/MFPenaltiesName' => 'mf_penalty#searchByName', via: [:get, :post]
  match '/MFAddPenalties' => 'mf_penalty#addPenalty', via: [:get, :post]

  match '/PenaltySetup' => 'mf_penalty_fee_setup#index', via: [:get, :post]
  match '/NewPenaltySetup' => 'mf_penalty_fee_setup#new', via: [:get, :post]
  match '/CreatePenalty' => 'mf_penalty_fee_setup#create', via: [:get, :post]
  match '/DeletePenaltySetup' => 'mf_penalty_fee_setup#delete', via: [:get, :post]

  match '/SuspendUser' => 'users#suspend_account', via: [:get, :post]
  match '/TerminateUser' => 'users#destroy', via: [:get, :post]

  match '/APIKeyGeneration' => 'api_key#index', via: [:get, :post]
  match '/APIKeyGenerated' => 'api_key#generateKey', via: [:get, :post]

  match '/ServiceFees' => 'service_fees_setup#index', via: [:get, :post]
  match '/ServiceFeesNew' => 'service_fees_setup#new', via: [:get, :post]
  match '/ServiceFeesSetup' => 'service_fees_setup#create', via: [:get, :post]
  match '/ServiceFeesDetails' => 'service_fees_setup#show', via: [:get, :post]

  match '/MFProfile' => 'user_profiles#profile', via: [:get, :post]

  match '/AssignKiosk' => 'kiosks#index', via: [:get, :post]
  match '/NewKiosk' => 'kiosks#new', via: [:get, :post]
  match '/CreateKiosk' => 'kiosks#create', via: [:get, :post]
  match '/AssignNewKiosk' => 'kiosks#assign', via: [:get, :post]
  match '/AssignedNewKiosk' => 'kiosks#assigned', via: [:get, :post]
  match '/SearchKiosk' => 'kiosks#search', via: [:get, :post]
  get '/printcode/:id' => 'kiosks#print' ,as: :printcode
  match '/ReGenerateKioskQRCode' => 'kiosks#regenerate', via: [:get, :post]
  match '/DownloadKioskQRCode' => 'kiosks#downloadQRCode', via: [:get, :post]

  match '/ForgotPassword' => 'forgot_passowrd#index', via: [:get, :post]
  match '/ValidateForNewPassword' => 'forgot_passowrd#validate', via: [:get, :post]
  match '/ResetPasswordDetails' => 'forgot_passowrd#new', via: [:get, :post]
  match '/SetNewPassword' => 'forgot_passowrd#forgot', via: [:get, :post]

  match '/PaymentModes' => 'payment_modes#index', via: [:get, :post]
  match '/AddPaymentMode' => 'payment_modes#new', via: [:get, :post]
  match '/CreatePaymentMode' => 'payment_modes#create', via: [:get, :post]
  match '/DeletePaymentMode' => 'payment_modes#delete', via: [:get, :post]
  

  
  
  post 'users/new_role_save' => 'users#new_role_save'
  get 'users/:id/suspend_account' => 'users#suspend_account', as: :users_suspend_account
  get 'users/:id/suspend' => 'users#suspend', as: :users_susp
  get 'users/:id/delete_usr' => 'users#delete_usr', as: :users_del
  get "change_pin" => "sessions#change_pin", as: "change_pin"
  get "Dashboard" => "sessions#Dashboard", as: "Dashboard"
  post 'logout' => 'sessions#destroy', :as => :logout
  delete 'sessions' => 'sessions#destroy'
  get 'mtopup_history/new'
  get 'mtopup_history/new'
  get '/users/:id/profile' => 'user_profiles#profile', as: :users_suspet

  get 'intranet/cms_content_uploader'

  get 'intranet/cms_surveys'

  get 'intranet/cms_categories'

  get 'intranet/cms_client_feedback'

  get 'intranet/kms_kiosk_list'

  get 'internet/tariff_settings'

  get 'internet/free_internet_settings'

  get 'internet/free_website'

  get 'internet/bandwidth'

  get 'internet/internet_bandwidth_stats'

  get 'internet/tokens'

  get 'internet/online_offline_stats'

  resources :sessions
  resources :service_fees
  resources :users
  resources :pdf_generator
  resources :user_accounts
  resources :txn_history
  resources :cash_payment
  resources :search_user

  resources :user_profiles do
    collection do
      post :profile
    end
  end

  resources :cash_payment do
    collection do
      post :search_name
    end
  end

  resources :reset_password do
    collection do
      get :reset
      post :search_name
    end
  end
  
  resources :search_usr do
    collection do
      get :reset
      post :search_name
    end
  end
  
  resources :sessions do
    collection do
      post :change_admin_pin
    end
  end

  # ************** Starting of the APP routes ***************
  scope module: "api", path: "api", controller: "api" do
    scope module: "v01", path: "v0.1" do

      
      resources :field_officer,only: [:create] do 
        collection do 
          post :authenticate
          post :stats 
          post :history 
          post :agents
          post :get_kiosk
        end
      end

      resources :agent_officer,only: [:create] do 
        collection do 
          post :find
          post :review
        end
      end

      resources :agents do
        collection do
          post :changePin
          post :loanDetails
          post :getAccountInfo
          post :txnHistory
          post :commissionDetails
          post :balanceEnquiry
          post :getNotification
          post :getDetails
          post :getLoanStatus
          post :checkNationalID
          post :forgetPin
          post :getTransactionCategory
          post :readFlag
          post :commissionSum
          post :txnHistoryBWDates
          post :receipt
          post :penaltyHistory
          post :getServiceCharges
          post :test
        end
      end

      resources :txn_rollbacks do
        collection do
          post :txnRollbackInitiate
          post :checkPendingTxnStatus
        end
      end

      resources :request_credits do
        collection do
          post :requestCredit
        end
      end

      resources :electricity_recharges do
        collection do
          post :checkMeterNumber
          post :rechargeMeterNumber
        end
      end

      resources :mobile_recharges do
        collection do
          post :rechargeMobile
        end
      end

      resources :tv_recharges do
        collection do
          post :rechargeTV
        end
      end

      resources :announcements do
        collection do
          post :getAnnouncement
        end
      end

      resources :logins do
        collection do
          post :doLogin
        end
      end

      resources :internets do
        collection do
          post :getInternetDurations
          post :getInternetToken
        end
      end

      resources :suggestions do
        collection do
          post :suggestionTypes
          post :sendSuggestions
        end
      end

      resources :taxes do
        collection do
          post :validateBillNumber
          post :billPayment
        end
      end

      resources :ugandas do
        collection do
          post :rechargeMobile
          post :getNationalWaterAreaList   
          post :validateCustomerForWaterBill 
          post :payCustomerWaterBill
          post :checkMeterNumber
          post :rechargeMeterNumber
        end
      end

      resources :cash_deposit do
        collection do
          post :depositFloat
        end
      end

      # ************** Start of the API only routes ***************

      post 'auth/login', to: 'authentication#authenticate'
      post 'signup', to: 'users#create'
      
    end
  end

  namespace 'mshiriki' do
        
    match '1.1/MNO/*originId/customers/*customerId/payments' => 'symbox#creditAgent', via: [:post, :get]

  end 



  # ************** Ending of the APP routes ***************

end