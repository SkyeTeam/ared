set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.3.0'

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails rack}
set :rbenv_roles, :all # default value

# config valid only for current version of Capistrano
lock '3.11.0'

set :application, 'ared-admin'
set :scm, :git
set :repo_url, 'ssh://git@github.com/ARED-Group/ared-admin.git'
set :ssh_options, { :forward_agent => true }

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets public/uploads}

# Default value for keep_releases is 5
set :keep_releases, 5

# Whenever
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

namespace :deploy do
  desc 'Run rake yarn:install'
  task :yarn_install do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install")
      end
    end
  end

  desc 'npm install'
  task :npm_install do
    on roles(:web), in: :sequence, wait: 5 do
      within release_path do
        execute :npm, :install
      end
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end
end

# before :deploy, :checkbranch
before 'deploy:assets:precompile','deploy:npm_install'
after 'deploy:publishing', 'deploy:restart'

# after :deploy, "deploytags:clean"
