Rails.application.configure do

  config.cache_classes = false
  config.eager_load = false
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries= true

  config.action_mailer.delivery_method = :letter_opener
  config.action_mailer.perform_deliveries = true

  config.active_support.deprecation = :log
  config.active_record.migration_error = :page_load
  config.assets.debug = true
  config.assets.digest = true
  config.assets.raise_runtime_errors = true
  config.web_console.whitelisted_ips = ['192.168.1.2', '192.168.1.3', '192.168.1.5','192.168.1.8', '192.168.1.12', '192.168.1.13', '192.168.1.15', '192.168.1.16', '192.168.1.21']

end  