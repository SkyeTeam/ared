Rails.application.configure do

  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries= true
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    user_name: "mshirikiafrica6513@gmail.com",
    password: "Merciless1234",
    address: "smtp.gmail.com",
    port: 587,
    domain: "gmail.com",
    authentication: 'plain',
    enable_starttls_auto: true
  }

  config.public_file_server.enabled = false
  config.assets.js_compressor = :uglifier
  config.assets.compile = false
  config.assets.digest = true
  config.log_level = :debug
  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.log_formatter = ::Logger::Formatter.new
  config.active_record.dump_schema_after_migration = false

   config.middleware.use ExceptionNotification::Rack,
    :email => {
    :email_prefix => "[PREFIX] ",
    :sender_address => %{"notifier" <notifier@ared.com>},
    :exception_recipients => %w{dunake@gmail.com}
  }
  
end