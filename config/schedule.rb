# *     *     *   *    *        command to be executed
# -     -     -   -    -
# |     |     |   |    |
# |     |     |   |    +----- day of week (0 - 6) (Sunday=0)
# |     |     |   +------- month (1 - 12)
# |     |     +--------- day of month (1 - 31)
# |     +----------- hour (0 - 23)
# +------------- min (0 - 59)

set :output, 'log/whenever.log'
every :day, :at => '1:30 am' do
  rake 'survey:activate_by_startdate'
  rake 'survey:deactivate_by_enddate'
end

every :day do
    runner "Part.check_status_update",:environment => 'development'
end	

#*****This @environment varaibale is set at the time of  write crontab command as whenever --set environment=production  --write-crontab
case @environment

	when 'production'

		every :day, :at => '10:15am' do
			rake "expire:demo_prod" 
		end

		# this will update the agent credit limit
		every :day, :at => '8:00am' do
			rake "expire:challanges"
		end	

		# this will send the loan notification
		every :day, :at => '8:05am' do
		 	rake "expire:loan_notify"
		end	

		# this will describe the fine if loan is not paid at due date
		every :day, :at => '8:10am' do 
			rake "expire:loan_unpaid"
		end	

		#This is used to deduct the leasing fee from the agent account on every month
		every '5 0 10 * *' do
			rake "leasingFee:deductLeasingFee"
		end	

		#This is used to deduct the pending leasing fee from the agent account on every day
		every :day, :at =>  '0:10am' do 
			rake "leasingFee:deductPendingLeasingFee"
		end	

		#This is used to deduct the agent VAT from the agent account on every day
		every :day, :at =>  '0:15am' do 
			rake "vat:deductAgentsVAT"
		end	

		#This is used to deduct the ARED VAT from the commission account on every day
		every :day, :at =>  '0:20am' do 
			rake "vat:deductAREDVAT"
		end	

	when 'development'

		every 1.minutes do 
			rake "expire:demo", :environment => 'development'
		end	

		# this will update the agent credit limit
		every :day, :at =>  '11:05am' do 
			rake "expire:challanges", :environment => 'development'
		end	

		# this will send the loan notification
		every :day, :at =>  '11:10am' do 
		 	rake "expire:loan_notify", :environment => 'development'
		end	

		# this will describe the fine if loan is not paid at due date
		every :day, :at =>  '11:15am' do 
			rake "expire:loan_unpaid", :environment => 'development'
		end	

		#This is used to deduct the leasing fee from the agent account on every month
		every :day, :at =>  '10:30am' do 
			rake "leasingFee:deductLeasingFee", :environment => 'development'
		end	

		#This is used to deduct the pending leasing fee from the agent account on every day
		every :day, :at =>  '10:35am' do 
			rake "leasingFee:deductPendingLeasingFee", :environment => 'development'
		end	

		#This is used to deduct the agent VAT from the agent account on every day
		# every :day, :at =>  '10:35am' do 
		every 1.minutes do 
			rake "vat:deductAgentsVAT", :environment => 'development'
		end	

		#This is used to deduct the ARED VAT from the commission account on every day
		# every :day, :at =>  '10:35am' do 
		every 1.minutes do 
			rake "vat:deductAREDVAT", :environment => 'development'
		end	

end