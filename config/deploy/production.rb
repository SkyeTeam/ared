set :stage, :production
set :rails_env, :production

server = '13.93.122.87'

role :app, server, user: :ared
role :web, server, user: :ared
role :db, server, user: :ared

set :deploy_to, '/home/ared/ared-admin/'

set :branch, 'production'
