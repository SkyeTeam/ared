set :stage, :hardware
set :rails_env, :hardware

# server = 'ared.staging.9elements.com'
server = '144.76.92.84'

role :app, server, user: :ared
role :web, server, user: :ared
role :db, server, user: :ared

set :deploy_to, '/home/ared/ared-hardware-admin/'

def current_branch
  ref = `git symbolic-ref HEAD`.chomp
  pre_ref = 'refs/heads/'
  raise 'Unable to determine current branch' unless ref.start_with?(pre_ref)
  branch = ref[pre_ref.length..-1]
  puts "Deploying would/will now use branch #{branch}"
  branch
end

set :branch, ENV['BRANCH'] || current_branch
