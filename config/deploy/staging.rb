set :stage, :staging
set :rails_env, :staging

# server = 'ared.staging.9elements.com'
server = '13.84.48.214'

role :app, server, user: :henri01
role :web, server, user: :henri01
role :db, server, user: :henri01

set :deploy_to, '/home/henri01/ared-admin/'

def current_branch
  ref = `git symbolic-ref HEAD`.chomp
  pre_ref = 'refs/heads/'
  raise 'Unable to determine current branch' unless ref.start_with?(pre_ref)
  branch = ref[pre_ref.length..-1]
  puts "Deploying would/will now use branch #{branch}"
  branch
end

#set :branch, ENV['BRANCH'] || current_branch
