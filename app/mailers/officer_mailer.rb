class OfficerMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.officer_mailer.send_notification.subject
  #
 def send_notification(officer)
   @officer = officer
   mail(to: @officer.email, subject: "Greetings #{officer.name}")
 end

 def suspended(officer)
   @officer = officer
   mail(to: @officer.email, subject: "Account with ARED has been suspended")
 end
end
