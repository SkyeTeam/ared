class RegisterMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.register_mailer.success.subject
  #
  def success
    @greeting = "Hi"

    mail to: "to@example.org"
  end
end
