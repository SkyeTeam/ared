json.extract! score, :id, :metric, :target, :points, :scorable_id, :scorable_type, :created_at, :updated_at
json.url score_url(score, format: :json)
