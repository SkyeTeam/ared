json.extract! officer, :id, :email, :phone, :account, :password, :photo, :created_at, :updated_at
json.url officer_url(officer, format: :json)
