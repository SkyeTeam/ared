json.extract! visit, :id, :agent_id, :maintenance_officer_id, :reason, :fixed, :created_at, :updated_at
json.url visit_url(visit, format: :json)
