json.extract! maintenance_officer, :id, :email, :phone, :account, :password, :photo, :first_name, :middle_name, :last_name, :state, :birth, :gender, :country_id, :district_id, :town_id, :created_at, :updated_at
json.url maintenance_officer_url(maintenance_officer, format: :json)
