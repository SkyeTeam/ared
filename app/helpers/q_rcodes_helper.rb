module QRcodesHelper
	
	require 'barby'
	require 'barby/barcode'
	require 'barby/barcode/qr_code'
	require 'barby/outputter/png_outputter'
	  
	def generateQRCode(text)
	  	barcode = Barby::QrCode.new(text, level: :q, size: 5)
	  	barcodeOutput = Base64.encode64(barcode.to_png({ xdim: 5 }))
	  	"data:image/png;base64,#{barcodeOutput}"
	end
	
end
