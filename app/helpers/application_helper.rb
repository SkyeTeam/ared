module ApplicationHelper

  def bootstrap_class_for flash_type
    {
      success: "alert-success",
      error: "alert-danger",
      alert: "alert-warning",
      notice: "alert-info"
    }.fetch(flash_type.to_sym)
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do
              concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end

  def areas_for_select
    ancestry_options(Area.unscoped.arrange(:order => 'title')) {|i| "#{'&nbsp;' * i.depth * 2} #{i.title}".html_safe }
  end

  def get_category_by_id id
    category = Category.find_by(id: id)
    if category
      return category.title
    end
    ""
  end

  private
  def ancestry_options(items)
    result = []
    items.map do |item, sub_items|
      result << [yield(item), item.id]
      #this is a recursive call:
      result += ancestry_options(sub_items) {|i| "#{'&nbsp;' * i.depth * 2} #{i.title}".html_safe }
    end
    result
  end

  def self.createHeader(serviceType, subType, txnAmount, description, accountID)
    txnHeader = TxnHeader.new
      
      txnHeader.service_type = serviceType
      txnHeader.sub_type =  subType
      txnHeader.txn_date = Time.now
      txnHeader.currency_code = Utility.getCurrencyLabel
      txnHeader.amount = TxnHeader.my_money(txnAmount).to_f
      txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
      txnHeader.gateway = Constants::TXN_GATEWAY
      txnHeader.description = description
      txnHeader.account_id = accountID

    return txnHeader
  end

  def self.createPenaltyHeader(service_type, sub_service_type, txn_amount,agent_id, description, account_id)
    txnHeader = TxnHeader.new
      
      txnHeader.service_type = service_type
      txnHeader.sub_type =  sub_service_type
      txnHeader.txn_date = Time.now
      txnHeader.currency_code = Utility.getCurrencyLabel
      txnHeader.amount = txn_amount
      txnHeader.agent_id = agent_id
      txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
      txnHeader.gateway = Constants::TXN_GATEWAY
      txnHeader.description = description
      txnHeader.account_id = account_id

    return txnHeader
  end


  def self.getTxnItem(header_id, user_id, debit_amount, credit_amount, account_id, previous_balance, post_balance)

    txn_item = TxnItem.new
    txn_item.header_id = header_id
    txn_item.user_id= user_id
    txn_item.debit_amount = debit_amount
    txn_item.credit_amount = credit_amount
    txn_item.account_id = account_id # id from the user account table
    txn_item.previous_balance = previous_balance
    txn_item.post_balance = post_balance
   
    return txn_item
  end

  def self.get_txn_item(header_id, user_account, amount, fee_amt, role)
    txn_item = TxnItem.new
    txn_item.header_id = header_id
    txn_item.user_id= user_account.user_id
    previousBalance = Utility.getAccountBalance(user_account).to_f
    Log.logger.info("Application Helpaer previous balance of the user = #{user_account.user_id}  = #{previousBalance}")
    txn_item.previous_balance = previousBalance.to_f
    txn_item.account_id = user_account.id # id from the user account table
    txn_item.fee_amt = fee_amt

    if role == Constants::PAYER_LABEL
      txn_item.debit_amount = amount
      txn_item.role = Constants::PAYER_LABEL
      txn_item.post_balance = previousBalance.to_f - amount.to_f
    else
      txn_item.credit_amount = amount
      txn_item.role = role
      txn_item.post_balance = previousBalance.to_f + amount.to_f
    end
    txn_item.save
  end


  def self.get_agent_commission(service_type, sub_type, agent, commission_amount, header_id)
      post_balance = Utility.getAgentCommissionBalance(agent)

      agent_comm = AgentCommission.new
      agent_comm.user_id = agent.id
      agent_comm.service_type = service_type
      agent_comm.sub_type = sub_type
      agent_comm.txn_date = Time.now
      agent_comm.previous_balance = post_balance
      agent_comm.post_balance = post_balance + commission_amount
      agent_comm.header_id = header_id
      agent_comm.save

  end  
end