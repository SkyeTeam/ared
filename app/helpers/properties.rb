module Properties

	#<<<<<<<<<<<< First Check the Time of schedular in schedule file.
	
	IS_PRODUCTION_SERVER 	  	= true # it is true only at Henry's Server
	IS_SIMULATOR_ENABLE 		= true # it is false only at Production's Server
	# GCM_KEY 					= 'AIzaSyA2BtzJ-st1ACAWolfa2IcgqQR9vOn1oqw'
	# GCM_KEY    				= 'AIzaSyD5WycIWX9f3O7MHeOz6BuEU6LMvKD62M8'
	# GCM_KEY 					= 'AIzaSyBp9g0l01ffiNErhpnNeNzj5rE5NIx8xSg'
	# GCM_KEY    				= 'AIzaSyByp-5pTCVxkFqSBTB3byEeiR8FrH96fYk'
	GCM_KEY    					= 'AIzaSyAvbYc98Q5k_hlBS-fDezgFRxP4pBjUE7Y'
	MAX_TIMEOUT 				= 65 # in seconds
	COMMISSION_TAX_DEDUCT_PER	= 15 
	APP_VERSION 				= "5.0"
	MAX_SESSION_TIME_OUT 		= 1800 # give in seconds for the WEB

	KIMALI_IP_ADDRESS 			= "192.168.1.149"

	# >>>>>>>>>>>>>>>>>> Henri Production Server 
		
		# ******** RWANDA ********* (13.93.122.87) 
	# COUNTRY_NAME 					= "Rwanda" 
	# COUNTRYCODE 					= '250'
	# LOGIN_EMAIL_ENABLE 				= true
	# SUPERADMIN_EMAIL  				= "aredafrica@gmail.com" #for send the email at the time of admin registration
	# LOAN_UNPAID_EMAIL 				= "gs01han@gmail.com"# This email id is used to get the email from Superadmin about the loan unpaid
	# SENT_EMAIL_MSG_LOGIN_LINK 		= "https://www.mobileshiriki.com/"	 # This link is sent in the email
	# LOGS_FILE_LOCATION 				= "/home/ared/ard/log/mshiriki.log"
	# APP_LOGS_FILE_LOCATION      	= "/home/ared/ard/log/mshirikiApp.log"
	# SYMBOX_LOGS_FILE_LOCATION 		= '/home/ared/ard/log/symbox.log'
	# APP_IMAGE_PATH 					= 'https://www.mobileshiriki.com/' 
	# EMAIL_APP_IMAGE_PATH 			= 'https://www.mobileshiriki.com/'
 	# AIRTEL_RECHARGE_URL 			= "https://197.157.129.12:8054/pretups/C2SReceiver?REQUEST_GATEWAY_CODE=ARED&REQUEST_GATEWAY_TYPE=EXTGW&LOGIN=ARED_TPH&PASSWORD=908cff9930023413c4eff8e45acaa7e8&SOURCE_TYPE=EXTGW&SERVICE_PORT=190"
 	# FDI_SERVER_CHECK_URL 			= "https://converged-api-01.fdibiz.com/prod/status/v1/"
 	# FDI_TXNS_URL 					= "https://converged-api-01.fdibiz.com/prod/trx/v1/"
	# MTNRECHARGEURL 					= 'http://localhost:8080/mshiriki/MTNRechargeController' # from my system localhost tomcat
	# IREMBO_BILL_VALIDATE_URL 		= 'http://156.38.10.43:2160/iremboValidateBillNumber'
	# IREMBO_BILL_PAYMENT_URL 		= 'http://156.38.10.43:2160/iremboBillPayment'
	# TIME_ZONE 						= 'Africa/Kigali'
	# KENYA_APP_ACTION_PATH 			= "https://www.mobileshiriki.com/sessions"
	# RWANDA_APP_ACTION_PATH			= "https://www.mobileshiriki.com/sessions"
	# UGANDA_APP_ACTION_PATH 			= "https://ug.mobileshiriki.com/sessions"
	# KENYA_APP_FORGOT_ACTION_PATH 	= "https://www.mobileshiriki.com/ValidateForNewPassword"
	# RWANDA_APP_FORGOT_ACTION_PATH	= "https://www.mobileshiriki.com/ValidateForNewPassword"
	# UGANDA_APP_FORGOT_ACTION_PATH 	= "https://ug.mobileshiriki.com/ValidateForNewPassword"
 	# DATABASE_USERNAME 				= 'test'
 	# DATABASE_PASSWORD 				= 'tester'
 	# DATABASE_NAME 					= 'ared-admin_production_final'
	# DEVELOPMENT_DATABASE_NAME 		= 'ared_admin_development'
	# UGANDA_WEB_HOST_NAME 			= "ug.mobileshiriki.com"
	# RWANDA_WEB_HOST_NAME			= "mobileshiriki.com"
	# SYMBOX_CLIENT_ID 				=  "ared_sp_test"
	# SYMBOX_API_KEY 				=  "edakdamNfnLrY9ACB9ZeHX4H"
	# SYMBOX_HMAC_GENERATE_URL 		=  "http://localhost:8080/Symbox/"
	# SYMBOX_ADD_HOURS_VALUE 			=  1

 		# ******** UGANDA *********

	# COUNTRY_NAME 					= "Uganda"	
	# COUNTRYCODE 					= '256'
	# LOGIN_EMAIL_ENABLE 				= true
	# SUPERADMIN_EMAIL  				= "aredafrica@gmail.com" 
	# LOAN_UNPAID_EMAIL 				= "gs01han@gmail.com"
	# SENT_EMAIL_MSG_LOGIN_LINK 		= "https://ug.mobileshiriki.com/sessions"
	# LOGS_FILE_LOCATION 				= "/home/ared/ard-uganda/ard/log/mshiriki.log"
	# APP_LOGS_FILE_LOCATION      	= "/home/ared/ard-uganda/ard/log/mshirikiApp.log"
	# SYMBOX_LOGS_FILE_LOCATION 		= '/home/ared/ard-uganda/ard/log/symbox.log'
	# APP_IMAGE_PATH 					= 'https://ug.mobileshiriki.com/'
	# EMAIL_APP_IMAGE_PATH 			= 'https://ug.mobileshiriki.com/'
	# TIME_ZONE 						= 'Africa/Kampala'
	# KENYA_APP_ACTION_PATH 			= "https://www.mobileshiriki.com/sessions"
	# RWANDA_APP_ACTION_PATH			= "https://www.mobileshiriki.com/sessions"
	# UGANDA_APP_ACTION_PATH 			= "https://ug.mobileshiriki.com/sessions"
	# KENYA_APP_FORGOT_ACTION_PATH 	= "https://www.mobileshiriki.com/ValidateForNewPassword"
	# RWANDA_APP_FORGOT_ACTION_PATH	= "https://www.mobileshiriki.com/ValidateForNewPassword"
	# UGANDA_APP_FORGOT_ACTION_PATH 	= "https://ug.mobileshiriki.com/ValidateForNewPassword"
	# DATABASE_USERNAME 				= 'test'
	# DATABASE_PASSWORD 				= 'tester'
	# DATABASE_NAME 					= 'ared_uganda_admin_production'
	# DEVELOPMENT_DATABASE_NAME 		= 'uganda_ared_admin_development'
	# PAYWAY_MOBILE_RECHARGE_URL  	= "http://localhost:8080/PayWay/AirtimeRechargeController"
	# PAYWAY_NATIONAL_WATER_URL   	= 'http://localhost:8080/PayWay/NationalWaterBillPayController'
	# PAYWAY_ELECTRICITY_RECHARGE_URL = 'http://localhost:8080/PayWay/ElectricityRechargeController'
	# PAYWAY_GET_BALANCE_URL			= 'http://localhost:8080/PayWay/ProfileController'
	# QUICK_TELLER_VALIDATE_URL 		=  "https://services.interswitchug.com/api/v1/quickteller/transactions/inquirys"
	# QUICK_TELLER_TRANSACTION_URL	=  "https://services.interswitchug.com/api/v1A/svapayments/sendAdviceRequest"
	# QUICK_TELLER_BALANCE_URL 		=  "https://services.interswitchug.com/api/v1A/svapayments/terminal/balance/0/#{UgandaConstants::QUICK_TELLER_TERMINAL_ID}"
	# UGANDA_WEB_HOST_NAME 			=  "ug.mobileshiriki.com"
	# RWANDA_WEB_HOST_NAME			=  "mobileshiriki.com"
	# SYMBOX_CLIENT_ID 				=  "ared_sp_test"
	# SYMBOX_API_KEY 				=  "edakdamNfnLrY9ACB9ZeHX4H"
	# SYMBOX_HMAC_GENERATE_URL 		=  "http://localhost:8080/Symbox/"
	# SYMBOX_ADD_HOURS_VALUE 			=  2



  	# >>>>>>>>>>>>>>>>>> Henri Staging Server 

		# ******** RWANDA ********* (13.84.48.214) 
	COUNTRY_NAME 					= "Rwanda"	
	COUNTRYCODE 					= '250'	
 	LOGIN_EMAIL_ENABLE 				= true
	SUPERADMIN_EMAIL  				= "aredafrica@gmail.com" 
	LOAN_UNPAID_EMAIL 				= "gs01han@gmail.com"
	SENT_EMAIL_MSG_LOGIN_LINK 		= "https://13.84.48.214/"
	LOGS_FILE_LOCATION 				= "/home/henri01/ard/log/mshiriki.log"
	APP_LOGS_FILE_LOCATION      	= "/home/henri01/ard/log/mshirikiApp.log"
	SYMBOX_LOGS_FILE_LOCATION 		= '/home/henri01/ard/log/symbox.log'
	APP_IMAGE_PATH 					= 'https://13.84.48.214/'
	EMAIL_APP_IMAGE_PATH 			= 'https://13.84.48.214/'
	TIME_ZONE 						= 'Africa/Kigali'
	KENYA_APP_ACTION_PATH 			= "/sessions"
	RWANDA_APP_ACTION_PATH			= "https://13.84.48.214/sessions"
	UGANDA_APP_ACTION_PATH 			= "http://104.210.155.45/sessions"
	KENYA_APP_FORGOT_ACTION_PATH 	= "/ValidateForNewPassword"
	RWANDA_APP_FORGOT_ACTION_PATH	= "https://13.84.48.214/ValidateForNewPassword"
	UGANDA_APP_FORGOT_ACTION_PATH 	= "http://104.210.155.45/ValidateForNewPassword"
	DATABASE_USERNAME 				= 'M_SHIRIKI'
	DATABASE_PASSWORD 				= 'ared@123'
	DATABASE_NAME 					= 'ared_admin_staging'
	DEVELOPMENT_DATABASE_NAME 		= 'ared_admin_development'
	FDI_TXNS_URL 					= "http://localhost:8081/MyDemoService/AllServlet"
 	AIRTEL_RECHARGE_URL 			= "http://localhost:8081/MyDemoService/AirtelServlet"
 	MTNRECHARGEURL 					= 'http://localhost:8081/MyDemoService/MtnServlet' # from my system localhost tomcat
 	IREMBO_BILL_VALIDATE_URL 		= 'http://156.38.10.43:2160/iremboValidateBillNumber'
 	IREMBO_BILL_PAYMENT_URL 		= 'http://156.38.10.43:2160/iremboBillPayment'
 	UGANDA_WEB_HOST_NAME 			= "104.210.155.45"
	RWANDA_WEB_HOST_NAME			= "13.84.48.214"
	SYMBOX_CLIENT_ID 				= "ared_sp_test"
 	SYMBOX_API_KEY 					= "edakdamNfnLrY9ACB9ZeHX4H"
 	SYMBOX_HMAC_GENERATE_URL 		= "http://localhost:8081/Symbox/"
 	SYMBOX_ADD_HOURS_VALUE 			=  1
	

		# ******** UGANDA ********* (104.210.155.45)
		
	# COUNTRY_NAME 					= "Uganda"	
	# COUNTRYCODE 					= '256'
 	# LOGIN_EMAIL_ENABLE 				= true
	# SUPERADMIN_EMAIL  				= "aredafrica@gmail.com" 
	# LOAN_UNPAID_EMAIL 				= "gs01han@gmail.com"
	# SENT_EMAIL_MSG_LOGIN_LINK 		= "http://104.210.155.45/"
	# LOGS_FILE_LOCATION 				= "/home/ugtest/ard/log/mshiriki.log"
	# APP_LOGS_FILE_LOCATION      	= "/home/ugtest/ard/log/mshirikiApp.log"
	# SYMBOX_LOGS_FILE_LOCATION 		= '/home/ugtest/ard/log/symbox.log'	
	# APP_IMAGE_PATH 					= 'http://104.210.155.45/'
	# EMAIL_APP_IMAGE_PATH 			= 'http://104.210.155.45/'
	# TIME_ZONE 						= 'Africa/Kampala'
	# KENYA_APP_ACTION_PATH 			= "/sessions"
	# RWANDA_APP_ACTION_PATH			= "https://13.84.48.214/sessions"
	# UGANDA_APP_ACTION_PATH 			= "http://104.210.155.45/sessions"
	# KENYA_APP_FORGOT_ACTION_PATH 	= "/ValidateForNewPassword"
	# RWANDA_APP_FORGOT_ACTION_PATH	= "https://13.84.48.214/ValidateForNewPassword"
	# UGANDA_APP_FORGOT_ACTION_PATH 	= "http://104.210.155.45/ValidateForNewPassword"
	# DATABASE_USERNAME 				= 'M_SHIRIKI'
	# DATABASE_PASSWORD 				= 'ared@123'
	# DATABASE_NAME 					= 'ared_uganda_admin_staging'
	# DEVELOPMENT_DATABASE_NAME 		= 'uganda_ared_admin_development'
	# PAYWAY_MOBILE_RECHARGE_URL  	=  "http://localhost:8080/PayWay/AirtimeRechargeController"
	# PAYWAY_NATIONAL_WATER_URL   	= 'http://localhost:8080/PayWay/NationalWaterBillPayController'
	# PAYWAY_ELECTRICITY_RECHARGE_URL = 'http://localhost:8080/PayWay/ElectricityRechargeController'
	# PAYWAY_GET_BALANCE_URL			= 'http://localhost:8080/PayWay/ProfileController'
	# QUICK_TELLER_VALIDATE_URL 		=  "https://services.interswitchug.com/api/v1/quickteller/transactions/inquirys"
	# QUICK_TELLER_TRANSACTION_URL	=  "https://services.interswitchug.com/api/v1A/svapayments/sendAdviceRequest"
	# QUICK_TELLER_BALANCE_URL 		=  "https://services.interswitchug.com/api/v1A/svapayments/terminal/balance/0/#{UgandaConstants::QUICK_TELLER_TERMINAL_ID}"
	# UGANDA_WEB_HOST_NAME 			= "104.210.155.45"
	# RWANDA_WEB_HOST_NAME			= "13.84.48.214"
	# SYMBOX_CLIENT_ID 				= "ared_sp_test"
 	# SYMBOX_API_KEY 					= "edakdamNfnLrY9ACB9ZeHX4H"
 	# SYMBOX_HMAC_GENERATE_URL 		=  "http://localhost:8080/Symbox/"
 	# SYMBOX_ADD_HOURS_VALUE 			=  2
	





	# >>>>>>>>>>>>>>>>>> local system

		# ******** RWANDA *********
	# COUNTRY_NAME 				= "Rwanda"	
	# COUNTRYCODE 				= '250'
	# DATABASE_USERNAME 			= 'M_SHIRIKI'
	# DATABASE_PASSWORD 			= 'ared@123'
	# DATABASE_NAME 				= 'ared_admin_staging'
	# DEVELOPMENT_DATABASE_NAME 	= 'ared_admin_development'
	# LOGIN_EMAIL_ENABLE 			= false
	# SUPERADMIN_EMAIL  			= "aredafrica@gmail.com" 
	# LOAN_UNPAID_EMAIL 			= "aredafrica@gmail.com"
 # 	SENT_EMAIL_MSG_LOGIN_LINK 	= "http://192.168.0.109:3000/"	 #for local
	# LOGS_FILE_LOCATION 			= "/Users/apple/Dropbox/Ared/logs/mshiriki.log"   # at local system
	# APP_LOGS_FILE_LOCATION      = "/Users/apple/Dropbox/Ared/logs/mshirikiApp.log"
	# SYMBOX_LOGS_FILE_LOCATION 		= '/Users/apple/Dropbox/Ared/logs/symbox.log'	
	# APP_IMAGE_PATH 				= 'http://192.168.1.3:3000'
	# EMAIL_APP_IMAGE_PATH 		= 'http://13.84.48.214/'
	# TIME_ZONE 					= 'Asia/Kolkata'
	# KENYA_APP_ACTION_PATH 		= "/sessions"
	# RWANDA_APP_ACTION_PATH		= "/sessions"
	# UGANDA_APP_ACTION_PATH 		= "/sessions"
	# KENYA_APP_FORGOT_ACTION_PATH 	= "/ValidateForNewPassword"
	# RWANDA_APP_FORGOT_ACTION_PATH	= "/ValidateForNewPassword"
	# UGANDA_APP_FORGOT_ACTION_PATH 	= "/ValidateForNewPassword"

	# # FDI_SERVER_CHECK_URL 			= "https://converged-api-01.fdibiz.com/prod/status/v1/"
 # 	# FDI_TXNS_URL 					= "https://converged-api-01.fdibiz.com/prod/trx/v1/"
 # 	# FDI_SERVER_CHECK_URL 			= "https://176.58.103.80/prod/status/v1/"
 # 	# FDI_TXNS_URL 					= "https://176.58.103.80/prod/trx/v1/"
	# # AIRTEL_RECHARGE_URL 			= "https://197.157.129.12:8054/pretups/C2SReceiver?REQUEST_GATEWAY_CODE=ARED&REQUEST_GATEWAY_TYPE=EXTGW&LOGIN=ARED_TPH&PASSWORD=908cff9930023413c4eff8e45acaa7e8&SOURCE_TYPE=EXTGW&SERVICE_PORT=190"
 # 	# MTNRECHARGEURL 				    = 'http://localhost:8080/mshiriki/MTNRechargeController' # from my system localhost tomcat
 	
	# FDI_TXNS_URL 					= "http://localhost:8080/MyDemoService/AllServlet"
 # 	AIRTEL_RECHARGE_URL 			= "http://localhost:8080/MyDemoService/AirtelServlet"
 # 	MTNRECHARGEURL 					= 'http://localhost:8080/MyDemoService/MtnServlet' # from my system localhost tomcat
 # 	IREMBO_BILL_VALIDATE_URL 		= 'http://156.38.10.43:2160/iremboValidateBillNumber'
 # 	IREMBO_BILL_PAYMENT_URL 		= 'http://156.38.10.43:2160/iremboBillPayment'
 # 	RWANDA_WEB_HOST_NAME			= "192.168.1.3"
 # 	UGANDA_WEB_HOST_NAME 			= "104.210.155.45"

 # 	SYMBOX_CLIENT_ID 				= "ared_sp_test"
 # 	SYMBOX_API_KEY 					= "edakdamNfnLrY9ACB9ZeHX4H"
 # 	SYMBOX_HMAC_GENERATE_URL 		=  "http://localhost:8080/Symbox/"
 # 	SYMBOX_ADD_HOURS_VALUE 			=  1
	


 		# ******** UGANDA *********
 	
 # 	COUNTRY_NAME 					= 'Uganda'
 # 	COUNTRYCODE 					= '256'
 # 	DATABASE_USERNAME 				= 'M_SHIRIKI'
	# DATABASE_PASSWORD 				= 'ared@123'
	# DATABASE_NAME 					= 'ared_uganda_admin_staging'
 # 	DEVELOPMENT_DATABASE_NAME 		= 'uganda_ared_admin_development'
	# LOGIN_EMAIL_ENABLE 				= false
	# SUPERADMIN_EMAIL  				= 'aredafrica@gmail.com'
	# LOAN_UNPAID_EMAIL 				= 'xpwallet27@gmail.com'
 # 	SENT_EMAIL_MSG_LOGIN_LINK 		= 'http://192.168.1.3:3000/'	 #for local
	
	# LOGS_FILE_LOCATION 				= '/Users/apple/Dropbox/Ared/logs/mshiriki.log'   # at local system
	# APP_LOGS_FILE_LOCATION      	= '/Users/apple/Dropbox/Ared/logs/mshirikiApp.log'
	# SYMBOX_LOGS_FILE_LOCATION 		= '/Users/apple/Dropbox/Ared/logs/symbox.log'	

	# APP_IMAGE_PATH 					= 'http://192.168.1.3:3000/'
	# EMAIL_APP_IMAGE_PATH 			= 'http://13.84.48.214/'
	# TIME_ZONE 						= 'Asia/Kolkata'
	
	# KENYA_APP_ACTION_PATH 			= "/sessions"
	# RWANDA_APP_ACTION_PATH			= "/sessions"
	# UGANDA_APP_ACTION_PATH 			= "/sessions"
	
	# KENYA_APP_FORGOT_ACTION_PATH 	= "/ValidateForNewPassword"
	# RWANDA_APP_FORGOT_ACTION_PATH	= "/ValidateForNewPassword"
	# UGANDA_APP_FORGOT_ACTION_PATH 	= "/ValidateForNewPassword"
	
	# UGANDA_WEB_HOST_NAME 			= "104.210.155.45"
	# RWANDA_WEB_HOST_NAME			= "13.84.48.214"
 # 	PAYWAY_MOBILE_RECHARGE_URL  	= 'http://localhost:8080/PayWay/AirtimeRechargeController'
 # 	PAYWAY_NATIONAL_WATER_URL   	= 'http://localhost:8080/PayWay/NationalWaterBillPayController'
 # 	PAYWAY_ELECTRICITY_RECHARGE_URL = 'http://localhost:8080/PayWay/ElectricityRechargeController'
 # 	PAYWAY_GET_BALANCE_URL			= 'http://localhost:8080/PayWay/ProfileController'

 # 	QUICK_TELLER_VALIDATE_URL 		=  "https://services.interswitchug.com/api/v1/quickteller/transactions/inquirys"
 # 	QUICK_TELLER_TRANSACTION_URL	=  "https://services.interswitchug.com/api/v1A/svapayments/sendAdviceRequest"
 # 	QUICK_TELLER_BALANCE_URL 		=  "https://services.interswitchug.com/api/v1A/svapayments/terminal/balance/0/#{UgandaConstants::QUICK_TELLER_TERMINAL_ID}"

 # 	SYMBOX_CLIENT_ID 				=  "ared_sp_test"
 # 	SYMBOX_API_KEY 					=  "edakdamNfnLrY9ACB9ZeHX4H"
 	# SYMBOX_HMAC_GENERATE_URL 		=  "http://localhost:8080/Symbox/"
 	# SYMBOX_ADD_HOURS_VALUE 			=  2

end