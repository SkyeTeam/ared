module SqlQueryHelper
	
	FETCH_MAX_VERSION_NO_QUERY = "select max(id) from txn_items"

	FETCH_AGENT_COMM_APP_QUERY = "SELECT service_type, sum(agent_commission) AS commission FROM txn_headers 
								WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} 
								AND txn_date >= 'fromDate' AND txn_date <= 'toDate' 
								AND  agent_id::INT = agentID
								AND service_type IN('#{Constants::MTOPUP_LABEL}', '#{Constants::FDI_ELECTRICITY_LABEL}', '#{Constants::FDI_DTH_LABEL}')
								GROUP BY service_type"


end