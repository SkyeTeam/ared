module Backend::UserHelper

  def get_kiosk user
    safe_join(
      user.kiosk_ids.collect { |id|
        link_to (Kiosk.find_by id: id).description, edit_kiosk_path(id) }, ", ".html_safe
    )
  end

end
