module Backend::KioskHelper

NO_DATA = "waiting for data.."

  def json_hash(json, key)
    if json.is_a?(String)
      json = JSON.parse json
      if json.is_a?(Hash)
        return json.fetch(key.to_s)
      end
    end

    NO_DATA
  end

  def get_release kiosk
    distribution = json_hash(kiosk.os_info, "release")["distribution"]
    version = json_hash(kiosk.os_info, "release")["version"]
    revision = json_hash(kiosk.os_info, "release")["revision"]

    distribution && version && revision ? distribution + " v" + version + " " + revision : NO_DATA
  end

  def get_hardware_name kiosk
    hardware_name = json_hash(kiosk.os_info, "system")
    hardware_name ? hardware_name : NO_DATA
  end

  def get_free_disk_space kiosk
    kiosk_size = json_hash(kiosk.device_info_content, "blockdevices")[0]["size"].to_i
    free_space = (kiosk_size - kiosk.device_size_content.to_i) / 1024 / 1024

    free_space < 0 ? NO_DATA : "#{free_space}MB"
  end

  def get_agents kiosk
    safe_join(
      kiosk.user_ids.collect { |id|
        link_to (User.find_by id: id).name, edit_user_path(id) }, ", ".html_safe
    )
  end

end
