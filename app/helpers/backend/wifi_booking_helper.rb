module Backend::WifiBookingHelper

def get_total_usage wifi_bookings
  count = 0
  wifi_bookings.each do |wifi_booking|
    if not wifi_booking.agent?
      count += wifi_booking.available_duration_in_seconds || 0
    end
  end

  return WifiBooking.to_time(count)
end

end
