module Backend::CategoryHelper

  def exists_translation?(translations, language)
    if translations.empty?
      false
    else
      translations.each do |translation|
         if translation.has_key?(language)
           return true
         end
      end
    end
    
    return false
  end

end
