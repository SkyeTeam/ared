module Backend::AdHelper

  def get_all_clicks ads
    count = 0
    ads.each do |ad|
      count += ad.try(:clicks) || 0
    end

    return count
  end

  def get_area id
    Area.find_by(id: id).title
  end

end
