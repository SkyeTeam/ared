module Backend::FirmwareHelper

  def get_areas firmware
    safe_join(
      firmware.areas.collect { |area|
        area.title }, ", ".html_safe
    )
  end

end
