module KioskHelper

  def multi_marker(kiosks)
    markers = []
    markers = kiosks.map do |kiosk|
      if kiosk.lat.present? && kiosk.lon.present?
        { lat: kiosk.lat, lng: kiosk.lon, title: kiosk.description }
      end
    end
    return markers.compact.to_json
  end

  def single_marker(kiosk)
    if kiosk.lat.present? && kiosk.lon.present?
        marker = { lat: kiosk.lat, lng: kiosk.lon, title: kiosk.description }
    end
    return marker.to_json
  end
end
