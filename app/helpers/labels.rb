module Labels

	
	FORGOT_PASSWORD_DB_DES_LABEL 		= "Forgot Password"	


	ADD_NEW_KIOSK_LABLE 				= "Add Kiosk"
	ASSIGN_NEW_KIOSK_LABLE				= "Assign Kiosk"
	DE_ASSIGN_KIOSK_LABLE 				= "De-Assign Kiosk"
	ASSIGNEMENT_HISTORY_LABEL 			= "Assignment History"
	FORGOT_PASSWORD_LABEL 				= "Forgot Password?"
	BACK_TO_LOGIN_LABEL 				= "Back to Login"
	NEW_PASSWORD_LABEL 					= "New Password"
	CONFIRM_PASSWORD_LABEL 				= "Confirm Password"
	ADD_NEW_PAYMENT_MODE_LABEL 			= "Add New Payment Mode"

	KIOSK_FUNCTIONAL_STATUS_LABEL 		= "Functional"
	KIOSK_NON_FUNCTIONAL_STATUS_LABEL 	= "Non-Functional"
	KIOSK_TERMINATE_STATUS_LABEL 		= "Terminate"

	SEARCH_BUTTON_LABEL 				= "Search"
	SAVE_BUTTON_LABEL 					= "Save"
	BACK_BUTTON_LABEL 					= "Back"	
	REGENERATE_BUTTON_LABEL 			= "Re-Generate"
	ASSIGN_BUTTON_LABEL					= "Assign"
	DEASSIGN_BUTTON_LABEL 				= "De-Assign"
	SUBMIT_BUTTON_LABEL 				= "Submit"
end	