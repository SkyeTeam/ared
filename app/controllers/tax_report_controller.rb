# This class used for the MF Tax Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class TaxReportController < ApplicationController

	before_action :require_login
	layout 'header'

	def searchTaxDeductDetails
		begin
			Log.logger.info("Tax Report Controller's searchCommissionPaymentDetails method is called at line = #{__LINE__}")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 
			# after getting the month and year from the web page give to the manager class
			response = TaxReportManager.serachTaxDetails(params)

			if !ResponseCheck.isSuccessful(response)
					responseMsg = response.Response_Msg
		          	DashboardManager.setDisplayMessage(responseMsg+"|false") 
		          	render '/txn_history/index'
	            return
	      	else
		      	  @profitLoseList = response.commPaidMonthlyList
		      	  render '/tax_report/show'
	          	return
	      	end  	
	    rescue => exception
	     		Log.logger.info("Inside the Tax Report Controller's at line #{__LINE__} and method searchTaxDeductDetails Exception is = \n #{exception.message}")
	        	Log.logger.info("Inside the Tax Report Controller's at line #{__LINE__} and method searchTaxDeductDetails Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/txn_history'
	    end     	
	end	
end