# This class used for the Users Signup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 33 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:TooManyInstanceVariables: { max_instance_variables: 8 }
# :reek:TooManyMethods: { max_methods: 18 }

class UsersController < ApplicationController
  before_action :require_login
  before_action :set_user, only: [:show, :edit, :update, :destroy,:suspend,:delete_usr]
  layout 'header'

  #This meathod show list of all the user
  def index
    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
    @activeUsers = User.where(User.getUserListQuery).where("status = #{Constants::USER_ACTIVE_STATUS}").all.order("updated_at desc")
    @suspendUsers = User.where(User.getUserListQuery).where("status = #{Constants::USER_SUSPEND_STATUS}").all.order("updated_at desc")
    @terminateUsers = User.where(User.getUserListQuery).where("status = #{Constants::USER_DELETE_STATUS}").all.order("updated_at desc")
  end

  #This method search the user by phone for Cash Deposit
  def searchByPhoneOrName
    begin
      details = params[:details] # Get the phone number entered on the web page to search the agent for the cash deposit
      dateRange = params[:dateRange]
      Log.logger.info("The User Controller's method search By Phone at line #{__LINE__} is called with phone =#{details} and dateRange = #{dateRange}")

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      response = User.search(details, dateRange) # after getting the phone number from the webpage pass it to the manager class
      # If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull
      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg
          DashboardManager.setDisplayMessage(responseMsg+"|false")
        redirect_to users_path
      else
        # if the response code is 200 then after getting the detils of the user render to the search page
          @user = response.User_Object
          @isSearchNameViewDisplay = response.object # This may come true or false
        render :controller => 'user', :action => 'show'
      end
    rescue => exception
          Log.logger.info("The User Controller's at line #{__LINE__} inside the searchByPhone method  and exception message is = #{exception.message}")
          Log.logger.info("The User Controller's at line #{__LINE__} inside the searchByPhone method  and full exception is = #{exception.backtrace.join("\n")}")
          DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false")
        redirect_to users_path
    end
  end

  def suspend
  end

  def delete_usr
  end

  def activatePhone
    primaryKey = params[:primaryKey]
    userID = params[:userID]

    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

    UserManager.activateMFPhone(userID, primaryKey, loggedInAdmin, params)
  end

  def deActivatePhone
    userID = params[:userID]
    primaryKey = params[:primaryKey]

    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

    UserManager.deActivateMFPhone(userID, primaryKey, loggedInAdmin, params)
  end

  #This method  show the particular user details
  def show
  end

  def search
  end

  #This method show form detail for create the new user
  def new
    @user = User.new
  end

  #This method edit the user details
  def edit
    @serialNumber = SerialNumber.where(user_id: @user.id).last
  end

  #This method Create the new user
  def create
    begin
      Log.logger.info("Inside the User Controller and method create is called at line = #{__LINE__}")

      @user = User.new(user_params) # @user make the global as it use in the form page of the registation

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      # This is used to check that the role is admin or the micro franchisee. if the role is admin then get the roles otherwise the role is nil
      if @user.role == 'admin'
        admin_roles = params[:puppy][:commands]
      else
        admin_roles = nil
      end

      response = UserManager.createUser(@user, admin_roles, loggedInAdmin, params)
      responseMsg = response.Response_Msg

      if !ResponseCheck.isSuccessful(response)
          urlBarStatus = params[:urlBarStatus]#This is used if the admin has MF signup role only
          if urlBarStatus == "agent"
            session[:urlBarStatus] = urlBarStatus
          end
          DashboardManager.setDisplayMessage(responseMsg+"|false")
          render new_user_path
        return
      else
          DashboardManager.setDisplayMessage(responseMsg+"|true")
          redirect_to users_path
        return
      end

    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false")
      redirect_to new_user_path
    end

  end

  #This method Update the details of user
  def update
    begin
      email = params[:user][:email]
      phone = params[:user][:phone]
      tin   = params[:user][:tin_no]
      national_id = params[:user][:national_id]
      user_status = params[:user][:status]
      date_of_birth = params[:user][:date_of_birth]

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      response = UserManager.updateUser(@user, email, phone, @user.id, tin, national_id, user_status, date_of_birth, loggedInAdmin, params)

      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg
          DashboardManager.setDisplayMessage(responseMsg+"|false")
          render new_user_path
        return
      end

      responseMsg =  response.Response_Msg
      params[:user][:email] = @user.email
      params[:user][:email_flag] = @user.email_flag

      respond_to do |format|
        if @user.update(user_params)
          if @user.role.eql?("admin")
            UserManager.updateAdminRoles(@user, params[:puppy][:commands])
          end

          DashboardManager.setDisplayMessage(responseMsg+"|true")

          format.html { redirect_to users_path}
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end

    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false")
      redirect_to new_user_path
    end
  end

  #This method delete the user details
  def destroy
    begin
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      response = UserManager.deleteUser(params[:id], loggedInAdmin, params)

      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg
          respond_to do |format|
            DashboardManager.setDisplayMessage(responseMsg+"|false")
            format.html { redirect_to users_path}
            format.json { head :no_content }
          end
      else
          responseMsg =  response.Response_Msg
          respond_to do |format|
            DashboardManager.setDisplayMessage(responseMsg+"|true")
            format.html { redirect_to users_path}
            format.json { head :no_content }
          end
      end
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false")
      redirect_to new_user_path
    end
  end

  #This method suspend the user account
  def suspend_account
    begin
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      response = UserManager.suspendUser(params[:id], loggedInAdmin, params)

      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg
          respond_to do |format|
            DashboardManager.setDisplayMessage(responseMsg+"|false")
            format.html { redirect_to users_path}
            format.json { head :no_content }
          end
      else
          responseMsg =  response.Response_Msg
          respond_to do |format|
            DashboardManager.setDisplayMessage(responseMsg+"|true")
            format.html { redirect_to users_path}
            format.json { head :no_content }
          end
      end
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false")
      redirect_to new_user_path
    end
  end

  #This method get the all Admin roles at the time of admin registration
  def user_role
          @user_role= Role.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :role, :password, :salt, :remember_me_token, :remember_me_token_expires_at, :name, :last_name, :phone, :date_of_birth, :address, :city, :country, :zip_code, :status, :business_name, :tin_no, :image, :gender, :national_id, :district, :email_flag)
    end
end
