# This class used for the Third Party Config
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class ThirdPartyConfigController < ApplicationController

	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'
	
	def index
		@thirdpartyConfigs = ThirdPartyConfig.all.order("party_name ASC") 
	end	

	def updateConfigs
		begin
			
			Log.logger.info("Inside the Third Party Config Controller's method updateConfigs is called at line = #{__LINE__}")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		
			response = ThirdPartyConfig.updateConfigs(loggedInAdmin, params)

			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true")
		    end

		    redirect_to '/ThirdPartyConfig'

		rescue => exception
	        
	        	Log.logger.info("Inside the Third Party Config Controller's method updateConfigs and  exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/ThirdPartyConfig'

	    end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::THIRD_PARTY_CONFIG_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::THIRD_PARTY_CONFIG_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end

end
