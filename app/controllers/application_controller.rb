# This class used as a Parent Class
class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  add_flash_types :error
  before_action :set_cache_buster
  skip_before_action :verify_authenticity_token
  before_action :check_session_expiry
  before_action :set_locale
  
  #This method is used to disable the back button after logout
  # :reek:DuplicateMethodCall { max_calls: 6 }
  def set_cache_buster
     response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
     response.headers["Pragma"] = "no-cache"
     response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  #This method is used to authenticate the user
  def not_authenticated
    DashboardManager.setDisplayMessage((I18n.t :NOT_AUTH_MSG).to_s + "|false")
    redirect_to new_session_path
  end
 
  #This method is used to expire the session after 10 minutes
  # :reek:DuplicateMethodCall { max_calls: 6 }
  # :reek:NilCheck { enabled: false }
  def check_session_expiry
    if !session[:expires_at].nil? and session[:expires_at] < DateTime.now
      DashboardManager.setDisplayMessage((I18n.t :SESSION_TIMEOUT_MSG).to_s + "|false")
      redirect_to new_session_path
    end
    session[:expires_at] = Properties::MAX_SESSION_TIME_OUT.seconds.from_now
  end  

  # This method is used to change the locale means to change the language of the project
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

end
