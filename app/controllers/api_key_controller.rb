# This class used to generate the Api Key
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class ApiKeyController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
		@apiKeys = ApiKey.getKey(current_user.id)
	end	

	# :reek:TooManyStatements: { max_statements: 16 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
	def generateKey
		begin
			Log.logger.info("At line #{__LINE__} inside the Api Key Controller's and method generateKey is called")
      		
 			response = ApiKey.createAPIKey(params, current_user)
 			responseMsg =  response.Response_Msg 
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false") 
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
		    end

     		redirect_to '/APIKeyGeneration'

	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} and exception is = \n #{exception.message}")
	        	Log.logger.info("Full exception is = \n #{exception.backtrace.join("\n")}")
	      		DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/APIKeyGeneration'
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::GENERATE_API_KEY_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::GENERATE_API_KEY_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end