# This class used for the QR Codes
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class QRcodesController < ApplicationController
	before_action :require_login

  	layout 'header'

	def index
    	@booths = Booth.all
  	end

  	def qr_codes
  		@booths = Booth.all
  	end
end
