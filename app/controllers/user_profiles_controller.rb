# This class used for the Users Profile
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }

class UserProfilesController < ApplicationController

	before_action :require_login
	before_action :checkServiceAccess
	layout 'header'

	#This method is use to see the profile of the user
	def profile
	    begin
	    	agentId = params[:id]

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 
			@response = UserProfile.getAgentProfile(params) #This make global bcoz used in the profile.html.erb
		    if !ResponseCheck.isSuccessful(@response)
		        	DashboardManager.setDisplayMessage( @response.Response_Msg+"|false") 
	          	redirect_to users_path
	          	return
	    	end
		rescue => exception
				Log.logger.info("Exception occur in the UserProfilesController  = #{exception.message}")
	        	Log.logger.info("Exception occur in the  User Profiles Controller's profile method and full exception is = #{exception.backtrace.join("\n")}")
	        DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	        redirect_to users_path
    	end
	end

	private
  	def checkServiceAccess
    	hasAccess = Utility.hasServiceAccess(Constants::MF_PROFILE_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::MF_PROFILE_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end

end