# This class used for the Internet

class InternetController < ApplicationController
  # before_action :require_login                                                      x
  layout 'header' 


  def tariff_settings
  end

  def free_internet_settings
  end

  def free_website
  end

  def bandwidth
  end

  def internet_bandwidth_stats
  end

  def tokens
  end

  def online_offline_stats
  end
end
