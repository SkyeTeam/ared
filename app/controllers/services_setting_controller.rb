# This class used for the Service Settings
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:FeatureEnvy { enabled: false }

class ServicesSettingController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
	layout 'header'
		
	def index
		Log.logger.info("Service Setting Controller's at line #{__LINE__} and index is called")
		@services_available = ServiceSetting.all.order('service ASC')
	end		

	def enable
		begin
		    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
	        Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
	 		ServiceSetting.enableService(params, loggedInAdmin)
	 	rescue => exception
	        Log.logger.info("Inside the Services Setting Controller's method enable to enable the service and exception is = \n #{exception.message}")
	        Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	    end	
	end	

	def disable
		begin
			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
	        Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
	 		ServiceSetting.disableService(params, loggedInAdmin)
	 	rescue => exception
	        Log.logger.info("Inside the Services Setting Controller's method enable to enable the service and exception is = \n #{exception.message}")
	        Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	    end	
	end

	def threshold
		unless params[:isDataPresent].present?
			redirect_to  '/ServiceSettings'
		end	
	end	

	def setThreshold
		begin
			Log.logger.info("Service Setting Controller's at  line #{__LINE__} and setThresholdAmount is called");

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
	        Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			response = ServiceSetting.updateServiceThreshold(params, loggedInAdmin)
			
			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	        render :controller => 'services_setting', :action => 'threshold'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
				redirect_to '/ServiceSettings'
		    end

		rescue => exception
	        	Log.logger.info("Inside the Services Setting Controller's method setThreshold and exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	        DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/ServiceSettings'
	    end				
	end


	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::SERVICE_SETTING_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::SERVICE_SETTING_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end	
end
