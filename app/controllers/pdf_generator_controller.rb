# This class used for the PDF Generate
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class PdfGeneratorController < ApplicationController

	def index
		begin
			Log.logger.info("At line #{__LINE__} and inside the PdfGenerator Controller's index method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
	    	Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			htmlContent = render_to_string "pdf_generator/pdf"
			
			kit = PDFKit.new(htmlContent, page_size: 'A4', margin_top: '0.5in', margin_right: '0.5in', margin_bottom: '0.5in', margin_left: '0.5in')
			send_data kit.to_pdf, 
			filename: 'Invoice.pdf', 
			type: 'application/pdf', 
			disposition: "attachement"
		rescue => exception
				Log.logger.info("Exception occur in the PdfGenerator Controller  = \n #{exception.message}")
	        	Log.logger.info("Exception occur in the  PdfGenerator Controller's and full exception is = \n#{exception.backtrace.join("\n")}")
	        DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	        redirect_to "/TransactionHistory"
    	end
	end	

end