# This class used for the Kiosks
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }
# :reek:RepeatedConditional { max_ifs: 5 }

class KiosksController < ApplicationController

	before_action :require_login
  	before_action :checkServiceAccess
  	layout :whichLayoutUse

  	def index
  		@kiosksList = KioskMaster.getAllKiosks
  	end

  	def new
  		KioskMaster.setPageParams(params)
  	end	

  	def create
  		begin
			Log.logger.info("At line #{__LINE__} inside the KiosksController and create method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			response = KioskMaster.createKiosk(loggedInAdmin, params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")
			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	        render :controller => 'kiosks', :action => 'new'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
				redirect_to '/AssignKiosk'
		    end
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the KiosksController and create method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/AssignKiosk'
	    end
  	end	

  	def assign
  	end

  	def print 
        @kiosk_master_id = params[:id]

	  	respond_to do |format|
	      format.html { render :layout => false }
	      format.pdf do
		      pdf = Prawn::Document.new
		      pdf.text 'Hello World'
		      send_data pdf.render  
	      end
	    end
  	end

  	def assigned
  		begin
			Log.logger.info("At line #{__LINE__} inside the KiosksController and assign method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			response = KioskAgent.assignKiosk(loggedInAdmin, params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")
			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	        render :controller => 'kiosks', :action => 'assign'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
				if params[:userAction].to_i == Constants::KIOSK_ASSIGN_STATUS && params[:isPrintQRCode].present?
					redirect_to "/AssignKiosk#{response.object}"
				else
					redirect_to '/AssignKiosk'
				end	
		    end
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the KiosksController and assign method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/AssignKiosk'
	    end
  	end	
  	
  	def search
  		begin
			Log.logger.info("At line #{__LINE__} inside the KiosksController and search method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			response = KioskMaster.searchKiosk(loggedInAdmin, params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(response.Response_Msg+"|false")
	  	        redirect_to '/AssignKiosk'
			else
				@kiosksList = response.object
				render :controller => 'kiosks', :action => 'index'
		    end
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the KiosksController and search method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/AssignKiosk'
	    end
  	end

  	def regenerate
  		begin
			Log.logger.info("At line #{__LINE__} inside the KiosksController and regenerate method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			response = KioskMaster.regenerateKioskQRCode(loggedInAdmin, params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(response.Response_Msg+"|false")
	  	        redirect_to '/AssignKiosk'
			else
				DashboardManager.setDisplayMessage(response.Response_Msg+"|true")
	  	        redirect_to "/AssignKiosk#{response.object}"
		    end
		    
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the KiosksController and regenerate method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/AssignKiosk'
	    end
  	end

  	def downloadQRCode
  		begin
			Log.logger.info("At line #{__LINE__} inside the KiosksController and downloadQRCode method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			htmlContent = render_to_string "pdf_generator/qr_code"
			kit = PDFKit.new(htmlContent, page_size: 'A4', margin_top: '0.5in', margin_right: '0.5in', margin_bottom: '0.5in', margin_left: '0.5in')
			send_data kit.to_pdf, 
			filename: 'QRCode.pdf', 
			type: 'application/pdf', 
			disposition: "attachement"

		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the KiosksController and downloadQRCode method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/AssignKiosk'
	    end
  	end	

  	private
	def whichLayoutUse
	    case action_name
      	when "index", "new", "create", "assign", "assigned", "search", "regenerate"
        	"header"
      	else
        	"application"
      	end
	end

  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::KIOSKS_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::KIOSKS_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      	return true
	    else
	      		DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      	redirect_to "/Dashboard"
	      	return
	    end
  	end

end