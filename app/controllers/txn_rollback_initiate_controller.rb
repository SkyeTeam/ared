# This class used for the Transaction Rollback Initiate
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:NilCheck { enabled: false }

class TxnRollbackInitiateController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
  	end	

  	def initiateTxnRollback
  		begin
  			Log.logger.info("At line #{__LINE__} inside the TxnRollbackInitiateController and method initiateTxnRollback is called")

      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 	
 			txnID = params[:txnID]
 			Log.logger.info("At Line #{__LINE__} and the transaction id that is rollback = #{txnID}")
			
			response = TxnHeader.getTxnDetails(txnID) 
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseTxnReport = TranscationReport.search(params)
			@txnHistory = responseTxnReport.txn_history

 			if !ResponseCheck.isSuccessful(response)
		          responseMsg =  response.Response_Msg 
		          DashboardManager.setDisplayMessage(responseMsg+"|false") 
		          render 'txn_history/index'
		        return
		    end

		    @txnDetails = response.txnDetails # This varaible make global bcoz used in the view file
     		render 'txn_rollback_initiate/index'
	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} and full exception is = \n#{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/InitiateTxnRollback'
	    end
  	end	

  	def initiatedTxnRollbackConfirm
  		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 	
 			txnID = params[:txnID]  
 			txnAmount = params[:txnAmount]
			agentID = params[:agentID]
 			serviceType = params[:serviceType]
 			description = params[:description]

 			Log.logger.info("At Line #{__LINE__} and the transaction id that is rollback = #{txnID}, serviceType = #{serviceType}, amount of the transaction is = #{txnAmount} and description = #{description}")
			
			response = TxnHeader.initiatedTxnRollback(txnID, txnAmount, description, agentID, serviceType, loggedInAdmin) 
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")
			responseMsg =  response.Response_Msg

			responseTxnReport = TranscationReport.search(params)
			@txnHistory = responseTxnReport.txn_history
 			
 			if !ResponseCheck.isSuccessful(response)
	          	DashboardManager.setDisplayMessage(responseMsg+"|false") 
		    else
		    	DashboardManager.setDisplayMessage(responseMsg+"|true")
		    end
		    redirect_to '/TransactionHistory'
	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} and full exception is = \n#{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/InitiateTxnRollback'
	    end
  	end	

  	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::TXN_ROLLBACK_INITIATE_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::TXN_ROLLBACK_INITIATE_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end