# This class used for the Login on the software
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:RepeatedConditional { max_ifs: 5 }

class SessionsController < ApplicationController

  layout :whichLayoutUse
  before_action :require_login, :only => [:Dashboard, :change_admin_pin]
  skip_before_action :check_session_expiry
  
  def index
    redirect_to new_session_path
  end  

  def new
    session[:expires_at] = nil
  end

  #This method is call when the admin is login from  site
  def create
    begin
     
      response = Session.authenticateUser(params, request) #This method is in the Model class that is  used to check the user has enter the correct information as in our database is exist

      Log.logger.info("Autentication funtion on web is called at line #{__LINE__} and  response Code #{response.Response_Code}") 
      
      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg 
          DashboardManager.setDisplayMessage(responseMsg+"|false") 
          redirect_to root_path
        return
      end

      Session.sendOTPEmail(params, response.user)

      render 'sessions/otp'

    rescue => exception
        Log.logger.info("Inside the Sessions Controller's at line #{__LINE__} method create Exception is = #{exception.message}")
        Log.logger.info("Indise the Sessions Controller's at line #{__LINE__} method create full exception is  #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to root_path
    end

  end

  def validateOTP
    begin
      Log.logger.info("At line #{__LINE__} inside the SessionsController and validateOTP method is called")
      response = Session.validateOTP(params)
      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg 
          DashboardManager.setDisplayMessage(responseMsg+"|false")
          
          if response.Response_Code != Error::INVALID_OTP
              redirect_to root_path
            return
          else 
              render 'sessions/otp'
            return
          end  
      end
       
        login(params[:email], params[:password]) #This is sorcery login check used for the require_login
        redirect_to :controller => 'sessions', :action => 'Dashboard'
      return  
    rescue => exception
        Log.logger.info("At line #{__LINE__} and inside the otp method exception message is = \n #{exception.message}")
        Log.logger.info("At line #{__LINE__} and and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to root_path
    end
  end  

  def destroy
    begin
      logout #This is sorcery logout method
      DashboardManager.setDisplayMessage((I18n.t :WEB_LOGOUT_MSG).to_s + "|true") 
      redirect_to root_path
    rescue => exception
        Log.logger.info("Inside the Sessions Controller's at line #{__LINE__} method destroy Exception is = #{exception.message}")
        Log.logger.info("Inside the Sessions Controller's at line #{__LINE__} method destroy full exception is  #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to root_path
    end
  end

  def show
    render(:template => '/header')
  end

  def change_admin_pin
    begin
      
      loggedUserId = current_user.id
      oldPassword = params[:old_password].first
      newPassword = params[:new_password].first

      Log.logger.info("Inside the Sessions Controller's at line #{__LINE__} method change_admin_pin  and id who want to change his pin = #{loggedUserId}")

      response = Session.changeUserPin(loggedUserId, oldPassword, newPassword)
      responseMsg =  response.Response_Msg

      if !ResponseCheck.isSuccessful(response)
        DashboardManager.setDisplayMessage(responseMsg+"|false") 
        redirect_to change_pin_path 
        return
      end  

      DashboardManager.setDisplayMessage(responseMsg+"|true") 
      redirect_to root_path 

    rescue => exception
        Log.logger.info("Inside the Sessions Controller's at line #{__LINE__} method change_admin_pin Exception is = #{exception.message}")
        Log.logger.info("Inside the Sessions Controller's at line #{__LINE__} method change_admin_pin full exception is  #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to root_path
    end 
  end  


  #contoller for the dashboard view
  def Dashboard
    if session[:expires_at].present? && session[:expires_at] < DateTime.now
      DashboardManager.setDisplayMessage((I18n.t :SESSION_TIMEOUT_MSG).to_s + "|false")
      redirect_to new_session_path
      return
    else
      session[:expires_at] = Properties::MAX_SESSION_TIME_OUT.seconds.from_now  
    end
  end

  private
  #This method to choose the layout according to ypur choice
  def whichLayoutUse
    case action_name
      when "Dashboard"
        "header"
      when "change_pin"
        "header"  
      else
        "login"
      end
  end
end