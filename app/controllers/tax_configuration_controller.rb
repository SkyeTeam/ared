# This class used for the MF Tax Configuration
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class TaxConfigurationController < ApplicationController

	before_action :require_login
	before_action :checkServiceAccess
	layout 'header'

	def index
		@taxConfiguration = TaxConfiguration.all.order('effective_date DESC')
	end	

	def new
	end

	def create
		begin
			Log.logger.info("At line #{__LINE__} inside the TaxConfigurationController and create method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
    		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			response = TaxConfigurationManager.isCreate(params, loggedInAdmin)
			responseMsg = response.Response_Msg

			if !ResponseCheck.isSuccessful(response)
				  	DashboardManager.setDisplayMessage(responseMsg+"|false")
 		          	render :controller => 'tax_configuration', :action => 'new'
	            return
	      	else
 					DashboardManager.setDisplayMessage(responseMsg+"|true")
 		        	redirect_to '/TaxConfiguration'
	          	return
	      	end  	

		rescue => exception
      			Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
       		 	Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      		DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      		render :controller => 'tax_configuration', :action => 'new'
		end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::TAX_CONFIG_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::TAX_CONFIG_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end


end
