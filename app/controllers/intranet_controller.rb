# This class used for the Intranet

class IntranetController < ApplicationController
  # before_action :require_login                                                   $x
  layout 'header'  

  def cms_content_uploader
  end

  def cms_surveys
  end

  def cms_categories
  end

  def cms_client_feedback
  end

  def kms_kiosk_list
  end
end
