# This class used for the Profit Lose Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class ProfitLoseReportController < ApplicationController

	before_action :require_login
	layout 'header'

	def searchProfitLoseDetails
		begin
			Log.logger.info("Profit Lose Report Controller's searchPenaltyDetails method is called at line = #{__LINE__}")

 			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
    
			# after getting the from and to date from the web page give to the manager class
			response = ProfitLoseManager.serachProfitLoseBWdates(params)

			if !ResponseCheck.isSuccessful(response)
		          	responseMsg = response.Response_Msg
		          	DashboardManager.setDisplayMessage(responseMsg+"|false") 
		          	render '/txn_history/index'
	            return
	      	else
		      	  	@profitLoseList = response.profitLoseList	
		      	  	render '/profit_lose_report/show'
	          	return
	      	end  	
	    rescue => exception
	        Log.logger.info("Inside the Profit Lose Report Controller's at line #{__LINE__} and method searchProfitLoseDetails Exception is = \n #{exception.message}")
	        Log.logger.info("Inside the Profit Lose Report Controller's at line #{__LINE__} and method searchProfitLoseDetails Full Exception is = \n #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/txn_history'
	    end  	
	end	
end