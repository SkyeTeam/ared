# This class used for the  Mf Commission Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class MfCommissionReportController < ApplicationController

	before_action :require_login
	before_action :checkServiceAccess
  	layout 'header'

	def index
		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

 		    unless params[:reportType].present?
 		    	params[:reportType] = Constants::MF_COMM_REPORT_LABEL
 		    	params[:serviceTypeCommReport] = Constants::ANY_SERVICE_LABEL
 		    	params[:userTypeCommReport] = Constants::ALL_USER_RADIO_LABEL
 		    	params[:viewTypeCommReport] = Constants::VIEW_GRAPH_LABEL
 		    	params[:monthCommReport] = Date::MONTHNAMES[Date.today.month]
 		    	params[:yearCommReport] = Date.today.year
 		    end	

 			response = MFCommReportManager.getDetails(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")
			
 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(response.Response_Msg + "|false") 
	  	    else
	  	    	@txnHeader = response.detailsHash
			    @totalComm = response.totalValue
	  	    end    
	    rescue => exception
	        Log.logger.info("Inside the Credit Report Controller's at line #{__LINE__} and method index Exception is = #{exception.message}")
	        Log.logger.info("Inside the Credit Report Controller's at line #{__LINE__} and method index Full Exception is = #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	    end
	end	

	def singleUserReport
		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

  			response = MFCommReportManager.fetchSingleUserDetails(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")
			
 			if !ResponseCheck.isSuccessful(response)
	  	        	DashboardManager.setDisplayMessage(response.Response_Msg + "|false") 
	 	        	render "mf_commission_report/index"
	  	        return
	  	    end    

		    @txnHeader = response.detailsHash
	    rescue => exception
	        	Log.logger.info("Inside the Credit Report Controller's at line #{__LINE__} and method singleUserReport Exception is = #{exception.message}")
	        	Log.logger.info("Inside the Credit Report Controller's at line #{__LINE__} and method singleUserReport Full Exception is = #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to "/MFCommissionReport"
	    end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::COMM_REPORT_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::COMM_REPORT_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
