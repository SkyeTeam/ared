# This class used for the Transaction Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class TxnHistoryController < ApplicationController
 
  before_action :require_login
  before_action :checkServiceAccess
  layout 'header'
 
  def index
    begin
      Log.logger.info("At line #{__LINE__} inside the TxnHistoryController and index method is called")

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      response = TranscationReport.search(params)
      if !ResponseCheck.isSuccessful(response)
        responseMsg =  response.Response_Msg 
        DashboardManager.setDisplayMessage(responseMsg+"|false")
        return
      end

      @txnHistory = response.txn_history

    rescue => exception
        Log.logger.info("Exception occur inside the txn_history controller's search_txn_history method is = \n #{exception.message}")
        Log.logger.info("Full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
    end
  end

  private
  def checkServiceAccess
    hasAccess = Utility.hasServiceAccess(Constants::TXN_REPORT_ROLE,current_user.id)
    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::TXN_REPORT_ROLE} service or not = #{hasAccess}")
    if hasAccess
      return true
    else
      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
      redirect_to "/Dashboard"
      return
    end
  end
  
end