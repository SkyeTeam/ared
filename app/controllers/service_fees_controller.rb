# This class used for the Service Fees
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }

class ServiceFeesController < ApplicationController
  
  before_action :require_login
  before_action :checkServiceAccess
  layout 'header'
  before_action :set_service_fee, only: [:show, :edit, :update, :destroy]

  #This method show all the service detail
  def index
    @service_fees = ServiceFee.all.order('effective_date DESC')
  end

  def show
  end

  def new
    @service_fee = ServiceFee.new
  end

  def edit
  end
  
  #This method create the new service fee
  def create
    begin
      Log.logger.info("Inside the ServiceFeesController and method create is called at line = #{__LINE__}")

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 
      @service_fee = ServiceFee.new(service_fee_params)
      
      if params[:service_fee][:service] != Constants::MTOPUP_LABEL
        if !params[:service_fee][:company].present?
          @service_fee[:company]= params[:service_fee][:service]
        end
      end  
    
      respond_to do |format|
        if @service_fee.save
 
          DashboardManager.setDisplayMessage("Service fee was successfully created.|true")
          ActivityLog.saveActivity((I18n.t :SERVICE_FEE_CREATE_ACTIVITY_MSG, :subType => @service_fee[:company]), loggedInAdmin.id, nil, params)
 
          format.html { redirect_to @service_fee}
          format.json { render :show, status: :created, location: @service_fee }
        else
          format.html { render :new }
          format.json { render json: @service_fee.errors, status: :unprocessable_entity }
        end
      end
      
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_service_fee_path
    end 

  end
 
  #This method update the existing service fees
  def update
    begin
      respond_to do |format|
        Log.logger.info("Inside the ServiceFeesController and method update is called at line = #{__LINE__}")

        loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
        Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
        
        if @service_fee.update(service_fee_params)
          
          DashboardManager.setDisplayMessage("Service fee was successfully updated.|true")
         
          ActivityLog.saveActivity((I18n.t :SERVICE_FEE_UPDATE_ACTIVITY_MSG, :subType => params[:service_fee][:company]), loggedInAdmin.id, nil, params)
          
          format.html { redirect_to @service_fee}
          format.json { render :show, status: :ok, location: @service_fee }
        else
          format.html { render :edit }
          format.json { render json: @service_fee.errors, status: :unprocessable_entity }
        end
      end
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_service_fee_path
    end 
  end

  #This method delete the existing service fees
  def destroy
    begin
      Log.logger.info("service_fee Controller method destroy called")

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
  
      ActivityLog.saveActivity((I18n.t :SERVICE_FEE_DELETE_ACTIVITY_MSG, :subType => @service_fee.company), loggedInAdmin.id, nil, params)
      
      @service_fee.destroy

      respond_to do |format|
        DashboardManager.setDisplayMessage("Service fee was successfully deleted.|true")
        format.html { redirect_to service_fees_url }
        format.json { head :no_content }
      end
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_service_fee_path
    end 
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_service_fee
    @service_fee = ServiceFee.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def service_fee_params
    params.require(:service_fee).permit(:service, :company, :total_comm_per, :total_comm_fix, :our_comm_per, :our_comm_fix, :agent_comm_per, :agent_comm_fix, :effective_date, :who_will_pay, :rate)
  end
  
  private
  def checkServiceAccess
    hasAccess = Utility.hasServiceAccess(Constants::SERVICE_FEE_ROLE,current_user.id)
    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::SERVICE_FEE_ROLE} service or not = #{hasAccess}")
    if hasAccess
      return true
    else
      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
      redirect_to "/Dashboard"
      return
    end
  end
end