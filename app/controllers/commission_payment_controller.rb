# This class used for the Commission Payment
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CommissionPaymentController < ApplicationController

	before_action :require_login
	before_action :checkServiceAccess
	layout 'header'

	# :reek:TooManyStatements: { max_statements: 24 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
  	# :reek:NilCheck { enabled: false }
	def search
		begin
			Log.logger.info("Commission Payment Controller's search method is called at line = #{__LINE__}")
			# first we get the values from the webpage user type check whether the commission give to all user or one particular user,
			# if the commission give to the one particular user then we get his phone number to search that user and month , year
			# we put the @ in front of each variable so that we can access it on the view page again

			user_type = params[:user_type]
			phone = params[:agent_phone]
			month = params[:month]
			year  = params[:year]

			Log.logger.info("At line = #{__LINE__} and parameters are user type = #{user_type}, phone = #{phone}, month = #{month}, year = #{year} and give to the manager class ")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
     		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
     
			# after getting the value from the web page pass to the manager class
			response = CommissionManager.searchDetails(user_type, phone, month, year)

			if response == nil
				response = Response.new
				response.Response_Code = Error::UNAUTHORIZED
			end	

			# If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull
			if !ResponseCheck.isSuccessful(response)
		        	responseMsg = response.Response_Msg
		        	# if the response code is not 200 then we store the last entered values by the user on webpage and again show the same values on the view page back  
		        	# to pass value we store into the session and redirect again to the same first page
		        	DashboardManager.setDisplayMessage(responseMsg+"|false")
		       	 	render :controller => 'commission_payment', :action => 'index'
	          	return
	      	else
	      		  # if the response code is 200 then we show the list of those agents whose commission is pending		
		      	  @commission_list = response.commission_list	
		      	  Log.logger.info("At line = #{__LINE__} and render to show.html.erb file")
		      	  render '/commission_payment/show'
	          	return
	      	end

	    rescue => exception
	      	Log.logger.info("Exception occur in Commission Payment Controller's search method is = #{exception.message}")
	      	Log.logger.info("Full Exception is =#{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/CommissionPayment'
	    end  	  
	end	

	# :reek:TooManyStatements: { max_statements: 26 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
  	# :reek:NilCheck { enabled: false }
	def payCommission
		begin
	
			Log.logger.info("Commission Payment Controller's payCommission method is called at line = #{__LINE__}")

			agent_ids = params[:agent_ids] # This is list of all the agents whose commission is pending
			isTaxDeduct = params[:taxDeduct] # This is check whether the tax is dedcut or not 
			fromDate = params[:fromDate] # get the from date
			toDate = params[:toDate] # get the to date
			commission =params[:commission_balance] # get the commissiom balance of the agent
			
			Log.logger.info("At line = #{__LINE__} and list of the agent's id is = #{agent_ids},commission = #{commission} , isTaxDeduct = #{isTaxDeduct}, from = #{fromDate} and to = #{toDate} give to the manager class")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
     		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

			# after getting the values from the web page and give to the manager class
			response = CommissionManager.payCommission(agent_ids, isTaxDeduct, fromDate, toDate, commission, loggedInAdmin, request)

			if response == nil
				response = Response.new
				response.Response_Code = Error::UNAUTHORIZED
			end	

			# If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull
			if !ResponseCheck.isSuccessful(response)
		        responseMsg = response.Response_Msg

		        DashboardManager.setDisplayMessage(responseMsg+"|false")
          		redirect_to '/CommissionPayment'

		        #redirect_to '/CommissionPayment', alert: responseMsg
	          return
	      	else
		    	Log.logger.info("At line = #{__LINE__} and render to index.html.erb file")
	      	    responseMsg = response.Response_Msg
			
			    DashboardManager.setDisplayMessage(responseMsg+"|true")
          		redirect_to '/CommissionPayment'

 	      	    #redirect_to '/CommissionPayment', alert: responseMsg
	           return
	      	end  
		rescue => exception
		      	Log.logger.info("Exception occur in Commission Payment Controller's payCommission method is = #{exception.message}")
		      	Log.logger.info("Full Exception is = #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/CommissionPayment'
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::COMM_PAY_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::COMM_PAY_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end

end
