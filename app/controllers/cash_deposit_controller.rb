# This class used for the Deposit funds in the MF account
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CashDepositController < ApplicationController
  	
  	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
	end
  	
  	# :reek:TooManyStatements: { max_statements: 19 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
  	def searchMF
	    begin
	      	phoneOrName = params[:phoneOrName]
	      	userAccType = params[:userAccType]
	      	Log.logger.info("At line #{__LINE__} inside the CashDepositController and searchMF method is called with phone = #{phoneOrName} and userAccType = #{userAccType}") 

		    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
	     
	      	response = CashDepositManager.searchByPhoneNew(phoneOrName, userAccType)
	      
	      	if !ResponseCheck.isSuccessful(response)
	          		responseMsg =  response.Response_Msg 
	          		DashboardManager.setDisplayMessage(responseMsg+"|false")
	          	render "cash_deposit/index"
	          	return
	      	else
		        	@user = response.User_Object 
	        		@isSearchViewDisplay = false
	        	render "cash_deposit/index"
	        	return
	      	end
	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the CashDepositController and exception message is = \n#{exception.message}")
	        	Log.logger.info("At line #{__LINE__} inside the CashDepositController and full message is = \n#{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/MFCashDeposit'
	    end 
  	end

  	# :reek:TooManyStatements: { max_statements: 16 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
  	def depositFloat
    	begin
      		Log.logger.info("At line #{__LINE__} inside the CashDepositController and depositFloat method is called") 
      
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      		params[:serviceType] = Constants::USER_CASH_DEPOSIT_LABLE
      		params[:subType] = Constants::USER_CASH_DEPOSIT_LABLE
      		response =  CashDepositManager.agentCashDeposit(params, loggedInAdmin, request)
      		responseMsg =  response.Response_Msg 

        	if !ResponseCheck.isSuccessful(response)
            	DashboardManager.setDisplayMessage(responseMsg+"|false")
   			else
            	DashboardManager.setDisplayMessage(responseMsg+"|true")
        	end
	        
	        redirect_to '/MFCashDeposit'
    	rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the CashDepositController and exception message is = \n#{exception.message}")
	        	Log.logger.info("At line #{__LINE__} inside the CashDepositController and full message is = \n#{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/MFCashDeposit'
		end 
  	end

	private
	# :reek:TooManyStatements: { max_statements: 7 }
	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::MF_DEPOSIT_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::MF_DEPOSIT_ROLE} service or not = #{hasAccess}")
	    
	    if hasAccess
	      	return true
	    else
	      	DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      	redirect_to "/Dashboard"
	      	return
	    end
	end

end