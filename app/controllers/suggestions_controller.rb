# This class used for the MF Suggestions
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class SuggestionsController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
		begin
			Log.logger.info("Inside the Suggestions Controller's method searchFeedbacks is called at line = #{__LINE__}")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		
			unless params[:suggestionType].present? #This is for the user when click on the first time from the menu bar
				params[:suggestionType] = Constants::SUGGESTION_TYPES_LIST[0]
				params[:from_date] = Utility.getDefaultDateArray
				params[:to_date] = Utility.getDefaultDateArray
			end	

			response = Suggestion.fetchDetails(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(response.Response_Msg+"|false")
			else
				@suggestionList = response.txn_history
		    end
		rescue => exception
	        	Log.logger.info("Inside the Suggestions Controller's method new and  exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	    end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::MF_SUGGESTIONS_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::MF_SUGGESTIONS_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end