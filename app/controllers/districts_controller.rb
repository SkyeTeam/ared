class DistrictsController < ApplicationController
  layout "header"

  before_action :set_country, only: [:show, :edit, :update, :destroy]
  before_action :set_district, only: [:show, :edit, :update, :destroy]
  
  # GET /districts
  # GET /districts.json
  def index
    @districts = District.all
  end

  # GET /districts/1
  # GET /districts/1.json
  def show
  end

  # GET /districts/new
  def new
    @district = District.new
  end

  # GET /districts/1/edit
  def edit
    @district.towns.build
  end

  # POST /districts
  # POST /districts.json
  def create
    @district = District.new(district_params)

    respond_to do |format|
      if @district.save
        format.html { redirect_to country_district_path(@country,@district), notice: 'District was successfully created.' }
        format.json { render :show, status: :created, location: @district }
      else
        format.html { render :new }
        format.json { render json: @district.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /districts/1
  # PATCH/PUT /districts/1.json
  def update
    respond_to do |format|
      if @district.update(district_params)
        format.html { redirect_to country_district_path(@country,@district), notice: 'District was successfully updated.' }
        format.json { render :show, status: :ok, location: @district }
      else
        format.html { render :edit }
        format.json { render json: @district.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /districts/1
  # DELETE /districts/1.json
  def destroy
    @district.destroy
    respond_to do |format|
      format.html { redirect_to country_path(@country), notice: 'District was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

    def set_country
      @country = Country.find(params[:country_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_district
      @district = @country.districts.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def district_params
      params.require(:district).permit(:name, :country_id,:towns_attributes => [:id,:district_id,:name,:_destory])
    end
end
