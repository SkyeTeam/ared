module Api::V01
	class AgentOfficerController < ApiController

	  def find 
	  	koisk = KioskMaster.find(params[:kiosk_master_id])

	  	koiskAssignedResponse = KioskAgent.getAssignedAgent(koisk.id)
	  	currentAgent = koiskAssignedResponse.user

	  	params.merge(id: currentAgent.id)
	  	@response = UserProfile.getAgentProfile(params) 

#########START OF ANALYTICS############
	  totalTurnoverObj = UserProfile.fetchTotalTurnoverData(@response.totalTurnover)

	  totalAgentCommObj = UserProfile.getGraphData(@response.totalCommission)
	  totalServicesComm = Utility.getFormatedAmount(totalAgentCommObj.totalValue)

	  totalTransactionCountObj = UserProfile.getGraphData(@response.totalTransactionCount)
	  txnCountSum = Utility.getFormatedAmount(totalTransactionCountObj.totalValue)

	  totalTransactionAmtObj = UserProfile.getGraphData(@response.totalAgentTxnsAmount)
	  totalTxnAmountSum = Utility.getFormatedAmount(totalTransactionAmtObj.totalValue)

	  agentAccountAge = @response.date
	  maxLoanCapacity = Utility.getAgentMaxLoanCapacity(currentAgent)
	  agentLoanAmount = Utility.getAgentLoanBalance(currentAgent)    
	  commissionBalance = Utility.getAgentCommissionBalance(currentAgent) 
########## END OF ANYLYSIS###############


	  	if koisk
	  	  render json: { kiosk: koisk,
	  	                  agent: {
						  	"id": currentAgent.id,
					        "email": currentAgent.email,
					        "name": currentAgent.name,
					        "phone": currentAgent.phone,
					        "date_of_birth": currentAgent.date_of_birth,
					        "address": currentAgent.address,
					        "city": currentAgent.city,
					        "country": currentAgent.country,
					        "zip_code": currentAgent.zip_code,
					        "status": currentAgent.status,
					        "business_name": currentAgent.business_name,
					        "tin_no": currentAgent.tin_no,
					        "image": {
					           "url": currentAgent.image
					         },
					        "last_name": currentAgent.last_name,
					        "amount_deposit": currentAgent.try(:amount_deposit) ,
					        "description": currentAgent.description ,
					        "gender": currentAgent.gender ,
					        "district": currentAgent.district ,
					        "national_id": currentAgent.national_id ,
	  	                  },
	  	  	              kiosk_id: "#{koisk.model_no} - #{koisk.serial_no}",
	  	  	              purchase_date: Utility.getFormatedDateNoTime(koisk.purchase_date.to_s),
	  	  	              assigned_to: Utility.getAgentFullName(currentAgent) ,
	  	  	              assigned_date: Utility.getFormatedDateNoTime(koiskAssignedResponse.date.to_s) ,
	  	  	              account_balance: "#{Utility.getCurrencyLabel} #{Utility.getFormatedAmount(Utility.getAgentAccountBalance(currentAgent))}",
	  	  	              client_complaints: 1, 
	  	  	              credit_balance: "#{Utility.getCurrencyLabel} #{Utility.getFormatedAmount(agentLoanAmount)}",
	  	  	              commission_balance: "#{Utility.getCurrencyLabel} #{Utility.getFormatedAmount(commissionBalance)}",
	  	  	              hardware_calls: 1, 
	  	  	              penalty:  "#{Utility.getCurrencyLabel} #{Utility.getFormatedAmount(Utility.getAgentPenaltyBalance(currentAgent))}",
	  	  	              account_age: agentAccountAge ,
	  	  	              total_transaction_count: txnCountSum ,
	  	  	              total_commission_amount: totalServicesComm,
	  	  	              total_transaction_amount: totalTxnAmountSum,
	  	  	              violations: 1
	  	  	           }.except(:crypted_password) ,status: :ok
	  	else
	  	  render json: {error: 'Invalid Request'}, status: :unauthorized
	  	end

		rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end

	  def review 
	  	review = Review.new
	  	#review.agent_id = params[:agent_id]
	  	review.kiosk_master_id = params[:kiosk_master_id]
	  	review.officer_id = params[:officer_id]
	  	review.agent_present = determine_value params[:agent]
	  	review.uniform = determine_value params[:uniform]
	  	review.wifi = determine_value params[:wifi]
	  	review.replaced = determine_value params[:replaced]
	  	review.clean = determine_value params[:clean]
	  	review.issue = determine_value params[:issue]
	  	review.summary_type = params[:summary_type]
	  	review.summary_text = params[:summary_text]

	  	if review.save!
	      render json: { review: review,success: 1 }, status: :ok
	    else
	      render json: review.errors, status: :unprocessable_entity
	    end
	  end

	 
	  def create
	  	officer = Officer.find_by_account(params[:account])
	    agent = officer.agents.new
	    agent.name = params[:name]
	    agent.phone = params[:phone]
        agent.location = params[:location]
        agent.latitude = params[:latitude]
        agent.longitude = params[:longitude]
        agent.photo = params[:photo]

	    if agent.save
	      render json: {agent: agent,success: 1}, status: :ok
	    else
	      render json: agent.errors, status: :unprocessable_entity
	    end
	  end

	private 

	 def determine_value(val)
	 	if val == "1"
	 		true 
	 	else
	 		false 
	 	end
	 end



	end
end


