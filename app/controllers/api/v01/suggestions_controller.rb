# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
	# :reek:FeatureEnvy { enabled: false }
	
  	class SuggestionsController < ApiController

  		before_action :authenticateAgent

  		def suggestionTypes
  			begin 
		        AppLog.logger.info("Inside the Suggestions Controller and suggestionTypes API is called at line #{__LINE__}")
	            render json: {
	            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
	            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => Constants::SUGGESTION_TYPES_LIST
	            }
		    	return
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	         	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    	}
			    return
      		end
  		end	
  		
  		def sendSuggestions
  			begin 
		        AppLog.logger.info("Inside the Suggestions Controller and sendSuggestions API is called at line #{__LINE__}")
		        
		        response =  Suggestion.saveSuggestion(@agent, params, request)
		        
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	         	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    	}
			    return
      		end
  		end

  	end
end  		