# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
  	class RequestCreditsController < ApiController

  		before_action :authenticateAgent

  		def requestCredit
  			begin
		        unless @agent.present?
		        	AppLog.logger.info("Inside the API Request Credits Controller's  requestCredit method at line #{__LINE__} and agent is not present")
		          return
		        end  
		        
		        AppLog.logger.info("Inside the API Request Credits Controller's  requestCredit method at line #{__LINE__}")

		        currentAgent = @agent

		        requestedAmount = CreditManager.getActuallAmount(params[""+AppConstants::AMOUNT_PARAM_LABEL+""])
		        appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]

		        AppLog.logger.info("At line #{__LINE__} and parameter are agent id is = #{currentAgent.id} , agent phone is = #{currentAgent.phone} and requested loan amount is = #{requestedAmount}")
		          
		        response = CreditManager.approveCredit(currentAgent,requestedAmount, appTxnUniqueID, request)  

		        AppLog.logger.info("At line #{__LINE__} of Request Credits Controller and response code from the Credit Manager's class is = #{response.Response_Code}")
		     
		        if !ResponseCheck.isSuccessful(response)
				    	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
				    	}
			    	return
		        else
			            render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
				    	}
			    	return
		        end

		        AppLog.logger.info("API Request Credits Controlleris finished at line #{__LINE__}")

      		rescue => exception 
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	        		render json: {
					    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
					    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
					}
				return
      		end
  		end

	end  
end		