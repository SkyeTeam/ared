# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
  	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }

  	class ElectricityRechargesController < ApiController
  		
  		before_action :authenticateAgent
  		
  		def checkMeterNumber
  			begin 
		        AppLog.logger.info("At line #{__LINE__} inside the ElectricityRechargesController and Check Meter Number API is called")
		        
		        response = ElectricityRechargeManager.checkMeterNumberForRecharge(@agent, params)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	
		            	""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {
		            	
		            		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg, 
		            		""+AppConstants::RESPONSE_CUSTOMER_NAME_KEY_LABEL+"" => response.customerName, 
		            		""+AppConstants::RESPONSE_CUSTOMER_METER_NO_KEY_LABEL+"" => response.customerMeterNo
		            	
		            	}
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	         	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    }
			    return
      		end
  		end	

  		def rechargeMeterNumber
  			begin 
		        AppLog.logger.info("Inside the Electricity Recharges Controller and Recharge Meter Number API is called at line #{__LINE__}")
		       
		        response =  ElectricityRechargeManager.doRechargeMeterNumber(@agent, params, request)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg,
		            	""+AppConstants::RESPONSE_MSG_DETAILS_KEY_LABEL+"" => (I18n.t :SUCCESS_ELEC_RECH_MSG)
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	         	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    }
			    return
      		end
  		end	

  	end
end  		