# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
  	
  	class TxnRollbacksController < ApiController

  		before_action :authenticateAgent

	  	def txnRollbackInitiate
	  		begin
		  		unless @agent.present?
		          return
		        end

		  		AppLog.logger.info("Inside the Txn Rollbacks Controller's  txnRollbackInitiate method of the API at line = #{__LINE__} ")

		  		txnID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]

		  		AppLog.logger.info("At line = #{__LINE__} and trasnsaction id that will be rollback is = #{txnID}")

		  		response = TxnHeader.getTxnDetails(txnID)

		  		AppLog.logger.info("At line = #{__LINE__} and response code from the manager class is  = #{response.Response_Code} after serach the trasnsaction ID")

		  		if !ResponseCheck.isSuccessful(response)
				    	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
				    	}
				    return
				end

				txnHeaderCurrentTxn = response.txnDetails
				
				finalResponse = TxnHeader.initiatedTxnRollback(txnID, txnHeaderCurrentTxn['amount'], "", txnHeaderCurrentTxn['agent_id'],  txnHeaderCurrentTxn['service_type'], @agent) 

				if !ResponseCheck.isSuccessful(finalResponse)
				    	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => finalResponse.Response_Msg
				    	}
				    return
				else
						render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => finalResponse.Response_Msg
				    	}
				    return
				end
			rescue => exception 
	          		AppLog.logger.info("Inside the Txn Rollbacks Controller's  txnRollbackInitiate method of the API at line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
	          		AppLog.logger.info("Inside the Txn Rollbacks Controller's  txnRollbackInitiate method of the API at line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	        	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
				}
				return
	     	end
	  	end


	  	def checkPendingTxnStatus	
	  	
	  		begin
		  		AppLog.logger.info("Inside the Txn Rollbacks Controller's  checkPendingTxnStatus method of the API at line = #{__LINE__} ")

		  		response = PendingRequest.checkStatus(@agent, params)

		  		AppLog.logger.info("At line = #{__LINE__} and response code from the manager class is  = #{response.Response_Code} after serach the trasnsaction ID")

	  		 	if !ResponseCheck.isSuccessful(response)
				    	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
				    	}
			    	return
		        else
			            render json: {
			            	
			            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
			            	""+AppConstants::RESPONSE_TXN_STATUS_KEY_LABEL+"" => response.Response_Msg
			            }
			    	return

		        end
			rescue => exception 
	          		AppLog.logger.info("Inside the Txn Rollbacks Controller's  txnRollbackInitiate method of the API at line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
	          		AppLog.logger.info("Inside the Txn Rollbacks Controller's  txnRollbackInitiate method of the API at line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	        	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
				}
				return
	     	end

	  	end	

  	end
end  	