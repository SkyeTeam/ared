module Api::V01
	class FieldOfficerController < ApplicationController

	  def authenticate 
	  	officer = Officer.find_by_account(params[:account])
	  	 if officer.try(:password) == params[:password]
	  	   render json: officer, status: :ok
	  	 else
    	   render json: {error: 'Invalid Request'}, status: :unauthorized
	  	 end
	  	rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end

	   def get_kiosk
	  	kiosk = KioskMaster.find(params[:kiosk_master_id])

	  	if kiosk
	      render json: { kiosk: kiosk,success: 1 }, status: :ok
	    else
	      render json: {error: 'Record not found'}, status: :unauthorized
	    end
	    rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end


	  def stats 
	  	officer = Officer.find_by_account(params[:account])
	  	if officer 
	  	  visited = Agent.joins(:reviews).where(reviews: {officer_id: officer.id}).count
	  	  render json: {region: officer.agents.count,visited: visited } ,status: :ok
	  	else
	  	  render json: {error: 'Invalid Request'}, status: :unauthorized
	  	end
	  	rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end

	  def history 
	  	officer = Officer.find_by_account(params[:account])
	  	if officer 
	  	  render json: { reviews: officer.reviews.as_json(methods: [:associated_agents,:date_string]),success: 1 }
	  	else
	  	  render json: { error: 'Invalid Request' }, status: :unauthorized
	  	end
	   rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end

	 
	  # def create
	  #   officer = Officer.new
	  #   officer.email = params[:email]
	  #   officer.phone = params[:phone]
   #      officer.account = params[:account]
   #      officer.password = params[:password]
   #      officer.photo = params[:photo]
   #      officer.name = params[:name]

	  #     if officer.save
	  #       render json: officer, status: :ok
	  #     else
	  #     	render json: officer.errors, status: :unprocessable_entity
	  #     end
	  # end

	  def agents
	    officer = Officer.find_by_account(params[:account])
	  	if officer 
	  	  render json: { agents: officer.agents,success: 1 }
	  	else
	  	  render json: { error: 'Invalid Request' }, status: :unauthorized
	  	end
	  	rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end


	  private 

	  def not_found
	  	rescue ActiveRecord::RecordNotFound
		  render json: {error: 'Record not found'}, status: :unauthorized
	  end

	
	end

end