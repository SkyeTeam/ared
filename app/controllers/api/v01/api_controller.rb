# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
  # This class used for the App APIS
  # :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
  # :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
  # :reek:TooManyStatements: { max_statements: 33 }
  # :reek:DuplicateMethodCall { max_calls: 9 }
  # :reek:InstanceVariableAssumption { enabled: false }
  # :reek:RepeatedConditional { max_ifs: 4 }

  class ApiController < ActionController::Base

    layout false

    def authenticateAgent
      
      phone = params[""+AppConstants::PHONE_PARAM_LABEL+""]
      pin   = params[""+AppConstants::PIN_PARAM_LABEL+""]
      isLoginService = params[""+AppConstants::IS_LOGIN_SERVICE_PARAM_LABEL+""]
      AppLog.logger.info("At line #{__LINE__} inside the ApiController and authenticateAgent method is called to validate the agent with phone = #{phone}") 

      @agent = User.find_by_phone(phone)
      if @agent.present?
        
        if isLoginService.present?
          accessID = Utility.addDataIntoAccessLogs(@agent.id,  User.getUserRoleIntValue(@agent), Constants::LOGIN_THROUGH_APP_STATUS, request)
        end  

        AppLog.logger.info("At line #{__LINE__} and phone = #{phone} is exist in the database and now check the pin of the agent") 
          
        status = @agent.valid_password?(pin)
        AppLog.logger.info("At line #{__LINE__} and whether pin is match in the database or not =  #{status}") 

        if status.eql? Constants::BOOLEAN_TRUE_LABEL 
          AppLog.logger.info("At line #{__LINE__} and status of the agent is  =  #{@agent.status}")

          if @agent.status.eql? Constants::ACTIVE_DB_LABEL or @agent.status.eql? Constants::SUSPEND__DB_LABEL
            if isLoginService.present?
              Utility.updateAccessLoginStatus(accessID)
            end  
            AppLog.logger.info("At line #{__LINE__} and go to API's related class")
          else
            render json: {
              ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE,
              ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => (I18n.t :MF_ACCOUNT_DELETES_MSG)
            }
            return
          end  
        else
            AppLog.logger.info("At line #{__LINE__} authentication failed bcoz pin is not match in the database")
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE,
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => (I18n.t :MF_APP_AUTH_FAIL_MSG)
          }
          return   
        end
      else
          if isLoginService.present?
            Utility.addDataIntoAccessLogs(Constants::ANONYMOUS_LOGIN_USER_ID, Constants::ANONYMOUS_LOGIN_USER_ROLE, Constants::LOGIN_THROUGH_APP_STATUS, request)
          end  
          AppLog.logger.info("At line #{__LINE__} authentication failed bcoz phone = #{phone} is not exist in the database")
        
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE,
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => (I18n.t :MF_APP_AUTH_FAIL_MSG)
        }
        return
      end

    end
   
  end
end