# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
	# :reek:FeatureEnvy { enabled: false }

  	class InternetsController < ApiController

  		before_action :authenticateAgent

  		def getInternetDurations
      		begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Internets Controller and getInternetDurations API is called")
	            render json: {
	            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
	            	""+AppConstants::RESPONSE_INTERNET_DURATIONS_KEY_LABEL+"" => Constants::INTERNET_DURATIONS_LIST,
	            	""+AppConstants::RESPONSE_INTERNET_PER_SEC_RATE_KEY_LABEL+"" => InternetManager.getPerMBRate(@agent.id)
	            }
		    	return
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    }
			    return
      		end
  		end	

  		def getInternetToken
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Internets Controller and getInternetToken API is called")
		        
		        response =  InternetManager.generateToken(@agent, params, request)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg,
		            	""+AppConstants::RESPONSE_MSG_DETAILS_KEY_LABEL+"" => (I18n.t :SUCCESS_TOKEN_GENERATE_MSG)
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end	

  	end
end  		