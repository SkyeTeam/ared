# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
  	class CashDepositController < ApiController

  		def depositFloat
  			begin 
		        Log.logger.info("At line #{__LINE__} inside the CashDepositController and depositFloat API is called")

		        response =  CashDepositManager.agentCashDepositByAPI(params, request)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
		            }
			    	return
		        end
		    rescue => exception
		          	Log.logger.info("At line #{__LINE__} exception message come is = \n#{exception.message}")
		          	Log.logger.info("At line #{__LINE__} and full exception Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end	

  	end
end  		