# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
  	class MobileRechargesController < ApiController

  		before_action :authenticateAgent

  		def rechargeMobile

  			begin 
		    
		        AppLog.logger.info("At line #{__LINE__} inside the MobileRechargesController and rechargeMobile API is called")
		        
		        response =  MobileRechargeManager.doMobileRecharge(@agent, params, request)
		        
		        if !ResponseCheck.isSuccessful(response)
				    	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
				    	}
			    	return
		        else
			            render json: {
			            	
			            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
			            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg,
			            	""+AppConstants::RESPONSE_MSG_DETAILS_KEY_LABEL+"" => (I18n.t :SUCCESS_MOBILE_RECH_MSG)
			            }
			    	return

		        end

		    rescue => exception
		          AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is #{exception.message}")
		          AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  #{exception.backtrace.join("\n")}")
		         	render json: {
				    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
				    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
				    	}
				    return
      		end

  		end	

  	end
end  		