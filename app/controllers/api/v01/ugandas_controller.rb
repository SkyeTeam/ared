# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
	# :reek:RepeatedConditional { max_ifs: 7 }

  	class UgandasController < ApiController

  		before_action :authenticateAgent
  		
  		def rechargeMobile
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Ugandas Controller and rechargeMobile API is called")
		        
		        response =  MobileRechargeManager.doMobileRecharge(@agent, params, request)
		        
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg,
		            	""+AppConstants::RESPONSE_MSG_DETAILS_KEY_LABEL+"" => (I18n.t :SUCCESS_MOBILE_RECH_MSG)
		            }
			    	return
		        end
		    rescue => exception
	          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n#{exception.message}")
	          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end

  		def getNationalWaterAreaList	
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Ugandas Controller and getNationalWaterAreaList API is called")
		        
		        response =  UgandaNationalWaterBillManager.getAreaList
		        
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
		            }
			    	return
		        end
		    rescue => exception
	          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n#{exception.message}")
	         	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end	

  		def validateCustomerForWaterBill
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Ugandas Controller and validateCustomerForWaterBill API is called")
		        
		        response =  UgandaNationalWaterBillManager.validateCustomerForWaterBill(@agent, params, request)
		        
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {
		            		""+AppConstants::RESPONSE_CUSTOMER_NAME_KEY_LABEL+"" => response.customerName, 
		            		""+AppConstants::RESPONSE_REQUEST_REF_KEY_LABEL+"" => response.requestRef,
		            		""+AppConstants::RESPONSE_TRANSACTION_REF_KEY_LABEL+"" => response.customerRef
		            	}
		            }
			    	return
		        end
		    rescue => exception
	          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n#{exception.message}")
	          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end	

  		def payCustomerWaterBill
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Ugandas Controller and payCustomerWaterBill API is called")
		        
		        response =  UgandaNationalWaterBillManager.payCustomerWaterBill(@agent, params, request)

		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg,
		            	""+AppConstants::RESPONSE_MSG_DETAILS_KEY_LABEL+"" => (I18n.t :SUCCESS_WATER_BILL_RECH_MSG)
		            }
			    	return
		        end
		    rescue => exception
	          		AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n#{exception.message}")
	          		AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end	

  		def checkMeterNumber
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Ugandas Controller and checkMeterNumber API is called")
		        
		        response =  UgandaElectricityRechargeManager.checkMeterNumberForRecharge(@agent, params)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {
		            		""+AppConstants::RESPONSE_CUSTOMER_NAME_KEY_LABEL+"" => response.customerName, 
		            		""+AppConstants::RESPONSE_REQUEST_REF_KEY_LABEL+"" => response.requestRef,
		            		""+AppConstants::RESPONSE_TRANSACTION_REF_KEY_LABEL+"" => response.customerRef
		            	}
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  \n #{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    }
			    return
      		end
  		end	

  		def rechargeMeterNumber
  			begin 
		        AppLog.logger.info("At line #{__LINE__} and inside the Ugandas Controller and rechargeMeterNumber API is called")
		        
		        response =  UgandaElectricityRechargeManager.doRechargeMeterNumber(@agent, params, request)
		        
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg,
		            	""+AppConstants::RESPONSE_MSG_DETAILS_KEY_LABEL+"" => (I18n.t :SUCCESS_ELEC_RECH_MSG)
		            }
			    	return
		        end
		    rescue => exception
		          AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n#{exception.message}")
		          AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    }
			    return
      		end
  		end	

  	end
end  	