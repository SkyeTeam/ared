# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
  # This class used for the App APIS
  # :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
  # :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
  # :reek:TooManyStatements: { max_statements: 33 }
  # :reek:DuplicateMethodCall { max_calls: 9 }
  # :reek:InstanceVariableAssumption { enabled: false }
  # :reek:RepeatedConditional { max_ifs: 16 }
  # :reek:TooManyInstanceVariables: { max_instance_variables: 8 }
  # :reek:TooManyMethods: { max_methods: 20 }
  # :reek:FeatureEnvy { enabled: false }

  class AgentsController < ApiController

    before_action :authenticateAgent, except: [:test, :checkNationalID, :forgetPin]
    
    def test
      

    responseData = "{"
    responseData << " \"transactionId\" : \"42352352352\","
    responseData << " \"providerTransactionId\" : \"235325235\","
    responseData << " \"status\" : \"#{Constants::ACCEPTED_LABEL}\","

    responseData << " \"origin\" : {"
    
    responseData << " \"operator\" : \"2353252\","
    responseData << " \"subscriber\" : \"252523523\","
    responseData << " \"countryCode\" : \"25235325325\""
    responseData << " },"
    responseData << " \"amount\" : {"
    responseData << " \"local\" : {"
    responseData << " \"currency\" : \"53252332523532\","
    responseData << " \"value\" : 2352353223"
    responseData << " },"
    responseData << " \"target\" : {"
    responseData << " \"currency\" : \"523532532\","
    responseData << " \"value\" : 525325235235"
    responseData << " },"
    responseData << " \"exchangeRate\" : 252353"
    responseData << " },"
    responseData << " \"extensions\" : [ ]"
    responseData << "}"

      # isvalid  = TxnManager.valid_json?(responseData)

      #responseData  = JSON.parse(responseData)

      render :json => responseData.gsub('\\"', '')
      # render json: {        isvalid: isvalid      }
      
    end 
    
    def balanceEnquiry
      begin
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and balanceEnquiry API is called for get the account info of the loggedin agent in the app")
        currentAgent = @agent

        unless currentAgent.present?
            AppLog.logger.info("At line #{__LINE__} and agent is not present")
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => (I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
          }
          return
        end
        AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")
        
        agentAccountBalance = Utility.getAgentAccountBalance(currentAgent)
        agentGovtAccountBalance = Utility.getAgentGovtAccountBalance(currentAgent)
        agentCommissionBalance = Utility.getAgentCommissionBalance(currentAgent)
        agentLoanBalance = Utility.getAgentLoanBalance(currentAgent)
        agentPenaltyBalance = Utility.getAgentPenaltyBalance(currentAgent)
        AppLog.logger.info("At line #{__LINE__} and agentAccountBalance = #{agentAccountBalance}, agentGovtAccountBalance =#{agentGovtAccountBalance}, agentCommissionBalance = #{agentCommissionBalance}, agentLoanBalance = #{agentLoanBalance} and agentPenaltyBalance = #{agentPenaltyBalance}")
        
        render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              ""+AppConstants::RESPONSE_AGENT_BALANCE_KEY_LABEL+"" => agentAccountBalance,
              ""+AppConstants::RESPONSE_AGENT_GOVT_BALANCE_KEY_LABEL+"" => agentGovtAccountBalance,
              ""+AppConstants::RESPONSE_AGENT_COMM_KEY_LABEL+"" => agentCommissionBalance,
              ""+AppConstants::RESPONSE_AGENT_CREDIT_KEY_LABEL+"" => agentLoanBalance,
              ""+AppConstants::RESPONSE_AGENT_PENALTY_KEY_LABEL+"" => agentPenaltyBalance
            }
          }
        return
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def changePin
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and changePin API is called for change the PIN of the agent")
          
        response = ResetPasswordManager.changeAgentPIN(@agent, params)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def checkNationalID
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and checkNationalID API is called for reset the PIN of the agent")
        
        response = UserManager.validateNationalID(params)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          currentAgent = response.object
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              ""+AppConstants::RESPONSE_USER_ID_KEY_LABEL+"" => currentAgent.id,
              ""+AppConstants::RESPONSE_USER_NATIONAL_ID_KEY_LABEL+"" => currentAgent.national_id
            }
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end  

    def commissionDetails
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and commissionDetails API is called for Today Commission Report on the app")
          
        response =  CommissionManager.getAgentCommissionList(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_DATA_KEY_LABEL+"" => response.commission_list
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
          }
        return
      end
    end  

    def commissionSum
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and commissionSum API is called for Commission Report Chart on the app")
        
        response =  CommissionManager.getAgentCommissionBetweenDates(@agent, params)
        
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
              ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
              ""+AppConstants::RESPONSE_DATA_KEY_LABEL+"" => {
              ""+AppConstants::RESPONSE_SUM_AMOUNT_KEY_LABEL+"" => response.serviceSoldValueArray,
              ""+AppConstants::RESPONSE_SERVICE_NAME_LABEL+"" => response.serviceSoldNameArray
            }
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
          }
        return
      end
    end

    def forgetPin
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and forgetPin API is called for reset the PIN of the agent")
          
        response = ResetPasswordManager.resetAgentPIN(params)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
            }
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end  

    def getServiceCharges
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and getServiceCharges API is called for get the charges on the service for the end customer")

        response = ServiceFeesMaster.getServiceFeeCharges(@agent, params)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              ""+AppConstants::RESPONSE_CLIENT_CHARGES_KEY_LABEL+"" => response.applicableCharges
            }
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end  
    
    def getAccountInfo
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and getAccountInfo API is called for get the account info of the loggedin agent in the app")
        currentAgent = @agent

        unless currentAgent.present?
            AppLog.logger.info("At line #{__LINE__} and agent is not present")
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => (I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
          }
          return
        end
        AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")
        
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
          ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              
            ""+AppConstants::RESPONSE_ACCOUNT_BALANCE_KEY_LABEL+"" => Utility.getAgentAccountBalance(currentAgent),
            ""+AppConstants::RESPONSE_CREDIT_LIMIT_KEY_LABEL+"" => Utility.getAgentMaxLoanCapacity(currentAgent),
            ""+AppConstants::RESPONSE_LOAN_BALANCE_KEY_LABEL+"" => Utility.getAgentLoanBalance(currentAgent)

          }
        }
        return
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def getDetails
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and getDetails API is called for get the info of the loggedin agent in the app")
        currentAgent = @agent

        unless currentAgent.present?
            AppLog.logger.info("At line #{__LINE__} and agent is not present")
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => (I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
          }
          return
        end
        AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")
        
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
          ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              
            ""+AppConstants::RESPONSE_USER_ID_KEY_LABEL+"" => currentAgent.id,
            ""+AppConstants::RESPONSE_USER_FIRST_NAME_KEY_LABEL+"" => currentAgent.name,
            ""+AppConstants::RESPONSE_USER_LAST_NAME_KEY_LABEL+"" => currentAgent.last_name,
            ""+AppConstants::RESPONSE_USER_PHONE_KEY_LABEL+"" => currentAgent.phone,
            ""+AppConstants::RESPONSE_USER_GENDER_KEY_LABEL+"" => currentAgent.gender,
            ""+AppConstants::RESPONSE_USER_EMAIL_KEY_LABEL+"" => currentAgent.email,
            ""+AppConstants::RESPONSE_USER_ADDRESS_KEY_LABEL+"" => currentAgent.address,
            ""+AppConstants::RESPONSE_USER_CITY_KEY_LABEL+"" => currentAgent.city,
            ""+AppConstants::RESPONSE_USER_DISTRICT_KEY_LABEL+"" => currentAgent.district,
            ""+AppConstants::RESPONSE_USER_COUNTRY_KEY_LABEL+"" => currentAgent.country,
            ""+AppConstants::RESPONSE_USER_ZIP_CODE_KEY_LABEL+"" => currentAgent.zip_code,
            ""+AppConstants::RESPONSE_USER_IMAGE_KEY_LABEL+"" => Properties::APP_IMAGE_PATH.to_s + currentAgent.image.to_s,
            ""+AppConstants::RESPONSE_USER_CREATED_AT_KEY_LABEL+"" => currentAgent.created_at.strftime("%d %B %Y"),
            ""+AppConstants::RESPONSE_USER_UPDATED_AT_KEY_LABEL+"" => currentAgent.updated_at,
            ""+AppConstants::RESPONSE_KIOSK_ASSIGNED_KEY_LABEL+"" => KioskMaster.getAgentAssignKioskModel(currentAgent.id)

          }
        }
        return
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end  

    def getLoanStatus
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and getLoanStatus API is called for the displaying message at the first time of login in the app")
          
        response = CreditManager.getAgentCreditStatus(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
            }  
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
              ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
            }
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end  

    def getNotification
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and getNotification API is called for the displaying norifications at the top of app")
          
        response = NotificationNew.getNotificationsList(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_NOTIFICATIONS_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_NOTIFICATIONS_KEY_LABEL+"" => response.object
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def readFlag
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and readFlag API is called for the update the read status of the norifications in the app")
          
        response = NotificationNew.updateNotificationReadFlag(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def getTransactionCategory 
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and getTransactionCategory API is called for the Transaction History in the app")
          
        response = TranscationCategory.getOperatorsList(@agent, params)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.object
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end      
   
    def loanDetails
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and loanDetails API is called for the Transaction History in the app")
          
        response = CreditManager.getAgentCreditHistory(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_LOANS_KEY_LABEL+"" => response.object
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def penaltyHistory
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and penaltyHistory API is called for the Penalty History in the app")
          
        response = AgentPenaltyDetail.fetchDetails(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_TXN_DATA_KEY_LABEL+"" => response.object
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end 
    end

    def txnHistory
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and txnHistory API is called for the Transaction History in the app")
          
        response = TranscationReport.getAgentTxnHistory(@agent)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_TXN_DATA_KEY_LABEL+"" => response.object,
            ""+AppConstants::RESPONSE_ACC_BALANCE_KEY_LABEL+"" => response.balance
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end 
    end

    def txnHistoryBWDates
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and txnHistoryBWDates API is called for the Transaction History when agent select dates in the app")
          
        response = TranscationReport.getAgentTxnHistoryBWDates(@agent, params)
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_TXN_DATA_KEY_LABEL+"" => response.object,
            ""+AppConstants::RESPONSE_ACC_BALANCE_KEY_LABEL+"" => response.balance
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end
    end

    def receipt
      begin 
        AppLog.logger.info("At line #{__LINE__} inside the AgentsController's and receipt API is called for the print receipt or rollback in the app")
          
        response = TranscationReport.getReceiptDetails(@agent, params)
          
        if !ResponseCheck.isSuccessful(response)
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
            ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
          }
          return
        else
          txnHeader = response.object
          render json: {
            ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
            ""+AppConstants::RESPONSE_TXN_DATA_KEY_LABEL+"" => {
              ""+AppConstants::RESPONSE_TRANSACTION_TYPE_KEY_LABEL+"" => txnHeader.service_type,
              ""+AppConstants::RESPONSE_TRANSACTION_SUB_TYPE_KEY_LABEL+"" => txnHeader.sub_type,
              ""+AppConstants::RESPONSE_CUSTOMER_NO_KEY_LABEL+"" => txnHeader.account_id,
              ""+AppConstants::RESPONSE_DATE_KEY_LABEL+"" => txnHeader.txn_date,
              ""+AppConstants::RESPONSE_AMOUNT_KEY_LABEL+"" => txnHeader.amount,
              ""+AppConstants::RESPONSE_CUSTOMER_NAME_KEY_LABEL+"" => txnHeader.electricity_costumer,
              ""+AppConstants::RESPONSE_CLIENT_CHARGES_KEY_LABEL+"" => txnHeader.customer_fee,
              ""+AppConstants::RESPONSE_CURRENCY_VALUE_KEY_LABEL+"" => Utility.getCurrencyLabel,
              ""+AppConstants::RESPONSE_TOKEN_KEY_LABEL+"" => txnHeader.param1,
              ""+AppConstants::RESPONSE_VAT_KEY_LABEL+"" => txnHeader.vat,
              ""+AppConstants::RESPONSE_UNIT_KEY_LABEL+"" => txnHeader.unit
            }
          }
          return
        end
      rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception message is =\n #{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
        render json: {
          ""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
          ""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
        }
        return
      end  
    end

  end
end