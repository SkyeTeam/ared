# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
  	class TaxesController < ApiController

  		before_action :authenticateAgent

  		def validateBillNumber
  			begin 
		        AppLog.logger.info("At line #{__LINE__} inside the TaxesController and validateBillNumber method is called")
		        
		        response =  TaxesManager.validateBillNumber(@agent, params, request)
		        
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {  
			             
				            ""+AppConstants::RESPONSE_BALANCE_KEY_LABEL+"" => response.balance,
				            ""+AppConstants::RESPONSE_DEBIT_ACCOUNT_NO_KEY_LABEL+"" => Constants::IREMBO_DEBIT_ACCOUNT_NO,
				            ""+AppConstants::RESPONSE_CURRENCY_CODE_KEY_LABEL+"" => response.currencyCode,
				            ""+AppConstants::RESPONSE_ACCOUNT_NAME_KEY_LABEL+"" => response.accountName,
				            ""+AppConstants::RESPONSE_ACCOUNT_NO_KEY_LABEL+"" => response.accountNo,
			              	""+AppConstants::RESPONSE_SERVICE_NAME_KEY_LABEL+"" => response.serviceName
			            
			            }
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} inside the TaxesController and exception message come in the isvalidateBillNumber method is = \n#{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and full exception Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end

  		def billPayment
  			begin 
		        AppLog.logger.info("At line #{__LINE__} inside the TaxesController and billPayment method is called")
		        
		        response =  TaxesManager.payTax(@agent, params, request)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} inside the TaxesController and exception message come in the billPayment method is = \n#{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and full exception Details are =  \n#{exception.backtrace.join("\n")}")
	         	render json: {
		    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
		    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
		    	}
			    return
      		end
  		end	

  	end
end  		