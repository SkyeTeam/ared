# :reek:UncommunicativeModuleName: { reject: -!ruby/regexp /x/ }
module Api::V01
	# This class used for the App APIS
	# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
	# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
	# :reek:TooManyStatements: { max_statements: 33 }
	# :reek:DuplicateMethodCall { max_calls: 9 }
	# :reek:InstanceVariableAssumption { enabled: false }
  	class LoginsController < ApiController

  		before_action :authenticateAgent

  		def doLogin
  			begin 
		        AppLog.logger.info("At line #{__LINE__} inside the LoginsController and doLogin API is called for login the agent")
		        
		        response =  AppLoginManager.doLogin(@agent, params)
		        if !ResponseCheck.isSuccessful(response)
			    	render json: {
			    		""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    		""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => response.Response_Msg
			    	}
			    	return
		        else
		            render json: {
		            	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_TRUE_VALUE,  
		            	""+AppConstants::RESPONSE_DETAILS_KEY_LABEL+"" => {
		            		""+AppConstants::RESPONSE_USER_ID_KEY_LABEL+"" => @agent.id,
							""+AppConstants::RESPONSE_USER_NAME_KEY_LABEL+"" => @agent.name,
		            		""+AppConstants::RESPONSE_USER_LAST_NAME_KEY_LABEL+"" => @agent.last_name,
		            		""+AppConstants::RESPONSE_USER_PHONE_KEY_LABEL+"" => @agent.phone,
		            		""+AppConstants::RESPONSE_USER_EMAIL_KEY_LABEL+"" => @agent.email,
		            		""+AppConstants::RESPONSE_USER_CREATED_ON_KEY_LABEL+"" => @agent.created_at.strftime("%d %B %Y"),
		            		""+AppConstants::RESPONSE_USER_IMAGE_KEY_LABEL+"" => Properties::APP_IMAGE_PATH.to_s + @agent.image.to_s,
		            		""+AppConstants::RESPONSE_KIOSK_ASSIGNED_KEY_LABEL+"" => KioskMaster.getAgentAssignKioskModel(@agent.id)
		            	}
		            }
			    	return
		        end
		    rescue => exception
		          	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n#{exception.message}")
		          	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are =  \n #{exception.backtrace.join("\n")}")
	         	render json: {
			    	""+AppConstants::RESPONSE_SUCCESS_KEY_LABEL+"" => AppConstants::RESPONSE_FALSE_VALUE, 
			    	""+AppConstants::RESPONSE_MSG_KEY_LABEL+"" => exception.message
			    }
			    return
      		end
  		end	

  	end
end  		