# This class used for the Audit the logs
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class AuditLogsController < ApplicationController

	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'
	
	# :reek:TooManyStatements: { max_statements: 16 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
	def index
		begin
			Log.logger.info("At line #{__LINE__} inside the AuditLogsController and method searchDetails is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			unless params[:reportType].present? #This is for the user when click on the first time from the menu bar
				params[:reportType] = Constants::ACCESS_LOGS_REPORT_LABEL
				params[:user] = Constants::ALL_USER_LABEL
				params[:from_date] = Date.today
				params[:to_date] = Date.today
			end	

			response = AccessLog.fetchDetails(params, loggedInAdmin, request)
			responseMsg =  response.Response_Msg 
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
			else
				@detailsHash = response.detailsHash
		    end
		rescue => exception
	        	Log.logger.info("Inside the Audit Logs Controller's method new and  exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::AUDIT_LOGS_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__} and check user has access of #{Constants::AUDIT_LOGS_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end

end