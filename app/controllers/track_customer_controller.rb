# This class used for the Track Customer
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class TrackCustomerController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
		begin
			Log.logger.info("Inside the Track Customer Controller's at line #{__LINE__} and method fetchCustomer is called")
      		
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 			
 			unless params[:transactionType].present? #This is for the user when click on the first time from the menu bar
				params[:transactionType] = Constants::ANY_SERVICE_LABEL
				params[:from_date] = Utility.getDefaultDateArray
				params[:to_date] = Utility.getDefaultDateArray
			end	

 			response = TrackCustomerManager.getDetails(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg = response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false") 
			else
				@txnHeader = response.detailsHash
		    end
	    rescue => exception
	       	 	Log.logger.info("Inside the Track Customer Controller's at line #{__LINE__} and method fetchCustomer Exception is = \n #{exception.message}")
	        	Log.logger.info("Inside the Track Customer Controller's at line #{__LINE__} and method fetchCustomer Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	    end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::TRACK_CUSTOMER_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::TRACK_CUSTOMER_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
