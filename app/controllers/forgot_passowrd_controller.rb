# This class used for the  Leasing Fee Setup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class ForgotPassowrdController < ApplicationController

	layout 'login'
	skip_before_action :check_session_expiry

	def index
	end

	def new
	end
		
	def validate
		begin
			Log.logger.info("At line #{__LINE__} inside the ForgotPassowrdController and method forgot is called")
      		
 			response = ResetPasswordManager.validateUserEmail(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
  	        	DashboardManager.setDisplayMessage(response.Response_Msg.to_s + "|false") 
			else
				DashboardManager.setDisplayMessage(response.Response_Msg.to_s + "|true") 
		    end
		    redirect_to '/ForgotPassword'
	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the ForgotPassowrdController and method forgot is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      		DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/ForgotPassword'
	    end
	end	

	def forgot
		begin
			Log.logger.info("At line #{__LINE__} inside the ForgotPassowrdController and method forgot is called")
      		
 			response = ResetPasswordManager.setNewPassword(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
  	        	DashboardManager.setDisplayMessage(response.Response_Msg.to_s + "|false") 
			else
				DashboardManager.setDisplayMessage(response.Response_Msg.to_s + "|true") 
		    end
		    redirect_to root_path
	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the ForgotPassowrdController and method forgot is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      		DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/ForgotPassword'
	    end
	end	

end