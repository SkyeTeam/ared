# This class used for the Aggregator Signup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class AggregatorController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'
	
	def index
		@aggregatorList = AggregatorManager.getAllAggregators("created_at","DESC")
	end	
	
	# :reek:TooManyStatements: { max_statements: 16 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
	def create
		begin
			Log.logger.info("Inside the Aggregator Controller's method new to create the new aggreator at line = #{__LINE__}")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		
			response = AggregatorManager.createAggregator(loggedInAdmin, params)

			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	        render :controller => 'aggreator', :action => 'new'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
				redirect_to '/AggregatorSignup'
		    end

		rescue => exception
	        Log.logger.info("Inside the Aggregator Controller's method new to create the new aggreator and exception is = \n #{exception.message}")
	        Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/NewAggregatorSignup'
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::AGGREGATOR_SIGNUP_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::AGGREGATOR_SIGNUP_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
