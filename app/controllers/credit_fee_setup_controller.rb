# This class used for the Credit Fee Setup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CreditFeeSetupController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
		@availableCredits = CreditFeeConfig.getDefaultCreditDetails
	end	

	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
	def getCreditDetails
		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

 			response = CreditFeeConfig.getCreditFeeDetalis(params, loggedInAdmin)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(response.Response_Msg+"|false") 
	  	        render 'credit_fee_setup/index'
	  	        return
		    else
		    	if response.status_flag
		    		DashboardManager.setDisplayMessage((I18n.t :NO_CREDIT_FEE_FOUND_MSG).to_s+"|false") 
		    	end	
			    @availableCredits = response.object
			    @agent = response.user
	     		render 'credit_fee_setup/index'
	     		return
	     	end	

	    rescue => exception
	        Log.logger.info("Inside the Credit Fee Setup Controller's at line #{__LINE__} and method getCreditDetails Exception is = #{exception.message}")
	        Log.logger.info("Inside the Credit Fee Setup Controller's at line #{__LINE__} and method getCreditDetails Full Exception is = #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/CreditFeeSetup'
	    end
	end	

	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
	def setupCredit
		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 	
 			response = CreditFeeConfig.updateCreditFee(params, loggedInAdmin)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false") 
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
		    end

     		redirect_to '/CreditFeeSetup'

	    rescue => exception
	        Log.logger.info("Inside the Credit Fee Setup Controller's at line #{__LINE__} and method setupCredit Exception is = #{exception.message}")
	        Log.logger.info("Inside the Credit Fee Setup Controller's at line #{__LINE__} and method setupCredit Full Exception is = #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/CreditFeeSetup'
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::CREDIT_FEE_SETUP_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::CREDIT_FEE_SETUP_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
