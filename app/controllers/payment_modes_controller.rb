# This class used for the Aggregator Signup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class PaymentModesController < ApplicationController

	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
  		@paymentModesList = PaymentMode.all.order("created_at DESC")
  	end	

  	def new
  	end
  	
  	def create

  		begin
			Log.logger.info("At line #{__LINE__} inside the Payment Modes Controller and create method is called to add the new Payment mode")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are name = #{Utility.getAgentFullName(loggedInAdmin)}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

			response = PaymentMode.saveNewPaymentMode(params, loggedInAdmin)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")
			
			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	        render :controller => 'payment_modes', :action => 'new'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
				redirect_to '/PaymentModes'
		    end
			
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the Payment Modes Controller and create method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/PaymentModes'
	    end

  	end	

  	def delete

  		begin
			Log.logger.info("At line #{__LINE__} inside the Payment Modes Controller and delete method is called to delete the Payment mode")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are name = #{Utility.getAgentFullName(loggedInAdmin)}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

			response = PaymentMode.deletePaymentMode(params, loggedInAdmin)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")
			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
		    end

		    redirect_to '/PaymentModes'
			
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the Payment Modes Controller and delete method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/PaymentModes'
	    end

  	end	

  	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::PAYMENT_MODES_SETUP_ROLE, current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::PAYMENT_MODES_SETUP_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      	return true
	    else
	      		DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      		redirect_to "/Dashboard"
	      	return false
	    end
  	end

end