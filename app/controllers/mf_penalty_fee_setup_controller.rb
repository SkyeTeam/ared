# This class used for the  Mf Penalty Fee Setup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class MfPenaltyFeeSetupController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
  		@penaltiesList = MfPenaltyFee.where("status = #{Constants::MF_PENALTY_ACTIVE_STATUS}").order('created_at DESC')
  	end	
	
	def new
	end

	def create
		begin
			Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Fee Setup Controller's method create is called for create new Penalty")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		
			response = MfPenaltyFee.createPenalty(params, loggedInAdmin, request)

			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	        render :controller => 'mf_penalty_fee_setup', :action => 'new'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true")
				redirect_to '/PenaltySetup'
		    end
		rescue => exception
	        Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Fee Setup Controller's method create and exception is = \n #{exception.message}")
	        Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/PenaltySetup'
	    end
	end	

	def delete
		begin
			Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Fee Setup Controller's method delete is called for delete the Penalty")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		
			response = MfPenaltyFee.deletePenalty(params, loggedInAdmin, request)

			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
	  	 	else
				DashboardManager.setDisplayMessage(responseMsg+"|true")
		    end
		    redirect_to '/PenaltySetup'
		rescue => exception
	        Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Fee Setup Controller's method create and exception is = \n #{exception.message}")
	        Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/PenaltySetup'
	    end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::MF_PENALTY_SETUP_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::MF_PENALTY_SETUP_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end