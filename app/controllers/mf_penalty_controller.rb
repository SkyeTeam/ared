# This class used for the  Mf Penalty
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:InstanceVariableAssumption { enabled: false }

class MfPenaltyController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'
  		
  	def index
  		userID = params[:user]
	    if userID != nil
	        @user = User.find(userID) # This @user is used at the time of search by name and click on the one particualr user then his details are get at the view page new
	        params[:name] = @user.name.to_s + " " + @user.last_name.to_s
	    end 
  	end	
  	
  	def searchByPhone
	    begin
		    Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Controller's method searchByPhone is called")
		    phone = params[:phone] # Get the phone number entered on the web page to search the agent for the cash deposit

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		     
		    response = CashDepositManager.searchByPhone(phone) # after getting the phone number from the webpage pass it to the manager class
		      
		    # If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull  
		    if !ResponseCheck.isSuccessful(response)
		        responseMsg =  response.Response_Msg 
		        DashboardManager.setDisplayMessage(responseMsg+"|false")
		    else
		    	# if the response code is 200 then after getting the detils of the user render to the search page 
		        @user = response.User_Object 
		        @isSearchViewDisplay = false
		    end
		    render :controller => 'mf_penalty', :action => 'index'
	    rescue => exception
	        	Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Controller's method searchByPhone and exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/MFPenalties'
	    end 
  	end

	def searchByName
	    begin
	      	name = params[:name]
		    Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Controller's method searchByName is called and search with name = #{name}")
		     
		    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

		    response = CashDepositManager.searchByName(name)

		    if !ResponseCheck.isSuccessful(response)
		        responseMsg =  response.Response_Msg 
		        DashboardManager.setDisplayMessage(responseMsg+"|false")
		    else
		        @user = response.User_Object
		        @isSearchViewDisplay = true 
		    end
		    render :controller => 'mf_penalty', :action => 'index'
	    rescue => exception
	        	Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Controller's method searchByName and exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/MFPenalties'
	    end 
	end

	def addPenalty
		begin
	      	Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Controller's method addPenalty is called")
		     
		    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

		    response = MfPenaltyFee.applyPenalty(params, loggedInAdmin, request)

			responseMsg =  response.Response_Msg 
		    if !ResponseCheck.isSuccessful(response)
		        DashboardManager.setDisplayMessage(responseMsg+"|false")
		    else
		    	DashboardManager.setDisplayMessage(responseMsg+"|true")
		    end
		    redirect_to '/MFPenalties'
	    rescue => exception
	        	Log.logger.info("At line = #{__LINE__} and inside the Mf Penalty Controller's method addPenalty and exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/MFPenalties'
	    end 
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::MF_PENALTY_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::MF_PENALTY_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end