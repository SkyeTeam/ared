# This class used for the Credit Approval
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CreditApprovalController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
  		@pendingCredits = PendingCredit.getPendingCredits
  	end	

  	# :reek:NilCheck { enabled: false }
	def viewSingleCredit
  		isDataPresent = params[:isDataPresent] 
		if isDataPresent == nil
			redirect_to "/CreditApproval"
	     	return
		end
  	end	

  	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
  	def approvedCredit 
  		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			Log.logger.info("Inside the Credit Approval Controller's line = #{__LINE__} and params give to the manager class")

			response = PendingCredit.confrimApproveRejectCredit(params, loggedInAdmin, request)

			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg
 			
 			if !ResponseCheck.isSuccessful(response)
		          	DashboardManager.setDisplayMessage(responseMsg+"|false") 
		        redirect_to '/CreditApproval'
		        return
		    else
		    		DashboardManager.setDisplayMessage(responseMsg+"|true")
		   		redirect_to '/CreditApproval'
		   		return	
		    end

	    rescue => exception
	        Log.logger.info("Inside the Credit Approval Controller's at line #{__LINE__} and method approvedCredit Exception is = #{exception.message}")
	        Log.logger.info("Inside the Credit Approval Controller's at line #{__LINE__} and method approvedCredit Full Exception is = #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/CreditApproval'
	    end

  	end	

  	private
  	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::CREDIT_APPROVAL_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::CREDIT_APPROVAL_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
