# This class used for the  Leasing Fee Setup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class LeasingFeeSetupController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
		@leasingFees =  LeasingFee.all.order('applicable_from DESC')
	end	

	def new
		params[:applicable_from] = Date.today
	end

	def createLeasingFeeSetup
		begin
			Log.logger.info("At line #{__LINE__} inside the LeasingFeeSetupController and method createLeasingFeeSetup is called")
      		
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 	
 			response = LeasingFee.createLeasingFee(params, loggedInAdmin)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")
			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
  	        	DashboardManager.setDisplayMessage(responseMsg+"|false") 
  	        	render :controller => 'leasing_fee_setup', :action => 'new'
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
				redirect_to '/LeasingFeeSetup'
		    end

	    rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the LeasingFeeSetupController and method createLeasingFeeSetup is called exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} inside the LeasingFeeSetupController and method createLeasingFeeSetup is called full exception is = \n #{exception.backtrace.join("\n")}")
	      		DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/LeasingFeeSetup'
	    end
	end	

	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::LEASING_FEE_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::LEASING_FEE_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
