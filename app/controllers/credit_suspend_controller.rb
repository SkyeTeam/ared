# This class used for the Credit Suspend
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CreditSuspendController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
		userID = params[:user]
	    if userID != nil
	        @user = User.find(userID) # This @user is used at the time of search by name and click on the one particualr user then his details are get at the view page new
	        params[:name] = @user.name.to_s + " " + @user.last_name.to_s
	    end 
	end	

	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
  	def searchByPhone
	    begin
		    phone = params[:phone] # Get the phone number entered on the web page to search the agent for the cash deposit

		    unless phone.present? #This is for the new request if user direct enter the url
	        	redirect_to '/CreditSuspend'
	       	 	return
	      	end  
		    
		    Log.logger.info("The Credit Suspend Controller's method search By Phone at line #{__LINE__} is called with phone =#{phone}") 

		    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		     
		    response = CashDepositManager.searchByPhone(phone) # after getting the phone number from the webpage pass it to the manager class
		      
		    # If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull  
		    if !ResponseCheck.isSuccessful(response)
		        responseMsg =  response.Response_Msg 
		        DashboardManager.setDisplayMessage(responseMsg+"|false")
		        render 'credit_suspend/index'
		    else
		    	# if the response code is 200 then after getting the detils of the user render to the search page 
		        @user = response.User_Object 
		        @isSearchViewDisplay = false
		        render 'credit_suspend/index'
		    end
	    rescue => exception
	        	Log.logger.info("The Credit Suspend Controller's at line #{__LINE__} inside the searchByPhone method  and exception message is = #{exception.message}")
	        	Log.logger.info("The Credit Suspend Controller's at line #{__LINE__} inside the searchByPhone method  and full exception is = #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/CreditSuspend'
	    end 
  	end

  	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
	def searchByName
	    begin

	      	name = params[:name]
	      	unless name.present?  #This is for the new request if user direct enter the url
	        		redirect_to '/CreditSuspend'
	        	return
	      	end  

		    Log.logger.info("The Credit Suspend Controller's method search By Name at line #{__LINE__} is called with name =#{name}") 
		     
		    loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

		    response = CashDepositManager.searchByName(name)

		    if !ResponseCheck.isSuccessful(response)
		        responseMsg =  response.Response_Msg 
		        DashboardManager.setDisplayMessage(responseMsg+"|false")
		        render 'credit_suspend/index'
		    else
		        @user = response.User_Object
		        @isSearchViewDisplay = true 
		        render 'credit_suspend/index'
		    end
	    rescue => exception
	        	Log.logger.info("The Credit Suspend Controller's method search By Name at line #{__LINE__}  and exception message is = #{exception.message}")
	        	Log.logger.info("The Credit Suspend Controller's method search By Name at line #{__LINE__}  and full exception is = #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/CreditSuspend'
	    end 
	end  


	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
	def suspendCredit
	    begin
	      	Log.logger.info("The Credit Suspend Controller's method suspend Credit is called at line #{__LINE__}") 
	      
	      	loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
	      	Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

	      	response = CreditSuspend.updateCreditStauts(params, loggedInAdmin)
	      	responseMsg =  response.Response_Msg 

        	if !ResponseCheck.isSuccessful(response)
          		DashboardManager.setDisplayMessage(responseMsg+"|false")
          		redirect_to '/CreditSuspend'
        	else
          		DashboardManager.setDisplayMessage(responseMsg+"|true")
          		redirect_to '/CreditSuspend'
        	end
	    rescue => exception
	        Log.logger.info("The Credit Suspend Controller's method suspend Credit at line #{__LINE__}  and exception message is = #{exception.message}")
	        Log.logger.info("The Credit Suspend Controller's method suspend Credit at line #{__LINE__}  and full exception is = #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/CreditSuspend'
	    end  
	end

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::CREDIT_SUSPEND_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::CREDIT_SUSPEND_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
