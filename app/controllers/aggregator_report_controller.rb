# This class used for the Aggregator Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class AggregatorReportController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'
	
	# :reek:TooManyStatements: { max_statements: 16 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
	def index
		begin
			Log.logger.info("Inside the Aggregator Report Controller's method new to deposit the amount at line = #{__LINE__}")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			unless params[:aggregatorName].present?
				params[:aggregatorName] = AggregatorManager.getAllAggregatorName.values[1]
				params[:reportType] = Constants::AGGREGATOR_REPORT_TYPE_SEL_OPT.values[2]
				params[:from_date] = Date.today
				params[:to_date] = Date.today
			end	

			response = AggregatorManager.fetchDetails(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

 			if !ResponseCheck.isSuccessful(response)
  	        	DashboardManager.setDisplayMessage(response.Response_Msg+"|false")
			else
				@txnHeader = response.txn_history
		    end
		rescue => exception
	        	Log.logger.info("Inside the Aggregator Report Controller's method new and  exception is = \n #{exception.message}")
	        	Log.logger.info("Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::AGGREGATOR_REPORT_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::AGGREGATOR_REPORT_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      	return true
	    else
	      		DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      	redirect_to "/Dashboard"
	      	return
	    end
  	end
end