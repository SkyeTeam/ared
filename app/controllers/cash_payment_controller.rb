# This class used for the Withdrawal funds from the MF account
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CashPaymentController < ApplicationController

  layout 'header'
  before_action :require_login
  before_action :checkServiceAccess

  def new
  end

  # :reek:TooManyStatements: { max_statements: 19 }
  # :reek:DuplicateMethodCall { max_calls: 6 }
  def create
    begin
      Log.logger.info("At line #{__LINE__} inside the CashPaymentController and create method  is called")

      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("At line #{__LINE__} details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
     
      response = User.searchByPhoneName(params)
      if !ResponseCheck.isSuccessful(response)
          DashboardManager.setDisplayMessage(response.Response_Msg.to_s + "|false")
          render new_cash_payment_path
      else
          @user = response.User_Object 
          render new_cash_payment_path
      end
    rescue => exception
        Log.logger.info("At line #{__LINE__} inside the CashPaymentController and create method exception is = \n #{exception.message}")
        Log.logger.info("At line #{__LINE__} inside the CashPaymentController and create method full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      render new_cash_payment_path
    end  
  end

  # :reek:TooManyStatements: { max_statements: 19 }
  # :reek:DuplicateMethodCall { max_calls: 6 }
  def payment
    begin
      Log.logger.info("At line #{__LINE__} inside the CashPaymentController and payment method  is called")
  
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
    
      response = CashPaymentManager.cashPayment(params[:user_id], params[:amount].to_f, params[:payment_mode], params[:ext_ref], loggedInAdmin, request)
      responseMsg =  response.Response_Msg

      if !ResponseCheck.isSuccessful(response)
        DashboardManager.setDisplayMessage(responseMsg+"|false")
      else
        DashboardManager.setDisplayMessage(responseMsg+"|true")
      end
      redirect_to new_cash_payment_path  
    rescue => exception
        Log.logger.info("At line #{__LINE__} inside the CashPaymentController and payment method exception is = \n #{exception.message}")
        Log.logger.info("At line #{__LINE__} inside the CashPaymentController and payment method full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_cash_payment_path
    end  
  end
  
  private
  # :reek:TooManyStatements: { max_statements: 7 }
  def checkServiceAccess
    hasAccess = Utility.hasServiceAccess(Constants::MF_REMBURSE_ROLE,current_user.id)
    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::MF_REMBURSE_ROLE} service or not = #{hasAccess}")
    if hasAccess
      return true
    else
      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
      redirect_to "/Dashboard"
      return
    end
  end

end