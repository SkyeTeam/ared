# This class used for the Transaction Rollback Approve
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:NilCheck { enabled: false }

class TxnRollbackApproveController < ApplicationController
	
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
  		@initatedTxns = TxnHeader.getAllInitiatedTxns
  	end	

  	def approveSingleTxn
  		isDataPresent = params[:isDataPresent] 
		if isDataPresent == nil
			redirect_to "/ApproveTxnRollback"
	     	return
		end
  	end	

  	def approveTxnRollbackConfirm
  		begin
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 	
	 		userAction = params[:userAction]
	  		txnID = params[:txnID] # This is id of the TRANSACTION_ROLLBACK entry in the txn_headers table
	  		rolledBackTxnId = params[:rolledBackTxnId] # This is the id of that transaction that will br roll back
	  		amount = params[:amount]
	  		serviceType = params[:serviceType]
 			Log.logger.info("At Line #{__LINE__} and user action is = #{userAction} ,the transaction id that will be rollback = #{rolledBackTxnId} and initiate transaction rollback entry id is = #{txnID}.")
			
			response = TxnHeader.confrimApproveRejectTxn(userAction, txnID, rolledBackTxnId, amount, serviceType, loggedInAdmin, params) 
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")
			responseMsg =  response.Response_Msg
 			
 			if !ResponseCheck.isSuccessful(response)
		        DashboardManager.setDisplayMessage(responseMsg+"|false") 
		    else
		    	DashboardManager.setDisplayMessage(responseMsg+"|true")	
		    end
		   	redirect_to '/ApproveTxnRollback'
	    rescue => exception
	        Log.logger.info("Inside the Txn Rollback Approve Controller's at line #{__LINE__} and method approveTxnRollbackConfirm Exception is = #{exception.message}")
	        Log.logger.info("Inside the Txn Rollback Approve Controller's at line #{__LINE__} and method approveTxnRollbackConfirm Full Exception is = #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/ApproveTxnRollback'
	    end
  	end	

  	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::TXN_ROLLBACK_APPROVE_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::TXN_ROLLBACK_APPROVE_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
  
end