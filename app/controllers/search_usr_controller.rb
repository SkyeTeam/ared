# This class used for the Search User
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:NilCheck { enabled: false }

class SearchUsrController < ApplicationController
	before_action :require_login
  layout 'header'
  
  def new
    params[:status]
  end

  #This method search the user by phone  and perform the action ie update,suspend
  def create
    begin
      r= Option.new
      Log.logger.info("Search Usr Controller method create called, option mamager view is called with parameter phone and status are=#{params[:phone]} and #{params[:status]}")
     
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 
      response=r.view(params[:phone],params[:status])
      #responseCode = response.Response_Code 
      if !ResponseCheck.isSuccessful(response)
        responseMsg =  response.Response_Msg

        DashboardManager.setDisplayMessage(responseMsg+"|false")
        redirect_to new_search_usr_path(:status=> params[:status])
 
        #redirect_to new_search_usr_path(:status=> params[:status]) , alert: responseMsg
        return
      end  

      if response.status_flag ==1
        Log.logger.info("Search Usr Controller method create called, option mamager view is called and return parameter status_flag =1 and redirected to controller=users,action =edit")
        redirect_to :controller => "users", :action => "edit", :id => response.user
      elsif response.status_flag ==2
        Log.logger.info("Search Usr Controller method create called, option mamager view is called and return parameter status_flag =2 and redirected to users_susp_path")
        redirect_to users_susp_path(:id => response.user,:status => "suspend")
      elsif response.status_flag ==3
        Log.logger.info("Search Usr Controller method create called, option mamager view is called and return parameter status_flag =3 and redirected to users_del_path")
        redirect_to users_del_path(:id => response.user,:status => "delete")
      elsif response.status_flag ==4
        Log.logger.info("Search Usr Controller method create called, option mamager view is called and return parameter status_flag =4 and redirected to  users_suspet_path")
        redirect_to users_suspet_path(:id => response.user)
      end
    rescue => exception
        Log.logger.info("***********Exception is#{exception.message}****************")
        Log.logger.info("Exception is Search Usr controller's create method #{exception.backtrace.join("\n")}")
        redirect_to new_search_usr_path(:status=> params[:status]) , alert: "We're sorry, but something went wrong"
    end
   
  end

  #This method search all the users by name
  def search_name
    begin
      r= Option.new
      Log.logger.info("Search Usr Controller method create called, option mamager view_name is called with parameter phone and status are=#{params[:phone]} and #{params[:status]}")
 
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
  
      if params[:name] == nil 
        redirect_to new_search_usr_path(:status=> params[:status])
        return
      end  

      response=r.view_name(params[:name],params[:status])

      Log.logger.info("SearchUsrController and search name is called name = #{params[:name]} and statsu is =#{params[:status]}")
      if !ResponseCheck.isSuccessful(response)
        responseMsg =  response.Response_Msg

        DashboardManager.setDisplayMessage(responseMsg+"|false")
        redirect_to new_search_usr_path(:status=> params[:status])
 
        #redirect_to new_search_usr_path(:status=> params[:status]),alert: responseMsg
        return
      end 

      Log.logger.info("SearchUsrController and search name is called adn user exist")
      @user=response.user
      render 'search_usr/show', notice: ''
    rescue => exception
      Log.logger.info("***********Exception is#{exception.message}****************")
      Log.logger.info("Exception is Search Usr controller's search_name method #{exception.backtrace.join("\n")}")
      redirect_to new_search_usr_path , alert: "We're sorry, but something went wrong"
    end  
  end  

  def show
  end 
end