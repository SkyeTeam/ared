# This class used for the Service Fees Setup
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 22 }
# :reek:DuplicateMethodCall { max_calls: 9 }

class ServiceFeesSetupController < ApplicationController

	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

  	def index
  		@serviceFeesList = ServiceFeesMaster.all.order("effective_date DESC")
  	end	

  	def new
  		params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"] = Constants::SERVICE_FEE_DEFAULT_SLABS
  		params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"] = Constants::SERVICE_FEE_DEFAULT_SLABS
  		params[:effectiveDate] = Date.today
  	end	

  	def create
  		begin
			Log.logger.info("At line #{__LINE__} inside the Service Fees Setup Controller and create method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
			
			if params[:userAction].to_i == Constants::SERVICE_FEE_SAVE_USER_ACTION
				response = ServiceFeesMaster.createServiceFee(loggedInAdmin, params)
				Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")
				responseMsg =  response.Response_Msg 

	 			if !ResponseCheck.isSuccessful(response)
		  	        DashboardManager.setDisplayMessage(responseMsg+"|false")
		  	        render :controller => 'service_fees_setup', :action => 'new'
				else
					DashboardManager.setDisplayMessage(responseMsg+"|true") 
					redirect_to '/ServiceFees'
			    end
			else
				ServiceFeesSlab.addServiceFeeRow(loggedInAdmin, params)
				render :controller => 'service_fees_setup', :action => 'new'
			end    
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the Service Fees Setup Controller and create method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/ServiceFees'
	    end
  	end	

  	def show
  		begin
			Log.logger.info("At line #{__LINE__} inside the Service Fees Setup Controller and show method is called")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
			Log.logger.info("At line #{__LINE__} and the details of the logged in admin are : name = #{loggedInAdmin.name}, email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
		
			response = ServiceFeesMaster.getServiceFee(params)
			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is = #{response.Response_Code}")
			
 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(response.Response_Msg.to_s + "|false")
	  	        redirect_to '/ServiceFees'
			else
				render :controller => 'service_fees_setup', :action => 'new'
		    end
		rescue => exception
	        	Log.logger.info("At line #{__LINE__} inside the Service Fees Setup Controller and show method is called and exception is = \n #{exception.message}")
	        	Log.logger.info("At line #{__LINE__} full exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/ServiceFees'
	    end
  	end	

  	private
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::SERVICE_FEE_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::SERVICE_FEE_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end
end
