# This class used for the  Questions
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class QuestionsController < ApplicationController
	layout 'header'
	
  	def show
  		@questions = Listquestion.find(params[:id])
  	end
end
