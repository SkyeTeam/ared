# This class used for the Penalty Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }

class PenaltyReportController < ApplicationController
	
	before_action :require_login
	layout 'header'

	def searchPenaltyDetails
		begin
			Log.logger.info("Penalty Report Controller's searchPenaltyDetails method is called at line = #{__LINE__}")
			
			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
    		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
    
			response = PenaltyReportManager.serachPenaltyBWdates(params)

			if !ResponseCheck.isSuccessful(response)
		          	responseMsg = response.Response_Msg
		          	DashboardManager.setDisplayMessage(responseMsg+"|false") 
		          	render '/txn_history/index'
	            return
	      	else
		      	  @penalty_list = response.penalty_list	
		      	  render '/penalty_report/show'
	          	return
	      	end 

        rescue => exception
	      		Log.logger.info("Inside the Penalty Report Controller's at line #{__LINE__} and method searchPenaltyDetails Exception is = \n #{exception.message}")
	        	Log.logger.info("Inside the Penalty Report Controller's at line #{__LINE__} and method searchPenaltyDetails Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/txn_history'
	    end    
	end	
end