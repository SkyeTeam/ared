# This class used for the Reset Password
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 19 }
# :reek:DuplicateMethodCall { max_calls: 6 }
# :reek:RepeatedConditional { max_ifs: 5 }

class ResetPasswordController < ApplicationController

	before_action :require_login
  before_action :checkServiceAccess
  layout 'header'

  def new
  end

  def create #This method search the user by phone to reset his password
    begin
      Log.logger.info("At line #{__LINE__} and Reset Password Controller's create method is called to search the user by phone")
      
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      phone = params[:phone] # get the phone number entered by the admin in the webpage to search the user
      Log.logger.info("At line #{__LINE__} phone = #{phone}")   
      
      response = ResetPasswordManager.searchByPhone(phone)

      # If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull
      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg
          DashboardManager.setDisplayMessage(responseMsg+"|false")
        redirect_to new_reset_password_path
      else
        # if the response code is 200 then render to the reset password page
          @user = response.User_Object 
        render new_reset_password_path
      end
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_reset_password_path
    end     
  end

  def search_name #This method search the user by name to reset his password
    begin
      Log.logger.info("At line #{__LINE__} and Reset Password Controller's search_name method is called to search the user by name")
      
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

      user_name = params[:name] # get the name entered by the admin in the webpage to search the user
      Log.logger.info("At line #{__LINE__} and Reset Password Controller's create method is called and name=#{user_name}")

      response = ResetPasswordManager.searchByName(user_name)
      # If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull
      if !ResponseCheck.isSuccessful(response)
          responseMsg =  response.Response_Msg 
          DashboardManager.setDisplayMessage(responseMsg+"|false")
        redirect_to new_reset_password_path
      else
        # if the response code is 200 then render to the reset password page
          @user = response.User_Object 
        render 'reset_password/show'
      end 
    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_reset_password_path
    end
  end  

  #This method reset the password of User
  def reset
    begin
      Log.logger.info("At line #{__LINE__} and Reset Password Controller's reset method is called to reset the password of the user")
      
      loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
      Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
      
      response = ResetPasswordManager.resetUserPassword(loggedInAdmin, params)
      responseMsg =  response.Response_Msg 

      # If the response come from the manager class is not nill them we check the response code if the response code come 200 then it is successfull
      if !ResponseCheck.isSuccessful(response)
          DashboardManager.setDisplayMessage(responseMsg+"|false")
      else
          DashboardManager.setDisplayMessage(responseMsg+"|true")
      end

      redirect_to new_reset_password_path

    rescue => exception
        Log.logger.info("At line = #{__LINE__} and exception message is = \n #{exception.message}")
        Log.logger.info("At line = #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
      redirect_to new_reset_password_path
    end 
  end
  
  def show
  end 

  private
    def checkServiceAccess
      hasAccess = Utility.hasServiceAccess(Constants::RESET_PASSWORD_ROLE,current_user.id)
      Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::RESET_PASSWORD_ROLE} service or not = #{hasAccess}")
      if hasAccess
        return true
      else
        DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
        redirect_to "/Dashboard"
        return
      end
    end

end