module Mshiriki
	
	class SymboxController < ApplicationController

		skip_before_action :check_session_expiry

		def creditAgent
			
			begin
				
				SymboxLog.logger.info("At line #{__LINE__} inside the SymboxController class and creditAgent method is called")

				serverResponse = SymboxManager.creditAgentAccount(params, request)

				if !ResponseCheck.isSuccessful(serverResponse)
			    	render json: {

			    		""+AppConstants::RESPONSE_ERROR_CODE_KEY_LABEL+"" => serverResponse.Response_Code, 
			    		""+AppConstants::RESPONSE_ERROR_MESSAGE_KEY_LABEL+"" => serverResponse.Response_Msg
			    	
			    	}
		        else
		        	response.headers['ResponseIntegrity'] = serverResponse.ResponseIntegrity
		            render :json => serverResponse.Response_Msg.gsub('\\"', '')
		        end

			rescue => exception
		          
		          	SymboxLog.logger.info("At line #{__LINE__} and exception message is = \n #{exception.message}")
		          	SymboxLog.logger.info("At line #{__LINE__} and full exception details are = \n #{exception.backtrace.join("\n")}")
		        render json: {
		          	""+AppConstants::RESPONSE_ERROR_CODE_KEY_LABEL+"" => Constants::INTERNAL_SERVER_ERROR, 
		          	""+AppConstants::RESPONSE_ERROR_MESSAGE_KEY_LABEL+"" => exception.message
		        }

      		end
		
		end	

	end

end