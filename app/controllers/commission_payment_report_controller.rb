# This class used for the Commission Payment Report
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class CommissionPaymentReportController < ApplicationController

	before_action :require_login
	layout 'header'

	# :reek:TooManyStatements: { max_statements: 19 }
  	# :reek:DuplicateMethodCall { max_calls: 6 }
	def searchCommissionPaymentDetails
		begin
			Log.logger.info("Commission Payment Report Controller's searchCommissionPaymentDetails method is called at line = #{__LINE__}")

			loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
     		Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")

			response = CommissionManager.serachCommPayment(params)

			if !ResponseCheck.isSuccessful(response)
				  	responseMsg = response.Response_Msg
		          	DashboardManager.setDisplayMessage(responseMsg+"|false") 
		          	render '/txn_history/index'
	            return
	      	else
		      	  @profitLoseList = response.commPaidMonthlyList
		      	  render '/commission_payment_report/show'
	          return
	      	end  	
	    rescue => exception
	     		Log.logger.info("Inside the Commission Payment Report Controller's at line #{__LINE__} and method searchCommissionPaymentDetails Exception is = \n #{exception.message}")
	        	Log.logger.info("Inside the Commission Payment Report Controller's at line #{__LINE__} and method searchCommissionPaymentDetails Full Exception is = \n #{exception.backtrace.join("\n")}")
	      	DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      	redirect_to '/txn_history'
	    end    	
	end	
end