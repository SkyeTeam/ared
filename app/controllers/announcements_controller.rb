# This class used for the Announcements
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }

class AnnouncementsController < ApplicationController
	before_action :require_login
  	before_action :checkServiceAccess
  	layout 'header'

	def index
		@announcement =  Announcement.all.order('txn_date DESC')
	end	

	# :reek:TooManyStatements: { max_statements: 16 }
	# :reek:DuplicateMethodCall { max_calls: 6 }
	def createAnnouncement
		begin

			Log.logger.info("Inside the Announcement Controller's at line #{__LINE__} and method createAnnouncement is called")
      		
      		loggedInAdmin = Utility.getLoggedInAdminDetails(current_user)
 		    Log.logger.info("The details of the logged in admin are : name = #{loggedInAdmin.name} , email = #{loggedInAdmin.email} and phone = #{loggedInAdmin.phone}")
 	
 			response = Announcement.createAnnouncement(params, current_user)

			Log.logger.info("At Line #{__LINE__} and response come from the manager class with response code is  = #{response.Response_Code}")

			responseMsg =  response.Response_Msg 

 			if !ResponseCheck.isSuccessful(response)
	  	        DashboardManager.setDisplayMessage(responseMsg+"|false") 
			else
				DashboardManager.setDisplayMessage(responseMsg+"|true") 
		    end

     		redirect_to '/Announcements'

	    rescue => exception
	        Log.logger.info("Inside the Announcement Controller's at line #{__LINE__} and method createAnnouncement Exception is = \n #{exception.message}")
	        Log.logger.info("Inside the Announcement Controller's at line #{__LINE__} and method createAnnouncement Full Exception is = \n #{exception.backtrace.join("\n")}")
	      DashboardManager.setDisplayMessage((I18n.t :EXCEPTION_MSG).to_s + "|false") 
	      redirect_to '/Announcements'
	    end
	end	

	private
	# :reek:TooManyStatements: { max_statements: 7 }
  	def checkServiceAccess
	    hasAccess = Utility.hasServiceAccess(Constants::ANNOUNCEMENTS_ROLE,current_user.id)
	    Log.logger.info("At line #{__LINE__}  and check user has access of #{Constants::ANNOUNCEMENTS_ROLE} service or not = #{hasAccess}")
	    if hasAccess
	      return true
	    else
	      DashboardManager.setDisplayMessage((I18n.t :INVALID_ACCESS_MSG).to_s + "|false")
	      redirect_to "/Dashboard"
	      return
	    end
  	end

end
