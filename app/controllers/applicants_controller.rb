# This class used for the Applicants
class ApplicantsController < ApplicationController
	
  layout 'header'
 
  def show
    @applicants = Applicant.all
  end

  def list
 		@userdetails = Applicant.find(params[:id])
  end

  def edit
 		@edituserdetails = Applicant.find(params[:id])
  end

end