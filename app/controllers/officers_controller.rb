class OfficersController < ApplicationController
  layout 'header'
  before_action :set_officer, only: [:show, :edit, :update, :destroy,:deploy,:deploy_officer,:suspend,:terminate,:reactivate,:password_reset,:reviews]

 
  set_tab :new_officer,only: [:index]
  set_tab :deployed ,only: [:deployed]
  set_tab :suspended,only: [:suspended]
  set_tab :terminated ,only: [:terminated]


  
  # GET /officers
  # GET /officers.json
  def index
    @officers = Officer.where(state: 'new').order(created_at: :desc)
  end

  def deployed 
     @officers = Officer.where(state: 'deployed').order(created_at: :desc)
  end

  def suspended 
     @officers = Officer.where(state: 'suspended').order(created_at: :desc)
  end

  def terminated 
     @officers = Officer.where(state: 'terminated').order(created_at: :desc)
  end

  def listings_districts
    country = Country.find(params[:id])
    @districts = country.districts
    respond_to do |format|
       format.js 
    end
  end

  def listings_towns
    district = District.find(params[:id])
    @towns = district.towns
    respond_to do |format|
       format.js 
    end
  end

  def deploy 
  end

  def deploy_officer
    deployment = @officer.deployments.new 
    deployment.country_id = params[:country]
    deployment.district_id = params[:district]
    deployment.town_id = params[:town]

    @officer.state = "deployed"
    @officer.save 
    deployment.save 
    flash[:notice] = "Officer has been deployed."
    OfficerMailer.send_notification(@officer).deliver_now!

    redirect_to deployed_officers_path 
  end

   def suspend
    @officer.suspend!
    OfficerMailer.suspended(@officer).deliver_now!
    flash[:notice] = "Officer has been suspended."
    redirect_to suspended_officers_path
  end

   def terminate
    @officer.state = "terminated"
    @officer.save 
     flash[:notice] = "Officer has been terminated."
    redirect_to terminated_officers_path
  end

  def reactivate
    @officer.state = "deployed"
    @officer.save 
    flash[:notice] = "Officer has been re-activated."
    OfficerMailer.send_notification(@officer).deliver_now!
    redirect_to deployed_officers_path 
  end

  def password_reset 
    @officer.password = "123456"
    @officer.save 
    flash[:notice] = "Officer password has been reset to 123456"
    redirect_to @officer
  end

 
  # GET /officers/1.json
  def show
    ids = @officer.reviews.pluck(:kiosk_master_id)
    @kiosksList = KioskMaster.order("id ASC").where(id: ids)
  end


  def reviews
    @reviews = @officer.reviews
  end

  # GET /officers/new
  def new
    @officer = Officer.new
  end

  # GET /officers/1/edit
  def edit
  end

  # POST /officers
  # POST /officers.json
  def create
    @officer = Officer.new(officer_params)

    respond_to do |format|
      if @officer.save
        format.html { redirect_to officers_url, notice: 'Officer was successfully created.' }
        format.json { render :show, status: :created, location: @officer }
      else
        format.html { render :new }
        format.json { render json: @officer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /officers/1
  # PATCH/PUT /officers/1.json
  def update
    respond_to do |format|
      if @officer.update(officer_params)
        format.html { redirect_to @officer, notice: 'Officer was successfully updated.' }
        format.json { render :show, status: :ok, location: @officer }
      else
        format.html { render :edit }
        format.json { render json: @officer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /officers/1
  # DELETE /officers/1.json
  def destroy
    @officer.destroy
    respond_to do |format|
      format.html { redirect_to officers_url, notice: 'Officer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_officer
      @officer = Officer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def officer_params
      params.require(:officer).permit(:email, :phone, :account, :password, :photo,:first_name,:last_name,:middle_name,:photo,:birth,:gender,:country_id, :district_id, :town_id)
    end
end


