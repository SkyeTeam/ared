class MaintenanceOfficersController < ApplicationController
  layout 'header'
  before_action :set_maintenance_officer, only: [:show, :edit, :update, :destroy,:deploy,:deploy_maintenance_officer,:suspend,:terminate,:reactivate,:password_reset]

 
  set_tab :new_maintenance_officer,only: [:index]
  set_tab :deployed ,only: [:deployed]
  set_tab :suspended,only: [:suspended]
  set_tab :terminated ,only: [:terminated]


  
  # GET /Maintenance_officers
  # GET /Maintenance_officers.json
  def index
    @maintenance_officers = MaintenanceOfficer.where(state: 'new').order(created_at: :desc)
  end

  def deployed 
     @maintenance_officers = MaintenanceOfficer.where(state: 'deployed').order(created_at: :desc)
  end

  def suspended 
     @maintenance_officers = MaintenanceOfficer.where(state: 'suspended').order(created_at: :desc)
  end

  def terminated 
     @maintenance_officers = MaintenanceOfficer.where(state: 'terminated').order(created_at: :desc)
  end


  def deploy 
  end

  def deploy_maintenance_officer
    deployment = @maintenance_officer.deployments.new 
    deployment.country_id = params[:country]
    deployment.district_id = params[:district]
    deployment.town_id = params[:town]

    @maintenance_officer.state = "deployed"
    @maintenance_officer.save 
    deployment.save 
    flash[:notice] = "Maintenance officer has been deployed."
    #MaintenanceOfficerMailer.send_notification(@maintenance_officer).deliver_now!

    redirect_to deployed_maintenance_officers_path 
  end

   def suspend
    @maintenance_officer.suspend!
    #MaintenanceOfficerMailer.suspended(@maintenance_officer).deliver_now!
    flash[:notice] = "Maintenance officer has been suspended."
    redirect_to suspended_maintenance_officers_path
  end

   def terminate
    @maintenance_officer.state = "terminated"
    @maintenance_officer.save 
     flash[:notice] = "Maintenance officer has been terminated."
    redirect_to terminated_maintenance_officers_path
  end

  def reactivate
    @maintenance_officer.state = "deployed"
    @maintenance_officer.save 
    flash[:notice] = "maintenance_Officer has been re-activated."
    #MaintenanceOfficerMailer.send_notification(@maintenance_officer).deliver_now!
    redirect_to deployed_maintenance_officers_path 
  end

  def password_reset 
    @maintenance_officer.password = "123456"
    @maintenance_officer.save 
    flash[:notice] = "maintenance_Officer password has been reset to 123456"
    redirect_to @maintenance_officer
  end

 
  # GET /Maintenance_officers/1.json
  def show
  end

  # GET /Maintenance_officers/new
  def new
    @maintenance_officer = MaintenanceOfficer.new
  end

  # GET /Maintenance_officers/1/edit
  def edit
  end

  # POST /Maintenance_officers
  # POST /Maintenance_officers.json
  def create
    @maintenance_officer = MaintenanceOfficer.new(maintenance_officer_params)

    respond_to do |format|
      if @maintenance_officer.save
        format.html { redirect_to maintenance_officers_url, notice: 'Maintenance Officer was successfully created.' }
        format.json { render :show, status: :created, location: @maintenance_officer }
      else
        format.html { render :new }
        format.json { render json: @maintenance_officer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /Maintenance_officers/1
  # PATCH/PUT /Maintenance_officers/1.json
  def update
    respond_to do |format|
      if @maintenance_officer.update(maintenance_officer_params)
        format.html { redirect_to @maintenance_officer, notice: 'Maintenance officer was successfully updated.' }
        format.json { render :show, status: :ok, location: @maintenance_officer }
      else
        format.html { render :edit }
        format.json { render json: @maintenance_officer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /Maintenance_officers/1
  # DELETE /Maintenance_officers/1.json
  def destroy
    @maintenance_officer.destroy
    respond_to do |format|
      format.html { redirect_to maintenance_officers_url, notice: 'Maintenance officer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_maintenance_officer
      @maintenance_officer = MaintenanceOfficer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maintenance_officer_params
      params.require(:maintenance_officer).permit(:email, :phone, :account, :password, :photo,:first_name,:last_name,:middle_name,:photo,:birth,:gender,:country_id, :district_id, :town_id)
    end
end


