# encoding: utf-8

class FirmwareUploader < CarrierWave::Uploader::Base

  storage :file

  def extension_white_list
    %w(gz img img.gz)
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

end
