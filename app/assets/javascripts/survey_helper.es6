/* global $ */
export default class SurveyHelper {

  constructor() {
    // take care of new questions and answers
  }

  initialize ($el) {
    this.$el = $el;

    // elements
    this.newQuestionTrigger = this.$el.find('.js-new-question');

    // listener
    this.newQuestionTrigger.on('click', this.observeNewQuestion.bind(this));
    this.$el.on('click', '.js-new-answer', this.insertNewAnswer);
  }

  observeNewQuestion () {
    $.post('/surveys/new_question', { index: $('.js-question-card').length })
      .done((data) => {
        $('.js-questions').append(data.html)
      }).error(function() {
        alert('Sorry - Something went wrong!')
      })
  }

  insertNewAnswer () {
    const index = parseInt($(this).data('question-index'), 10)
    const answers = $('.js-answers').eq(index)

    $.post('/surveys/new_answer', { index: index, answersCount: answers.find('input').length  })
      .done((data) => {
        answers.append(data.html)
      }).error(function() {
        alert('Sorry - Something went wrong!')
      })
  }
}
