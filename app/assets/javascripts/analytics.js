$(document).ready(function(){
    
    var currentDate = new Date();

  	var reportTypeValue = $("select[name=reportType]").val();
  	manageForms(reportTypeValue);
  
  	var serviceTypeValue = $("select[name=serviceTypeCommReport]").val();
  	manageSubType(serviceTypeValue);

    var userTypeCommReport = $("select[name=userTypeCommReport]").val();
    managerCommFormPhoneDiv(userTypeCommReport);

    var creditReportFormUType = $("select[name=userType]").val();
    managerCreditFormPhoneDiv(creditReportFormUType);

    var serviceTypeValueRev = $("select[name=serviceTypeRevReport]").val();
    manageSubTypeRev(serviceTypeValueRev);


    $("select[name=reportType]").change(function() {
      $("#monthCommReport").val(getMonthList()[currentDate.getMonth()]);
      $("#yearCommReport").val(currentDate.getFullYear());
      $("#monthRevReport").val(getMonthList()[currentDate.getMonth()]);
      $("#yearRevReport").val(currentDate.getFullYear());
      manageForms($(this).val());
    });

    $("select[name=serviceTypeCommReport]").change(function() {
    	manageSubType($(this).val());
    });

    $("select[name=userTypeCommReport]").change(function() {
    	managerCommFormPhoneDiv($(this).val());
    });

    $("select[name=userType]").change(function() {
      managerCreditFormPhoneDiv($(this).val());
    });

    $("select[name=serviceTypeRevReport]").change(function() {
      manageSubTypeRev($(this).val());
    });
    
});



function manageForms(reportType){

	if (reportType == revenueReportLabel) { //This  variable(revenueReportLabel) defined in the layouts/header file
	      	
      	$("#revenueReportForm").show();
      	$("#commReportForm").hide();
       	$("#creditReportForm").hide();
    
    } else if(reportType == creditReportLabel) { //This  variable(creditReportLabel) defined in the layouts/header file
      	
      	$("#revenueReportForm").hide();
       	$("#creditReportForm").show();
        $("#commReportForm").hide();

    } else{
      	
      	$("#revenueReportForm").hide();
       	$("#commReportForm").show();
        $("#creditReportForm").hide();

    }

}

function managerCreditFormPhoneDiv(userType){
	if(userType == singleUserType){ //This is used for the Single User and variable(singleUserType) defined in the layouts/header file
    $("#phoneDiv").removeAttr('disabled');
    $('input[name=phoneCreditReport]').prop('required', 'true');
  } else {
    $("#phoneDiv").prop('disabled', 'true');
    $('input[name=phoneCreditReport]').removeAttr('required');
  }
}

function managerCommFormPhoneDiv(userType){
	if(userType == singleUserType){ //This is used for the Single User and variable(singleUserType) defined in the layouts/header file
    $("#phoneDivCommReport").removeAttr('disabled');
    $('input[name=phoneCommReport]').prop('required', 'true');
  } else {
    $("#phoneDivCommReport").prop('disabled', 'true');
    $('input[name=phoneCommReport]').removeAttr('required');
  }
}

function manageSubType(serviceType){
  if (serviceType == airtimeLabel ){//This  variable(airtimeLabel) defined in the layouts/header file
    $("#subTypeDiv").removeAttr('disabled');
	} else {
    $("#subTypeDiv").prop('disabled', 'true');
	}
}

function manageSubTypeRev(serviceType){
  if (serviceType == airtimeLabel ){//This  variable(airtimeLabel) defined in the layouts/header file
    $("#subTypeRevDiv").removeAttr('disabled');
  } else {
    $("#subTypeRevDiv").prop('disabled', 'true');
  }
}


function validateForm(formID){
	if(!formValidation(formID)) return false;
}

function getMonthList(){
  var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
  return monthNames;
}
