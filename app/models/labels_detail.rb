class LabelsDetail

	def self.subTypeViewModuleFirst(txnObj)

		if txnObj["sub_type"].eql? Constants::USER_LOAN_REQUEST_LABLE
    
	      	return Constants::USER_LOAN_REQUEST_VIEW_LABLE
	    
	    elsif txnObj["sub_type"].eql? Constants::USER_CASH_DEPOSIT_LABLE
	    
	      	return Constants::USER_CASH_DEPOSIT_VIEW_LABLE
	    
	    elsif txnObj["sub_type"].eql? Constants::USER_LOAN_DEPOSIT_LABLE
	     
	      	return Constants::USER_LOAN_DEPOSIT_VIEW_LABLE
	    
	    elsif txnObj["sub_type"].eql? Constants::MTOPUP_LABEL
	    
	      	return Constants::AIRTIME_LABEL
	    
	    elsif txnObj["sub_type"].eql? Constants::USER_PENALTY_CHARGE_LABLE
	    
	      	return Constants::USER_PENALTY_CHARGE_VIEW_LABLE

	    elsif txnObj["sub_type"].eql? Constants::USER_CASH_PAYMENT_LABLE
	    
	      	return Constants::USER_CASH_PAYMENT_VIEW_LABLE
	    
	    elsif txnObj["sub_type"].eql? Constants::USER_COMMISSION_PAYMENT_LABLE
	    
	      	return Constants::USER_COM_PAYMENT_VIEW_LABLE
	    
	    else

	      	return subTypeViewModuleSecond(txnObj)
	    
	    end

	end

	def self.subTypeViewModuleSecond(txnObj)

	    if txnObj["sub_type"].eql? Constants::TAX_DEDUCT_LABEL
	    
	      	return Constants::TAX_DEDUCT_VIEW_LABEL
	    
	    elsif txnObj["sub_type"].eql? Constants::USER_NON_PAYMENT_PENALTY_LABLE
	    
	      	return Constants::USER_NON_PAYMENT_PENALTY_VIEW_LABLE
	    
	    elsif txnObj["sub_type"].eql? Constants::USER_NON_PAYMENT_LOAN_LABLE
	    
	      	return Constants::USER_NON_PAYMENT_LOAN_VIEW_LABLE
	    
	    elsif txnObj["sub_type"].eql? Constants::TXN_ROLLBACK_DB_LABEL
	        
	      	return Constants::TXN_ROLLBACK_VIEW_LABEL + " (" + txnObj["external_reference"] + "/" + Utility.checkRollBackTxnStatus(txnObj["external_reference"]).to_s +  ")"
	    
	    elsif txnObj["sub_type"].eql? Constants::LEASING_FEE_DB_LABEL
	        
	      	return Constants::LEASING_FEE_VIEW_LABEL

	    elsif txnObj["sub_type"].eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL
	        
	      	return UgandaConstants::NATIONAL_WATER_BILL_VIEW_LABEL   

	    elsif txnObj["sub_type"].eql? Constants::USER_GOVT_CASH_DEPOSIT_LABLE
	        
	      	return Constants::USER_GOVT_CASH_DEPOSIT_VIEW_LABLE  

	    else

	      	return subTypeViewModuleThird(txnObj)

	    end  

  	end  

	def self.subTypeViewModuleThird(txnObj)
	    
	    if txnObj["sub_type"].eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
	        
	      	return Constants::IREMBO_TAX_PAYMENT_VIEW_LABEL  

	    elsif txnObj["sub_type"].eql? Constants::GOVT_FUNDS_WITHDRAWAL_DB_LABEL
	        
	      	return Constants::GOVT_FUNDS_WITHDRAWAL_VIEW_LABEL  

	    elsif txnObj["sub_type"].eql? Constants::FDI_SUB_TYPE_DB_LABEL or txnObj["sub_type"].eql? Constants::MTN_SUB_TYPE_DB_LABEL or txnObj["sub_type"].eql? Constants::AIRTEL_SUB_TYPE_DB_LABEL or txnObj["sub_type"].eql? Constants::TAX_PAYMENT_SUB_TYPE_DB_LABEL or txnObj["sub_type"].eql? UgandaConstants::QUICK_TELLER_SUB_TYPE_DB_LABEL or txnObj["sub_type"].eql? UgandaConstants::PAYWAY_SUB_TYPE_DB_LABEL
	      
	      	if txnObj["service_type"].eql? Constants::COMMISSION_PAID_SERVICE_TYPE_DB_LABEL

	        	return Constants::COMMISSION_PAID_SERVICE_TYPE_VIEW_LABEL

	      	elsif txnObj["service_type"].eql? Constants::COMMISSION_RECEIVABLE_SERVICE_TYPE_DB_LABEL
	        
	        	return Constants::COMMISSION_RECEIVABLE_SERVICE_TYPE_VIEW_LABEL

	      	end  

	    elsif txnObj["sub_type"].eql? Constants::AGENT_VAT_DB_LABEL

	      	return Constants::AGENT_VAT_VIEW_LABEL
	    
	    elsif txnObj["sub_type"].eql? Constants::OPT_VAT_DB_LABEL
	          
	      	return Constants::OPT_VAT_VIEW_LABEL

	    else
	    
	      	return subTypeViewModuleFourth(txnObj)
	    
	    end 
	 
	end   

	def self.subTypeViewModuleFourth(txnObj)

	    if txnObj["sub_type"].eql? UgandaConstants::QUICK_TELLER_AGGREGATOR

	      	if txnObj["service_type"].eql? Constants::ELECTRICITY_SALE_LABEL

	        	return Constants::ELECTRICITY_SALE_VIEW_LABEL

	      	elsif txnObj["service_type"].eql? UgandaConstants::NATIONAL_WATER_BILL_SALES_LABEL

	        	return UgandaConstants::NATIONAL_WATER_BILL_SALES_VIEW_LABEL

	      	end  

	    
	    elsif txnObj["sub_type"].eql? Constants::FDI_AGGREGATOR

	    	if txnObj["service_type"].eql? Constants::DTH_SALE_LABEL

	    		return Constants::DTH_SALE_VIEW_LABEL

	    	elsif txnObj["service_type"].eql? Constants::ELECTRICITY_SALE_LABEL

	    		return Constants::ELECTRICITY_SALE_VIEW_LABEL

	    	elsif txnObj["service_type"].eql? Constants::MTOP_UP_SALES_LABEL
	    	
	    		return 	Constants::MTOP_UP_SALES_VIEW_LABEL

	    	end	

	    elsif txnObj["sub_type"].eql? Constants::AGGREGATOR_DEPOSIT_DB_LABEL

	    	return Constants::AGGREGATOR_DEPOSIT_VIEW_LABEL	

	    
	    elsif txnObj["sub_type"].eql? Constants::SYMBOX_AIRTEL_WALLET_DEPOSIT_DB_LABEL

	    	return Constants::SYMBOX_AIRTEL_WALLET_DEPOSIT_VIEW_LABEL	

	    elsif txnObj["sub_type"].eql? Constants::SYMBOX_MTN_WALLET_DEPOSIT_DB_LABEL
	    
	    	return Constants::SYMBOX_MTN_WALLET_DEPOSIT_VIEW_LABEL	

	    else
	  
	      	return txnObj["sub_type"]
	    
	    end 

	end

end	