class ServiceFeesSlab < ApplicationRecord

	def self.saveServiceFeeSlabs(serviceFeesMasterID, commPayer, minAmount, maxAmount, commType, totalComm, ourComm, agentComm, aggregatorComm, sequence)
		Log.logger.info("At line #{__LINE__} Inside the ServiceFeesSlab class and parameters are as minAmount = #{minAmount}, maxAmount = #{maxAmount}, commType = #{commType}, totalComm = #{totalComm}, ARED comm = #{ourComm} and agent comm = #{agentComm}")
		serviceFeesSlab	= ServiceFeesSlab.new
			serviceFeesSlab.master_id = serviceFeesMasterID
			serviceFeesSlab.comm_payer = commPayer
			serviceFeesSlab.min_amount = minAmount
			serviceFeesSlab.max_amount = maxAmount
			serviceFeesSlab.comm_type = commType
			serviceFeesSlab.total_comm = totalComm
			serviceFeesSlab.our_comm = ourComm
			serviceFeesSlab.agent_comm = agentComm
			serviceFeesSlab.aggregator_comm = aggregatorComm
			serviceFeesSlab.sequence = sequence
		serviceFeesSlab.save	
	end	

	def self.updateServiceFeeSlabs(serviceFeesMasterID, commPayer, minAmount, maxAmount, commType, totalComm, ourComm, agentComm, aggregatorComm, sequence)
		Log.logger.info("At line #{__LINE__} Inside the ServiceFeesSlab class to updateServiceFeeSlabs and parameters are as minAmount = #{minAmount}, maxAmount = #{maxAmount}, commType = #{commType}, totalComm = #{totalComm}, ARED comm = #{ourComm}, agent comm = #{agentComm} and sequence = #{sequence}")
		serviceFeesSlab = ServiceFeesSlab.where("master_id = #{serviceFeesMasterID} AND sequence = #{sequence}").first
		if serviceFeesSlab.present?
			serviceFeesSlab.update_attributes(:comm_payer => commPayer, :min_amount => minAmount, :max_amount => maxAmount, :comm_type => commType, :total_comm => totalComm, :our_comm => ourComm, :agent_comm => agentComm, :aggregator_comm => aggregatorComm)
		else
			saveServiceFeeSlabs(serviceFeesMasterID, commPayer, minAmount, maxAmount, commType, totalComm, ourComm, agentComm, sequence)
		end	
	end	

	def self.getServiceFeeSlabs(params, serviceFeesMasterID)
		serviceFeesSlabs = ServiceFeesSlab.where("master_id = #{serviceFeesMasterID}").order("sequence ASC")
		if serviceFeesSlabs.present?
			
			aggregatorSlabNumber = 0
			endUserSlabNumber = 0

			serviceFeesSlabs.each do |slab|

				if slab.comm_payer.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL
					
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"MinAmount"+aggregatorSlabNumber.to_s] = TxnHeader.my_money(slab.min_amount)
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"MaxAmount"+aggregatorSlabNumber.to_s] = TxnHeader.my_money(slab.max_amount)
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"CommType"+aggregatorSlabNumber.to_s] = slab.comm_type
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"TotalComm"+aggregatorSlabNumber.to_s] = TxnHeader.my_money(slab.total_comm)
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"OurComm"+aggregatorSlabNumber.to_s] = TxnHeader.my_money(slab.our_comm)
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"AgentComm"+aggregatorSlabNumber.to_s] = TxnHeader.my_money(slab.agent_comm)
					params[Constants::AGGREGATOR_HTML_NAME_LABEL+"AggregatorComm"+aggregatorSlabNumber.to_s] = TxnHeader.my_money(slab.aggregator_comm)
					aggregatorSlabNumber = aggregatorSlabNumber.to_i + 1

				else

					params[Constants::END_USER_HTML_NAME_LABEL+"MinAmount"+endUserSlabNumber.to_s] = TxnHeader.my_money(slab.min_amount)
					params[Constants::END_USER_HTML_NAME_LABEL+"MaxAmount"+endUserSlabNumber.to_s] = TxnHeader.my_money(slab.max_amount)
					params[Constants::END_USER_HTML_NAME_LABEL+"CommType"+endUserSlabNumber.to_s] = slab.comm_type
					params[Constants::END_USER_HTML_NAME_LABEL+"TotalComm"+endUserSlabNumber.to_s] = TxnHeader.my_money(slab.total_comm)
					params[Constants::END_USER_HTML_NAME_LABEL+"OurComm"+endUserSlabNumber.to_s] = TxnHeader.my_money(slab.our_comm)
					params[Constants::END_USER_HTML_NAME_LABEL+"AgentComm"+endUserSlabNumber.to_s] = TxnHeader.my_money(slab.agent_comm)
					params[Constants::END_USER_HTML_NAME_LABEL+"AggregatorComm"+endUserSlabNumber.to_s] = TxnHeader.my_money(slab.aggregator_comm)
					endUserSlabNumber = endUserSlabNumber.to_i + 1

				end	

			end	

			params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"] = aggregatorSlabNumber
			params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"] = endUserSlabNumber
		end	
	end	

	def self.addServiceFeeRow(loggedInAdmin, params)
		if params[:userAction].to_i == Constants::SERVICE_FEE_AGG_ADD_ROW_USER_ACTION 
			params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"] = params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"].to_i + 1
		elsif params[:userAction].to_i == Constants::SERVICE_FEE_END_ADD_ROW_USER_ACTION 
	  		params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"] = params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"].to_i + 1
	  	end	
 	end	
		
end