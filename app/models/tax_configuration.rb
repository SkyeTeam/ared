class TaxConfiguration < ApplicationRecord

	def self.getApplicableTaxPercentage
		AppLog.logger.info("At line #{__LINE__} inside the TaxConfiguration class and getApplicableTaxPercentage method is called")
		applicableTaxPercentage = 0

			taxConfiguration = TaxConfiguration.where("service = '#{Constants::TAX_LABEL}' AND effective_date <= '#{Time.now}'").order('effective_date ASC').last
			if taxConfiguration.present?
				applicableTaxPercentage = taxConfiguration.percentage
			end	
			AppLog.logger.info("At line #{__LINE__} and applicable VAT Percentage is = #{applicableTaxPercentage}")

		return applicableTaxPercentage
	end	

	def self.calculateVATOnAmount(amount)
		return TxnHeader.my_money((TxnHeader.my_money(amount).to_f * TxnHeader.my_money(getApplicableTaxPercentage).to_f)/100).to_f
	end	

end
