class SurveyResult < ApplicationRecord
  belongs_to :survey
  belongs_to :kiosk

  has_many :survey_result_answers
  has_many :answers, :through => :survey_result_answers, dependent: :destroy
end
