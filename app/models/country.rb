class Country < ApplicationRecord
	has_many :districts, :dependent => :destroy
    accepts_nested_attributes_for :districts, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true

    has_many :officers 
end
