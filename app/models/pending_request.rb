class PendingRequest < ApplicationRecord

	def self.checkStatus(agent, params)

		AppLog.logger.info("Inside the Mobile Recharges Manager and doMobileRecharge method is called at line #{__LINE__}")

		appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]

        AppLog.logger.info("At line = #{__LINE__} and parameters are txnID = #{appTxnUniqueID}")
       
        response = validate(agent, appTxnUniqueID)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end

        pendingRequest = getTxnIDDetails(appTxnUniqueID)

        unless pendingRequest.present?
        		AppLog.logger.info("Inside the PendingRequest's  checkStatus method at line #{__LINE__} and txnId is not exist in the database")
                response.Response_Code=(Error::INVALID_TXN_ID)
                response.Response_Msg=(I18n.t :INVALID_TXN_ID_MSG) 
            return response
        end
       	
       	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=(pendingRequest.status) 
      	return response  

	end	


	def self.getTxnIDDetails(txnID)
		
			AppLog.logger.info("Inside the PendingRequest's  checkTxnIDExist method at line #{__LINE__} and check the txnId = #{txnID}")

			pendingRequest = PendingRequest.where('txn_id = ?',txnID).first

		return pendingRequest	

	end	


	def self.validate(currentAgent, txnID)
      
        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("Inside the PendingRequest's  validate method at line #{__LINE__} and agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end

            if txnID.eql? Constants::BLANK_VALUE
                    AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
        	end   

        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=("") 
      	return response  

    end

end
