class VatManager

	def self.getAgentTotalVAT(agentID, vatStatus)
		AppLog.logger.info("At line #{__LINE__} inside the VatManager class and getAgentTotalVAT method is called with agentID = #{agentID}")

		agentTotalVAT = TxnHeader.where("agent_id::INT = #{agentID} AND vat_status = #{vatStatus}").sum("agent_vat")

		return agentTotalVAT
	end	

	def self.getAREDTotalVAT(vatStatus)
		AppLog.logger.info("At line #{__LINE__} inside the VatManager class and getOperatorTotalVAT method is called")

		aredTotalVAT = TxnHeader.where("vat_status = #{vatStatus}").sum("ared_vat")

		return aredTotalVAT
	end	

	def self.getAgentsList
		agentsIDList = Array.new
			AppLog.logger.info("At line #{__LINE__} inside the VatManager class and getAgentsList method is called to get the list of agent whose VAT will deduct")
			txnHeader = TxnHeader.select("distinct(agent_id)").where("vat_status IN (#{Constants::DEDUCT_FROM_AGENT_VAT_STATUS}, #{Constants::ROLLBACK_TO_AGENT_VAT_STATUS})")
			if txnHeader.present?
				txnHeader.each do |header|
					agentsIDList.push(header.agent_id)
				end	
			end	
			AppLog.logger.info("At line #{__LINE__} and the agents list is #{agentsIDList}")
		return agentsIDList
	end	

	def self.getVATHeaders(agentID, vatStatus)
			txnHeader = TxnHeader.where("agent_id::INT = #{agentID} AND vat_status = #{vatStatus}")
		return txnHeader	
	end	

	def self.deductAgentsVAT
		begin
			agentsIDList = getAgentsList
			if agentsIDList.size.to_i > 0
				ActiveRecord::Base.transaction do
					for i in 0..agentsIDList.size.to_i - 1
						agentTotalVATPending = getAgentTotalVAT(agentsIDList[i], Constants::DEDUCT_FROM_AGENT_VAT_STATUS)
						AppLog.logger.info("At line #{__LINE__} total VAT is pending to Pay of the agent is = #{agentTotalVATPending}")

						agentTotalVATRollback = getAgentTotalVAT(agentsIDList[i], Constants::ROLLBACK_TO_AGENT_VAT_STATUS)
						AppLog.logger.info("At line #{__LINE__} total VAT is pending to Rollback of the agent is = #{agentTotalVATRollback}")

						calculatedVATAmount = TxnHeader.my_money(TxnHeader.my_money(agentTotalVATPending).to_f - TxnHeader.my_money(agentTotalVATRollback).to_f).to_f
						AppLog.logger.info("At line #{__LINE__} calculated VAT amount is = #{calculatedVATAmount}")

						if calculatedVATAmount > 0
							deductVATFromAgent(agentsIDList[i], calculatedVATAmount)
						else
							if calculatedVATAmount != 0
								creditVATToAgent(agentsIDList[i], -(calculatedVATAmount))
							end	
						end	

						updateDeductVATFromOptStatus(agentsIDList[i])
	   					AppLog.logger.info("At line #{__LINE__} and successfuly update the status of all the header to 2(DEDUCT_FROM_OPT_VAT_STATUS) whose VAT is paid")

	   					updateRollbackVATToOptStatus(agentsIDList[i])
	   					AppLog.logger.info("At line #{__LINE__} and successfuly update the status of all the header to 4(ROLLBACK_TO_OPT_VAT_STATUS) whose VAT is rollback")

					end
				end	
			end
		rescue => exception
			AppLog.logger.info("At line #{__LINE__} inside the VatManager and exception occur in the deductAgentsVAT method is \n #{exception.message}")
          	AppLog.logger.info("At line #{__LINE__} and full exception details are \n #{exception.backtrace.join("\n")}")
		end
	end

	def self.deductVATFromAgent(agentID, vatAmount)

		vatAdmin = Utility.getTaxAdmin
		vatAdminAccount = Utility.getTaxAdminAccount(vatAdmin)
		currentAgentAccount = Utility.getUserAccountWithUserType(agentID, Constants::USER_ACCOUNT_TYPE)

		txnHeader = ApplicationHelper.createHeader(Constants::AGENT_VAT_DB_LABEL, Constants::AGENT_VAT_DB_LABEL, TxnHeader.my_money(vatAmount).to_f, (I18n.t :AGENT_VAT_DEDUCT_DESCRIPTION_MSF), Constants::BLANK_VALUE)
		txnHeader.agent_id = agentID
		txnHeader.save
		AppLog.logger.info("At line #{__LINE__} and Agent VAT deduct entry is saved in the TxnHeader table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, agentID, currentAgentAccount.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

	    AppUtility.getTxnItemNew(txnHeader.id, vatAdmin.id, vatAdminAccount.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(TAX Admin) transaction item is saved in the TxnItem table successfully")

	    Utility.updateUserAccountBalanceNew(agentID, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, true)
    	Utility.updateUserAccountBalanceNew(vatAdmin.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, false)
    	AppLog.logger.info("At line #{__LINE__} and successfuly update balance of  Operator(TAX Admin) and Agent's ACC accounts")

	end	

	def self.creditVATToAgent(agentID, vatAmount)

		vatAdmin = Utility.getTaxAdmin
		vatAdminAccount = Utility.getTaxAdminAccount(vatAdmin)
		currentAgentAccount = Utility.getUserAccountWithUserType(agentID, Constants::USER_ACCOUNT_TYPE)

		txnHeader = ApplicationHelper.createHeader(Constants::AGENT_VAT_DB_LABEL, Constants::AGENT_VAT_DB_LABEL, TxnHeader.my_money(vatAmount).to_f, (I18n.t :AGENT_VAT_CREDIT_DESCRIPTION_MSF), Constants::BLANK_VALUE)
		txnHeader.agent_id = agentID
		txnHeader.save
		AppLog.logger.info("At line #{__LINE__} and Agent VAT deduct entry is saved in the TxnHeader table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, vatAdmin.id, vatAdminAccount.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(TAX Admin) transaction item is saved in the TxnItem table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, agentID, currentAgentAccount.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")
	    
    	Utility.updateUserAccountBalanceNew(vatAdmin.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, true)
	    Utility.updateUserAccountBalanceNew(agentID, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, false)
    	AppLog.logger.info("At line #{__LINE__} and successfuly update balance of  Operator(TAX Admin) and Agent's ACC accounts")

	end	

	def self.updateDeductVATFromOptStatus(agentID)

		txnHeader = getVATHeaders(agentID, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)
		if txnHeader.present?
			txnHeader.each do |header|
				header.update_attribute(:vat_status, Constants::DEDUCT_FROM_OPT_VAT_STATUS)
			end	
		end	

	end	

	def self.updateRollbackVATToOptStatus(agentID)

		txnHeader = getVATHeaders(agentID, Constants::ROLLBACK_TO_AGENT_VAT_STATUS)
		if txnHeader.present?
			txnHeader.each do |header|
				header.update_attribute(:vat_status, Constants::ROLLBACK_TO_OPT_VAT_STATUS)
			end	
		end	

	end	

	def self.deductAREDVAT
		begin
			ActiveRecord::Base.transaction do
				
				aredTotalVATPending = getAREDTotalVAT(Constants::DEDUCT_FROM_OPT_VAT_STATUS)
				AppLog.logger.info("At line #{__LINE__} total VAT is pending to Pay of the ARED is = #{aredTotalVATPending}")

				aredTotalVATRollback = getAREDTotalVAT(Constants::ROLLBACK_TO_OPT_VAT_STATUS)
				AppLog.logger.info("At line #{__LINE__} total VAT is pending to Rollback of the ARED is = #{aredTotalVATRollback}")

				calculatedVATAmount = TxnHeader.my_money(TxnHeader.my_money(aredTotalVATPending).to_f - TxnHeader.my_money(aredTotalVATRollback).to_f).to_f
				AppLog.logger.info("At line #{__LINE__} calculated VAT amount is = #{calculatedVATAmount}")

				if calculatedVATAmount > 0
					deductVATFromARED(calculatedVATAmount)
				else
					if calculatedVATAmount != 0
						creditVATToARED(-(calculatedVATAmount))
					end	
				end	

				updateNoActionStatus(Constants::DEDUCT_FROM_OPT_VAT_STATUS)
	   			AppLog.logger.info("At line #{__LINE__} and successfuly update the status of all the header to 0(NO_ACTION_VAT_STATUS) whose VAT is paid from the Operator")

	   			updateNoActionStatus(Constants::ROLLBACK_TO_OPT_VAT_STATUS)
	   			AppLog.logger.info("At line #{__LINE__} and successfuly update the status of all the header to 0(NO_ACTION_VAT_STATUS) whose VAT is rollback from the Operator")
				
			end	
		rescue => exception
			AppLog.logger.info("At line #{__LINE__} inside the VatManager and exception occur in the deductAREDVAT method is \n #{exception.message}")
          	AppLog.logger.info("At line #{__LINE__} and full exception details are \n #{exception.backtrace.join("\n")}")
		end
	end

	def self.deductVATFromARED(vatAmount)
		
		vatAdmin = Utility.getTaxAdmin
		vatAdminAccount = Utility.getTaxAdminAccount(vatAdmin)

		commissionAdmin = Utility.getCommissionAdmin
		commissionAdminAccount = Utility.getCommissionAdminAccount(commissionAdmin)

		txnHeader = ApplicationHelper.createHeader(Constants::OPT_VAT_DB_LABEL, Constants::OPT_VAT_DB_LABEL, TxnHeader.my_money(vatAmount).to_f, (I18n.t :OPT_VAT_DEDUCT_DESCRIPTION_MSF), Constants::BLANK_VALUE)
		txnHeader.agent_id = commissionAdmin.id
		txnHeader.save
		AppLog.logger.info("At line #{__LINE__} and Operator VAT deduct entry is saved in the TxnHeader table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, commissionAdmin.id, commissionAdminAccount.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(Commission Admin) transaction item is saved in the TxnItem table successfully")

	    AppUtility.getTxnItemNew(txnHeader.id, vatAdmin.id, vatAdminAccount.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(TAX Admin) transaction item is saved in the TxnItem table successfully")

	    Utility.updateUserAccountBalanceNew(commissionAdmin.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, true)
    	Utility.updateUserAccountBalanceNew(vatAdmin.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, false)
    	AppLog.logger.info("At line #{__LINE__} and successfuly update balance of Operator(TAX Admin) and Operator(Commission Admin) accounts")
	
	end	

	def self.creditVATToARED(vatAmount)
		
		vatAdmin = Utility.getTaxAdmin
		vatAdminAccount = Utility.getTaxAdminAccount(vatAdmin)

		commissionAdmin = Utility.getCommissionAdmin
		commissionAdminAccount = Utility.getCommissionAdminAccount(commissionAdmin)

		txnHeader = ApplicationHelper.createHeader(Constants::OPT_VAT_DB_LABEL, Constants::OPT_VAT_DB_LABEL, TxnHeader.my_money(vatAmount).to_f, (I18n.t :OPT_VAT_CREDIT_DESCRIPTION_MSF), Constants::BLANK_VALUE)
		txnHeader.agent_id = commissionAdmin.id
		txnHeader.save
		AppLog.logger.info("At line #{__LINE__} and Operator VAT Credit entry is saved in the TxnHeader table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, vatAdmin.id, vatAdminAccount.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(TAX Admin) transaction item is saved in the TxnItem table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, commissionAdmin.id, commissionAdminAccount.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(Commission Admin) transaction item is saved in the TxnItem table successfully")
	   
	    Utility.updateUserAccountBalanceNew(vatAdmin.id, Constants::TAX_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, true)
	    Utility.updateUserAccountBalanceNew(commissionAdmin.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(vatAmount).to_f, false)
    	AppLog.logger.info("At line #{__LINE__} and successfuly update balance of Operator(TAX Admin) and Operator(Commission Admin) accounts")
	
	end

	def self.updateNoActionStatus(vatStatus)
		txnHeader = TxnHeader.where("vat_status = #{vatStatus}")
		if txnHeader.present?
			txnHeader.each do |header|
				header.update_attribute(:vat_status, Constants::NO_ACTION_VAT_STATUS)
			end	
		end	
	
	end	

end	