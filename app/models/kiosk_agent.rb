class KioskAgent < ApplicationRecord

	def self.assignKiosk(loggedInAdmin, params)
		Log.logger.info("At line #{__LINE__} inside the KioskAgent class and assignKiosk method is called")
		userAction = params[:userAction]
		koiskMasterID = params[:koiskMasterID]
		kioskModel = params[:kioskModel]
		kioskSerialNumber = params[:kioskSerialNumber]
		functionalStatus = params[:functionalStatus]

		if userAction.to_i == Constants::KIOSK_ASSIGN_STATUS
			assignTo = params[:assignTo]
			assignDate = Date.today
			isPrintQRCode = params[:isPrintQRCode]
		else	
			assignTo = params[:agentID]
			comments = params[:comments]
		end
		Log.logger.info("At line #{__LINE__} userAction is = #{userAction}, koiskMasterID = #{koiskMasterID}, kioskModel = #{kioskModel}, kioskSerialNumber = #{kioskSerialNumber}, functionalStatus = #{functionalStatus}, assignDate = #{assignDate}, assignTo = #{assignTo} and isPrintQRCode = #{isPrintQRCode}")

		response = validate(koiskMasterID, kioskModel, kioskSerialNumber, functionalStatus, assignDate, assignTo, isPrintQRCode, userAction)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
        	return response
        else
        	if userAction.to_i == Constants::KIOSK_ASSIGN_STATUS
				return assignKioskToAgent(koiskMasterID, kioskModel, kioskSerialNumber, assignTo, functionalStatus, loggedInAdmin, params)
			else
				return deAssignKioskFromAgent(koiskMasterID, kioskModel, kioskSerialNumber, assignTo, functionalStatus, comments, loggedInAdmin, params)
			end	
		end
	end

	def self.getAssignKioskAgentsList
		agentsList = Hash.new

			sqlQuery = "SELECT id, name, last_name, phone FROM users WHERE role = #{Constants::MF_USER_ROLE} AND status = #{Constants::USER_ACTIVE_STATUS} AND id NOT IN "
			sqlQuery << "(SELECT agent_id FROM kiosk_agents WHERE assign_status = #{Constants::KIOSK_ASSIGN_STATUS}) ORDER BY name ASC"
			
			activeAgents = Utility.executeSQLQuery(sqlQuery)
			agentsList.merge!("Select Agent": "")
			activeAgents.each do |activeAgent|
				agentsList.merge!("#{activeAgent['name']} #{activeAgent['last_name']} - #{activeAgent['phone']}": "#{activeAgent['id']}")
			end	
		return agentsList
	end 

	def self.getDeAssignKioskAgentsList
		agentsList = Hash.new
			activeAgents = Utility.getAllActiveMF
			agentsList.merge!("Select Agent": "")
			activeAgents.each do |activeAgent|
				agentsList.merge!("#{activeAgent['name']} #{activeAgent['last_name']} - #{activeAgent['phone']}": "#{activeAgent['id']}")
			end	
		return agentsList
	end 

	def self.assignKioskToAgent(koiskMasterID, kioskModel, kioskSerialNumber, agentID, functionalStatus, loggedInAdmin, params)
		response = Response.new
			ActiveRecord::Base.transaction do

				kioskMaster = KioskMaster.getKioskMaster(koiskMasterID, kioskModel, kioskSerialNumber)
				kioskMaster.update_attribute(:functional_status, Constants::KIOSK_FUNCTIONAL_STATUS)
				
				kioskAgent = KioskAgent.new
					kioskAgent.kiosk_master_id = koiskMasterID 
					kioskAgent.agent_id = agentID
					kioskAgent.assign_status = Constants::KIOSK_ASSIGN_STATUS
					kioskAgent.functional_status = functionalStatus
				kioskAgent.save
				
				ActivityLog.saveActivity((I18n.t :KIOSK_SUCCESS_ASSIGN_ACTIVITY_MSG), loggedInAdmin.id, agentID, params)
			end	
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			if params[:isPrintQRCode].present?
            	response.Response_Msg=(I18n.t :KIOSK_SUCCESS_ASSIGN_MSG).to_s + " Please wait while the QR Code is downloading"
            else	
            	response.Response_Msg=(I18n.t :KIOSK_SUCCESS_ASSIGN_MSG) 
            end	
            response.object=(KioskMaster.setParamsList(kioskModel, kioskSerialNumber, agentID, params[:qrCodeNo]))
        return response  
	end	

	def self.deAssignKioskFromAgent(koiskMasterID, kioskModel, kioskSerialNumber, agentID, functionalStatus, comments, loggedInAdmin, params)
		response = updateKioskMasterStatus(koiskMasterID, kioskModel, kioskSerialNumber, functionalStatus)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
        	return response
        else
        	return updateKioskAgentStatus(koiskMasterID, kioskModel, kioskSerialNumber, agentID, functionalStatus, comments, loggedInAdmin, params)
        end
        	
	end	

	def self.updateKioskMasterStatus(koiskMasterID, kioskModel, kioskSerialNumber, functionalStatus)
		response = Response.new
		kioskMaster = KioskMaster.getKioskMaster(koiskMasterID, kioskModel, kioskSerialNumber)
		if kioskMaster.present?
				kioskMaster.update_attribute(:functional_status, functionalStatus)
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    	       	response.Response_Msg=("") 
        	return response
		else
				response.Response_Code=(Error::KIOSK_NOT_FOUND)
    	       	response.Response_Msg=(I18n.t :KIOSK_NOT_FOUND_MSG) 
        	return response 
        end	
	end	

	def self.updateKioskAgentStatus(koiskMasterID, kioskModel, kioskSerialNumber, agentID, functionalStatus, comments, loggedInAdmin, params)
		response = Response.new
		kioskAgent = KioskAgent.where("kiosk_master_id = #{koiskMasterID} AND agent_id = #{agentID} AND assign_status = #{Constants::KIOSK_ASSIGN_STATUS}").first
		if kioskAgent.present?
				kioskAgent.update_attributes(:assign_status => Constants::KIOSK_DEASSIGN_STATUS, :functional_status => functionalStatus, :comments => comments)

				ActivityLog.saveActivity((I18n.t :KIOSK_SUCCESS_DE_ASSIGN_ACTIVITY_MSG, :kioskModel => kioskModel.to_s + "-" + kioskSerialNumber.to_s), loggedInAdmin.id, agentID, params)

				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    	       	response.Response_Msg=(I18n.t :KIOSK_SUCCESS_DE_ASSIGN_MSG) 
        	return response
		else
				response.Response_Code=(Error::KIOSK_NOT_ASSIGN)
    	       	response.Response_Msg=(I18n.t :KIOSK_NOT_ASSIGN_MSG) 
        	return response 
        end	
	
	end	

	def self.validate(koiskMasterID, kioskModel, kioskSerialNumber, functionalStatus, assignDate, assignTo, isPrintQRCode, userAction)
		response = Response.new
	        
	        if !koiskMasterID.present? || !kioskModel.present? || !kioskSerialNumber.present? || !functionalStatus.present? || !assignTo.present?
	        		Log.logger.info("At line #{__LINE__} some data is come nil from the software")	
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		if userAction.to_i == Constants::KIOSK_ASSIGN_STATUS && isAnyOtherKioskAssigned(assignTo)
	 				Log.logger.info("At line #{__LINE__} kiosk is already assign to this agent")	
	                response.Response_Code=(Error::KIOSK_ALREADY_ASSIGN)
	                response.Response_Msg=(I18n.t :KIOSK_ALREADY_ASSIGN_MSG) 
	            return response    
	 		end	

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end	

	def self.isAnyOtherKioskAssigned(agentID)
		kioskAgent = KioskAgent.where("agent_id = #{agentID} AND assign_status = #{Constants::KIOSK_ASSIGN_STATUS}").first
		if kioskAgent.present?
        	return Constants::BOOLEAN_TRUE_LABEL
		else
        	return Constants::BOOLEAN_FALSE_LABEL
		end	

	end	

	def self.getAssignedAgent(koiskMasterID)
		response = Response.new
		kioskAgent = KioskAgent.where("kiosk_master_id = #{koiskMasterID} AND assign_status = #{Constants::KIOSK_ASSIGN_STATUS}").order("id DESC").first
		if kioskAgent.present?
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            	response.user=(Utility.getCurrentAgent(kioskAgent.agent_id))
            	response.date=(kioskAgent.created_at)
        	return response 
		else
				response.Response_Code=(Error::NO_RECORD_FOUND)
            	response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
        	return response 
		end	
	end	

	def self.getAssignementHistory(koiskMasterID)
		response = Response.new
		kioskAgent = KioskAgent.where("kiosk_master_id = #{koiskMasterID}").order("id ASC")
		if kioskAgent.present?
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            	response.object=(kioskAgent)
        	return response 
		else
				response.Response_Code=(Error::NO_RECORD_FOUND)
            	response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
        	return response 
		end	
	end	

	def self.getAgentAssignKioskID(agentID)
			kioskAgent = KioskAgent.where("agent_id = #{agentID} AND assign_status = #{Constants::KIOSK_ASSIGN_STATUS}").order("id DESC").first
		return kioskAgent
	end	

end
