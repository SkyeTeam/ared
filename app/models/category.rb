class Category < ApplicationRecord
  has_many :articles
  store_accessor :translations

end
