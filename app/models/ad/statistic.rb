class Ad::Statistic < ApplicationRecord
  enum action: {
    impression: 1,
    click:      2
  }

  belongs_to :ad
  belongs_to :kiosk

  attr_accessor :ad_statistics

  # Returns an array of open_structs of all kiosk-month-data
  # @param month 1..12
  # @param year e.g. 2016
  # @return array_of_open_structs
  def self.get_all_month_data(month, year)
    ar_data = []
    Kiosk.find_each do |kiosk|
      ar_data << kiosk.get_month_data(month, year)
    end

    return ar_data
  end

end
