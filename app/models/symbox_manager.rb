class SymboxManager < ApplicationRecord

	def self.creditAgentAccount(params, request)

		SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and creditAgentAccount method is called")
		
		originId = params[:originId]
		customerId = params[:customerId]
		requestMethod = request.method
		SymboxLog.logger.info("At line #{__LINE__} parameters come in the request URL are originId = #{originId}, customerId = #{customerId} and request come in the method = #{requestMethod}")

		validateResponse = validate(originId, customerId, requestMethod, nil, nil, nil, nil, nil, request, false)
		if validateResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
			return validateResponse
		end	
		
		requestHeadersResponse = getRequestHeaders(params, request) #Here we validate the admin and content-type
		if requestHeadersResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
			return requestHeadersResponse
		end

		loggedInAdmin = requestHeadersResponse.loggedInAdmin
		hashData = requestHeadersResponse.detailsHash

		userAPIKey = ApiKey.getKey(loggedInAdmin.id)
      	unless userAPIKey.present?
      			SymboxLog.logger.info("At line #{__LINE__} and current user does not have any API Key")
	        	response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
    	    	response.Response_Msg=(I18n.t :API_KEY_NOT_EXIST_MSG) 
       		return response
      	end

      	apiKey = Utility.decryptedData(userAPIKey.api_key)
		messageTimeStamp = requestHeadersResponse.messageTimeStamp
		symboxMessageId =	requestHeadersResponse.messageId

		requestParameterResponse = getRequestParameters(params, request, hashData, apiKey) #Here we validate the agent and duplicate request
		if requestParameterResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
			return requestParameterResponse
		end
		
		agentID = requestParameterResponse.agentID
		originTransactionId = requestParameterResponse.originTransactionId
		depositAmount = requestParameterResponse.txnAmount

		return processCredit(params, agentID, originId, depositAmount, symboxMessageId, originTransactionId, loggedInAdmin, request, requestParameterResponse, apiKey)

	end	


	def self.processCredit(params, agentID, originId, depositAmount, symboxMessageId, originTransactionId, loggedInAdmin, request, requestParameterResponse, apiKey)

		params[:agentID] = agentID
		params[:userAccType] = Constants::M_SHIRIKI_ACC_LABEL
		params[:serviceType] = Constants::SYMBOX_WALLET_DEPOSIT_DB_LABEL
  		params[:subType] = getSubType(originId)
  		params[:amount] = depositAmount
  		params[:externalRef] = symboxMessageId
  		params[:description] = originTransactionId

  		cashDepositResponse = CashDepositManager.agentCashDeposit(params, loggedInAdmin, request)
    	if cashDepositResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
		    return cashDepositResponse  
		else
			SymboxLog.logger.info("At line #{__LINE__} the amount has been successfully deposited into the Micro Franchisee account")
			response = Response.new
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				responseData = generateSuccessResponse(originTransactionId, cashDepositResponse.ourTransactionId, requestParameterResponse)
				response.Response_Msg=(responseData) 
				SymboxLog.logger.info("At line #{__LINE__} responseData to generate the response HMAC is = \n#{responseData}")

				encodedHashData = generateHMAC(false, apiKey, responseData, nil, nil, nil)
				encodedHashData = encodedHashData.strip
				SymboxLog.logger.info("At line #{__LINE__}  key used is = #{apiKey} to generate Base64 encoded HashData is for the response header = #{encodedHashData}")

				response.ResponseIntegrity=(encodedHashData)
				
			return response	
		end 

	end	


	def self.generateSuccessResponse(transactionId, providerTransactionId, requestParameterResponse)

		responseData = "{"
		responseData << "\"transactionId\":\"#{transactionId}\","
		responseData << "\"reference\":\"#{requestParameterResponse.reference}\","
		responseData << "\"providerTransactionId\":\"#{providerTransactionId}\","
		responseData << "\"status\":\"#{Constants::PENDING_LABEL}\","

		responseData << "\"origin\":{"
		
		responseData << "\"operator\":\"#{requestParameterResponse.operator}\","
		responseData << "\"subscriber\":\"#{requestParameterResponse.subscriber}\","
		responseData << "\"countryCode\":\"#{requestParameterResponse.countryCode}\""
		responseData << "},"
		responseData << "\"amount\":{"
		responseData << "\"local\":{"
		responseData << "\"currency\":\"#{requestParameterResponse.localCurrency}\","
		responseData << "\"value\":#{requestParameterResponse.localValue}"
		responseData << "},"
		responseData << "\"target\":{"
		responseData << "\"currency\":\"#{requestParameterResponse.targetCurrency}\","
		responseData << "\"value\":#{requestParameterResponse.targetValue}"
		responseData << "},"
		responseData << "\"exchangeRate\":#{requestParameterResponse.exchangeRate}"
		responseData << "},"
		responseData << "\"extensions\":[ ]"
		responseData << "}"

		return responseData
	
	end	


	def self.getRequestHeaders(params, request)

		SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and getRequestHeaders method is called")

		authorization = request.headers[""+AppConstants::AUTHORIZATION_PARAM_LABEL+""]
		messageTimeStamp = request.headers[""+AppConstants::MESSAGETIMESTAMP_PARAM_LABEL+""]
		messageId = request.headers[""+AppConstants::MESSAGEID_PARAM_LABEL+""]
		smsSupport = request.headers[""+AppConstants::SMSSUPPORT_PARAM_LABEL+""]
		contentType = request.headers[""+AppConstants::CONTENT_TYPE_PARAM_LABEL+""]
		SymboxLog.logger.info("At line #{__LINE__} headers parameters are authorization = #{authorization}, messageTimeStamp = #{messageTimeStamp}, messageId = #{messageId}, smsSupport = #{smsSupport} and contentType = #{contentType}")

		validateResponse = validate(params[:originId], params[:customerId], nil, authorization, messageTimeStamp, messageId, smsSupport, contentType, request, true)
		if validateResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
			return validateResponse
		else	
			response = Response.new
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    			response.messageTimeStamp=(messageTimeStamp)
    			response.messageId=(messageId)
    			response.loggedInAdmin=(validateResponse.loggedInAdmin)
    			response.detailsHash=(validateResponse.detailsHash)
	      	return response  
		end

	end	


	def self.getRequestParameters(params, request, hashData, apiKey)

		response = Response.new

		SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and getRequestParameters method is called")

		requestRawData = request.raw_post
		SymboxLog.logger.info("At line #{__LINE__} and requested raw data is = \n#{requestRawData}")

		hashData = hashData.strip #remove only leading and trailing whitespace

		encodedHashData = generateHMAC(true, apiKey, requestRawData, request.headers[""+AppConstants::MESSAGEID_PARAM_LABEL+""], params[:originId], params[:customerId])
		encodedHashData = encodedHashData.strip
		SymboxLog.logger.info("At line #{__LINE__}  key used is = #{apiKey} to generate Base64 encoded HashData is = #{encodedHashData}")

		unless encodedHashData.eql? hashData
				SymboxLog.logger.info("At line #{__LINE__} symbox requested HASH and our generated HASH does not match")
				response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
                response.Response_Msg=(I18n.t :REQUEST_HASH_DATA_NOT_MATCH_MSG) 
           	return response
		end
		SymboxLog.logger.info("At line #{__LINE__} symbox requested HASH and our generated HASH  matched successfully")
				
		isValidJSON = TxnManager.valid_json?(requestRawData)
		SymboxLog.logger.info("At line #{__LINE__} and check that the requested raw data is come in valid JSON or not  = #{isValidJSON}")

		if isValidJSON
				requestData = JSON.parse(requestRawData)
		        
		        originTransactionId = requestData['transactionId']
		        SymboxLog.logger.info("At line #{__LINE__} and origin Transaction Id(Origin ID i.e. Airtel Txn ID) come in the request data is  = #{originTransactionId}")

		        agentPhoneNo = requestData['origin']['subscriber']
		        SymboxLog.logger.info("At line #{__LINE__} and the phone number of the agent whose account will be credit is = #{agentPhoneNo}")
		       
		        currentAgent = Utility.getCurrentAgentByPhone(getActualPhoneNumber(agentPhoneNo))
		        SymboxLog.logger.info("At line #{__LINE__} check that the agent with phone number = #{agentPhoneNo} is exist in the our database or not = #{currentAgent.present?}")

				unless currentAgent.present?
						response.Response_Code=(SymboxErrorCodes::CUSTOMER_NOT_FOUND)
		                response.Response_Msg=(I18n.t :CUSTOMR_NOT_EXIST_MSG, :customerID => agentPhoneNo) 
		           	return response
				end	

				if isDuplicateRequest(params[:originId], agentPhoneNo, request.headers[""+AppConstants::MESSAGETIMESTAMP_PARAM_LABEL+""])
			    		SymboxLog.logger.info("At line #{__LINE__} duplicate request is come for originId = #{params[:originId]}, agent Phone No = #{agentPhoneNo} with in #{Constants::TIME_GAP_FOR_SYMBOX_NEW_RQST} minutes")
						response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
		                response.Response_Msg=(I18n.t :DUPLICATE_REQUEST_MSG, :timeGap => Constants::TIME_GAP_FOR_SYMBOX_NEW_RQST) 
		           	return response
			    end

		     	currencyCode = requestData['amount']['local']['currency']
		        depositAmount = requestData['amount']['local']['value']
		        SymboxLog.logger.info("At line #{__LINE__} and currencyCode is = #{currencyCode} and amount deposit in the agent account is = #{depositAmount}")

				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.originTransactionId=(originTransactionId)
				response.txnAmount=(depositAmount)
				response.agentID=(currentAgent.id)

				getRequestedValues(requestData, response)

	      	return response 
		
		else

				response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
		        response.Response_Msg=(I18n.t :REQUEST_DATA_IS_NOT_VALID_JSON_MSG) 
		    return response

		end	

	end	


	def self.getRequestedValues(requestData, response)
		reference = requestData['reference']
		operator = requestData['origin']['operator']
		subscriber = requestData['origin']['subscriber']
		countryCode = requestData['origin']['countryCode']
		localCurrency = requestData['amount']['local']['currency']
		localValue = requestData['amount']['local']['value']
		targetCurrency = requestData['amount']['target']['currency']
		targetValue = requestData['amount']['target']['value']
		exchangeRate = requestData['amount']['exchangeRate']
		
		response.reference=(reference)
		response.operator=(operator)
		response.subscriber=(subscriber)
		response.countryCode=(countryCode)
		response.localCurrency=(localCurrency)
		response.localValue=(localValue)
		response.targetCurrency=(targetCurrency)
		response.targetValue=(targetValue)
		response.exchangeRate=(exchangeRate)

		return response
	end	


	def self.validate(originId, customerId, requestMethod, authorization, messageTimeStamp, messageId, smsSupport, contentType, request, isRequestHeadersValidate)

		response = Response.new

			if !isRequestHeadersValidate

				if !isRequestedIPWhiteList(request)
						response.Response_Code=(SymboxErrorCodes::FORBIDDEN_OPERATION)
		                response.Response_Msg=(I18n.t :IP_NOT_WHITE_LISTED_MSG) 
		           	return response
				end	

				if !originId.present? || !customerId.present?
						SymboxLog.logger.info("At line #{__LINE__} may be originId or customerId are coming empty")
						response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
		                response.Response_Msg=(I18n.t :EMPTY_DATA_MSG) 
		           	return response
				end	
				
				if !requestMethod.eql? UgandaConstants::HTTP_POST_METHOD
						SymboxLog.logger.info("At line #{__LINE__} request is not come in the POST method")
						response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
		                response.Response_Msg=(I18n.t :NOT_POST_METHOD_MSG) 
		           	return response
				end	

			else

				if !authorization.present? || !messageTimeStamp.present? || !messageId.present? || !smsSupport.present? || !contentType.present?
						SymboxLog.logger.info("At line #{__LINE__} may be authorization, messageTimeStamp, messageId, smsSupport or contentType are coming empty")
						response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
		                response.Response_Msg=(I18n.t :EMPTY_DATA_MSG) 
		           	return response
		        end   

		        adminValidateResponse = isAdminCredentialsMatch(authorization, request)	
			    if adminValidateResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
		           	return adminValidateResponse
			    end	
			    loggedInAdmin = adminValidateResponse.object
			    hashData = adminValidateResponse.detailsHash

			    if !isValidContentType(contentType)
			    		SymboxLog.logger.info("At line #{__LINE__} request body's contentType is not #{Constants::SYMBOX_VALID_CONTENT_TYPE}")
						response.Response_Code=(SymboxErrorCodes::OPERATION_NOT_SUCCEED)
		                response.Response_Msg=(I18n.t :INVALID_CONTENT_TYPE_MSG, :validContentType => Constants::SYMBOX_VALID_CONTENT_TYPE) 
		           	return response
			    end	

			    response.loggedInAdmin=(loggedInAdmin)
			    response.detailsHash=(hashData)

			end

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 

      	return response  
	
	end	

	
	def self.isAdminCredentialsMatch(authorization, request)

		response = Response.new
		
		SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and isAdminCredentialsMatch method is called")

		if authorization.include? ":"
			
			authorizationArray = authorization.split(":", 2)
       		username = authorizationArray[0]
       		hashData = authorizationArray[1]

       		response = UserManager.isUserExist(username, request)

       		response.detailsHash=(hashData)
       		
       		return response

       	else

       			SymboxLog.logger.info("At line #{__LINE__} the authorization does not include the : symbol")

       		 	response.Response_Code=(SymboxErrorCodes::AUTHENTICATION_FAILED)
                response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
            return response

       	end		

	end	


	def self.isDuplicateRequest(originId, agentPhoneNo, messageTimeStamp)

		SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and isDuplicateRequest method is called")

		databaseDetails = Utility.executeSQLQuery(getSQLQuery(agentPhoneNo))
		
		if databaseDetails.present?

	        lastTxnTimeStamp = Time.parse(databaseDetails[0]["txn_date"])
	        SymboxLog.logger.info("At line #{__LINE__} and last transaction time stamp is = #{lastTxnTimeStamp}")

	        parsedMessageTimeStamp = Time.parse(messageTimeStamp)
	        parsedMessageTimeStamp = parsedMessageTimeStamp + Properties::SYMBOX_ADD_HOURS_VALUE.to_i.hours
	        SymboxLog.logger.info("At line #{__LINE__} and current transaction time stamp is = #{parsedMessageTimeStamp}")
	        
	        minutesDifference = ((parsedMessageTimeStamp - lastTxnTimeStamp) / 1.minutes).ceil
	        SymboxLog.logger.info("At line #{__LINE__} time difference in the last request and current request is = #{minutesDifference} minutes")

	        if minutesDifference.to_i < Constants::TIME_GAP_FOR_SYMBOX_NEW_RQST.to_i

	        	if originId.eql? getOriginID(databaseDetails[0]["sub_type"])

	        		return Constants::BOOLEAN_TRUE_LABEL

	        	else

	        		return Constants::BOOLEAN_FALSE_LABEL		
	        	
	        	end	

	        else

	        	return Constants::BOOLEAN_FALSE_LABEL

	        end	

        else

        	return Constants::BOOLEAN_FALSE_LABEL

        end	

	end	


	def self.getSQLQuery(agentPhoneNo)

			query = "SELECT u.phone, t.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, t.sub_type "
			query << "FROM txn_headers t, users u "
			query << "WHERE t.agent_id::INT = u.id AND t.service_type = '#{Constants::SYMBOX_WALLET_DEPOSIT_DB_LABEL}' AND u.phone = '#{agentPhoneNo}' "
			query << "ORDER BY t.txn_date DESC LIMIT 1"

		return query	

	end	


	def self.getOriginID(subType)

		if subType.eql? Constants::SYMBOX_AIRTEL_WALLET_DEPOSIT_DB_LABEL

			return Constants::SYMBOX_AIRTEL_ORIGIN_ID

		elsif subType.eql? Constants::SYMBOX_MTN_WALLET_DEPOSIT_DB_LABEL
		
			return Constants::SYMBOX_MTN_ORIGIN_ID

		else

			return subType					

		end	

	end	


	def self.getSubType(originId)

		if originId.eql? Constants::SYMBOX_AIRTEL_ORIGIN_ID

			return Constants::SYMBOX_AIRTEL_WALLET_DEPOSIT_DB_LABEL

		elsif originId.eql? Constants::SYMBOX_MTN_ORIGIN_ID
		
			return Constants::SYMBOX_MTN_WALLET_DEPOSIT_DB_LABEL

		else

			return originId					

		end	

	end	


	def self.isValidContentType(contentType)
		
		if contentType.eql? Constants::SYMBOX_VALID_CONTENT_TYPE

			return Constants::BOOLEAN_TRUE_LABEL

		else

			return Constants::BOOLEAN_FALSE_LABEL

		end		

	end


	def self.isRequestedIPWhiteList(request)

		SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and isRequestedIPisWhiteList method is called to check the IP = #{request.remote_ip}")

		if Constants::SYMBOX_WHITE_LISTED_IPS.include? request.remote_ip

				SymboxLog.logger.info("At line #{__LINE__} and requested IP is white-listed")
			return Constants::BOOLEAN_TRUE_LABEL

		else

				SymboxLog.logger.info("At line #{__LINE__} and requested IP is  not white-listed")
			return Constants::BOOLEAN_FALSE_LABEL	
		
		end	

	end	

	def self.generateHMAC(isConcate, sharedSecret, requestBody, messageId, originId, customerId)
		begin
				SymboxLog.logger.info("At line #{__LINE__} inside the SymboxManager class and generateHMAC method is called")

				thirdPartyURL = Properties::SYMBOX_HMAC_GENERATE_URL
		    	thirdPartyURI = URI.parse(thirdPartyURL)

		    	http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
		    	http.read_timeout = 20
		     
		    	request = Net::HTTP::Post.new(thirdPartyURI.request_uri)
		    	request.set_form_data({"isConcate" => isConcate, "sharedSecret" => sharedSecret, "requestBody" => requestBody, "messageId" => messageId, "originId" => originId, "customerId" => customerId})
		       
		    	response = http.request(request)
		    	responseData = response.body

		    return responseData 
			
		rescue => exception  

		     	SymboxLog.logger.info("At line #{__LINE__} exception message is = \n #{exception.message}")
    	      	SymboxLog.logger.info("At line #{__LINE__} full exception details are = \n #{exception.backtrace.join("\n")}")

        	return Constants::BLANK_VALUE

        end	
	end	

	def self.getActualPhoneNumber(requestedPhoneNo)

		if requestedPhoneNo.start_with? Constants::RWANDA_PHONE_CODE_SECOND.to_s

			return requestedPhoneNo.gsub(Constants::RWANDA_PHONE_CODE_SECOND.to_s, "")

		elsif requestedPhoneNo.start_with? UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s

			return requestedPhoneNo.gsub(UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s, "")
			
		else

			return requestedPhoneNo
		
		end
	
	end		


end	