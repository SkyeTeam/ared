# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:FeatureEnvy { enabled: false }
# :reek:UtilityFunction { public_methods_only: true }

class TxnManager

  def check_agent_status(agent)
    agent_status = agent.status
    if agent_status == 'suspended'
       return false 
    else
       return true     
    end  
  end  

  def self.update_account(user_account, amount, is_payer)
    if is_payer == true
      Utility.executeSQLQuery("UPDATE user_accounts SET balance = balance - " + amount.to_s +  " WHERE user_id = "+user_account.user_id.to_s)
    else
      Utility.executeSQLQuery("UPDATE user_accounts SET balance = balance + " + amount.to_s +  " WHERE user_id = "+user_account.user_id.to_s)
    end
  end

  def self.update_govt_bank_account(userID, amount, isPayer)
    if isPayer == true
      Utility.executeSQLQuery("UPDATE user_govt_accounts SET balance = balance - " + amount.to_s +  " WHERE user_id = " + userID.to_s)
    else
      Utility.executeSQLQuery("UPDATE user_govt_accounts SET balance = balance + " + amount.to_s +  " WHERE user_id = " + userID.to_s)
    end
  end

  def self.updateServiceBalance(service, serviceBalance)
      
    AppLog.logger.info("At line #{__LINE__} inside the TxnManager's updateServiceBalance method is called with service name is = #{service} and balance of the service is = #{serviceBalance}")
    
    if serviceBalance.present? && serviceBalance != 0
      if serviceBalance.to_s.include? ","
        serviceBalance = serviceBalance.gsub(/,/, '').to_f
      end  
    end  

    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      
      if service.eql? Constants::FDI_DTH_LABEL or service.eql? Constants::FDI_TIGO_LABEL or service.eql? Constants::FDI_ELECTRICITY_LABEL
        
        serviceSetting =   ServiceSetting.where('service = ?',Constants::FDI_TIGO_LABEL).first
        serviceSetting.update_attribute(:balance, serviceBalance)

        serviceSetting =   ServiceSetting.where('service = ?',Constants::FDI_ELECTRICITY_LABEL).first
        serviceSetting.update_attribute(:balance, serviceBalance)
       
        serviceSetting =   ServiceSetting.where('service = ?',Constants::FDI_DTH_LABEL).first
        serviceSetting.update_attribute(:balance, serviceBalance)
      else
        serviceSetting =   ServiceSetting.where('service = ?',service).first
        serviceSetting.update_attribute(:balance, serviceBalance)
      end
   
    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
      
      if service.eql? Constants::FDI_ELECTRICITY_LABEL or service.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL
        serviceSetting =   ServiceSetting.where("service = '#{UgandaConstants::ELECTRICITY_LABEL}'").first
        serviceSetting.update_attribute(:balance, serviceBalance)

        serviceSetting =   ServiceSetting.where("service = '#{UgandaConstants::NATIONAL_WATER_BILL_LABEL}'").first
        serviceSetting.update_attribute(:balance, serviceBalance)
      else  
        allServices = ServiceSetting.where("service != '#{UgandaConstants::ELECTRICITY_LABEL}' AND service != '#{UgandaConstants::NATIONAL_WATER_BILL_LABEL}' AND service != '#{UgandaConstants::INTERNET_LABEL}'")
        allServices.each do |serviceSetting|
          serviceSetting.update_attribute(:balance, serviceBalance)
        end  
      end  

    end   

  end 

  def get_txn_history(agent)
    txn_his = TxnHeader.where(agent_id: agent.id).where(status: Constants::SUCCESS_RESPONSE_CODE).order('txn_date DESC').select(:id,:service_type,:txn_date,:sub_type,:account_id,:amount,:description,:param1,:external_reference)
   return txn_his
  end

  def get_txn_history_on_dates(agent,from_date,to_date)
    txn_his_on_dates = TxnHeader.where(agent_id: agent.id).where("txn_date >= ? AND txn_date <= ?", from_date,to_date).order('txn_date DESC').select(:id,:service_type,:txn_date,:sub_type,:account_id,:amount,:description,:param1)
   return txn_his_on_dates
  end

  def get_sub_type_details(sub_type)
      if sub_type == "loan_request"
        return "Service Credit"
      elsif sub_type == "cash_deposit"
        return "Cash Deposit"
      elsif sub_type == "loan_deposit"
        return "Credit Payment"
      elsif sub_type == "MTOP_UP" || sub_type == "M_TopUp"
        return "AIRTIME"
      elsif sub_type == "delay_charge"
        return "Penalty"
      elsif sub_type == "cash_payment"
        return "Cash Payment"
      elsif sub_type == "commission_payment"
        return "Commission Payment"
      elsif sub_type == "nonpayment_loan"
        return "Non-Payment Credit"
      elsif sub_type == "nonpayment_penalty"
        return "Non-Payment Penalty"
      else
        return sub_type
      end 
  end

  def get_txn_his_sub_type_details(sub_type,t)
    if sub_type == "loan_request"
      return "Service Credit"
    elsif sub_type == "cash_deposit"
      if t.description == 'Cash'
        return "Cash Deposit"
      elsif t.description == 'Bank'
        return "Bank Deposit"
      elsif t.description == 'Wallet'
        return "Wallet Deposit"  
      end  
    elsif sub_type == "loan_deposit"
      return "Credit Payment"
    elsif sub_type == "MTOP_UP"
      return "AirTime"
    elsif sub_type == "delay_charge"
      return "Penalty"
    elsif sub_type == "cash_payment"
      if t.description == 'Cash'
        return "Cash Payment"
      elsif t.description == 'Bank'
        return "Bank Payment"
      elsif t.description == 'Wallet'
        return "Wallet Payment"  
      end  
    elsif sub_type == "commission_payment"
      return "Commission Payment"
    elsif sub_type == "commission_tax_deduct"
      return "Tax Deduction"
    elsif sub_type == "nonpayment_penalty"
      return "Non-Payment Penalty"
    elsif sub_type == "nonpayment_loan"
      return "Non-Payment Credit"
    else
      return sub_type
    end 
  end

  def self.get_user_role(role)
     if role == "agent"
        return "Micro Franchisee"
     elsif role == "admin"
         return "Administrator"
     else
        return role    
     end   
  end  

  def self.getUserRole(user)
    if user.id == 0
      return "Super Administrator"
    elsif user.role == "agent"
      return "Micro Franchisee"
    elsif user.role == "admin"
      return "Administrator"
    elsif user.role == "content_manager"
      return "Content Manager"
    else
      return user.role    
    end   
  end

  def self.get_user_status(status)
     if status == "active"
        return "Active"
     elsif status == "disabled"
         return "Terminated"
     elsif status == "suspended"
         return "Suspended"
     else
        return role    
     end   
  end  

  def self.get_user_gender(gender)
     if gender == "male"
        return "Male"
     elsif gender == "female"
         return "Female"
     else
        return gender    
     end   
  end 

  def self.get_user_gender_short(gender)
     if gender == "male"
        return "M"
     elsif gender == "female"
         return "F"
     else
        return gender    
     end   
  end 

  def self.get_user_email(user,name,last_name)

    x = user.email.include? 'email{'
    y = user.email.include? 'email('
    if x || y
        return name.to_s + ' ' + last_name.to_s
    elsif user.email_flag == 1
        return name.to_s + ' ' + last_name.to_s  
    else
        return user.email    
    end    
  end 

  def get_agent_max_loan_capacity(current_agent)

      last_credit_limit = current_agent.agent_credit_limits.last  
      max_capacity = last_credit_limit.loan_amount 

      return max_capacity
  end

  def getAgentCreditLimit(current_agent,loan_capacity_date,loan_amount)

      agent_credit_limit = AgentCreditLimit.new
      agent_credit_limit.user_id=current_agent.id
      agent_credit_limit.loan_capacity_date = loan_capacity_date
      agent_credit_limit.loan_amount = loan_amount
      agent_credit_limit.save

  end 

  def self.getRegFormRoles(role)
      if role== 'agent'
        return 'Micro Franchisee'
       else
        return role
       end 
  end  

  def self.getNationalIdStatus(user)
      if user.national_id.present? 
        disable_status = true  
      else
        disable_status = false 
      end 
      return disable_status
  end 

  def self.getUserEmail(user)
      if user.id != nil
        x = user.email.include? 'email{'
        y = user.email.include? 'email('
    
         if x || y
            @user_email =''
         elsif user.email_flag == 1
            @user_email =''
         else
            @user_email = user.email 
         end 
      else
        @user_email =''
      end 
    return @user_email
  end  

  def self.getRandomEmail()
     email_expersion = [('a'..'z'),('A'..'Z'),('0'..'9')].map { |i| i.to_a }.flatten
     random_email = (0...15).map { email_expersion[rand(email_expersion.length)] }.join
    return random_email
  end 

  def self.getTinNoStatus(user)
      if user.id != nil
        if user.tin_no.present? 
          if user.tin_no != 0
             isTINDisable = true  
          else
             isTINDisable = false 
          end 
        else
          isTINDisable = false 
        end 
      else
        isTINDisable = false 
      end  
    return isTINDisable
  end  

  def self.valid_json?(json)
    begin
        JSON.parse(json)
      return true
    rescue JSON::ParserError => e
      return false
    end
  end

end