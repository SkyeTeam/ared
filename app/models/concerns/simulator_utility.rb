# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class SimulatorUtility

	def self.sendFDIRechargeRequest(pdtType, customerPhone, amount, externalRef, timeStamp, customerMeterNo, customerSmartCardNo, gatewayLogID)
		
		if pdtType.eql? 'airtime'
			requestParams = {'pdt' => pdtType, 'ep' => 'vend', 'customer_msisdn' => customerPhone, 'amount' => amount, 'trxref' => externalRef, 'tstamp' => timeStamp}
		elsif pdtType.eql? 'electricity'
			requestParams = {'pdt' => pdtType, 'ep' => 'vend', 'meterno' => customerMeterNo, 'amount' => amount, 'trxref' => externalRef, 'customer_msisdn' => customerPhone, 'tstamp' => timeStamp}
		elsif pdtType.eql? 'startimes_tv'
			requestParams = {'pdt' => pdtType, 'ep' => 'vend', 'customer_msisdn' => customerPhone, 'amount' => amount, 'smartcard' => customerSmartCardNo, 'trxref' => externalRef,  'tstamp' => timeStamp}
		end	

		thirdPartyURI = URI.parse(Properties::FDI_TXNS_URL)

		requestData = Net::HTTP::Post.new(thirdPartyURI)
        requestData.set_form_data(requestParams)
        
        requestBody = thirdPartyURI.to_s + "?" + requestData.body.to_s
        GatewayLog.updateRequestData(gatewayLogID, requestBody)
        
        AppLog.logger.info("At line = #{__LINE__} and request sent to the FDI is = \n#{requestBody}")
        
        # Timeout in seconds
        response = Net::HTTP.start(thirdPartyURI.host, thirdPartyURI.port) {|http| http.request(requestData)}
        responseData = response.body

        return responseData

	end	

	def self.sendAirtelRechargeRequest(externalRef, customerPhone, amount, gatewayLogID)
		
		begin

		 	AppLog.logger.info("Inside the SimulatorUtility class and sendAirtelRequest method is called")

			AppLog.logger.info("At line #{__LINE__} and parameters are as externalRef = #{externalRef}, customerPhone = #{customerPhone} and amount = #{amount}")		 	

            customerPhone = AppUtility.getCustomerPhoneToRecharge(customerPhone)

		 	thirdParty = ThirdPartyConfig.getThirdparty(Constants::AIRTEL_AGGREGATOR)

			airtelUsername = thirdParty.username

	        airtelPin = thirdParty.password

	        connectionTimeout = thirdParty.connection_timeout

			thirdPartyURL = Properties::AIRTEL_RECHARGE_URL
	        	
	        requestData = AirtelUtility.getRequestData(airtelUsername, airtelPin, externalRef, customerPhone, amount)

	        GatewayLog.updateRequestData(gatewayLogID, requestData)
	        
	        AppLog.logger.info("At line = #{__LINE__} and request sent to the airtel is = \n#{requestData}")

	        thirdPartyURI = URI.parse(thirdPartyURL)
	        http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
	        request = Net::HTTP::Post.new(thirdPartyURI.request_uri)
	        request['content-type'] = 'text/xml'
	        request.body = requestData
	         
	        response = http.request(request)
	        responseData = response.body

	    	return  responseData  
	    
	    rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        		responseData = AirtelUtility.getError(exception.message)
        	
        	return responseData

        end	

	end	


end	