# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class ProfitLoseManager

	def self.serachProfitLoseBWdates(params)

        Log.logger.info("Inside the Profit Lose Report Manager class and serachProfitLoseBWdates method is called")

        fromDate = params[:from_date]
		toDate = params[:to_date]
        response = Response.new

        unless fromDate.present? || toDate.present?
                Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end   

		Log.logger.info("Parameters are from date is = #{fromDate} and to date is = #{toDate}")

		if fromDate.to_s > toDate.to_s
				response.Response_Code=(Error::CHECK_DATES_AGAIN)
			    response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG)
            return response   
		end	

		firstDate = Date.parse(fromDate.to_s)
        beginFromDate = firstDate.beginning_of_day

        secondDate = Date.parse(toDate.to_s)
        endToDate = secondDate.end_of_day

        Log.logger.info("At line = #{__LINE__} and begin of from date is = #{beginFromDate} and end of to date is = #{endToDate}")     
         
        query = "SELECT users.id,users.name,users.last_name,users.phone,txn_headers.amount,txn_headers.service_type,txn_headers.txn_date "
        query << "FROM users,txn_headers "
        query << "WHERE txn_headers.service_type = '#{Constants::USER_NON_PAYMENT_LOAN_LABLE}' AND txn_headers.agent_id::INT = users.id "
        query << "AND txn_headers.txn_date >= '#{beginFromDate}' AND txn_headers.txn_date <= '#{endToDate}' "
        query << "GROUP BY users.id,users.name,users.last_name,users.phone,txn_headers.amount,txn_headers.service_type,txn_headers.txn_date "
        query << "ORDER BY txn_headers.txn_date DESC"
        
        profitLoseDetails =  TxnHeader.find_by_sql(query)
        Log.logger.info("At line = #{__LINE__} and check that whether that the profit and lose details in the database is exist or not = #{profitLoseDetails.present?}")
         
        profitLoseList = Array.new
          
        if profitLoseDetails.present?

            profitLoseDetails.each do |profitLose|

                allUsersHash = ActiveSupport::HashWithIndifferentAccess.new

                allUsersHash[:user_id] = profitLose.id
                allUsersHash[:name] = profitLose.name
                allUsersHash[:last_name] = profitLose.last_name
                allUsersHash[:phone] = profitLose.phone
                allUsersHash[:amount] = profitLose.amount
                allUsersHash[:service_type] = profitLose.service_type
                allUsersHash[:txn_date] = profitLose.txn_date
                
                profitLoseList << allUsersHash 

            end  
        end

        Log.logger.info("At line = #{__LINE__} and check that the data in the array is pesent or not = #{profitLoseList.present?} ")

        Log.logger.info("At line = #{__LINE__} and response give back to controller with Response Code = #{response.Response_Code}")

        if profitLoseList.present?
	            response.profitLoseList=(profitLoseList)  
	            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            return response   
        else
	          	response.Response_Code=(Error::NO_TRANSCATIONS)
			    response.Response_Msg=(I18n.t :NO_TRANSCATIONS_MSG)
		    return response 
        end
	end	
end	