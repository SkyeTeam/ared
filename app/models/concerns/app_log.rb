# This class used as a Manager in MVC
# :reek:ClassVariable { enabled: false }

class AppLog

  require 'properties'	 	 

  def self.logger
    @@logger ||= Logger.new(Properties::APP_LOGS_FILE_LOCATION , 'daily')
  end

end	