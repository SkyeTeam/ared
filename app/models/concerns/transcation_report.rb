# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class TranscationReport

	def self.search(params)
	    Log.logger.info("At line #{__LINE__} and inside the TranscationReport class and search method is called")

	    if params[:transactionType].present?
	    	trancationType = params[:transactionType]
	    	operator = params[:operator]
	    	whichUser = params[:whichUser]
	    	userPhone = params[:user_phone]
	    	fromDate = params[:from_date]
	    	toDate = params[:to_date]
	    else
	        trancationType = Constants::ALL_TXNS_LABEL
	        whichUser = Constants::ALL_USER_RADIO_LABEL
	        fromDate = Date.today
	        toDate = Date.today
      	end  

		response = Response.new
		Log.logger.info("Parameters are type = #{trancationType} , operator =#{operator}, from date is = #{fromDate}, to date is = #{toDate}, whichUser =  #{whichUser} , userPhone is = #{userPhone}")

	    fromDate = Date.parse(fromDate.to_s).beginning_of_day
	    toDate = Date.parse(toDate.to_s).end_of_day
	    
	   	response = validate(fromDate, toDate)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
	    else
	    	if whichUser.eql? Constants::ALL_USER_RADIO_LABEL

	    		Log.logger.info("At line = #{__LINE__} inside the if of #{Constants::ALL_USER_RADIO_VALUE} and now chcek that the data for transaction type = #{trancationType}")	
	    		txnHistory = getUsersTxnList(fromDate, toDate, trancationType, operator)
		 	
		 	elsif whichUser.eql? Constants::SINGLE_USER_RADIO_LABEL

		 		Log.logger.info("At line = #{__LINE__} inside the elsif of single user and search by phone number = #{userPhone}")	
		 		agent = User.find_by_phone(userPhone)
		 		Log.logger.info("At line = #{__LINE__} and check agent is present or not = #{agent.present?}")

		 		if agent.present?
		 			txnHistory = getSingleAgentTxnDetails(agent.id,fromDate,toDate,trancationType,operator)
				else
					response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
		   			response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
		           return response
				end    	
		 	end
	    end

  		Log.logger.info("At line = #{__LINE__} and now check the history is  present or not = #{txnHistory.present?}")		
        if txnHistory.present?
          		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
          		response.txn_history=(txnHistory)
          	return response
        else
          		response.Response_Code=(Error::NO_TRANSCATIONS)
	   	  		response.Response_Msg=(I18n.t :NO_TRANSCATIONS_MSG) 
	   	  	return response
        end
	end

	def self.validate(fromDate, toDate)
        response = Response.new

            if !Utility.isValidYear(fromDate.to_s)
	    			Log.logger.info("At line = #{__LINE__} and from date has not a valid year ")
	   				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
    			return response
			end

			if !Utility.isValidYear(toDate.to_s)
	    			Log.logger.info("At line = #{__LINE__} and to date has not a valid year ")
	   				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

		    if fromDate > toDate
		    		Log.logger.info("At line = #{__LINE__} and from date is greate tehen to date ")
		       		response.Response_Code=(Error::CHECK_DATES_AGAIN)
			    	response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
			    return response
			end
			    
        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
    end

	def self.getUsersTxnList(fromDate, toDate, serviceType, subType)
			if serviceType.eql? Constants::ALL_TXNS_LABEL
				txnHistory = TxnHeader.where("txn_date >= ? AND txn_date <= ?", fromDate, toDate).where(getExcludeServiceType).order("txn_date desc")
			elsif serviceType.eql? Constants::CREDIT_FEE_VIEW_LABEL
				txnHistory = getAllMFCreditFeeDetails(fromDate, toDate)
			else
				if subType.present?
					txnHistory = TxnHeader.where("txn_date >= ? AND txn_date <= ? AND service_type = ? AND sub_type = ?", fromDate, toDate, serviceType, subType).where(getExcludeServiceType).order("txn_date desc")
				else
					txnHistory = TxnHeader.where("txn_date >= ? AND txn_date <= ? AND service_type = ?", fromDate, toDate, serviceType).where(getExcludeServiceType).order("txn_date desc")
				end	
			end	
	  	return txnHistory	
	end	

	def self.getSingleAgentTxnDetails(userId, fromDate, toDate, isServiceTypeAvailable, isSubTypeAvailable)
	  	excludeServiceTypeQuery = getExcludeServiceType.present? ? "AND #{getExcludeServiceType}" : ""

	  	if isServiceTypeAvailable.eql? Constants::ALL_TXNS_LABEL
	  		query = "SELECT h.id, i.user_id, h.txn_date, h.service_type, h.sub_type, h.account_id, h.amount, i.previous_balance, "
	  		query << "i.post_balance, h.description, h.external_reference "
	      	query << "FROM txn_items i,  txn_headers h WHERE h.id=i.header_id AND i.user_id = #{userId} "
	      	query << "AND h.txn_date >= '#{fromDate}' AND h.txn_date <= '#{toDate}' #{excludeServiceTypeQuery} "
	      	query << "ORDER BY h.txn_date DESC"
	  	
		elsif isServiceTypeAvailable.eql? Constants::CREDIT_FEE_VIEW_LABEL
			query = "SELECT u.name, u.last_name, u.phone, l.amount AS loan_amount,  l.credit_fee AS amount, l.created_at AS txn_date "
			query << "FROM loans l, users u "
			query << "WHERE l.amount > 0 AND l.user_id = #{userId} AND l.user_id = u.id AND l.created_at >= '#{fromDate}' AND l.created_at <= '#{toDate}'"
			query << "ORDER BY l.created_at DESC;"

	  	else
		  	if isServiceTypeAvailable.present? && isSubTypeAvailable.present?
		  		query = "SELECT h.id, i.user_id, h.txn_date, h.service_type, h.sub_type, h.account_id, h.amount, i.previous_balance, "
		  		query << "i.post_balance, h.description, h.external_reference "
		      	query << "FROM txn_items i,  txn_headers h WHERE h.id=i.header_id AND i.user_id = #{userId} "
		      	query << "AND h.txn_date >= '#{fromDate}' AND h.txn_date <= '#{toDate}' "
		      	query << "AND h.service_type = '#{isServiceTypeAvailable}' AND h.sub_type = '#{isSubTypeAvailable}' "
		      	query << "#{excludeServiceTypeQuery} ORDER BY h.txn_date DESC"
		  	else
		  		query = "SELECT h.id, i.user_id, h.txn_date, h.service_type, h.sub_type, h.account_id, h.amount, i.previous_balance, "
		  		query << "i.post_balance, h.description, h.external_reference "
		      	query << "FROM txn_items i,  txn_headers h WHERE h.id=i.header_id AND i.user_id = #{userId} "
		      	query << "AND h.txn_date >= '#{fromDate}' AND h.txn_date <= '#{toDate}' "
		      	query << "AND h.service_type = '#{isServiceTypeAvailable}' #{excludeServiceTypeQuery} "
		      	query << "ORDER BY h.txn_date DESC"
		  	end	
		end
		 
      		list = Utility.executeSQLQuery(query)
		return list
  	end	

  	def self.getAllMFCreditFeeDetails(fromDate, toDate)
	  		query = "SELECT u.name, u.last_name, u.phone, l.amount AS loan_amount, l.credit_fee AS amount, l.created_at AS txn_date "
			query << "FROM loans l, users u "
			query << "WHERE l.amount > 0 AND l.user_id = u.id AND l.created_at >= '#{fromDate}' AND l.created_at <= '#{toDate}'"
			query << "ORDER BY l.created_at DESC;"

			Log.logger.info("At line #{__LINE__} and inside the getAllMFCreditFeeDetails method and query is = #{query}")

  			creditFeeDetails = Utility.executeSQLQuery(query)
		return creditFeeDetails
  	end	
  
	def self.getExcludeServiceType
  # 		query = "service_type != '#{Constants::AGGREGATOR_DEPOSIT_DB_LABEL}' AND "
  # 		query << "service_type != '#{Constants::MTOP_UP_SALES_LABEL}' AND "
  # 		query << "service_type != '#{Constants::ELECTRICITY_SALE_LABEL}' AND "
  # 		query << "service_type != '#{Constants::DTH_SALE_LABEL}' AND "
  # 		query << "service_type != '#{Constants::INTERNET_SALE_LABEL}' AND "
  # 		query << "service_type != '#{UgandaConstants::NATIONAL_WATER_BILL_SALES_LABEL}' AND "
  # 		query << "service_type != '#{Constants::IREMBO_TAX_PAYMENT_SALE_LABEL}'"
		# return query		 
	end

	def self.getAgentTxnHistory(currentAgent)
		AppLog.logger.info("At line #{__LINE__} inside the TranscationReport's  and getAgentTxnHistory method is called")
		response = Response.new
		
		unless currentAgent.present?
        		AppLog.logger.info("At line #{__LINE__} and agent is not present")
        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      		return response
    	end
    	AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

    	txnHeader = TxnHeader.where(agent_id: currentAgent.id).where(status: Constants::SUCCESS_RESPONSE_CODE).order('txn_date DESC').select(:id,:service_type,:txn_date,:sub_type,:account_id,:amount,:description,:param1,:external_reference)
    	AppLog.logger.info("At line #{__LINE__} and check that agent has transaction history or not = #{txnHeader.present?}")

    	if txnHeader.present?
      			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.object=(txnHeader)
        		response.balance=(Utility.getAgentAccountBalance(currentAgent))
      		return response
    	else  
       			response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
    	end
	end	

	def self.getAgentTxnHistoryBWDates(currentAgent, params)
		AppLog.logger.info("At line #{__LINE__} inside the TranscationReport's  and getAgentTxnHistory method is called")
		response = Response.new
		
		unless currentAgent.present?
        		AppLog.logger.info("At line #{__LINE__} and agent is not present")
        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      		return response
    	end
    	AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

    	fromDate = params[""+AppConstants::FROM_DATE_PARAM_LABEL+""]
    	toDate = params[""+AppConstants::TO_DATE_PARAM_LABEL+""]

    	if !fromDate.present? || !toDate.present?
        		AppLog.logger.info("At line = #{__LINE__} and data is come nil from the app")
        		response.Response_Code=(Error::INVALID_DATA)
        		response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
      		return response
  		end

  		fromDate = Date.parse(fromDate.to_s).beginning_of_day
    	toDate = Date.parse(toDate.to_s).end_of_day
    
    	if fromDate > toDate
       			response.Response_Code=(Error::CHECK_DATES_AGAIN)
	    		response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
	    	return response
	  	end    

    	txnHeader = TxnHeader.where(agent_id: currentAgent.id).where("txn_date >= '#{fromDate}' AND txn_date <= '#{toDate}'").order('txn_date DESC').select(:id,:service_type,:txn_date,:sub_type,:account_id,:amount,:description,:param1)
    	AppLog.logger.info("At line #{__LINE__} and check that agent has transaction history or not = #{txnHeader.present?}")

    	if txnHeader.present?
       			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.object=(txnHeader)
        		response.balance=(Utility.getAgentAccountBalance(currentAgent))
      		return response
    	else  
        		response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
    	end
	end	

	def self.getReceiptDetails(currentAgent, params)
		AppLog.logger.info("At line #{__LINE__} inside the TranscationReport's  and getReceiptDetails method is called")
		response = Response.new
		
		unless currentAgent.present?
        		AppLog.logger.info("At line #{__LINE__} and agent is not present")
        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      		return response
    	end
    	AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

    	transactionID = params[""+AppConstants::RECEIPT_ID_PARAM_LABEL+""]
    	AppLog.logger.info("At line #{__LINE__} and transactionID to search the details is = #{transactionID}")

    	unless transactionID.present?
        		AppLog.logger.info("At line = #{__LINE__} and transactionID is come nil from the app")
        		response.Response_Code=(Error::INVALID_DATA)
        		response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
      		return response
  		end

  		txnHeader = TxnHeader.find_by_id(transactionID)
  		AppLog.logger.info("At line #{__LINE__} and check that this transactionID  has present or not = #{txnHeader.present?}")

  		if txnHeader.present?
      			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.object=(txnHeader) 
      		return response
   	 	else  
       			response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
    	end
	end	

end