# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class ResetPasswordManager

    def self.searchByPhone(phone)
    	Log.logger.info("Inside the ResetPasswordManager class and method searchByPhone is call at line = #{__LINE__} with phone = #{phone}")
		response = Response.new
		
		if !phone.present?
                Log.logger.info("At line = #{__LINE__} and phone is empty")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end

		user = User.find_by_phone(phone)
	    if user.present?
	        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	   	    	response.User_Object=(user)
		  	return response
	    else
	        	response.Response_Code=(Error::USER_NOT_EXIST)
	        	response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
		  	return response
	    end
	end	

	def self.searchByName(full_name)
		Log.logger.info("Inside the ResetPasswordManager class and method searchByName is call at line = #{__LINE__} with name = #{full_name}")
		
		response = Response.new

		if !full_name.present?
                Log.logger.info("At line = #{__LINE__} and name is empty")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end
        
	    length = 0  
	    query = ''  
	          
	    if full_name.include? ' '
	        x = full_name.split(' ')
	        length = x.length
	        for i in (0..length-1)
	          query = query.to_s + " LOWER(name) LIKE '%" + x[i].downcase + "%' or "
	          query = query.to_s + " LOWER(last_name) LIKE '%" + x[i].downcase + "%' or"
	        end  
	        query = query.chomp('or')
	    else
	        query = "LOWER(name) LIKE '%" + full_name.downcase + "%' or LOWER(last_name) LIKE '%" + full_name.downcase + "%'"
	    end 

	    user = User.where(query).where("status != #{Constants::USER_DELETE_STATUS}")
	    
	    if user.present?
	        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	   	    	response.User_Object=(user)
		  	return response
	    else
    	    	response.Response_Code=(Error::USER_NOT_EXIST)
	        	response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
		  	return response
   		end	
	end


	def self.resetUserPassword(loggedInAdmin, params)
		Log.logger.info("Inside the ResetPasswordManager class and method resetUserPassword is call at line = #{__LINE__}")
		
		response = Response.new

			userID = params[:user] # get the details of the user from the web page that are come after search the user by name or phone

			user = User.find(userID)
		  	country_code = Properties::COUNTRYCODE
		    mobile_no = country_code.to_s + user.phone.to_s

		    user_manager = UserManager.new
		    random_password = user_manager.getUserPassword(user)

		    if user.role.eql? Constants::AGENT_DB_LABEL
			  	user.change_password!(random_password)
			  	user_manager.sendMsg(user, random_password, Constants::RESETSMSPURPOSE)
		      	response.Response_Msg=(I18n.t :MF_PASSWORD_RESET_MSG)		
		    else
	          	user.change_password!(random_password)
	            message = (I18n.t :ADMIN_PASSWORD_RESET_EMAIL_MSG, :name => (user.name.to_s + ' ' + user.last_name.to_s), :username => user.email, :password => random_password, :loginLink => Properties::SENT_EMAIL_MSG_LOGIN_LINK)
	          	Utility.sendEmail(Properties::SUPERADMIN_EMAIL, user.email, (I18n.t :ADMIN_PASSWORD_RESET_EMAIL_TITLE), message)
		      	response.Response_Msg=(I18n.t :ADMIN_PASSWORD_RESET_MSG)
		    end

		    ActivityLog.saveActivity((I18n.t :USER_PASSWORD_RESET_ACTIVITY_MSG, :role => User.getRoleDisValue(user.role)), loggedInAdmin.id, user.id, params)
		    
		    response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	  
	  	return response	
	end	
    
    def self.changeAgentPIN(currentAgent, params)
    	AppLog.logger.info("At line #{__LINE__} inside the ResetPasswordManager's  and changeAgentPIN method is called")
  		response = Response.new
  		unless currentAgent.present?
                AppLog.logger.info("At line #{__LINE__} and agent is not present")
                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
            return response
        end
        AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

        newPIN = params[""+AppConstants::NEW_PIN_PARAM_LABEL+""]
        confirmPIN = params[""+AppConstants::RE_PIN_PARAM_LABEL+""]
        
        if !newPIN.present? || !confirmPIN.present? 
                AppLog.logger.info("At line = #{__LINE__} and data is come nil from the app")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
    	end

    	if !(newPIN.eql? confirmPIN)
    		    AppLog.logger.info("At line = #{__LINE__} and new and confirm PIN is not same")
                response.Response_Code=(Error::NEW_AND_RE_PIN_NOT_MATCH)
                response.Response_Msg=(I18n.t :NEW_AND_RE_PIN_NOT_MATCH_MSG) 
            return response
    	end	

		my_password = BCrypt::Password.create newPIN
        status = currentAgent.change_password!(newPIN)
        AppLog.logger.info("At line #{__LINE__} and now check that the PIN is change or not = #{status}")

    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=(I18n.t :PIN_SUCCESS_CHANGE_MSG) 
        return response
    end	

    def self.resetAgentPIN(params)
    	AppLog.logger.info("At line #{__LINE__} inside the ResetPasswordManager's  and resetAgentPIN method is called")
    	response = Response.new
		agentPhone = params[""+AppConstants::PHONE_PARAM_LABEL+""]
    	newPIN = params[""+AppConstants::NEW_PIN_PARAM_LABEL+""]
        confirmPIN = params[""+AppConstants::CONFIRM_PIN_PARAM_LABEL+""]
        
        if !agentPhone.present? || !newPIN.present? || !confirmPIN.present? 
                AppLog.logger.info("At line = #{__LINE__} and data is come nil from the app")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
    	end

    	if !(newPIN.eql? confirmPIN)
    		    AppLog.logger.info("At line = #{__LINE__} and new and confirm PIN is not same")
                response.Response_Code=(Error::NEW_AND_RE_PIN_NOT_MATCH)
                response.Response_Msg=(I18n.t :NEW_AND_RE_PIN_NOT_MATCH_MSG) 
            return response
    	end	

    	currentAgent = User.find_by_phone(agentPhone)
    	AppLog.logger.info("At line  #{__LINE__} and now check that the agent with this phone = #{agentPhone} is present or not = #{currentAgent.present?}")

    	if currentAgent.present?
    		my_password = BCrypt::Password.create newPIN
            status = currentAgent.change_password!(newPIN)
            AppLog.logger.info("At line #{__LINE__} and now check that the PIN is change or not = #{status}")
            	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.Response_Msg=(I18n.t :PIN_SUCCESS_CHANGE_MSG) 
            return response
           
    	else
    			AppLog.logger.info("At line #{__LINE__} and agent is not present")
                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
            return response
    	end	   
    end 

    def self.validateUserEmail(params)
        response = Response.new
        email = params[:email]
        Log.logger.info("At line #{__LINE__} inside the ResetPasswordManager class and validateUserEmail method is called with email is #{email}")

        if !email.present? 
                Log.logger.info("At line #{__LINE__} and email is come nil from the software")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end

        user = User.where("email = '#{email}' AND role = #{Constants::ADMIN_USER_ROLE}").first
        if user.present?
                saveForgotPasswordRequest(user.id, user.email)
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.Response_Msg=(I18n.t :EMAIL_SUCCESS_SENT_MSG) 
            return response
        else
                Log.logger.info("At line #{__LINE__} and email is not present in the database")
                response.Response_Code=(Error::EMAIL_NOT_EXIST)
                response.Response_Msg=(I18n.t :EMAIL_NOT_EXIST_MSG) 
            return response
        end
    end  

    def self.saveForgotPasswordRequest(userID, userEmail)
        ActiveRecord::Base.transaction do
            otp = OneTimePassword.new
                encryptedNo = BCrypt::Password.create Utility.getRandomNumber(6)
                otp.otp_id = OneTimePassword.getOTPID
                otp.otp = encryptedNo
                otp.description = Labels::FORGOT_PASSWORD_DB_DES_LABEL
                otp.comments = userID
                otp.status = Constants::OTP_NOT_USED_STATUS
            otp.save

            Utility.sendEmail(Properties::SUPERADMIN_EMAIL, userEmail, (I18n.t :FORGOT_PASSWARD_EMAIL_TITLE), (I18n.t :FORGOT_PASSWARD_EMAIL_MSG, :email => userEmail, :resetLink => "#{Properties::APP_IMAGE_PATH}ResetPasswordDetails?forgotID=#{encryptedNo}"))
        end    
    end

    def self.setNewPassword(params)
        Log.logger.info("At line #{__LINE__} inside the ResetPasswordManager class and setNewPassword method is called")
       
        forgotID = params[:forgotID]
        newPassword = params[:newPassword]
        confirmPassword = params[:confirmPassword]
        response = validateSetNewPassword(forgotID, newPassword, confirmPassword)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
           return response 
        else
                ActiveRecord::Base.transaction do
                    linkDetails =  response.Response_Msg # if the responseCode is 200 then the linkDetails object is come in Response_Msg
                        
                    userID = linkDetails.comments
                    currentUser = User.find_by_id(userID)
                    Log.logger.info("At line #{__LINE__} password is reset of the user id = #{userID} and name is = #{Utility.getAgentFullName(currentUser)}")    

                    currentUser.change_password!(newPassword)
                   
                    linkDetails.update_attribute(:status, Constants::OTP_USED_STATUS)

                    ActivityLog.saveActivity((I18n.t :FORGOT_PASSWORD_RESET_ACTIVITY_MSG), userID, nil, params)
                end  
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.Response_Msg=(I18n.t :PASSWORD_SUCCESSFULLY_RESET_MSG)   
            return response    
        end       
    end  

    def self.validateSetNewPassword(forgotID, newPassword, confirmPassword)
        response = Response.new

            unless forgotID.present?
                    Log.logger.info("At line #{__LINE__} and forgotID is come empty in the link")
                    response.Response_Code=(Error::INVALID_RESET_PASSWORD_LINK)
                    response.Response_Msg=(I18n.t :INVALID_RESET_PASSWORD_LINK_MSG) 
                return response
            end

            if !newPassword.present? || !confirmPassword.present?
                    Log.logger.info("At line #{__LINE__} and either newPassword or confirmPassword is come nil from the software")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
            end

            unless newPassword.eql? confirmPassword            
                    Log.logger.info("At line #{__LINE__} newPassword and confirmPassword is not same")
                    response.Response_Code=(Error::NEW_CONFIRM_PASSWORD_NOT_SAME)
                    response.Response_Msg=(I18n.t :NEW_CONFIRM_PASSWORD_NOT_SAME_MSG) 
                return response
            end 

            linkDetails = OneTimePassword.where("otp = '#{forgotID}'").first
            unless linkDetails.present?
                    Log.logger.info("At line #{__LINE__} details are not present in the database with this forgotID = #{forgotID}")
                    response.Response_Code=(Error::INVALID_RESET_PASSWORD_LINK)
                    response.Response_Msg=(I18n.t :INVALID_RESET_PASSWORD_LINK_MSG) 
                return response
            end

            if isLinkExpire(linkDetails.created_at)
                    response.Response_Code=(Error::EXPIRE_RESET_PASSWORD_LINK)
                    response.Response_Msg=(I18n.t :EXPIRE_RESET_PASSWORD_LINK_MSG) 
                return response
            end
            
            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=(linkDetails) 
        return response
    end

    def self.isLinkExpire(linkCreatedAt)
        linkCreatedAtTime = Time.parse(linkCreatedAt.to_s)
        presentTime = Time.now
        hoursDifference = ((presentTime - linkCreatedAtTime)/3600).to_i
        if hoursDifference >= Constants::RESET_PASSWORD_LINK_EXPIRE_TIME
            return Constants::BOOLEAN_TRUE_LABEL
        else
            return Constants::BOOLEAN_FALSE_LABEL
        end    
    end    
end
