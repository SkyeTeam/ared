# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:FeatureEnvy { enabled: false }
# :reek:NilCheck { enabled: false }

class UserManager

	def self.createUser(user, admin_roles, loggedInAdmin, params)
    	   
            response = validateCreateUser(user, admin_roles)

            if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
                return response
            end

            userManager = UserManager.new 
            userPassword = userManager.getUserPassword(user) 
            user.password = userPassword

            ActiveRecord::Base.transaction do
                if user.save
                    userAccount = userManager.getUserAccount(user)
                    userAccount.save

                    loan = userManager.getUserLoan(user)
                    loan.save

                    agentCreditLimit = userManager.getUserCreditLimit(user)
                    agentCreditLimit.save
                end

                if user.role.eql? Constants::AGENT_DB_LABEL
                    userManager.sendMsg(user, userPassword, Constants::REGSMSPURPOSE)
                elsif user.role.eql? Constants::ADMIN_DB_LABEL
                    userManager.sendEmail(user, userPassword, admin_roles)
                end  

                ActivityLog.saveActivity((I18n.t :USER_CREATE_ACTIVITY_MSG, :role => User.getRoleDisValue(user.role)), loggedInAdmin.id, user.id, params)
            end    
            
		  response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
          response.Response_Msg=(I18n.t :USER_SUCCESSFULLY_CREATED_MSG)
		return response
 	end 

    def self.validateCreateUser(user, admin_roles)
        response = Response.new

            # this is used to check that the micro franchisee that is terminated will not be register again 
            # and so we check that his national id if the national id is matchec with terminated user then the user cant be registered

            isNationalIDPresent = User.where("role = #{Constants::MF_USER_ROLE} and status = #{Constants::USER_DELETE_STATUS} and national_id = '#{user.national_id}'")
            if isNationalIDPresent.present?
                    response.Response_Code=(Error::NATIONAL_ID_ALREADY_EXIST)
                    response.Response_Msg=(I18n.t :USER_TERMINATED_MSG) 
                return response
            end  
           
            isEmailExist = User.where("email = '#{user.email}'").first
            if isEmailExist.present?
                  response.Response_Code=(Error::EMAIL_ID_ALREADY_EXIST)
                  response.Response_Msg=(I18n.t :EMAIL_ID_ALREADY_EXIST_MSG) 
                return response
            end  
            
            if user.role.eql? Constants::AGENT_DB_LABEL
                isNationalIDExist = User.where("national_id = '#{user.national_id}'").where("status IN (#{Constants::USER_ACTIVE_STATUS}, #{Constants::USER_SUSPEND_STATUS})")
                if isNationalIDExist.present?
                        response.Response_Code=(Error::NATIONAL_ID_ALREADY_EXIST)
                        response.Response_Msg=(I18n.t :NATIONAL_ID_ALREADY_EXIST_MSG) 
                    return response
                end 

                isTinNoExist = User.where("tin_no = #{user.tin_no}").where("status IN (#{Constants::USER_ACTIVE_STATUS}, #{Constants::USER_SUSPEND_STATUS})")
                if isTinNoExist.present?
                        response.Response_Code=(Error::TIN_ALREADY_EXIST)
                        response.Response_Msg=(I18n.t :TIN_ALREADY_EXIST_MSG) 
                    return response
                end 

                if user.email == ''
                    user.email = TxnManager.getRandomEmail
                    user.email_flag = Constants::USER_EMPTY_EMAIL_FLAG
                end 
            elsif user.role.eql? Constants::ADMIN_DB_LABEL
                if !admin_roles.present?
                        response.Response_Code=(Error::NO_ADMIN_ROLE_SELECT)
                        response.Response_Msg=(I18n.t :NO_ADMIN_ROLE_SELECT_MSG) 
                    return response
                end    
            end 

            isPhoneExist = User.where("phone = '#{user.phone}'")
            if isPhoneExist.present?
                    # again set the email is empty if MF has not any email bcoz when render to previous page then on email it show the random email thats y do it
                    if user.role.eql? Constants::AGENT_DB_LABEL
                        user.email = '' 
                    end
                    response.Response_Code=(Error::PHONE_ALREADY_EXIST)
                    response.Response_Msg=(I18n.t :PHONE_ALREADY_EXIST_MSG) 
                return response
            end  

            if !Utility.isValidYear(user.date_of_birth.to_s)
                    response.Response_Code=(Error::INVALID_DATE)
                    response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
                return response
            end

            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("") 
        return response
    end     

 	def getUserPassword(user) # get the random password for Micro franchisee and admin 
	    if user.role.eql? Constants::AGENT_DB_LABEL
	           expersion = [('0'..'9')].map { |i| i.to_a }.flatten
	           random_password = (0...4).map { expersion[rand(expersion.length)] }.join
	        return random_password
        else
	           admin_expersion = [('a'..'z'),  ('0'..'9')].map { |i| i.to_a }.flatten
	           random_password_admin = (0...8).map { admin_expersion[rand(admin_expersion.length)] }.join
       	    return random_password_admin
        end  
 	end	

    def self.updateUser(user, email, phone, user_id, tin, national_id, user_status, date_of_birth, loggedInAdmin, params)

        response = Response.new

            if email == ''
                user.email_flag = Constants::USER_EMPTY_EMAIL_FLAG
                user.email = TxnManager.getRandomEmail
            else    
                isEmailPresent = User.where("email = '#{email}'").first
                if isEmailPresent.present?
                    if isEmailPresent.id != user_id 
                          response.Response_Code=(Error::EMAIL_ID_ALREADY_EXIST)
                          response.Response_Msg=(I18n.t :EMAIL_ID_ALREADY_EXIST_MSG) 
                        return response
                    else
                        user.email = email
                        user.email_flag = 0
                    end
                else
                   user.email = email
                   user.email_flag = 0 
                end 
            end

            isPhoneExist = User.where("phone = '#{phone}'").first
            if isPhoneExist.present?
                if isPhoneExist.id != user_id
                        response.Response_Code=(Error::PHONE_ALREADY_EXIST)
                        response.Response_Msg=(I18n.t :PHONE_ALREADY_EXIST_MSG) 
                    return response
                elsif user.phone != phone
                    sendMsg_update(user,phone)
                end
            else
                if user.phone != phone
                    sendMsg_update(user,phone)
                end 
            end  

            if user.role.eql? Constants::AGENT_DB_LABEL
                isNationalIDPresent = User.where("national_id = '#{national_id}'").where("role = #{Constants::MF_USER_ROLE}").where("status IN (#{Constants::USER_ACTIVE_STATUS}, #{Constants::USER_SUSPEND_STATUS})").first
                if isNationalIDPresent.present?
                    if isNationalIDPresent.id != user_id
                            response.Response_Code=(Error::NATIONAL_ID_ALREADY_EXIST)
                            response.Response_Msg=(I18n.t :NATIONAL_ID_ALREADY_EXIST_MSG) 
                        return response
                    end 
                end 

                isTinNoExist = User.where("tin_no = '#{tin}'").where("role = #{Constants::MF_USER_ROLE}").where("status IN (#{Constants::USER_ACTIVE_STATUS}, #{Constants::USER_SUSPEND_STATUS})").first
                if isTinNoExist.present?
                    if isTinNoExist.id != user_id
                            response.Response_Code=(Error::TIN_ALREADY_EXIST)
                            response.Response_Msg=(I18n.t :TIN_ALREADY_EXIST_MSG) 
                        return response
                    end 
                end   

                #Send the notification on suspend and delete
                checkPreviousStatus = User.find(user.id)
                if user.role.eql? Constants::AGENT_DB_LABEL and checkPreviousStatus.status.eql? Constants::ACTIVE_DB_LABEL
                    if user_status.eql? Constants::SUSPEND__DB_LABEL
                        Utility.sendNotification(user_id, (I18n.t :ACCOUNT_SUSPEND_NOTI_MSG), (I18n.t :ACCOUNT_SUSPEND_NOTI_TITLE))
                    end    
                elsif user.role.eql? Constants::AGENT_DB_LABEL and user_status.eql? Constants::DELETE__DB_LABEL
                    Utility.sendNotification(user_id, (I18n.t :ACCOUNT_DELETE_NOTI_MSG), (I18n.t :ACCOUNT_DELETE_NOTI_TITLE))
                end        
            end 

             if !Utility.isValidYear(date_of_birth.to_s)
                    response.Response_Code=(Error::INVALID_DATE)
                    response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
                return response
            end

            ActivityLog.saveActivity((I18n.t :USER_UPDATE_ACTIVITY_MSG, :role => User.getRoleDisValue(user.role)), loggedInAdmin.id, user.id, params)

            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=(I18n.t :USER_SUCCESSFULLY_UPDATED_MSG)
        return response

    end

 	def getUserAccount(user) 
 		userAccount = UserAccount.new
            userAccount.user_id  = user.id
            userAccount.acc_type = Constants::USER_ACCOUNT_TYPE
            userAccount.currency = Constants::USER_ACCOUNT_CURRENCY
            userAccount.balance  = Constants::USER_ACCOUNT_INITIAL_BALANCE
        return userAccount         
 	end	

 	def getUserLoan(user)
 		loan =  Loan.new
            loan.user_id  = user.id
            loan.acc_type = Constants::USER_ACCOUNT_TYPE
            loan.currency = Constants::USER_ACCOUNT_CURRENCY
            loan. amount  = Constants::USER_INITIAL_LOAN_AMOUNT
            loan.balance  = Constants::USER_INITIAL_LOAN_BALANCE
            loan.due_date = Constants::USER_INITIAL_LOAN_DUE_DATE
        return loan  
 	end	

 	def getUserCreditLimit(user)
        agent_credit_limit = AgentCreditLimit.new
            agent_credit_limit.user_id=user.id
            agent_credit_limit.loan_capacity_date = user.created_at
            agent_credit_limit.loan_amount = Constants::USER_INITIAL_MAX_LOAN_LIMIT
            agent_credit_limit.status = 0
        return agent_credit_limit  
 	end


 	def sendEmail(user,user_password,admin_roles)
        message = getMessage(user,user_password)  
        if admin_roles != nil
            admin_roles.each do |e|
   	          RoleHistory.create(role_id: e,user_id: user.id)
            end
        end
        
        Utility.sendEmail(Properties::SUPERADMIN_EMAIL, user.email, "Successfully registered", message)
    end 


 	def sendMsg(user,user_password,sms_purpose)
        message = getMessage(user,user_password)  
        country_code = Properties::COUNTRYCODE
        mobile_no = country_code.to_s + user.phone.to_s
        
        t = Thread.new do   
            response = SmsUtility.sendSmsReg(mobile_no,message)
                if response == 'true'
                    response_status = Constants::SENT_SMS_SUCCESS_RESPONSE_STATUS
                else
                    response_status = Constants::SENT_SMS_FAILURE_RESPONSE_STATUS
                end  
            
            saveUserSMS(user,message,response_status,sms_purpose)
        end    
    end 

    def getMessage(user,user_password)
    	if user.role == 'admin'
            message =   'Dear ' + user.name.to_s + ' ' + user.last_name.to_s + ',
                            You are  successfully registered to M-Shiriki.
                            Username : ' + user.email.to_s + '
                            Password : ' + user_password.to_s + ' (change the password after login)
                            Please write down carefully the Username and Password, both are case-sensitive.
                            Login to ' + Properties::SENT_EMAIL_MSG_LOGIN_LINK.to_s
    	elsif user.role == 'agent'
    		country_code = Properties::COUNTRYCODE
            mobile_no = country_code.to_s + user.phone.to_s
            message =   'Dear ' + user.name.to_s + ' ' + user.last_name.to_s + ',
                            Welcome to M-SHIRIKI.
                            Your four digit PIN is ' + user_password +
                            ' . Please do not share this PIN with anyone.'
    	end
      return message		
    end           

    def saveUserSMS(user,message,response_status,sms_purpose)
    	sms_reg =SmsRegistration.new
	        sms_reg.mobile_no = user.phone.to_s
	        sms_reg.purpose = sms_purpose
	        sms_reg.message = message
	        sms_reg.deliver_on = Date.today
	        sms_reg.status = response_status
        sms_reg.save
    end

    def self.sendMsg_update(user,phone)
        userPhone = Properties::COUNTRYCODE.to_s + phone.to_s
        message = (I18n.t :USER_PHONE_UPDATE_MSG, :agentName => Utility.getAgentFullName(user), :phone => userPhone)

        Thread.new do  
            response = SmsUtility.sendSmsReg(userPhone, message)
            if response == 'true'
                response_status = Constants::SENT_SMS_SUCCESS_RESPONSE_STATUS
            else
                response_status = Constants::SENT_SMS_FAILURE_RESPONSE_STATUS
            end  
            
            saveUserSMS(user,message,response_status,Constants::PHONECHANGESMS)
        end    
    end

    def self.updateAdminRoles(user, adminRoles)
        userRoles = RoleHistory.where(user_id: user.id)
        if userRoles.present?
            userRoles.each do |role|
         	    role.destroy
        	end
    	end

        if adminRoles.present?
            adminRoles.each do |newRoleId|
             	RoleHistory.create(role_id: newRoleId, user_id: user.id)
         	end
        end
    end	

    def getViewAdminRolesStatus(user,user_bar)
    	status = 0

	    #This case is new user
        if user.id == nil
            if user.role == 'admin'
                status = 1
            elsif user.role == 'agent'
                status = 0  
            end  
        end 

        #This case is for update 
        if user.id != nil
            if user.role == 'admin'
               status = 1
            elsif user.role == 'agent'
               status = 0  
            end  
        end 

        #This case is for assign admin roles
        if user_bar.present? 
            if user_bar == 'agent' 
              status = 0
            end
        end      
   	  return status
    end

     #######################End of form functions

    def self.deleteUser(userID, loggedInAdmin, params)
    	
        response = Response.new

        	current_agent = User.find(userID)
            accountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(current_agent)).to_f
            loanBalance = TxnHeader.my_money(Utility.getAgentLoanBalance(current_agent)).to_f
            commBalance = TxnHeader.my_money(Utility.getAgentCommissionBalance(current_agent)).to_f
            govtAccBalance = TxnHeader.my_money(Utility.getAgentGovtAccountBalance(current_agent)).to_f
            penaltyBalance = TxnHeader.my_money(Utility.getAgentPenaltyBalance(current_agent)).to_f
    	    
            ActiveRecord::Base.transaction do
                if accountBalance == 0 && loanBalance == 0 && commBalance == 0 && penaltyBalance == 0 && govtAccBalance == 0
                    updateAgentDeletedPhone(current_agent)
        	    
        	        updateAgentDeletedEmail(current_agent)

        	        current_agent.update_attribute(:status, 1)

                    deletedPhoneIMEI(current_agent)

                    if current_agent.role.eql? Constants::AGENT_DB_LABEL
                        Utility.sendNotification(userID, (I18n.t :ACCOUNT_DELETE_NOTI_MSG), (I18n.t :ACCOUNT_DELETE_NOTI_TITLE))
                    end    

                    ActivityLog.saveActivity((I18n.t :USER_DELETE_ACTIVITY_MSG, :role => User.getRoleDisValue(current_agent.role)), loggedInAdmin.id, current_agent.id, params)
        	       
        	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		    response.Response_Msg=(I18n.t :USER_SUCCESSFULLY_DELETED_MSG)
            	else
        	        msg = 'The agent '+ current_agent.name + ', phone ' + current_agent.phone + ' has balance in account, please settle it first.'
        	        response.Response_Code=(Error::USER_DELETE_FAILED)
        		    response.Response_Msg=(msg) 
           		end
            end

        return response

    end 

    def self.updateAgentDeletedPhone(currentAgent)
        phone = currentAgent.phone
        phoneQuery = phone.to_s + '(%'.to_s

        isPhonePresent = User.where("phone like ?", phoneQuery).last
        if isPhonePresent.present?
            db_last_phone = isPhonePresent.phone

            ph_first = db_last_phone.split("(",2);
            ph_second = ph_first[1].split(")",2)
            org = ph_second[0]  
            org = org.to_i + 1  
            phone = phone.to_s + '(' + org.to_s + ')'
            currentAgent.update_attribute(:phone, phone)
        else
            phone = phone.to_s + '(1)'  
            currentAgent.update_attribute(:phone, phone)
        end 
    end    

    def self.updateAgentDeletedEmail(currentAgent)
        email = currentAgent.email
        
        email_query = email.to_s + '(%'.to_s
        
        isEmailPresent = User.where("email like ?", email_query).last

        if isEmailPresent.present?
          db_last_email = isEmailPresent.email
          email_first = db_last_email.split("(",2);
          email_second = email_first[1].split(")",2)
          org_email = email_second[0]  
          org_email = org_email.to_i + 1  
          email = email.to_s + '(' + org_email.to_s + ')'
          currentAgent.update_attribute(:email, email)
        else
          email = email.to_s + '(1)'  
          currentAgent.update_attribute(:email, email)
        end  
    end

    def self.deletedPhoneIMEI(currentAgent)
        serial_number = SerialNumber.where('user_id = ?', currentAgent.id)
        if serial_number.present?
            serial_number.each do |serial_no|
               serial_no.update_attribute(:active_flag, 2) 
            end    
        end   
    end

    def self.suspendUser(user_id, loggedInAdmin, params)
        response = Response.new
        	user = User.find(user_id)
        	if user.present?
         		
                user.update_attribute(:status, 3)
                
                Utility.sendNotification(user_id, (I18n.t :ACCOUNT_SUSPEND_NOTI_MSG), (I18n.t :ACCOUNT_SUSPEND_NOTI_TITLE))
                ActivityLog.saveActivity((I18n.t :USER_SUSPEND_ACTIVITY_MSG, :role => User.getRoleDisValue(user.role)), loggedInAdmin.id, user.id, params)

    			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    		    response.Response_Msg=(I18n.t :USER_SUCCESSFULLY_SUSPENDED_MSG)
         	else
         		response.Response_Code=(Error::USER_SUSPEND_FAILED)
    		    response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
         	end
        return response
    end	

    def self.activateMFPhone(userID, id, loggedInAdmin, params)
    	serialNumber = SerialNumber.where(user_id: userID)
		if serialNumber.present?
  			serialNumber.each do |serial_no|
    			if serial_no.id.to_i != id.to_i
      				serial_no.destroy
                    Utility.sendNotification(userID, (I18n.t :DEVICE_DEACTIVATE_NOTI_MSG), (I18n.t :DEVICE_DEACTIVATE_NOTI_TITLE))
                else
                    Log.logger.info("Inside the UserManager class's activateMFPhone At line #{__LINE__} and current device activate is #{serial_no.serial_number}")
                    serial_no.update_attributes(active_flag:1) 
                    ActivityLog.saveActivity((I18n.t :USER_DEVICE_ACTIVATE_ACTIVITY_MSG, :IMEI => serial_no.serial_number), loggedInAdmin.id, userID, params)   
   				end
	      	end
		end
    end	

    def self.deActivateMFPhone(userID, id, loggedInAdmin, params)
        serialNumber=SerialNumber.where(user_id: userID)
        if serialNumber.present?
            serialNumber.each do |serial_no|
                serial_no.update_attributes(active_flag: 0)
                
                ActivityLog.saveActivity((I18n.t :USER_DEVICE_DEACTIVATE_ACTIVITY_MSG, :IMEI => serial_no.serial_number), loggedInAdmin.id, userID, params)   
                
                Utility.sendNotification(userID, (I18n.t :DEVICE_DEACTIVATE_NOTI_MSG), (I18n.t :DEVICE_DEACTIVATE_NOTI_TITLE))
            end
        end
    end

    def self.validateNationalID(params)
        AppLog.logger.info("At line #{__LINE__} inside the UserManager's  and validateNationalID method is called")
        response = Response.new
        agentPhone = params[""+AppConstants::PHONE_PARAM_LABEL+""]
        if !agentPhone.present?
                AppLog.logger.info("At line = #{__LINE__} and agentPhone is come nil from the app")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end

        currentAgent = User.find_by_phone(agentPhone)
        AppLog.logger.info("At line  #{__LINE__} and now check that the agent with this phone = #{agentPhone} is present or not = #{currentAgent.present?}")

        if currentAgent.present?
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.object=(currentAgent)
            return response
        else
                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
            return response
        end 
    end  


    def self.isUserExist(email, request)

        response = Response.new
        
        user = User.find_by_email(email)
        SymboxLog.logger.info("At line #{__LINE__} check whether the user with this email = #{email} is exist or not =#{user.present?}") 

        if user.present?

            accessID = Utility.addDataIntoAccessLogs(user.id, User.getUserRoleIntValue(user), Constants::LOGIN_THROUGH_WEB_STATUS, request)
                
            if user.role.eql? Constants::ADMIN_DB_LABEL

                if user.status.eql? Constants::ACTIVE_DB_LABEL

                        Utility.updateAccessLoginStatus(accessID)
                        SymboxLog.logger.info("At line #{__LINE__} this user is successfully validate") 

                        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                        response.object=(user)
                    return response

                else

                        SymboxLog.logger.info("At line #{__LINE__} this user is not active by the superadmin") 
                        response.Response_Code=(SymboxErrorCodes::AUTHORIZATION_FAILED)
                        response.Response_Msg=(I18n.t :DO_NOT_HAVE_ACCESS_TO_LOGIN_MSG) 
                    return response

                end   

            else

                    SymboxLog.logger.info("At line #{__LINE__} this user is not admin") 
                    response.Response_Code=(SymboxErrorCodes::AUTHORIZATION_FAILED)
                    response.Response_Msg=(I18n.t :DO_NOT_HAVE_ACCESS_TO_LOGIN_MSG) 
                return response

            end
            
        else

                SymboxLog.logger.info("At line #{__LINE__} email is not match with the database") 
                Utility.addDataIntoAccessLogs(Constants::ANONYMOUS_LOGIN_USER_ID, Constants::ANONYMOUS_LOGIN_USER_ROLE, Constants::LOGIN_THROUGH_WEB_STATUS, request)

                response.Response_Code=(SymboxErrorCodes::AUTHENTICATION_FAILED)
                response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
            return response
            
        end

    end    
      
end	