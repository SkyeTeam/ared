# This class used as a Manager in MVC
# :reek:ClassVariable { enabled: false }

class Log

  require 'properties'	 	 

  def self.logger
    @@logger ||= Logger.new(Properties::LOGS_FILE_LOCATION , 'daily')
  end

end	