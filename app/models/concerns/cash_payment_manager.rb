# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 45 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class CashPaymentManager

	def self.cashPayment(agentID, approvedAmount, description, extRef, loggedInAdmin, request)
 		Log.logger.info("At line #{__LINE__} inside the CashPaymentManager's cashPayment method is called with parameters are user id = #{agentID}, payment amount = #{approvedAmount}, description = #{description} and external reference = #{extRef}")   
		
			currentAgent = User.find(agentID)
			txnUniqueID = Utility.getRandomNumber(15)
			
	        accBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent))
	        govtAccBalance = TxnHeader.my_money(Utility.getAgentGovtAccountBalance(currentAgent))
	        commissionBalance = TxnHeader.my_money(Utility.getAgentCommissionBalance(currentAgent))
	        loanBalance = TxnHeader.my_money(Utility.getAgentLoanBalance(currentAgent))
	      	penaltyBalance = TxnHeader.my_money(Utility.getAgentPenaltyBalance(currentAgent))
	        Log.logger.info("At line #{__LINE__} and Account balance = #{accBalance}, Govt Accont balance = #{govtAccBalance}, Commission balance = #{commissionBalance} , Loan balance = #{loanBalance} , Penalty balance = #{penaltyBalance}")
	      	
	        totalPayableAmount = Utility.getTotalPayableAmount(accBalance, commissionBalance, govtAccBalance, loanBalance, penaltyBalance)
	      	Log.logger.info("At line #{__LINE__} and Total Payable Amount = #{totalPayableAmount}")

	      	response = validateCashPayment(currentAgent, approvedAmount, description, extRef, totalPayableAmount)
		    if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
		      	return response
		    end  

	        AppUtility.savePendingRequest(txnUniqueID, Constants::USER_CASH_PAYMENT_LABLE, Constants::USER_CASH_PAYMENT_LABLE, nil, approvedAmount, currentAgent.id, Utility.getUserRemoteIP(request), loggedInAdmin.id, Constants::ADMIN_USER_ROLE)
	        Log.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")

		    ActiveRecord::Base.transaction do
		      	
		      	Log.logger.info("At line #{__LINE__} and now commission is credit to the agent account if the commission balance is present = #{commissionBalance}")
		      	if commissionBalance.to_f != 0
		      		creditCommToAgent(currentAgent, commissionBalance)
			    end

			    Log.logger.info("At line #{__LINE__} and now govt acc balance is credit to the agent account if the govt acc balance is present = #{govtAccBalance}")
		      	if govtAccBalance.to_f != 0
		      		creditGovtBalanceToAgent(currentAgent, govtAccBalance)
			    end

			    Log.logger.info("At line #{__LINE__} and now loan is adjust if the loan balance is present = #{loanBalance}")
			    if loanBalance.to_f != 0
			    	accountBalance = Utility.getAgentAccountBalance(currentAgent)
			    	Log.logger.info("At line #{__LINE__} inside the if loanBalance != 0 and  account balance of the agent after commission settlement = #{accountBalance} and loan balance is = #{loanBalance}")

			    	if accountBalance.to_f >= loanBalance.to_f
			    		settleLoan(TxnHeader.my_money(loanBalance), currentAgent, (I18n.t :LOAN_SETTLE_REIMBURSEMENT_DESC), "")
					else
						Log.logger.info("At line #{__LINE__} inside the else of accountBalance >= loanBalance and account balance after commission settlement = #{accountBalance} and loan balance is = #{loanBalance}")
						if accountBalance.to_f != 0	
							settleLoan(TxnHeader.my_money(accountBalance), currentAgent, (I18n.t :LOAN_SETTLE_REIMBURSEMENT_DESC), "")
						end

						amountDifference = TxnHeader.my_money(Utility.getAgentLoanBalance(currentAgent)).to_f - TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
						Log.logger.info("At line #{__LINE__} and amount difference between Loan balance and account balance is = #{amountDifference}")

						setteNonPaymentLoan(TxnHeader.my_money(amountDifference),currentAgent,(I18n.t :LOAN_SETTLE_REIMBURSEMENT_DESC),"")
					end	
			    end

			    Log.logger.info("At line #{__LINE__} and now penalty is adjust if the penalty balance is present = #{penaltyBalance}")
			    if penaltyBalance.to_f != 0
			    	accBalanceAfterLoanSettle = Utility.getAgentAccountBalance(currentAgent)
			    	Log.logger.info("At line #{__LINE__} and account balance of the agent after loan settlement and inside the if penaltyBalance != 0 = #{accBalanceAfterLoanSettle}")

			    	if accBalanceAfterLoanSettle.to_f >= penaltyBalance.to_f
			    		settlePenalty(TxnHeader.my_money(penaltyBalance),currentAgent,(I18n.t :PENALTY_SETTLE_REIMBURSEMENT_DESC),"")
			    	else
			    		if accBalanceAfterLoanSettle.to_f != 0	
			    			settlePenalty(TxnHeader.my_money(accBalanceAfterLoanSettle),currentAgent,(I18n.t :PENALTY_SETTLE_REIMBURSEMENT_DESC),"")
			    		end	

			    		penaltyAmountDifference = TxnHeader.my_money(Utility.getAgentPenaltyBalance(currentAgent)).to_f - TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
			    		settleNonPaymentPenalty(TxnHeader.my_money(penaltyAmountDifference),currentAgent)
			    	end	
				
				end

				Log.logger.info("At line #{__LINE__} and now remaining amount is give to the agent and his account is debit")
				if approvedAmount.to_f != 0
					paymentToAgent(currentAgent, approvedAmount, description, extRef)
		        end

		        AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
	        	Log.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")

		    end

		   	Utility.sendNotification(currentAgent.id, (I18n.t :AGENT_ACCOUNT_DEBIT_MSG), Constants::CASH_DEPOSIT_NOTIFICATION_TITLE)

		    response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		    response.Response_Msg=(I18n.t :AGENT_ACCOUNT_DEBIT_MSG)
	  	return response	
	end 

	def self.validateCashPayment(currentAgent, approvedAmount, description, externalRef, totalPayableAmount)
		Log.logger.info("At line #{__LINE__} inside the CashPaymentManager and validateCashPayment method is called")
		
		response = Response.new

			if totalPayableAmount > 0
				if !approvedAmount.present? || !description.present? || !externalRef.present?
						Log.logger.info("At line #{__LINE__} and some values are empty")
						response.Response_Code=(Error::INVALID_DATA)
		    	    	response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
		       		return response
				end 

				if approvedAmount.to_f <= 0
	      				Log.logger.info("At line #{__LINE__} and approvedAmount is less then zero")
		        		response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
	    	    		response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
	       			return response
	      		end
			end	

	      	if approvedAmount > totalPayableAmount && totalPayableAmount > 0
		    		Log.logger.info("At line #{__LINE__} and approvedAmount is more then totalPayableAmount and return back from here")
		        	response.Response_Code=(Error::AMOUNT_MORE_THEN_PAYABLE)
			    	response.Response_Msg=(I18n.t :AMOUNT_MORE_THEN_PAYABLE_MSG) 
		  		return response
	    	end

	      	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	      	response.Response_Msg=("") 
        return response  
	end	

	def self.creditCommToAgent(currentAgent, commissionBalance)
		txnHeader = getHeader(TxnHeader.my_money(commissionBalance), Constants::USER_COMMISSION_PAYMENT_LABLE, (I18n.t :COMMISSION_REIMBURSEMENT_DESC), currentAgent.id,"")
        txnHeader.save
	    Log.logger.info("At line #{__LINE__} and Commission Payment entry is saved in the TxnHeader table successfully")

	    agentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(commissionBalance), false, Utility.getCurrentAgentAccount(currentAgent))
	    agentTxnItem.header_id = txnHeader.id
	    agentTxnItem.save
	    Log.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

		optComTxnItem  = getTxnItem(Utility.getCommissionAdmin, TxnHeader.my_money(commissionBalance), true, Utility.getCommissionAdminAccount(Utility.getCommissionAdmin))
	    optComTxnItem.header_id = txnHeader.id;
	    optComTxnItem.save
        Log.logger.info("At line #{__LINE__} and Operator(Commission) transaction item is saved in the TxnItem table successfully")

        getAgentCommission(currentAgent, commissionBalance, txnHeader.id)

      	TxnManager.update_account(Utility.getCommissionAdminAccount(Utility.getCommissionAdmin), TxnHeader.my_money(commissionBalance), true)
  		TxnManager.update_account( Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(commissionBalance), false)  
	    Log.logger.info("At line #{__LINE__} and balances of Agent and Operator Commission Account are updated successfully")
	end	

	def self.creditGovtBalanceToAgent(currentAgent, govtAccBalance)
		txnHeader = getHeader(TxnHeader.my_money(govtAccBalance).to_f, Constants::GOVT_FUNDS_WITHDRAWAL_DB_LABEL, (I18n.t :GOVT_FUNDS_WITHDRAWAL_DESC), currentAgent.id, "")
        txnHeader.save
	    Log.logger.info("At line #{__LINE__} and Commission Payment entry is saved in the TxnHeader table successfully")

	    AppUtility.getTxnItem(txnHeader.id, Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(govtAccBalance).to_f, nil, Constants::PAYEE_LABEL);
	    Log.logger.info("At line #{__LINE__} and Agent(M-SHIRIKI Account) transaction item is saved in the TxnItem table successfully")

	    AppUtility.getTxnItemGovtAcc(txnHeader.id, Utility.getUserGovtAccount(currentAgent), TxnHeader.my_money(govtAccBalance).to_f, nil, Constants::PAYER_LABEL)
        Log.logger.info("At line #{__LINE__} and Agent(Govt Account) transaction item is saved in the TxnItem table successfully")

      	TxnManager.update_govt_bank_account(currentAgent.id, TxnHeader.my_money(govtAccBalance).to_f, true);
  		TxnManager.update_account( Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(govtAccBalance), false)  
	    Log.logger.info("At line #{__LINE__} and balances of Agent(M-SHIRIKI Account) and Agent(Govt Account) are updated successfully")
	end	

	def self.getAgentCommission(currentAgent, commissionBalance, headerID)
		agentCommission = AgentCommission.new
	        agentCommission.user_id = currentAgent.id
	        agentCommission.service_type = Constants::USER_COMMISSION_PAYMENT_LABLE
	        agentCommission.sub_type = Constants::USER_COMMISSION_PAYMENT_LABLE
	        agentCommission.txn_date = Time.now
	        agentCommission.previous_balance = TxnHeader.my_money(commissionBalance)
	        agentCommission.post_balance = TxnHeader.my_money(commissionBalance).to_f - TxnHeader.my_money(commissionBalance).to_f
	        agentCommission.header_id = headerID
	        agentCommission.status = 1
	    	agentCommission.payment_ref = headerID
	    	agentCommission.acc_balance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent))
			agentCommission.loan = TxnHeader.my_money(Utility.getAgentLoanBalance(currentAgent))
			agentCommission.penalty = TxnHeader.my_money(Utility.getAgentPenaltyBalance(currentAgent))
			agentCommission.total_comm = TxnHeader.my_money(commissionBalance)
        agentCommission.save
        Log.logger.info("At line #{__LINE__} and commission entry of the agent is saved in the agent_commission table successfully")      	

       	commissionPaidIDs = AgentCommission.where('user_id = ? and status = 0', currentAgent.id)
	    commissionPaidIDs.each do |commission|    
	        commission.update_attribute(:status, 1)
	        commission.update_attribute(:payment_ref, headerID)
	    end 
	    Log.logger.info("At line #{__LINE__} and status of all the transaction is update to 1 in the agent_commission table successfully") 
	end	

	def self.settleLoan(loanBalance, currentAgent, description, extRef)
		Log.logger.info("In settleLoan method and loan settle balance = #{loanBalance}")

		loanTxnHeader = getHeader(TxnHeader.my_money(loanBalance), Constants::USER_LOAN_DEPOSIT_LABLE, description, currentAgent.id, extRef)
      	loanTxnHeader.save
      	Log.logger.info("At line #{__LINE__} and Loan Deposit(loan_deposit) entry is saved in the TxnHeader table successfully")

	    agentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(loanBalance), true, Utility.getCurrentAgentAccount(currentAgent))
	  	agentTxnItem.header_id = loanTxnHeader.id
      	agentTxnItem.save
	  	Log.logger.info("At line #{__LINE__} and Agent loan transaction item is saved in the TxnItem table successfully")

        optStoclTxnItem  = getTxnItem(Utility.getStockAdmin(), TxnHeader.my_money(loanBalance), false, Utility.getStockAdminAccount(Utility.getStockAdmin()))
	  	optStoclTxnItem.header_id = loanTxnHeader.id;
      	optStoclTxnItem.save
      	Log.logger.info("At line #{__LINE__} and Operator Stock loan transaction item is saved in the TxnItem table successfully")

        loanRecord = getLoanRecord(currentAgent, TxnHeader.my_money(loanBalance), Utility.getCurrentAgentLoanAccount(currentAgent),0)
      	loanRecord.save
      	Log.logger.info("At line #{__LINE__} and loan entry of the agent is saved in the Loans table successfully")      	

   		TxnManager.update_account(Utility.getStockAdminAccount(Utility.getStockAdmin()), TxnHeader.my_money(loanBalance), false)
  		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(loanBalance), true)  
  		Log.logger.info("At line #{__LINE__} and balances of Agent and Operator Stock Account are updated successfully")
	end	

	def self.setteNonPaymentLoan(loanBalance, currentAgent, description, extRef)
		txnHeader = getHeader(TxnHeader.my_money(loanBalance), Constants::USER_NON_PAYMENT_LOAN_LABLE, (I18n.t :NON_RECOVERY_LOAN_MSG), currentAgent.id, "")
      	txnHeader.save
      	Log.logger.info("At line #{__LINE__} and Non Payment Loan(nonpayemt_loan) entry is saved in the TxnHeader table successfully")

        optStockTxnItem = getTxnItem(Utility.getStockAdmin(), TxnHeader.my_money(loanBalance), false, Utility.getStockAdminAccount(Utility.getStockAdmin()))
      	optStockTxnItem.header_id = txnHeader.id
      	optStockTxnItem.save
      	Log.logger.info("At line #{__LINE__} and Operator Stock transaction item is saved in the TxnItem table successfully")
       
        optPLTxnItem  = getTxnItem(Utility.getProfitAndLoseAdmin(), TxnHeader.my_money(loanBalance), true, Utility.getProfitAndLoseAdminAccount(Utility.getProfitAndLoseAdmin()))
      	optPLTxnItem.header_id = txnHeader.id;
      	optPLTxnItem.save
		Log.logger.info("At line #{__LINE__} and Operator Profit and lose  transaction item is saved in the TxnItem table successfully")

 		loan_record = getLoanRecord(currentAgent, TxnHeader.my_money(loanBalance), Utility.getCurrentAgentLoanAccount(currentAgent),1)
      	loan_record.save
      	Log.logger.info("At line #{__LINE__} and loan entry of the agent is saved in the Loans table successfully")      	

  		TxnManager.update_account(Utility.getProfitAndLoseAdminAccount(Utility.getProfitAndLoseAdmin()), TxnHeader.my_money(loanBalance), true)
  		TxnManager.update_account(Utility.getStockAdminAccount(Utility.getStockAdmin()), TxnHeader.my_money(loanBalance), false)  
		Log.logger.info("At line #{__LINE__} and balances of Operator(Profit and Lose) and Operator(Stock) Account are updated successfully")
	end	

	def self.settlePenalty(penaltyBalance,currentAgent,description,extRef)
 		txnHeader = getHeader(TxnHeader.my_money(penaltyBalance), Constants::USER_PENALTY_CHARGE_LABLE, description,currentAgent.id,extRef)
        txnHeader.save
        Log.logger.info("At line #{__LINE__} and Penalty Deposit(delay_charge) entry is saved in the TxnHeader table successfully")

        agentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(penaltyBalance), true, Utility.getCurrentAgentAccount(currentAgent))
        agentTxnItem.header_id = txnHeader.id
        agentTxnItem.save
        Log.logger.info("At line #{__LINE__} and Agent Penalty transaction item is saved in the TxnItem table successfully")

        optPenaltyTxnItem  = getTxnItem(Utility.getPenaltyAdmin(), TxnHeader.my_money(penaltyBalance), false, Utility.getPenaltyAdminAccount(Utility.getPenaltyAdmin()))
        optPenaltyTxnItem.header_id = txnHeader.id;
        optPenaltyTxnItem.save
        Log.logger.info("At line #{__LINE__} and Operator Penalty transaction item is saved in the TxnItem table successfully")

        penaltyRecord = getPenaltyRecord(currentAgent, TxnHeader.my_money(penaltyBalance), Utility.getCurrentAgentPenaltyAccount(currentAgent), 0)
        penaltyRecord.header_id = txnHeader.id
        penaltyRecord.save
        Log.logger.info("At line #{__LINE__} and penalty entry of the agent is saved in the agent_penalty table successfully")      	

       	TxnManager.update_account(Utility.getPenaltyAdminAccount(Utility.getPenaltyAdmin()), TxnHeader.my_money(penaltyBalance), false)
  		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(penaltyBalance), true)  
        Log.logger.info("At line #{__LINE__} and balances of Operator(Penalty) and Agent Account are updated successfully")
	end		

	def self.settleNonPaymentPenalty(penaltyBalance,currentAgent)
 		txnHeader = getHeader(TxnHeader.my_money(penaltyBalance), Constants::USER_NON_PAYMENT_PENALTY_LABLE, (I18n.t :NON_RECOVERY_PENALTY_MSG),currentAgent.id,"")
 		txnHeader.save
 		Log.logger.info("At line #{__LINE__} and Non Payment Penalty Deposit(nonpayemt_penalty) entry is saved in the TxnHeader table successfully")

        penaltyRecord = getPenaltyRecord(currentAgent, TxnHeader.my_money(penaltyBalance), Utility.getCurrentAgentPenaltyAccount(currentAgent), 1)
        penaltyRecord.header_id = txnHeader.id
        penaltyRecord.save
        Log.logger.info("At line #{__LINE__} and penalty entry of the agent is saved in the agent_penalty table successfully")
	end	

	def self.paymentToAgent(currentAgent, approvedAmount, description, extRef)
		txnHeader = getHeader(TxnHeader.my_money(approvedAmount), Constants::USER_CASH_PAYMENT_LABLE, description, currentAgent.id, extRef)
    	txnHeader.save
		Log.logger.info("At line #{__LINE__} and Cash Payment entry is saved in the TxnHeader table successfully")

		agentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(approvedAmount), true, Utility.getCurrentAgentAccount(currentAgent))
    	agentTxnItem.header_id = txnHeader.id
    	agentTxnItem.save
    	Log.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

  	 	optTxnItem  = getTxnItem(Utility.getCashAdmin(), TxnHeader.my_money(approvedAmount), false, Utility.getCashAdminAccount(Utility.getCashAdmin()))
    	optTxnItem.header_id = txnHeader.id;
    	optTxnItem.save
    	Log.logger.info("At line #{__LINE__} and Operator(Cash) transaction item is saved in the TxnItem table successfully")

		TxnManager.update_account(Utility.getCashAdminAccount(Utility.getCashAdmin()), TxnHeader.my_money(approvedAmount), false)
  		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(approvedAmount), true)  
    	Log.logger.info("At line #{__LINE__} and balances of Agent and Operator Cash Account are updated successfully")
	end	

	def self.getHeader(approvedAmount, serviceType, description, agentID, externalRef)
		txnHeader = TxnHeader.new
		    txnHeader.service_type = serviceType
		    txnHeader.sub_type =  serviceType
		    txnHeader.txn_date = Time.now
		    txnHeader.currency_code = Constants::USER_ACCOUNT_CURRENCY
		    txnHeader.amount = TxnHeader.my_money(approvedAmount)
		    txnHeader.agent_id = agentID
		    txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
		    txnHeader.gateway =Constants::TXN_GATEWAY
		    txnHeader.description = description
		    txnHeader.external_reference = externalRef
	    return txnHeader
	end

	def self.getTxnItem(whichUser, amount, isDebit, userAccount)
	    txnItem = TxnItem.new
	   
		    txnItem.user_id = whichUser.id

		    previousBalance = Utility.getAgentAccountBalance(whichUser).to_f

		    if isDebit == false
		      txnItem.debit_amount = nil
		      txnItem.credit_amount = TxnHeader.my_money(amount)
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)
		    else
		      txnItem.debit_amount = TxnHeader.my_money(amount)
		      txnItem.credit_amount = nil
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)
		    end
		    txnItem.account_id = userAccount.id
		    txnItem.fee_id = nil
		    txnItem.fee_amt =  0
		    txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f 
	    return txnItem
	end

	def self.getPenaltyRecord(agent, amount, last_penalty_record,non_payment_status)
	    agentPenalty = AgentPenaltyDetail.new
		    agentPenalty.user_id = agent.id
		    agentPenalty.loan_amount = last_penalty_record.loan_amount  
		    agentPenalty.penalty_amount = (amount.to_f - (amount.to_f + amount.to_f))
		    agentPenalty.pending_penalty_amount = last_penalty_record.pending_penalty_amount.to_f - amount.to_f
		    agentPenalty.non_payment_status = non_payment_status
	    return agentPenalty
	end

	def self.getLoanRecord(agent, amount, last_loan_record,non_payment_status)

	    loan = Loan.new
	    loan.user_id = agent.id
	    loan.acc_type = Constants::USER_ACCOUNT_TYPE
	    loan.currency = Constants::USER_ACCOUNT_CURRENCY
	    loan.amount = (amount.to_f - (amount.to_f + amount.to_f))
	    loan.balance = last_loan_record.balance.to_f - amount.to_f
	    loan.due_date = last_loan_record.due_date
	    loan.non_payment_status = non_payment_status
	    return loan
	end

end