# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class PdfDownloadManager

	def self.downloadPDF(params)
		Log.logger.info("At line = #{__LINE__} and inside the PdfDownloadManager and downloadPDF method is called")

		commPaidDate = params[:commPaidDate]
		userID = params[:userId].tr('[]', '').split(',').map(&:to_i)
		loanBalance = params[:loanBalance].tr('[]', '').split(',').map(&:to_f)
		penaltyBalance = params[:penaltyBalance].tr('[]', '').split(',').map(&:to_f)
		commissionBalance = params[:commissionBalance].tr('[]', '').split(',').map(&:to_f)
		taxPercentage = params[:taxPercentage].tr('[]', '').split(',').map(&:to_f)
		taxAmount = params[:taxAmount].tr('[]', '').split(',').map(&:to_f)
		headerId = params[:headerId].tr('[]', '').split(',').map(&:to_i)
		monthOfComm = params[:month]
		yearOfComm = params[:year]
		totalRecords = params[:counter]
		
		Log.logger.info("At line = #{__LINE__} and parameter are list of the user is for the pdf is = #{userID}, month = #{monthOfComm} and year is = #{yearOfComm}")

		commissionArray = Array.new
		fromDate = Utility.getMonthBeginDate(monthOfComm, yearOfComm)
		toDate = Utility.getMonthEndDate(monthOfComm, yearOfComm)

		for i in 0..totalRecords.to_i - 1
			commissionHash = ActiveSupport::HashWithIndifferentAccess.new
			currentAgent = Utility.getCurrentAgent(userID[i])

			commissionHash[:commPaymentNo] = headerId[i]
			commissionHash[:fromDate] = Utility.getFormatedDateNoTime(fromDate.to_s)
			commissionHash[:toDate] = Utility.getFormatedDateNoTime(toDate.to_s)
			commissionHash[:currentAgent] = currentAgent
			commissionHash[:commPaidDate] = commPaidDate
			commissionHash[:totalComm] = commissionBalance[i]
			commissionHash[:pendingCredit] = loanBalance[i]
			commissionHash[:penalty] = penaltyBalance[i]
			commissionHash[:taxPercentage] = taxPercentage[i]
			commissionHash[:taxAmount] = taxAmount[i]
			commissionHash[:paidComm] = CommissionManager.getReportPaidCommisson(loanBalance[i], penaltyBalance[i], (commissionBalance[i].to_f - taxAmount[i].to_f))
			commissionHash[:txnHeader] = getFloatPurchaseDetails(currentAgent.id, fromDate, toDate)
			commissionHash[:txnHeaderSales] = getSalesDetails(currentAgent.id, fromDate, toDate)

        	commissionArray << commissionHash
        end	

        return commissionArray
	end	

	def self.getFloatPurchaseDetails(agentID, fromDate, toDate)
			query = "SELECT description, SUM(amount) AS amount FROM txn_headers "
			query << "WHERE service_type = '#{Constants::USER_CASH_DEPOSIT_LABLE}' AND agent_id = '#{agentID}' AND txn_date >= '#{fromDate}' AND "
			query << "txn_date <= '#{toDate}' GROUP BY description"
			txnHeader = Utility.executeSQLQuery(query)
		return txnHeader	
	end

	def self.getSalesDetails(agentID, fromDate, toDate)
       		subTypes = Utility.getSQLQueryAirtimeSubTypes + ',' + Utility.getSQLQueryServiceTypesExclude(Constants::MTOPUP_LABEL)

			query = "SELECT sub_type AS service_type, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
			query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND txn_date <= '#{toDate}' "
			query << "AND agent_id::INT = #{agentID} AND sub_type IN(#{subTypes}) AND agent_commission != 0 "
			query << "GROUP BY sub_type"

			txnHeader =	Utility.executeSQLQuery(query)
		return txnHeader	
	end		

end	