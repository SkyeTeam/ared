# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 45 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class CreateUserManager

	def self.isSuccessful(user)
    	
    		r = Response.new
		    #**********CHECK EMAIL
		    user_email_check = User.where("email = ?", user.email)
		    if user_email_check.present?
		        r.Response_Code=(ErrorHelper::EMAIL_ID_ALREADY_EXIST)
		        r.Response_Msg=("Email ID already registered") 
		        return r
		    end  

		    #**********CHECK PHONE
		    u = User.where("phone = ?", user.phone)
		    if u.present?
		        flash[:error] = "Phone already registered"
		        render new_user_path
		        return
		    end  

		    #********CHECK EMPTY EMAIL
		    if user.role == 'agent'
		         Rails.logger.debug("*******INSIDE IF ROLE")
		          if user.email == ''
		              user.email_flag = 1
		              user.email = TxnManager.getRandomEmail
		          end 

		           #**********CHECK NATIONAL ID
		          u_national_id = User.where("national_id = ?", user.national_id).where("status != 1")
		          if u_national_id.present?
		              flash[:error] = "National ID is  already exist"
		              render new_user_path
		              return
		          end 

		          #**********CHECK TIN
		          u_tin_no = User.where("tin_no = ?", user.tin_no).where("status != 1")
		          if u_tin_no.present?
		              flash[:error] = "TIN is  already exist"
		              render new_user_path
		              return
		          end 
		    end 
 	end   
end	