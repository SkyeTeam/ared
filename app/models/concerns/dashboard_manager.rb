# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:NilCheck { enabled: false }

class DashboardManager

    @message = nil # this is used to displaay the js alert

	def self.getTodayTotalCommission
        todayHourlyTotalComm= TxnHeader.group_by_hour(:txn_date,range: Date.today.beginning_of_day..Time.now,last: 24).where('txn_date >= ?', Date.today).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum('total_commission')
        totalCommArray = []
            if todayHourlyTotalComm.present? 
                i = 0 
                todayHourlyTotalComm.each do |time,commission|
    	            totalCommArray[i] = TxnHeader.my_money(commission).to_f
                    i = i.to_i + 1
                end 
            end 
        return totalCommArray
	end	

	def self.getTodayTotalAredCommission
        todayHourlyAredComm = TxnHeader.group_by_hour(:txn_date,range: Date.today.beginning_of_day..Time.now,last: 24).where('txn_date >= ?', Date.today).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum('opt_commission') 
        totalAredCommArray = []
            if todayHourlyAredComm.present? 
                i = 0 
                todayHourlyAredComm.each do |time,commission|
                    totalAredCommArray[i] = TxnHeader.my_money(commission).to_f
                    i = i.to_i + 1
                end 
          	end
        return totalAredCommArray	
	end	

	def self.getTodayTotalMFCommission
        todayHourlyAgentComm = TxnHeader.group_by_hour(:txn_date,range: Date.today.beginning_of_day..Time.now,last: 24).where('txn_date >= ?', Date.today).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum('agent_commission') 
        totalAgentCommArray = []
            if todayHourlyAgentComm.present?
                i = 0   
                todayHourlyAgentComm.each do |time,commission|
                    totalAgentCommArray[i] = TxnHeader.my_money(commission).to_f
                    i = i.to_i + 1
                end 
            end
        return totalAgentCommArray  
	end	

	def self.getTodayTotalTurnover
        todayHourlyTurnover= TxnHeader.group_by_hour(:txn_date,range: Date.today.beginning_of_day..Time.now,last: 24).where('txn_date >= ?', Date.today).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum('amount')
        totalTurnoverArray = []
            if todayHourlyTurnover.present?
                i = 0      
             	todayHourlyTurnover.each do |time,amount|
                	totalTurnoverArray[i] = TxnHeader.my_money(amount).to_f
                	i = i.to_i + 1
             	end 
            end 
        return totalTurnoverArray   
	end	

	def self.dailyTurnover
        dailyTurnoverHourlyDetails = TxnHeader.group_by_hour_of_day(:txn_date,).where("txn_date >= ?", Date.today).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum(:amount)
      	dailyTurnoverArray = []
        	if dailyTurnoverHourlyDetails.present?
                i = 0
                currentTime = Time.now
                hour = currentTime.strftime('%H')
                dailyTurnoverHourlyDetails.each do |hourValue,txnAmount|
                    if txnAmount == nil
                        i = i+1
                    elsif hourValue.to_i <= hour.to_i
                        dailyTurnoverArray[i] = [ i , txnAmount.to_f]
                        i=i+1
                    end
                end 
            end 
        return dailyTurnoverArray
	end	

	def self.serviceSoldDetails
        response = Response.new
        serviceSoldValueArray, serviceSoldNameArray = Array.new, Array.new
        
            query = "SELECT SUM(amount), service_type FROM txn_headers "
            query << "WHERE txn_date >= '#{Date.today.beginning_of_day}' AND is_rollback != #{Constants::TXN_INITIATE_ROLLBACK_STATUS} AND service_type IN (#{Utility.getSQLQueryServiceTypes}) "
            query << "GROUP BY service_type ORDER BY service_type ASC"
            txnHeader = Utility.executeSQLQuery(query)  

            if txnHeader.present?
                txnHeader.each do |header|
                    serviceSoldNameArray.push(Utility.getServiceTypeDisplayValue(header['service_type']))
                    serviceSoldValueArray.push(TxnHeader.my_money(header['sum']).to_f)
                end  
            end 
            response.serviceSoldNameArray=(serviceSoldNameArray)
            response.serviceSoldValueArray=(serviceSoldValueArray)

        return response 

	end	

    def self.getSMSStatus(status)
        if status == 1
           return 'Yes'
        else
           return 'No'
        end  
    end 

    def self.getTopAgentsCollection
            query = "SELECT SUM(amount), agent_id FROM txn_headers "
            query << "WHERE txn_date >= '#{Date.today.beginning_of_day}' AND is_rollback != #{Constants::TXN_INITIATE_ROLLBACK_STATUS} AND service_type IN (#{Utility.getSQLQueryServiceTypes}) "
            query << "GROUP BY agent_id ORDER BY SUM(amount) DESC LIMIT #{Constants::TOTAL_TOP_USER_SHOW}"
            txnHeader = Utility.executeSQLQuery(query)  
        return txnHeader    
    end 

    def self.getTopAgentsCollectionsTotal(txnHeader) 
        totalAmount = 0
            txnHeader.each do |header|
                totalAmount = totalAmount.to_f + header['sum'].to_f
            end    
        return totalAmount    
    end

    def self.getTopAgentBarPerValue(amount, totalAmount)  
            percentageValue =  TxnHeader.my_money((amount.to_f * 100)/totalAmount).to_f
        return percentageValue
    end    

	def self.getTodayTotalCommissionAmount
            totalCommissionAmount = TxnHeader.where("txn_date >= ? ", Time.zone.now.beginning_of_day).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum(:total_commission)	
	    return totalCommissionAmount	
	end		
	
	def self.getTodayTotalAredCommissionAmount
            aredCommissionAmount = TxnHeader.where("txn_date >= ? ", Time.zone.now.beginning_of_day).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum(:opt_commission)
        return aredCommissionAmount	
	end	

	def self.getTodayMFCommissionAmount
            todayCommissionAmount = TxnHeader.where("txn_date >= ? ", Time.zone.now.beginning_of_day).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum(:agent_commission)
        return todayCommissionAmount
	end

	def self.getTodayTotalTransactionAmount
		    todayTransactionAmount = TxnHeader.where("txn_date >= ?", Time.zone.now.beginning_of_day).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum(:amount)
        return todayTransactionAmount
	end
		
	def self.getTodayTotalTransactions
		    todayTotalTransactions = TxnHeader.where("txn_date >= ?", Time.zone.now.beginning_of_day).where(service_type: Utility.getServiceTypes).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).count(:id)
	    return todayTotalTransactions	
	end

	def self.getTotalMicroFranchisee
		    totalMicroFranchisee = User.where('role = ? ', Constants::MF_USER_ROLE).where('status = ?', Constants::USER_ACTIVE_STATUS).count
	    return totalMicroFranchisee
	end	

    def self.checkSuperAdminLogin(userAllRoles)
        isSuperAdmin = false
            if userAllRoles.present? 
                roleAssigned = RoleHistory.find(userAllRoles.ids.last)
                if roleAssigned.present?
                    if roleAssigned.role_id == 0
                      isSuperAdmin = true
                    end 
                end 
            end    
        return isSuperAdmin
    end

    def self.isSuperAdminRoleAssign(adminID)
        role = RoleHistory.find_by_user_id(adminID)
        if role.present?
          roleId = role.role_id 
        end

        if roleId == 0
            return true
        else
            return false    
        end    
    end    

    def self.setDisplayMessage(msg)
        @message = msg # this make the global bcoz use inside the getDisplayMessage function
    end 

    def self.getDisplayMessage()
        mes = nil
            if @message != nil
                mes = @message
                @message = nil
            end    
        return mes
    end   

end	