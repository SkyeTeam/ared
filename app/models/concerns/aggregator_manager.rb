# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 37 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class AggregatorManager
	
	def self.createAggregator(loggedInAdmin, params)
		Log.logger.info("Inside the Aggregator manager class and method createAggregator is call at line = #{__LINE__}")
		
		isAggregatorUpdate = params[:isUpdate]
		aggregatorName = params[:aggregatorName]
		contactPersonFirstName = params[:contPerFName]
		contactPersonLastName = params[:contPerLName]
		contactPersonPhone = params[:contPerPhone]
		isCommInclude = params[:isCommInclude]

		if isAggregatorUpdate.eql? Constants::BOOLEAN_TRUE_VALUE_LABEL
			aggregatorID = params[:aggregatorID]
			response = validate(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, false, aggregatorID)
			if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            	return response
	        else
		       		updateAggregator(aggregatorID, aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isCommInclude)
		        	
		        	ActivityLog.saveActivity((I18n.t :AGGREGATOR_UPDATE_ACTIVITY_MSG, :name => aggregatorName), loggedInAdmin.id, aggregatorID, params)
		        	
		        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		        	response.Response_Msg=(I18n.t :AGGREATOR_SUCCESS_UPDATE_MSG) 

		        return response
	        end
		else

			response = validate(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, true, nil)
	        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	            return response
	        else
		       		aggregatorID = saveAggregator(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isCommInclude)

		       		ActivityLog.saveActivity((I18n.t :AGGREGATOR_CREATE_ACTIVITY_MSG), loggedInAdmin.id, aggregatorID, params)
		        	
		        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		        	response.Response_Msg=(I18n.t :AGGREATOR_SUCCESS_CREATE_MSG) 
		        return response
	        end
	    end    
	end

	def self.saveAggregator(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isCommInclude)
		userID = Constants::BLANK_VALUE
			ActiveRecord::Base.transaction do
				user = getAggregatorUser(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isCommInclude)
				user.save
				userID = user.id
				Log.logger.info("At line #{__LINE__} and aggregator successfully added into the user table")

				userAccount = getAggregatorUserAccount(userID)
				userAccount.save
				Log.logger.info("At line #{__LINE__} and aggregator normal account successfully added into the user account table")

				userCommPaidAccount = getAggregatorCommPaidAccount(userID)
				userCommPaidAccount.save
				Log.logger.info("At line #{__LINE__} and aggregator commission paid account successfully added into the user account table")


				userCommRecvAccount = getAggregatorCommRecvAccount(userID)
				userCommRecvAccount.save
				Log.logger.info("At line #{__LINE__} and aggregator commission receive account successfully added into the user account table")
			end	
		return userID
	end	


	def self.updateAggregator(aggregatorID, aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isCommInclude)
		
		aggregator = User.find(aggregatorID)
		aggregator.update_attributes(:business_name => aggregatorName, :name => contactPersonFirstName, :last_name => contactPersonLastName, :phone => contactPersonPhone, :is_comm_include => getCommIncludeValue(isCommInclude))
		Log.logger.info("At line #{__LINE__} and aggregator successfully updated into the user table")

	end	


	def self.getAggregatorUser(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isCommInclude)
		user = User.new
			user.email = TxnManager.getRandomEmail
			user.password = Utility.getAlphaNumRandomNumber(8)
			user.role = Constants::AGGREGATOR_USER_ROLE
			user.phone = contactPersonPhone
			user.business_name = aggregatorName.upcase
			user.name = contactPersonFirstName
			user.last_name = contactPersonLastName
			user.status = Constants::USER_ACTIVE_STATUS
			user.email_flag = Constants::USER_EMPTY_EMAIL_FLAG
			user.is_comm_include = getCommIncludeValue(isCommInclude)
		return user	
	end

	def self.getCommIncludeValue(isCommInclude)
		if isCommInclude.eql? Constants::YES_LABEL
			return Constants::AGG_COMM_INCLUDE_VALUE
		else 	
			return Constants::AGG_COMM_EXCLUDE_VALUE
		end	
	end	

	def self.getCommIncludeDisValue(isCommInclude)
		if isCommInclude == Constants::AGG_COMM_INCLUDE_VALUE
			return Constants::YES_LABEL
		else 	
			return Constants::NO_LABEL
		end	
	end	

	def self.getAggregatorUserAccount(aggregatorID)
		userAccount = UserAccount.new
	        userAccount.user_id  = aggregatorID
	        userAccount.acc_type = Constants::USER_ACCOUNT_TYPE
	        userAccount.currency = Utility.getCurrencyLabel
	        userAccount.balance  = Constants::USER_ACCOUNT_INITIAL_BALANCE
      	return userAccount    
	end	

	def self.getAggregatorCommPaidAccount(aggregatorID)
		userAccount = UserAccount.new
	        userAccount.user_id  = aggregatorID
	        userAccount.acc_type = Constants::AGGREGATOR_COMM_PAID_ACCOUNT_TYPE
	        userAccount.currency = Utility.getCurrencyLabel
	        userAccount.balance  = Constants::USER_ACCOUNT_INITIAL_BALANCE
      	return userAccount    
	end	

	def self.getAggregatorCommRecvAccount(aggregatorID)
		userAccount = UserAccount.new
	        userAccount.user_id  = aggregatorID
	        userAccount.acc_type = Constants::AGGREGATOR_COMM_RECV_ACCOUNT_TYPE
	        userAccount.currency = Utility.getCurrencyLabel
	        userAccount.balance  = Constants::USER_ACCOUNT_INITIAL_BALANCE
      	return userAccount    
	end	


	def self.validate(aggregatorName, contactPersonFirstName, contactPersonLastName, contactPersonPhone, isNew, aggregatorID)
		response = Response.new
        
	        if !aggregatorName.present? || !contactPersonFirstName.present? || !contactPersonLastName.present? || !contactPersonPhone.present?
                	Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		if isNew
		 		if isAggregatorExist(aggregatorName, true, nil)
						Log.logger.info("At line = #{__LINE__} and aggregator is already exist")
		                response.Response_Code=(Error::AGGREGATOR_ALREADY_EXIST)
		                response.Response_Msg=(I18n.t :AGGREGATOR_ALREADY_EXIST_MSG) 
		            return response    
		 		end	

		 		if isPhoneExist(contactPersonPhone, true, nil)
						Log.logger.info("At line = #{__LINE__} and aggregator contact person phone is already exist")
		                response.Response_Code=(Error::PHONE_ALREADY_EXIST)
		                response.Response_Msg=(I18n.t :PHONE_ALREADY_EXIST_MSG) 
		            return response    
		 		end
		 	else
		 		if isAggregatorExist(aggregatorName, false, aggregatorID)
						Log.logger.info("At line = #{__LINE__} and aggregator is already exist")
		                response.Response_Code=(Error::AGGREGATOR_ALREADY_EXIST)
		                response.Response_Msg=(I18n.t :AGGREGATOR_ALREADY_EXIST_MSG) 
		            return response    
		 		end	

		 		if isPhoneExist(contactPersonPhone, false, aggregatorID)
						Log.logger.info("At line = #{__LINE__} and aggregator contact person phone is already exist")
		                response.Response_Code=(Error::PHONE_ALREADY_EXIST)
		                response.Response_Msg=(I18n.t :PHONE_ALREADY_EXIST_MSG) 
		            return response    
		 		end
		 	end		

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end

	def self.validateDeposit(aggregatorName, amount, description, extRef)
		response = Response.new
	        if !aggregatorName.present? || !amount.present?
                	Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		#Now check that the amount is cannot be less then zero 
            if amount.to_f <= 0
                    Log.logger.info("At line = #{__LINE__} and amount is may be zero or less then zero")
                    response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
                    response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
                return response
            end

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end
	
	def self.isAggregatorExist(aggregatorName, isNew, aggregatorID)
		aggregator = User.where("role = #{Constants::AGGREGATOR_USER_ROLE}").where("LOWER(business_name) = '#{aggregatorName}' OR UPPER(business_name) = '#{aggregatorName}'").first
		Log.logger.info("At line = #{__LINE__} and check that this aggregator is already exist or not = #{aggregator.present?}")
		if aggregator.present?
			if isNew
				return Constants::BOOLEAN_TRUE_LABEL
			else
				if aggregator.id.to_i != aggregatorID.to_i
					return Constants::BOOLEAN_TRUE_LABEL
				else
					return Constants::BOOLEAN_FALSE_LABEL
				end	
			end	
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end		
	end	

	def self.getCurrentAggregator(aggregatorName)
			aggregator = User.where("role = #{Constants::AGGREGATOR_USER_ROLE}").where("business_name = '#{aggregatorName}' OR LOWER(business_name) = '#{aggregatorName}' OR UPPER(business_name) = '#{aggregatorName}'").first
		return aggregator	
	end	

	def self.isPhoneExist(contactPersonPhone, isNew, aggregatorID)
		aggregator = User.where("role = #{Constants::AGGREGATOR_USER_ROLE} AND phone = '#{contactPersonPhone}'").first
		Log.logger.info("At line = #{__LINE__} and check that this aggregator contact person phone is already exist or not = #{aggregator.present?}")
		if aggregator.present?
			if isNew
				return Constants::BOOLEAN_TRUE_LABEL
			else
				if aggregator.id.to_i != aggregatorID.to_i
					return Constants::BOOLEAN_TRUE_LABEL
				else
					return Constants::BOOLEAN_FALSE_LABEL	
				end	
			end	
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end	
	end	


	def self.getAllAggregators(orderBY, orderType)
			aggregators = User.where("role = #{Constants::AGGREGATOR_USER_ROLE}").order("#{orderBY} #{orderType}")
		return 	aggregators
	end	


	def self.getAllAggregatorName
			aggregatorNameList = {""+Constants::SELECT_AGGREGATOR_LABEL+"" => ''}

			allAggregators = getAllAggregators("business_name", "ASC")
		
			if allAggregators.present?
				allAggregators.each do |aggregator|
					aggregatorNameList[""+aggregator.business_name+""] = aggregator.business_name
				end	
			end	

		return aggregatorNameList
	end


	def self.aggregatorDeposit(params, loggedInAdmin, request)
		Log.logger.info("Inside the Aggregator Manager  class and aggregatorDeposit method is call at line #{__LINE__} and parameters are as below")

		aggregatorName = params[:aggregatorName]
		amount = params[:amount]
		description = params[:description]
		extRef = params[:extRef]
		txnUniqueID = Utility.getRandomNumber(15)
		response = Response.new

		Log.logger.info("At line =#{__LINE__} and Aggregator name is = #{aggregatorName}, amount = #{amount}, description = #{description} and extRef = #{extRef}")

		response = validateDeposit(aggregatorName, amount, description, extRef)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end

    	aggregator = User.where("role = #{Constants::AGGREGATOR_USER_ROLE} AND business_name = '#{aggregatorName}'").first
    	aggregatorID = aggregator.id
    	purchaseAdmin = Utility.getPurchaseAdmin()
    	Log.logger.info("At line #{__LINE__} and aggregator ID is = #{aggregatorID}")

   	    #First we add one entry in the pending request table before processing
        AppUtility.savePendingRequest(txnUniqueID, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, nil, amount, aggregatorID, Utility.getUserRemoteIP(request), loggedInAdmin.id, Constants::ADMIN_USER_ROLE)
        Log.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")

        ActiveRecord::Base.transaction do
        
        	txnHeader =	Utility.getHeader(TxnHeader.my_money(amount).to_f, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, aggregatorID, Constants::SUCCESS_RESPONSE_CODE, description, extRef)
        	txnHeader.save
        	Log.logger.info("At line = #{__LINE__} and Aggregator Deposit entry is saved in the TxnHeader table successfully")

        	aggregatorTxnItem = Utility.getTxnItemNew(aggregator, TxnHeader.my_money(amount).to_f, false, Utility.getCurrentAgentAccount(aggregator))
        	aggregatorTxnItem.header_id = txnHeader.id
        	aggregatorTxnItem.save
        	Log.logger.info("At line = #{__LINE__} and Aggregator entry is saved in the Txn Item table successfully")

        	purchaseOptTxnItem = Utility.getTxnItemNew(purchaseAdmin, TxnHeader.my_money(amount).to_f, true, Utility.getPurchaseAdminAccount(purchaseAdmin))
			purchaseOptTxnItem.header_id = txnHeader.id
        	purchaseOptTxnItem.save
        	Log.logger.info("At line = #{__LINE__} and Operator(Purchase) entry is saved in the Txn Item table successfully")

        	Utility.updateUserAccountBalance(aggregatorID, TxnHeader.my_money(amount).to_f, false)
        	Utility.updateUserAccountBalance(purchaseAdmin.id, TxnHeader.my_money(amount).to_f, true)
			Log.logger.info("At line = #{__LINE__} and account balances of both Aggregator and  Operator(Purchase) are update successfully")        	

	   	    AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
        	Log.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")

        end	

        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=(I18n.t :AGGREGATOR_DEPOSIT_SUCESS_MSG, :name => aggregatorName, :amount => Utility.getFormatedAmount(amount))
      	return response  

	end

	def self.aggregatorDepositNew(aggregatorName, amount, description, extRef, request)
		AppLog.logger.info("Inside the Aggregator Manager  class and aggregatorDeposit method is call at line #{__LINE__} and parameters are as below")

		txnUniqueID = Utility.getRandomNumber(15)
		response = Response.new

		AppLog.logger.info("At line =#{__LINE__} and Aggregator name is = #{aggregatorName}, amount = #{amount}, description = #{description} and extRef = #{extRef}")

		response = validateDeposit(aggregatorName, amount, description, extRef)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end

    	aggregator = User.where("role = #{Constants::AGGREGATOR_USER_ROLE} AND business_name = '#{aggregatorName}'").first
    	
    	aggregatorID = aggregator.id
    	aggregatorAccount = Utility.getUserAccountWithUserType(aggregatorID, Constants::USER_ACCOUNT_TYPE)
    	
    	purchaseAdmin = Utility.getPurchaseAdmin()
    	purchaseAdminAccount = Utility.getPurchaseAdminAccount(purchaseAdmin)
    	AppLog.logger.info("At line #{__LINE__} and aggregator ID is = #{aggregatorID}")

   	    #First we add one entry in the pending request table before processing
        AppUtility.savePendingRequest(txnUniqueID, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, nil, amount, aggregatorID, Utility.getUserRemoteIP(request), nil, nil)
        AppLog.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")

        ActiveRecord::Base.transaction do
        
        	txnHeader =	Utility.getHeader(TxnHeader.my_money(amount).to_f, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, Constants::AGGREGATOR_DEPOSIT_DB_LABEL, aggregatorID, Constants::SUCCESS_RESPONSE_CODE, description, extRef)
        	txnHeader.save
        	AppLog.logger.info("At line = #{__LINE__} and Aggregator Deposit entry is saved in the TxnHeader table successfully")
			
			AppUtility.getTxnItemNew(txnHeader.id, purchaseAdmin.id, purchaseAdminAccount.id, Constants::PURCHASE_ADMIIN_ACCOUNT_TYPE, TxnHeader.my_money(amount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
        	AppLog.logger.info("At line = #{__LINE__} and Operator(Purchase) entry is saved in the Txn Item table successfully")

        	AppUtility.getTxnItemNew(txnHeader.id, aggregatorID, aggregatorAccount.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(amount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
        	AppLog.logger.info("At line = #{__LINE__} and Aggregator entry is saved in the Txn Item table successfully")
        	
        	Utility.updateUserAccountBalanceNew(aggregatorID, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(amount).to_f, false)
        	Utility.updateUserAccountBalanceNew(purchaseAdmin.id, Constants::PURCHASE_ADMIIN_ACCOUNT_TYPE, TxnHeader.my_money(amount).to_f, true)
			AppLog.logger.info("At line = #{__LINE__} and account balances of both Aggregator(ACC) and Operator(Purchase) are update successfully")        	

	   	    AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
        	AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")

        end	

        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=(I18n.t :AGGREGATOR_DEPOSIT_SUCESS_MSG, :name => aggregatorName, :amount => Utility.getFormatedAmount(amount))
      	return response  

	end


	def self.fetchDetails(params)
		
		Log.logger.info("Inside the Aggregator Manager class and method fetchDetails is called at line = #{__LINE__}")

		aggregatorName = params[:aggregatorName]
		reportType = params[:reportType]
		fromDate = params[:from_date]
		toDate = params[:to_date]

		Log.logger.info("At line #{__LINE__} aggregatorName = #{aggregatorName}, reportType = #{reportType}, fromDate = #{fromDate} and toDate = #{toDate}")
		
		response = validateReport(aggregatorName, reportType, fromDate, toDate)

		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
		
		fromDate = Date.parse(fromDate.to_s).beginning_of_day
	    toDate = Date.parse(toDate.to_s).end_of_day

	    txnHeader = getDetails(aggregatorName, reportType, fromDate, toDate)

		if txnHeader.present?
	          response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	          response.txn_history=(txnHeader)
          	return response
        else
          		response.Response_Code=(Error::NO_TRANSCATIONS)
	   	  		response.Response_Msg=(I18n.t :NO_TRANSCATIONS_MSG) 
	   	  	return response
        end

	end	

	
	def self.getDetails(aggregatorName, reportType, fromDate, toDate)

		serviceType = Array.new
		aggregatorID = Constants::BLANK_VALUE
		subType = Constants::BLANK_VALUE
		aggregator = getCurrentAggregator(aggregatorName)
		
		if aggregator.present?
			aggregatorID = aggregator.id
		end	
		
		if reportType.eql? Constants::AGGREGATOR_DEPOSIT_REPORT_LABEL
		
			serviceType.push(Constants::AGGREGATOR_DEPOSIT_DB_LABEL)
			subType = Constants::AGGREGATOR_DEPOSIT_DB_LABEL

		elsif reportType.eql? Constants::AGGREGATOR_SALES_REPORT_LABEL
			
			if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]#For Rwanda
				if aggregatorName.eql? Constants::FDI_AGGREGATOR
					
					serviceType.push(Constants::MTOP_UP_SALES_LABEL)
					serviceType.push(Constants::ELECTRICITY_SALE_LABEL)
					serviceType.push(Constants::DTH_SALE_LABEL)
					subType = Constants::FDI_AGGREGATOR
				
				elsif aggregatorName.eql? Constants::MTN_AGGREGATOR
					
					serviceType.push(Constants::MTOP_UP_SALES_LABEL)
					subType = Constants::MTN_AGGREGATOR

				elsif aggregatorName.eql? Constants::AIRTEL_AGGREGATOR

					serviceType.push(Constants::MTOP_UP_SALES_LABEL)
					subType = Constants::AIRTEL_AGGREGATOR

				elsif aggregatorName.eql? Constants::INTERNET_AGGREGATOR

					serviceType.push(Constants::INTERNET_SALE_LABEL)
					subType = Constants::INTERNET_AGGREGATOR	

				elsif aggregatorName.eql? Constants::TAX_PAYMENT_AGGREGATOR

					serviceType.push(Constants::IREMBO_TAX_PAYMENT_SALE_LABEL)
					subType = Constants::TAX_PAYMENT_AGGREGATOR	
				
				end	
			elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]#For Uganda
				if aggregatorName.eql? UgandaConstants::PAYWAY_AGGREGATOR

					serviceType.push(Constants::MTOP_UP_SALES_LABEL)
					subType = UgandaConstants::PAYWAY_AGGREGATOR
				
				elsif aggregatorName.eql? UgandaConstants::QUICK_TELLER_AGGREGATOR
					
					serviceType.push(Constants::ELECTRICITY_SALE_LABEL)
					serviceType.push(UgandaConstants::NATIONAL_WATER_BILL_SALES_LABEL)
					subType = UgandaConstants::QUICK_TELLER_AGGREGATOR
					
				end	
			end							

		end	

		txnHeader = getSQLQueryRessult(aggregatorID, serviceType, subType, fromDate, toDate)

		return txnHeader
	
	end	


	def self.getSQLQueryRessult(aggregatorID, serviceType, subType, fromDate, toDate)

		serviceTypeQuery = ""
		for i in 0..serviceType.size.to_i - 1 
			
			if i < serviceType.size.to_i - 1
				commaValue = ","
			else 
				commaValue = ""
			end	
			serviceTypeQuery = serviceTypeQuery.to_s + "'#{serviceType[i]}'" + commaValue
		end 
		
		if subType.eql? Constants::AGGREGATOR_DEPOSIT_DB_LABEL
			subPartQuery1 = Constants::BLANK_VALUE
			subPartQuery2 = Constants::BLANK_VALUE
			subPartQuery3 = Constants::BLANK_VALUE
		else
			subPartQuery1 = ", h.param1, u.phone AS mfphone"
			subPartQuery2 = ", users u"
			subPartQuery3 = "AND u.id = h.param1::INT"
		end	
		
		query = "SELECT h.id, i.user_id, h.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, h.service_type, h.sub_type, h.account_id, h.amount, "
		query << "i.previous_balance, i.post_balance, h.description, h.external_reference#{subPartQuery1} "
		query << "FROM txn_items i,  txn_headers h#{subPartQuery2}  WHERE h.id=i.header_id AND i.user_id=#{aggregatorID} "
		query << "AND h.service_type IN (#{serviceTypeQuery}) AND h.sub_type = '#{subType}' #{subPartQuery3} "
		query << "AND h.txn_date >= '#{fromDate}' AND h.txn_date <= '#{toDate}' "
		query << "ORDER BY h.txn_date DESC"

		txnHeader = Utility.executeSQLQuery(query)

		return txnHeader

	end	


	def self.validateReport(aggregatorName, reportType, fromDate, toDate)
		response = Response.new
	        if !aggregatorName.present? || !reportType.present? || !fromDate.present? || !toDate.present?
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

			fromDate = Date.parse(fromDate.to_s).beginning_of_day
		    toDate = Date.parse(toDate.to_s).end_of_day

            if fromDate > toDate
	       			response.Response_Code=(Error::CHECK_DATES_AGAIN)
		    		response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
		    	return response
			end

			if !Utility.isValidYear(fromDate.to_s)
        			Log.logger.info("At line = #{__LINE__} and from date has not a valid year ")
       				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

			if !Utility.isValidYear(toDate.to_s)
        			Log.logger.info("At line = #{__LINE__} and to date has not a valid year ")
       				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end

end	