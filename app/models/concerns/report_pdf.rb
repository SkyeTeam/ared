# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 64 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class ReportPdf < Prawn::Document

  def print(userID,commissionBalance,taxPercentage,headerId,totalRecords,monthOfComm,yearOfComm)
    super()
    Log.logger.info("ReportPdf's print method is callled at line = #{__LINE__} and total Records = #{totalRecords}")
    
    userIDArray = userID.tr('[]', '').split(',').map(&:to_i)
    commissionBalanceArray = commissionBalance.tr('[]', '').split(',').map(&:to_f)
    taxPercentageArray = taxPercentage.tr('[]', '').split(',').map(&:to_f)
    headerIdArray = headerId.tr('[]', '').split(',').map(&:to_i)
    loopSize = totalRecords.to_i - 1

    for i in 0..loopSize
      start_new_page(:layout => :portrait ,:margin => 30)
      header(headerIdArray[i])
      text_content(userIDArray[i],commissionBalanceArray[i],taxPercentageArray[i],monthOfComm,yearOfComm)
   
      
      # This will print the page number at the bottom
      # number_pages "<page> in a total of <total>", 
      #                                    {:start_count_at => 2,
      #                                     :page_filter => lambda{ |pg| pg != 1 },
      #                                     :at => [bounds.right - 50, 0],
      #                                     :align => :right,
      #                                     :size => 10}
     
      # last_page = state.page
      # last_page_size = last_page.layout
    end
  end

  def header(invoiceNo)

      Log.logger.info("ReportPdf's header method is callled at line = #{__LINE__} and headerId = #{invoiceNo}")

      #This will draw the border around the page
      stroke do
       line(bounds.bottom_left, bounds.bottom_right)
       line(bounds.bottom_right, bounds.top_right)
       line(bounds.top_right, bounds.top_left)
       line(bounds.top_left, bounds.bottom_left)
      end

      bounding_box([10, 690], :width => 350, :height => 70) do

        #This inserts an image in the pdf file and sets the size of the image
        image "#{Rails.root}/app/assets/images/00-1.png", width: 128, height: 44

      end

      bounding_box([345, 710], :width => 200, :height => 60) do
        text "Commission Payment No:", size: 12,:color => "231f20"
      end

      bounding_box([482, 710], :width => 270, :height => 60) do
        text invoiceNo.to_s, size: 12,:color => "231f20"
      end


  end

  def text_content(userID,commission,taxper,monthOfComm,yearOfComm)
    # The cursor for inserting content starts on the top left of the page. Here we move it down a little to create more space between the text and the image inserted above

    # The bounding_box takes the x and y coordinates for positioning its content and some options to style it
    bounding_box([10, 620], :width => 350, :height => 90) do
      text "African Renewable Energy Distributor Ltd.", size: 15, style: :bold,:color => "231f20"
      text "Tax ID : 102880508", size: 12,:color => "231f20"
      text "KN 4 AV 76", size: 12,:color => "231f20"
      text "kigali", size: 12,:color => "231f20"
      text "Rwanda", size: 12,:color => "231f20"
      
    end

    user = User.find(userID)  
    
    bounding_box([10, 510], :width => 270, :height => 120) do
      text "Bill To:",:color => "231f20", style: :bold
      text user.name + " " + user.last_name,:color => "231f20"
      text user.address + "," + user.city,:color => "231f20"
      text user.district,:color => "231f20"
      text user.country,:color => "231f20"
      text "TIN:" + user.tin_no.to_s,:color => "231f20"
    end

    bounding_box([430, 510], :width => 270, :height => 80) do
      text " Date :",:color => "231f20", style: :bold
    end

    bounding_box([475, 510], :width => 270, :height => 80) do
      text Time.now.strftime("%d-%m-%Y").to_s,:color => "231f20" #, size: 15, style: :bold
      text " "
    end

    # Start of Des header Row
      bounding_box([10, 410], :width => 20, :height => 20) do  # This is the first uper rectangle of Item and Des
        stroke do
            fill_color 'FFFFFF'
                                            #  x  y   wd   ht  radius
            fill_and_stroke_rounded_rectangle [0,30], 530, 240, 0
        end
      end

      #  x1 y1 x2 y2  radius
      line(430,420,430,179)

     
      bounding_box([13, 410], :width => 100, :height => 20) do
        text "#" ,:color => "231f20"
      end

      bounding_box([30, 410], :width => 100, :height => 20) do
        text "Item & Description", :color => "231f20"
      end

      bounding_box([465, 410], :width => 100, :height => 20) do
        text "Amount", :color => "231f20"
      end
    # End of Des header Row

     # Draw the horizontal line
      bounding_box([10, 390], :width => 530, :height => 20) do
        stroke_horizontal_rule
      end

      # ************************************ Data Row 1
      bounding_box([13, 380], :width => 20, :height => 20) do
        text "1",:color => "231f20"#, size: 15, style: :bold
      end

      bounding_box([30, 380], :width => 300, :height => 20) do
        text "Micro franchisee commission (" +monthOfComm.to_s+ " " +yearOfComm + ")" ,:color => "231f20"#, size: 15, style: :bold
      end

      bounding_box([445, 380], :width => 60, :height => 20) do
        text TxnHeader.my_money(commission),:color => "231f20" ,:align => :right
      end

      bounding_box([10, 260], :width => 530, :height => 20) do
        stroke_horizontal_rule
      end

      #  ************************************Data row 3
      bounding_box([310, 250], :width => 200, :height => 20) do
        text "Sub Total",:color => "231f20", size: 10
      end

      bounding_box([445, 250], :width => 60, :height => 20) do
        text TxnHeader.my_money(commission),:color => "231f20",:align => :right
      end

      #  ************************************ Data Row 4
      bounding_box([360, 250], :width => 200, :height => 20) do
        text "(Tax Inclusive)",:color => "231f20", size: 10
      end

      #  ************************************Data row 5
      bounding_box([360, 230], :width => 200, :height => 20) do
        text "TAX (" + TxnHeader.my_money(taxper).to_s + "%)",:color => "231f20", size: 10
      end

      taxAmount = (TxnHeader.my_money(commission).to_f * TxnHeader.my_money(taxper).to_f)/100

      bounding_box([445, 230], :width => 60, :height => 20) do
        text TxnHeader.my_money(taxAmount).to_s,:color => "231f20",:align => :right
      end
     
      bounding_box([10, 210], :width => 530, :height => 20) do
        stroke_horizontal_rule
      end

       #  ************************************Data row 6
      bounding_box([360, 200], :width => 200, :height => 20) do
        text "Total (RWF)",:color => "231f20"
      end

      afterTax = TxnHeader.my_money(commission).to_f - TxnHeader.my_money(taxAmount).to_f

      bounding_box([445, 200], :width => 60, :height => 20) do
        text TxnHeader.my_money(afterTax).to_s,:color => "231f20",:align => :right
      end
      
      # Draw the horizontal line
      # bounding_box([10, 150], :width => 530, :height => 20) do
      #   stroke_horizontal_rule
      # end

      #  ************************************Data row 9
      bounding_box([10, 140], :width => 100, :height => 20) do
        text "Notes:",:color => "231f20"#, size: 15, style: :bold
      end

       #  ************************************Data row 10
      bounding_box([10, 120], :width => 300, :height => 20) do
        text "Thanks for business.", size: 10,:color => "231f20"
      end
      
  end

end