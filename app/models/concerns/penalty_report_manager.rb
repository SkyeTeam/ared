# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class PenaltyReportManager

	def self.serachPenaltyBWdates(params)
		
		Log.logger.info("Inside the Penalty Report Manager class and serachPenaltyBWdates method is called")
	
	    fromDate = params[:from_date]
        toDate = params[:to_date]
        response = Response.new

        unless fromDate.present? || toDate.present?
                Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end   

        Log.logger.info("Parameters are from date is = #{fromDate} and to date is = #{toDate}")

		if fromDate.to_s > toDate.to_s
				response.Response_Code=(Error::CHECK_DATES_AGAIN)
			    response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG)
            return response   
		end	

		firstDate = Date.parse(fromDate.to_s)
        beginFromDate = firstDate.beginning_of_day

        secondDate = Date.parse(toDate.to_s)
        endToDate = secondDate.end_of_day

        Log.logger.info("At line = #{__LINE__} and begin of from date is = #{beginFromDate} and end of to date is = #{endToDate}")     
         
        agent_penalty =  AgentPenaltyDetail.find_by_sql('SELECT agent_penalty_details.user_id,users.name,users.last_name,users.phone,SUM(agent_penalty_details.penalty_amount)   
                                                         FROM users,agent_penalty_details  
                                                         WHERE agent_penalty_details.user_id =users.id AND agent_penalty_details.penalty_amount < 0
                                                         AND  agent_penalty_details.non_payment_status=0
                                                         AND agent_penalty_details.created_at >= \''+beginFromDate.to_s+'\' 
                                                         AND agent_penalty_details.created_at <= \''+endToDate.to_s+'\'
                                                         GROUP BY agent_penalty_details.user_id,users.name,users.last_name,users.phone')

        Log.logger.info("At line = #{__LINE__} and check that whether that the penalty details in the database is exist or not = #{agent_penalty.present?}")
         
        penalty_list = Array.new
          
        if agent_penalty.present?

            agent_penalty.each do |penalty|

                allUsersHash = ActiveSupport::HashWithIndifferentAccess.new

                allUsersHash[:user_id]=penalty.user_id
                allUsersHash[:name]=penalty.name
                allUsersHash[:last_name]=penalty.last_name
                allUsersHash[:phone]=penalty.phone
                allUsersHash[:penalty]=penalty.sum
                
                penalty_list << allUsersHash 

            end  
        end

        Log.logger.info("At line = #{__LINE__} and check that the data in the array is pesent or not = #{penalty_list.present?} ")

        if penalty_list.present?
	            response.penalty_list=(penalty_list)  
	            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            return response   
        else
	          	response.Response_Code=(Error::NO_TRANSCATIONS)
			    response.Response_Msg=(I18n.t :NO_TRANSCATIONS_MSG)
		    return response 
        end
	end	
end	