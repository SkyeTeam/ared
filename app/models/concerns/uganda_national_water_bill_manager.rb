# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class UgandaNationalWaterBillManager

	def self.getAreaList
		AppLog.logger.info("At line #{__LINE__} inside the UgandaNationalWaterBillManager class and getAreaList method is called")
		response = Response.new

      areaListArray = Array.new
      dbAreas = QuickTeller.all.order("id ASC")

      dbAreas.each do |area|
        areaListHash = Hash.new
        areaListHash[:areaName] = area.payment_item_name
        areaListHash[:paymentCode] = area.payment_code
        areaListArray << areaListHash
      end  

    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
      response.Response_Msg=(areaListArray)

    return response    
	end	

	def self.validateCustomerForWaterBill(currentAgent, params, request)
		AppLog.logger.info("At line #{__LINE__} inside the UgandaNationalWaterBillManager class and validateCustomerForWaterBill method is called")

    clientAreaID = params[""+AppConstants::CLIENT_AREA_ID_PARAM_LABEL+""]
    clientAreaPaymentCode = params[""+AppConstants::CLIENT_AREA_PAYMENT_CODE_PARAM_LABEL+""]
		clientMobileNo = params[""+AppConstants::CLIENT_MOBILE_NO_PARAM_LABEL+""]
		clientWaterBillNo = params[""+AppConstants::CLIENT_WATER_BILL_NO_PARAM_LABEL+""]
    AppLog.logger.info("At line #{__LINE__} parameters are clientAreaID is = #{clientAreaID}, clientAreaPaymentCode = #{clientAreaPaymentCode}, clientMobileNo = #{clientMobileNo} and clientWaterBillNo = #{clientWaterBillNo}")
 
    response = validate(currentAgent, Constants::BLANK_VALUE, Constants::BLANK_VALUE, clientAreaID, clientAreaPaymentCode, clientMobileNo, clientWaterBillNo, Constants::BLANK_VALUE, Constants::BLANK_VALUE, Constants::BLANK_VALUE, false)
    if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
      return response
    end

    if !clientMobileNo.start_with? UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s and  !clientMobileNo.start_with? UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s
   	  clientMobileNo = UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s + clientMobileNo.to_s
		end
    AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{currentAgent.id}, agent Phone = #{currentAgent.phone} and name is = #{Utility.getAgentFullName(currentAgent)}")

    requestData =  QuickTellerUtility.getValidateCustomerReqData(clientWaterBillNo, clientMobileNo, clientAreaPaymentCode)
    AppLog.logger.info("At line #{__LINE__} request data send to the Quick Teller on URL = #{Properties::QUICK_TELLER_VALIDATE_URL} is = \n #{requestData}")

    responseData = QuickTellerUtility.sendRequestToQuickTeller(Properties::QUICK_TELLER_VALIDATE_URL, Constants::BLANK_VALUE, requestData, UgandaConstants::HTTP_POST_METHOD, Time.now.to_i)
    AppLog.logger.info("At line #{__LINE__} response come from the Quick Teller side is = \n #{responseData}")

    return QuickTellerUtility.getValidateCustomerResponse(responseData)
	end	

	def self.payCustomerWaterBill(agent, params, request)
		AppLog.logger.info("At line #{__LINE__} inside the UgandaNationalWaterBillManager class and payCustomerWaterBill method is called")
    response = Response.new
		
		requestReference = params[""+AppConstants::REQ_REF_PARAM_LABEL+""]
    transactionReference = params[""+AppConstants::TXN_REF_PARAM_LABEL+""]
    clientAreaID = params[""+AppConstants::CLIENT_AREA_ID_PARAM_LABEL+""]
    clientAreaPaymentCode = params[""+AppConstants::CLIENT_AREA_PAYMENT_CODE_PARAM_LABEL+""]
		clientMobileNo = params[""+AppConstants::CLIENT_MOBILE_NO_PARAM_LABEL+""]
		clientWaterBillNo = params[""+AppConstants::CLIENT_WATER_BILL_NO_PARAM_LABEL+""]
		rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
    customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
		appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
    AppLog.logger.info("At line #{__LINE__} and parameters are requestReference = #{requestReference}, transactionReference = #{transactionReference}, clientAreaID = #{clientAreaID}, clientAreaPaymentCode = #{clientAreaPaymentCode}, clientMobileNo = #{clientMobileNo}, clientWaterBillNo = #{clientWaterBillNo}, rechargeAmount = #{rechargeAmount}, customerFee = #{customerFee} and appTxnUniqueID = #{appTxnUniqueID}")
 
    response = validate(agent, requestReference, transactionReference, clientAreaID, clientAreaPaymentCode, clientMobileNo, clientWaterBillNo, rechargeAmount, customerFee, appTxnUniqueID, true)
    if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
      return response
    end

    calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(UgandaConstants::NATIONAL_WATER_BILL_LABEL, UgandaConstants::NATIONAL_WATER_BILL_LABEL, rechargeAmount)
    if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
        return calculatedResponse
    end

    AppUtility.savePendingRequest(appTxnUniqueID, UgandaConstants::NATIONAL_WATER_BILL_LABEL, UgandaConstants::NATIONAL_WATER_BILL_LABEL, clientMobileNo, rechargeAmount, agent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
    AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
    
    gatewayLogID = GatewayLog.getGatewayLogID
    aggregatorName = AppUtility.getAggregatorName(UgandaConstants::NATIONAL_WATER_BILL_LABEL)
    GatewayLog.saveGatewayLog(gatewayLogID, agent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
    AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")
    
    if Properties::IS_PRODUCTION_SERVER    
      processingResponse = processWaterBillRequest(requestReference, transactionReference, clientAreaID, clientAreaPaymentCode, clientMobileNo, clientWaterBillNo, rechargeAmount, customerFee,gatewayLogID)
    else 
      processingResponse = getRowData
    end  
    AppLog.logger.info("At line #{__LINE__} and response code after hit to URL is = #{processingResponse.Response_Code}. (if it is 200 then saved into database)") 

    if processingResponse.Response_Code.to_i != Constants::SUCCESS_RESPONSE_CODE
        AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
        AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of the transaction id = #{appTxnUniqueID}")
      return processingResponse 
    else
        requestParams = setRequestParams(rechargeAmount, customerFee, clientMobileNo, clientAreaID, clientAreaPaymentCode, clientWaterBillNo, requestReference)
      return AppUtility.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
    end   
	end		

  def self.processWaterBillRequest(requestReference, transactionReference, clientAreaID, clientAreaPaymentCode, customerMobile, clientWaterBillNo, rechargeAmount, customerFee, gatewayLogID)
    response = Response.new

    begin
      AppLog.logger.info("At line #{__LINE__} and inside the UgandaElectricityRechargeManager's sendMeterRechargeRequest method is called")

      if !customerMobile.start_with? UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s and  !customerMobile.start_with? UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s
        customerMobile = UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s + customerMobile.to_s
      end

      requestData = QuickTellerUtility.getRechargeCustomerReqData(rechargeAmount, customerFee, requestReference, clientWaterBillNo, customerMobile, transactionReference, clientAreaPaymentCode)
      GatewayLog.updateRequestData(gatewayLogID, requestData)
      AppLog.logger.info("At line #{__LINE__} request data send to the Quick Teller on URL = #{Properties::QUICK_TELLER_TRANSACTION_URL} is = \n #{requestData}")

      responseData = QuickTellerUtility.sendRequestToQuickTeller(Properties::QUICK_TELLER_TRANSACTION_URL, Constants::BLANK_VALUE, requestData, UgandaConstants::HTTP_POST_METHOD, Time.now.to_i)
      GatewayLog.updateResponseData(gatewayLogID, responseData)
      AppLog.logger.info("At line #{__LINE__} response come from the Quick Teller side is = \n #{responseData}")
      
      if responseData.present?
        isJSONValid = TxnManager.valid_json?(responseData) 
        AppLog.logger.info("At line #{__LINE__} check that the responseData is come in valid JSON or not = #{isJSONValid}")

        if isJSONValid
          jsonResponseData = JSON.parse(responseData)
          if jsonResponseData["responseCode"].to_i == UgandaConstants::QUICK_TELLER_SUCCESS_RESPONSE_CODE
              response.thirdPartyResponseID=(jsonResponseData["transferCode"])
              response.serviceBalance=(QuickTellerUtility.getInterswitchBalance)
              response.Response_Msg=(jsonResponseData["responseMessage"])
              response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            return response
          else
              response.Response_Code=(jsonResponseData["responseCode"])
              response.Response_Msg=(jsonResponseData["responseMessage"]) 
            return response
          end  
        else
            response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            response.Response_Msg=(responseData) 
          return response
        end    
      else
          response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
          response.Response_Msg=(responseData) 
        return response
      end
    rescue => exception
          AppLog.logger.info("At line #{__LINE__} and exception is occur inside the processWaterBillRequest method = \n#{exception.message}")
          AppLog.logger.info("At line #{__LINE__} and full exception is = \n#{exception.backtrace.join("\n")}")
        response.Response_Code=(Error::FAILED_TXN)
        response.Response_Msg=(exception.message)  
      return response  
    end
  end  

  def self.setRequestParams(rechargeAmount, customerFee, clientMobileNo, clientAreaID, clientAreaPaymentCode, clientWaterBillNo, requestReference)
    requestParams = RequestParams.new
      requestParams.serviceType=(UgandaConstants::NATIONAL_WATER_BILL_LABEL)
      requestParams.operator=(UgandaConstants::NATIONAL_WATER_BILL_LABEL)
      requestParams.rechargeAmount=(rechargeAmount)
      requestParams.customerFee=(customerFee)
      requestParams.clientMobileNo=(clientMobileNo)
      requestParams.clientAreaID=(clientAreaID.to_s + "|" + clientAreaPaymentCode.to_s)   
      requestParams.clientWaterBillNo=(clientWaterBillNo)   
      requestParams.description=(I18n.t :WATER_BILL_RECH_RQST_DES)
      requestParams.serviceTxnID=(requestReference)
    return requestParams        
  end   
  	
	def self.validate(currentAgent, requestReference, transactionReference, clientAreaID, clientAreaPaymentCode, clientMobileNo, clientWaterBillNo, rechargeAmount, customerFee, appTxnUniqueID, isRecharge)
    
    response = Response.new

      unless currentAgent.present?
          AppLog.logger.info("Inside the API Request Credits Controller's  requestCredit method at line #{__LINE__} and agent is not present")
          response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
          response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
        return response
      end

      if !clientAreaID.present? || !clientAreaPaymentCode.present? || !clientMobileNo.present? || !clientWaterBillNo.present?
          AppLog.logger.info("At line #{__LINE__} and client meter number is come nil from the app")
          response.Response_Code=(Error::INVALID_DATA)
          response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
        return response
      end    

      isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
      AppLog.logger.info("At line #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
      if isAccountSuspend
          response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
          response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
        return response
      end  
      
      if isRecharge   
         
        if !requestReference.present? || !transactionReference.present? || !rechargeAmount.present? || !customerFee.present? || !appTxnUniqueID.present?
            AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
            response.Response_Code=(Error::INVALID_DATA)
            response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
          return response
        end    

        if rechargeAmount.to_f <= 0
            AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
            response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
            response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
          return response
        end

        if (rechargeAmount.to_f%10) != 0
            AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
            response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
            response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
          return response
        end
        
        isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
        if isAppUniqueTxnIdExist
            AppLog.logger.info("At line #{__LINE__} and unique txn id that is generated by the app is exist")
            response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
            response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
          return response
        end

        if AppUtility.isServiceBalanceDeceedThreshold(UgandaConstants::NATIONAL_WATER_BILL_LABEL)
          AppLog.logger.info("At line #{__LINE__} and servcie balance is deceed then the threshold and email is sent to the admins")
          AppUtility.sendThresholdEmailToAdmin(UgandaConstants::NATIONAL_WATER_BILL_LABEL)
        end 

        isServiceEnable = Utility.checkServiceStatus(UgandaConstants::NATIONAL_WATER_BILL_LABEL)
        if !isServiceEnable
            AppLog.logger.info("At line #{__LINE__} and service is currently disable in service_setting table")
            response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
            response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
          return response
        end

        agentAccountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
        agentTotalVATPending = TxnHeader.my_money(VatManager.getAgentTotalVAT(currentAgent.id, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)).to_f
        AppLog.logger.info("At line #{__LINE__} account balance of the agent is = #{agentAccountBalance} and pending agent VAT is = #{agentTotalVATPending}")
        if (agentAccountBalance - agentTotalVATPending) <   TxnHeader.my_money(rechargeAmount).to_f +   TxnHeader.my_money(customerFee).to_f
                AppLog.logger.info("At line #{__LINE__} and agent account balance is below then the rechargeAmount")
                response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
                response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
            return response
        end

      end    

    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    	response.Response_Msg=("") 
  	return response  
  end

  def self.getRowData
    response = Response.new
        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.serviceBalance=("423,980")
        response.serviceTxnID=("123565454554")
    return response  
  end  
  
end	