# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class MFCommReportManager

	def self.getDetails(params)

		Log.logger.info("At line = #{__LINE__} and inside the MFCommReportManager class's getDetails method")

		response = Response.new

		serviceType = params[:serviceTypeCommReport]
		subType = params[:subTypeCommReport]
		userType  = params [:userTypeCommReport]
		userPhone = params[:phoneCommReport]
		viewType = params[:viewTypeCommReport]
		month = params[:monthCommReport]
		year = params[:yearCommReport]

		Log.logger.info("Parameters to fetch the details are service_type = #{serviceType}, subType = #{subType}, userType = #{userType}, userPhone = #{userPhone}, viewType = #{viewType}, month = #{month} and year = #{year}")

		fromDate = Utility.getMonthBeginDate(month, year)
		toDate = Utility.getMonthEndDate(month, year)

		Log.logger.info("At line = #{__LINE__} and from date is = #{fromDate} and to date is = #{toDate}")

		if userType.eql? Constants::SINGLE_USER_RADIO_LABEL
			
			agent =	Utility.getCurrentAgentByPhone(userPhone)

			if agent.present?
				txnHeader = fetchDetails(serviceType, subType, userType, agent.id, fromDate, toDate)
			else
					response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
		     		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
	     		return response 
			end	
		else 
			txnHeader = fetchDetails(serviceType, subType, userType, nil, fromDate, toDate)
		end	

		if txnHeader.present?
			totalMFComm = 0 
			txnHeader.each do |txn|
				totalMFComm = totalMFComm.to_f + txn['commission'].to_f
			end	
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.detailsHash=(txnHeader)
			response.totalValue=(totalMFComm)
		else
			response.Response_Code=(Error::NO_HISTORY)
		   	response.Response_Msg=(I18n.t :NO_HISTORY_MSG) 
		end	

	  return response	 

	end	


	def self.fetchDetails(serviceType, subType, userType, userID, fromDate, toDate)
		
		if serviceType.eql? Constants::MTOPUP_LABEL and isSubTypeUsed(subType)
			
			Log.logger.info("At line = #{__LINE__} inside the if statement of both the serviceType and subType are present")
			
			if subType.eql? Constants::ANY_COMPANY_LABEL	# If fetch the record of all the sub type i.e Airtel, Mtn and Tigo

				Log.logger.info("At line = #{__LINE__} inside the if statement all subType (i.e Airtel, Mtn and Tigo) ")

				if userType.eql? Constants::SINGLE_USER_RADIO_LABEL and  userID.present?
					Log.logger.info("At line = #{__LINE__} inside the if statement of #{Constants::SINGLE_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' "
                    query << "AND txn_date <= '#{toDate}' AND service_type = '#{serviceType}' AND agent_id::INT = #{userID} AND "
                    query << "sub_type IN (#{Utility.getSQLQueryAirtimeSubTypes}) GROUP BY agent_id"

				else
					Log.logger.info("At line = #{__LINE__} inside the else statement of #{ Constants::ALL_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND  service_type = '#{serviceType}' AND sub_type IN (#{Utility.getSQLQueryAirtimeSubTypes}) "
                    query << "GROUP BY agent_id"

				end	
			else
				
				Log.logger.info("At line = #{__LINE__} inside the else statement only one  subType")

				if userType.eql? Constants::SINGLE_USER_RADIO_LABEL and userID.present?
					Log.logger.info("At line = #{__LINE__} inside the if statement of #{Constants::SINGLE_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND  service_type = '#{serviceType}' AND agent_id::INT = #{userID} AND "
                    query << "sub_type = '#{subType}'  GROUP BY agent_id"

				else
					Log.logger.info("At line = #{__LINE__} inside the else statement of #{ Constants::ALL_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND  service_type = '#{serviceType}' AND sub_type = '#{subType}'  GROUP BY agent_id"

				end	
			
			end

		else
			Log.logger.info("At line = #{__LINE__} inside the else statement of only serviceType is present")

			if serviceType.eql? Constants::ANY_SERVICE_LABEL # If fetch the record of all the services i.e Airtime, electricity and dth

				Log.logger.info("At line = #{__LINE__} inside the if statement of all Services")

				if userType.eql? Constants::SINGLE_USER_RADIO_LABEL and  userID.present?
					Log.logger.info("At line = #{__LINE__} inside the if statement of #{Constants::SINGLE_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND  agent_id::INT = #{userID} AND service_type IN(#{Utility.getSQLQueryServiceTypes}) "
                    query << "GROUP BY agent_id"

				else
					Log.logger.info("At line = #{__LINE__} inside the else statement of #{ Constants::ALL_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND service_type IN(#{Utility.getSQLQueryServiceTypes}) "
                    query << "GROUP BY agent_id"

                end 

			else
				Log.logger.info("At line = #{__LINE__} inside the else statement of #{serviceType} Service")

				if userType.eql? Constants::SINGLE_USER_RADIO_LABEL and userID.present?
					Log.logger.info("At line = #{__LINE__} inside the if statement of #{Constants::SINGLE_USER_RADIO_LABEL}")

					query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND   agent_id::INT = #{userID} AND service_type = '#{serviceType}'  GROUP BY agent_id"
                else
                	Log.logger.info("At line = #{__LINE__} inside the else statement of #{ Constants::ALL_USER_RADIO_LABEL}")

                	query = "SELECT agent_id, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                    query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                    query << "txn_date <= '#{toDate}' AND  service_type = '#{serviceType}'  GROUP BY agent_id"
                end        	

			end	
		end	

		Log.logger.info("At line #{__LINE__} and query is used to fethc the details is = #{query}")

		txnHeader = Utility.executeSQLQuery(query)

	  return txnHeader	

	end	


	def self.fetchSingleUserDetails(params)
		
		response = Response.new

		agent = Utility.getCurrentAgentByPhone(params[:phoneCommReport])

		unless agent.present?
				response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
		     	response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
	     	return response 
		end
		
		serviceType = params[:serviceTypeCommReport]
		subType = params[:subTypeCommReport]
		userID = agent.id
		month = params[:monthCommReport]
		year = params[:yearCommReport]

		fromDate = Utility.getMonthBeginDate(month, year)
		toDate = Utility.getMonthEndDate(month, year)


		Log.logger.info("At line = #{__LINE__} inside the function of fetchSingleUserDetails")

		if serviceType.eql? Constants::MTOPUP_LABEL and isSubTypeUsed(subType)
			
			Log.logger.info("At line = #{__LINE__} inside the if statement of both the serviceType and subType are present")
			
			if subType.eql? Constants::ANY_COMPANY_LABEL	# If fetch the record of all the sub type i.e Airtel, Mtn and Tigo

				Log.logger.info("At line = #{__LINE__} inside the if statement all subType (i.e Airtel, Mtn and Tigo) ")

				query = "SELECT sub_type AS service_type, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
                query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                query << "txn_date <= '#{toDate}' AND agent_id::INT = #{userID} AND service_type = '#{serviceType}' AND "
                query << "sub_type IN (#{Utility.getSQLQueryAirtimeSubTypes}) GROUP BY sub_type"

			else
				
				Log.logger.info("At line = #{__LINE__} inside the else statement only one  subType")
				
				query = "SELECT sub_type AS service_type, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
				query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
				query << "txn_date <= '#{toDate}' AND agent_id::INT = #{userID} AND service_type = '#{serviceType}' AND "
				query << "sub_type = '#{subType}' GROUP BY sub_type"
			
			end

		else
			
			Log.logger.info("At line = #{__LINE__} inside the else statement of only serviceType is present")

			if serviceType.eql? Constants::ANY_SERVICE_LABEL # If fetch the record of all the services i.e Airtime, electricity and dth

				Log.logger.info("At line = #{__LINE__} inside the if statement of all Services")
			
				query = "SELECT service_type, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
				query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
				query << "txn_date <= '#{toDate}' AND agent_id::INT = #{userID} AND service_type IN(#{Utility.getSQLQueryServiceTypes}) "
				query << "GROUP BY service_type"

			else
				Log.logger.info("At line = #{__LINE__} inside the else statement of #{serviceType} Service")

				query = "SELECT sub_type AS service_type, SUM(amount) AS turnover, SUM(agent_commission) AS commission FROM txn_headers "
				query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
				query << "txn_date <= '#{toDate}' AND agent_id::INT = #{userID} AND service_type = '#{serviceType}' "
				query << "GROUP BY sub_type"

			end	
		end	

		Log.logger.info("At line #{__LINE__} and query is used to fethc the details is = #{query}")

		txnHeader = Utility.executeSQLQuery(query)

		if txnHeader.present?
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.detailsHash=(txnHeader)
		else
			response.Response_Code=(Error::NO_HISTORY)
		   	response.Response_Msg=(I18n.t :NO_HISTORY_MSG) 
		end	

	  return response	

	end	


	def self.isSubTypeUsed(subType)
		if subType.eql? Constants::AIRTEL_LABEL or subType.eql? Constants::MTN_LABEL or subType.eql? Constants::FDI_TIGO_LABEL or subType.eql? UgandaConstants::AFRICELL_LABEL or subType.eql? UgandaConstants::VODAFONE_LABEL or subType.eql? UgandaConstants::K2_TELECOM_LABEL or subType.eql? UgandaConstants::SMART_TELECOM_LABEL or subType.eql? UgandaConstants::UGANDA_TELECOM_LABEL
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end	

	end	
end