# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:NestedIterators: { max_allowed_nesting: 4}

class PaywayUtility

	def self.getPaywayBalance
        begin
	            AppLog.logger.info("At line #{__LINE__} and inside the PaywayUtility class and getPaywayBalance method is called")

	            response = Response.new

	            thirdParty = ThirdPartyConfig.getThirdparty(UgandaConstants::PAYWAY_AGGREGATOR)

	            requestData = getProfileXML(thirdParty.username, thirdParty.password, Utility.getCurrentDateTimePayWay)

	            responseData = sendPaywayRequest(requestData, Properties::PAYWAY_GET_BALANCE_URL, thirdParty.connection_timeout.to_i)
	            AppLog.logger.info("At line #{__LINE__} and Response come from the tomcat server is = \n #{responseData}")
	            
	            if responseData.present? and responseData.include? '<amount>'
	                paywayBalance = Utility.getTagValue(responseData, 'amount')
	            else
	            	paywayBalance = Constants::BLANK_VALUE 
	            end 

            return paywayBalance
        rescue => exception
            	AppLog.logger.info("At line #{__LINE__} and exception is occur inside the getPaywayBalance method = \n#{exception.message}")
            	AppLog.logger.info("At line #{__LINE__} and full exception is = \n#{exception.backtrace.join("\n")}")
        	return Constants::BLANK_VALUE
        end
    end

    def self.getClientCharges(serviceProvider, amount)
        begin
	            AppLog.logger.info("At line #{__LINE__} and inside the PaywayUtility class and getClientCharges method is called")

	            thirdPartyURL = Properties::PAYWAY_GET_BALANCE_URL
				thirdPartyURL = thirdPartyURL + "?userAction=1"

	            thirdParty = ThirdPartyConfig.getThirdparty(UgandaConstants::PAYWAY_AGGREGATOR)

	            requestData = getClientChargesXML(thirdParty.username, thirdParty.password, Utility.getCurrentDateTimePayWay)

	            responseData = sendPaywayRequest(requestData, thirdPartyURL, thirdParty.connection_timeout.to_i)
	            AppLog.logger.info("At line #{__LINE__} and response is come from the tomcat server but not print as it is too long but will come in java logs")
	            
	            response = getApplicableCharges(responseData, serviceProvider, amount)

            return response
        rescue => exception
            	AppLog.logger.info("At line #{__LINE__} and exception is occur inside the getPaywayBalance method = \n#{exception.message}")
            	AppLog.logger.info("At line #{__LINE__} and full exception is = \n#{exception.backtrace.join("\n")}")
        	return Constants::BLANK_VALUE
        end
    end

    def self.sendPaywayRequest(requestData, thirdPartyURL, connectionTimeout)
		begin
  			AppLog.logger.info("At line #{__LINE__} inside the PaywayUtility class and sendPaywayRequest method is called and request send to the tomcat server is = \n #{requestData}")
	       
	        thirdPartyURI = URI.parse(thirdPartyURL)
	        http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
	        http.read_timeout = connectionTimeout

	        request = Net::HTTP::Post.new(thirdPartyURI.request_uri)
	        request['content-type'] = 'text/xml'
	        request.body = requestData
	           
	        response = http.request(request)
	        responseData = response.body

	      return responseData
	    rescue => exception
	       	AppLog.logger.info("At line #{__LINE__} and exception is occur in sendPaywayRequest method = \n #{exception.message}")
	       	AppLog.logger.info("At line #{__LINE__} and full exception is = \n #{exception.backtrace.join("\n")}")
	     	return exception.message
	  	end 
	end	

    def self.getApplicableCharges(responseData, serviceProvider, amount)
    	response = Response.new
	    	applicableChargeTypeArray, maxAmountArray, minAmountArray, chargesArray = Array.new, Array.new, Array.new, Array.new
        	applicableCharges, applicableChargeType = Constants::BLANK_VALUE, Constants::BLANK_VALUE

	    	xmlResponseData = Nokogiri::XML(responseData)
	        returnItem = xmlResponseData.css('return')
	       
	        returnItem.css('tiers').each do |tiers|
	          	serviceProviderId = tiers.css('serviceProviderId').to_s.gsub("<serviceProviderId>", "").gsub("</serviceProviderId>", "")
	          	if serviceProviderId.eql? serviceProvider
			            if tiers.css('items').present?
			                tiers.css('items').each do |subItem|
			                  	maxAmount = subItem.css('maxAmount').to_s.gsub("<maxAmount>", "").gsub("</maxAmount>", "")
			                  	maxAmountArray.push(maxAmount)

			                  	minAmount = subItem.css('minAmount').to_s.gsub("<minAmount>", "").gsub("</minAmount>", "")
			                  	minAmountArray.push(minAmount)

			                  	chargesType = subItem.css('type').to_s.gsub("<type>", "").gsub("</type>", "")
			                  	applicableChargeTypeArray.push(chargesType)

			                  	chargeAmount = subItem.css('value').to_s.gsub("<value>", "").gsub("</value>", "")
			                  	chargesArray.push(chargeAmount)  
			                end 
			            else
			                applicableCharges = tiers.css('defaultValue').to_s.gsub("<defaultValue>", "").gsub("</defaultValue>", "")
			                applicableChargeType = tiers.css('defaultType').to_s.gsub("<defaultType>", "").gsub("</defaultType>", "")
			            end  
		            break
	          	end  
	        end  

	        if maxAmountArray.size > 0
	          	for i in 0..maxAmountArray.size.to_i - 1
		            if amount.to_f >= minAmountArray[i].to_f && amount.to_f <= maxAmountArray[i].to_f
		                	applicableCharges = chargesArray[i]
		                	applicableChargeType = applicableChargeTypeArray[i]
		              	break
		            elsif amount.to_f <= minAmountArray[i].to_f && amount.to_f >= maxAmountArray[i].to_f
		                	applicableCharges = chargesArray[i]
		                	applicableChargeType = applicableChargeTypeArray[i]
		              	break
		            end    
	          	end  
	        end  

	        response.applicableCharges=(applicableCharges)
	        response.applicableChargesType=(applicableChargeType)
	    return response    
    end

    def self.calculateClientCharges(serviceProvider, txnAmount)
		response = getClientCharges(serviceProvider, txnAmount)
		
		applicableType = response.applicableChargesType
		applicableAmount = response.applicableCharges
		AppLog.logger.info("At line #{__LINE__} and applicableType is = #{applicableType} and applicableAmount = #{applicableAmount} on the txnAmount = #{txnAmount}")
		if applicableType.eql? UgandaConstants::PAYWAY_APPLICABLE_CHARGES_ABSOLUTE_TYPE
			return applicableAmount
		elsif applicableType.eql? UgandaConstants::PAYWAY_APPLICABLE_CHARGES_PERCENTAGE_TYPE
			return ((txnAmount.to_f * applicableAmount.to_f)/100).round
		else
			return 0
		end	
    end	

    def self.getProfileXML(paywayUsername, paywayPassword, dateTime)
			newLine = "\n"
			tab = "\t"

			requestData = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">#{newLine}"
	        
	    	requestData << "#{tab}<s:Header>#{newLine}"
	    	requestData << "#{tab}#{tab}<Action s:mustUnderstand=\"1\" xmlns=\"http://schemas.microsoft.com/ws/2005/05/addressing/none\" />#{newLine}"
	    	requestData << "#{tab}</s:Header>#{newLine}"
	    
	    	requestData << "#{tab}<s:Body xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">#{newLine}"
	    	requestData << "#{tab}#{tab}<getProfile xmlns=\"http://www.avs.ug/external/v3\">#{newLine}"
	    	
	    	requestData << "#{tab}#{tab}#{tab}<arg0 xmlns=\"\">#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}<auth>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}#{tab}<password>#{paywayPassword}</password>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}#{tab}<userName>#{paywayUsername}</userName>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}</auth>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}<dateTime>#{dateTime}</dateTime>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}</arg0>#{newLine}"
	    	
	    	requestData << "#{tab}#{tab}</getProfile>#{newLine}"
	    	requestData << "#{tab}</s:Body>#{newLine}"
	    
	    	requestData << "</s:Envelope>"

    	return requestData
	end	

	def self.getClientChargesXML(paywayUsername, paywayPassword, dateTime)
			newLine = "\n"
			tab = "\t"

			requestData = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">#{newLine}"
	        
	    	requestData << "#{tab}<s:Header>#{newLine}"
	    	requestData << "#{tab}#{tab}<Action s:mustUnderstand=\"1\" xmlns=\"http://schemas.microsoft.com/ws/2005/05/addressing/none\" />#{newLine}"
	    	requestData << "#{tab}</s:Header>#{newLine}"
	    
	    	requestData << "#{tab}<s:Body xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">#{newLine}"
	    	requestData << "#{tab}#{tab}<getTiersInfo xmlns=\"http://www.avs.ug/external/v3\">#{newLine}"
	    	
	    	requestData << "#{tab}#{tab}#{tab}<arg0 xmlns=\"\">#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}<auth>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}#{tab}<password>#{paywayPassword}</password>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}#{tab}<userName>#{paywayUsername}</userName>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}</auth>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}#{tab}<dateTime>#{dateTime}</dateTime>#{newLine}"
	    	requestData << "#{tab}#{tab}#{tab}</arg0>#{newLine}"
	    	
	    	requestData << "#{tab}#{tab}</getTiersInfo>#{newLine}"
	    	requestData << "#{tab}</s:Body>#{newLine}"
	    
	    	requestData << "</s:Envelope>"

    	return requestData
	end	

	def self.getServiceFeeMesage
			message = "<p style = 'font-size: 16px;'><b>Information</b></p>"
			message << "<ul style = 'font-size: 14px; text-align: left;' >"
			message << "<li style = 'padding: 5px 0'>For Electricity and National Water Bill ARED is getting commission from Aggregator.</li>"
			message << "<li style = 'padding: 5px 0'>The amount charged from the end-user will be transaction amount plus commission.</li>"
			message << "<li style = 'padding: 5px 0'>This commission is being collected by the Micro Franchisee so we need to collect operator fee part from the Micro Franchisee.</li>"
			message << "<li style = 'padding: 5px 0'>The Service Fee configuration should be defined in percentage value only and will be computed as ratio of 100%.</li>"
			message << "</ul>"
		return message
	end	

end	