# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class CommissionManager

	def self.getAgentCommissionList(currentAgent)

    	AppLog.logger.info("CommissionManager's getAgentCommission is called at line #{__LINE__}")

    	response = validate(currentAgent, nil, nil, false)
	
    	if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end

        txnHeader = getCommHeader(currentAgent, nil, nil, false)

		if txnHeader.present?
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.commission_list=(txnHeader)
		else
			response.Response_Code=(Error::NO_HISTORY)
		   	response.Response_Msg=(I18n.t :NO_TXN_FOUND_MSG) 
		end	

        return response	
   
    end 


    def self.getAgentCommissionBetweenDates(currentAgent, params)
	    	
    	fromDate = params[""+AppConstants::FROM_DATE_PARAM_LABEL+""]
    	toDate 	 = params[""+AppConstants::TO_DATE_PARAM_LABEL+""]
   		
   		response = validate(currentAgent, fromDate, toDate, true)
    	
    	if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
		    
        fromDate= Date.parse(fromDate.to_s).beginning_of_day
        toDate =  Date.parse(toDate.to_s).end_of_day
        
		txnHeader = getCommHeader(currentAgent, fromDate, toDate, true)

		if txnHeader.present?
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			serviceTypeArray = Array.new
			sumAmountArray = Array.new
			txnHeader.each do |header|
				serviceTypeArray.push(Utility.getServiceTypeDisplayValue(header['service_type']))
				sumAmountArray.push(header['commission'])
			end	
			response.serviceSoldNameArray=(serviceTypeArray)
			response.serviceSoldValueArray=(sumAmountArray)	
		else
			response.Response_Code=(Error::NO_HISTORY)
		   	response.Response_Msg=(I18n.t :NO_TXN_FOUND_MSG) 
		end	

        return response	
		
    end	

    def self.getCommHeader(currentAgent, fromDate, toDate, isDatesCheck)
    	if isDatesCheck
	    	query = "SELECT service_type, sum(agent_commission) AS commission FROM txn_headers "
			query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}  "
			query << "AND txn_date >= '#{fromDate}' AND txn_date <= '#{toDate}' "
			query << "AND  agent_id::INT = #{currentAgent.id} "
			query << "AND service_type IN(#{Utility.getSQLQueryServiceTypes}) "
			query << "GROUP BY service_type"
		else
			query = "SELECT service_type, sum(amount) AS turnover, sum(agent_commission) AS commission FROM txn_headers "
			query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} "
			query << "AND txn_date >= '#{Utility.getTodayBeginDate}' AND txn_date <= '#{Utility.getTodayEndDate}' "
			query << "AND  agent_id::INT = #{currentAgent.id} "
			query << "AND service_type IN(#{Utility.getSQLQueryServiceTypes}) "
			query << "GROUP BY service_type"
		end			
        
		txnHeader = Utility.executeSQLQuery(query)	

		return txnHeader
    end	

    def self.validate(currentAgent, fromDate, toDate, isDatesCheck)
    	response = Response.new

		 	unless currentAgent.present?
	                AppLog.logger.info("Inside the MobileRechargeManager's  validate method at line #{__LINE__} and agent is not present")
	                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	            return response
	        end

	        if isDatesCheck
		        if fromDate.eql? Constants::BLANK_VALUE or toDate.eql? Constants::BLANK_VALUE
		                AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
		                response.Response_Code=(Error::INVALID_DATA)
		                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
		            return response
		    	end   

		    	if fromDate > toDate
			       		response.Response_Code=(Error::CHECK_DATES_AGAIN)
				    	response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
				    return response
				end
			end	

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.Response_Msg=("") 
		return response
    end	


    def self.searchDetails(user_type,phone,month,year)

    	response = Response.new

    	Log.logger.info("Commission Manager's searchDetails method for web is called at line #{__LINE__}")
    	Log.logger.info("At line = #{__LINE__} and parameters are user type = #{user_type} and phone = #{phone}")

    	if user_type == "All Users"
    		Log.logger.info("At line = #{__LINE__} and inside the if of all users")
    		commission_list = searchDetailsAllUsers(month,year)
    	
    	elsif user_type == "Single User"
    		Log.logger.info("At line = #{__LINE__} and inside the elsif of single user and search by the phone number = #{phone}")

    		user = User.where('role = 1 AND phone = ?',phone).first
    		Log.logger.info("At line = #{__LINE__} and check whether the user is exist or not = #{user.present?}")

	        if user.present?
	        	Log.logger.info("At line = #{__LINE__} and status of the user is = #{user.status}")

	        	if user.status == "active"
	        		commission_list = searchDetailsSingleUser(month,year,user)
	            else
	            		Log.logger.info("At line = #{__LINE__} and MICRO FRANCHISEE is not active")
		            	response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_ACTIVE)
			     		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_ACTIVE_MSG) 
		  			return response
	            end  	
            else
	            	Log.logger.info("At line = #{__LINE__} and MICRO FRANCHISEE does not exist")
	            	response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
		     		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
		  		return response

            end  	
    	end	

    	if commission_list.present?
  			Log.logger.info("At line = #{__LINE__} and commission list is not empty and give to controller")
          	response.commission_list=(commission_list)
          	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	    else
	    	Log.logger.info("At line = #{__LINE__} and commission list is empty and nuthing give to controller")
        	response.Response_Code=(Error::NO_COMMISSION_EXIST)
        	response.Response_Msg=(I18n.t :NO_COMMISSION_EXIST_MSG) 
        end  
	  return response	    	
   	end 	


   	def self.searchDetailsAllUsers(month,year)
   		
   		Log.logger.info("At line = #{__LINE__} and searchDetailsAllUsers method is called")
		
		commission_list = Array.new

		monthNumber = Date::MONTHNAMES.index(month)

        Log.logger.info("At line  = #{__LINE__} and month number is = #{monthNumber}") 

      	month_beginning = Date.new(year.to_i, monthNumber.to_i)
      	month_ending = month_beginning.end_of_month

      	firstDate = Date.parse(month_beginning.to_s)
      	beginFromDate = firstDate.beginning_of_day

      	secondDate = Date.parse(month_ending.to_s)
      	endToDate = secondDate.end_of_day

      	commissionList = Array.new

      	sqlQuery = "SELECT user_id  FROM agent_commissions WHERE txn_date >= '#{beginFromDate}'	AND txn_date <=  '#{endToDate}' "
	    sqlQuery << "AND status = 0 GROUP BY user_id ORDER BY user_id"

	    usersList = AgentCommission.find_by_sql(sqlQuery)

	    if usersList.present?
	        usersList.each do |user|

	        	agent = User.find(user.user_id)

	        	user_account = agent.user_account
            	account_balance = user_account.balance

                loan_account = agent.loans.last
                if loan_account.present?
                  credit_balance = loan_account.balance
                else
                  credit_balance = 0
                end

                penalty_account = agent.agent_penalty_details.last
                if penalty_account.present?
                  penalty_balance = penalty_account.pending_penalty_amount
                else
                  penalty_balance = 0
                end
	            
	            Log.logger.info("At line = #{__LINE__} and user id whose Commission is exist in the month #{monthNumber} = #{user.user_id}")
	            
	            totalPayableCommission = getAgentCurrentMonthCommBalance(beginFromDate,endToDate,user.user_id).to_f
	            Log.logger.info("At line  = #{__LINE__} and Commission of this month is = #{totalPayableCommission}")

	            commissionListHash = ActiveSupport::HashWithIndifferentAccess.new

	            commissionListHash[:user_id]=user.user_id
	            commissionListHash[:name]=agent.name
            	commissionListHash[:last_name]=agent.last_name
            	commissionListHash[:phone]=agent.phone
            	commissionListHash[:account_balance]=account_balance
            	commissionListHash[:credit_balance]= credit_balance
            	commissionListHash[:penalty_balance]= penalty_balance
	            commissionListHash[:month]=monthNumber
	            commissionListHash[:year]=year
	            commissionListHash[:from_date]=beginFromDate
	            commissionListHash[:to_date]=endToDate
	            commissionListHash[:commission_balance]=totalPayableCommission
	            
	            commissionList << commissionListHash 
	        end 
	    end  
      return commissionList	
   	end	

   
   	def self.searchDetailsSingleUser(month,year,user)
   		
   		Log.logger.info("At line = #{__LINE__} and searchDetailsSingleUser method is called")
   		
   		commissionList = Array.new

		monthNumber =Date::MONTHNAMES.index(month)

        Log.logger.info("At line  = #{__LINE__} and month number is = #{monthNumber}") 

      	month_beginning = Date.new(year.to_i, monthNumber.to_i)
      	month_ending = month_beginning.end_of_month

      	firstDate = Date.parse(month_beginning.to_s)
      	beginFromDate = firstDate.beginning_of_day

      	secondDate = Date.parse(month_ending.to_s)
      	endToDate = secondDate.end_of_day

      	commissionList = Array.new

      	user_account = user.user_account
    	account_balance = user_account.balance

        loan_account = user.loans.last
        if loan_account.present?
          credit_balance = loan_account.balance
        else
          credit_balance = 0
        end

        penalty_account = user.agent_penalty_details.last
        if penalty_account.present?
          penalty_balance = penalty_account.pending_penalty_amount
        else
          penalty_balance = 0
        end
        
        Log.logger.info("At line = #{__LINE__} and user id whose Commission is exist in the month #{monthNumber} = #{user.id}")
       
	        
		totalPayableCommission = getAgentCurrentMonthCommBalance(beginFromDate,endToDate,user.id).to_f 
        Log.logger.info("At line  = #{__LINE__} and Commission of this month is = #{totalPayableCommission}")

        if totalPayableCommission!=0
	        commissionListHash = ActiveSupport::HashWithIndifferentAccess.new

	        commissionListHash[:user_id]=user.id
	        commissionListHash[:name]=user.name
	    	commissionListHash[:last_name]=user.last_name
	    	commissionListHash[:phone]=user.phone
	    	commissionListHash[:account_balance]=account_balance
	    	commissionListHash[:credit_balance]= credit_balance
	    	commissionListHash[:penalty_balance]= penalty_balance
	        commissionListHash[:month]=monthNumber
	        commissionListHash[:year]=year
	        commissionListHash[:from_date]=beginFromDate
	        commissionListHash[:to_date]=endToDate
	        commissionListHash[:commission_balance]=totalPayableCommission
	        
	        commissionList << commissionListHash
    	end
	     
      return commissionList	

   	end 
   	
   	def self.getAgentCurrentMonthCommBalance(beginFromDate, endToDate, user_id)
	   		query = "SELECT  SUM(post_balance - previous_balance)  FROM agent_commissions "
	        query << "WHERE txn_date >=  '#{beginFromDate}' AND txn_date <= '#{endToDate}' "
	        query << "AND user_id = '#{user_id}' AND status = 0"	

	   		agentCurrentMonthCommBalance = Utility.executeSQLQuery(query).first
	   		
	   		if agentCurrentMonthCommBalance["sum"] != nil
	        	commissionBalance = agentCurrentMonthCommBalance["sum"]
	      	else
	        	commissionBalance = 0 
	      	end 
	  	return commissionBalance
   	end	
   
   	def self.payCommission(agent_ids, isTaxDeduct, fromDate, toDate, commission, loggedInAdmin, request)
    	Log.logger.info("Commission Manager's payCommission method for web is called at line #{__LINE__}")
    	Log.logger.info("At line = #{__LINE__} and list of the agent_ids = #{agent_ids}")

    	totalSize = agent_ids.size
    	valueableSize = totalSize.to_i - 1

    	@commissionPDFList = Array.new

    	for i in 0..valueableSize
    		Log.logger.info("At line = #{__LINE__} and list of the agent_id = #{agent_ids[i]}")
    		response = paidCommission(agent_ids[i], isTaxDeduct, fromDate, toDate, commission[i], loggedInAdmin, request)
		end
		
      return response
    end	


    def self.paidCommission(user_id, isTaxDeduct, fromDate, toDate, commission_balance, loggedInAdmin, request)
   		response = Response.new	
   		txnUniqueID = Utility.getRandomNumber(15)

 		Log.logger.info("The Commission Manager's paidCommission method is called at line #{__LINE__} and Agent id is  = #{user_id}")   
 		
 		# First find the user with the user id
		current_agent = User.find(user_id)
	    Log.logger.info("At line #{__LINE__} and Agent phone is  = #{current_agent.phone}")

	    currentCommissionBalance = Utility.getAgentCommissionBalance(current_agent)

	    Log.logger.info("At line #{__LINE__} and current Commission Balance in account  is  = #{currentCommissionBalance} and approved commission = #{commission_balance}")

	    if commission_balance.to_f > currentCommissionBalance.to_f
		    	response.Response_Code=(Error::COMMISSION_MORE_THEN_PAYABLE)
		    	response.Response_Msg=(I18n.t :COMMISSION_MORE_THEN_PAYABLE_MSG)
	    	return response	
	    end	

	    #First we add one entry in the pending request table before processing
        AppUtility.savePendingRequest(txnUniqueID, Constants::USER_COMMISSION_PAYMENT_LABLE, Constants::USER_COMMISSION_PAYMENT_LABLE, nil, commission_balance, current_agent.id, Utility.getUserRemoteIP(request), loggedInAdmin.id, Constants::ADMIN_USER_ROLE)
        Log.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")

	    ActiveRecord::Base.transaction do
	    	#********* First we deduct the Tax from commission balance of the agent**************
	    	# This is used to check whether the admin can select the witholding tax deduct checkbox or not if it is checked then isTaxDeduct come true
	    	if isTaxDeduct
		    	# Now get the what tax percentage will be deduct
		    	taxDeductPercentage = getTaxDeductPercentage()
		    	Log.logger.info("At line #{__LINE__} and tax deduct percentage is = #{taxDeductPercentage}")

		    	totalTaxDeduct = ((TxnHeader.my_money(commission_balance).to_f * TxnHeader.my_money(taxDeductPercentage).to_f)/100).to_f
		    	totalTaxDeduct = TxnHeader.my_money(totalTaxDeduct).to_f
		    	Log.logger.info("At line #{__LINE__} and tax deduct amount is = #{totalTaxDeduct}")

		    	deductTax(totalTaxDeduct, current_agent, commission_balance, taxDeductPercentage)
		    end    

	        #********************** Now  deduct the pending commission and credit to his account*********************
	    	commBalanceAfterTaxPaid = TxnHeader.my_money(commission_balance).to_f - TxnHeader.my_money(totalTaxDeduct).to_f
	    	Log.logger.info("At line #{__LINE__} and commission balance after tax paid is = #{commBalanceAfterTaxPaid}")
	    	creditCommissionToAgent(commBalanceAfterTaxPaid, current_agent, fromDate, toDate, commission_balance, taxDeductPercentage, totalTaxDeduct)
	        
	        #********************** Now  deduct the Loan from his account*********************
	        isPenaltyDeduct = false

	        loan_balance = Utility.getAgentLoanBalance(current_agent).to_f
	        	
	        Log.logger.info("At line #{__LINE__} and npw check that his loan amount is more then 0 or not = #{loan_balance}")

	        if loan_balance > 0 # first check that whether the loan is pending or not
	        	if commBalanceAfterTaxPaid >= loan_balance
	        		Log.logger.info("At line #{__LINE__} and commBalanceAfterTaxPaid >= loan_balance deduct complete loan amount")
	        		
	        		settleLoan(current_agent, loan_balance)
	        		isPenaltyDeduct = true
	        	else
	        		Log.logger.info("At line #{__LINE__} and commBalanceAfterTaxPaid < loan_balance deduct complete commBalance After Tax Paid amount")
	        		settleLoan(current_agent, commBalanceAfterTaxPaid)
	        		isPenaltyDeduct = false 
	        	end		
	        else
	        	isPenaltyDeduct = true
	        end

	        #********************** Now  deduct the Penalty from his account*********************
	        Log.logger.info("At line #{__LINE__} and now check that his penalty is deduct or not #{isPenaltyDeduct}")

	        penalty_balance = Utility.getAgentPenaltyBalance(current_agent)

	        if isPenaltyDeduct == true && commBalanceAfterTaxPaid > 0
	        	 Log.logger.info("At line #{__LINE__} and his penalty amount is = #{penalty_balance}")
	        	if penalty_balance > 0
	        		amountAfterPayLoan = commBalanceAfterTaxPaid.to_f - loan_balance.to_f
	              	Log.logger.info("At line #{__LINE__} and commission after deduct loan  = #{amountAfterPayLoan}")
	              	if amountAfterPayLoan >= penalty_balance # Now adjust the penalty with his commission
	              		settlePenalty(current_agent, penalty_balance)	
	              	else
	              		settlePenalty(current_agent, amountAfterPayLoan)
	              	end 
		    	end
		    end	
	    end

      	    AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
        	Log.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")

	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	    	response.Response_Msg=(I18n.t :COMMISSION_SUCCESS_PAID)

	    return response	
    end	

    def self.getTaxDeductPercentage()

    	taxConfiguration = TaxConfiguration.where('service = ? and effective_date <= ?', "TAX",Date.today).order('effective_date ASC').last

    	Log.logger.info("At line = #{__LINE__} and check whether the tax percentage is exist in database or not = #{taxConfiguration.present?} ")

      	if taxConfiguration.present?
      		Log.logger.info("At line = #{__LINE__} and tax applicable id is = #{taxConfiguration.id} and percentage = #{taxConfiguration.percentage}")	

      		taxPercentage = taxConfiguration.percentage
      	else
      		taxPercentage = 0
      	end	
      return taxPercentage 	
    end	

    def self.deductTax(totalTaxDeduct, current_agent, commission_balance, taxDeductPercentage)

    	Log.logger.info("At line #{__LINE__} and total tax deduct = #{totalTaxDeduct}")
		
        txnHeader = getHeader(TxnHeader.my_money(totalTaxDeduct), Constants::TAX_DEDUCT_LABEL, (I18n.t :TAX_DEDUCT_DESC), current_agent.id,"")
	    txnHeader.save
	    Log.logger.info("At line = #{__LINE__} and Tax Deduct entry is saved in the TxnHeader table successfully")

	    optTaxTxnItem = getTxnItem(Utility.getTaxAdmin(), TxnHeader.my_money(totalTaxDeduct), false, Utility.getTaxAdminAccount(Utility.getTaxAdmin()))
     	optTaxTxnItem.header_id = txnHeader.id
	    optTaxTxnItem.save
	    Log.logger.info("At line = #{__LINE__} and Operator(Tax) transaction item is saved in the TxnItem table successfully")

	    optComtxnItem  = getTxnItem(Utility.getCommissionAdmin(), TxnHeader.my_money(totalTaxDeduct), true, Utility.getCommissionAdminAccount(Utility.getCommissionAdmin()))
	    optComtxnItem.header_id = txnHeader.id;
	    optComtxnItem.save
        Log.logger.info("At line = #{__LINE__} and Operator(Commission) transaction item is saved in the TxnItem table successfully")
      
        commissionBalanceNew = Utility.getAgentCommissionBalance(current_agent)
        agentCommission = getAgentCommission(current_agent, Constants::TAX_DEDUCT_LABEL, commissionBalanceNew,
        									  totalTaxDeduct, txnHeader.id, commission_balance, taxDeductPercentage, totalTaxDeduct)
        agentCommission.save
        Log.logger.info("At line #{__LINE__} and commission balance updated successfully and saved in the commission table")


  		TxnManager.update_account(Utility.getTaxAdminAccount(Utility.getTaxAdmin()), TxnHeader.my_money(totalTaxDeduct), false)
		TxnManager.update_account(Utility.getCommissionAdminAccount(Utility.getCommissionAdmin()), TxnHeader.my_money(totalTaxDeduct), true)  
	    Log.logger.info("At line = #{__LINE__} and balances of Operator(Tax) and Operator(Commission) Account are updated successfully")

    end	

    def self.creditCommissionToAgent(commBalanceAfterTaxPaid,current_agent,fromDate,toDate,total_commission,taxDeductPercentage,totalTaxDeduct)

    	Log.logger.info("At line #{__LINE__} and commissio after deduct the tax is = #{commBalanceAfterTaxPaid}")
    	
        txnHeader = getHeader(TxnHeader.my_money(commBalanceAfterTaxPaid), Constants::USER_COMMISSION_PAYMENT_LABLE, (I18n.t :COMMISSION_PAID_DESC),current_agent.id,"")
	    txnHeader.save
	    Log.logger.info("At line = #{__LINE__} and Commission payment entry is saved in the TxnHeader table successfully")


	    agentTxnItem  = getTxnItem(current_agent, TxnHeader.my_money(commBalanceAfterTaxPaid), false, Utility.getCurrentAgentAccount(current_agent))
	   	agentTxnItem.header_id = txnHeader.id
	    agentTxnItem.save
	    Log.logger.info("At line = #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")


	    optComtxnItem  = getTxnItem(Utility.getCommissionAdmin(), TxnHeader.my_money(commBalanceAfterTaxPaid), true, Utility.getCommissionAdminAccount(Utility.getCommissionAdmin()))
	    optComtxnItem.header_id = txnHeader.id;
	    optComtxnItem.save
		Log.logger.info("At line = #{__LINE__} and Operator(Commission) transaction item is saved in the TxnItem table successfully")

		updatePaidStatus(fromDate,toDate,current_agent.id,txnHeader.id)
		Log.logger.info("At line = #{__LINE__} and update the status of those transactions whose commission is paid successfully in agent_commission table")

        #Again get the last commission because a new entry of tax payment is saved
        commissionBalanceNew = Utility.getAgentCommissionBalance(current_agent)
        agentCommission = getAgentCommission(current_agent, Constants::USER_COMMISSION_PAYMENT_LABLE, commissionBalanceNew,
        									  commBalanceAfterTaxPaid, txnHeader.id, total_commission, taxDeductPercentage, totalTaxDeduct)
        agentCommission.save
        Log.logger.info("At line #{__LINE__} and commission balance updated successfully and saved in the commission table")
		
	    TxnManager.update_account(Utility.getCommissionAdminAccount(Utility.getCommissionAdmin()), TxnHeader.my_money(commBalanceAfterTaxPaid), true)
		TxnManager.update_account(Utility.getCurrentAgentAccount(current_agent), TxnHeader.my_money(commBalanceAfterTaxPaid), false)  
		Log.logger.info("At line = #{__LINE__} and balances of Operator(Commission) and Agent Account are updated successfully")

    end	

    def self.updatePaidStatus(fromDate,toDate,agentID,paymentRef)
    	#This query update  the status of those id's whose commission is paid to the agent
       	commissionPaidIDs = AgentCommission.find_by_sql('SELECT i.id , i.user_id 
       								FROM agent_commissions i 
       								WHERE i.user_id = '+agentID.to_s+' AND status = 0 AND i.txn_date >= \''+fromDate.to_s+'\' 
       								AND i.txn_date <= \''+toDate.to_s+'\'
       								GROUP BY i.user_id , i.id ORDER BY i.id')
	        
	    commissionPaidIDs.each do |commission|    
	        commission.update_attribute(:status, 1)
	        commission.update_attribute(:payment_ref, paymentRef)
	    end    
    end	
    
    def self.getAgentCommission(current_agent, service_type, commission_balance_new, deduct_amount, txn_header_id, total_commission, taxDeductPercentage, totalTaxDeduct)
		
		agent_commission = AgentCommission.new
			agent_commission.user_id          = current_agent.id
			agent_commission.service_type     = service_type
			agent_commission.sub_type         = service_type
			agent_commission.txn_date         = Time.now
			agent_commission.previous_balance = TxnHeader.my_money(commission_balance_new)
			agent_commission.post_balance     = TxnHeader.my_money(TxnHeader.my_money(commission_balance_new).to_f - TxnHeader.my_money(deduct_amount).to_f)
			agent_commission.header_id        = txn_header_id
			agent_commission.status           = 1
			agent_commission.payment_ref      = txn_header_id
			agent_commission.acc_balance      = TxnHeader.my_money(Utility.getAgentAccountBalance(current_agent))
			agent_commission.loan             = TxnHeader.my_money(Utility.getAgentLoanBalance(current_agent))
			agent_commission.penalty          = TxnHeader.my_money(Utility.getAgentPenaltyBalance(current_agent))
			agent_commission.total_comm       = TxnHeader.my_money(total_commission)
			agent_commission.tax_percentage   = TxnHeader.my_money(taxDeductPercentage)
			agent_commission.total_tax        = TxnHeader.my_money(totalTaxDeduct)
		return agent_commission  

    end

    def self.settleLoan(current_agent, loan_balance)
    	if loan_balance < 0
    		loan_balance = 0
    	end	
    		
    	loanTxnHeader = getHeader(TxnHeader.my_money(loan_balance), Constants::USER_LOAN_DEPOSIT_LABLE, (I18n.t :COMMISSION_ADJ_TO_LOAN_DESC),current_agent.id,"")
        loanTxnHeader.save
		Log.logger.info("At line = #{__LINE__} and Loan Deposit(loan_deposit) entry is saved in the TxnHeader table successfully")

        agentTxnItem  = getTxnItem(current_agent, TxnHeader.my_money(loan_balance), true, Utility.getCurrentAgentAccount(current_agent))
        agentTxnItem.header_id = loanTxnHeader.id
      	agentTxnItem.save
      	Log.logger.info("At line = #{__LINE__} and Agent loan transaction item is saved in the TxnItem table successfully")

      	opStockTxnItem  = getTxnItem(Utility.getStockAdmin(), TxnHeader.my_money(loan_balance), false, Utility.getStockAdminAccount(Utility.getStockAdmin()))
      	opStockTxnItem.header_id = loanTxnHeader.id;
      	opStockTxnItem.save
      	Log.logger.info("At line = #{__LINE__} and Operator Stock loan transaction item is saved in the TxnItem table successfully")

        loanRecord = getLoanRecord(current_agent, TxnHeader.my_money(loan_balance), Utility.getCurrentAgentLoanAccount(current_agent))
      	loanRecord.save
      	Log.logger.info("At line = #{__LINE__} and loan entry of the agent is saved in the Loans table successfully")      		

      	TxnManager.update_account(Utility.getStockAdminAccount(Utility.getStockAdmin()), TxnHeader.my_money(loan_balance), false)
		TxnManager.update_account(Utility.getCurrentAgentAccount(current_agent), TxnHeader.my_money(loan_balance), true)  
      	Log.logger.info("At line = #{__LINE__} and balances of Agent and Operator Stock Account are updated successfully")
    
    end	


    def self.settlePenalty(current_agent, penalty_balance)
    	
    	if penalty_balance < 0 
    		penalty_balance = 0
    	end

 		penaltyTxnHeader = getHeader(TxnHeader.my_money(penalty_balance), Constants::USER_PENALTY_CHARGE_LABLE, (I18n.t :COMMISSION_ADJ_TO_PENALTY_DESC),current_agent.id,"")
        penaltyTxnHeader.save
		Log.logger.info("At line = #{__LINE__} and Penalty Deposit(delay_charge) entry is saved in the TxnHeader table successfully")


        agentTxnItem  = getTxnItem(current_agent, TxnHeader.my_money(penalty_balance), true, Utility.getCurrentAgentAccount(current_agent))
        agentTxnItem.header_id = penaltyTxnHeader.id
        agentTxnItem.save
        Log.logger.info("At line = #{__LINE__} and Agent penalty transaction item is saved in the TxnItem table successfully")

  		optPenaltyItem  = getTxnItem(Utility.getPenaltyAdmin(), TxnHeader.my_money(penalty_balance), false, Utility.getPenaltyAdminAccount(Utility.getPenaltyAdmin()))
        optPenaltyItem.header_id = penaltyTxnHeader.id;
        optPenaltyItem.save
        Log.logger.info("At line = #{__LINE__} and Operator Penalty  transaction item is saved in the TxnItem table successfully")

        penaltyRecord = getPenaltyRecord(current_agent, TxnHeader.my_money(penalty_balance), Utility.getCurrentAgentPenaltyAccount(current_agent))
        penaltyRecord.header_id = penaltyTxnHeader.id
        penaltyRecord.save
		Log.logger.info("At line = #{__LINE__} and penalty entry of the agent is saved in the agent_penalty table successfully")          

   	    TxnManager.update_account(Utility.getPenaltyAdminAccount(Utility.getPenaltyAdmin()), TxnHeader.my_money(penalty_balance), false)
		TxnManager.update_account(Utility.getCurrentAgentAccount(current_agent), TxnHeader.my_money(penalty_balance), true)  
        Log.logger.info("At line = #{__LINE__} and balances of Agent and Operator Penalty Account are updated successfully")
    
    end	


    def self.serachCommPayment(params)

		Log.logger.info("Inside the Commission Manager class and serachCommPayment method is called at line  = #{__LINE__}")

    	month = params[:month]
		year = params[:year]
		response = Response.new

        unless month.present? || year.present?
                Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end 
			
		Log.logger.info("At line  = #{__LINE__} and parameters are month is = #{month} and year is = #{year}")

		monthNumber = Date::MONTHNAMES.index(month)

		if monthNumber < 12
			monthNumber = monthNumber.to_i + 1 #This is new changes to show the one month previous report
		else
			monthNumber = 1
			year = year.to_i + 1
		end	

		Log.logger.info("At line  = #{__LINE__} and month number is = #{monthNumber}") 

        month_beginning = Date.new(year.to_i, monthNumber.to_i)
        month_ending = month_beginning.end_of_month

		firstDate = Date.parse(month_beginning.to_s)
        beginFromDate = firstDate.beginning_of_day

        secondDate = Date.parse(month_ending.to_s)
        endToDate = secondDate.end_of_day

        Log.logger.info("At line = #{__LINE__} and begin of from date is = #{beginFromDate} and end of to date is = #{endToDate}")     
         
        sqlQuery = "SELECT u.id,u.name,u.last_name,u.phone,comm.txn_date,comm.acc_balance,comm.loan, "
		sqlQuery << "comm.penalty,comm.total_comm,comm.tax_percentage,comm.total_tax,comm.header_id  "
		sqlQuery << "FROM agent_commissions comm , users u "
		sqlQuery << "WHERE u.id = comm.user_id AND comm.service_type = '#{Constants::USER_COMMISSION_PAYMENT_LABLE}' "
		sqlQuery << "AND comm.txn_date >= '#{beginFromDate}' AND comm.txn_date <= '#{endToDate}' "
        sqlQuery << "ORDER BY comm.txn_date DESC"

        commPaidMonthlyList =  AgentCommission.find_by_sql(sqlQuery)

        Log.logger.info("At line = #{__LINE__} and check that whether that the  details in the database is exist or not = #{commPaidMonthlyList.present?}")
         
        commPaidArray = Array.new
          
        if commPaidMonthlyList.present?

            commPaidMonthlyList.each do |profitLose|

                commPaidArrayHash = ActiveSupport::HashWithIndifferentAccess.new

               	commPaidArrayHash[:user_id]=profitLose.id
                commPaidArrayHash[:name]=profitLose.name
                commPaidArrayHash[:last_name]=profitLose.last_name
                commPaidArrayHash[:phone]=profitLose.phone
                commPaidArrayHash[:txn_date]=profitLose.txn_date
                commPaidArrayHash[:acc_balance]=profitLose.acc_balance
                commPaidArrayHash[:loan]=profitLose.loan
                commPaidArrayHash[:penalty]=profitLose.penalty
                commPaidArrayHash[:total_comm]=profitLose.total_comm
                commPaidArrayHash[:tax_percentage]=profitLose.tax_percentage
                commPaidArrayHash[:total_tax]=profitLose.total_tax
                commPaidArrayHash[:header_id]=profitLose.header_id
                
                commPaidArray << commPaidArrayHash 

            end  
        end

        Log.logger.info("At line = #{__LINE__} and check that the data in the array is pesent or not = #{commPaidArray.present?} ")

        if commPaidArray.present?
	            response.commPaidMonthlyList=(commPaidArray)  
	            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            return response   
        else
	          	response.Response_Code=(Error::NO_TRANSCATIONS)
			    response.Response_Msg=(I18n.t :NO_TRANSCATIONS_MSG)
		    return response 
        end
	end	

	def self.getReportPaidCommisson(loanBalance,penaltyBalance,commBalance)
		calculatedCommission = (commBalance.to_f - (loanBalance.to_f + penaltyBalance.to_f))
		if calculatedCommission > 0
			return calculatedCommission.to_f
		else
			return 0
		end		
	end	

	def self.getHeader(approvedAmount, serviceType, description, agentID, externalRef)
	   
		txnHeader = TxnHeader.new
		    txnHeader.service_type = serviceType
		    txnHeader.sub_type =  serviceType
		    txnHeader.txn_date = Time.now
		    txnHeader.currency_code = Constants::USER_ACCOUNT_CURRENCY
		    txnHeader.amount = TxnHeader.my_money(approvedAmount)
		    txnHeader.agent_id = agentID
		    txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
		    txnHeader.gateway =Constants::TXN_GATEWAY
		    txnHeader.description = description
		    txnHeader.external_reference = externalRef
		    
	    return txnHeader
	end

	def self.getTxnItem(whichUser, amount, isDebit, user_account)
	    usertn = TxnItem.new
	    usertn.user_id = whichUser.id # Agent ID

	    previousBalance = Utility.getAgentAccountBalance(whichUser).to_f

	    if isDebit==false
	      usertn.debit_amount = nil
	      usertn.credit_amount = TxnHeader.my_money(amount)
	      usertn.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)  # agent_user_account balance + amount
	    else
	      usertn.debit_amount = TxnHeader.my_money(amount)
	      usertn.credit_amount = nil
	      usertn.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)  # agent_user_account balance + amount
	    end
	    usertn.account_id = user_account.id
	    usertn.fee_id = nil
	    usertn.fee_amt =  0
	    usertn.previous_balance = TxnHeader.my_money(previousBalance).to_f  # agent_user_account balance
	    return usertn
	end


	def self.getLoanRecord(agent, amount, last_loan_record)
	    loan = Loan.new
	    loan.user_id = agent.id
	    loan.acc_type = Constants::USER_ACCOUNT_TYPE
	    loan.currency = Constants::USER_ACCOUNT_CURRENCY
	    loan.amount = (amount.to_f - (amount.to_f + amount.to_f))
	    loan.balance = TxnHeader.my_money(last_loan_record.balance.to_f - amount.to_f)
	    loan.due_date = last_loan_record.due_date
	    return loan
	end


	def self.getPenaltyRecord(agent, amount, last_penalty_record)
	    agent_penalty = AgentPenaltyDetail.new
	    agent_penalty.user_id = agent.id
	    agent_penalty.loan_amount = last_penalty_record.loan_amount  
	    agent_penalty.penalty_amount = (amount.to_f - (amount.to_f + amount.to_f))
	    agent_penalty.pending_penalty_amount = last_penalty_record.pending_penalty_amount.to_f - amount.to_f
	    return agent_penalty
	end
end	