# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class UgandaElectricityRechargeManager

    def self.checkMeterNumberForRecharge(currentAgent, params)
        billType = params[""+AppConstants::TYPE_PARAM_LABEL+""]
        clientMeterNo = params[""+AppConstants::CLIENT_METER_NO_PARAM_LABEL+""]
        clientMobileNumber = params[""+AppConstants::CLIENT_MOBILE_NO_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} and inside the UgandaElectricityRechargeManager's checkMeterNumberForRecharge method with billType is = #{billType}, clientMeterNo is = #{clientMeterNo} and clientMobileNumber is = #{clientMobileNumber}")
                   
        response = validate(currentAgent, billType, Constants::BLANK_VALUE, Constants::BLANK_VALUE, clientMeterNo, clientMobileNumber, Constants::BLANK_VALUE, Constants::BLANK_VALUE, Constants::BLANK_VALUE, false)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
        AppLog.logger.info("At line #{__LINE__} and this request is do by the agent ID = #{currentAgent.id}, agent Phone = #{currentAgent.phone} and name is = #{currentAgent.name}")
        
        if !clientMobileNumber.start_with? UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s and  !clientMobileNumber.start_with? UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s
            clientMobileNumber = UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s + clientMobileNumber.to_s
        end

        requestData =  QuickTellerUtility.getValidateCustomerReqData(clientMeterNo, clientMobileNumber, QuickTellerUtility.getPaymentCode(billType))
        AppLog.logger.info("At line #{__LINE__} request data send to the Quick Teller on URL = #{Properties::QUICK_TELLER_VALIDATE_URL} is = \n #{requestData}")

        responseData = QuickTellerUtility.sendRequestToQuickTeller(Properties::QUICK_TELLER_VALIDATE_URL, Constants::BLANK_VALUE, requestData, UgandaConstants::HTTP_POST_METHOD, Time.now.to_i)
        AppLog.logger.info("At line #{__LINE__} response come from the Quick Teller side is = \n #{responseData}")

        return QuickTellerUtility.getValidateCustomerResponse(responseData)
    end 

    def self.doRechargeMeterNumber(agent, params, request)
        AppLog.logger.info("At line #{__LINE__} inside the UgandaElectricityRechargeManager's doRechargeMeterNumber method is called")

        billType = params[""+AppConstants::TYPE_PARAM_LABEL+""]
        requestReference = params[""+AppConstants::REQ_REF_PARAM_LABEL+""]
        transactionReference = params[""+AppConstants::TXN_REF_PARAM_LABEL+""]
        rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
        customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
        clientMeterNo = params[""+AppConstants::CLIENT_METER_NO_PARAM_LABEL+""]
        clientMobileNumber = params[""+AppConstants::CLIENT_MOBILE_NO_PARAM_LABEL+""]
        appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__}  billType = #{billType}, requestReference = #{requestReference}, transactionReference = #{transactionReference}, rechargeAmount = #{rechargeAmount}, customerFee = #{customerFee}, clientMeterNo = #{clientMeterNo}, clientMobileNumber = #{clientMobileNumber} and appTxnUniqueID = #{appTxnUniqueID}")

        response = validate(agent, billType, requestReference, transactionReference, clientMeterNo, clientMobileNumber, rechargeAmount, customerFee, appTxnUniqueID, true)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
        AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{agent.id}, agent Phone = #{agent.phone} and name is = #{Utility.getAgentFullName(agent)}")

        calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(Constants::FDI_ELECTRICITY_LABEL, Constants::FDI_ELECTRICITY_LABEL, rechargeAmount)
        if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return calculatedResponse
        end
        
        AppUtility.savePendingRequest(appTxnUniqueID, Constants::FDI_ELECTRICITY_LABEL, Constants::FDI_ELECTRICITY_LABEL, clientMobileNumber, rechargeAmount, agent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
        
        gatewayLogID = GatewayLog.getGatewayLogID
        aggregatorName = AppUtility.getAggregatorName(Constants::FDI_ELECTRICITY_LABEL)
        GatewayLog.saveGatewayLog(gatewayLogID, agent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")

        if Properties::IS_PRODUCTION_SERVER    
            processingResponse = sendMeterRechargeRequest(rechargeAmount, customerFee, requestReference, clientMeterNo, clientMobileNumber, transactionReference, billType, gatewayLogID)
        else 
            processingResponse = getRowData
        end    
        AppLog.logger.info("At line #{__LINE__} and response code after hit to URL is = #{processingResponse.Response_Code}. (if it is 200 then saved into database)") 

        if processingResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
                AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
                AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of the transaction id = #{appTxnUniqueID}")
            return processingResponse 
        else
                requestParams = setRequestParams(rechargeAmount, customerFee, clientMeterNo, clientMobileNumber, requestReference)
            return AppUtility.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
        end   
    end

    def self.setRequestParams(rechargeAmount, customerFee, clientMeterNo, clientMobileNo, requestReference)
        requestParams = RequestParams.new
            requestParams.serviceType=(Constants::FDI_ELECTRICITY_LABEL)
            requestParams.operator=(Constants::FDI_ELECTRICITY_LABEL)
            requestParams.rechargeAmount=(rechargeAmount)
            requestParams.customerFee=(customerFee)
            requestParams.clientMeterNo=(clientMeterNo)
            requestParams.clientMobileNo=(clientMobileNo)
            requestParams.description=(I18n.t :ELECTRICITY_RECH_RQST_DES)
            requestParams.serviceTxnID=(requestReference)
        return requestParams        
    end  

    def self.sendMeterRechargeRequest(rechargeAmount, customerFee, requestReference, clientMeterNo, customerMobile, transactionReference, billType, gatewayLogID)
        response = Response.new

        begin
            AppLog.logger.info("At line #{__LINE__} and inside the UgandaElectricityRechargeManager's sendMeterRechargeRequest method is called")

            if !customerMobile.start_with? UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s and  !customerMobile.start_with? UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s
                customerMobile = UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s + customerMobile.to_s
            end

            requestData = QuickTellerUtility.getRechargeCustomerReqData(rechargeAmount, customerFee, requestReference, clientMeterNo, customerMobile, transactionReference, QuickTellerUtility.getPaymentCode(billType))
            GatewayLog.updateRequestData(gatewayLogID, requestData)
            AppLog.logger.info("At line #{__LINE__} request data send to the Quick Teller on URL = #{Properties::QUICK_TELLER_TRANSACTION_URL} is = \n #{requestData}")

            responseData = QuickTellerUtility.sendRequestToQuickTeller(Properties::QUICK_TELLER_TRANSACTION_URL, Constants::BLANK_VALUE, requestData, UgandaConstants::HTTP_POST_METHOD, Time.now.to_i)
            GatewayLog.updateResponseData(gatewayLogID, responseData)
            AppLog.logger.info("At line #{__LINE__} response come from the Quick Teller side is = \n #{responseData}")
            
            if responseData.present?
                isJSONValid = TxnManager.valid_json?(responseData) 
                AppLog.logger.info("At line #{__LINE__} check that the responseData is come in valid JSON or not = #{isJSONValid}")

                if isJSONValid
                    return getActualResponseData(JSON.parse(responseData))
                else
                        response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                        response.Response_Msg=(responseData) 
                    return response
                end    
            else
                    response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                    response.Response_Msg=(responseData) 
                return response
            end
        rescue => exception
                    AppLog.logger.info("At line #{__LINE__} and exception is occur inside the sendMeterRechargeRequest method = \n#{exception.message}")
                    AppLog.logger.info("At line #{__LINE__} and full exception is = \n#{exception.backtrace.join("\n")}")
                response.Response_Code=(Error::FAILED_TXN)
                response.Response_Msg=(exception.message)  
            return response  
        end
    end

    def self.getActualResponseData(jsonResponseData)
        response = Response.new
        if jsonResponseData["responseCode"].to_i == UgandaConstants::QUICK_TELLER_SUCCESS_RESPONSE_CODE
                response.customerName=("")
                response.electricityToken=(jsonResponseData["rechargePIN"])
                response.electricityUnit=("")
                response.electricityVat=("")
                response.thirdPartyResponseID=(jsonResponseData["transferCode"])
                response.serviceBalance=(QuickTellerUtility.getInterswitchBalance)
                response.Response_Msg=(jsonResponseData["responseMessage"])
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            return response
        else
                response.Response_Code=(jsonResponseData["responseCode"])
                response.Response_Msg=(jsonResponseData["responseMessage"]) 
            return response
        end  
    end

    def self.validate(currentAgent, billType, requestReference, transactionReference, clientMeterNo, clientMobileNo, rechargeAmount, customerFee, appTxnUniqueID, isRechargeMeterNo)
      
        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("Inside the API Request Credits Controller's  requestCredit method at line #{__LINE__} and agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end

            if !billType.present? || !clientMeterNo.present? || !clientMobileNo.present?
                    AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
            end    

            #Now we check that the agent account is suspend or not
            isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
            AppLog.logger.info("At line #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
            if isAccountSuspend
                    response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
                return response
            end  
            
            if isRechargeMeterNo   
                
                if !requestReference.present? || !transactionReference.present? || !rechargeAmount.present? || !customerFee.present? || !appTxnUniqueID.present?
                        AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
                        response.Response_Code=(Error::INVALID_DATA)
                        response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                    return response
                end    

                #Now check that the amount is cannot be less then zero 
                if rechargeAmount.to_f <= 0
                        AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                        response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
                        response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
                    return response
                end

                #Now check that the amount should be divisble by the 10
                if (rechargeAmount.to_f%10) != 0
                        AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                        response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
                        response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
                    return response
                end
                
                #Now check that the unique txn id that is generated by the app can not be exist
                isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
                if isAppUniqueTxnIdExist
                    AppLog.logger.info("At line #{__LINE__} and unique txn id that is generated by the app is exist")
                        response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
                        response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
                    return response
                end

                #Now check that the servcie balance is deceed from threshold or not
                if AppUtility.isServiceBalanceDeceedThreshold(Constants::FDI_ELECTRICITY_LABEL)
                    AppLog.logger.info("At line #{__LINE__} and servcie balance is deceed then the threshold and email is sent to the admins")
                    AppUtility.sendThresholdEmailToAdmin(Constants::FDI_ELECTRICITY_LABEL)
                end 

                isServiceEnable = Utility.checkServiceStatus(Constants::FDI_ELECTRICITY_LABEL)
                if !isServiceEnable
                    AppLog.logger.info("At line #{__LINE__} and service is currently disable in service_setting table")
                        response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
                        response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
                    return response
                end

                agentAccountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
                agentTotalVATPending = TxnHeader.my_money(VatManager.getAgentTotalVAT(currentAgent.id, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)).to_f
                AppLog.logger.info("At line #{__LINE__} account balance of the agent is = #{agentAccountBalance} and pending agent VAT is = #{agentTotalVATPending}")
                if (agentAccountBalance - agentTotalVATPending) <   TxnHeader.my_money(rechargeAmount).to_f +   TxnHeader.my_money(customerFee).to_f
                        AppLog.logger.info("At line #{__LINE__} and agent account balance is below then the rechargeAmount")
                        response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
                        response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
                    return response
                end

            end    

            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("") 
        return response  
    end

    def self.getRowData
        response = Response.new
            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("")
            response.electricityToken=("1234567890")
            response.electricityUnit=("100")
            response.electricityVat=("1.2")
            response.customerName=("TUBEMASO JONAS")
            response.serviceBalance=("")
        return response  
    end  

end 