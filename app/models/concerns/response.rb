# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:TooManyInstanceVariables: { max_instance_variables: 100 }
# :reek:TooManyMethods: { max_methods: 200 }

class Response

	def Response_Code
  		@Response_Code
	end
	def Response_Code=(val)
	  	@Response_Code = val
	end


	def Response_Msg
  		@Response_Msg
	end
	def Response_Msg=(val)
	  	@Response_Msg = val
	end


	def user_account
  		@user_account
	end
	def user_account=(val)
	  	@user_account = val
	end


	def today_user
  		@today_user
	end
	def today_user=(val)
	  	@today_user = val
	end


	def view
  		@view
	end
	def view=(val)
	  	@view = val
	end


	def date
  		@date
	end
	def date=(val)
	  	@date = val
	end


	def amount_profile
  		@amount_profile
	end
	def amount_profile=(val)
	 	 @amount_profile = val
	end


	def loan_profile
		@loan_profile
	end
	def loan_profile=(val)
	  	@loan_profile = val
	end


	def penlty_profile
		@penlty_profile
	end
	def penlty_profile=(val)
	  	@penlty_profile = val
	end


	def totalTurnover
  		@totalTurnover
	end
	def totalTurnover=(val)
	  	@totalTurnover = val
	end


	def totalTurnoverIndServ
  		@totalTurnoverIndServ
	end
	def totalTurnoverIndServ=(val)
	  	@totalTurnoverIndServ = val
	end


	def totalCommission
  		@totalCommission
	end
	def totalCommission=(val)
	  	@totalCommission = val
	end


	def today_commission
  		@today_commission
	end
	def today_commission=(val)
	 	@today_commission = val
	end


	def totalTransactionCount
  		@totalTransactionCount
	end
	def totalTransactionCount=(val)
	  	@totalTransactionCount = val
	end


	def totalAgentTxnsAmount=(val)
	  	@totalAgentTxnsAmount = val
	end
	def totalAgentTxnsAmount
  		@totalAgentTxnsAmount
	end


	def today_transaction_count
  		@today_transaction_count
	end
	def today_transaction_count=(val)
	  	@today_transaction_count = val
	end


	def today_transaction
  		@today_transaction
	end
	def today_transaction=(val)
	  	@today_transaction = val
	end


	def loan_amount
  		@loan_amount
	end
	def loan_amount=(val)
	  	@loan_amount = val
	end


	def penality_amount
  		@penality_amount
	end
	def penality_amount=(val)
	  	@penality_amount = val
	end


	def txn_history
  		@txn_history
	end
	def txn_history=(val)
	  	@txn_history = val
	end


	def txn_history_opt
  		@txn_history_opt
	end
	def txn_history_opt=(val)
	  	@txn_history_opt = val
	end


	def status_flag
  		@status_flag
	end
	def status_flag=(val)
	  	@status_flag = val
	end


	def user
		@user
	end
	def user=(val)
	  	@user = val
	end
	

	def txn_sub_type_history
		@txn_sub_type_history
	end
	def txn_sub_type_history=(val)
	  	@txn_sub_type_history = val
	end


	def txn_history_opt
		@txn_history_opt
	end
	def txn_history_opt=(val)
	 	 @txn_history_opt = val
	end


	def txn_history_total
		@txn_history_total
	end
	def txn_history_total=(val)
	  	@txn_history_total = val
	end


	def tsn_commission
		@tsn_commission
	end
	def tsn_commission=(val)
	  	@tsn_commission = val
	end
	

	def User_Object
  		@user
	end
	def User_Object=(val)
	  	@user = val
	end

	
	def total_airtime_commission=(val)
		@airtime_commission = val
	end	
	def total_airtime_commission
		@airtime_commission
	end	


	def total_electricity_commission=(val)
		@electricity_commission = val
	end	
	def total_electricity_commission
		@electricity_commission
	end	


	def total_internet_commission=(val)
		@internet_commission = val
	end	
	def total_internet_commission
		@internet_commission
	end	


	def total_dth_commission=(val)
		@dth_commission = val
	end	
	def total_dth_commission
		@dth_commission
	end	


	def total_insurance_commission=(val)
		@insurance_commission = val
	end	
	def total_insurance_commission
		@insurance_commission
	end	


	def commission_list=(val)
		@commission_list = val
	end	
	def commission_list
		@commission_list
	end	


	def penalty_list=(val)
		@penalty_list = val
	end	
	def penalty_list
		@penalty_list
	end	


	def profitLoseList=(val)
		@profitLoseList = val
	end	
	def profitLoseList
		@profitLoseList
	end


	def commPaidMonthlyList=(val)
		@commPaidMonthlyList = val
	end	
	def commPaidMonthlyList
		@commPaidMonthlyList
	end
	

	def commissionPDFList=(val)
		@commissionPDFList = val
	end	
	def commissionPDFList
		@commissionPDFList
	end	


	def serviceSoldValueArray=(val)
		@serviceSoldValueArray = val
	end	
	def serviceSoldValueArray
		@serviceSoldValueArray
	end


	def serviceSoldNameArray=(val)
		@serviceSoldNameArray = val
	end	
	def serviceSoldNameArray
		@serviceSoldNameArray
	end


	def totalAirtimePer=(val)
		@totalAirtimePer = val
	end	
	def totalAirtimePer
		@totalAirtimePer
	end


	def totalDTHPer=(val)
		@totalDTHPer = val
	end	
	def totalDTHPer
		@totalDTHPer
	end


	def totalElectricityPer=(val)
		@totalElectricityPer = val
	end	
	def totalElectricityPer
		@totalElectricityPer
	end


	def totalInternetPer=(val)
		@totalInternetPer = val
	end	
	def totalInternetPer
		@totalInternetPer
	end


	def totalWaterBillPer=(val)
		@totalWaterBillPer = val
	end	
	def totalWaterBillPer
		@totalWaterBillPer
	end


	def txnDetails=(val)
		@txnDetails = val
	end	
	def txnDetails
		@txnDetails
	end


	def detailsHash=(val)
		@detailsHash = val
	end	
	def detailsHash
		@detailsHash
	end
	

	def totalHashIndex=(val)
		@totalHashIndex = val
	end	
	def totalHashIndex
		@totalHashIndex
	end
	

	def totalValue=(val)
		@totalValue = val
	end	
	def totalValue
		@totalValue
	end


	def customerName=(val)
		@customerName = val
	end	
	def customerName
		@customerName
	end


	def customerMeterNo=(val)
		@customerMeterNo = val
	end	
	def customerMeterNo
		@customerMeterNo
	end


	def electricityToken=(val)
		@electricityToken = val
	end	
	def electricityToken
		@electricityToken
	end


	def electricityUnit=(val)
		@electricityUnit = val
	end	
	def electricityUnit
		@electricityUnit
	end


	def electricityVat=(val)
		@electricityVat = val
	end	
	def electricityVat
		@electricityVat
	end


	def requestTxnID=(val)
		@requestTxnID = val
	end	
	def requestTxnID
		@requestTxnID
	end


	def registeredNo=(val)
		@registeredNo = val
	end	
	def registeredNo
		@registeredNo
	end


	def clientNo=(val)
		@clientNo = val
	end	
	def clientNo
		@clientNo
	end


	def txnAmount=(val)
		@txnAmount = val
	end	
	def txnAmount
		@txnAmount
	end


	def requestTime=(val)
		@requestTime = val
	end	
	def requestTime
		@requestTime
	end


	def serviceBalance=(val)
		@serviceBalance = val
	end	
	def serviceBalance
		@serviceBalance
	end

	def thirdPartyResponseID=(val)
		@thirdPartyResponseID = val
	end	
	def thirdPartyResponseID
		@thirdPartyResponseID
	end

	
	def serviceResponseCode=(val)
		@serviceResponseCode = val
	end	
	def serviceResponseCode
		@serviceResponseCode
	end


	def serviceResponseMsg=(val)
		@serviceResponseMsg = val
	end	
	def serviceResponseMsg
		@serviceResponseMsg
	end


	def serviceTxnID=(val)
		@serviceTxnID = val
	end	
	def serviceTxnID
		@serviceTxnID
	end


	def array=(val)
		@array = val
	end	
	def array
		@array
	end


	def object=(val)
		@object = val
	end	
	def object
		@object
	end


	def balance=(val)
		@balance = val
	end	
	def balance
		@balance
	end


	def commission=(val)
		@commission = val
	end	
	def commission
		@commission
	end


	def customerRef=(val)
		@customerRef = val
	end	
	def customerRef
		@customerRef
	end


	def customerArea=(val)
		@customerArea = val
	end	
	def customerArea
		@customerArea
	end


	def applicableCharges=(val)
		@applicableCharges = val
	end	
	def applicableCharges
		@applicableCharges
	end


	def applicableChargesType=(val)
		@applicableChargesType = val
	end	
	def applicableChargesType
		@applicableChargesType
	end


	def currencyCode=(val)
		@currencyCode = val
	end	
	def currencyCode
		@currencyCode
	end


	def accountName=(val)
		@accountName = val
	end	
	def accountName
		@accountName
	end


	def accountNo=(val)
		@accountNo = val
	end	
	def accountNo
		@accountNo
	end


	def serviceName=(val)
		@serviceName = val
	end	
	def serviceName
		@serviceName
	end


	def serviceValue=(val)
		@serviceValue = val
	end	
	def serviceValue
		@serviceValue
	end


	def serviceFeeObj=(val)
		@serviceFeeObj = val
	end	
	def serviceFeeObj
		@serviceFeeObj
	end


	def xAxisLabelValue=(val)
		@xAxisLabelValue = val
	end	
	def xAxisLabelValue
		@xAxisLabelValue
	end


	def commPayer=(val)
		@commPayer = val
	end	
	def commPayer
		@commPayer
	end


	def vatType=(val)
		@vatType = val
	end	
	def vatType
		@vatType
	end
	


	def commType=(val)
		@commType = val
	end	
	def commType
		@commType
	end


	def totalComm=(val)
		@totalComm = val
	end	
	def totalComm
		@totalComm
	end

	
	def ourComm=(val)
		@ourComm = val
	end	
	def ourComm
		@ourComm
	end


	def agentComm=(val)
		@agentComm = val
	end	
	def agentComm
		@agentComm
	end


	def aggregatorComm=(val)
		@aggregatorComm = val
	end	
	def aggregatorComm
		@aggregatorComm
	end


	def aggregatorSlabObject=(val)
		@aggregatorSlabObject = val
	end	
	def aggregatorSlabObject
		@aggregatorSlabObject
	end


	def endUserSlabObject=(val)
		@endUserSlabObject = val
	end	
	def endUserSlabObject
		@endUserSlabObject
	end


	def agentDebitAmount=(val)
		@agentDebitAmount = val
	end	
	def agentDebitAmount
		@agentDebitAmount
	end


	def stockOptCreditAmt=(val)
		@stockOptCreditAmt = val
	end	
	def stockOptCreditAmt
		@stockOptCreditAmt
	end


	def commOptCreditAmt=(val)
		@commOptCreditAmt = val
	end	
	def commOptCreditAmt
		@commOptCreditAmt
	end


	def internetToken=(val)
		@internetToken = val
	end	
	def internetToken
		@internetToken
	end


	def qrCodeNo=(val)
		@qrCodeNo = val
	end	
	def qrCodeNo
		@qrCodeNo
	end



	def requestRef=(val)
		@requestRef = val
	end	
	def requestRef
		@requestRef
	end



	def vatPercentage=(val)
		@vatPercentage = val
	end	
	def vatPercentage
		@vatPercentage
	end


	def vatAmount=(val)
		@vatAmount = val
	end	
	def vatAmount
		@vatAmount
	end


	def agentVATAmount=(val)
		@agentVATAmount = val
	end	
	def agentVATAmount
		@agentVATAmount
	end


	def aredVATAmount=(val)
		@aredVATAmount = val
	end	
	def aredVATAmount
		@aredVATAmount
	end


	def aggregatorTotalCommReceive=(val)
		@aggregatorTotalCommReceive = val
	end	
	def aggregatorTotalCommReceive
		@aggregatorTotalCommReceive
	end



	def messageTimeStamp=(val)
		@messageTimeStamp = val
	end	
	def messageTimeStamp
		@messageTimeStamp
	end


	def messageId=(val)
		@messageId = val
	end	
	def messageId
		@messageId
	end


	def originTransactionId=(val)
		@originTransactionId = val
	end	
	def originTransactionId
		@originTransactionId
	end


	def ourTransactionId=(val)
		@ourTransactionId = val
	end	
	def ourTransactionId
		@ourTransactionId
	end


	def agentID=(val)
		@agentID = val
	end	
	def agentID
		@agentID
	end



	def agentPhoneNo=(val)
		@agentPhoneNo = val
	end	
	def agentPhoneNo
		@agentPhoneNo
	end

	


	def loggedInAdmin=(val)
		@loggedInAdmin = val
	end	
	def loggedInAdmin
		@loggedInAdmin
	end



	def isCommRollback=(val)
		@isCommRollback = val
	end	
	def isCommRollback
		@isCommRollback
	end


	def reference=(val)
		@reference = val
	end	
	def reference
		@reference
	end

	

	def operator=(val)
		@operator = val
	end	
	def operator
		@operator
	end


	def subscriber=(val)
		@subscriber = val
	end	
	def subscriber
		@subscriber
	end


	def countryCode=(val)
		@countryCode = val
	end	
	def countryCode
		@countryCode
	end


	def localCurrency=(val)
		@localCurrency = val
	end	
	def localCurrency
		@localCurrency
	end


	def localValue=(val)
		@localValue = val
	end	
	def localValue
		@localValue
	end


	def targetCurrency=(val)
		@targetCurrency = val
	end	
	def targetCurrency
		@targetCurrency
	end


	def targetValue=(val)
		@targetValue = val
	end	
	def targetValue
		@targetValue
	end

	
	def exchangeRate=(val)
		@exchangeRate = val
	end	
	def exchangeRate
		@exchangeRate
	end


	def ResponseIntegrity=(val)
		@ResponseIntegrity = val
	end	
	def ResponseIntegrity
		@ResponseIntegrity
	end




end