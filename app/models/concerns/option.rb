# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 69 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:FeatureEnvy { enabled: false }

class Option

    def view(phone,status)
    	r = Response.new

    	@user = User.find_by_phone(phone) # this method check the phone if exit then give to the variable user
    	
    	Log.logger.info("option Manager's function view is called check it is present=#{@user.present?}")
    	
    	status_flag=0
	    if @user.present?
	    	Log.logger.info("option Manager's function view is called id is = #{@user.id}")
	    	r.user=(@user.id)
	        user_status = @user.status
	       
	       if status=="update"
	        
		            if user_status == 'active' || user_status == 'suspended'
		           		r.status_flag=(1)
		                r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		         	  return r
		               #redirect_to :controller => "users", :action => "edit", :id => @user.id 
		            else
		           		r.Response_Code=(Error::USER_DELETE)
						r.Response_Msg=(I18n.t :USER_DELETE_MSG)
						return r
		              #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is not deleted.' 
		            end
		    
	        elsif status=="agent_update"
	        	user_role = @user.role
	          #Rails.logger.debug("*********USER ROLE************#{user_role}")
		        if user_role == 'agent'
		           
		            if user_status == 'active' || user_status == 'suspended'
		           		r.status_flag=(1)
		                r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		         	  return r
		               #redirect_to :controller => "users", :action => "edit", :id => @user.id 
		            else
		           		r.Response_Code=(Error::USER_DELETE)
						r.Response_Msg=(I18n.t :USER_DELETE_MSG)
						return r
		              #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is not deleted.' 
		            end
		        else
		          	r.Response_Code=(Error::UNAUTHORIZED_SEARCH)
					r.Response_Msg=(I18n.t :UNAUTHORIZED_Modify_SEARCH_MSG)
				return r
		            #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'You do not have to authority to suspend this user.'
		        end   
	        elsif status=="suspend"
	            if user_status == 'active'
		           	r.status_flag=(2)
		            r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		         	return r
	              #redirect_to users_susp_path(:id => @user.id,:status => "suspend")
	            else
	           		r.Response_Code=(Error::USER_SUSPEND)
					r.Response_Msg=(I18n.t :USER_SUSPEND_MSG)
					return r
	              #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is not active.' 
	            end   
	        elsif status=="agent_suspend" 
	          user_role = @user.role
	          #Rails.logger.debug("*********USER ROLE************#{user_role}")
		        if user_role == 'agent'
		            if user_status == 'active'
		             	r.status_flag=(2)
			            r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			          return r
		                #redirect_to users_susp_path(:id => @user.id,:status => "suspend")
		            else
		             	r.Response_Code=(Error::USER_SUSPEND)
						r.Response_Msg=(I18n.t :USER_SUSPEND_MSG)
					  return r
		                #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is not active.' 
		            end   
		        else
		          	r.Response_Code=(Error::UNAUTHORIZED_SEARCH)
					r.Response_Msg=(I18n.t :UNAUTHORIZED_SEARCH_MSG)
				   return r
		            #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'You do not have to authority to suspend this user.'
		        end  
	        elsif status=="agent_delete" 
	          user_role = @user.role
	          #Rails.logger.debug("*********USER ROLE************#{user_role}")
	          if user_role == 'agent'
	            if user_status == 'active' || user_status == 'suspended'
	            	r.status_flag=(3)
		            r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		         	return r
	                #redirect_to users_del_path(:id => @user.id,:status => "delete")
	             else
	             	r.Response_Code=(Error::ALREADY_DELETE)
					r.Response_Msg=(I18n.t :ALREADY_DELETE_MSG)
					return r
	                #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is already deleted.' 
	             end  
	          else
	          	r.Response_Code=(Error::UNAUTHORIZED_SEARCH)
				r.Response_Msg=(I18n.t :UNAUTHORIZED_SEARCH_MSG)
				return r
	            #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'You do not have to authority to delete this user.'
	          end  
	        elsif status=="delete"
	             if user_status == 'active' || user_status == 'suspended'
	             	r.status_flag=(3)
		            r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		         	return r
	                #redirect_to users_del_path(:id => @user.id,:status => "delete")
	             else
	             	r.Response_Code=(Error::ALREADY_DELETE)
					r.Response_Msg=(I18n.t :ALREADY_DELETE_MSG)
					return r
	                #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is already deleted.' 
	             end 
	        elsif status=="agent_profile"
	        	r.status_flag=(4)
		        r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		        return r
	          	#redirect_to users_suspet_path(:id => @user.id)
	          	user_role = "exception"

	          if user_role == 'agent'
	          	r.Response_Code=(Error::USER_SUSPEND)
				r.Response_Msg=(I18n.t :USER_SUSPEND_MSG)
				return r
	            #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'This user is not active.' 
	          elsif user_role == 'exception'
	          else
	          	r.Response_Code=(Error::AGENT_INFO)
				r.Response_Msg=(I18n.t :AGENT_INFO_MSG)
				return r
	           # redirect_to new_search_usr_path(:status=> params[:status]), alert: 'You can see profile only Micro Franchisee.'
	          end 

	        end
	   
	    else
	    	r.Response_Code=(Error::USER_NOT_EXIST)
			r.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG)
			return r
	      #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'Micro Franchisee does not exist'
	    end
	  
	end

	def view_name(name,status)
	  Log.logger.debug("***************Manager Status**********#{status}")
      r = Response.new
      user_name = name
      
      full_name= user_name
    
      length = 0  
      @query = ''  
          
      if full_name.include? ' '
        x = full_name.split(' ')
        length = x.length

          for i in (0..length-1)
            @query = @query.to_s + " LOWER(name) LIKE '%" + x[i].downcase + "%' or "
            @query = @query.to_s + " LOWER(last_name) LIKE '%" + x[i].downcase + "%' or"
          end  
        @query = @query.chomp('or')
      else
        @query = "LOWER(name) LIKE '%" + full_name.downcase + "%' or LOWER(last_name) LIKE '%" + full_name.downcase + "%'"
      end  

        if status == 'agent_suspend' || status=="agent_delete" || status== "agent_profile" || status == "agent_update"
          @user = User.where(@query).where("role = 1 and status = 2 or status = 3")
          #Rails.logger.debug("Response in login api: {user: #{@user.ids}")
          if @user.present?
          	r.user =(@user)
          	r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		    return r
             #render 'search_usr/show', notice: ''
          else
          	r.Response_Code=(Error::AGENT_NOT_EXIST)
			r.Response_Msg=(I18n.t :AGENT_NOT_EXIST_MSG)
			return r
             #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'Micro Franchisee does not exist'
          end
        elsif status=="suspend" || status=="update" || status=="delete"
            @user = User.where(@query).where("status = 2 or status = 3") 
            if @user.present?
              	r.user =(@user)
          		r.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		      return r
                 #render 'search_usr/show', notice: ''
            else
                r.Response_Code=(Error::USER_NOT_EXIST)
			    r.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG)
			 return r
                 #redirect_to new_search_usr_path(:status=> params[:status]), alert: 'User does not exist'
              end
        else
        	r.Response_Code=(Error::USER_NOT_EXIST)
			r.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
		  return r
        end
    end
end