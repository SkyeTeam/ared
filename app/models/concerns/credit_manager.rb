# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class CreditManager

  def self.getActuallAmount(stringAmount)
      if stringAmount.include? Constants::RWANDA_CURRENCY_LABEL
        amount = stringAmount.split(Constants::RWANDA_CURRENCY_LABEL, 2)
        actualAmount = amount[0].strip
      elsif stringAmount.include? UgandaConstants::UGANDA_CURRENCY_LABEL
        amount = stringAmount.split(UgandaConstants::UGANDA_CURRENCY_LABEL, 2)
        actualAmount = amount[0].strip
      else
        actualAmount = stringAmount
      end 
    return actualAmount  
  end  

	def self.approveCredit(currentAgent,requestedAmount, appTxnUniqueID, request)

    AppLog.logger.info("Inside the Credit Manager class's  approve credit method at line #{__LINE__} and requested amount is = #{requestedAmount} and appTxnUniqueID = #{appTxnUniqueID}")
	  
    response = validateAgentCredit(currentAgent,requestedAmount, appTxnUniqueID)

    if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
      return response
    end  
      
    approvedAmount = getCreditApprovedAmount(currentAgent,requestedAmount)
    AppLog.logger.info("At line = #{__LINE__} and approved credit amount is = #{approvedAmount}")

    creditConfig = CreditFeeConfig.getCreditConfig(requestedAmount, currentAgent.id)

    isApprovalReq = CreditFeeConfig.isCreditAutoApproval(creditConfig)
    AppLog.logger.info("At line = #{__LINE__} and check that the approval is require for the amount = #{requestedAmount} or not = #{isApprovalReq}")

    creditFee = CreditFeeConfig.getCreditFee(creditConfig)
    AppLog.logger.info("At line = #{__LINE__} and the credit fee is = #{creditFee} on the credit amount = #{requestedAmount}")

    #First we add one entry in the pending request table before processing
    AppUtility.savePendingRequest(appTxnUniqueID, Constants::USER_LOAN_REQUEST_LABLE, Constants::USER_LOAN_REQUEST_LABLE, nil, requestedAmount, currentAgent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
    AppLog.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")
      
    ActiveRecord::Base.transaction do

      if isApprovalReq

          pendingCredits = getPendingCredits(currentAgent, approvedAmount, appTxnUniqueID)
          pendingCredits.save
          AppLog.logger.info("At line = #{__LINE__} and data saved into the pending Credits table.")

          response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
          response.Response_Msg=(I18n.t :AGENT_CREDIT_APPROVAL_INITIATE_MSG) 
        return response

      else  

          txnheader = getHeader(approvedAmount)
          txnheader.agent_id= currentAgent.id
          txnheader.save
          AppLog.logger.info("At line = #{__LINE__} and data saved into the txn header table with header ID = #{txnheader.id}.")

          agentTxnItem  = getTxnItem(currentAgent, approvedAmount, false, Utility.getCurrentAgentAccount(currentAgent))
          agentTxnItem.header_id = txnheader.id
          agentTxnItem.save
          AppLog.logger.info("At line = #{__LINE__} and agent txn item data saved into the txn_item table.")

          optTxnItem = getTxnItem(Utility.getStockAdmin(), approvedAmount, true, Utility.getStockAdminAccount(Utility.getStockAdmin()))
          optTxnItem.header_id = txnheader.id;
          optTxnItem.save
          AppLog.logger.info("At line = #{__LINE__} and operator's stock txn item data saved into the txn_item table.")

          loanRecord = getLoanRecord(currentAgent, approvedAmount, creditFee)
          loanRecord.save
          AppLog.logger.info("At line = #{__LINE__} and agent loan details is saved into the loan table.")

          TxnManager.update_account(Utility.getStockAdminAccount(Utility.getStockAdmin()), approvedAmount, true);
          TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), approvedAmount, false);
          AppLog.logger.info("At line = #{__LINE__} and balances of the agent and stock admin accounts are updated successfully.")

          AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
          AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{appTxnUniqueID}")

          response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
          response.Response_Msg=(I18n.t :MF_LOAN_CREDIT_MSG, :approvedAmount => Utility.getFormatedAmount(approvedAmount)) 
        return response
      end 

    end
    	
  end


  def self.validateAgentCredit(currentAgent, requestedAmount, appTxnUniqueID)
      
      response = Response.new

      if !requestedAmount.present? || !appTxnUniqueID.present?
          AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
          response.Response_Code=(Error::INVALID_DATA)
          response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
        return response
      end   

      #First Check that the credit of the agent is suspend or active
      isCreditActive = CreditSuspend.isAgentAbleForCredit(currentAgent.id)
      AppLog.logger.info("At line = #{__LINE__} and first check that the agent credit is active or not = #{isCreditActive}")
      if !isCreditActive
          response.Response_Code=(Error::AGENT_CREDIT_SUSPEND)
          response.Response_Msg=(I18n.t :AGENT_CREDIT_SUSPEND_MSG) 
        return response
      end  

      #Now we check that the agent account is suspend or not
      isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
      AppLog.logger.info("At line = #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
      if isAccountSuspend
        response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
       return response
      end  

      #Now check that the agent previous loan balance is nil or not
      isPreviousLoanExist = Utility.isAgentLastLoanPending(currentAgent)
      AppLog.logger.info("At line = #{__LINE__} and check the agent's last loan balance is pending or not = #{isPreviousLoanExist}")
      if isPreviousLoanExist
        response.Response_Code=(Error::LOAN_BALANCE_PENDING)
        response.Response_Msg=(I18n.t :LOAN_BALANCE_PENDING_MSG) 
       return response
      end 

      isPreviousPendingCreditExist = checkPendingCredit(currentAgent.id)
      AppLog.logger.info("At line = #{__LINE__} and check that the previous pending credit is exist or not =  #{isPreviousPendingCreditExist}")
      if isPreviousPendingCreditExist
          response.Response_Code=(Error::PREEVIOUS_PENDING_CREDIT_EXIST)
          response.Response_Msg=(I18n.t :PREEVIOUS_PENDING_CREDIT_EXIST_MSG) 
        return response
      end 

      #Now check that the agent previous penalty balance is nil or not
      isPenaltyExist = Utility.isAgentPenaltyPending(currentAgent)
      AppLog.logger.info("At line = #{__LINE__} and check the agent's penalty balance is pending or not = #{isPenaltyExist}")
      if isPenaltyExist
        response.Response_Code=(Error::PENALTY_BALANCE_PENDING)
        response.Response_Msg=(I18n.t :PENALTY_BALANCE_PENDING_MSG) 
       return response
      end  

      #Now check that the agent's credit maximum limit is reached or not
      isLimitExced = Utility.isAgentMaxLoanLimitReach(currentAgent,requestedAmount)
      AppLog.logger.info("At line = #{__LINE__} and check the agent's credit maximum limit is exceded or not = #{isLimitExced}")
      if isLimitExced
        response.Response_Code=(Error::LOAN_LIMIT_REACHED)
        response.Response_Msg=(I18n.t :LOAN_LIMIT_REACHED_MSG) 
       return response  
      end

      #Now check that the amount is cannot be less then zero 
      if requestedAmount.to_f <= 0
        response.Response_Code=(Error::LOAN_AMOUNT_LESS_THEN_ZERO)
        response.Response_Msg=(I18n.t :LOAN_AMOUNT_LESS_THEN_ZERO_MSG) 
       return response
      end

      #Now check that the unique txn id that is generated by the app can not be exist
      isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
      if isAppUniqueTxnIdExist
          AppLog.logger.info("At line = #{__LINE__} and unique txn id that is generated by the app is exist")
              response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
              response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
          return response
      end

      response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
      response.Response_Msg=("") 
    return response  
  end 


  def self.getCreditApprovedAmount(currentAgent,requestedAmount)
      limitAvailable = (Utility.getAgentMaxLoanCapacity(currentAgent)).to_f - (Utility.getAgentLoanBalance(currentAgent)).to_f
      if requestedAmount.to_f > limitAvailable.to_f
        approvedAmount = limitAvailable
      else
        approvedAmount = requestedAmount
      end
    return  approvedAmount 
  end 


  def self.getHeader(approvedAmount)
    txnHeader = TxnHeader.new
      txnHeader.service_type = Constants::USER_LOAN_REQUEST_LABLE
      txnHeader.sub_type =  Constants::USER_LOAN_REQUEST_LABLE
      txnHeader.txn_date = Time.now
      txnHeader.currency_code = Constants::USER_ACCOUNT_CURRENCY
      txnHeader.amount = approvedAmount
      txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
      txnHeader.gateway =Constants::TXN_GATEWAY
      txnHeader.description = Constants::USER_LOAN_REQUEST_DES
    return txnHeader
  end


  def self.getTxnItem(whichUser, amount, isDebit, user_account)
    
    AppLog.logger.info("At line = #{__LINE__} and inside the Credit manager method getTxnItem is call")
    
    txnItem = TxnItem.new
      
      txnItem.user_id = whichUser.id

      previousBalance = Utility.getAgentAccountBalance(whichUser).to_f

      Log.logger.info("Credit manager's previous balance of the user = #{whichUser.id}  =  #{previousBalance}")

      if isDebit == false
        txnItem.debit_amount = nil
        txnItem.credit_amount = TxnHeader.my_money(amount)
        txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)  # agent_user_account balance + amount
      else
        txnItem.debit_amount = TxnHeader.my_money(amount)
        txnItem.credit_amount = nil
        txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)  # agent_user_account balance + amount
      end
      txnItem.account_id = user_account.id
      txnItem.fee_id = nil
      txnItem.fee_amt =  0
      txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f  

    return txnItem
  end


  def self.getLoanRecord(agent,amount,creditFee)
    AppLog.logger.info("At line = #{__LINE__} and inside the Credit manager's getLoanRecord method with parameters are agent id is = #{agent.id} and amount is = #{amount}") 
    totalAmount = TxnHeader.my_money(amount).to_f + TxnHeader.my_money(creditFee).to_f
    loan = Loan.new
      loan.user_id = agent.id
      loan.acc_type = Constants::USER_ACCOUNT_TYPE
      loan.currency = Constants::USER_ACCOUNT_CURRENCY
      loan.amount = TxnHeader.my_money(totalAmount).to_f
      loan.balance = TxnHeader.my_money(TxnHeader.my_money(Utility.getAgentLoanBalance(agent)).to_f + TxnHeader.my_money(totalAmount).to_f).to_f
      loan.due_date = Utility.getAgentLoanDueDate(agent)
      loan.credit_fee = TxnHeader.my_money(creditFee).to_f
    return loan
  end

  def self.checkPendingCredit(agentID)
      pendingCredits = PendingCredit.where('agent_id = ? and status = ?',agentID,Constants::INITIATE_TXN_CODE)
      if pendingCredits.present?
        isExist = true
      else
        isExist = false  
      end  
    return isExist
  end 

  def self.getPendingCredits(currentAgent, approvedAmount, appTxnUniqueID)
    AppLog.logger.info("At line = #{__LINE__} and inside the Credit manager's getPendingCredits method with parameters are agent id is = #{currentAgent.id} and amount is = #{approvedAmount}") 
    pendingCredits =  PendingCredit.new
      pendingCredits.agent_id = currentAgent.id
      pendingCredits.amount = TxnHeader.my_money(approvedAmount).to_f
      pendingCredits.txn_date = Time.now
      pendingCredits.status = Constants::INITIATE_TXN_CODE
      pendingCredits.app_unique_id = appTxnUniqueID
    return pendingCredits  
  end  

  def self.getAgentCreditHistory(currentAgent)
    AppLog.logger.info("At line #{__LINE__} inside the CreditManager's  and getAgentCreditHistory method is called")
    response = Response.new
    unless currentAgent.present?
        AppLog.logger.info("At line #{__LINE__} and agent is not present")
        response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      return response
    end
    AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

    creditHistory = Loan.where(user_id: currentAgent.id).where("amount != 0 AND non_payment_status = 0").order('created_at DESC').select(:id,:user_id,:acc_type,:currency,:amount,:balance,:due_date,:created_at)
    AppLog.logger.info("At line #{__LINE__} and check that agent has creditHistory or not = #{creditHistory.present?}")
    
    if creditHistory.present?
        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.object=(creditHistory)
      return response
    else  
        response.Response_Code=(Error::NO_RECORD_FOUND)
        response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      return response
    end
  end 

  def self.getAgentCreditStatus(currentAgent)
    AppLog.logger.info("At line #{__LINE__} inside the CreditManager's  and getAgentCreditStatus method is called")
    response = Response.new
    unless currentAgent.present?
        AppLog.logger.info("At line #{__LINE__} and agent is not present")
        response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      return response
    end
    AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

    loanBalance = Utility.getAgentLoanBalance(currentAgent)
    AppLog.logger.info("At line #{__LINE__} and loanBalance of the agent is = #{loanBalance}")

    if loanBalance.to_f > 0
        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=(I18n.t :AGENT_CREDIT_PENDING_MSG, :loanBalance => Utility.getFormatedAmount(loanBalance))
      return response
    else
        response.Response_Code=(Error::NO_RECORD_FOUND)
        response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      return response
    end  
  end  


end