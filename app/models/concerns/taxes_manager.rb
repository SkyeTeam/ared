# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class TaxesManager

	def self.validateBillNumber(currentAgent, params, request)

		clientTaxID = params[""+AppConstants::CLIENT_TAX_ID_PARAM_LABEL+""]
		AppLog.logger.info("At line #{__LINE__} inside the TaxesManager class and validateBillNumber is called with clientTaxID is = #{clientTaxID}")

		response = validate(currentAgent, clientTaxID, nil, nil, nil, nil, nil, false)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
        	return processValidateBillReq(clientTaxID)
        end
	end

	def self.processValidateBillReq(clientTaxID)
		if Properties::IS_PRODUCTION_SERVER == Constants::BOOLEAN_TRUE_LABEL && Properties::IS_SIMULATOR_ENABLE == Constants::BOOLEAN_FALSE_LABEL
			response = Response.new

			requestData = getValidateBillIDXML(clientTaxID)
			
			responseData = sendIremboRequest(Properties::IREMBO_BILL_VALIDATE_URL, requestData, nil)
			AppLog.logger.info("At line #{__LINE__} and response come from the IREMBO is = \n #{responseData}")
			
			statusCode = getResponseStatusCode(responseData)
			AppLog.logger.info("At line #{__LINE__} and status code come from the IREMBO is = #{statusCode}")

			if statusCode.present?
				if  statusCode.eql? Constants::IREMBO_SUCCESS_RESPONSE_CODE
					isExpireID = isExpire(responseData)
					AppLog.logger.info("At line #{__LINE__} and check that the Id is expire or not = #{isExpireID}")
					if isExpireID
							response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	        				response.Response_Msg=(I18n.t :IREMBO_TAX_ID_EXPIRE_MSG) 
	      				return response 
					else	
							response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)	
							response.balance=(Utility.getTagValue(responseData, "ns9:amount"))
							response.currencyCode=(Utility.getTagValue(responseData, "ns9:currencyCode"))
							response.accountName=(Utility.getTagValue(responseData, "ns9:accountName"))
							response.accountNo=(Utility.getTagValue(responseData, "ns9:account"))
							response.serviceName=(Utility.getTagValue(responseData, "ns9:service_name"))
						return response	
					end	
				else
						response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	        			response.Response_Msg=(getResponseStatusDescription(responseData)) 
	      			return response 	
				end	
			else
					response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	        		response.Response_Msg=(responseData) 
	      		return response 
			end	
		else
			return getValidateBillReqRowData
		end	
	end	

	def self.getValidateBillReqRowData
		response = Response.new
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)	
			response.balance=(15000)
			response.currencyCode=("RWF")
			response.accountName=("Jonas")
			response.accountNo=("12344567788")
			response.serviceName=("TAX")
		return response	
	end

	def self.payTax(currentAgent, params, request)
		clientTaxID = params[""+AppConstants::CLIENT_TAX_ID_PARAM_LABEL+""]
		amount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
		customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
		debitAccount = params[""+AppConstants::DEBIT_ACCOUNT_PARAM_LABEL+""]
		currencyCode = params[""+AppConstants::CURRENCY_CODE_PARAM_LABEL+""]
		appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
		AppLog.logger.info("At line #{__LINE__} inside the TaxesManager class and payTax is called with clientTaxID is = #{clientTaxID}, amount = #{amount}, customerFee = #{customerFee}, debitAccount = #{debitAccount}, currencyCode = #{currencyCode} and appTxnUniqueID = #{appTxnUniqueID}")

		response = validate(currentAgent, clientTaxID, amount, customerFee, debitAccount, currencyCode, appTxnUniqueID, true)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
        	AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{currentAgent.id}, agent Phone = #{currentAgent.phone} and name is = #{currentAgent.name}")
        	return sendPayTaxReq(currentAgent, clientTaxID, amount, customerFee, debitAccount, currencyCode, appTxnUniqueID, request)
        end
	end	

	def self.sendPayTaxReq(agent, clientTaxID, rechargeAmount, customerFee, debitAccount, currencyCode, appTxnUniqueID, request)
		calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(Constants::IREMBO_TAX_PAYMENT_DB_LABEL, Constants::IREMBO_TAX_PAYMENT_DB_LABEL, rechargeAmount)
        if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return calculatedResponse
        end

        AppUtility.savePendingRequest(appTxnUniqueID, Constants::IREMBO_TAX_PAYMENT_DB_LABEL, Constants::IREMBO_TAX_PAYMENT_DB_LABEL, nil, rechargeAmount, agent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
       
       	gatewayLogID = GatewayLog.getGatewayLogID
       	aggregatorName = AppUtility.getAggregatorName(Constants::IREMBO_TAX_PAYMENT_DB_LABEL)
       	GatewayLog.saveGatewayLog(gatewayLogID, agent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
       	AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")
  
	  	processingResponse = processPayTaxReq(clientTaxID, debitAccount, currencyCode, gatewayLogID)
      	if processingResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
      			AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
	            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of this transaction id #{appTxnUniqueID}")
	        return processingResponse 
      	else
      		requestParams = setRequestParams(rechargeAmount, customerFee, clientTaxID)
      		return AppUtility.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
      	end
	end	

	def self.processPayTaxReq(clientTaxID, debitAccount, currencyCode, gatewayLogID)
		response = Response.new

		if Properties::IS_PRODUCTION_SERVER == Constants::BOOLEAN_TRUE_LABEL && Properties::IS_SIMULATOR_ENABLE == Constants::BOOLEAN_FALSE_LABEL
			requestData = getPayTaxBillXML(debitAccount, clientTaxID, currencyCode)
			
			responseData = sendIremboRequest(Properties::IREMBO_BILL_PAYMENT_URL, requestData, gatewayLogID)
			AppLog.logger.info("At line #{__LINE__} and Response come from the IREMBO server is = \n #{responseData}")
			GatewayLog.updateResponseData(gatewayLogID, responseData)
		    
		    statusCode = getResponseStatusCode(responseData)
			AppLog.logger.info("At line #{__LINE__} and status code come from the IREMBO is = #{statusCode}")

			if statusCode.present?
				if  statusCode.eql? Constants::IREMBO_SUCCESS_RESPONSE_CODE
						response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)	
						response.Response_Msg=(getResponseStatusDescription(responseData))
						response.serviceBalance=(nil)
					return response	
				else
						response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	        			response.Response_Msg=(getResponseStatusDescription(responseData)) 
	      			return response 	
				end	
			else
					response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	        		response.Response_Msg=(responseData) 
	      		return response 
			end
		else
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)	
				response.Response_Msg=(nil)
				response.serviceBalance=(nil)
			return response	
		end		
	end	

	def self.setRequestParams(rechargeAmount, customerFee, clientTaxID)
        requestParams = RequestParams.new
            requestParams.serviceType=(Constants::IREMBO_TAX_PAYMENT_DB_LABEL)
            requestParams.operator=(Constants::IREMBO_TAX_PAYMENT_DB_LABEL)
            requestParams.rechargeAmount=(rechargeAmount)
            requestParams.customerFee=(customerFee)
            requestParams.serviceTxnID=(clientTaxID)
            requestParams.description=(I18n.t :TAX_REQUEST_DES)
        return requestParams        
    end  

	def self.getResponseStatusCode(responseData)
		if responseData.include? "<ns2:StatusCode>"
			return Utility.getTagValue(responseData, "ns2:StatusCode")
		elsif responseData.include? "<ns2:statusCode>"
			return Utility.getTagValue(responseData, "ns2:statusCode")
		elsif responseData.include? "<ns5:StatusCode>"
			return Utility.getTagValue(responseData, "ns5:StatusCode")
		elsif responseData.include? "<ns5:statusCode>"
			return Utility.getTagValue(responseData, "ns5:statusCode")
		elsif responseData.include? "<ns9:StatusCode>"
			return Utility.getTagValue(responseData, "ns9:StatusCode")
		elsif responseData.include? "<ns9:statusCode>"
			return Utility.getTagValue(responseData, "ns9:statusCode")
		else
			return Constants::BLANK_VALUE
		end	
	end	

	def self.getResponseStatusDescription(responseData)
		if responseData.include? "<ns2:StatusDescription>"
			return getActualDescription(Utility.getTagValue(responseData, "ns2:StatusDescription"))
		elsif responseData.include? "<ns5:StatusDescription>"
			return getActualDescription(Utility.getTagValue(responseData, "ns5:StatusDescription"))
		elsif responseData.include? "<ns9:StatusDescription>"
			return getActualDescription(Utility.getTagValue(responseData, "ns9:StatusDescription"))
		else
			return Constants::BLANK_VALUE
		end	
	end

	def self.getActualDescription(statusDescription)
		if statusDescription.include? "/"
			statusDescriptionArray = statusDescription.split("/",2)
			if statusDescriptionArray[1].present?
				return statusDescriptionArray[1]
			else
				return statusDescriptionArray[0]
			end		
		else
			return statusDescription
		end
	end		

	def self.isExpire(responseData)
		todayDate = Date.today
		expiryDate = Date.parse(Utility.getTagValue(responseData, "ns9:expiryDate").to_s)
		AppLog.logger.info("At line #{__LINE__} todayDate is =  #{todayDate} and expiryDate is = #{expiryDate}")
		if todayDate > expiryDate
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end		
	end

	def self.validate(currentAgent, clientTaxID, amount, customerFee, debitAccount, currencyCode, appTxnUniqueID, isBillPayment)
      	AppLog.logger.info("At line #{__LINE__} inside the TaxesManager and validate method is called")

        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("At line #{__LINE__} and currentAgent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end

            if !clientTaxID.present?
                    AppLog.logger.info("At line #{__LINE__} and clientTaxID is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
        	end   

        	if isBillPayment
        		
        		if !amount.present? || !customerFee.present? || !debitAccount.present? || !currencyCode.present? || !appTxnUniqueID.present?
	                    AppLog.logger.info("At line #{__LINE__} and data is come nil from the app")
	                    response.Response_Code=(Error::INVALID_DATA)
	                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	                return response
        		end  

	        	#Now check that the amount is cannot be less then zero 
	            if amount.to_f <= 0
	                    AppLog.logger.info("At line #{__LINE__} and bill pay amount is may be zero or less then zero")
	                    response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
	                    response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
	                return response
	            end

	            #Now check that the amount should be divisble by the 10
	            if (amount.to_f%10) != 0
	                    AppLog.logger.info("At line #{__LINE__} and bill pay amount is not br divisble by the 10")
	                    response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
	                    response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
	                return response
	            end

	            #Now we check that the agent account is suspend or not
	            isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
	            AppLog.logger.info("At line #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
	            if isAccountSuspend
	                    response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
	                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
	                return response
	            end  

	            isServiceEnable = Utility.checkServiceStatus(Constants::IREMBO_TAX_PAYMENT_DB_LABEL)
	            if !isServiceEnable
	                	AppLog.logger.info("At line #{__LINE__} and service is currently disable in service_setting table")
	                    response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
	                    response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
	                return response
	            end

	            agentGovtAccountBalance = Utility.getAgentGovtAccountBalance(currentAgent)
				AppLog.logger.info("At line #{__LINE__} and agent Govt Account Balance is = #{agentGovtAccountBalance}")
				if agentGovtAccountBalance.to_f <   TxnHeader.my_money(amount).to_f +   TxnHeader.my_money(customerFee).to_f
	                	AppLog.logger.info("At line #{__LINE__} and agent govt account balance is below then the amount")
	                    response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
	                    response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
	                return response
	            end

	            #Now check that the unique txn id that is generated by the app can not be exist
	            isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
	            if isAppUniqueTxnIdExist
	                	AppLog.logger.info("At line #{__LINE__} and unique txn id that is generated by the app is exist")
	                    response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
	                    response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
	                return response
	            end

	        end 

        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
    end

	def self.sendIremboRequest(thirdPartyURL, requestData, gatewayLogID)
		begin
			 	AppLog.logger.info("At line #{__LINE__} sendIremboRequest method is called")

			 	thirdParty = ThirdPartyConfig.getThirdparty(Constants::TAX_PAYMENT_AGGREGATOR)
				iremboUsername = thirdParty.username
		        iremboPassword = thirdParty.password
		        connectionTimeout = thirdParty.connection_timeout

		        if gatewayLogID.present?
		        	GatewayLog.updateRequestData(gatewayLogID, requestData)
		        end	
		        AppLog.logger.info("At line = #{__LINE__} and request send to the IREMBO is = \n#{requestData}")

		        thirdPartyURI = URI.parse(thirdPartyURL)

		        http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
		        http.read_timeout = connectionTimeout
				
				isSecureURL = AppUtility.isSecureURL(thirdPartyURL)
				AppLog.logger.info("At line = #{__LINE__} and check that Is secure URL or not = #{isSecureURL}")			        
		        if isSecureURL
			        http.use_ssl = true
			        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			    end    
		       
		        request = Net::HTTP::Post.new(thirdPartyURI.request_uri)
		        request.content_type = 'text/xml'
		        request.basic_auth iremboUsername, iremboPassword
		        request.body = requestData
		         
		        response = http.request(request)
		        responseData = response.body

	    	return  responseData  
	    
	    rescue => exception  
		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        		responseData = getError(exception.message)
        	return responseData
        end	
	end	

	def self.getValidateBillIDXML(clientTaxID)
		newLine = "\n"
        tab = "\t"
	        requestData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>#{newLine}"
	        requestData << "<ns2:validateBillNumberRequest xmlns:ns2=\"http://rol.rw/BillRequest\">#{newLine}"
	        requestData << "#{tab}<ns2:billNumber>#{clientTaxID}</ns2:billNumber>#{newLine}"
	        requestData << "</ns2:validateBillNumberRequest>"
	    return requestData    
	end	

	def self.getPayTaxBillXML(debitAccount, billRefNumber, debitCurrency)
		newLine = "\n"
        tab = "\t"
	        requestData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>#{newLine}"
	        requestData << "<ns2:BillPaymentRequest xmlns:ns2=\"http://rol.rw/BillRequest\">#{newLine}"
	        requestData << "#{tab}<ns2:debitAccount>#{debitAccount}</ns2:debitAccount>#{newLine}"
	        requestData << "#{tab}<ns2:billRefNumber>#{billRefNumber}</ns2:billRefNumber>#{newLine}"
	        requestData << "#{tab}<ns2:debitCurrency>#{debitCurrency}</ns2:debitCurrency>#{newLine}"
	        requestData << "</ns2:BillPaymentRequest>"
	    return requestData    
	end	

	def self.getError(message)
			errorMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			errorMessage << "<ns2:response xmlns:ns2=\"http://rol.rw/BillRequest\" xmlns:ns5=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns3=\"http://www.fiorano.com/fesb/activity/fault\" xmlns:ns1=\"http://www.fiorano.com/fesb/activity/ISOMessage\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
	  		errorMessage << "<ns2:StatusCode>001</ns2:StatusCode>"
	  		errorMessage << "<ns2:StatusDescription>#{message}</ns2:StatusDescription>"
			errorMessage << "</ns2:response>"
		return errorMessage	
	end
		
end	