# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 37 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class AirtelUtility

	def self.sendRequest(externalRef, customerPhone, amount, gatewayLogID)
		
		begin

		 	AppLog.logger.info("Inside the AirtelUtility class and sendRequest method is called")

			AppLog.logger.info("At line #{__LINE__} and parameters are as externalRef = #{externalRef}, customerPhone = #{customerPhone} and amount = #{amount}")		 	

		 	if !externalRef.present? || !customerPhone.present? || !amount.present?
                	AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
            	return getError(I18n.t :INVALID_DATA_MSG)
 	    	end

 	    	if amount.to_f <= 0
                    AppLog.logger.info("At line = #{__LINE__} and recharge amount is may be zero or less then zero")
                return getError(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG)
            end

            customerPhone = AppUtility.getCustomerPhoneToRecharge(customerPhone)

		 	thirdParty = ThirdPartyConfig.getThirdparty(Constants::AIRTEL_AGGREGATOR)

			airtelUsername = thirdParty.username

	        airtelPin = thirdParty.password

	        connectionTimeout = thirdParty.connection_timeout

			thirdPartyURL = Properties::AIRTEL_RECHARGE_URL
	        
	        requestData = getRequestData(airtelUsername, airtelPin, externalRef, customerPhone, amount)

	        GatewayLog.updateRequestData(gatewayLogID, requestData)
	        
	        AppLog.logger.info("At line = #{__LINE__} and request sent to the airtel is = \n#{requestData}")

	        thirdPartyURI = URI.parse(thirdPartyURL)

	        http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
	        http.use_ssl = true
	        http.read_timeout = connectionTimeout
	        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
	       
	        request = Net::HTTP::Post.new(thirdPartyURI.request_uri)
	        request['content-type'] = 'text/xml'
	        request.body = requestData
	         
	        response = http.request(request)
	        responseData = response.body

	    	return  responseData  
	    
	    rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        		responseData = getError(exception.message)
        	
        	return responseData

        end	

	end	

	def self.getRequestData(airtelUsername, airtelPin, externalRef, customerPhone, amount)
			requestData = "<?xml version=\"1.0\"?>"
	        requestData << "<!DOCTYPE COMMAND PUBLIC \"-//Ocam//DTD XML Command1.0//EN\" \"xml/command.dtd\">"
	        requestData << "<COMMAND>"
	        requestData << "<TYPE>EXRCTRFREQ</TYPE>"
	        requestData << "<DATE></DATE>"
	        requestData << "<EXTNWCODE>RW</EXTNWCODE>"
	        requestData << "<MSISDN>"+airtelUsername.to_s+"</MSISDN>"
	        requestData << "<PIN>"+airtelPin.to_s+"</PIN>"
	        requestData << "<LOGINID></LOGINID>"
	        requestData << "<PASSWORD></PASSWORD>"
	        requestData << "<EXTCODE></EXTCODE>"
	        requestData << "<EXTREFNUM>"+externalRef.to_s+"</EXTREFNUM>"
	        requestData << "<MSISDN2>"+customerPhone.to_s+"</MSISDN2>"
	        requestData << "<AMOUNT>"+amount.to_s+"</AMOUNT>"
	        requestData << "<LANGUAGE1>1</LANGUAGE1>"
	        requestData << "<LANGUAGE2>1</LANGUAGE2>"
	        requestData << "<SELECTOR>1</SELECTOR>"
	        requestData << "</COMMAND>"
        return requestData
	end	

	def self.getError(errorMessage)
			error = "<?xml version=\"1.0\"?>"
			error << "<COMMAND>"
			error << "<TYPE>EXRCTRFRESP</TYPE>"
			error << "<TXNSTATUS>500</TXNSTATUS>"
			error << "<DATE></DATE>"
			error << "<EXTREFNUM></EXTREFNUM>"
			error << "<TXNID></TXNID>"
			error << "<MESSAGE>Msg:500:"+errorMessage.to_s+"</MESSAGE>"
			error << "</COMMAND>"
		return error
	end	

end	