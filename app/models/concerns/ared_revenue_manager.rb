# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 37 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class AredRevenueManager

	def self.getDetails(params)

		Log.logger.info("At line = #{__LINE__} and inside the AredRevenueManager class's getDetails method")

		response = Response.new

		serviceType = params[:serviceTypeRevReport]
		subType = params[:subTypeRevReport]
		viewType = params[:viewTypeRevReport]
		month = params[:monthRevReport]
		year = params[:yearRevReport]

		Log.logger.info("Parameters to fetch the details are service_type = #{serviceType}, subType = #{subType}, viewType = #{viewType}, month = #{month} and year = #{year}")

		fromDate = Utility.getMonthBeginDate(month, year)
		toDate = Utility.getMonthEndDate(month, year)

		Log.logger.info("From date is = #{fromDate} and to date is = #{toDate}")

		txnHeader = fetchDetails(serviceType, subType, fromDate, toDate)
		response = Response.new

		if txnHeader.present?
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.detailsHash=(txnHeader)
		else
			response.Response_Code=(Error::NO_HISTORY)
		   	response.Response_Msg=(I18n.t :NO_HISTORY_MSG) 
		end	

	  return response	 

	end	


	def self.fetchDetails(serviceType, subType, fromDate, toDate)

		if serviceType.eql? Constants::MTOPUP_LABEL and (subType.eql? Constants::AIRTEL_LABEL or subType.eql? Constants::MTN_LABEL or subType.eql? Constants::FDI_TIGO_LABEL)
			
			Log.logger.info("At line = #{__LINE__} inside the if statement of both the serviceType and subType are present")
			
			query = "SELECT sub_type AS service_type, SUM(amount) AS turnover, SUM(opt_commission) AS revenue FROM txn_headers "
            query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
            query << "txn_date <= '#{toDate}' AND  service_type = '#{serviceType}' AND sub_type = '#{subType}' GROUP BY sub_type"

		else
			Log.logger.info("At line = #{__LINE__} inside the else statement of only serviceType is present")

			if serviceType.eql? Constants::ANY_SERVICE_LABEL # If fetch the record of all the services i.e Airtime, electricity and dth

				Log.logger.info("At line = #{__LINE__} inside the if statement of all Services")

				query = "SELECT service_type, SUM(amount) AS turnover, SUM(opt_commission) AS revenue FROM txn_headers "
				query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
				query << "txn_date <= '#{toDate}' AND service_type IN(#{Utility.getSQLQueryServiceTypes}) GROUP BY service_type"

			elsif serviceType.eql? Constants::MTOPUP_LABEL

				query = "SELECT sub_type AS service_type, SUM(amount) AS turnover, SUM(opt_commission) AS revenue FROM txn_headers "
				query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
				query << "txn_date <= '#{toDate}' AND service_type = '#{serviceType}' GROUP BY sub_type"

			else
				Log.logger.info("At line = #{__LINE__} inside the else statement of #{serviceType} Service")

            	query = "SELECT service_type, SUM(amount) AS turnover, SUM(opt_commission) AS revenue FROM txn_headers "
                query << "WHERE is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS} AND txn_date >= '#{fromDate}' AND "
                query << "txn_date <= '#{toDate}' AND  service_type = '#{serviceType}' GROUP BY service_type"
			end	
		end	

		Log.logger.info("At line #{__LINE__} and query is used to fethc the details is = #{query}")

		txnHeader = Utility.executeSQLQuery(query)

	  return txnHeader	

	end	

end