# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 50 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class AppUtility

	def self.getAgentCommOnService(serviceObj, rechargeAmount)
		
			agentCommPct = TxnHeader.my_money(serviceObj.agent_comm_per).to_f
			AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and agent commission percentage value = #{agentCommPct}")
	        
	        agentCommFix = TxnHeader.my_money(serviceObj.agent_comm_fix).to_f
	        AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and agent commission fix value = #{agentCommFix}")

	        agentCalcualtedPct = (rechargeAmount.to_f  *  agentCommPct.to_f) / 100
	        agentCommTotal = agentCalcualtedPct.to_f + agentCommFix.to_f
	        AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total agent commission value = #{agentCommTotal}")

	    return TxnHeader.my_money(agentCommTotal).to_f

	end	

	def self.getAgentCommInRatioOnService(serviceObj, rechargeAmount)
		
			agentCommPct = TxnHeader.my_money(serviceObj.agent_comm_per).to_f
			AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and agent commission percentage value (used in the ratio) = #{agentCommPct}")
	        
	        totalCommPct = TxnHeader.my_money(serviceObj.total_comm_per).to_f
	        AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total commission percentage value (used in the ratio) = #{totalCommPct}")

	        agentCommTotal = (rechargeAmount.to_f  *  agentCommPct.to_f) / totalCommPct.to_f
	        AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total agent commission value = #{agentCommTotal}")

	    return TxnHeader.my_money(agentCommTotal).to_f

	end	

	def self.getOptCommOnService(serviceObj, rechargeAmount)
		
			optCommPct = TxnHeader.my_money(serviceObj.our_comm_per).to_f
			AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and operator commission percentage value = #{optCommPct}")

        	optCommFix = TxnHeader.my_money(serviceObj.our_comm_fix).to_f
        	AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and operator commission fix value = #{optCommFix}")

        	optCalculatedPct = (rechargeAmount.to_f  *  optCommPct.to_f) / 100
        	optCommTotal = optCalculatedPct + optCommFix
        	AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total operator commission value = #{optCommTotal}")

	    return TxnHeader.my_money(optCommTotal).to_f

	end	

	def self.getOptCommInRatioOnService(serviceObj, rechargeAmount)
		
			optCommPct = TxnHeader.my_money(serviceObj.our_comm_per).to_f
			AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and operator commission percentage value (used in the ratio)= #{optCommPct}")

        	totalCommPct = TxnHeader.my_money(serviceObj.total_comm_per).to_f
	        AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total commission percentage value (used in the ratio) = #{totalCommPct}")

        	optCommTotal = (rechargeAmount.to_f  *  optCommPct.to_f) / totalCommPct.to_f
        	AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total operator commission value = #{optCommTotal}")

	    return TxnHeader.my_money(optCommTotal).to_f

	end	

	def self.getTotalCommOnService(serviceObj, rechargeAmount)
		
			totalCommPct = TxnHeader.my_money(serviceObj.total_comm_per).to_f
			AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total applicable commission percentage value = #{totalCommPct}")

        	totalCommFix = TxnHeader.my_money(serviceObj.total_comm_fix).to_f
        	AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and total applicable commission fix value = #{totalCommFix}")

        	totalCalculatedPct = (rechargeAmount.to_f  *  totalCommPct.to_f) / 100
        	commTotal = totalCalculatedPct + totalCommFix
        	AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and applicable commission value = #{commTotal}")

	    return TxnHeader.my_money(commTotal).to_f

	end	

	def self.getExtRefElectricity

			noExpersion = [('0'..'9')].map { |i| i.to_a }.flatten
	        transactionsDigit = (0...9).map { noExpersion[rand(noExpersion.length)] }.join
	        
	        alphaExpersion = [('A'..'Z')].map { |i| i.to_a }.flatten
	        transactionAlpha = (0...3).map { alphaExpersion[rand(alphaExpersion.length)] }.join
	        
	        transactionRefNo = transactionAlpha + "-" +  transactionsDigit
	    return transactionRefNo

	end	

	def self.checkTxnIDExist(appTxnUniqueID)

		pendingRequest = PendingRequest.where('txn_id = ?',appTxnUniqueID).first
		
		if pendingRequest.present?
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end
			
	end	

	def self.savePendingRequest(txnID, serviceType, subType, clientMobileNo, amount, agentID, remoteIP, userID, userRole)
		pendingRequest = PendingRequest.new	

			pendingRequest.txn_id = txnID
			pendingRequest.service_type = serviceType
			pendingRequest.sub_type = subType
			pendingRequest.client_number = clientMobileNo
			pendingRequest.amount = TxnHeader.my_money(amount).to_f
			pendingRequest.agent_id = agentID
			pendingRequest.status = Constants::INITIATE_TXN_CODE
			pendingRequest.txn_date = Time.now
			pendingRequest.remote_ip = remoteIP
			pendingRequest.user_id = userID
			pendingRequest.role = userRole

		pendingRequest.save	
	end

	def self.updatePendingRequestStatus(appTxnUniqueID, status)
		
		pendingRequest = PendingRequest.where('txn_id = ?',appTxnUniqueID).first
		
		if pendingRequest.present?
			pendingRequest.update_attribute(:status, status)
		end	

	end	

	def self.createHeader(serviceType, subType, txnAmount, description, accountID)
	    txnHeader = TxnHeader.new
		    
		    txnHeader.service_type = serviceType
		    txnHeader.sub_type =  subType
		    txnHeader.txn_date = Time.now
		    txnHeader.currency_code = Utility.getCurrencyLabel
		    txnHeader.amount = TxnHeader.my_money(txnAmount).to_f
		    txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
		    txnHeader.gateway = Constants::TXN_GATEWAY
		    txnHeader.description = description
		    txnHeader.account_id = accountID

	    return txnHeader
 	end

	def self.createPenaltyHeader(serviceType, subType, txnAmount, agentID, description, accountID)
	    
	    txnHeader = TxnHeader.new
		
		    txnHeader.service_type = serviceType
		    txnHeader.sub_type =  subType
		    txnHeader.txn_date = Time.now
		    txnHeader.currency_code = Constants::USER_ACCOUNT_CURRENCY
		    txnHeader.amount = TxnHeader.my_money(txnAmount).to_f
		    txnHeader.agent_id = agentID
		    txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
		    txnHeader.gateway = Constants::TXN_GATEWAY
		    txnHeader.description = description
		    txnHeader.account_id = accountID

	   	return txnHeader
  	end

	def self.getTxnItem(headerID, userAccount, amount, feeAmt, role)
	    txnItem = TxnItem.new

		    txnItem.header_id = headerID
		    txnItem.user_id= userAccount.user_id
		    
		    previousBalance = Utility.getAccountBalance(userAccount).to_f
		    
		    txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f
		    txnItem.account_id = userAccount.id # id from the user account table
		    txnItem.fee_amt = feeAmt

		    if role.eql? Constants::PAYER_LABEL
		      txnItem.debit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = Constants::PAYER_LABEL
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f).to_f
		    else
		      txnItem.credit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = role
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f).to_f
		    end

	    txnItem.save
	end


	def self.getTxnItemNew(headerID, userID, accountID, accountType, amount, feeAmt, role)
	    txnItem = TxnItem.new

		    previousBalance = Utility.getUserAccountBalanceWithType(userID, accountType)

		    txnItem.header_id = headerID
		    txnItem.user_id = userID

		    if role.eql? Constants::PAYER_LABEL
		      txnItem.debit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = Constants::PAYER_LABEL
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f).to_f
		    else
		      txnItem.credit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = role
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f).to_f
		    end

		    txnItem.account_id = accountID
		    txnItem.fee_amt = feeAmt
			txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f

	    txnItem.save
	end

	def self.getTxnItemForRollback(headerID, userID, accountID, accountType, amount, feeAmt, role, isInitiate, isGovtAccountAccess)
	    txnItem = TxnItem.new

	    	if isGovtAccountAccess
	    		previousBalance = Utility.getUserGovtAccountBalanceWithID(userID)
	    	else
	    		previousBalance = Utility.getUserAccountBalanceWithType(userID, accountType)
	    	end	

		    txnItem.header_id = headerID
		    txnItem.user_id = userID

		    if role.eql? Constants::PAYER_LABEL
		      txnItem.debit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = Constants::PAYER_LABEL
		      if isInitiate
		      	txnItem.post_balance = TxnHeader.my_money(previousBalance).to_f 
		      else	
		      	txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f).to_f
		      end	
		    else
		      txnItem.credit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = role
		      if isInitiate
		      	txnItem.post_balance = TxnHeader.my_money(previousBalance).to_f
		      else	
		      	txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f).to_f
		      end	
		    end

		    txnItem.account_id = accountID
		    txnItem.fee_amt = feeAmt
			txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f

	    txnItem.save
	end

	def self.getTxnItemGovtAcc(headerID, userGovtAccount, amount, feeAmt, role)
	    txnItem = TxnItem.new

		    txnItem.header_id = headerID
		    txnItem.user_id = userGovtAccount.user_id
		    
		    previousBalance = Utility.getUserGovtAccountBalance(userGovtAccount).to_f
		    
		    txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f
		    txnItem.account_id = userGovtAccount.id # id from the user account table
		    txnItem.fee_amt = feeAmt

		    if role.eql? Constants::PAYER_LABEL
		      txnItem.debit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = Constants::PAYER_LABEL
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f).to_f
		    else
		      txnItem.credit_amount = TxnHeader.my_money(amount).to_f
		      txnItem.role = role
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f).to_f
		    end

	    txnItem.save
	end
	
  	def self.getAgentCommission(serviceType, subType, agent, commissionAmount, headerID)
	    agent_comm = AgentCommission.new

	      	postBalance = Utility.getAgentCommissionBalance(agent)
	      	agent_comm.user_id = agent.id
	      	agent_comm.service_type = serviceType
	      	agent_comm.sub_type = subType
	      	agent_comm.txn_date = Time.now
	      	agent_comm.previous_balance = TxnHeader.my_money(postBalance).to_f
	      	if serviceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL or serviceType.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL or serviceType.eql? Constants::FDI_ELECTRICITY_LABEL
				agent_comm.post_balance = TxnHeader.my_money(postBalance).to_f
				agent_comm.status = Constants::COMM_PAID_STATUS
				agent_comm.payment_ref = headerID
				agent_comm.total_comm = commissionAmount
	      	else	
	      		agent_comm.post_balance = TxnHeader.my_money(TxnHeader.my_money(postBalance).to_f + TxnHeader.my_money(commissionAmount).to_f).to_f
	      	end	
	      	agent_comm.header_id = headerID
	    
	    agent_comm.save

 	end 

 	def self.getAgentCommissionNew(agent, serviceType, subType, commissionAmount, headerID, isCommCredit, vatType)
	    agentComm = AgentCommission.new

	      	postBalance = Utility.getAgentCommissionBalance(agent)

	      	agentComm.user_id = agent.id
	      	agentComm.service_type = serviceType
	      	agentComm.sub_type = subType
	      	agentComm.txn_date = Time.now
	      	agentComm.previous_balance = TxnHeader.my_money(postBalance).to_f

	      	if isCommCredit
	      		agentComm.post_balance = TxnHeader.my_money(TxnHeader.my_money(postBalance).to_f + TxnHeader.my_money(commissionAmount).to_f).to_f
	      	else
	      		agentComm.post_balance = TxnHeader.my_money(postBalance).to_f
				agentComm.status = Constants::COMM_PAID_STATUS
				agentComm.payment_ref = headerID
				agentComm.total_comm = commissionAmount
			end
					
	      	agentComm.header_id = headerID
	    
	    agentComm.save

 	end 


 	def self.getSalesDeatils(headerID, agentID, serviceName, amount, commAmt, description, clientMobileNo)
 		
 		aggregator = getCurrentAggregator(getAggregatorName(serviceName))
 		
 		if aggregator.present?

 			if isCommInclude(aggregator)
 				deductAmount = TxnHeader.my_money(TxnHeader.my_money(amount).to_f - TxnHeader.my_money(commAmt).to_f).to_f
 			else
 				deductAmount = TxnHeader.my_money(amount).to_f
 				commAmt = 0
 			end	

			salesAdmin = Utility.getSalesAdmin
	 		salesAdminAccount =	Utility.getSalesAdminAccount(salesAdmin)
			aggregatorAccount = Utility.getUserAccountWithUserType(aggregator.id, Constants::USER_ACCOUNT_TYPE)

			txnHeader = ApplicationHelper.createHeader(getSalesSerivceType(serviceName), getAggregatorName(serviceName), TxnHeader.my_money(deductAmount).to_f, (I18n.t :AGG_SALES_DESCRIPTION_MSF), clientMobileNo)
          	txnHeader.agent_id = aggregator.id
          	txnHeader.external_reference = headerID
          	txnHeader.param1 = agentID
          	txnHeader.save
          	AppLog.logger.info("At line #{__LINE__} and sales entry is saved successfully in the TxnHeader table")

          	AppUtility.getTxnItemNew(txnHeader.id, aggregator.id, aggregatorAccount.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(deductAmount).to_f, TxnHeader.my_money(commAmt).to_f, Constants::PAYER_LABEL)
	 		AppLog.logger.info("At line #{__LINE__} and Aggregator transaction item is saved in the TxnItem table successfully")
	 
	 		AppUtility.getTxnItemNew(txnHeader.id, salesAdmin.id, salesAdminAccount.id, Constants::SALE_ADMIIN_ACCOUNT_TYPE, TxnHeader.my_money(deductAmount).to_f, TxnHeader.my_money(commAmt).to_f, Constants::SALES_LABEL)
	 		AppLog.logger.info("At line #{__LINE__} and Operator(Sales) transaction item is saved in the TxnItem table successfully")

	 		Utility.updateUserAccountBalanceNew(aggregator.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(deductAmount).to_f, true)
	 		Utility.updateUserAccountBalanceNew(salesAdmin.id, Constants::SALE_ADMIIN_ACCOUNT_TYPE, TxnHeader.my_money(deductAmount).to_f, false)
			AppLog.logger.info("At line #{__LINE__} and successfuly update balance of Aggregator and Operator(Sales) accounts")

	 	end	

 	end

 	def self.getCurrentAggregator(aggregatorName)
 			AppLog.logger.info("At line #{__LINE__} inside the AppUtility class and getCurrentAggregator method is called to get the details from the database of aggregator = #{aggregatorName}")
	 		aggregator = User.where("role = #{Constants::AGGREGATOR_USER_ROLE}").where("LOWER(business_name) = '#{aggregatorName}' OR UPPER(business_name) = '#{aggregatorName}'").first
	 	return aggregator
 	end	

	def self.getAggregatorName(serviceName)
			
			if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1] #For the rwanda
				if serviceName.eql? Constants::FDI_TIGO_LABEL or serviceName.eql? Constants::FDI_DTH_LABEL or serviceName.eql? Constants::FDI_ELECTRICITY_LABEL
		 			aggregatorName = Constants::FDI_AGGREGATOR
		 		elsif serviceName.eql? Constants::INTERNET_LABEL
		 			aggregatorName = Constants::INTERNET_AGGREGATOR		
		 		elsif serviceName.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
		 			aggregatorName = Constants::TAX_PAYMENT_AGGREGATOR
		 		else 
		 			aggregatorName = serviceName
		 		end
		 	elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2] #For the uganda
		 		if serviceName.eql? Constants::FDI_ELECTRICITY_LABEL or serviceName.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL
		 			aggregatorName = UgandaConstants::QUICK_TELLER_AGGREGATOR
		 		else
			 		aggregatorName = UgandaConstants::PAYWAY_AGGREGATOR
			 	end	
		 	end
		 				
	 	return aggregatorName
 	end	


 	def self.isCommInclude(aggregator)
 		if aggregator.is_comm_include == Constants::AGG_COMM_INCLUDE_VALUE
 			return Constants::BOOLEAN_TRUE_LABEL
 		else
 			return Constants::BOOLEAN_FALSE_LABEL	
 		end	
 	end	

 	def self.getSalesSerivceType(serviceName)
		serviceType = Constants::BLANK_VALUE	
			if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1] #For the rwanda
				if serviceName.eql? Constants::FDI_TIGO_LABEL or serviceName.eql? Constants::AIRTEL_LABEL or serviceName.eql? Constants::MTN_LABEL
		 		
		 			serviceType = Constants::MTOP_UP_SALES_LABEL
		 		
		 		elsif serviceName.eql? Constants::FDI_ELECTRICITY_LABEL
		 		
		 			serviceType = Constants::ELECTRICITY_SALE_LABEL
		 		
		 		elsif serviceName.eql? Constants::FDI_DTH_LABEL
		 			
		 			serviceType = Constants::DTH_SALE_LABEL

		 		elsif serviceName.eql? Constants::INTERNET_LABEL
		 			
		 			serviceType = Constants::INTERNET_SALE_LABEL	

		 		elsif serviceName.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
		 			
		 			serviceType = Constants::IREMBO_TAX_PAYMENT_SALE_LABEL		

		 		end
		 	elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2] #For the uganda
		 		if UgandaConstants::UGANDA_AIRTIME_SERVICES_LIST.include? serviceName
		 			
		 			serviceType = Constants::MTOP_UP_SALES_LABEL
		 		
		 		elsif serviceName.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL
		 		
		 			serviceType = UgandaConstants::NATIONAL_WATER_BILL_SALES_LABEL

		 		elsif serviceName.eql? UgandaConstants::ELECTRICITY_LABEL
		 			
		 			serviceType = Constants::ELECTRICITY_SALE_LABEL

		 		elsif serviceName.eql? UgandaConstants::INTERNET_LABEL
		 			
		 			serviceType = Constants::INTERNET_SALE_LABEL		
		 				
		 		end	
		 	end	
	 	return serviceType
 	end	

 	def self.getAggregatorBalance(serviceName)
 		aggregatorName = getAggregatorName(serviceName)
 		AppLog.logger.info("At line #{__LINE__} inside the AppUtility class and getAggregatorBalance is called with serviceName = #{serviceName} and its aggregatorName is = #{aggregatorName}")
 		
 		currentAggregator =	getCurrentAggregator(aggregatorName)

		if currentAggregator.present?
			return Utility.getUserAccountBalanceWithType(currentAggregator.id, Constants::USER_ACCOUNT_TYPE)
		else
			return 0
		end		
 	end

 	def self.isServiceBalanceDeceedThreshold(serviceName)
 		serviceBalance = getAggregatorBalance(serviceName)
 		AppLog.logger.info("At line #{__LINE__} of AppUtility class and Service balance of service = #{serviceName} is = #{serviceBalance}")

 		thresholdamount =  ServiceSetting.getServiceThresholdAmount(serviceName)
 		AppLog.logger.info("At line #{__LINE__} of AppUtility class and thresholdamount of service = #{serviceName} is = #{thresholdamount}")

 		if serviceBalance.to_f <= thresholdamount.to_f
 			return Constants::BOOLEAN_TRUE_LABEL
 		else
 			return Constants::BOOLEAN_FALSE_LABEL
 		end		
 	end	

 	def self.sendThresholdEmailToAdmin(serviceName)
 		response = getThresholdEmails(serviceName)
		service = response.object
		isEmailSend = checkEmailSend(service)

		AppLog.logger.info("At line = #{__LINE__} and check that the email is able to send or not = #{isEmailSend}")

		if isEmailSend
			emailArray = response.array
			AppLog.logger.info("At line #{__LINE__} and Array of the emails is = #{emailArray}")

			for i in 0..emailArray.size.to_i - 1
				AppLog.logger.info("At line #{__LINE__} send email to the = #{emailArray[i]}")
				Utility.sendMsgEmailToAdmin(Properties::SUPERADMIN_EMAIL, emailArray[i], (I18n.t :THRESHOLD_EMAIL_SUBJECT, :currency => Utility.getCurrencyLabel, :thresholdAmount => Utility.getFormatedAmount(service.threshold_amount), :serviceName => serviceName), (I18n.t :THRESHOLD_EMAIL_MESSAGE, :email => emailArray[i], :thresholdAmount => Utility.getFormatedAmount(service.threshold_amount), :serviceName => serviceName, :currency => Utility.getCurrencyLabel))
				AppLog.logger.info("At line #{__LINE__} email has been sent successfully")
			end	

			updateSentNotDate(service)
		end	
 	end	

 	def self.getThresholdEmails(serviceName)
		AppLog.logger.info("At line #{__LINE__} of AppUtility class and getThresholdEmails method is called")
		response = Response.new
 		emailArray = Array.new

	 		service = ServiceSetting.getServiceDetailsWithName(serviceName)
	 		if service.present?
		 		emails = service.emails
		 		AppLog.logger.info("At line #{__LINE__} and list of the emails is = #{emails}")

		 		if emails.present?
			 		if emails.include? ','
			 			emailArray = emails.split(',')
			 		else	
			 			emailArray.push(emails)
			 		end
			 	end	
			end 
			response.array=(emailArray)
			response.object=(service)
		return response
 	end	


 	def self.checkEmailSend(service)
 		notificationSentDate = service.notification_sent_date
 		if notificationSentDate.present?
 			notificationSentDate = notificationSentDate.strftime('%Y-%m-%d')
 		end	

 		if !notificationSentDate.present? || notificationSentDate.to_s != Date.today.to_s
 			return Constants::BOOLEAN_TRUE_LABEL
 		else
 			return Constants::BOOLEAN_FALSE_LABEL
 		end		
 	end	

 	def self.updateSentNotDate(service)
 		service.update_attribute(:notification_sent_date, Time.now)
 		AppLog.logger.info("At line #{__LINE__} and notfication sent time is update successfully")
 	end	

 	def self.getCustomerPhoneToRecharge(customerPhone)
 		if !customerPhone.start_with? Constants::RWANDA_PHONE_CODE_FIRST and !customerPhone.start_with? Constants::RWANDA_PHONE_CODE_SECOND
 			return Constants::RWANDA_PHONE_CODE_SECOND.to_s + customerPhone.to_s
        else
        	return customerPhone
        end	
 	end	

 	def self.updateAggregatorBalance(serviceName, pendingServiceBalance, request)
 		AppLog.logger.info("At line #{__LINE__} and inside the AppUtility class and updateAggregatorBalance method is callefd with serviceName = #{serviceName} and pendingServiceBalance is = #{pendingServiceBalance}")
 		if pendingServiceBalance.present? && pendingServiceBalance != 0
	      	if pendingServiceBalance.to_s.include? ","
	        	pendingServiceBalance = pendingServiceBalance.gsub(/,/, '').to_f
	      	end  
	    end 
 		
 		currentAggregatorBalance = getAggregatorBalance(serviceName)
 		AppLog.logger.info("At line #{__LINE__} and balance in the our system for the service #{serviceName} is = #{currentAggregatorBalance}")
 		
 		if TxnHeader.my_money(currentAggregatorBalance).to_f != TxnHeader.my_money(pendingServiceBalance).to_f
 			depositAmount = TxnHeader.my_money(pendingServiceBalance).to_f - TxnHeader.my_money(currentAggregatorBalance).to_f
 			AppLog.logger.info("At line #{__LINE__} and automatically deposit amount is = #{depositAmount}")
 			if depositAmount.to_f > 0
 				AggregatorManager.aggregatorDepositNew(getAggregatorName(serviceName), depositAmount, nil, nil, request)
 			end	
 		else
 			AppLog.logger.info("At line #{__LINE__} not any amount deposit automatically because both the balances are equal")
 		end	
 	end

 	def self.getUgandaServiceProvider(operator)
 		if operator.eql? UgandaConstants::AIRTEL_LABEL

 			return UgandaConstants::AIRTEL_SERVICE_PROVIDER
 		
 		elsif operator.eql? UgandaConstants::AFRICELL_LABEL
 			
 			return UgandaConstants::ORANGE_SERVICE_PROVIDER

 		elsif operator.eql? UgandaConstants::MTN_LABEL
 			
 			return UgandaConstants::MTN_SERVICE_PROVIDER

 		elsif operator.eql? UgandaConstants::VODAFONE_LABEL

			return UgandaConstants::VODAFONE_SERVICE_PROVIDER

		elsif operator.eql? UgandaConstants::K2_TELECOM_LABEL

			return UgandaConstants::K2_TELECOM_SERVICE_PROVIDER

		elsif operator.eql? UgandaConstants::UGANDA_TELECOM_LABEL

			return UgandaConstants::UGANDA_TELECOM_SERVICE_PROVIDER

 		end	
 	end	

 	def self.isSecureURL(thirdPartyURL)
 		thirdPartyURLArray = thirdPartyURL.split(":", 2)
 		if thirdPartyURLArray[0].eql? Constants::HTTPS_LABEL
 			return Constants::BOOLEAN_TRUE_LABEL
 		else
 			return Constants::BOOLEAN_FALSE_LABEL
 		end		
 	end	

 	def self.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
		AppLog.logger.info("At line #{__LINE__} inside the AppUtility class and saveRequest method is called")

		response = Response.new
			
			serviceType = requestParams.serviceType
			operator = requestParams.operator
			rechargeAmount = requestParams.rechargeAmount
			customerFee = getCustomerFee(requestParams)
			clientMobileNo = requestParams.clientMobileNo
			description = requestParams.description
			externalReference = requestParams.serviceTxnID

			commissionPayer = calculatedResponse.commPayer
			vatType = calculatedResponse.vatType

			if commissionPayer.eql? Constants::BOTH_COMM_PAYER_DB_LABEL
				
				aggregatorParamsObject = calculatedResponse.aggregatorSlabObject
				endUserParamsObject = calculatedResponse.endUserSlabObject
				response = ServiceFeesMaster.calculateCreditDebitParamsForBoth(rechargeAmount, aggregatorParamsObject.totalComm, aggregatorParamsObject.ourComm, aggregatorParamsObject.agentComm, aggregatorParamsObject.aggregatorComm, endUserParamsObject.totalComm, endUserParamsObject.ourComm, endUserParamsObject.agentComm, endUserParamsObject.aggregatorComm, vatType)

				agentCommAmount = TxnHeader.my_money(response.agentComm).to_f
		        optCommAmount = TxnHeader.my_money(response.ourComm).to_f
		        aggregatorCommAmount = TxnHeader.my_money(response.aggregatorComm).to_f
		        totalCommAmount = TxnHeader.my_money(response.totalComm).to_f
		        agentDebitAmount = TxnHeader.my_money(response.agentDebitAmount).to_f
		        stockOptCreditAmt = TxnHeader.my_money(response.stockOptCreditAmt).to_f
		        commOptCreditAmt = TxnHeader.my_money(response.commOptCreditAmt).to_f
		        aredVATAmount = TxnHeader.my_money(response.aredVATAmount).to_f
				agentVATAmount = TxnHeader.my_money(response.agentVATAmount).to_f
				aggregatorTotalCommReceive = TxnHeader.my_money(response.aggregatorTotalCommReceive).to_f

		        aggregatorAgentCommAmount = TxnHeader.my_money(aggregatorParamsObject.agentComm).to_f
		        endUserAgentCommAmount = TxnHeader.my_money(endUserParamsObject.agentComm).to_f 
				
			else

				agentCommAmount = TxnHeader.my_money(calculatedResponse.agentComm).to_f
		        optCommAmount = TxnHeader.my_money(calculatedResponse.ourComm).to_f
		        aggregatorCommAmount = TxnHeader.my_money(calculatedResponse.aggregatorComm).to_f
		        totalCommAmount = TxnHeader.my_money(calculatedResponse.totalComm).to_f
		        agentDebitAmount = TxnHeader.my_money(calculatedResponse.agentDebitAmount).to_f
		        stockOptCreditAmt = TxnHeader.my_money(calculatedResponse.stockOptCreditAmt).to_f
		        commOptCreditAmt = TxnHeader.my_money(calculatedResponse.commOptCreditAmt).to_f
		        aredVATAmount = TxnHeader.my_money(calculatedResponse.aredVATAmount).to_f
				agentVATAmount = TxnHeader.my_money(calculatedResponse.agentVATAmount).to_f
				aggregatorTotalCommReceive = TxnHeader.my_money(calculatedResponse.aggregatorTotalCommReceive).to_f

		    end    
      		
      		responseIDFromThridParty = processingResponse.thirdPartyResponseID
      		pendingServiceBalance = processingResponse.serviceBalance
          	AppLog.logger.info("At line #{__LINE__} and thirdPartyId is = #{responseIDFromThridParty} and  pending balance is = #{pendingServiceBalance}")

			ActiveRecord::Base.transaction do
	       		
	          	txnHeader = ApplicationHelper.createHeader(serviceType, operator, TxnHeader.my_money(rechargeAmount).to_f, description, clientMobileNo)
	          	setTxnHeaderParams(txnHeader, agent, serviceType, agentCommAmount, optCommAmount, aggregatorCommAmount, totalCommAmount, externalReference, processingResponse, requestParams, responseIDFromThridParty, aredVATAmount, agentVATAmount)
	          	txnHeader.customer_fee = TxnHeader.my_money(customerFee).to_f	
	          	txnHeader.save
	          	AppLog.logger.info("At line #{__LINE__} and entry in the txnHeader TABLE successfully for the serviceType = #{serviceType}")

	            setTxnItemsParams(txnHeader, agent, agentDebitAmount, agentCommAmount, stockOptCreditAmt, commOptCreditAmt, optCommAmount, commissionPayer, serviceType, operator, aggregatorAgentCommAmount, endUserAgentCommAmount, vatType)
		        
		        currentAggregator = AppUtility.getCurrentAggregator(aggregatorName)
	            currentAggregatorBalance = Utility.getUserAccountBalance(currentAggregator.id)

		        salesAmount = getActualSalesAmount(serviceType, rechargeAmount, customerFee)
	            AppUtility.getSalesDeatils(txnHeader.id, agent.id, operator, TxnHeader.my_money(salesAmount).to_f, TxnHeader.my_money(totalCommAmount).to_f, description, clientMobileNo)

	            createAggregatorHeaders(txnHeader.id, operator, currentAggregator, aggregatorTotalCommReceive, aggregatorCommAmount, commissionPayer)

	            TxnManager.updateServiceBalance(operator, pendingServiceBalance)
	            AppLog.logger.info("At line #{__LINE__} and balance of the service is updated successfully in the ServiceSetting TABLE")

	            AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
	            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{appTxnUniqueID} in the PendingRequest TABLE")
	           
	            GatewayLog.updatePendingAtt(gatewayLogID, txnHeader.id, TxnHeader.my_money(currentAggregatorBalance).to_f)
	            AppLog.logger.info("At line #{__LINE__} and successfuly update the header id = #{txnHeader.id} and current aggregator balance = #{currentAggregatorBalance} of gatewayLogID = #{gatewayLogID} in the GatewayLog TABLE")
	            
	            AppUtility.updateAggregatorBalance(operator, pendingServiceBalance, request)
	            AppLog.logger.info("At line #{__LINE__} and successfuly matched the balance with aggregator")

	        end

	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=(Constants::TXN_SUCCESS_MSG)

	    return response

	end	

	def self.setTxnHeaderParams(txnHeader, agent, serviceType, agentCommAmount, optCommAmount, aggregatorCommAmount, totalCommAmount, externalReference, processingResponse, requestParams, responseIDFromThridParty, aredVATAmount, agentVATAmount)
			txnHeader.agent_id = agent.id
          	txnHeader.agent_commission = agentCommAmount
        	txnHeader.opt_commission = optCommAmount
        	txnHeader.aggregator_comm = aggregatorCommAmount
        	txnHeader.total_commission = totalCommAmount
        	txnHeader.external_reference = externalReference
        	txnHeader.agent_vat = agentVATAmount
        	txnHeader.ared_vat = aredVATAmount
        	txnHeader.vat_status = Constants::DEDUCT_FROM_AGENT_VAT_STATUS
          	
          	if serviceType.eql? Constants::FDI_ELECTRICITY_LABEL  
	          	
	          	txnHeader.param1 = processingResponse.electricityToken
	          	txnHeader.bill_no = requestParams.clientMeterNo
                txnHeader.unit = processingResponse.electricityUnit
                txnHeader.vat = processingResponse.electricityVat
                txnHeader.electricity_costumer = processingResponse.customerName
                if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
                	txnHeader.third_party_txn_id = processingResponse.thirdPartyResponseID
                end	
          		
          	elsif serviceType.eql? Constants::FDI_DTH_LABEL  
          	
          		txnHeader.param1 = responseIDFromThridParty
	          	txnHeader.bill_no = requestParams.clientSmartCardNo
          	
          	elsif serviceType.eql? Constants::INTERNET_LABEL  

          		txnHeader.param1 = processingResponse.internetToken
                txnHeader.unit = requestParams.internetUsedMB
                txnHeader.duration = TxnHeader.my_money(requestParams.internetDuration).to_f
          	
          	elsif serviceType.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL

          		txnHeader.param1 = responseIDFromThridParty
            	txnHeader.area = requestParams.clientAreaID
            	txnHeader.bill_no = requestParams.clientWaterBillNo

          	else
          	
          		txnHeader.param1 = responseIDFromThridParty
          	
          	end	
		return txnHeader
	end	

	def self.setTxnItemsParams(txnHeader, agent, agentDebitAmount, agentCommAmount, stockOptCreditAmt, commOptCreditAmt, optCommAmount, commissionPayer, serviceType, operator, aggregatorAgentCommAmount, endUserAgentCommAmount, vatType)
		currentAgentAccount = Utility.getCurrentAgentAccount(agent)
		
		stockAdmin = Utility.getStockAdmin()
		stockAdminAccount = Utility.getStockAdminAccount(stockAdmin)

		commissionAdmin = Utility.getCommissionAdmin
		commissionAdminAccount = Utility.getCommissionAdminAccount(commissionAdmin)

		govtBankAdmin = Utility.getGovtBankAdmin()
		govtBankAdminAccount = Utility.getGovtBankAdminAccount(govtBankAdmin)

		if serviceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
            AppUtility.getTxnItemGovtAcc(txnHeader.id, Utility.getUserGovtAccount(agent), TxnHeader.my_money(agentDebitAmount).to_f, TxnHeader.my_money(agentCommAmount).to_f, Constants::PAYER_LABEL)
            AppLog.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

            AppUtility.getTxnItemNew(txnHeader.id, govtBankAdmin.id, govtBankAdminAccount.id, Constants::GOVT_BANK_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(stockOptCreditAmt).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
            AppLog.logger.info("At line #{__LINE__} and Operator(Govt Bank) transaction item is saved in the TxnItem table successfully")
		else	
			AppUtility.getTxnItemNew(txnHeader.id, agent.id, currentAgentAccount.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(agentDebitAmount).to_f, TxnHeader.my_money(agentCommAmount).to_f, Constants::PAYER_LABEL)
	        AppLog.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

	        AppUtility.getTxnItemNew(txnHeader.id, stockAdmin.id, stockAdminAccount.id, Constants::STOCK_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(stockOptCreditAmt).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	        AppLog.logger.info("At line #{__LINE__} and Operator(Stock) transaction item is saved in the TxnItem table successfully")
	    end    

	    AppUtility.getTxnItemNew(txnHeader.id, commissionAdmin.id, commissionAdminAccount.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(commOptCreditAmt).to_f, TxnHeader.my_money(optCommAmount).to_f, Constants::COMM_LABEL)	
        AppLog.logger.info("At line #{__LINE__} and Operator(Commission) transaction item is saved in the TxnItem table successfully")
        
   		if commissionPayer.eql? Constants::BOTH_COMM_PAYER_DB_LABEL
   			AppUtility.getAgentCommissionNew(agent, serviceType, operator, TxnHeader.my_money(aggregatorAgentCommAmount).to_f, txnHeader.id, ServiceFeesMaster.isAgentCommCredit(Constants::AGGREGATOR_COMM_PAYER_DB_LABEL), vatType)
   			AppUtility.getAgentCommissionNew(agent, serviceType, operator, TxnHeader.my_money(endUserAgentCommAmount).to_f, txnHeader.id, ServiceFeesMaster.isAgentCommCredit(Constants::END_USER_COMM_PAYER_DB_LABEL), vatType)
   		else	
       		AppUtility.getAgentCommissionNew(agent, serviceType, operator, TxnHeader.my_money(agentCommAmount).to_f, txnHeader.id, ServiceFeesMaster.isAgentCommCredit(commissionPayer), vatType)
        end	
        AppLog.logger.info("At line #{__LINE__} and agent commission is saved in the agent_commissions table successfully")

        
        if serviceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
        	Utility.updateUserGovtAccountBalanceNew(agent.id, Constants::USER_GOVT_ACCOUNT_TYPE, TxnHeader.my_money(agentDebitAmount).to_f, true)
        	Utility.updateUserAccountBalanceNew(govtBankAdmin.id, Constants::GOVT_BANK_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(stockOptCreditAmt).to_f, false)
        else	
        	Utility.updateUserAccountBalanceNew(agent.id, Constants::USER_ACCOUNT_TYPE, TxnHeader.my_money(agentDebitAmount).to_f, true)
        	Utility.updateUserAccountBalanceNew(stockAdmin.id, Constants::STOCK_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(stockOptCreditAmt).to_f, false)
        end	

        Utility.updateUserAccountBalanceNew(commissionAdmin.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(commOptCreditAmt).to_f, false)
        AppLog.logger.info("At line #{__LINE__} and successfuly update balance of Agent, Operator(Govt Bank) and Operator(Commission) accounts")
	end	


	def self.createAggregatorHeaders(actualTxnID, serviceType, currentAggregator, aggregatorTotalCommReceive, aggregatorCommAmount, commissionPayer)
		isCommRecv = true

		if commissionPayer.eql? Constants::END_USER_COMM_PAYER_DB_LABEL
			if !(getAggregatorName(serviceType).eql? UgandaConstants::QUICK_TELLER_AGGREGATOR)
				isCommRecv = false
			end	
		end	

		if TxnHeader.my_money(aggregatorTotalCommReceive).to_f > 0 && isCommRecv
			createAggCommRecvHeaders(actualTxnID, serviceType, aggregatorTotalCommReceive, currentAggregator)
		end	
		
		if TxnHeader.my_money(aggregatorCommAmount).to_f > 0
			createAggCommPaidHeaders(actualTxnID, serviceType, aggregatorCommAmount, currentAggregator)
		end	

	end	

	def self.createAggCommRecvHeaders(actualTxnID, serviceType, commissonAmount, currentAggregator)

		commReceivableAdmin = Utility.getCommReceivableAdmin
		commReceivableAdminAccount = Utility.getCommReceivableAdminAccount(commReceivableAdmin)

		commReceivableAggregatorAccount = Utility.getUserAccountWithUserType(currentAggregator.id, Constants::AGGREGATOR_COMM_RECV_ACCOUNT_TYPE)

		txnHeader = ApplicationHelper.createHeader(Constants::COMMISSION_RECEIVABLE_SERVICE_TYPE_DB_LABEL, getAggCommSubTypes(serviceType), TxnHeader.my_money(commissonAmount).to_f, (I18n.t :AGG_COMM_RECV_DESCRIPTION_MSF, :aggregatorName => getAggregatorName(serviceType)), Constants::BLANK_VALUE)
		txnHeader.agent_id = currentAggregator.id
		txnHeader.external_reference = actualTxnID
		txnHeader.save
		AppLog.logger.info("At line #{__LINE__} and Aggregator Commission Receive entry is saved in the TxnHeader table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, commReceivableAdmin.id, commReceivableAdminAccount.id, Constants::COMMISSION_RECEIVABLE_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(commissonAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(Commission Receivable Admin) transaction item is saved in the TxnItem table successfully")

	    AppUtility.getTxnItemNew(txnHeader.id, currentAggregator.id, commReceivableAggregatorAccount.id, Constants::AGGREGATOR_COMM_RECV_ACCOUNT_TYPE, TxnHeader.my_money(commissonAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Aggregator transaction item is saved in the TxnItem table successfully")

	    Utility.updateUserAccountBalanceNew(commReceivableAdmin.id, Constants::COMMISSION_RECEIVABLE_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(commissonAmount).to_f, true)
    	Utility.updateUserAccountBalanceNew(currentAggregator.id, Constants::AGGREGATOR_COMM_RECV_ACCOUNT_TYPE, TxnHeader.my_money(commissonAmount).to_f, false)
    	AppLog.logger.info("At line #{__LINE__} and successfuly update balance of  Operator(Commission Receivable Admin) and Aggregator's Commission Receivable accounts")

	end

	def self.createAggCommPaidHeaders(actualTxnID, serviceType, aggregatorCommAmount, currentAggregator)

		commissionAdmin = Utility.getCommissionAdmin
		commissionAdminAccount = Utility.getCommissionAdminAccount(commissionAdmin)

		commPaidAggregatorAccount = Utility.getUserAccountWithUserType(currentAggregator.id, Constants::AGGREGATOR_COMM_PAID_ACCOUNT_TYPE)

		txnHeader = ApplicationHelper.createHeader(Constants::COMMISSION_PAID_SERVICE_TYPE_DB_LABEL, getAggCommSubTypes(serviceType), TxnHeader.my_money(aggregatorCommAmount).to_f, (I18n.t :AGG_COMM_PAID_DESCRIPTION_MSF, :aggregatorName => getAggregatorName(serviceType)), Constants::BLANK_VALUE)
		txnHeader.agent_id = currentAggregator.id
		txnHeader.external_reference = actualTxnID
		txnHeader.save
		AppLog.logger.info("At line #{__LINE__} and Aggregator Commission Receive entry is saved in the TxnHeader table successfully")

		AppUtility.getTxnItemNew(txnHeader.id, commissionAdmin.id, commissionAdminAccount.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(aggregatorCommAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYER_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Operator(Commission Admin) transaction item is saved in the TxnItem table successfully")

	    AppUtility.getTxnItemNew(txnHeader.id, currentAggregator.id, commPaidAggregatorAccount.id, Constants::AGGREGATOR_COMM_PAID_ACCOUNT_TYPE, TxnHeader.my_money(aggregatorCommAmount).to_f, Constants::ZERO_FEES_AMOUNT_VALUE, Constants::PAYEE_LABEL)
	    AppLog.logger.info("At line #{__LINE__} and Aggregator(Commission Paid) transaction item is saved in the TxnItem table successfully")

	    Utility.updateUserAccountBalanceNew(commissionAdmin.id, Constants::COMMISSION_ADMIN_ACCOUNT_TYPE, TxnHeader.my_money(aggregatorCommAmount).to_f, true)
    	Utility.updateUserAccountBalanceNew(currentAggregator.id, Constants::AGGREGATOR_COMM_PAID_ACCOUNT_TYPE, TxnHeader.my_money(aggregatorCommAmount).to_f, false)
    	AppLog.logger.info("At line #{__LINE__} and successfuly update balance of  Operator(Commission Admin) and Aggregator's Commission Paid accounts")
	
	end

	def self.getAggCommSubTypes(serviceType)
		if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
			
			if serviceType.eql? Constants::FDI_TIGO_LABEL or serviceType.eql? Constants::FDI_DTH_LABEL or serviceType.eql? Constants::FDI_ELECTRICITY_LABEL
			
				return Constants::FDI_SUB_TYPE_DB_LABEL
			
			elsif serviceType.eql? Constants::AIRTEL_LABEL
			
				return Constants::AIRTEL_SUB_TYPE_DB_LABEL

			elsif serviceType.eql? Constants::MTN_LABEL

				return Constants::MTN_SUB_TYPE_DB_LABEL

			elsif serviceType.eql? Constants::INTERNET_LABEL	
					
				return Constants::INTERNET_SUB_TYPE_DB_LABEL

			elsif serviceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL

				return Constants::TAX_PAYMENT_SUB_TYPE_DB_LABEL
			
			end	

		elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
			
			if serviceType.eql? UgandaConstants::ELECTRICITY_LABEL or serviceType.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL

				return UgandaConstants::QUICK_TELLER_SUB_TYPE_DB_LABEL

			elsif serviceType.eql? Constants::INTERNET_LABEL	
					
				return Constants::INTERNET_SUB_TYPE_DB_LABEL

			else 

				return UgandaConstants::PAYWAY_SUB_TYPE_DB_LABEL

			end	

		end	
		
	end	

	def self.getArgumentValue(item)
            argumentValue = item.css('value').to_s.gsub("<value>", "").gsub("</value>", "")
        return argumentValue    
    end 

    def self.getCustomerFee(requestParams)
    	if requestParams.customerFee.present?
    		return requestParams.customerFee
    	else
    		return 0
    	end		
    end	

    def self.getActualSalesAmount(serviceType, rechargeAmount, customerFee)
    	if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
    		if serviceType.eql? UgandaConstants::ELECTRICITY_LABEL or serviceType.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL
    			return TxnHeader.my_money(TxnHeader.my_money(rechargeAmount).to_f + TxnHeader.my_money(customerFee).to_f).to_f
    		else
    			return TxnHeader.my_money(rechargeAmount).to_f
    		end	
    	else
    		return TxnHeader.my_money(rechargeAmount).to_f
    	end	
    end	
 		
end	