# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
 
class TaxConfigurationManager

	def self.isCreate(params, loggedInAdmin)
			service = params[:service]
			percentage = params[:percentage]
			effectiveDate = params[:effective_date]
			effectiveTime = params[:effectiveTime]
			Log.logger.info("At line #{__LINE__} inside the TaxConfigurationManager class isCreate method is called with parameters are service = #{service}, percentage = #{percentage}, effectiveDate = #{effectiveDate} and effectiveTime = #{effectiveTime}")
			
			response = validate(service, percentage, effectiveDate, effectiveTime)
	        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	            return response
	        end
		
			ActiveRecord::Base.transaction do
				taxConfiguration = TaxConfiguration.new	
					taxConfiguration.service = service
					taxConfiguration.percentage = percentage
					taxConfiguration.effective_date = Utility.getActualDateTime(effectiveDate, effectiveTime)
				taxConfiguration.save

				ActivityLog.saveActivity((I18n.t :TAX_CONFIG_CREATE_ACTIVITY_MSG), loggedInAdmin.id, nil, params)
			end	

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		    response.Response_Msg=(I18n.t :SUCCESS_TAX_CONFIG_MSG)
		return response		
	end	

	def self.validate(service, percentage, effectiveDate, effectiveTime)
        response = Response.new

        	if !service.present? || !percentage.present? || !effectiveDate.present? || !effectiveTime.present?
	                Log.logger.info("At line #{__LINE__} and some data is come nil from the software")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response
        	end

        	if percentage.to_f > 100
        			Log.logger.info("At line #{__LINE__} and percentage is more then 100 ")
	   				response.Response_Code=(Error::PERCENTAGE_MORE_THEN_HUNDRED)
	    			response.Response_Msg=(I18n.t :PERCENTAGE_MORE_THEN_HUNDRED_MSG) 
    			return response
        	end

            if !Utility.isValidYear(effectiveDate.to_s)
	    			Log.logger.info("At line #{__LINE__} and effectiveDate date has not a valid year ")
	   				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
    			return response
			end

            effectiveDate = Time.parse(Utility.getActualDateTime(effectiveDate, effectiveTime).to_s)
			todayDate = Time.now
            if effectiveDate <= todayDate
	       			response.Response_Code=(Error::APPLICABLE_DATE_IN_PAST)
		    		response.Response_Msg=(I18n.t :APPLICABLE_DATE_IN_PAST_MSG) 
		    	return response
			end
			    
        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
    end

    def self.getApplicableVATPercentage
    	taxConfiguration = TaxConfiguration.where("service = '#{Constants::TAX_LABEL}' AND effective_date <= '#{Time.now}'").order('effective_date ASC').last
    	if taxConfiguration.present?
    		return taxConfiguration.percentage
    	else
    		return 0
    	end		
    end	

end