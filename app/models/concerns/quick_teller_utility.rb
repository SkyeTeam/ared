# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class QuickTellerUtility

	def self.generateInterswitchAuth(clientId, clientSecretKey, quickTellerBaseURL, additionalParameters, httpMethod, timestamp, signatureMethod, terminalID)
		AppLog.logger.info("At line #{__LINE__} inside the QuickTellerUtility class and generateInterswitchAuth method is called")
		interswitchAuth = Hash.new
		
	        clientIdBase64 = ::Base64.strict_encode64("#{clientId}")
	        authorization = "InterswitchAuth" + " " + clientIdBase64.to_s

	        uuid = SecureRandom.uuid
	        nonce = uuid.gsub!("-", "")

	        encodedResourceUrl = URI.encode_www_form_component quickTellerBaseURL
	        signatureCipher = httpMethod.to_s + "&" + encodedResourceUrl.to_s + "&" + timestamp.to_s + "&" + nonce.to_s + "&" + clientId.to_s + "&" + clientSecretKey.to_s
	        if additionalParameters.present?
	          signatureCipher = signatureCipher + "&" + additionalParameters
	        end 

	        salt = SecureRandom.hex
	        signatureBytes = Digest::SHA512.hexdigest(salt + signatureCipher)
	        signature = ::Base64.strict_encode64("#{signatureBytes}")

	        interswitchAuth.merge!("AUTHORIZATION": authorization)
			interswitchAuth.merge!("NONCE": nonce)
			interswitchAuth.merge!("SIGNATURE_METHOD": signatureMethod)
			interswitchAuth.merge!("SIGNATURE": signature)
			interswitchAuth.merge!("TerminalID": terminalID)
		return interswitchAuth
	end	

	def self.sendRequestToQuickTeller(thirdPartyURL, additionalParameters, requestParams, httpMethod, timestamp)
		begin
			AppLog.logger.info("At line #{__LINE__} inside the QuickTellerUtility class and sendRequestToQuickTeller method is called")

			connectionTimeout = 60
	        interswitchAuth = generateInterswitchAuth(UgandaConstants::QUICK_TELLER_CLIENT_ID, UgandaConstants::QUICK_TELLER_CLIENT_SECRET_KEY, thirdPartyURL, additionalParameters, UgandaConstants::HTTP_POST_METHOD, timestamp, UgandaConstants::QUICK_TELLER_SIGNATURE_METHOD, UgandaConstants::QUICK_TELLER_TERMINAL_ID)

	        thirdPartyURI = URI.parse(thirdPartyURL)
	        http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
	        http.use_ssl = true
	        http.read_timeout = connectionTimeout
	        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

	        if httpMethod.eql? UgandaConstants::HTTP_POST_METHOD
	        	request = Net::HTTP::Post.new(thirdPartyURI.path, {'Content-Type' => 'application/json'})
	        else
	        	request = Net::HTTP::Get.new(thirdPartyURI.path, {'Content-Type' => 'application/json'})
	        end		

	        setRequestHeaderAttributes(request, interswitchAuth, timestamp)
	        
	        request.body = requestParams

	        response = http.request(request)
	        return response.body

	    rescue => exception  
		     	AppLog.logger.info("At line #{__LINE__} inside the QuickTellerUtility class and sendRequestToQuickTeller method is called and exception is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} full exception details are = \n #{exception.backtrace.join("\n")}")
        	return getError(exception.message)
        end	
	end	

	def self.setRequestHeaderAttributes(request, interswitchAuth, timestamp)
		request.add_field("SIGNATURE_METHOD", interswitchAuth[:SIGNATURE_METHOD])
        request.add_field("SIGNATURE", interswitchAuth[:SIGNATURE])
        request.add_field("AUTHORIZATION", interswitchAuth[:AUTHORIZATION])
        request.add_field("NONCE", interswitchAuth[:NONCE])
        request.add_field("TIMESTAMP", timestamp)
        request.add_field("TerminalID", interswitchAuth[:TerminalID])
	end	

	def self.getError(errorMessage)
			errorMessage = errorMessage.gsub("\n", ' ').squeeze(' ')
			errorMsg = "{\"errors\":[{\"code\":\"E500\",\"message\":\"#{errorMessage}\"}],\"error\":{\"code\":\"E500\",\"message\":\"#{errorMessage}\"}}"
		return errorMsg
	end	

	def self.getPaymentCode(billType)
		if billType.eql? UgandaConstants::PREPAID_LABEL
			return UgandaConstants::UMEME_PREPAID_PAYMENT_CODE
		else
			return UgandaConstants::UMEME_POSTPAID_PAYMENT_CODE
		end	
	end	

	def self.getValidateCustomerReqData(customerId, customerMobile, paymentCode)

        requestParams = "{\"requestReference\":\"#{UgandaConstants::QUICK_TELLER_RQST_REF_PREFIX.to_s + Utility.getRandomNumber(8)}\","
        requestParams << "\"customerId\":\"#{customerId}\","
        requestParams << "\"bankCbnCode\":\"#{UgandaConstants::QUICK_TELLER_BANK_CBN_CODE}\","
        requestParams << "\"customerMobile\":\"#{customerMobile}\","
        requestParams << "\"terminalId\":\"#{UgandaConstants::QUICK_TELLER_TERMINAL_ID}\","
        requestParams << "\"paymentCode\":\"#{paymentCode}\"}"

        return requestParams
    end 

    def self.getRechargeCustomerReqData(amount, customerFee, requestReference, customerId, customerMobile, transactionReference, paymentCode)
    	amount = TxnHeader.my_money(amount).to_i * 100
    	customerFee = TxnHeader.my_money(customerFee).to_i * 100
        requestParams = "{\"amount\":\"#{amount}\","
        requestParams << "\"terminalId\":\"#{UgandaConstants::QUICK_TELLER_TERMINAL_ID}\","
        requestParams << "\"requestReference\":\"#{requestReference}\","
        requestParams << "\"customerId\":\"#{customerId}\","
        requestParams << "\"bankCbnCode\":\"#{UgandaConstants::QUICK_TELLER_BANK_CBN_CODE}\","
        requestParams << "\"surcharge\":\"#{customerFee}\","
        requestParams << "\"customerMobile\":\"#{customerMobile}\","
        requestParams << "\"transactionRef\":\"#{transactionReference}\","
        requestParams << "\"paymentCode\":\"#{paymentCode}\"}"

        return requestParams
    end 

    def self.getValidateCustomerResponse(responseData)
	    AppLog.logger.info("At line #{__LINE__} inside the QuickTellerUtility class and getValidateCustomerResponse method is called")
	    response = Response.new
	    
	    if responseData.present?
	      	isJSONValid = TxnManager.valid_json?(responseData) 
	      	AppLog.logger.info("At line #{__LINE__} check that the responseData is come in valid JSON or not = #{isJSONValid}")

	      	if isJSONValid
	          	jsonResponseData = JSON.parse(responseData)
	          	error = jsonResponseData["error"]
	          	if error.present?
	              		response.Response_Code=(error["code"])
	              		response.Response_Msg=(error["message"]) 
	            	return response
	          	else
	              		response.requestRef=(jsonResponseData["ShortTransactionRef"])    
	              		response.customerRef=(jsonResponseData["TransactionRef"])
	              		response.customerName=(jsonResponseData["CustomerName"])
	              		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	            	return response
	          	end  
	      	else
	          		response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	          		response.Response_Msg=(responseData) 
	        	return response
	      	end    
	    else
	        	response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	        	response.Response_Msg=(responseData) 
	      	return response
	    end
  	end  

  	def self.getInterswitchBalance
  		AppLog.logger.info("At line #{__LINE__} inside the QuickTellerUtility class and getInterswitchBalance method is called")
        responseData = QuickTellerUtility.sendRequestToQuickTeller(Properties::QUICK_TELLER_BALANCE_URL, Constants::BLANK_VALUE, Constants::BLANK_VALUE, UgandaConstants::HTTP_GET_METHOD, Time.now.to_i)
        if responseData.present?
        	isJSONValid = TxnManager.valid_json?(responseData) 
	      	AppLog.logger.info("At line #{__LINE__} check that the responseData is come in valid JSON or not = #{isJSONValid}")
	      	if isJSONValid
	      			jsonResponseData = JSON.parse(responseData)
	      			AppLog.logger.info("At line #{__LINE__} balance come from the interswitch side is = #{jsonResponseData["responseMessage"]}")
	      		return jsonResponseData["responseMessage"]
	      	else
	      		return UgandaConstants::QUICK_TELLER_DEFAULT_BALANCE
	      	end	
        else
        	return UgandaConstants::QUICK_TELLER_DEFAULT_BALANCE
        end	
  	end	

end	