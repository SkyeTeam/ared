# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class InternetManager
  
    def self.getPerMBRate(agentID)
        serverResponse = sendGetRateRequest(agentID)
        AppLog.logger.info("At line #{__LINE__} and response come from the Kimali Server is = \n#{serverResponse}")

        if serverResponse.present?
            isJSONValid = TxnManager.valid_json?(serverResponse) 
            AppLog.logger.info("At line #{__LINE__} and now check that whether the response is come in a valid JSON or not = #{isJSONValid}")

            if isJSONValid
                responseData = JSON.parse(serverResponse)

                statusCode = responseData['success']
                AppLog.logger.info("At line #{__LINE__} and server status code of the INTERNET is = #{statusCode}")

                if statusCode.to_i == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE
                    return responseData['ratePerMin']    
                else
                    return Constants::DEFAULT_INTERNET_RATE
                end
            else
                return Constants::DEFAULT_INTERNET_RATE  
            end    
        else
            return Constants::DEFAULT_INTERNET_RATE
        end 
    end    

    def self.generateToken(currentAgent, params, request)
        AppLog.logger.info("At line #{__LINE__} and inside the InternetManager class and generateToken method is called")

        internetDuration = params[""+AppConstants::DURATION_PARAM_LABEL+""]
        rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
        customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
        clientMobileNo = params[""+AppConstants::MOBILE_PARAM_LABEL+""]
        appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} and parameters are internetDuration = #{internetDuration}, rechargeAmount = #{rechargeAmount}, customerFee = #{customerFee}, clientMobileNo = #{clientMobileNo} and appTxnUniqueID = #{appTxnUniqueID}")
       
        response = validate(currentAgent, internetDuration, rechargeAmount, customerFee, clientMobileNo, appTxnUniqueID)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
        AppLog.logger.info("At line #{__LINE__} and this request is done by the currentAgent ID = #{currentAgent.id}, currentAgent Phone = #{currentAgent.phone} and name is = #{Utility.getAgentFullName(currentAgent)}")
          
        calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(Constants::INTERNET_LABEL, Constants::INTERNET_LABEL, rechargeAmount)
        if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return calculatedResponse
        end

        AppUtility.savePendingRequest(appTxnUniqueID, Constants::INTERNET_LABEL, Constants::INTERNET_LABEL, clientMobileNo, rechargeAmount, currentAgent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
       
        gatewayLogID = GatewayLog.getGatewayLogID
        aggregatorName = AppUtility.getAggregatorName(Constants::INTERNET_LABEL)
        GatewayLog.saveGatewayLog(gatewayLogID, currentAgent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")
  
        processingResponse = processRequest(currentAgent, internetDuration, clientMobileNo, Constants::AGENT_kIOSK_ID, gatewayLogID)
        AppLog.logger.info("At line #{__LINE__} and response code after hit to URL is  = #{processingResponse.Response_Code}. (if 200 then saved into database)") 
        
        if processingResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
                AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
                AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of this transaction id #{appTxnUniqueID}")
            return processingResponse
        else
            requestParams = setRequestParams(rechargeAmount, customerFee, internetDuration, clientMobileNo)
            return AppUtility.saveRequest(calculatedResponse, currentAgent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
        end
    
    end  

    def self.setRequestParams(rechargeAmount, customerFee, internetDuration, clientMobileNo)
        requestParams = RequestParams.new
            requestParams.serviceType=(Constants::INTERNET_LABEL)
            requestParams.operator=(Constants::INTERNET_LABEL)
            requestParams.rechargeAmount=(rechargeAmount)
            requestParams.customerFee=(customerFee)
            requestParams.internetDuration=(internetDuration)    
            requestParams.internetUsedMB=(getInternetMB(internetDuration.to_f))
            requestParams.clientMobileNo=(clientMobileNo)
            requestParams.description=(I18n.t :INTERNET_REQUEST_DES)
        return requestParams        
    end  

    def self.getInternetMB(duration)
            usedMB = TxnHeader.my_money((duration * 60 ) * Constants::PER_SEC_INTERNET_MB_USED).to_f
            AppLog.logger.info("At line #{__LINE__} and total MB's used in #{duration} minutes are = #{usedMB}")
        return usedMB
    end    

    def self.validate(currentAgent, internetDuration, rechargeAmount, customerFee, clientMobileNo, appTxnUniqueID)
      
        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("At line #{__LINE__} and inside the Internet Manager class's  validate method and agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end

            if !internetDuration.present? || !rechargeAmount.present? || !customerFee.present? || !clientMobileNo.present? || !appTxnUniqueID.present?
                    AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
            end

            #Now check that the amount is cannot be less then zero 
            if rechargeAmount.to_f <= 0
                    AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                    response.Response_Code=(Error::LOAN_AMOUNT_LESS_THEN_ZERO)
                    response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
                return response
            end

            # #Now check that the amount should be divisble by the 10
            # if (rechargeAmount.to_f%10) != 0
            #         AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
            #         response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
            #         response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
            #     return response
            # end

            #Now we check that the agent account is suspend or not
            isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
            AppLog.logger.info("At line #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
            if isAccountSuspend
                    response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
                return response
            end  

            isServiceEnable = Utility.checkServiceStatus(Constants::INTERNET_LABEL)
            if !isServiceEnable
                AppLog.logger.info("At line #{__LINE__} and service is currently disable in service_setting table")
                    response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
                    response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
                return response
            end

            agentAccountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
            agentTotalVATPending = TxnHeader.my_money(VatManager.getAgentTotalVAT(currentAgent.id, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)).to_f
            AppLog.logger.info("At line #{__LINE__} account balance of the agent is = #{agentAccountBalance} and pending agent VAT is = #{agentTotalVATPending}")
            if (agentAccountBalance - agentTotalVATPending) <   TxnHeader.my_money(rechargeAmount).to_f +   TxnHeader.my_money(customerFee).to_f
                    AppLog.logger.info("At line #{__LINE__} and agent account balance is below then the rechargeAmount")
                    response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
                    response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
                return response
            end

            #Now check that the unique txn id that is generated by the app can not be exist
            isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
            if isAppUniqueTxnIdExist
                AppLog.logger.info("At line #{__LINE__} and unique txn id that is generated by the app is exist")
                    response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
                    response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
                return response
            end

            #Now check that the servcie balance is deceed from threshold or not
            if AppUtility.isServiceBalanceDeceedThreshold(Constants::INTERNET_LABEL)
                AppLog.logger.info("At line #{__LINE__} and servcie balance is deceed then the threshold and email is sent to the admins")
                AppUtility.sendThresholdEmailToAdmin(Constants::INTERNET_LABEL)
            end 

        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=("") 
        return response  
    end  

    def self.processRequest(currentAgent, internetDuration, clientMobileNo, kioskID, gatewayLogID)
        AppLog.logger.info("At line #{__LINE__} and inside the Internet Manager class and processRequest method is called")
        response = Response.new

        serverResponse = sendTokenRequest(getRequestData(currentAgent, internetDuration, clientMobileNo, kioskID), gatewayLogID)
        AppLog.logger.info("At line #{__LINE__} and response come from the Server is = \n#{serverResponse}")

        GatewayLog.updateResponseData(gatewayLogID, serverResponse)

        if serverResponse.present?
            isJSONValid = TxnManager.valid_json?(serverResponse) 
            AppLog.logger.info("At line #{__LINE__} and now check that whether the response is come in a valid JSON or not = #{isJSONValid}")

            if isJSONValid
                responseData = JSON.parse(serverResponse)

                statusCode = responseData['success']
                AppLog.logger.info("At line #{__LINE__} and server status code of the INTERNET is = #{statusCode}")

                if statusCode.to_i == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE
                        internetToken = responseData['token']    
                        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                        response.Response_Msg=("")
                        response.internetToken=(internetToken) 
                    return response
                else
                        errorMessage = responseData['errDesc']    
                        response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                        response.Response_Msg=(errorMessage) 
                    return response
                end
            else
                    response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                    response.Response_Msg=(serverResponse) 
                return response  
            end    
        else
                response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                response.Response_Msg=(serverResponse) 
            return response  
        end 
    end  

    def self.getRequestData(currentAgent, internetDuration, clientMobileNo, kioskID)
            if !clientMobileNo.start_with? Constants::RWANDA_PHONE_CODE_FIRST.to_s and  !clientMobileNo.start_with? Constants::RWANDA_PHONE_CODE_SECOND.to_s
                clientMobileNo = Constants::RWANDA_PHONE_CODE_SECOND.to_s + clientMobileNo.to_s
            end 
            requestData = "http://13.84.48.214:8080/splash/token.php?"
            requestData << "duration="+internetDuration.to_s
            requestData << "&cell_no="+clientMobileNo.to_s
            requestData << "&agent_id="+currentAgent.id.to_s
            requestData << "&kiosk_id="+kioskID.to_s
        return requestData    
    end  

    def self.sendGetRateRequest(agentID)
        begin
            connectionTimeout = 30
            thirdPartyURL = "http://13.84.48.214:8080/tokenrate.php?agent_id=#{agentID}"
            thirdPartyURI = URI.parse(thirdPartyURL)
            requestData = Net::HTTP::Post.new(thirdPartyURI)
            requestBody = thirdPartyURI.to_s

            AppLog.logger.info("At line #{__LINE__} and request sent to the Kimali Server for the rate is = \n#{requestBody}")
            
            response = Net::HTTP.start(thirdPartyURI.host, thirdPartyURI.port, :read_timeout => connectionTimeout) {|http| http.request(requestData)}
            responseData = response.body

            return responseData

        rescue => exception  
                AppLog.logger.info("At line #{__LINE__} inside the InternetManager class and method sendGetRateRequestand  with exception is = \n #{exception.message}")
                AppLog.logger.info("At line #{__LINE__} and full exception Details are = \n #{exception.backtrace.join("\n")}")
                responseData = getError(exception.message)
            return responseData
        end 
    end

    def self.sendTokenRequest(thirdPartyURL, gatewayLogID)
        begin
            connectionTimeout = 30

            thirdPartyURI = URI.parse(thirdPartyURL)

            requestData = Net::HTTP::Post.new(thirdPartyURI)
            
            requestBody = thirdPartyURI.to_s
            
            if gatewayLogID.present?
                GatewayLog.updateRequestData(gatewayLogID, requestBody)
            end 

            AppLog.logger.info("At line #{__LINE__} and request sent to the Server for the Token generation is = \n#{requestBody}")
            
            # Timeout in seconds
            response = Net::HTTP.start(thirdPartyURI.host, thirdPartyURI.port, :read_timeout => connectionTimeout) {|http| http.request(requestData)}
            responseData = response.body

            return responseData

        rescue => exception  
                AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
                AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
            
                responseData = getError(exception.message)
            return responseData
        end 
    end

    def self.getError(errorMessage)
            errorMessage = errorMessage.gsub("\n", ' ').squeeze(' ')
            errorMsg = "{\"success\":0,\"errId\":500,\"errDesc\":\""+errorMessage+"\",\"token\":\"\"}"
        return errorMsg
    end    

end