# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class FdiUtility

	def self.checkFDIServerStatus(pdtType, timeStamp)

			AppLog.logger.info("Inside the FdiUtility class and function checkFDIServerStatus is called with params are as pdtType = #{pdtType} and timeStamp = #{timeStamp}")
	       
	        requestParams = {'pdt' => pdtType, 'ep' => 'link', 'tstamp' => timeStamp}
	      	 
	        responseData = sendRequestToFDI(Properties::FDI_SERVER_CHECK_URL, requestParams, nil)
        	
        return responseData
	
	end	


	def self.sendMobileRequest(customerPhone, amount, externalRef, timeStamp, gatewayLogID)

		begin
			
			AppLog.logger.info("Inside the FdiUtility class and function sendMobileRequest is called and first we check the status of the FDI server")

			fdiServerResponse = checkFDIServerStatus("tigo", timeStamp)

			AppLog.logger.info("At line = #{__LINE__} and response come from the FDI side of server check is = \n#{fdiServerResponse}")

			if fdiServerResponse.present?
				
				responseData = JSON.parse(fdiServerResponse)

				statusCode = responseData['status_code']

				AppLog.logger.info("Inside the FdiUtility class and function sendMobileRequest at line = #{__LINE__} and server status code of the FDI is = #{statusCode}. If status code is 1 then the link is up if it is 0 then it is down no further processing")

				if statusCode == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE

						if !customerPhone.present? || !amount.present? || !externalRef.present? || !timeStamp.present?
			                	AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
			            	return getError(I18n.t :INVALID_DATA_MSG)
			 	    	end

			 	    	if amount.to_f <= 0
			                    AppLog.logger.info("At line = #{__LINE__} and recharge amount is may be zero or less then zero")
			                return getError(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG)
			            end

			            customerPhone = AppUtility.getCustomerPhoneToRecharge(customerPhone)
					
		            	requestParams = {'pdt' => 'airtime', 'ep' => 'vend', 'customer_msisdn' => customerPhone, 'amount' => amount, 'trxref' => externalRef, 'tstamp' => timeStamp}          
		      
			        	responseData = sendRequestToFDI(Properties::FDI_TXNS_URL, requestParams, gatewayLogID)

			        return responseData

				else

					return fdiServerResponse

				end

			else
				getError(fdiServerResponse)
			end	

		rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        	return getError(exception.message)

        end		

	end

	def self.sendCheckMeterNoRequest(customerMeterNo, timeStamp)

		begin
			if Properties::IS_PRODUCTION_SERVER && !Properties::IS_SIMULATOR_ENABLE
			
				AppLog.logger.info("Inside the FdiUtility class and function sendCheckMeterNoRequest is called and first we check the status of the FDI server")

				fdiServerResponse = checkFDIServerStatus("electricity", timeStamp)

				AppLog.logger.info("At line = #{__LINE__} and response come from the FDI side of server check is = \n#{fdiServerResponse}")

				if fdiServerResponse.present?
					
					responseData = JSON.parse(fdiServerResponse)

					statusCode = responseData['status_code']

					AppLog.logger.info("Inside the FdiUtility class and function sendCheckMeterNoRequest at line = #{__LINE__} and server status code of the FDI is = #{statusCode}. If status code is 1 then the link is up if it is 0 then it is down no further processing")

					if statusCode == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE

							if !customerMeterNo.present?
				                	AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
				            	return getError(I18n.t :INVALID_DATA_MSG)
				 	    	end

			            	requestParams = {'pdt' => 'electricity', 'ep' => 'query', 'meterno' => customerMeterNo, 'tstamp' => timeStamp}          
			      
				        	responseData = sendRequestToFDI(Properties::FDI_TXNS_URL, requestParams, nil)

				        return responseData

					else

						return fdiServerResponse

					end

				else
					getError(fdiServerResponse)
				end	

			else
				return getRowResponse
			end	

		rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        	return getError(exception.message)

        end		

	end

	
	def self.sendRechargeMeterNoRequest(customerMeterNo, amount, externalRef, customerPhone, timeStamp, gatewayLogID)

		begin
			
			AppLog.logger.info("Inside the FdiUtility class and function sendRechargeMeterNoRequest is called and first we check the status of the FDI server")

			fdiServerResponse = checkFDIServerStatus("electricity", timeStamp)

			AppLog.logger.info("At line = #{__LINE__} and response come from the FDI side of server check is = \n#{fdiServerResponse}")

			if fdiServerResponse.present?
			
				responseData = JSON.parse(fdiServerResponse)

				statusCode = responseData['status_code']

				AppLog.logger.info("Inside the FdiUtility class and function sendRechargeMeterNoRequest at line = #{__LINE__} and server status code of the FDI is = #{statusCode}. If status code is 1 then the link is up if it is 0 then it is down no further processing")

				if statusCode == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE

						if !customerMeterNo.present? || !amount.present? || !externalRef.present? || !customerPhone.present? || !timeStamp.present?
			                	AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
			            	return getError(I18n.t :INVALID_DATA_MSG)
			 	    	end

			 	    	if amount.to_f <= 0
			                    AppLog.logger.info("At line = #{__LINE__} and recharge amount is may be zero or less then zero")
			                return getError(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG)
			            end

			            customerPhone = AppUtility.getCustomerPhoneToRecharge(customerPhone)

		            	requestParams = {'pdt' => 'electricity', 'ep' => 'vend', 'meterno' => customerMeterNo, 'amount' => amount, 'trxref' => externalRef, 'customer_msisdn' => customerPhone, 'tstamp' => timeStamp}          
		      
			        	responseData = sendRequestToFDI(Properties::FDI_TXNS_URL, requestParams, gatewayLogID)

			        return responseData

				else

					return fdiServerResponse

				end

			else
				getError(fdiServerResponse)
			end	

		rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        	return getError(exception.message)

        end		

	end


	def self.sendRechargeStarTimesRequest(customerSmartCardNo, amount, externalRef, customerPhone, timeStamp, gatewayLogID)

		begin
			
			AppLog.logger.info("Inside the FdiUtility class and function sendRechargeStarTimesRequest is called and first we check the status of the FDI server")

			fdiServerResponse = checkFDIServerStatus("startimes_tv", timeStamp)

			AppLog.logger.info("At line = #{__LINE__} and response come from the FDI side of server check is = \n#{fdiServerResponse}")

			if fdiServerResponse.present?
			
				responseData = JSON.parse(fdiServerResponse)

				statusCode = responseData['status_code']

				AppLog.logger.info("Inside the FdiUtility class and function sendRechargeStarTimesRequest at line = #{__LINE__} and server status code of the FDI is = #{statusCode}. If status code is 1 then the link is up if it is 0 then it is down no further processing")

				if statusCode == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE

						if !customerSmartCardNo.present? || !amount.present? || !externalRef.present? || !customerPhone.present? || !timeStamp.present?
			                	AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
			            	return getError(I18n.t :INVALID_DATA_MSG)
			 	    	end

			 	    	if amount.to_f <= 0
			                    AppLog.logger.info("At line = #{__LINE__} and recharge amount is may be zero or less then zero")
			                return getError(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG)
			            end

			            customerPhone = AppUtility.getCustomerPhoneToRecharge(customerPhone)

		            	requestParams = {'pdt' => 'startimes_tv', 'ep' => 'vend', 'customer_msisdn' => customerPhone, 'amount' => amount, 'smartcard' => customerSmartCardNo, 'trxref' => externalRef,  'tstamp' => timeStamp}
		      
			        	responseData = sendRequestToFDI(Properties::FDI_TXNS_URL, requestParams, gatewayLogID)

			        return responseData

				else

					return fdiServerResponse

				end

			else
				getError(fdiServerResponse)
			end	

		rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        	return getError(exception.message)

        end		

	end


	def self.sendRequestToFDI(thirdPartyURL, requestParams, gatewayLogID)

		begin

			thirdParty = ThirdPartyConfig.getThirdparty(Constants::FDI_AGGREGATOR)

			fdiUsername = thirdParty.username

	        fdiPin = thirdParty.password

	        connectionTimeout = thirdParty.connection_timeout

	        thirdPartyURI = URI.parse(thirdPartyURL)

			requestData = Net::HTTP::Post.new(thirdPartyURI.path)
	        
	        requestData.set_form_data(requestParams)
	        requestData["username"] = fdiUsername
	        requestData["pin"] = fdiPin

	        requestBody = thirdPartyURI.to_s + "?" + requestData.body.to_s
	        
	        if gatewayLogID.present?
	        	GatewayLog.updateRequestData(gatewayLogID, requestBody)
	        end	

	        AppLog.logger.info("At line = #{__LINE__} and request sent to the FDI is = \n#{requestBody}")
	        
	        # Timeout in seconds
	        response = Net::HTTP.start(thirdPartyURI.host, thirdPartyURI.port, :use_ssl => true, :verify_mode => OpenSSL::SSL::VERIFY_NONE, :read_timeout => connectionTimeout) {|http| http.request(requestData)}
	        responseData = response.body

	        return responseData

	    rescue => exception  

		     	AppLog.logger.info("At line #{__LINE__} and EXCEPTION OCCUR is = \n #{exception.message}")
    	      	AppLog.logger.info("At line #{__LINE__} and FULL EXCEPTION Details are = \n #{exception.backtrace.join("\n")}")
        	
        		responseData = getError(exception.message)
        	
        	return responseData

        end	

	end	


	def self.getError(errorMessage)
			errorMessage = errorMessage.gsub("\n", ' ').squeeze(' ')
			errorMsg = "{\"status_code\":0,\"status_msg\":\"#{errorMessage}\",\"data\":{}}"
		return errorMsg
	end	

	def self.getRowResponse
		return "{\"status_code\":1,\"status_msg\":\"STUDIO PHOTO AMAZONE\",\"data\":{\"vend_min_amount\":100,\"vend_max_amount\":500000,\"meterno\":\"07067414883\",\"customer_name\":\"STUDIO PHOTO AMAZONE\"},\"tstamp\":\"2017-12-27 11:20:43\"}"
	end	

end	