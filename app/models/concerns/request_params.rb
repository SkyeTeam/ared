# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:TooManyInstanceVariables: { max_instance_variables: 13 }
# :reek:TooManyMethods: { max_methods: 25 }

class RequestParams

	def serviceType
  		@serviceType
	end
	def serviceType=(val)
	  	@serviceType = val
	end


	def rechargeAmount
  		@rechargeAmount
	end
	def rechargeAmount=(val)
	  	@rechargeAmount = val
	end


	def customerFee
  		@customerFee
	end
	def customerFee=(val)
	  	@customerFee = val
	end


	def operator
  		@operator
	end
	def operator=(val)
	  	@operator = val
	end


	def clientMobileNo
  		@clientMobileNo
	end
	def clientMobileNo=(val)
	  	@clientMobileNo = val
	end
	

	def description
  		@description
	end
	def description=(val)
	  	@description = val
	end


	def clientMeterNo
  		@clientMeterNo
	end
	def clientMeterNo=(val)
	  	@clientMeterNo = val
	end


	def serviceTxnID
  		@serviceTxnID
	end
	def serviceTxnID=(val)
	  	@serviceTxnID = val
	end


	def clientSmartCardNo
  		@clientSmartCardNo
	end
	def clientSmartCardNo=(val)
	  	@clientSmartCardNo = val
	end



	def internetDuration
  		@internetDuration
	end
	def internetDuration=(val)
	  	@internetDuration = val
	end
	

	def internetUsedMB
  		@internetUsedMB
	end
	def internetUsedMB=(val)
	  	@internetUsedMB = val
	end


	def clientAreaID
  		@clientAreaID
	end
	def clientAreaID=(val)
	  	@clientAreaID = val
	end


	def clientWaterBillNo
  		@clientWaterBillNo
	end
	def clientWaterBillNo=(val)
	  	@clientWaterBillNo = val
	end


end	