# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class CreditReportManager

    def self.getDetails(params) # here post is for check search by name or phone , text2 is for name and text1 is for phone
    	
    	Log.logger.info("Inside the Credit Report Manager class's getDetails method at line = #{__LINE__}")

    	userType = params[:userType]
    	phone = params[:phoneCreditReport]
    	
    	Log.logger.info("At line = #{__LINE__} userType to serach the details is = #{userType} and phone = #{phone}")

    	response = Response.new

    	if userType.eql? Constants::CREDIT_REPORT_USER_TYPE_OPT[0] # This is for the all users
    		searchDetails(Constants::CREDIT_REPORT_USER_TYPE_OPT[0], nil)
    	elsif userType.eql? Constants::CREDIT_REPORT_USER_TYPE_OPT[1] # This is for the suspended users
    		searchDetails(Constants::CREDIT_REPORT_USER_TYPE_OPT[1], nil)
    	elsif userType.eql? Constants::CREDIT_REPORT_USER_TYPE_OPT[2] # This is used for the single user
    		searchDetails(Constants::CREDIT_REPORT_USER_TYPE_OPT[2], phone)
    	end
	end

	def self.searchDetails(userType, phone)

		if userType.eql? Constants::CREDIT_REPORT_USER_TYPE_OPT[0] # This is for the all users
			Log.logger.info("At line = #{__LINE__} and inside the if of the all users")
			usersList = User.where('role = ? ',Constants::MF_USER_ROLE).where('status = ? OR status = ?', Constants::USER_ACTIVE_STATUS, Constants::USER_SUSPEND_STATUS)
			response = fetchDetails(usersList)
    	elsif userType.eql? Constants::CREDIT_REPORT_USER_TYPE_OPT[1] # This is for the suspended users
    		Log.logger.info("At line = #{__LINE__} and inside the if of the suspended users")
    		usersList = User.where('role = ? ',Constants::MF_USER_ROLE).where('status = ?', Constants::USER_SUSPEND_STATUS)
			response = fetchDetails(usersList)
    	elsif userType.eql? Constants::CREDIT_REPORT_USER_TYPE_OPT[2] # This is used for the single user
    		Log.logger.info("At line = #{__LINE__} and inside the if of the single user")
    		usersList = User.where('role = ? and phone = ?',Constants::MF_USER_ROLE, phone)
			response = fetchDetails(usersList)
    	end
		
		return response

	end	

	def self.fetchDetails(usersList)
		
		response = Response.new

		indexValue = 0

		detailsHash = ActiveSupport::HashWithIndifferentAccess.new

		if usersList.present?
			usersList.each do |user|
				creditBalance = isCreditPresent(user)

				if creditBalance != 0
					indexValue = indexValue.to_i + 1
					detailsHash["agentID"+indexValue.to_s] = user.id
					detailsHash["agentName"+indexValue.to_s] = user.name.to_s + " " + user.last_name.to_s
					detailsHash["agentGender"+indexValue.to_s] = user.gender
					detailsHash["agentPhone"+indexValue.to_s] = user.phone
					detailsHash["agentStatus"+indexValue.to_s] = user.status
					detailsHash["agentCreditDate"+indexValue.to_s] = getAgentCreditDate(user)
					detailsHash["agentCreditBalance"+indexValue.to_s] = creditBalance
					detailsHash["agentPenaltyBalance"+indexValue.to_s] = Utility.getAgentPenaltyBalance(user)
					detailsHash["effectiveDays"+indexValue.to_s] = getAgentLoanCreatedAt(user)
				end	
			end	

			if detailsHash.present?
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.totalHashIndex=(indexValue)
				response.detailsHash=(detailsHash)
			else	
				response.Response_Code=(Error::NO_CREDIT_OCCUR)
	  		   	response.Response_Msg=(I18n.t :NO_CREDIT_OCCUR_MSG) 
	  		end   	
		else
			response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	     	response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
		end	

		return response

	end


	def self.getAgentLoanCreatedAt(agent)
		agentCreditAccount = Utility.getCurrentAgentLoanAccount(agent)
		effectiveDays = 0
		if agentCreditAccount.present?
			creditTakeAt = agentCreditAccount.created_at	
			effectiveDays = (Date.today - Date.parse(creditTakeAt.to_s)).to_i
		end
			
	  return effectiveDays	
	end


	def self.getAgentCreditDate(agent)
		agentCreditAccount = Utility.getCurrentAgentLoanAccount(agent)
		if agentCreditAccount.present?
			creditTakeAt = agentCreditAccount.created_at	
		end
	  return creditTakeAt	
	end	


	def self.isCreditPresent(agent)
		creditBalance = TxnHeader.my_money(Utility.getAgentLoanBalance(agent)).to_f
		if creditBalance != 0
			return creditBalance
		else
		 	return 0
		end 	
	end	

end