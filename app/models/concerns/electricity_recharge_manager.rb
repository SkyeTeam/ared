# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class ElectricityRechargeManager

    def self.checkMeterNumberForRecharge(currentAgent, params)
        
        clientMeterNo = params[""+AppConstants::CLIENT_METER_NO_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} inside the ElectricityRechargeManager's and checkMeterNumberForRecharge method is called with clientMeterNo is = #{clientMeterNo}")
     
        response = validate(currentAgent, clientMeterNo, Constants::BLANK_VALUE, Constants::BLANK_VALUE, Constants::BLANK_VALUE, Constants::BLANK_VALUE, false)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
        AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{currentAgent.id}, agent Phone = #{currentAgent.phone} and name is = #{Utility.getAgentFullName(currentAgent)}")

        responseData = FdiUtility.sendCheckMeterNoRequest(clientMeterNo, Utility.getCurrentDateTime)
        AppLog.logger.info("At line of response come from the FDI Server  is = \n #{responseData}")

        isJSONValid = TxnManager.valid_json?(responseData) 
        AppLog.logger.info("At line #{__LINE__} and now check that whether the response is come in a valid JSON or not = #{isJSONValid}")
    
        if isJSONValid
            resData = JSON.parse(responseData)
            statusCode = resData['status_code']
            statusMsg  = resData['status_msg']
            AppLog.logger.info("At line #{__LINE__} response code is come from FDI side is = #{statusCode} and response message is =#{statusMsg}")

            if statusCode == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE
                    customerName = resData['data']['customer_name']
                    meterNo = resData['data']['meterno']
                    AppLog.logger.info("At line #{__LINE__} customer name is = #{customerName} and meter no is =#{meterNo}")

                    response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                    response.Response_Msg=(statusMsg)
                    response.customerName=(customerName)
                    response.customerMeterNo=(meterNo)
                return response    
            else
                    response.Response_Code=(statusCode)
                    response.Response_Msg=(statusMsg)
                return response    
            end
        else
                response.Response_Code=(Error::INVALID_JSON_DATA)
                response.Response_Msg=(responseData)
            return response   
        end 

    end 

    def self.doRechargeMeterNumber(agent, params, request)

        AppLog.logger.info("At line #{__LINE__} inside the ElectricityRechargeManager and doRechargeMeterNumber method is called")

        rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
        customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
        clientMeterNo = params[""+AppConstants::CLIENT_METER_NO_PARAM_LABEL+""]
        clientMobileNo = params[""+AppConstants::CLIENT_MOBILE_NO_PARAM_LABEL+""]
        appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} rechargeAmount = #{rechargeAmount}, customerFee = #{customerFee}, clientMeterNo = #{clientMeterNo}, clientMobileNumber = #{clientMobileNo} and appTxnUniqueID = #{appTxnUniqueID}")

        response = validate(agent, clientMeterNo, clientMobileNo, rechargeAmount, customerFee, appTxnUniqueID, true)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
        AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{agent.id}, agent Phone = #{agent.phone} and name is = #{Utility.getAgentFullName(agent)}")

        calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(Constants::FDI_ELECTRICITY_LABEL, Constants::FDI_ELECTRICITY_LABEL, rechargeAmount)
        if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return calculatedResponse
        end
        
        AppUtility.savePendingRequest(appTxnUniqueID, Constants::FDI_ELECTRICITY_LABEL, Constants::FDI_ELECTRICITY_LABEL, clientMobileNo, rechargeAmount, agent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
       
        gatewayLogID = GatewayLog.getGatewayLogID
        aggregatorName = AppUtility.getAggregatorName(Constants::FDI_ELECTRICITY_LABEL)
        GatewayLog.saveGatewayLog(gatewayLogID, agent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")
        
        txnReferenceNo = AppUtility.getExtRefElectricity

        if Properties::IS_PRODUCTION_SERVER    
            processingResponse = sendMeterRechargeRequest(rechargeAmount, clientMeterNo, txnReferenceNo, clientMobileNo, gatewayLogID)
        else 
            processingResponse = getRowData
        end 
        AppLog.logger.info("At line #{__LINE__} and response code after hit to URL is = #{processingResponse.Response_Code}. (if it is 200 then saved into database)") 

        if processingResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
                AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
                AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of the transaction id = #{appTxnUniqueID}")
            return processingResponse 
        else
                requestParams = setRequestParams(rechargeAmount, customerFee, clientMeterNo, clientMobileNo, txnReferenceNo)
            return AppUtility.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
        end 
       
    end

    def self.setRequestParams(rechargeAmount, customerFee, clientMeterNo, clientMobileNo, txnReferenceNo)
        requestParams = RequestParams.new
            requestParams.serviceType=(Constants::FDI_ELECTRICITY_LABEL)
            requestParams.operator=(Constants::FDI_ELECTRICITY_LABEL)
            requestParams.rechargeAmount=(rechargeAmount) 
            requestParams.customerFee=(customerFee)  
            requestParams.clientMeterNo=(clientMeterNo)
            requestParams.clientMobileNo=(clientMobileNo)
            requestParams.description=(I18n.t :ELECTRICITY_RECH_RQST_DES)
            requestParams.serviceTxnID=(txnReferenceNo)
        return requestParams        
    end    

    def self.getRowData
        response = Response.new
            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("")
            response.electricityToken=("1234567890")
            response.electricityUnit=("100")
            response.electricityVat=("1.2")
            response.customerName=("TUBEMASO JONAS")
            response.serviceBalance=("123456")
        return response  
    end    

    def self.sendMeterRechargeRequest(rechargeAmount, clientMeterNo, txnnRefNo, clientMobileNo, gatewayLogID)

        response = Response.new

        begin
            AppLog.logger.info("At line #{__LINE__} and request is send to the Tomcat Server to recharge the meter number = #{clientMeterNo}")

            if Properties::IS_SIMULATOR_ENABLE
                responseData = SimulatorUtility.sendFDIRechargeRequest('electricity', clientMobileNo, rechargeAmount, txnnRefNo, Utility.getCurrentDateTime, clientMeterNo, nil, gatewayLogID)
            else
                responseData = FdiUtility.sendRechargeMeterNoRequest(clientMeterNo, rechargeAmount, txnnRefNo, clientMobileNo, Utility.getCurrentDateTime, gatewayLogID)
            end    

            GatewayLog.updateResponseData(gatewayLogID, responseData)
            AppLog.logger.info("At line #{__LINE__} and Response come from the FDI side is = \n #{responseData}")

            isJSONValid = TxnManager.valid_json?(responseData) 
            AppLog.logger.info("At line #{__LINE__} and now check that whether the response is come in a valid JSON or not = #{isJSONValid}")
        
            if isJSONValid
                resData = JSON.parse(responseData)
                statusCode = resData['status_code']
                statusMsg  = resData['status_msg']
                AppLog.logger.info("At line #{__LINE__} response code is = #{statusCode} and response message is = #{statusMsg}")
                
                if statusCode.to_i == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE
                        elcToken = resData['data']['token']
                        elcUnit = resData['data']['units']
                        elcVat = resData['data']['vat']
                        customerName = resData['data']['customer_name']

                        firstSplit = statusMsg["Your balance is: "]
                        balFirst = statusMsg.split(firstSplit,2)  
                        serviceBalance = balFirst[1]
                        AppLog.logger.info("At line #{__LINE__} and ELECTRICITY balance is = #{serviceBalance}")

                        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                        response.Response_Msg=("")
                        response.electricityToken=(elcToken)
                        response.electricityUnit=(elcUnit)
                        response.electricityVat=(elcVat)
                        response.customerName=(customerName)
                        response.serviceBalance=(serviceBalance)
                    return response 
                else
                        response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                        response.Response_Msg=(statusMsg)  
                    return response 
                end
            else
                    response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                    response.Response_Msg=(responseData)  
                return response  
            end    
        rescue => exception
                AppLog.logger.info("At line #{__LINE__} sendMeterRechargeRequest method is called and exception is = \n#{exception.message}")
                AppLog.logger.info("At line #{__LINE__} sendMeterRechargeRequest method is called and full exception is = \n#{exception.backtrace.join("\n")}")
                
                response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                response.Response_Msg=(exception.message)  
            return response  
        end

    end

    def self.validate(currentAgent, clientMeterNo, clientMobileNo, rechargeAmount, customerFee, appTxnUniqueID, isRechargeMeterNo)
      
        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("At line #{__LINE__} agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end

            if !clientMeterNo.present?
                    AppLog.logger.info("At line #{__LINE__} client meter number is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
            end    

            #Now we check that the agent account is suspend or not
            isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
            AppLog.logger.info("At line #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
            if isAccountSuspend
                    response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
                return response
            end  
            
            if isRechargeMeterNo   
                
                if !clientMobileNo.present? || !rechargeAmount.present? || !customerFee.present? || !appTxnUniqueID.present?
                        AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
                        response.Response_Code=(Error::INVALID_DATA)
                        response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                    return response
                end    

                #Now check that the amount is cannot be less then zero 
                if rechargeAmount.to_f <= 0
                        AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                        response.Response_Code=(Error::LOAN_AMOUNT_LESS_THEN_ZERO)
                        response.Response_Msg=(I18n.t :LOAN_AMOUNT_LESS_THEN_ZERO_MSG) 
                    return response
                end

                #Now check that the amount should be divisble by the 10
                if (rechargeAmount.to_f%10) != 0
                        AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                        response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
                        response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
                    return response
                end
                
                #Now check that the unique txn id that is generated by the app can not be exist
                isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
                if isAppUniqueTxnIdExist
                    AppLog.logger.info("At line #{__LINE__} and unique txn id that is generated by the app is exist")
                        response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
                        response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
                    return response
                end

                #Now check that the servcie balance is deceed from threshold or not
                if AppUtility.isServiceBalanceDeceedThreshold(Constants::FDI_ELECTRICITY_LABEL)
                    AppLog.logger.info("At line #{__LINE__} and servcie balance is deceed then the threshold and email is sent to the admins")
                    AppUtility.sendThresholdEmailToAdmin(Constants::FDI_ELECTRICITY_LABEL)
                end 

                isServiceEnable = Utility.checkServiceStatus(Constants::FDI_ELECTRICITY_LABEL)
               
                if !isServiceEnable
                    AppLog.logger.info("At line #{__LINE__} and service is currently disable in service_setting table")
                        response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
                        response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
                    return response
                end

                agentAccountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
                agentTotalVATPending = TxnHeader.my_money(VatManager.getAgentTotalVAT(currentAgent.id, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)).to_f
                AppLog.logger.info("At line #{__LINE__} account balance of the agent is = #{agentAccountBalance} and pending agent VAT is = #{agentTotalVATPending}")
                if (agentAccountBalance - agentTotalVATPending) <   TxnHeader.my_money(rechargeAmount).to_f +   TxnHeader.my_money(customerFee).to_f
                        AppLog.logger.info("At line #{__LINE__} and agent account balance is below then the rechargeAmount")
                        response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
                        response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
                    return response
                end

            end    

            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("") 
        return response  
    end

end	