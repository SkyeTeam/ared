# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class TaxReportManager

	def self.serachTaxDetails(params)
	
		Log.logger.info("Inside the Commission Manager class and serachTaxDetails method is called at line  = #{__LINE__}")
			
		month = params[:month]
		year = params[:year]
		response = Response.new

        unless month.present? || year.present?
                Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end 
		
		Log.logger.info("At line  = #{__LINE__} and parameters are month is = #{month} and year is = #{year}")

		monthNumber =Date::MONTHNAMES.index(month)

		Log.logger.info("At line  = #{__LINE__} and month number is = #{monthNumber}") 

        month_beginning = Date.new(year.to_i, monthNumber.to_i)
        month_ending = month_beginning.end_of_month

		firstDate = Date.parse(month_beginning.to_s)
        beginFromDate = firstDate.beginning_of_day

        secondDate = Date.parse(month_ending.to_s)
        endToDate = secondDate.end_of_day

        Log.logger.info("At line = #{__LINE__} and begin of from date is = #{beginFromDate} and end of to date is = #{endToDate}")     
         
        commPaidMonthlyList =  AgentCommission.find_by_sql("SELECT u.id,u.name,u.last_name,u.phone,comm.txn_date,comm.acc_balance,comm.loan,
        											  comm.penalty,comm.total_comm,comm.tax_percentage,comm.total_tax 
													  FROM agent_commissions comm , users u
													  WHERE u.id = comm.user_id AND comm.service_type = '#{Constants::TAX_DEDUCT_LABEL}' 
													  AND comm.txn_date >= '#{beginFromDate.to_s}' AND comm.txn_date <= '#{endToDate.to_s}'
                                                      ORDER BY comm.txn_date DESC")

        Log.logger.info("At line = #{__LINE__} and check that whether that the  details in the database is exist or not = #{commPaidMonthlyList.present?}")
         
        commPaidArray = Array.new
          
        if commPaidMonthlyList.present?

            commPaidMonthlyList.each do |profitLose|

                commPaidArrayHash = ActiveSupport::HashWithIndifferentAccess.new

   				commPaidArrayHash[:user_id]=profitLose.id
                commPaidArrayHash[:name]=profitLose.name
                commPaidArrayHash[:last_name]=profitLose.last_name
                commPaidArrayHash[:phone]=profitLose.phone
                commPaidArrayHash[:txn_date]=profitLose.txn_date
                commPaidArrayHash[:total_comm]=profitLose.total_comm
                commPaidArrayHash[:tax_percentage]=profitLose.tax_percentage
                commPaidArrayHash[:total_tax]=profitLose.total_tax
                
                commPaidArray << commPaidArrayHash 

            end  
        end

        Log.logger.info("At line = #{__LINE__} and check that the data in the array is pesent or not = #{commPaidArray.present?} ")

        if commPaidArray.present?
	            response.commPaidMonthlyList=(commPaidArray)  
	            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            return response   
        else
	          	response.Response_Code=(Error::NO_TRANSCATIONS)
			    response.Response_Msg=(I18n.t :NO_TRANSCATIONS_MSG)
		    return response 
        end
	end
end	