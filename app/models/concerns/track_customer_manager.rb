# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class TrackCustomerManager

	def self.getDetails(params)
			Log.logger.info("At line = #{__LINE__} and inside the TrackCustomerManager class and getDetails method is called")

			response = Response.new

			serviceType = params[:transactionType]
			subType = params[:operator]
			userType = params[:userTypeCommReport]
			userPhone = params[:phoneCommReport]
			fromDate = params[:from_date]
			toDate = params[:to_date]

			Log.logger.info("At line #{__LINE__} and parameters are serviceType = #{serviceType}, subType = #{subType}, userType = #{userType}, userPhone = #{userPhone}, fromDate = #{fromDate} and toDate = #{toDate}")

			response = validate(serviceType, userType, userPhone, fromDate, toDate)

	        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	            return response
	        end

	        agentID = response.Response_Msg
	        Log.logger.info("At line #{__LINE__} and agentID is = #{agentID}")

   			fromDate = Date.parse(fromDate.to_s).beginning_of_day
		    toDate = Date.parse(toDate.to_s).end_of_day

	        txnHeader = fetchDetails(serviceType, subType, userType, agentID, fromDate, toDate)

			if txnHeader.present?
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.detailsHash=(txnHeader)
			else
				response.Response_Code=(Error::NO_HISTORY)
			   	response.Response_Msg=(I18n.t :NO_HISTORY_MSG) 
			end	

	  	return response	 
	end	

	def self.fetchDetails(serviceType, subType, userType, agentID, fromDate, toDate)

			startingQuery = "SELECT account_id, service_type, sub_type, sum(amount) as amount, COUNT(service_type) AS count "
			startingQuery << "FROM txn_headers WHERE txn_date >= '#{fromDate}' AND txn_date <= '#{toDate}' AND "

			if serviceType.eql? Constants::ANY_SERVICE_LABEL
				inBwQueryPart = "service_type IN (#{Utility.getSQLQueryServiceTypes})"
			else
				if serviceType.eql? Constants::MTOPUP_LABEL
					#Check the sub type
					if subType.eql? Constants::ANY_COMPANY_LABEL
						inBwQueryPart = "service_type = '#{serviceType}' AND sub_type IN (#{Utility.getSQLQueryAirtimeSubTypes}) "
					else
						inBwQueryPart = "service_type = '#{serviceType}' AND sub_type = '#{subType}'"
					end	
				else
					# No sub type is present
					inBwQueryPart = "service_type = '#{serviceType}'"
				end	
			end	

			if userType.eql? Constants::SINGLE_USER_RADIO_LABEL
				singleUserQuery = " AND agent_id::INT = '#{agentID}'"
			else	
				singleUserQuery = Constants::BLANK_VALUE
			end	

			endingQuery = " GROUP BY account_id, service_type, sub_type"

			query = startingQuery.to_s + inBwQueryPart.to_s + singleUserQuery.to_s + endingQuery.to_s

			Log.logger.info("At line = #{__LINE__} and query that is used to fetch the details is = #{query}")
			
			txnHeader = Utility.executeSQLQuery(query)

		return txnHeader	

	end

	def self.validate(serviceType, userType, userPhone, fromDate, toDate)
        response = Response.new
        agentID = Constants::BLANK_VALUE
        
        unless serviceType.present? || userType.present? || fromDate.present? || toDate.present?
                Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end 

        if userType.eql? Constants::SINGLE_USER_RADIO_LABEL
        	unless userPhone.present?
                	Log.logger.info("At line = #{__LINE__} and agent phone number is come nil from the web")
                	response.Response_Code=(Error::INVALID_DATA)
                	response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            	return response
            end	
        
			currentAgent = Utility.getCurrentAgentFromPhone(userPhone)
            unless currentAgent.present?
        			Log.logger.info("At line #{__LINE__} and agent is not present")
        			response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        			response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      			return response
      		end
      		agentID = currentAgent.id	
        end

        if fromDate.first > toDate.first
        		response.Response_Code=(Error::CHECK_DATES_AGAIN)
		    	response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
		    return response
        end	

        if !Utility.isValidYear(fromDate.first)
    			Log.logger.info("At line = #{__LINE__} and from date has not a valid year ")
   				response.Response_Code=(Error::INVALID_DATE)
    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
    		return response
		end

		if !Utility.isValidYear(toDate.first)
    			Log.logger.info("At line = #{__LINE__} and to date has not a valid year ")
   				response.Response_Code=(Error::INVALID_DATE)
    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
    		return response
		end
	   
	    response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=(agentID) 
      return response  
    end

end	