# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class UserProfile

	def self.getAgentProfile(params)
		response = Response.new

			agentID = params[:id] 
			Log.logger.info("At line #{__LINE__} inside the UserProfile class and getAgentProfile method is called with agentID = #{agentID}")

			unless agentID.present?
					response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response
			end

			currentAgent =  Utility.getCurrentAgent(agentID)
			agentAccountAge  = Date.today.mjd - Date.parse(currentAgent.created_at.to_s).mjd
			Log.logger.info("At line #{__LINE__} and difference of the days between agent created and today date are = #{agentAccountAge}")

			totalAgentTurnover = getAgentTurnover(agentAccountAge, agentID)

			totalAgentTurnoverIndServ = getAgentTurnoverIndvServ(agentAccountAge, agentID)
			
			totalAgentCommission = TxnHeader.group('service_type').where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").sum('agent_commission')
			
			totalAgentTxns = TxnHeader.group('service_type').where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").count
				
			totalAgentTxnsAmount=TxnHeader.group('service_type').where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").sum('amount')
							
			response.Response_Code= (Constants::SUCCESS_RESPONSE_CODE)
			response.user=(currentAgent)
			response.totalTurnover=(totalAgentTurnover)
			response.totalTurnoverIndServ=(totalAgentTurnoverIndServ)
			response.totalCommission=(totalAgentCommission)
			response.totalTransactionCount=(totalAgentTxns)
			response.totalAgentTxnsAmount=(totalAgentTxnsAmount)
			response.date=(getAgentAgeData(agentAccountAge))
			
		return response
	end

	def self.getAgentAgeData(agentAccountAge)
		if agentAccountAge.to_i > 365
			return (agentAccountAge/365).to_s + " " + "Years"
		elsif agentAccountAge.to_i > 31
			return (agentAccountAge/30).to_s + " " + "Months"
		else
			return agentAccountAge.to_s + " "+ "Days"
		end	
	end

	def self.getAgentTurnover(agentAccountAge, agentID)
			if agentAccountAge.to_i > 365
				totalAgentTurnover = TxnHeader.group_by_year(:txn_date, series: false, format: "%Y").where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").sum('amount')
			else
				totalAgentTurnover = TxnHeader.group_by_month(:txn_date, series: false, format: "%B-%Y").where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").sum('amount')	
			end	
		return totalAgentTurnover	
	end	

	def self.getAgentTurnoverIndvServ(agentAccountAge, agentID)
			if agentAccountAge.to_i > 365
				totalAgentTurnover = TxnHeader.group(:service_type).group_by_year(:txn_date, series: false, format: "%Y").where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").sum('amount')
			else
				totalAgentTurnover = TxnHeader.group(:service_type).group_by_month(:txn_date, series: false, format: "%B-%Y").where(agent_id: agentID).where(service_type: Utility.getServiceTypes).where("is_rollback != #{Constants::TXN_APPROVED_ROLLBACK_STATUS}").sum('amount')
			end	
		return totalAgentTurnover	
	end	

	def self.fetchTotalTurnoverData(totalAgentTurnoverObj)
		response = Response.new

			xAxisLabelValue, totalTurnover = Array.new, Array.new
			
			if totalAgentTurnoverObj.present?
				totalAgentTurnoverObj.each do |viewName, amount|
					xAxisLabelValue.push(viewName)
					totalTurnover.push(TxnHeader.my_money(amount).to_f)
				end	
			end	

			response.xAxisLabelValue=(xAxisLabelValue)
			response.serviceValue=(totalTurnover)
		return response
	end	

	def self.fetchTotalTurnoverIndvSerData(totalAgentTurnoverObj, xAxisLabelArray, serviceName)
        	viewType, serviceType, serviceValue, finalValue = Array.new, Array.new, Array.new, Array.new
	        
	        if totalAgentTurnoverObj.present?
		        totalAgentTurnoverObj.each do |viewArray, amount|
		          serviceType.push(viewArray[0])
		          viewType.push(viewArray[1])
		          serviceValue.push(amount.to_f)
		        end  
	        end

        return getFinalValueArray(xAxisLabelArray, serviceType, viewType, serviceValue, serviceName)
	end

	def self.getFinalValueArray(xAxisLabelArray, serviceType, viewType, serviceValue, serviceName)
		finalValue = Array.new
		
			counter = -1
			for i in 0..xAxisLabelArray.size.to_i - 1
	          	for j in 0..viewType.size.to_i - 1
	            	if xAxisLabelArray[i].eql? viewType[j]
	              		if serviceType[j].eql? serviceName
	                		counter = j
	                		break
	              		else
	                		counter = -1
	              		end  
	            	end  
	          	end 

	          	if counter != -1
	            	finalValue.push(serviceValue[counter])
	          	else
	            	finalValue.push(0)
	          	end 
	        end
	    return finalValue   
    end 

    def self.getGraphData(totalAgentCommObj)
	    response = Response.new
	    totalValue = 0
	    	serviceType, serviceValue = Array.new, Array.new
	    	if totalAgentCommObj.present?
		    	totalAgentCommObj.each do |serviceName, txnCountOrAmt|
		    		serviceType.push(Utility.getServiceTypeDisplayValue(serviceName))	
		    		serviceValue.push(TxnHeader.my_money(txnCountOrAmt).to_f)
		    		totalValue = totalValue + TxnHeader.my_money(txnCountOrAmt).to_f
		    	end	
		    end	
    		response.serviceName=(serviceType)
			response.serviceValue=(serviceValue)
			response.totalValue=(totalValue)
		return response
    end	

end