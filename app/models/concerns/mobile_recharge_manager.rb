# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class MobileRechargeManager

	def self.doMobileRecharge(agent, params, request)
		AppLog.logger.info("At line #{__LINE__} inside the MobileRechargeManager class and doMobileRecharge method is called")

		clientMobileNo = params[""+AppConstants::MOBILE_PARAM_LABEL+""]
        rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
        customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
        operator = params[""+AppConstants::OPERATOR_PARAM_LABEL+""]
        appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} and parameters are clientMobileNo = #{clientMobileNo}, rechargeAmount = #{rechargeAmount}, customerFee = #{customerFee}, operator = #{operator} and appTxnUniqueID = #{appTxnUniqueID}")
       
        response = validate(agent, clientMobileNo, rechargeAmount, customerFee, operator, appTxnUniqueID)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
    	AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{agent.id}, agent Phone = #{agent.phone} and name is = #{Utility.getAgentFullName(agent)}")
		 
		calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(Constants::MTOPUP_LABEL, operator, rechargeAmount)
		if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return calculatedResponse
        end
	    
        AppUtility.savePendingRequest(appTxnUniqueID, Constants::MTOPUP_LABEL, operator, clientMobileNo, rechargeAmount, agent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
       
       	gatewayLogID = GatewayLog.getGatewayLogID
       	aggregatorName = AppUtility.getAggregatorName(operator)
       	GatewayLog.saveGatewayLog(gatewayLogID, agent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
       	AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")
  
	  	processingResponse = processRequest(clientMobileNo, rechargeAmount, operator, gatewayLogID)
	    AppLog.logger.info("At line #{__LINE__} and response code after hit to URL is = #{processingResponse.Response_Code}. (if it is 200 then saved into database)") 

      	if processingResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
      			AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
	            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of the transaction id = #{appTxnUniqueID}")
	        return processingResponse 
      	else
      			requestParams = setRequestParams(rechargeAmount, customerFee, operator, clientMobileNo, processingResponse.serviceTxnID)
      		return AppUtility.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
      	end
	end	

	def self.processRequest(clientMobileNo, rechargeAmount, operator, gatewayLogID)
		response = Response.new

			if Properties::IS_PRODUCTION_SERVER
			
				AppLog.logger.info("At line #{__LINE__} and country name is = #{Properties::COUNTRY_NAME}")
				if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]      
			       
			        if operator.eql? Constants::FDI_TIGO_LABEL
			          externalReference = ServiceFee.getTigoRequestExtRef()
			          response = sendTigoRechargeRequest(clientMobileNo, rechargeAmount, externalReference, gatewayLogID)
			        elsif operator.eql? Constants::AIRTEL_LABEL
			          externalReference = ServiceFee.getAirtelRequestExtRef()
			          response = sendAirtelRechargeRequest(clientMobileNo, rechargeAmount, externalReference, gatewayLogID)
			        elsif operator.eql? Constants::MTN_LABEL  
			          externalReference = ServiceFee.getMTNRequestExtRef()
			          response = sendMTNRechargeRequest(clientMobileNo, rechargeAmount, externalReference, gatewayLogID)
			        end
			       
			    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]

			    	if !clientMobileNo.start_with? UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s and  !clientMobileNo.start_with? UgandaConstants::UGANDA_PHONE_CODE_SECOND.to_s
			        	clientMobileNo = UgandaConstants::UGANDA_PHONE_CODE_FIRST.to_s + clientMobileNo.to_s
			        end	

		        	externalReference = Utility.getRandomNumber(18)
			        response = sendPaywayRechargeRequest(rechargeAmount, clientMobileNo, externalReference, AppUtility.getUgandaServiceProvider(operator), gatewayLogID)

			    end  
  
		    else
		        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            	response.Response_Msg=("")
		    end  

		    response.serviceTxnID=(externalReference)

		return response
	end	

	def self.setRequestParams(rechargeAmount, customerFee, operator, clientMobileNo, serviceTxnID)
		requestParams = RequestParams.new
      		requestParams.serviceType=(Constants::MTOPUP_LABEL)
      		requestParams.rechargeAmount=(rechargeAmount)
      		requestParams.customerFee=(customerFee)
      		requestParams.operator=(operator)
      		requestParams.clientMobileNo=(clientMobileNo)
      		requestParams.description=(I18n.t :MOBILE_REQUEST_DES)
      		requestParams.serviceTxnID=(serviceTxnID)
      	return requestParams	
	end	
	
	def self.sendTigoRechargeRequest(clientMobileNo, rechargeAmount, txnRefNo, gatewayLogID)
    	response = Response.new

    	begin
			if Properties::IS_SIMULATOR_ENABLE
	      		responseData = SimulatorUtility.sendFDIRechargeRequest('airtime', clientMobileNo, rechargeAmount, txnRefNo, Utility.getCurrentDateTime, nil, nil, gatewayLogID)
	      	else
		      	responseData = FdiUtility.sendMobileRequest(clientMobileNo, rechargeAmount, txnRefNo, Utility.getCurrentDateTime, gatewayLogID)
		    end  	

	      	GatewayLog.updateResponseData(gatewayLogID, responseData)
		    AppLog.logger.info("At line #{__LINE__} and response come from the FDI side is = \n#{responseData}")

		    isJSONValid = TxnManager.valid_json?(responseData) 
            AppLog.logger.info("At line #{__LINE__} and now check that whether the response is come in a valid JSON or not = #{isJSONValid}")
        
            if isJSONValid
		        resData = JSON.parse(responseData)
		        statusCode = resData['status_code']
		        statusMsg = resData['status_msg']
		        AppLog.logger.info("At line #{__LINE__} response code is = #{statusCode} and response message is = #{statusMsg}")

		        if statusCode == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE
			          	firstSplit = statusMsg["Your combined wallet balance is: "]
			          	balFirst = statusMsg.split(firstSplit,2)  
			          	tigoServiceBalance = balFirst[1]
			          	AppLog.logger.info("At line #{__LINE__} and balance of the TIGO service is = #{tigoServiceBalance}")
			          	
			          	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            			response.Response_Msg=(statusMsg)
            			response.serviceBalance=(tigoServiceBalance)
            			response.thirdPartyResponseID=(nil)
        			return response
		        else
			        	response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            			response.Response_Msg=(statusMsg)
            		return response	
		        end
		    else
			        response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            		response.Response_Msg=(responseData) 
            	return response	
		    end
	    rescue => exception
	      		AppLog.logger.info("At line #{__LINE__} sendTigoRechargeRequest method is called and exception is = \n#{exception.message}")
	      		AppLog.logger.info("At line #{__LINE__} sendTigoRechargeRequest method is called and full exception is = \n#{exception.backtrace.join("\n")}")
	      		response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	            response.Response_Msg=(exception.message) 
	        return response 
	    end 
  	end

	def self.sendAirtelRechargeRequest(clientMobileNo, rechargeAmount, txnRefNo, gatewayLogID)
	    response = Response.new  

	    if Properties::IS_SIMULATOR_ENABLE
	    	responseData = SimulatorUtility.sendAirtelRechargeRequest(txnRefNo, clientMobileNo, rechargeAmount, gatewayLogID)
	    else
	    	responseData = AirtelUtility.sendRequest(txnRefNo, clientMobileNo, rechargeAmount, gatewayLogID)
	    end	

	    GatewayLog.updateResponseData(gatewayLogID, responseData)
        AppLog.logger.info("At line #{__LINE__} and response come from the airtel is = \n#{responseData}")
	    
	    firstArray = responseData.split("<TXNSTATUS>",2)
	    secondArray = firstArray[1].split("</TXNSTATUS>",2)
      	AppLog.logger.info("At line #{__LINE__} response code is come from the Airtel side = #{secondArray[0]} (if code is 200 then the success)")

      	if secondArray[0].to_i == 200
		        responseTxnId = secondArray[1].split("<TXNID>",2)
		        finalResponseTxnId = responseTxnId[1].split("</TXNID>",2)
		        airtelResponseTxnID = finalResponseTxnId[0]
		        AppLog.logger.info("At line #{__LINE__} and transaction id from the airtel side is = #{airtelResponseTxnID}")

		        firstMezArray = finalResponseTxnId[1].split("<MESSAGE>",2)
		        x = firstMezArray[1].split(":",3)
		        finalMezArray = x[2].split("</MESSAGE>",2) 

		        balanceDetail = finalMezArray[0]
		        firstSplit = balanceDetail["Your new balance is "]
		        secondSplit = balanceDetail[" RWF"]
		          
		        balFirst = balanceDetail.split(firstSplit,2)  
		        balSecond = balFirst[1].split(secondSplit,2)
		        airtelServiceBalance = balSecond[0]
		        AppLog.logger.info("At line #{__LINE__} and current balance of the airtel = #{airtelServiceBalance}")  

		        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            	response.Response_Msg=("")
            	response.serviceBalance=(airtelServiceBalance)
            	response.thirdPartyResponseID=(airtelResponseTxnID)
        	return response
      	else
        	if secondArray[1].include? '<MESSAGE>'
          			firstMezArray = secondArray[1].split("<MESSAGE>",2)
          			if firstMezArray[1].include? ":"
	                  	x = firstMezArray[1].split(":",3)
	                  	finalMezArray = x[2].split("</MESSAGE>",2) 
	                else
	                  	finalMezArray = firstMezArray[1].split("</MESSAGE>",2) 
	                end  

          			response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            		response.Response_Msg=(finalMezArray[0])
            	return response	
        	else
        			response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            		response.Response_Msg=("Contact to administrator")
            	return response
        	end 
      	end 
  	end
 
 	def self.sendMTNRechargeRequest(clientMobileNo, rechargeAmount, txnRefNo, gatewayLogID)
    	processingResponse = Response.new

    	begin
	      	AppLog.logger.info("At line #{__LINE__} amd sendMTNRechargeRequest method is called")

	      	rechargeData = getMTNRechargeXML(clientMobileNo, rechargeAmount, txnRefNo)
	      
	      	rechargeUrlMTN = Properties::MTNRECHARGEURL 
	      
	      	uri = URI(rechargeUrlMTN)
	      	http = Net::HTTP.new(uri.host, uri.port)
	      	request = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'text/xml'})
	      	request.body = rechargeData
	      	requestData = request.body

	      	GatewayLog.updateRequestData(gatewayLogID, requestData)
	      	AppLog.logger.info("At line #{__LINE__} and request send to the tomcat server is = \n #{requestData}")

	      	response = http.request(request)
	      	responseData = response.body

	      	GatewayLog.updateResponseData(gatewayLogID, responseData)
	      	AppLog.logger.info("At line #{__LINE__} and Response come from the tomcat server is = \n #{responseData}")

	        if responseData.include? 'responseCode='
	          	firstResponseCode = responseData.split('responseCode=',2)
	          	txnReferenceID = firstResponseCode[0].split('txRefId=',2)

	          	secondResponseCode = firstResponseCode[1].split('; responseMessage',2)
	          	AppLog.logger.info("At line #{__LINE__}  response code come from the MTN side is = #{secondResponseCode[0]} (if code is 0 then the success)")

	          	if secondResponseCode[0].to_i == 0
		            	mtnReference = txnReferenceID[1].split(';',2)
		            	mtnResponseTxnID = mtnReference[0]

		            	firstBalanceSplit = mtnReference[1].split('origBalance=',2)
		            	secondBalanceSplit = firstBalanceSplit[1].split('; destBalance',2) 
		            	mtnServiceBalance =  secondBalanceSplit[0]
		            	AppLog.logger.info("At line #{__LINE__} and MTN response transaction ID is = #{mtnResponseTxnID} and balance is = #{mtnServiceBalance}")
		            	
		            	processingResponse.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		            	processingResponse.Response_Msg=("")
		            	processingResponse.serviceBalance=(mtnServiceBalance)
		            	processingResponse.thirdPartyResponseID=(mtnResponseTxnID)
		        	return processingResponse
	          	else
		            	firstResponseMsg = secondResponseCode[1].split('=',2)
		            	secondResponseMsg = firstResponseMsg[1].split(';',2)

		            	processingResponse.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            			processingResponse.Response_Msg=(secondResponseMsg[0])
            		return processingResponse	
	          	end  
	        else
		          	firstResponseMsg = responseData.split('Message :',2)
		          	
		          	processingResponse.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            		processingResponse.Response_Msg=(firstResponseMsg[1])
            	return processingResponse	
	        end 
      	rescue => exception
      			AppLog.logger.info("At line #{__LINE__} sendMTNRechargeRequest method is called and exception is = \n#{exception.message}")
	      		AppLog.logger.info("At line #{__LINE__} sendMTNRechargeRequest method is called and full exception is = \n#{exception.backtrace.join("\n")}")
	      		processingResponse.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	            processingResponse.Response_Msg=(exception.message) 
	        return processingResponse 
      	end
  	end

  	def self.getMTNRechargeXML(clientMobileNo, rechargeAmount, txnRefNo)
	  		thirdParty = ThirdPartyConfig.getThirdparty(Constants::MTN_AGGREGATOR)

	  		mtnUsername = thirdParty.username
	  		mtnPassword = thirdParty.password
	  		mtnConnectionTomeout = thirdParty.connection_timeout.to_i
		
		    xmlContent = "<?xml version=\"1.0\"?><!DOCTYPE COMMAND PUBLIC \"-//Ocam//DTD XML Command1.0//EN\" \"xml/command.dtd\">"
			xmlContent << "<COMMAND><seq>"+txnRefNo.to_s+"</seq><orig>250780333331</orig><dest>250"+clientMobileNo.to_s+"</dest><amount>"+rechargeAmount.to_s+"</amount>"
			xmlContent << "<tariffId>1</tariffId><spId>1</spId><username>"+mtnUsername.to_s+"</username><password>"+mtnPassword.to_s+"</password>"
			xmlContent << "<connectionTimeout>"+mtnConnectionTomeout.to_s+"</connectionTimeout></COMMAND>"

	    return xmlContent   
  	end  

  	def self.sendPaywayRechargeRequest(amount, customerNumber, txnID, serviceProviderId, gatewayLogID)
  		processingResponse = Response.new

  		begin
  			AppLog.logger.info("At line #{__LINE__} and sendPaywayRechargeRequest method is called")

	        thirdParty = ThirdPartyConfig.getThirdparty(UgandaConstants::PAYWAY_AGGREGATOR)
	      
         	paywayUsername = thirdParty.username

	        paywayPassword = thirdParty.password

	        connectionTimeout = thirdParty.connection_timeout.to_i

	        requestData = getPayWayMobileRechargeXML(paywayUsername, paywayPassword, Utility.getCurrentDateTimePayWay, amount, customerNumber, txnID, serviceProviderId)

	        GatewayLog.updateRequestData(gatewayLogID, requestData)
	      	AppLog.logger.info("At line #{__LINE__} and request send to the tomcat server is = \n #{requestData}")

	        thirdPartyURL = Properties::PAYWAY_MOBILE_RECHARGE_URL

	        thirdPartyURI = URI.parse(thirdPartyURL)
	         
	        http = Net::HTTP.new(thirdPartyURI.host, thirdPartyURI.port)
	        http.read_timeout = connectionTimeout

	        request = Net::HTTP::Post.new(thirdPartyURI.request_uri)
	        request['content-type'] = 'text/xml'
	        request.body = requestData
	           
	        response = http.request(request)
	        responseData = response.body

	       	GatewayLog.updateResponseData(gatewayLogID, responseData)
	      	AppLog.logger.info("At line #{__LINE__} and Response come from the tomcat server is = \n #{responseData}")

	      	if responseData.present?
		      	if responseData.include? '<status>Success</status>'
	            		processingResponse.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		            	processingResponse.Response_Msg=("")
		            	processingResponse.serviceBalance=(PaywayUtility.getPaywayBalance.to_s)
		            	processingResponse.thirdPartyResponseID=(Utility.getTagValue(responseData, 'outTxId'))
		        	return processingResponse
	          	else
		            	if responseData.include? '<faultstring>'
		            		processingResponse.Response_Msg=(Utility.getTagValue(responseData, 'faultstring'))
		 				elsif responseData.include? '<notes>'
		 				   	processingResponse.Response_Msg=(Utility.getTagValue(responseData, 'notes'))
		 				else
		 					processingResponse.Response_Msg=(responseData)
		 				end 
	 					
	 					processingResponse.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
            		return processingResponse          			           		
	          	end
	        end  
        rescue => exception
        		AppLog.logger.info("At line #{__LINE__} sendPaywayRechargeRequest method is called and exception is = \n#{exception.message}")
	      		AppLog.logger.info("At line #{__LINE__} sendPaywayRechargeRequest method is called and full exception is = \n#{exception.backtrace.join("\n")}")
	      		
	      		processingResponse.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	            processingResponse.Response_Msg=(exception.message) 
	        return processingResponse 
      	end 
  	end	

  	def self.getPayWayMobileRechargeXML(paywayUsername, paywayPassword, dateTime, amount, customerNumber, txnID, serviceProviderId)
  		newLine = "\n"
  		tab = "\t"

  		requestData = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">#{newLine}"
          
      	requestData << "#{tab}<s:Header>#{newLine}"
      	requestData << "#{tab}#{tab}<Action s:mustUnderstand=\"1\" xmlns=\"http://schemas.microsoft.com/ws/2005/05/addressing/none\" />#{newLine}"
      	requestData << "#{tab}</s:Header>#{newLine}"
      
      	requestData << "#{tab}<s:Body xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">#{newLine}"
      	requestData << "#{tab}#{tab}<makeTransfer xmlns=\"http://www.avs.ug/external/v3\">#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}<arg0 xmlns="">#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<auth>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}#{tab}<password>#{paywayPassword}</password>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}#{tab}<userName>#{paywayUsername}</userName>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}</auth>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<dateTime>#{dateTime}</dateTime>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<amountToTransfer>#{amount}</amountToTransfer>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<arguments />#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<customerNumber>#{customerNumber}</customerNumber>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<inTxId>#{txnID}</inTxId>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}#{tab}<serviceProviderId>#{serviceProviderId}</serviceProviderId>#{newLine}"
      	requestData << "#{tab}#{tab}#{tab}</arg0>#{newLine}"
      	requestData << "#{tab}#{tab}</makeTransfer>#{newLine}"
      	requestData << "#{tab}</s:Body>#{newLine}"
      
      	requestData << "</s:Envelope>"

      	return requestData
  	end	


	def self.validate(currentAgent, clientMobileNo, rechargeAmount, customerFee, operator, appTxnUniqueID)
        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("At line #{__LINE__} and agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end

            if !clientMobileNo.present? || !rechargeAmount.present? || !customerFee.present? || !operator.present? || !appTxnUniqueID.present?
                    AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
        	end

            if rechargeAmount.to_f <= 0
                    AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                    response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
                    response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
                return response
            end

            if (rechargeAmount.to_f%10) != 0
                    AppLog.logger.info("At line #{__LINE__} and recharge amount is may be not divisble by the 10")
                    response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
                    response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
                return response
            end

            isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
            AppLog.logger.info("At line #{__LINE__} and check that the agent account is suspended or not = #{isAccountSuspend}")
            if isAccountSuspend
                    response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
                return response
            end  

            isServiceEnable = Utility.checkServiceStatus(operator)
            AppLog.logger.info("At line #{__LINE__} and check that the service is enable or not = #{isServiceEnable}")
            if !isServiceEnable
                    response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
                    response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
                return response
            end
            
            agentAccountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
            agentTotalVATPending = TxnHeader.my_money(VatManager.getAgentTotalVAT(currentAgent.id, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)).to_f
            AppLog.logger.info("At line #{__LINE__} account balance of the agent is = #{agentAccountBalance} and pending agent VAT is = #{agentTotalVATPending}")
            if (agentAccountBalance - agentTotalVATPending) <   TxnHeader.my_money(rechargeAmount).to_f +   TxnHeader.my_money(customerFee).to_f
                    AppLog.logger.info("At line #{__LINE__} and agent account balance is below then the rechargeAmount plus customerFee")
                    response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
                    response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
                return response
            end

            isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
            AppLog.logger.info("At line #{__LINE__} and check that unique txn id that is generated by the app is exist or not = #{isAppUniqueTxnIdExist}")
            if isAppUniqueTxnIdExist
                    response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
                    response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
                return response
            end

            if AppUtility.isServiceBalanceDeceedThreshold(operator)
            	AppLog.logger.info("At line #{__LINE__} and servcie balance is deceed then the threshold and email is sent to the admins")
            	AppUtility.sendThresholdEmailToAdmin(operator)
            end	

        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 

      	return response  
    end

end	