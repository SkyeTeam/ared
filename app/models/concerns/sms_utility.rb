# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class SmsUtility

	def self.sendSmsReg(mobileNo, message)
	
		returnValue = nil
		responseData = nil

		if Properties::IS_PRODUCTION_SERVER
			# smsUrl = 'http://121.241.242.114:8080/bulksms/bulksms?username=arit-ared&password=rwanda&type=0&dlr=1&destination='+mobileNo+'&source=Demo&message='+message
			smsUrl = "http://rslr.connectbind.com/bulksms/bulksms?username=arit-ared&password=rwanda&type=0&dlr=1&destination=#{mobileNo}&source=Demo&message=#{message}"

			url = URI.parse(smsUrl)
			req = Net::HTTP::Get.new(url.to_s)
			res = Net::HTTP.start(url.host, url.port) {|http|
	  		response = http.request(req)
	  		responseData = response.body}

	  		Log.logger.info("At line #{__LINE__} and response come from the SMS side = #{responseData}")

	  		if responseData.include? "|"

		        firstArray = responseData.split("|",2)

		        if firstArray[0].to_i == 1701  
		           returnValue = 'true'
		        else
		           returnValue = 'false'
		        end  
		    else
		    	returnValue = 'false'
		    end 
		else
			returnValue = 'true'
		end      

	    Log.logger.info("At line #{__LINE__} and check SMS is sent or not  = #{returnValue}") 
			
		return returnValue
	end	
end