# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 18 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }
# :reek:RepeatedConditional { max_ifs: 8 }
# :reek:NilCheck { enabled: false }

class Utility
	
	def self.executeSQLQuery(sqlQuery)
		begin
      Log.logger.info("At line #{__LINE__} inside the Utility class's and executeSQLQuery method is called")
	    results = ActiveRecord::Base.connection.exec_query(sqlQuery)
	    if results.present?
	        return results
	    else
	        return nil
	    end
		rescue => exception
		 	  Log.logger.info("At line #{__LINE__} exception occur in the executeSQLQuery method is = \n #{exception.message}")
        Log.logger.info("At line #{__LINE__} full exception Details are = \n #{exception.backtrace.join("\n")}")
	   	return false
	  end
	end

  def self.getTxnStatus(intStatus)
    if intStatus == Constants::SUCCESS_RESPONSE_CODE
      return Constants::SUCCESS_LABEL
    elsif intStatus == Constants::INITIATE_TXN_CODE
      return Constants::TXN_INITATE_LABEL
    else
      return Constants::FAILED_LABEL
    end    
  end  

  def self.getTxnColor(stringTxnStatus)
    if stringTxnStatus.eql? Constants::SUCCESS_LABEL
      return Constants::CREDIT_COLOR
    elsif stringTxnStatus.eql? Constants::FAILED_LABEL
      return Constants::DEDUCT_COLOR
    else 
      return ''
    end  
  end  

  def self.getCurrentDateTime
      dateTime = DateTime.now.strftime('%Y%m%d%I%M%S')
    return dateTime 
  end

  def self.getCurrentDateTimePayWay
      dateTime = Time.now.strftime('%Y-%m-%dT%I:%M:%S.%L')
    return dateTime 
  end

  def self.getRandomNumber(length)
      noExpersion = [('0'..'9')].map { |i| i.to_a }.flatten
      randomNo = (0...length).map { noExpersion[rand(noExpersion.length)] }.join
    return randomNo        
  end  

  def self.getAlphaNumRandomNumber(length)
      noExpersion = [('a'..'z'),  ('0'..'9')].map { |i| i.to_a }.flatten
      randomNo = (0...length).map { noExpersion[rand(noExpersion.length)] }.join
    return randomNo        
  end

  def self.getCapAlphaNumRandomNumber(length)
      noExpersion = [('A'..'Z'),  ('0'..'9')].map { |i| i.to_a }.flatten
      randomNo = (0...length).map { noExpersion[rand(noExpersion.length)] }.join
    return randomNo        
  end  

  def self.getUserRemoteIP(request)
      remoteIP = request.remote_ip
    return remoteIP 
  end  

  def self.checkServiceStatus(serviceName)
    serviceSetting = ServiceSetting.where('service = ?', serviceName).first
    if serviceSetting.present?
      if serviceSetting.status.to_i == 1
        return Constants::BOOLEAN_TRUE_LABEL
      else
        return Constants::BOOLEAN_FALSE_LABEL
      end    
    else
      return Constants::BOOLEAN_FALSE_LABEL
    end  
  end  

  def self.getServiceFee(serviceType, subType)
      serviceFee = ServiceFee.where('service = ? and  company = ? and effective_date <= ?', serviceType, subType, Date.today).order('effective_date ASC').last
    return serviceFee
  end  

  def self.getAllActiveMF
      activeMFs = User.where('role = ?', Constants::MF_USER_ROLE).where('status = ?', Constants::USER_ACTIVE_STATUS).order('name ASC')
    return activeMFs  
  end

  def self.getAllSuspendMF
      activeMFs = User.where('role = ?', Constants::MF_USER_ROLE).where('status = ?', Constants::USER_SUSPEND_STATUS).order('name ASC')
    return activeMFs  
  end

  def self.getFormatedAmount(amount)
      if amount != nil 
        formatedAmount = amount.to_f.round(2)
        formatedAmount = '%.2f' % formatedAmount
      else
        formatedAmount = '%.2f' % 0
      end   
    return ActiveSupport::NumberHelper::number_to_delimited(formatedAmount, :delimiter => Constants::DISPLAY_AMOUNT_SPERATOR)
  end  

  def self.getFormatedDate(date)
      parseDate = DateTime.parse(date)
      foramatedDate = parseDate.strftime("%d-%m-%Y %H:%M:%S")
    return foramatedDate
  end 

  def self.getFormatedDateWithTimeZone(date)
      foramatedDate = DateTime.parse(date).in_time_zone(Time.zone).strftime("%Y-%m-%d %H:%M")
    return foramatedDate
  end  

  def self.getFormatedDateWithoutSec(date)
      parseDate = DateTime.parse(date)
      foramatedDate = parseDate.strftime("%d-%m-%Y %H:%M")
    return foramatedDate
  end  

  def self.getFormatedDateNoTime(date)
      parseDate = DateTime.parse(date)
      foramatedDate = parseDate.strftime("%d-%m-%Y")
    return foramatedDate
  end  

  def self.getMonthBeginDate(month, year)
      monthNumber = (Date::MONTHNAMES.index(month)).to_i
      Log.logger.info("At line  = #{__LINE__} and month number of the month = #{month} is = #{monthNumber}") 

      monthStarting = Date.new(year.to_i, monthNumber.to_i)
      firstDate = Date.parse(monthStarting.to_s)
      beginFromDate = firstDate.beginning_of_day
    return  beginFromDate 
  end  

  def self.getMonthEndDate(month, year)
      monthNumber = (Date::MONTHNAMES.index(month)).to_i

      Log.logger.info("At line  = #{__LINE__} and month number of the month = #{month} is = #{monthNumber}") 

      monthStarting = Date.new(year.to_i, monthNumber.to_i)
      monthEnding = monthStarting.end_of_month

      secondDate = Date.parse(monthEnding.to_s)
      endDate = secondDate.end_of_day
    return  endDate 
  end  

  def self.getTodayBeginDate
      beginDate = Date.today.beginning_of_day
    return beginDate 
  end  

  def self.getTodayEndDate
      endDate = Date.today.end_of_day
    return endDate 
  end

  def self.getCurrentDayNumber
      currentDay = DateTime.now.strftime('%d')
    return currentDay 
  end  

  def self.getCurrentMonthNumber
      currentMonth = DateTime.now.strftime('%m')
    return currentMonth 
  end  

  def self.getCurrentYearNumber
      currentYear  = DateTime.now.strftime('%Y')
    return currentYear 
  end  

  def self.isAgentAccountSuspend(agent)
    if agent.status.eql? Constants::SUSPEND__DB_LABEL
      return Constants::BOOLEAN_TRUE_LABEL
    else
      return Constants::BOOLEAN_FALSE_LABEL  
    end  
  end
    
  def self.getCurrentAgent(agentId)
    if agentId != nil
        agent = User.find(agentId)
      return agent
    else
      return  nil
    end     
  end 

  def self.getCurrentAgentByPhone(phone)
    if phone.present?
        agent = User.find_by_phone(phone)
      return agent
    else
      return  nil
    end     
  end 

  def self.getCurrentAgentFromPhone(phone)
    if phone.present?
        agent = User.where("phone = '#{phone}' AND role = #{Constants::MF_USER_ROLE}").last
      return agent
    else
      return  nil
    end     
  end 

  def self.getUserFromEmail(email)
      user = User.find_by_email(email)
    return user
  end    

  def self.getCurrentAgentAccount(agent)
      agentAccount = agent.user_account
    return agentAccount
  end 

  def self.getUserAccount(user)
      userAccount = user.user_account
    return userAccount
  end 

  def self.getUserGovtAccount(user)
      userGovtAccount = user.user_govt_account
    return userGovtAccount
  end  

  def self.getCurrentAgentLoanAccount(agent)
      agentLoanAccount = agent.loans.last
    return agentLoanAccount
  end  

  def self.getCurrentAgentPenaltyAccount(agent)
      agentPenaltyAccount = agent.agent_penalty_details.last
    return agentPenaltyAccount
  end  

  def self.getCurrentAgentPenaltyAccountByID(agentID)
      agent = User.find(agentID)
      agentPenaltyAccount = agent.agent_penalty_details.last
    return agentPenaltyAccount
  end  

  def self.getCurrentAgentCommissionAccount(agent)
      agentCommissionAccount = agent.agent_commissions.last
    return agentCommissionAccount
  end  

  def self.getUserAccountWithUserType(userID, accountType)
    return UserAccount.where("user_id = #{userID} AND acc_type = '#{accountType}'").first
  end  
  
	def self.getAgentAccountBalance(agent)
			queryResultOfAgentAccount = Utility.executeSQLQuery('SELECT balance FROM user_accounts  WHERE user_id = '+agent.id.to_s)
      balanceGet = queryResultOfAgentAccount[0]["balance"]
    return balanceGet.to_f
	end	

  def self.getAgentGovtAccountBalance(agent)
      queryResultOfAgentGovtAccount = Utility.executeSQLQuery('SELECT balance FROM user_govt_accounts WHERE user_id = '+agent.id.to_s)
      if queryResultOfAgentGovtAccount.present?
        balanceGet = queryResultOfAgentGovtAccount[0]["balance"]
      else
        balanceGet = 0
      end  
    return balanceGet.to_f
  end
  
  def self.getUserAccountBalance(userID)
      queryResultOfAgentAccount = Utility.executeSQLQuery('SELECT balance FROM user_accounts  WHERE user_id = '+userID.to_s)
      balanceGet = queryResultOfAgentAccount[0]["balance"]
    return balanceGet.to_f
  end


  def self.getUserAccountBalanceWithType(userID, accountType)
      queryResultOfAgentAccount = Utility.executeSQLQuery("SELECT balance FROM user_accounts  WHERE user_id = #{userID} AND acc_type = '#{accountType}'")
      balanceGet = queryResultOfAgentAccount[0]["balance"]
    return balanceGet.to_f
  end 

  def self.getAccountBalance(userAccount)
      queryResultOfUserAccount = Utility.executeSQLQuery('SELECT balance FROM user_accounts  WHERE user_id = '+userAccount.user_id.to_s)
      balanceInUserAccount = queryResultOfUserAccount[0]["balance"]
    return balanceInUserAccount.to_f
  end

  def self.getUserGovtAccountBalanceWithID(userID)
      queryResultOfUserAccount = Utility.executeSQLQuery("SELECT balance FROM user_govt_accounts WHERE user_id = #{userID}")
      balanceInUserAccount = queryResultOfUserAccount[0]["balance"]
    return balanceInUserAccount.to_f
  end

  def self.getUserGovtAccountBalance(userAccount)
      queryResultOfUserAccount = Utility.executeSQLQuery('SELECT balance FROM user_govt_accounts WHERE user_id = '+userAccount.user_id.to_s)
      balanceInUserAccount = queryResultOfUserAccount[0]["balance"]
    return balanceInUserAccount.to_f
  end 

  def self.updateUserAccountBalance(userID, amount, isDebit)
      if isDebit
        Utility.executeSQLQuery("UPDATE user_accounts SET balance = balance - #{amount} WHERE user_id = "+userID.to_s)
      else
        Utility.executeSQLQuery("UPDATE user_accounts SET balance = balance + #{amount} WHERE user_id = "+userID.to_s)
      end  
  end 

  def self.updateUserAccountBalanceNew(userID, accountType, amount, isDebit)
      if isDebit
        Utility.executeSQLQuery("UPDATE user_accounts SET balance = balance - #{amount} WHERE user_id = #{userID} AND acc_type = '#{accountType}'")
      else
        Utility.executeSQLQuery("UPDATE user_accounts SET balance = balance + #{amount} WHERE user_id = #{userID} AND acc_type = '#{accountType}'")
      end  
  end  

  def self.updateUserGovtAccountBalance(userID, amount, isDebit)
    if isDebit
      Utility.executeSQLQuery("UPDATE user_govt_accounts SET balance = balance - #{amount} WHERE user_id = "+userID.to_s)
    else
      Utility.executeSQLQuery("UPDATE user_govt_accounts SET balance = balance + #{amount} WHERE user_id = "+userID.to_s)
    end  
  end  

  def self.updateUserGovtAccountBalanceNew(userID, accountType, amount, isDebit)
    if isDebit
      Utility.executeSQLQuery("UPDATE user_govt_accounts SET balance = balance - #{amount} WHERE user_id = #{userID} AND acc_type = '#{accountType}'")
    else
      Utility.executeSQLQuery("UPDATE user_govt_accounts SET balance = balance + #{amount} WHERE user_id = #{userID} AND acc_type = '#{accountType}'")
    end  
  end  

	def self.getAgentLoanBalance(agent)
		if agent.present?
			lastCreditRecord = agent.loans.last
      if lastCreditRecord.present?
  			 loanBalance = lastCreditRecord.balance
  			return TxnHeader.my_money(loanBalance).to_f
      else
        return 0
      end    
		else
			return 0
		end	
	end	

  def self.isAgentLastLoanPending(agent)
    loanBalance = getAgentLoanBalance(agent)
    if loanBalance > 0
      return Constants::BOOLEAN_TRUE_LABEL
    else
      return Constants::BOOLEAN_FALSE_LABEL
    end 
  end

  def self.getAgentLoanDueDate(agent)
      agentMaxCapacity = getAgentMaxLoanCapacity(agent)
      if agentMaxCapacity.to_f < 20000
        dueDate = Time.now + 12.hours
      elsif agentMaxCapacity.to_f >= 20000
        dueDate = Time.now + 12.hours
      end
    return dueDate  
  end

  def self.getAgentLastLoanDueDate(agent)
    loanAccount =  getCurrentAgentLoanAccount(agent)
    if loanAccount.present?
      return loanAccount.due_date
    else
      return nil
    end
  end  

  def self.getAgentMaxLoanCapacity(agent)
    if agent.present?
      agentMaxLoanCapacity = AgentCreditLimit.where(user_id: agent.id).last
      maxLoanCapacity = agentMaxLoanCapacity.loan_amount
      return maxLoanCapacity.to_f
    else
      return 0
    end 
  end 

  def self.isAgentMaxLoanLimitReach(agent,amount)
    agentMaxLoanCapacity = getAgentMaxLoanCapacity(agent)
    if amount.to_f > agentMaxLoanCapacity.to_f
      return Constants::BOOLEAN_TRUE_LABEL
    else
      return Constants::BOOLEAN_FALSE_LABEL
    end 
  end 

	def self.getAgentPenaltyBalance(agent)
		if agent.present?
  			agent_penalty_account = agent.agent_penalty_details.last
        if agent_penalty_account.present?  
          penalty_balance = agent_penalty_account.pending_penalty_amount
        else
          penalty_balance = 0
        end
			return penalty_balance.to_f
		else
			return 0
		end	
	end	

  def self.isAgentPenaltyPending(agent)
    penaltyBalance = getAgentPenaltyBalance(agent)
    if penaltyBalance > 0
      return Constants::BOOLEAN_TRUE_LABEL
    else
      return Constants::BOOLEAN_FALSE_LABEL
    end 
  end 

	def self.getAgentCommissionBalance(agent)
		if agent.present?
  			queryResult = Utility.executeSQLQuery('SELECT post_balance FROM agent_commissions WHERE user_id=\''+agent.id.to_s+'\' ORDER BY id DESC LIMIT 1')
        
        if queryResult.present?
            commission_balance = queryResult[0]["post_balance"]
			    return commission_balance.to_f
        else
          return 0
        end    
		else
			return 0
		end	
	end	

  def self.getAgentCommBalanceByID(agentID)
    if agentID.present?
        queryResult = Utility.executeSQLQuery('SELECT post_balance FROM agent_commissions WHERE user_id=\''+agentID.to_s+'\' ORDER BY id DESC LIMIT 1')
        
        if queryResult.present?
            commission_balance = queryResult[0]["post_balance"]
          return commission_balance.to_f
        else
          return 0
        end    
    else
      return 0
    end 
  end 


  def self.getTotalPayableAmount(accBalance, commissionBalance, govtAccBalance, loanBalance, penaltyBalance)
      totalPayableAmount = TxnHeader.my_money((accBalance.to_f + commissionBalance.to_f + govtAccBalance.to_f)).to_f - TxnHeader.my_money((loanBalance.to_f + penaltyBalance.to_f)).to_f
      totalPayableAmount = TxnHeader.my_money(totalPayableAmount).to_f
      Log.logger.info("At line #{__LINE__} and Total Payable Amount = #{totalPayableAmount}")
    return totalPayableAmount  
  end  

  def self.getCommissionAdmin()
      commission = User.find(ConstantsHelper::COMMISSION_ID)
    return commission  
  end

  def self.getCommissionAdminAccount(commissionAdmin)
      commissionAdminAccount = commissionAdmin.user_account 
    return commissionAdminAccount  
  end
    
  def self.getStockAdmin()
      stockAdminId = ConstantsHelper::OPERATOR_ID
      stock = User.find(stockAdminId)
    return stock  
  end
  
  def self.getStockAdminAccount(stockAdmin)
      stockAdminAccount = stockAdmin.user_account 
    return stockAdminAccount  
  end

  def self.getCashAdmin()
      cashAdminId = ConstantsHelper::OPERATOR_CASH_ACCOUNT_ID
      cash = User.find(cashAdminId)
    return cash  
  end
  
  def self.getCashAdminAccount(cashAdmin)
      cashAdminAccount = cashAdmin.user_account 
    return cashAdminAccount  
  end

  def self.getPenaltyAdmin()
      penaltyAdminId = ConstantsHelper::OPERATOR_PENALTY_ACCOUT_ID
      penalty = User.find(penaltyAdminId)
    return penalty  
  end
  
  def self.getPenaltyAdminAccount(penaltyAdmin)
      penaltyAdminAccount = penaltyAdmin.user_account 
    return penaltyAdminAccount  
  end

  def self.getTaxAdmin
      taxAdmin = User.find(ConstantsHelper::OPERATOR_TAX_ACCOUT_ID)
    return taxAdmin  
  end
  
  def self.getTaxAdminAccount(taxAdmin)
      taxAdminAccount = taxAdmin.user_account 
    return taxAdminAccount  
  end

  def self.getProfitAndLoseAdmin()
      profitAndLoseAdminId = ConstantsHelper::OPERATOR_PROFIT_AND_LOSE_ACCOUNT_ID
      profitAndLose = User.find(profitAndLoseAdminId)
    return profitAndLose  
  end
  
  def self.getProfitAndLoseAdminAccount(profitAndLoseAdmin)
      profitAndLoseAdminAccount = profitAndLoseAdmin.user_account 
    return profitAndLoseAdminAccount  
  end

  def self.getFeeAdmin()
      feeAdminId = ConstantsHelper::OPERATOR_FEE_ACCOUT_ID
      feeAdmin = User.find(feeAdminId)
    return feeAdmin  
  end
  
  def self.getFeeAdminAccount(feeAdmin)
      feeAdminAccount = feeAdmin.user_account 
    return feeAdminAccount  
  end

  def self.getSalesAdmin()
      salesAdminId = ConstantsHelper::OPERATOR_SALES_ACCOUT_ID
      salesAdmin = User.find(salesAdminId)
    return salesAdmin  
  end
  
  def self.getSalesAdminAccount(salesAdmin)
      salesAdminAccount = salesAdmin.user_account 
    return salesAdminAccount  
  end

  def self.getPurchaseAdmin()
      purchaseAdminId = ConstantsHelper::OPERATOR_PURCHASES_ACCOUT_ID
      purchaseAdmin = User.find(purchaseAdminId)
    return purchaseAdmin  
  end
  
  def self.getPurchaseAdminAccount(purchaseAdmin)
      purchaseAdminAccount = purchaseAdmin.user_account 
    return purchaseAdminAccount  
  end

  def self.getGovtBankAdmin()
      govtBankAdminId = ConstantsHelper::OPERATOR_GOVT_BANK_ACCOUT_ID
      govtBankAdmin = User.find(govtBankAdminId)
    return govtBankAdmin  
  end

  def self.getGovtBankAdminAccount(govtBankAdmin)
      govtBankAdminAccount = govtBankAdmin.user_account 
    return govtBankAdminAccount  
  end

  def self.getCommReceivableAdmin
      commRecvAdminId = ConstantsHelper::OPERATOR_COMMISSION_RECEIVABLE_ACCOUT_ID
      commRecvAdmin = User.find(commRecvAdminId)
    return commRecvAdmin  
  end

  def self.getCommReceivableAdminAccount(commRecvAdmin)
      commRecvAdminAccount = commRecvAdmin.user_account 
    return commRecvAdminAccount  
  end

	def self.getLoggedInAdminDetails(current_admin)
		loggedInAdmin = User.find(current_admin.id)
		if loggedInAdmin.present?
			return loggedInAdmin
		else
			return nil
		end	
	end	

	def self.getAgentFullName(agent)
	    if agent.last_name.present? 
	        fullName= agent.name + " "+ agent.last_name
        else
	        fullName= agent.name
	    end
	  return fullName  
	end	

  def self.getAgentFullNameFromPhone(phone)
      agent = User.find_by_phone(phone)
      if agent.present?
        if agent.role.eql? Constants::AGGREGATOR_DB_LABEL
          fullName = agent.business_name
        else  
          if agent.last_name.present? 
              fullName = agent.name + " "+ agent.last_name
            else
              fullName = agent.name
          end
        end  
      else
        fullName = ""
      end  
    return fullName  
  end 

	def self.getAgentPhone(agent)
		if agent.present?
			return agent.phone
		else
			return ""
		end	
	end	

  def self.getAgentGender(agent)
    if agent.present?
      if agent.gender.eql? Constants::MALE_DB_VALUE_LABEL
        return Constants::MALE_VIEW_VALUE_LABEL
      else
        return Constants::FEMALE_VIEW_VALUE_LABEL
      end  
    else
      return ""
    end 
  end 

  def self.getAgentImage(agent)
    if agent.present?
      if agent.image.present?
        return agent.image
      else
        return "user.png"
      end    
    else
      return "user.png"
    end
  end    

	def self.getTxnDescription(txnObj)
		  if txnObj.service_type == Constants::USER_CASH_PAYMENT_LABLE ||  txnObj.service_type == Constants::USER_CASH_DEPOSIT_LABLE
        description = txnObj.external_reference
      else
        description = txnObj.description
      end 
    return description     
	end	

  def self.getColorValue(subType,description)
      if subType.eql? Constants::USER_LOAN_REQUEST_VIEW_LABLE
        
        colorValue = Constants::DEDUCT_COLOR
      
      elsif subType == Constants::USER_CASH_DEPOSIT_VIEW_LABLE || subType == Constants::USER_BANK_DEPOSIT_VIEW_LABLE  || subType == Constants::USER_WALLET_DEPOSIT_VIEW_LABLE
      
        colorValue = Constants::CREDIT_COLOR
      
      elsif subType == Constants::USER_COM_PAYMENT_VIEW_LABLE && description == (I18n.t :COMMISSION_REIMBURSEMENT_DESC)
      
        colorValue = Constants::DEDUCT_COLOR
      
      elsif subType == Constants::USER_LOAN_DEPOSIT_VIEW_LABLE && description == (I18n.t :LOAN_SETTLE_REIMBURSEMENT_DESC)
      
        colorValue = Constants::DEDUCT_COLOR
      
      elsif subType == Constants::USER_NON_PAYMENT_LOAN_VIEW_LABLE
      
        colorValue = Constants::DEDUCT_COLOR
      
      elsif subType == Constants::USER_PENALTY_CHARGE_VIEW_LABLE && description == (I18n.t :PENALTY_SETTLE_REIMBURSEMENT_DESC)
      
        colorValue = Constants::DEDUCT_COLOR
      
      elsif subType == Constants::USER_NON_PAYMENT_PENALTY_VIEW_LABLE
      
        colorValue = Constants::DEDUCT_COLOR 
      
     elsif subType == Constants::GOVT_FUNDS_WITHDRAWAL_VIEW_LABEL
      
        colorValue = Constants::DEDUCT_COLOR 

      elsif subType == Constants::USER_CASH_PAYMENT_VIEW_LABLE || subType == Constants::USER_BANK_PAYMENT_VIEW_LABLE  || subType == Constants::USER_WALLET_PAYMENT_VIEW_LABLE
      
        colorValue = Constants::DEDUCT_COLOR 
      
      else         
      
        colorValue = Constants::NO_COLOR
      
      end 
    return colorValue  
  end 

  def self.getServiceTypeDisplayValue(dbServiceType)
    if dbServiceType.eql? Constants::USER_LOAN_REQUEST_LABLE
    
      return Constants::USER_LOAN_REQUEST_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::USER_CASH_DEPOSIT_LABLE

      return Constants::USER_CASH_DEPOSIT_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::USER_LOAN_DEPOSIT_LABLE

      return Constants::USER_LOAN_DEPOSIT_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::MTOPUP_LABEL
     
      return Constants::AIRTIME_LABEL
    
    elsif dbServiceType.eql? Constants::USER_PENALTY_CHARGE_LABLE
    
      return Constants::USER_PENALTY_CHARGE_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::USER_CASH_PAYMENT_LABLE
      
      return Constants::USER_CASH_PAYMENT_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::USER_COMMISSION_PAYMENT_LABLE
    
      return Constants::USER_COM_PAYMENT_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::USER_NON_PAYMENT_LOAN_LABLE
      
      return Constants::USER_NON_PAYMENT_LOAN_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::USER_NON_PAYMENT_PENALTY_LABLE
     
      return Constants::USER_NON_PAYMENT_PENALTY_VIEW_LABLE
    
    elsif dbServiceType.eql? Constants::AGGREGATOR_DEPOSIT_DB_LABEL
    
      return Constants::AGGREGATOR_DEPOSIT_VIEW_LABEL
   
    elsif dbServiceType.eql? Constants::MTOP_UP_SALES_LABEL
    
      return Constants::MTOP_UP_SALES_VIEW_LABEL
    
    elsif dbServiceType.eql? Constants::ELECTRICITY_SALE_LABEL
    
      return Constants::ELECTRICITY_SALE_VIEW_LABEL
   
    elsif dbServiceType.eql? Constants::DTH_SALE_LABEL
    
      return Constants::DTH_SALE_VIEW_LABEL

    elsif dbServiceType.eql? UgandaConstants::NATIONAL_WATER_BILL_LABEL
      
      return UgandaConstants::NATIONAL_WATER_BILL_VIEW_LABEL   

    elsif dbServiceType.eql? UgandaConstants::NATIONAL_WATER_BILL_SALES_LABEL
      
      return UgandaConstants::NATIONAL_WATER_BILL_SALES_VIEW_LABEL    

    elsif dbServiceType.eql? Constants::INTERNET_SALE_LABEL
      
      return Constants::INTERNET_SALE_VIEW_LABEL       

    elsif dbServiceType.eql? Constants::LEASING_FEE_DB_LABEL
        
      return Constants::LEASING_FEE_VIEW_LABEL

    elsif dbServiceType.eql? Constants::USER_GOVT_CASH_DEPOSIT_LABLE
        
      return Constants::USER_GOVT_CASH_DEPOSIT_VIEW_LABLE  

    elsif dbServiceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
        
      return Constants::IREMBO_TAX_PAYMENT_VIEW_LABEL  

    elsif dbServiceType.eql? Constants::GOVT_FUNDS_WITHDRAWAL_DB_LABEL
        
      return Constants::GOVT_FUNDS_WITHDRAWAL_VIEW_LABEL  
     
    else
    
      return dbServiceType
    
    end 
  end  


  def self.getTxnhisSubtypeDetails(txnObj)

    LabelsDetail.subTypeViewModuleFirst(txnObj)

  end

  def self.checkRollBackTxnStatus(txnID)
    txnHeader = TxnHeader.find(txnID)
    if txnHeader.is_rollback == Constants::TXN_APPROVED_ROLLBACK_STATUS
       return Constants::TXN_APPROVED_LABEL 
    elsif txnHeader.is_rollback == Constants::TXN_REJECTED_ROLLBACK_STATUS
      return Constants::TXN_REJECT_LABEL
    else
      return Constants::TXN_INITATE_LABEL
    end  
  end  
  
  def self.getStatusDisplayValue(status)
    if status.eql? Constants::ACTIVE_DB_LABEL
      return Constants::ACTIVE_VIEW_LABEL
    elsif status.eql? Constants::SUSPEND__DB_LABEL
      return Constants::SUSPEND__VIEW_LABEL
    elsif status.eql? Constants::DELETE__DB_LABEL
      return Constants::DELETE__VIEW_LABEL
    else
      return status
    end    
  end  

  def self.getStatusColorValue(status)
    if status.eql? Constants::ACTIVE_DB_LABEL
      return Constants::CREDIT_COLOR
    elsif status.eql? Constants::SUSPEND__DB_LABEL
      return Constants::DEDUCT_COLOR
    else status.eql? Constants::DELETE__DB_LABEL
      return Constants::NO_COLOR
    end    
  end  

  def self.sendNotification(user_id, notified, title)
   	begin
      Log.logger.info("At line #{__LINE__} and inside the Utility class and sendNotification method is called with userID is = #{user_id} and messgae is = #{notified}")
 
      NotificationNew.create(user_id: user_id, notification_content: notified, notification_title: title)
      
      gcmKey =[]
      agentGCM = SerialNumber.where("user_id = #{user_id}").first
      if agentGCM.present?
        gcmKey.push(agentGCM.gcm_key.to_s)
      end

      notification_count = NotificationNew.where(read_flag: 0).where(user_id: user_id).count
      Log.logger.info("At line #{__LINE__} and notification count is of the current agent is = #{notification_count}")

      fcmObj = FCM.new(Properties::GCM_KEY)
      response = fcmObj.send(gcmKey,{ 
        data:{
          :title => notified,
          :body => notified,
          :user_id => user_id,
          :tag => notified,
          :count => notification_count,
          :type => title,
          :largeIcon => "app_icon",
          :smallIcon => "app_icon"
        }
      })
      Log.logger.info("At line = #{__LINE__} and response of the notification is = \n #{response}") 
   	rescue => exception 
      Log.logger.info("At line = #{__LINE__} and Exception message is = \n #{exception.message}")
      Log.logger.info("At line = #{__LINE__} and full Exception is = \n #{exception.backtrace.join("\n")}")
    end
  end 

  def self.getSingleAgentTxnDetails(userId,fromDate,toDate,isServiceTypeAvailable,isSubTypeAvailable)
  	if isServiceTypeAvailable != nil && isSubTypeAvailable != nil
  		query = 'SELECT i.user_id, h.txn_date, h.service_type, h.sub_type,h.account_id,h.amount,i.previous_balance, i.post_balance,h.description
               	FROM txn_items i,  txn_headers h WHERE h.id=i.header_id AND i.user_id=\''+userId.to_s+'\'
               	AND h.txn_date >=\''+fromDate.to_s+'\' AND h.txn_date <= \''+toDate.to_s+'\'
               	AND h.service_type = \''+isServiceTypeAvailable.to_s+'\' AND h.sub_type = \''+isSubTypeAvailable.to_s+'\'
               	ORDER BY h.txn_date DESC;'
  	elsif isServiceTypeAvailable != nil && isSubTypeAvailable == nil
  		query = 'SELECT i.user_id, h.txn_date, h.service_type, h.sub_type,h.account_id,h.amount,i.previous_balance, i.post_balance,h.description
               	FROM txn_items i,  txn_headers h WHERE h.id=i.header_id AND i.user_id=\''+userId.to_s+'\'
               	AND h.txn_date >=\''+fromDate.to_s+'\' AND h.txn_date <= \''+toDate.to_s+'\'
               	AND h.service_type = \''+isServiceTypeAvailable.to_s+'\'
               	ORDER BY h.txn_date DESC;'
     	else
          query = 'SELECT i.user_id, h.txn_date, h.service_type, h.sub_type,h.account_id,h.amount,i.previous_balance, i.post_balance,h.description
               	FROM txn_items i,  txn_headers h WHERE h.id=i.header_id AND i.user_id=\''+userId.to_s+'\'
               	AND h.txn_date >=\''+fromDate.to_s+'\' AND h.txn_date <= \''+toDate.to_s+'\'
               	ORDER BY h.txn_date DESC;'     	
  	end	

  	list = Utility.executeSQLQuery(query)
  	return list
  end	

  def self.sendMsgEmailToAdmin(from, to, subject, message)
    Log.logger.info("Inside the Utility class at line =#{__LINE__} and sendMsgEmailToAdmin method is called = #{message}")
      Thread.new do
        action = ActionController::Base.new()
        htmlContent = action.render_to_string(:template => "layouts/mail.html.erb", locals: { :@message => message})
        ActionMailer::Base.mail(
            :content_type => 'text/html',
            :from => from, 
            :to => to, 
            :subject => subject, 
            :body => htmlContent).deliver_now
      end   
    Log.logger.info("Email has been successfully sent to the #{to}")
  end  

  def self.sendEmail(from, to, subject, message)
    Log.logger.info("Inside the Utility class at line =#{__LINE__} and send email funciton is called")
      Thread.new do
        action = ActionController::Base.new()
        htmlContent = action.render_to_string(:template => "layouts/mail.html.erb", locals: { :@message => message})
        ActionMailer::Base.mail(
            :content_type => 'text/html',
            :from => from, 
            :to => to, 
            :subject => subject, 
            :body => htmlContent).deliver_now
      end  
    Log.logger.info("Email has been successfully sent to the #{to}")
  end  

  def self.hasServiceAccess(serviceName, userID)
    hasAccess = false
      userAssignedRole = RoleHistory.where("user_id = #{userID}")
      userAssignedRole.each do |assignRole|
        if assignRole.role_id == Constants::SUPER_ADMIN_ROLE_ID
            hasAccess = true
          break
        else  
          role = Role.find(assignRole.role_id)
          if role.role_name.eql? serviceName
              hasAccess = true
            break
          end    
        end  
      end  
    return hasAccess
  end  

  def self.getHeader(approvedAmount, serviceType, subType, agentId, txnStatus, description, extRef)
    Log.logger.info("At line #{__LINE__} inside the Utility class and getHeader methos is called")
    Log.logger.info("At line #{__LINE__} serviceType = #{serviceType}, subType = #{subType}, approvedAmount = #{approvedAmount}, agentId = #{agentId}, txnStatus = #{txnStatus}, description = #{description} and extRef = #{extRef}")
   
    txnHeader = TxnHeader.new
      txnHeader.service_type = serviceType
      txnHeader.sub_type =  subType
      txnHeader.txn_date = Time.now
      txnHeader.currency_code = Utility.getCurrencyLabel
      txnHeader.amount = TxnHeader.my_money(approvedAmount).to_f
      txnHeader.agent_id = agentId
      txnHeader.status = txnStatus
      txnHeader.gateway = Constants::TXN_GATEWAY
      txnHeader.description = description
      txnHeader.external_reference = extRef
    return txnHeader
  end

  def self.getTxnItem(userID, amount, isDebit, userAccountID, isRollback, feeAmount)
   
      Log.logger.info("The Utility class's method getTxnItem is called at line = #{__LINE__}")
   
      txnItem = TxnItem.new
      
      txnItem.user_id = userID 

      previousBalance = Utility.getUserAccountBalance(userID).to_f

      Log.logger.info("At line = #{__LINE__} account balance of the user id = #{userID} is = #{previousBalance}")

      if isDebit == false
        txnItem.debit_amount = nil
        txnItem.credit_amount = TxnHeader.my_money(amount).to_f
        txnItem.role = Constants::PAYEE_LABEL
        if isRollback
          txnItem.post_balance = TxnHeader.my_money(previousBalance).to_f
        else
          txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)  # agent_user_account balance + amount
        end  
      else
        txnItem.debit_amount = TxnHeader.my_money(amount).to_f
        txnItem.credit_amount = nil
        txnItem.role = Constants::PAYER_LABEL
        if isRollback
          txnItem.post_balance = TxnHeader.my_money(previousBalance).to_f
        else
          txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)  # agent_user_account balance + amount
        end
      end

      txnItem.account_id = userAccountID
      txnItem.fee_id = nil
      txnItem.fee_amt =  TxnHeader.my_money(feeAmount).to_f
      txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f  # agent_user_account balance
    
    return txnItem
  
  end


  def self.getTxnItemNew(whichUser, amount, isDebit, userAccount)
    
    Log.logger.info("At line = #{__LINE__} inside the Utility's method getTxnItem")
      
      txnItem = TxnItem.new
     
        txnItem.user_id = whichUser.id

        previousBalance = Utility.getAgentAccountBalance(whichUser).to_f

        if isDebit == false
          txnItem.debit_amount = nil
          txnItem.credit_amount = TxnHeader.my_money(amount)
          txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)
          txnItem.role = Constants::PAYEE_LABEL
        else
          txnItem.debit_amount = TxnHeader.my_money(amount)
          txnItem.credit_amount = nil
          txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)
          txnItem.role = Constants::PAYER_LABEL
        end
        txnItem.account_id = userAccount.id
        txnItem.fee_id = nil
        txnItem.fee_amt =  0
        txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f 

      return txnItem
  end


  def self.getAgentLoanRecord(userID, amount, isDeduct)
    
    Log.logger.info("Inside the Utility Class's get agent Loan record method at line #{__LINE__} and settle the account of user #{userID} and amount is = #{amount}")
      
      currentAgent = User.find(userID)
      lastLoanRecord = getCurrentAgentLoanAccount(currentAgent)

      loan = Loan.new
      loan.user_id = userID
      loan.acc_type = Constants::USER_ACCOUNT_TYPE
      loan.currency = Constants::USER_ACCOUNT_CURRENCY
      if isDeduct
        loan.amount = TxnHeader.my_money(TxnHeader.my_money(amount).to_f - (TxnHeader.my_money(amount).to_f + TxnHeader.my_money(amount).to_f)).to_f
        loan.balance = TxnHeader.my_money(TxnHeader.my_money(lastLoanRecord.balance).to_f - TxnHeader.my_money(amount).to_f)
      else
        loan.amount = TxnHeader.my_money(amount).to_f
        loan.balance = TxnHeader.my_money(TxnHeader.my_money(lastLoanRecord.balance).to_f + TxnHeader.my_money(amount).to_f)
      end  
      loan.due_date = lastLoanRecord.due_date

    return loan
  end

  def self.addDataIntoAccessLogs(userID, userRole, useWebStatus, request)
    accessID  = AccessLog.getAccessID
      accessLog = AccessLog.new
        accessLog.access_id = accessID
        accessLog.user_id = userID
        accessLog.role = userRole
        accessLog.is_successful = Constants::FAILED_LOGIN_STATUS
        accessLog.remote_ip = Utility.getUserRemoteIP(request)
        accessLog.use_web = useWebStatus
      accessLog.save         
    return accessID  
  end 

  def self.updateAccessLoginStatus(accessID)
    Log.logger.info("At line = #{__LINE__} and accessID is = #{accessID}")
    accessLog = AccessLog.find_by_access_id(accessID)
    if accessLog.present?
      accessLog.update_attribute(:is_successful, Constants::SUCCESS_LOGIN_STATUS)
    end  
  end

  def self.getTagValue(data, tagName)
      firstArray = data.split("<#{tagName}>", 2)
      secondArray = firstArray[1].split("</#{tagName}>", 2)
      tagValue = secondArray[0]
    return tagValue 
  end

  def self.getCurrencyLabel
    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      return Constants::RWANDA_CURRENCY_LABEL
    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
      return UgandaConstants::UGANDA_CURRENCY_LABEL
    end  
  end  

  def self.getSQLQueryServiceTypes

    serviceTypeList = Constants::BLANK_VALUE
    
    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      
      for i in 0..Constants::DASHBOARD_AVAILABLE_SERVICES.size-1
        if i < Constants::DASHBOARD_AVAILABLE_SERVICES.size-1
          commaValue = ","
        else
          commaValue = Constants::BLANK_VALUE
        end  
        serviceTypeList = serviceTypeList.to_s + "'#{Constants::DASHBOARD_AVAILABLE_SERVICES[i]}'" + commaValue
      end 

    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]

      for i in 0..UgandaConstants::DASHBOARD_AVAILABLE_SERVICES.size-1
        if i < UgandaConstants::DASHBOARD_AVAILABLE_SERVICES.size-1
          commaValue = ","
        else
          commaValue = Constants::BLANK_VALUE
        end  
        serviceTypeList = serviceTypeList.to_s + "'#{UgandaConstants::DASHBOARD_AVAILABLE_SERVICES[i]}'" + commaValue
      end 

    end 

    return serviceTypeList   

  end  

  def self.getSQLQueryAirtimeSubTypes

    subTypeList = Constants::BLANK_VALUE
    
    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      
      for i in 0..Constants::AIRTIME_SUB_TYPES_LIST.size-1
        if i < Constants::AIRTIME_SUB_TYPES_LIST.size-1
          commaValue = ","
        else
          commaValue = Constants::BLANK_VALUE
        end  
        subTypeList = subTypeList.to_s + "'#{Constants::AIRTIME_SUB_TYPES_LIST[i]}'" + commaValue
      end 

    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]

      for i in 0..UgandaConstants::AIRTIME_SUB_TYPES_LIST.size-1
        if i < UgandaConstants::AIRTIME_SUB_TYPES_LIST.size-1
          commaValue = ","
        else
          commaValue = Constants::BLANK_VALUE
        end  
        subTypeList = subTypeList.to_s + "'#{UgandaConstants::AIRTIME_SUB_TYPES_LIST[i]}'" + commaValue
      end 

    end 

    return subTypeList   
  
  end  

  def self.getSQLQueryServiceTypesExclude(excludedServiceType)

    serviceTypeList = Constants::BLANK_VALUE
    
    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      
      for i in 0..Constants::DASHBOARD_AVAILABLE_SERVICES.size-1
        if i < Constants::DASHBOARD_AVAILABLE_SERVICES.size-1
          commaValue = ","
        else
          commaValue = Constants::BLANK_VALUE
        end  
        if !(Constants::DASHBOARD_AVAILABLE_SERVICES[i].eql? excludedServiceType)
          serviceTypeList = serviceTypeList.to_s + "'#{Constants::DASHBOARD_AVAILABLE_SERVICES[i]}'" + commaValue
        end  
      end 

    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]

      for i in 0..UgandaConstants::DASHBOARD_AVAILABLE_SERVICES.size-1
        if i < UgandaConstants::DASHBOARD_AVAILABLE_SERVICES.size-1
          commaValue = ","
        else
          commaValue = Constants::BLANK_VALUE
        end 
        if !(UgandaConstants::DASHBOARD_AVAILABLE_SERVICES[i].eql? excludedServiceType)
          serviceTypeList = serviceTypeList.to_s + "'#{UgandaConstants::DASHBOARD_AVAILABLE_SERVICES[i]}'" + commaValue
        end  
      end 

    end 

    return serviceTypeList   

  end 

  def self.getYesNoLabel(inteagerValue)
    if inteagerValue == Constants::MF_PENALTY_ACTIVE_STATUS  
      return Constants::YES_LABEL
    else
      return Constants::NO_LABEL
    end    
  end  

  def self.getDefaultDateArray
      dateArray = Array.new
      dateArray.push(Date.today.to_s)
    return dateArray
  end 
  
  def self.getServiceTypes
    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      return Constants::DASHBOARD_AVAILABLE_SERVICES
    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]  
      return UgandaConstants::DASHBOARD_AVAILABLE_SERVICES
    end
  end 

  def self.encryptedData(plainText)
      cipher = OpenSSL::Cipher::Cipher.new('DES-EDE3')
      cipher.encrypt()
      crypt = cipher.update(plainText) + cipher.final()
      encryptedData = Base64.encode64(crypt)
    return encryptedData  
  end 

  def self.decryptedData(encryptedData)
      cipher = OpenSSL::Cipher::Cipher.new('DES-EDE3')
      cipher.decrypt()
      decodedkey = Base64.decode64(encryptedData)
      decryptedData = cipher.update(decodedkey)
      decryptedData << cipher.final()
    return decryptedData
  end

  def self.generateHashData(data, secretKey)
      digest = OpenSSL::Digest.new('SHA512')
      hashData = OpenSSL::HMAC.hexdigest(digest, secretKey, data)
    return hashData  
  end 

  def self.generateHashDataWithAlgo(data, secretKey, algorithm)
      digest = OpenSSL::Digest.new(algorithm)
      hashData = OpenSSL::HMAC.hexdigest(digest, secretKey, data)
    return hashData  
  end 

  def self.isValidYear(date)
    selectedYear = Date.parse(date).year.to_s
    if selectedYear.length.to_i == 4
      if selectedYear.chars.first.to_i != 2 && selectedYear.chars.first.to_i != 1
        return Constants::BOOLEAN_FALSE_LABEL
      else
        return Constants::BOOLEAN_TRUE_LABEL
      end  
    else  
      return Constants::BOOLEAN_FALSE_LABEL
    end  
  end 

  def self.getActualDateTime(date, time)
      dateTime =  date.to_s + " " + time.to_s
      actualDateTime = Time.parse(dateTime)
    return actualDateTime  
  end 

  def self.getActualTime(dateTime)
      dateTime = DateTime.parse(dateTime)
    return  dateTime.hour.to_s + " : " +  dateTime.minute.to_s + " : " +  dateTime.second.to_s 
  end  

  def self.getCurrentActualTime
      dateTime = DateTime.now
    return  dateTime.hour.to_s + " : " +  dateTime.minute.to_s + " : " +  dateTime.second.to_s 
  end  

  def self.getDefaultKioskModel
    if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
      return Constants::RWANDA_DEFAULT_KIOSK_MODEL
    elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
      return UgandaConstants::UGANDA_DEFAULT_KIOSK_MODEL
    end  
  end  

  def self.convertToTenDigit(number)
      numberLength = number.length
      appendUpTo = 10 - numberLength.to_i
      actualNumber = number
      for i in 0..appendUpTo.to_i - 1
        actualNumber = "0" + actualNumber.to_s
      end  
    return actualNumber
  end 


  def self.isMatchWithAnyOperatorID(userID)
    operatorList = Array.new
    operatorList.push(ConstantsHelper::ANONYMOUS_USER_ID)
    operatorList.push(ConstantsHelper::OPERATOR_ID)
    operatorList.push(ConstantsHelper::COMMISSION_ID)
    operatorList.push(ConstantsHelper::OPERATOR_CASH_ACCOUNT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_PENALTY_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_PROFIT_AND_LOSE_ACCOUNT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_TAX_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_GOVT_TAX_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_FEE_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_SALES_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_PURCHASES_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_GOVT_BANK_ACCOUT_ID)
    operatorList.push(ConstantsHelper::OPERATOR_COMMISSION_RECEIVABLE_ACCOUT_ID)

    if operatorList.include? userID
      return Constants::BOOLEAN_TRUE_LABEL
    else
      return Constants::BOOLEAN_FALSE_LABEL
    end    
  end  

end