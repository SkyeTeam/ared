# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 37 }
# :reek:DuplicateMethodCall { max_calls: 9 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class AppLoginManager

	def self.doLogin(currentAgent, params)
		AppLog.logger.info("Inside the App Login Manager class and doLogin method is called at line #{__LINE__}")
	
		appVersion 		  = params[""+AppConstants::APP_VERSION_PARAM_LABEL+""]
        phoneSerialNumber = params[""+AppConstants::SERIAL_NUMBER_PARAM_LABEL+""]
        phoneGCMKey	   	  = params[""+AppConstants::GCM_KEY_PARAM_LABEL+""]

        AppLog.logger.info("At line = #{__LINE__} and requested parameters are currentAgent ID is = #{currentAgent.id}, appVersion = #{appVersion}, phoneSerialNumber = #{phoneSerialNumber} and phoneGCMKey = #{phoneGCMKey}")

		response = validate(currentAgent, appVersion, phoneSerialNumber, phoneGCMKey)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
	        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    			response.Response_Msg=("") 
  			return response  
        end
	end	


	def self.validate(currentAgent, appVersion, phoneSerialNumber, phoneGCMKey)
      
    	response = Response.new

        unless currentAgent.present?
                AppLog.logger.info("Inside the MobileRechargeManager's  validate method at line #{__LINE__} and agent is not present")
                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
            return response
        end

        #First we check that the IMEI number of the phone is present or not
        if phoneSerialNumber.eql? Constants::BLANK_VALUE
                AppLog.logger.info("At line = #{__LINE__} and phone IMEI Number is missing")
                response.Response_Code=(Error::PHONE_SERIAL_NUMBER_MISSING)
                response.Response_Msg=(I18n.t :PHONE_SERIAL_NUMBER_MISSING_MSG) 
            return response
    	end

		#Now we check that the GCM Key of the phone is present or not
        if phoneGCMKey.eql? Constants::BLANK_VALUE
                AppLog.logger.info("At line = #{__LINE__} and phone GCM Key is missing")
                response.Response_Code=(Error::GCM_KEY_MISSING)
                response.Response_Msg=(I18n.t :GCM_KEY_MISSING_MSG) 
            return response
    	end

    	#Now we check that the App version is present or not
        if appVersion.eql? Constants::BLANK_VALUE or !(appVersion.eql? Properties::APP_VERSION)
                AppLog.logger.info("At line = #{__LINE__} and App version is may be missing or old")
                response.Response_Code=(Error::APP_VERISON_MISSING)
                response.Response_Msg=(I18n.t :APP_VERISON_MISSING_MSG) 
            return response
    	end

    	#Now we check that this serial number is exit to any other agent
    	checkExistingSerial = SerialNumber.where('serial_number = ?', phoneSerialNumber).where('user_id != ?', currentAgent.id).where('active_flag = ?', AppConstants::DEVICE_ACTIVE_FLAG_VALUE)
    	AppLog.logger.info("At line = #{__LINE__} and now we check that this device is registered with any other agent or not = #{checkExistingSerial.present?}")
    	if checkExistingSerial.present?
    			response.Response_Code=(Error::DEVICE_ALREADY_REG)
                response.Response_Msg=(I18n.t :DEVICE_ALREADY_REG_MSG) 
            return response
    	end	

    		response = checkDeviceStatus(currentAgent, phoneSerialNumber, phoneGCMKey)
    	return response	

    end

    def self.checkDeviceStatus(currentAgent, phoneSerialNumber, phoneGCMKey)
    	
    	response = Response.new

    	#Now we check that this device is active or not
    	checkDeviceActive = SerialNumber.where('user_id = ?', currentAgent.id).first
    	if checkDeviceActive.present?
    		if checkDeviceActive.serial_number.eql? phoneSerialNumber
        		if checkDeviceActive.active_flag != AppConstants::DEVICE_ACTIVE_FLAG_VALUE
        				AppLog.logger.info("At line = #{__LINE__} and this device is not active")
        				response.Response_Code=(Error::DEVICE_NOT_ACTIVE)
                    	response.Response_Msg=(I18n.t :DEVICE_NOT_ACTIVE_MSG) 
                	return response
        		else
        				#Successfully Login
        				unless checkDeviceActive.gcm_key.present?
        					checkDeviceActive.update_attribute(:gcm_key, phoneGCMKey) # This is for the old agents
        					AppLog.logger.info("At line = #{__LINE__} and gcm key is update Successfully")
        				end	
        				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    					response.Response_Msg=("") 
  					return response  
        		end	
        	else
        			AppLog.logger.info("At line = #{__LINE__} and agent want to try to login with new device")
	        		isNewEntry = true
	        		allSerialNoList = SerialNumber.where('user_id = ?', currentAgent.id)
	        		allSerialNoList.each do |serialNo|
	        			if serialNo.serial_number.eql? phoneSerialNumber
	        				isNewEntry = false
	        			end	
	        		end		

	        		if isNewEntry
	        			SerialNumber.create(user_id: currentAgent.id, serial_number: phoneSerialNumber, gcm_key: phoneGCMKey)
	        		end	
	        		response.Response_Code=(Error::DEVICE_NOT_REG)
                    response.Response_Msg=(I18n.t :DEVICE_NOT_REG_MSG) 
                return response
        	end	
    	else	
    			#This entry for the those agent who try to login first time
    			SerialNumber.create(user_id: currentAgent.id, serial_number: phoneSerialNumber, gcm_key: phoneGCMKey, active_flag: AppConstants::DEVICE_ACTIVE_FLAG_VALUE)
	        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    			response.Response_Msg=("") 
  			return response  
    	end
    end	

end	