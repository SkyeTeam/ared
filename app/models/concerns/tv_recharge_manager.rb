# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class TvRechargeManager

  	def self.doTVRecharge(agent, params, request)
		AppLog.logger.info("Inside the TV Recharges Manager and doTVRecharge method is called at line #{__LINE__}")

		clientMobileNo = params[""+AppConstants::CLIENT_MOBILE_NO_PARAM_LABEL+""]
        clientSmartCardNo = params[""+AppConstants::CLIENT_SMART_CARD_NO_PARAM_LABEL+""]
        rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
        customerFee = params[""+AppConstants::CLIENT_CHARGES_PARAM_LABEL+""]
        appTxnUniqueID = params[""+AppConstants::TXN_ID_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} and parameters are clientMobileNo = #{clientMobileNo}, clientSmartCardNo = #{clientSmartCardNo}, rechargeAmount = #{rechargeAmount}, customerFee = #{customerFee} and appTxnUniqueID = #{appTxnUniqueID}")
       
        response = validate(agent, clientMobileNo, clientSmartCardNo, rechargeAmount, customerFee, appTxnUniqueID)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
    	AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{agent.id}, agent Phone = #{agent.phone} and name is = #{Utility.getAgentFullName(agent)}")
		 
        calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(Constants::FDI_DTH_LABEL, Constants::FDI_DTH_LABEL, rechargeAmount)
        if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return calculatedResponse
        end

        AppUtility.savePendingRequest(appTxnUniqueID, Constants::FDI_DTH_LABEL, Constants::FDI_DTH_LABEL, clientMobileNo, rechargeAmount, agent.id, Utility.getUserRemoteIP(request), nil, Constants::MF_USER_ROLE)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the pending request table before processing successfully")
       
        gatewayLogID = GatewayLog.getGatewayLogID
        aggregatorName = AppUtility.getAggregatorName(Constants::FDI_DTH_LABEL)
        GatewayLog.saveGatewayLog(gatewayLogID, agent.id, nil, aggregatorName, nil, rechargeAmount, nil, nil)
        AppLog.logger.info("At line #{__LINE__} and add one entry in the gateway log table before processing with log ID = #{gatewayLogID}")

        externalReference = ServiceFee.getTigoRequestExtRef()
        
        if Properties::IS_PRODUCTION_SERVER    
            processingResponse = sendTvRechargeRequest(rechargeAmount, clientSmartCardNo, externalReference, clientMobileNo, gatewayLogID)
        else 
            processingResponse = getRowData
        end 
        AppLog.logger.info("At line #{__LINE__} and response code after hit to URL is = #{processingResponse.Response_Code}. (if it is 200 then saved into database)") 

      	if processingResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
      			AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
	            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of this transaction id #{appTxnUniqueID}")
	        return processingResponse 
      	else
	        requestParams = setRequestParams(rechargeAmount, customerFee, clientSmartCardNo, clientMobileNo, externalReference)
            return AppUtility.saveRequest(calculatedResponse, agent, processingResponse, requestParams, appTxnUniqueID, gatewayLogID, aggregatorName, request)
      	end
	end	

    def self.setRequestParams(rechargeAmount, customerFee, clientSmartCardNo, clientMobileNo, externalReference)
        requestParams = RequestParams.new
            requestParams.serviceType=(Constants::FDI_DTH_LABEL)
            requestParams.operator=(Constants::FDI_DTH_LABEL)
            requestParams.rechargeAmount=(rechargeAmount)
            requestParams.customerFee=(customerFee)
            requestParams.clientSmartCardNo=(clientSmartCardNo)
            requestParams.clientMobileNo=(clientMobileNo)
            requestParams.description=(I18n.t :TV_REQUEST_DES)
            requestParams.serviceTxnID=(externalReference)
        return requestParams        
    end   

    def self.getRowData
        response = Response.new
            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("")
        return response  
    end  

    def self.sendTvRechargeRequest(rechargeAmount, clientSmartCardNo, externalReference, clientMobileNo, gatewayLogID)
        response = Response.new
        
        begin
            AppLog.logger.info("At line #{__LINE__} inside the TvRechargeManager class and sendTvRechargeRequest method is called")

            if Properties::IS_SIMULATOR_ENABLE
                responseData = SimulatorUtility.sendFDIRechargeRequest('startimes_tv', clientMobileNo, rechargeAmount, externalReference, Utility.getCurrentDateTime, nil, clientSmartCardNo, gatewayLogID)
            else    
                responseData = FdiUtility.sendRechargeStarTimesRequest(clientSmartCardNo, rechargeAmount, externalReference, clientMobileNo, Utility.getCurrentDateTime, gatewayLogID)
            end    

            GatewayLog.updateResponseData(gatewayLogID, responseData)
            AppLog.logger.info("At line of response come from the FDI Server  is = \n #{responseData}")

            isJSONValid = TxnManager.valid_json?(responseData) 
            AppLog.logger.info("At line #{__LINE__} and now check that whether the response is come in a valid JSON or not = #{isJSONValid}")
        
            if isJSONValid
                data = JSON.parse(responseData)
                statusCode = data['status_code']
                statusMsg = data['status_msg']
                AppLog.logger.info("At line #{__LINE__} response code is = #{statusCode} and response message is = #{statusMsg}")

                if statusCode.to_i == AppConstants::FDI_SUCCESS_RESPONSE_CODE_VALUE
                        firstSplit = statusMsg["Your balance is: "]
                        balFirst = statusMsg.split(firstSplit,2)  
                        
                        serviceBalance = balFirst[1]
                        AppLog.logger.info("At line #{__LINE__} and balance of the DTH service is = #{serviceBalance}")
                      
                        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                        response.Response_Msg=(statusMsg)
                        response.serviceBalance=(serviceBalance)
                        response.thirdPartyResponseID=(nil)
                    return response   
                else
                        response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                        response.Response_Msg=(statusMsg)
                    return response 
                end
            else
                    response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                    response.Response_Msg=(responseData)
                return response
            end 
        rescue => exception
                AppLog.logger.info("At line #{__LINE__} sendTvRechargeRequest method is called and exception is = \n#{exception.message}")
                AppLog.logger.info("At line #{__LINE__} sendTvRechargeRequest method is called and full exception is = \n#{exception.backtrace.join("\n")}")
                response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
                response.Response_Msg=(exception.message) 
            return response 
        end
    end


	def self.validate(currentAgent, clientMobileNo, clientSmartCardNo, rechargeAmount, customerFee, appTxnUniqueID)
      
        response = Response.new

            unless currentAgent.present?
                    AppLog.logger.info("Inside the MobileRechargeManager's  validate method at line #{__LINE__} and agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end
            
            if !clientMobileNo.present? || !clientSmartCardNo.present? || !rechargeAmount.present? || !customerFee.present? || !appTxnUniqueID.present?
                    AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
        	end   

            if rechargeAmount.to_f <= 0
                    AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                    response.Response_Code=(Error::LOAN_AMOUNT_LESS_THEN_ZERO)
                    response.Response_Msg=(I18n.t :LOAN_AMOUNT_LESS_THEN_ZERO_MSG) 
                return response
            end

            if (rechargeAmount.to_f%10) != 0
                    AppLog.logger.info("At line #{__LINE__} and recharge amount is may be zero or less then zero")
                    response.Response_Code=(Error::AMOUNT_NOT_DIV_BY_TEN)
                    response.Response_Msg=(I18n.t :AMOUNT_NOT_DIV_BY_TEN_MSG) 
                return response
            end

            isAccountSuspend = Utility.isAgentAccountSuspend(currentAgent)
            AppLog.logger.info("At line #{__LINE__} and check the agent account is suspended or not = #{isAccountSuspend}")
            if isAccountSuspend
                    response.Response_Code=(Error::MICRO_FRANCHISEE_ACCOUNT_SUSPEND)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_ACCOUNT_SUSPEND_MSG) 
                return response
            end  

            isServiceEnable = Utility.checkServiceStatus(Constants::FDI_DTH_LABEL)
            if !isServiceEnable
                    AppLog.logger.info("At line #{__LINE__} and service is currently disable in service_setting table")
                    response.Response_Code=(Error::SERVER_TEMP_UNAVAILABLE)
                    response.Response_Msg=(I18n.t :SERVER_TEMP_UNAVAILABLE_MSG) 
                return response
            end

            agentAccountBalance = TxnHeader.my_money(Utility.getAgentAccountBalance(currentAgent)).to_f
            agentTotalVATPending = TxnHeader.my_money(VatManager.getAgentTotalVAT(currentAgent.id, Constants::DEDUCT_FROM_AGENT_VAT_STATUS)).to_f
            AppLog.logger.info("At line #{__LINE__} account balance of the agent is = #{agentAccountBalance} and pending agent VAT is = #{agentTotalVATPending}")
            if (agentAccountBalance - agentTotalVATPending) <   TxnHeader.my_money(rechargeAmount).to_f +   TxnHeader.my_money(customerFee).to_f
                    AppLog.logger.info("At line #{__LINE__} and agent account balance is below then the rechargeAmount")
                    response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
                    response.Response_Msg=(I18n.t :INSUFFICIENT_FUND_MSG) 
                return response
            end

            isAppUniqueTxnIdExist = AppUtility.checkTxnIDExist(appTxnUniqueID)
            if isAppUniqueTxnIdExist
                    AppLog.logger.info("At line #{__LINE__} and unique txn id that is generated by the app is exist")
                    response.Response_Code=(Error::UNIQUE_TXN_ID_EXIST)
                    response.Response_Msg=(I18n.t :UNIQUE_TXN_ID_EXIST_MSG) 
                return response
            end

            if AppUtility.isServiceBalanceDeceedThreshold(Constants::FDI_DTH_LABEL)
                AppLog.logger.info("At line #{__LINE__} and servcie balance is deceed then the threshold and email is sent to the admins")
                AppUtility.sendThresholdEmailToAdmin(Constants::FDI_DTH_LABEL)
            end 

            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("") 
      	return response  
    end

end	