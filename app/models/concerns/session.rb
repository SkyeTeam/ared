# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 53 }
# :reek:DuplicateMethodCall { max_calls: 15 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class Session

    include Sorcery::TestHelpers::Rails
    
    @globalAccessID = Constants::BLANK_VALUE

    def self.authenticateUser(params, request)

      	#These parameter get from the view page or from Client side
    	email = params[:email]
      	password = params[:password]
      	country = params[:country]

      	Log.logger.info("Autentication funtion on web is called at line #{__LINE__} with email id is = #{email} and country is = #{country}")

    	response = Response.new

    	user = User.find_by_email(email) # this method check the email if exit then give to the variable user

    	Log.logger.info("Authentication Manager class is called at line = #{__LINE__} and check whether the user that try to login with this email = #{email} is exist or not #{user.present?}") 

	    if user.present?

	    	@globalAccessID = Utility.addDataIntoAccessLogs(user.id, User.getUserRoleIntValue(user), Constants::LOGIN_THROUGH_WEB_STATUS, request)

	     	isPasswordMatch = user.valid_password?(password)

	      	if isPasswordMatch == true
		        
		        if user.role.eql? Constants::ADMIN_DB_LABEL

		        	if user.status.eql? Constants::ACTIVE_DB_LABEL
			        	Log.logger.info("Inside the Authentication Manager class at line = #{__LINE__} and admin #{email} is successfully login") 
			           
			           	response.user=(user)
			           	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			           return response
			        else
			        	Log.logger.info("Inside the Authentication Manager class at line = #{__LINE__} and admin #{email} is failed to login because this admin is not active")
			        	response.Response_Code=(Error::UNAUTHORIZED)
					    response.Response_Msg=(I18n.t :UNAUTHORIZED_MSG)
				       return response
			        end   

		        else
		        	Log.logger.info("Inside the Authentication Manager class at line = #{__LINE__} and admin #{email} is failed to login because this user is not admin")
		        	response.Response_Code=(Error::UNAUTHORIZED)
				    response.Response_Msg=(I18n.t :UNAUTHORIZED_MSG)
				    return response
		        end

	      	else
	      		Log.logger.info("Inside the Authentication Manager class at line = #{__LINE__} and admin #{email} is failed to login  bcoz his password is not match inside the database")	
		      	response.Response_Code=(Error::UNAUTHORIZED_INFO)
				response.Response_Msg=(I18n.t :UNAUTHORIZED_INFO_MSG)
		      	return response
	      	end

	    else
	    	
	    	@globalAccessID = Utility.addDataIntoAccessLogs(Constants::ANONYMOUS_LOGIN_USER_ID, Constants::ANONYMOUS_LOGIN_USER_ROLE, Constants::LOGIN_THROUGH_WEB_STATUS, request)

	    	Log.logger.info("Inside the Authentication Manager class at line = #{__LINE__} and admin #{email} is failed to login  bcoz his email is not exist inside the database")
	    	response.Response_Code=(Error::UNAUTHORIZED_INFO)
			response.Response_Msg=(I18n.t :UNAUTHORIZED_INFO_MSG)
			return response
	    end
   	end 
   	

   	def self.sendOTPEmail(params, user)

   		emailID = params[:email]

   		Log.logger.info("Inside the senfOTPEmail method at line = #{__LINE__}")

   		otpID = OneTimePassword.getOTPID

   		randomOTPNo = Utility.getRandomNumber(6)

   		randomOTPEncrypt = BCrypt::Password.create randomOTPNo

   		params[:otpID] = otpID
       
   		otp = OneTimePassword.new
	   		otp.otp_id = otpID
	   		otp.otp = randomOTPEncrypt
	   		otp.description = Constants::LOGIN_OTP_DES_LABEL
	   		otp.status = Constants::OTP_NOT_USED_STATUS
   		otp.save

   		if Properties::LOGIN_EMAIL_ENABLE
			Utility.sendEmail(Properties::SUPERADMIN_EMAIL,emailID,(I18n.t :OTP_EMAIL_SUBJECT),(I18n.t :OTP_EMAIL_MESSAGE, :name => user.name.to_s + ' ' + user.last_name.to_s , :OTPValidTime => Constants::OTP_VALID_TIME_VALUE, :OTPNumber => randomOTPNo ))
		else
			Log.logger.info("At line = #{__LINE__} and OTP = #{randomOTPNo}")
		end			
   	end	


   	def self.validateOTP(params)

   		accessID = @globalAccessID

   		Log.logger.info("Inside the validateOTP method at line = #{__LINE__}")

   		response = Response.new
   		
   		otpID = params[:otpID]
      	enteredOTP = params[:otp]

      	unless otpID.present?
      			response.Response_Code=(Error::INVALID_DATA)
				response.Response_Msg=("")
		    return response
      	end	

      	Log.logger.info("At line #{__LINE__} with OTP id is = #{otpID} and entered OTP number is = #{enteredOTP}")

      	oneTimePassword = OneTimePassword.find_by_otp_id(otpID)

      	dbSavedOTP = oneTimePassword.otp

      	dbSavedOTPNew = BCrypt::Password.new(dbSavedOTP)

      	unless dbSavedOTPNew == enteredOTP
	  			response.Response_Code=(Error::INVALID_OTP)
				response.Response_Msg=(I18n.t :INVALID_OTP_MSG)
		    return response
      	end	

      	otpSendTime = DateTime.parse(oneTimePassword.created_at.to_s)
       	currentTime = DateTime.now
       	minutesDifference = ((currentTime - otpSendTime) * 24 * 60).to_i

       	Log.logger.info("At line = #{__LINE__} and time difference is = #{minutesDifference}")

       	if minutesDifference.to_i >= Constants::OTP_VALID_TIME_VALUE.to_i
       			response.Response_Code=(Error::OTP_EXPIRED)
				response.Response_Msg=(I18n.t :OTP_EXPIRED_MSG)
		    return response
       	end	

       	oneTimePassword.update_attribute(:status, Constants::OTP_USED_STATUS)
       	Utility.updateAccessLoginStatus(accessID)

      		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
      		response.Response_Msg=('')
		return response

   	end	

   	def self.changeUserPin(id,oldPassword,newPassword)
   		response = Response.new
   		
   		Log.logger.info("Change Pin's Manager class is called at line = #{__LINE__} and user id that want to change his pin = #{id}") 
	   
	   	user = User.find_by_id(id)
	   	Log.logger.info("Inside the Change Pin's Manager class at line = #{__LINE__} first check that user is exist or not = #{user.present?}")

	    if user.present?
	        isOldPassMatch = user.valid_password?(oldPassword)
	        Log.logger.info("Inside the Change Pin's Manager class at line = #{__LINE__} now check that his old password is match or not = #{isOldPassMatch}")
	       
	       	if isOldPassMatch == true
	       		if oldPassword.eql? newPassword
	       				response.Response_Code=(Error::OLD_NEW_PASSWORD_SAME)
		         		response.Response_Msg=(I18n.t :OLD_NEW_PASSWORD_SAME_MSG)
		         	return response
	       		else
			        	my_password = BCrypt::Password.create newPassword
			        	user.change_password!(newPassword)
			        
		         		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		         		response.Response_Msg=(I18n.t :PASSWORD_CHANGE_MSG)
		         	return response
		        end 	
	       	else
		       		response.Response_Code=(Error::INVALID_PIN)
					response.Response_Msg=(I18n.t :INVALID_PIN_MSG)
				return response
	       	end
	    else
	    		response.Response_Code=(Error::USER_NOT_EXIST)
				response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG)
			return response
	    end
   	end

end