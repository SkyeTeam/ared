# This class used as a Manager in MVC
# :reek:UncommunicativeMethodName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeVariableName: { reject: -!ruby/regexp /x/ }
# :reek:UncommunicativeParameterName: { reject: -!ruby/regexp /x/ }
# :reek:TooManyStatements: { max_statements: 50 }
# :reek:DuplicateMethodCall { max_calls: 13 }
# :reek:InstanceVariableAssumption { enabled: false }
# :reek:RepeatedConditional { max_ifs: 5 }
# :reek:ControlParameter { enabled: false }
# :reek:LongParameterList { enabled: false }
# :reek:UnusedParameters { enabled: false }

class CashDepositManager
	
	def self.searchByPhone(phone)

		Log.logger.info("The Cash Deposit Manager's method searchByPhone is called at line = #{__LINE__} and search the phone in the database is = #{phone}")

		response = Response.new

		user = User.find_by_phone(phone)

		Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and check agent is exist or not =#{user.present?}")

	    if user.present?

	      	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and role = #{user.role} and status = #{user.status}")

	      	if user.role.eql? Constants::AGENT_DB_LABEL #This check that the user that is search is a agent or not 
		        if user.status.eql? Constants::ACTIVE_DB_LABEL #This check that the user that is search is a active or not 
						Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line #{__LINE__} and information give back to controller")
		   	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		   	        response.User_Object=(user)
			        return response
		        else
			        	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line #{__LINE__} and agent is not active") 
		            response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_ACTIVE)
			        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_ACTIVE_MSG) 
			        return response
			    end
	      	else
			      	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and retrun bcoz his role is not agent bcoz this no belong to =#{user.role}")
		        response.Response_Code=(Error::MICRO_FRANCHISEE_PHONE_NOT_EXIST)
			    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_PHONE_NOT_EXIST_MSG) 
			    return response
		    end
	    else
	      	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and agentis not exist.") 
	      response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	      response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
		  return response
	    end

	end

	def self.searchByPhoneNew(phoneOrName, userAccType)

		Log.logger.info("At line #{__LINE__} inside the CashDepositManager and  searchByPhoneNew method is called with phoneOrName = #{phoneOrName}, userAccType = #{userAccType}")
		response = Response.new

		if !phoneOrName.present? || !userAccType.present?
        		Log.logger.info("At line = #{__LINE__} and data is come nil from the web")
        		response.Response_Code=(Error::INVALID_DATA)
        		response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
      		return response
  		end

  		phone = User.getUserPhone(phoneOrName)
		user = User.find_by_phone(phone)
		Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and check agent is exist or not =#{user.present?}")

	    if user.present?

	      	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and role = #{user.role} and status = #{user.status}")
	      	if user.role.eql? Constants::AGENT_DB_LABEL #This check that the user that is search is a agent or not 
		        if user.status.eql? Constants::ACTIVE_DB_LABEL #This check that the user that is search is a active or not 
						Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line #{__LINE__} and information give back to controller")
		   	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		   	        response.User_Object=(user)
			        return response
		        else
			        	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line #{__LINE__} and agent is not active") 
		            response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_ACTIVE)
			        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_ACTIVE_MSG) 
			        return response
			    end
	      	else
			      	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and retrun bcoz his role is not agent bcoz this no belong to =#{user.role}")
		        response.Response_Code=(Error::MICRO_FRANCHISEE_PHONE_NOT_EXIST)
			    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_PHONE_NOT_EXIST_MSG) 
			    return response
		    end
	    else
	      	Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and agentis not exist.") 
	      response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	      response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
		  return response
	    end

	end

	def self.searchByName(enteredName)
		
		Log.logger.info("The Cash Deposit Manager's method searchByName is called at line = #{__LINE__} and search the name in the database is = #{name}")

	    length = 0  
	    query = ''  
	    
	    if enteredName.include? ' '
	        x = enteredName.split(' ') #Split on the basis of the spaces
	        length = x.length
	    
	        for i in (0..length-1)
	          query = query.to_s + " LOWER(name) LIKE '%" + x[i].downcase + "%' or "
	          query = query.to_s + " LOWER(last_name) LIKE '%" + x[i].downcase + "%' or"
	        end  
	        query = query.chomp('or')
	    else
	        query = "LOWER(name) LIKE '%" + enteredName.downcase + "%' or LOWER(last_name) LIKE '%" + enteredName.downcase + "%'"
	    end 

	    user = User.where(query).where("role = 1 and status = 2") # here role 1 is for the agent and status 2 means active 

	    Log.logger.info("Inside the Cash Deposit Manager's method searchByName at line = #{__LINE__} and check agent is exist or not =#{user.present?}")
	    
	    response = Response.new
	    
	    if user.present?
	    	Log.logger.info("Inside the Cash Deposit Manager's method searchByName at line = #{__LINE__} and agent is present and information give back to the controller")
	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	   	    response.User_Object=(user)
		  return response
	    else
            response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
		    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
		  return response
	    end	
	end


	def self.agentCashDeposit(params, loggedInAdmin, request)

		agentID = params[:agentID]
		userAccType = params[:userAccType]
		serviceType = params[:serviceType]
		subType = params[:subType]
      	approvedAmount = params[:amount].to_f
      	description = params[:description]
      	externalRef = params[:externalRef]
      	txnUniqueID = Utility.getRandomNumber(15)
      	originalHeaderID = nil
		Log.logger.info("Inside the Cash Deposit Manager's method agentCashDeposit is called at line = #{__LINE__} and parameter are agent id = #{agentID}, userAccType = #{userAccType}, approved_amount = #{approvedAmount}, description = #{description} and external_reference = #{externalRef}")
		
		currentAgent = Utility.getCurrentAgent(agentID)

		response = validateCashDeposit(currentAgent, userAccType, approvedAmount, description, externalRef)

	    if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	      	return response
	    end 

	    accountBalance = 0

	    if userAccType.eql? Constants::M_SHIRIKI_ACC_LABEL

	  	    #First we add one entry in the pending request table before processing
	        AppUtility.savePendingRequest(txnUniqueID, serviceType, subType, nil, approvedAmount, agentID, Utility.getUserRemoteIP(request), loggedInAdmin.id, Constants::ADMIN_USER_ROLE)
	        Log.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")
		 
		    ActiveRecord::Base.transaction do

		    	# First Credit the agent Account 
		    	originalHeaderID = creditAgentAccount(currentAgent, approvedAmount, serviceType, subType, description, externalRef)

			    # *********** Now check that if the agent has pending credit balance or not if yes then deduct it ******
			   	agentLoanBalance = Utility.getAgentLoanBalance(currentAgent).to_f
			    Log.logger.info("At line = #{__LINE__} and credit balance of the agent is = #{agentLoanBalance}")
			    
			    isPenaltyDeduct = true
			    depositAmtAndLoanDifference = 0

			    if agentLoanBalance > 0

			    	isPenaltyDeduct = false
				    loanDepositAmount = approvedAmount;

		            if agentLoanBalance.to_f < loanDepositAmount.to_f
		            	depositAmtAndLoanDifference = loanDepositAmount.to_f - agentLoanBalance.to_f
		              	loanDepositAmount = agentLoanBalance
		              	isPenaltyDeduct = true
		            end

		            settleAgentCredit(currentAgent, agentLoanBalance, loanDepositAmount, description, externalRef)
			    end

			    Log.logger.info("At line = #{__LINE__} and check that isPenaltyDeduct = #{isPenaltyDeduct} (if true then the penalty will deduct)")
		        Log.logger.info("At line = #{__LINE__} and the difference of deposit Amt And Loan is = #{depositAmtAndLoanDifference}")

			    # This is for deduct the penalty if the penalty on the loan is present 
			    if isPenaltyDeduct 

			    	agentPenaltyBalance = Utility.getAgentPenaltyBalance(currentAgent).to_f

			    	if agentPenaltyBalance > 0 

				    	if depositAmtAndLoanDifference != 0
				            penaltyDepositAmount = depositAmtAndLoanDifference
				        else
				            penaltyDepositAmount = approvedAmount;
				        end 

				        if penaltyDepositAmount > agentPenaltyBalance
			          		penaltyDepositAmount = agentPenaltyBalance
			          	end	

			        	Log.logger.info("At line = #{__LINE__} and the penalty deposit amount is  = #{penaltyDepositAmount}")  	

				        settleAgentPenalty(currentAgent, penaltyDepositAmount, description, externalRef)
			        end 

			    end

				accountBalance = Utility.getAgentAccountBalance(currentAgent)

	    	    AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
	        	Log.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")

		    end  

		elsif userAccType.eql? Constants::GOVT_FUND_ACC_LABEL
			
	        AppUtility.savePendingRequest(txnUniqueID, Constants::USER_GOVT_CASH_DEPOSIT_LABLE, Constants::USER_GOVT_CASH_DEPOSIT_LABLE, nil, approvedAmount, agentID, Utility.getUserRemoteIP(request), loggedInAdmin.id, Constants::ADMIN_USER_ROLE)
	        Log.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")
		 
		    ActiveRecord::Base.transaction do
		    	currentAgentGovtAcc = saveUserGovtAccount(currentAgent)

		    	creditAgentGovtAccount(currentAgent, currentAgentGovtAcc, approvedAmount, description, externalRef, userAccType)

		    	accountBalance = Utility.getAgentGovtAccountBalance(currentAgent)

		    	AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
	        	Log.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")
		    end	

		end	
	   	
	   	Utility.sendNotification(currentAgent.id, (I18n.t :MF_ACC_CREDIT_NOTI_MSG, :agentName => (currentAgent.name + ' ' + currentAgent.last_name), :amount => Utility.getFormatedAmount(approvedAmount), :balance => Utility.getFormatedAmount(accountBalance)), Constants::CASH_DEPOSIT_NOTIFICATION_TITLE)

	    response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		response.Response_Msg=(I18n.t :MF_ACC_CREDIT_MSG, :amount => Utility.getFormatedAmount(approvedAmount))
		response.ourTransactionId=(originalHeaderID)

	  return response				
	end

	def self.saveUserGovtAccount(currentAgent)
		currentAgentGovtAcc = Utility.getUserGovtAccount(currentAgent)
		if currentAgentGovtAcc.present?
			return currentAgentGovtAcc
		else
			return getUserAccount(currentAgent)
		end	
	end	

	def self.getUserAccount(user) 
 		userGovtAccount = UserGovtAccount.new
            userGovtAccount.user_id  = user.id
            userGovtAccount.acc_type = Constants::USER_GOCT_ACC_LABEL
            userGovtAccount.currency = Utility.getCurrencyLabel
            userGovtAccount.balance  = Constants::USER_ACCOUNT_INITIAL_BALANCE
            userGovtAccount.save
        return userGovtAccount         
 	end	

	def self.validateCashDeposit(currentAgent, userAccType, approvedAmount, description, externalRef)
		Log.logger.info("Inside the Cash Deposit Manager's  validateCashDeposit method is called at line #{__LINE__}")
		
		response = Response.new

			unless currentAgent.present?
	        		Log.logger.info("At line #{__LINE__} and agent is not present")
	        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	      		return response
	    	end

			if !userAccType.present? || !approvedAmount.present? || !description.present? || !externalRef.present?
					Log.logger.info("At line #{__LINE__} and some values are empty")
					response.Response_Code=(Error::INVALID_DATA)
	    	    	response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	       		return response
			end 

			if !(userAccType.eql? Constants::M_SHIRIKI_ACC_LABEL) and !(userAccType.eql? Constants::GOVT_FUND_ACC_LABEL)
					Log.logger.info("At line #{__LINE__} and userAccType is not present")
					response.Response_Code=(Error::ACCOUNT_TYPE_NOT_EXIST)
	    	    	response.Response_Msg=(I18n.t :ACCOUNT_TYPE_NOT_EXIST_MSG) 
	       		return response
			end	

	      	if approvedAmount.to_f <= 0
	      			Log.logger.info("At line #{__LINE__} and approvedAmount is less then zero")
		        	response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
	    	    	response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
	       		return response
	      	end

	      	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	      	response.Response_Msg=("") 
        return response  
	end	


	def self.creditAgentAccount(currentAgent, approvedAmount, serviceType, subType, description, externalRef)
		
		Log.logger.info("Inside the CashDepositManager's method creditAgentAccount at line = #{__LINE__}")

		#Now save the data into the txn_header table
    	txnHeader = getHeaderNew(TxnHeader.my_money(approvedAmount), serviceType, subType, description, currentAgent.id, externalRef)
    	txnHeader.save
      	Log.logger.info("At line = #{__LINE__} and Cash Deposit entry is saved in the TxnHeader table successfully")

    	# And two entries of the credit and debit into the txn_item table
    	agentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(approvedAmount), false, Utility.getCurrentAgentAccount(currentAgent))
    	agentTxnItem.header_id = txnHeader.id
      	agentTxnItem.save
	    Log.logger.info("At line = #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

    	optCashTxnItem  = getTxnItem(Utility.getCashAdmin(), TxnHeader.my_money(approvedAmount), true, Utility.getCashAdminAccount(Utility.getCashAdmin()))
	    optCashTxnItem.header_id = txnHeader.id
	    optCashTxnItem.save
        Log.logger.info("At line = #{__LINE__} and operator(cash) transaction item is saved in the TxnItem table successfully")
          
        # After saving into the txn_item now update the account balances of the operator and the agent
    	TxnManager.update_account(Utility.getCashAdminAccount(Utility.getCashAdmin()), TxnHeader.my_money(approvedAmount), true)
		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(approvedAmount), false)  
	    Log.logger.info("At line = #{__LINE__} and balances of Agent and Operator Cash Account are updated successfully")

	    return txnHeader.id

	end	

	def self.creditAgentGovtAccount(currentAgent, currentAgentGovtAcc, approvedAmount, description, externalRef, userAccType)
		
		Log.logger.info("At line = #{__LINE__} inside the CashDepositManager and method creditAgentGovtAccount is called")

		#Now save the data into the txn_header table
    	txnHeader = getHeader(TxnHeader.my_money(approvedAmount), Constants::USER_GOVT_CASH_DEPOSIT_LABLE, description, currentAgent.id, externalRef)
    	txnHeader.save
      	Log.logger.info("At line = #{__LINE__} and Govt Fund Deposit entry is saved in the TxnHeader table successfully")

    	# And two entries of the credit and debit into the txn_item table
    	agentTxnItem  = getTxnItemNew(currentAgent, TxnHeader.my_money(approvedAmount), false, currentAgentGovtAcc.id, userAccType)
    	agentTxnItem.header_id = txnHeader.id
      	agentTxnItem.save
	    Log.logger.info("At line = #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")

    	optGovtBankTxnItem  = getTxnItem(Utility.getGovtBankAdmin(), TxnHeader.my_money(approvedAmount), true, Utility.getGovtBankAdminAccount(Utility.getGovtBankAdmin()))
	    optGovtBankTxnItem.header_id = txnHeader.id
	    optGovtBankTxnItem.save
        Log.logger.info("At line = #{__LINE__} and operator(Govt Bank) transaction item is saved in the TxnItem table successfully")
          
        # After saving into the txn_item now update the account balances of the operator and the agent
    	TxnManager.update_account(Utility.getGovtBankAdminAccount(Utility.getGovtBankAdmin()), TxnHeader.my_money(approvedAmount), true)
		TxnManager.update_govt_bank_account(currentAgent.id, TxnHeader.my_money(approvedAmount), false)  
	    Log.logger.info("At line = #{__LINE__} and balances of Agent and Operator Govt Bank Account are updated successfully")

	end	

	def self.settleAgentCredit(currentAgent, agentLoanBalance, loanDepositAmount, description, externalRef)
		
		lastLoanDueDate = DateTime.parse(Utility.getAgentLastLoanDueDate(currentAgent).to_s)
		Log.logger.info("At line = #{__LINE__} and due date of the loan is = #{lastLoanDueDate}")

		agentRatingValue = getAgentLoanRatingValue(Date.today, lastLoanDueDate, agentLoanBalance, loanDepositAmount)
	    loanRating = getLoanRating(currentAgent.id,Constants::LOAN_RATING_PARAMETER,agentRatingValue)
		loanRating.save
		Log.logger.info("At line = #{__LINE__} and agent credit rating is store in the table with rating value = #{agentRatingValue}")
	            
        loanTxnHeader = getHeader(TxnHeader.my_money(loanDepositAmount), Constants::USER_LOAN_DEPOSIT_LABLE, description, currentAgent.id, externalRef)
        loanTxnHeader.save
        Log.logger.info("At line = #{__LINE__} and Loan Deposit(loan_deposit) entry is saved in the TxnHeader table successfully")
        
        loanAgentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(loanDepositAmount), true, Utility.getCurrentAgentAccount(currentAgent))
        loanAgentTxnItem.header_id = loanTxnHeader.id
        loanAgentTxnItem.save
        Log.logger.info("At line = #{__LINE__} and Agent loan transaction item is saved in the TxnItem table successfully")

        loanOptStockItem  = getTxnItem(Utility.getStockAdmin(), TxnHeader.my_money(loanDepositAmount), false, Utility.getStockAdminAccount(Utility.getStockAdmin()))
		loanOptStockItem.header_id = loanTxnHeader.id;
        loanOptStockItem.save
        Log.logger.info("At line = #{__LINE__} and Operator Stock loan transaction item is saved in the TxnItem table successfully")

        loanRecord = getLoanRecord(currentAgent, TxnHeader.my_money(loanDepositAmount), lastLoanDueDate)
		loanRecord.save
        Log.logger.info("At line = #{__LINE__} and loan entry of the agent is saved in the Loans table successfully")      	

      	TxnManager.update_account( Utility.getStockAdminAccount(Utility.getStockAdmin()), TxnHeader.my_money(loanDepositAmount), false)
		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(loanDepositAmount), true)  
        Log.logger.info("At line = #{__LINE__} and balances of Agent and Operator Stock Account are updated successfully")

	end	


	def self.settleAgentPenalty(currentAgent, penaltyDepositAmount, description, externalRef)
           
        penaltyTxnHeader = getHeader(TxnHeader.my_money(penaltyDepositAmount), Constants::USER_PENALTY_CHARGE_LABLE, description, currentAgent.id, externalRef)
        penaltyTxnHeader.save
        Log.logger.info("At line = #{__LINE__} and Penalty Deposit(delay_charge) entry is saved in the TxnHeader table successfully")

	    penaltyAgentTxnItem  = getTxnItem(currentAgent, TxnHeader.my_money(penaltyDepositAmount), true, Utility.getCurrentAgentAccount(currentAgent))
		penaltyAgentTxnItem.header_id = penaltyTxnHeader.id
		penaltyAgentTxnItem.save
		Log.logger.info("At line = #{__LINE__} and Agent penalty transaction item is saved in the TxnItem table successfully")

	    optPenaltyTxnItem  = getTxnItem(Utility.getPenaltyAdmin(), TxnHeader.my_money(penaltyDepositAmount), false, Utility.getPenaltyAdminAccount(Utility.getPenaltyAdmin()))
        optPenaltyTxnItem.header_id = penaltyTxnHeader.id;
        optPenaltyTxnItem.save
        Log.logger.info("At line = #{__LINE__} and Operator Penalty  transaction item is saved in the TxnItem table successfully")

		penaltyRecord = getPenaltyRecord(currentAgent, TxnHeader.my_money(penaltyDepositAmount))
		penaltyRecord.header_id = penaltyTxnHeader.id
		penaltyRecord.save
	    Log.logger.info("At line = #{__LINE__} and penalty entry of the agent is saved in the agent_penalty table successfully")          

        TxnManager.update_account(Utility.getPenaltyAdminAccount(Utility.getPenaltyAdmin()), TxnHeader.my_money(penaltyDepositAmount), false)
		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(penaltyDepositAmount), true)
        Log.logger.info("At line = #{__LINE__} and balances of Agent and Operator Penalty Account are updated successfully")
			            
	end	

	def self.getAgentLoanRatingValue(todayDate,lastLoanDueDate,lastLoanBalance,loanDepositAmount)
			if todayDate <= lastLoanDueDate
	            if lastLoanBalance == loanDepositAmount
	                agentRating = ConstantsHelper::BEFORE_DUE_DATE_RATING  #Rating 1 
	            elsif lastLoanBalance == 0
	                agentRating = ConstantsHelper::BEFORE_DUE_DATE_RATING  #Ratimng 1 
	            elsif loanDepositAmount < lastLoanBalance
	                agentRating = 0.00  #Rating 0          
	            end  
	        else 
	            differnce_of_days = (todayDate - lastLoanDueDate).to_i

	            if differnce_of_days == 1 && lastLoanBalance == loanDepositAmount
	                agentRating = ConstantsHelper::AFTER_ONE_DAY_DUE_DATE_RATING #Rating 0.80
	            elsif differnce_of_days == 2 && lastLoanBalance == loanDepositAmount
	                agentRating = ConstantsHelper::AFTER_TWO_DAY_DUE_DATE_RATING #Rating 0.60     
	            elsif differnce_of_days == 3 && lastLoanBalance == loanDepositAmount
	                agentRating = ConstantsHelper::AFTER_THREE_DAY_DUE_DATE_RATING  #Rating 0.40     
	            elsif differnce_of_days == 4 && lastLoanBalance == loanDepositAmount
	                agentRating = ConstantsHelper::AFTER_FOUR_DAY_DUE_DATE_RATING  #Rating 0.20     
	            elsif differnce_of_days == 5 && lastLoanBalance == loanDepositAmount
	                agentRating = ConstantsHelper::AFTER_FIVE_DAY_DUE_DATE_RATING  #Rating 0.00     
	            else
	                agentRating = 0.00   
	            end      
	        end
      	return agentRating  
	end	

	def self.getHeader(approvedAmount, serviceType, description, agentID, externalRef)
		
		Log.logger.info("Inside the CashDepositManager's method getHeader is called at line = #{__LINE__}")
	   
		txnHeader = TxnHeader.new
		   
		    txnHeader.service_type = serviceType
		    txnHeader.sub_type =  serviceType
		    txnHeader.txn_date = Time.now
		    txnHeader.currency_code = Constants::USER_ACCOUNT_CURRENCY
		    txnHeader.amount = TxnHeader.my_money(approvedAmount)
		    txnHeader.agent_id = agentID
		    txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
		    txnHeader.gateway =Constants::TXN_GATEWAY
		    txnHeader.description = description
		    txnHeader.external_reference = externalRef
		    
	    return txnHeader
	end

	def self.getHeaderNew(approvedAmount, serviceType, subType, description, agentID, externalRef)
		
		Log.logger.info("Inside the CashDepositManager's method getHeaderNew is called at line = #{__LINE__}")
	   
		txnHeader = TxnHeader.new
		   
		    txnHeader.service_type = serviceType
		    txnHeader.sub_type =  subType
		    txnHeader.txn_date = Time.now
		    txnHeader.currency_code = Utility.getCurrencyLabel
		    txnHeader.amount = TxnHeader.my_money(approvedAmount).to_f
		    txnHeader.agent_id = agentID
		    txnHeader.status = Constants::SUCCESS_RESPONSE_CODE
		    txnHeader.gateway =Constants::TXN_GATEWAY
		    txnHeader.description = description
		    txnHeader.external_reference = externalRef
		    
	    return txnHeader
	end

	def self.getTxnItem(whichUser, amount, isDebit, userAccount)
		
		Log.logger.info("Inside the Cash Deposit manager's method getTxnItem at line = #{__LINE__}")
	    
	    txnItem = TxnItem.new
	   
		    txnItem.user_id = whichUser.id

		    previousBalance = Utility.getAgentAccountBalance(whichUser).to_f

		    if isDebit == false
		      txnItem.debit_amount = nil
		      txnItem.credit_amount = TxnHeader.my_money(amount)
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)
		      txnItem.role = Constants::PAYEE_LABEL
		    else
		      txnItem.debit_amount = TxnHeader.my_money(amount)
		      txnItem.credit_amount = nil
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)
		      txnItem.role = Constants::PAYER_LABEL
		    end
		    txnItem.account_id = userAccount.id
		    txnItem.fee_id = nil
		    txnItem.fee_amt =  0
		    txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f 

	    return txnItem
	end

	def self.getTxnItemNew(whichUser, amount, isDebit, userAccountID, userAccType)
		
		Log.logger.info("Inside the Cash Deposit manager's method getTxnItem at line = #{__LINE__}")
	    
	    txnItem = TxnItem.new
	   
		    txnItem.user_id = whichUser.id

		    if userAccType.eql? Constants::M_SHIRIKI_ACC_LABEL	
		    	previousBalance = Utility.getAgentAccountBalance(whichUser).to_f
		    else
		    	previousBalance = Utility.getAgentGovtAccountBalance(whichUser).to_f
		    end	

		    if isDebit == false
		      txnItem.debit_amount = nil
		      txnItem.credit_amount = TxnHeader.my_money(amount)
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f + TxnHeader.my_money(amount).to_f)
		      txnItem.role = Constants::PAYEE_LABEL
		    else
		      txnItem.debit_amount = TxnHeader.my_money(amount)
		      txnItem.credit_amount = nil
		      txnItem.post_balance = TxnHeader.my_money(TxnHeader.my_money(previousBalance).to_f - TxnHeader.my_money(amount).to_f)
		      txnItem.role = Constants::PAYER_LABEL 
		    end
		    txnItem.account_id = userAccountID
		    txnItem.fee_id = nil
		    txnItem.fee_amt =  0
		    txnItem.previous_balance = TxnHeader.my_money(previousBalance).to_f 

	    return txnItem
	end

	def self.getLoanRecord(agent, amount, lastLoanDueDate)
		Log.logger.info("Inside the Cash Deposit manager method getLoanRecord at line = #{__LINE__}")
		    loan = Loan.new
		    loan.user_id = agent.id
		    loan.acc_type = Constants::USER_ACCOUNT_TYPE
		    loan.currency = Constants::USER_ACCOUNT_CURRENCY
		    loan.amount = (amount.to_f - (amount.to_f + amount.to_f))
		    loan.balance = TxnHeader.my_money(TxnHeader.my_money(Utility.getAgentLoanBalance(agent)).to_f - TxnHeader.my_money(amount).to_f).to_f
		    loan.due_date = lastLoanDueDate
	    return loan
	end

	def self.getPenaltyRecord(agent, amount)
		Log.logger.info("Inside the Cash Deposit manager method getPenaltyRecord method at line = #{__LINE__}")
	    lastPenaltyRecord = Utility.getCurrentAgentPenaltyAccount(agent)
	    agentPenalty = AgentPenaltyDetail.new
		    agentPenalty.user_id = agent.id
		    agentPenalty.loan_amount = lastPenaltyRecord.loan_amount  
		    agentPenalty.penalty_amount = (amount.to_f - (amount.to_f + amount.to_f))
		    agentPenalty.pending_penalty_amount = TxnHeader.my_money(TxnHeader.my_money(lastPenaltyRecord.pending_penalty_amount).to_f - TxnHeader.my_money(amount).to_f).to_f
	    return agentPenalty
	end

	def self.getLoanRating(agent_id,parameter,rating)
		Log.logger.info("Inside the Cash Deposit manager method getLoanRating at line = #{__LINE__} ")
		loanRating = LoanRating.new
		    loanRating.user_id = agent_id
		    loanRating.parameter= parameter
		    loanRating.date = Date.today
		    loanRating.rating = rating
	    return loanRating     
	end 

	def self.agentCashDepositByAPI(params, request)

		Log.logger.info("At line #{__LINE__} inside the Cash Deposit Manager class and agentCashDepositByAPI method is called")
		userEmail = params[:email]
		agentPhone = params[:agentPhone]
		amount = params[:amount]
		timeStampz = params[:timeStamp]
		hashData = params[:hashData]
		Log.logger.info("At line #{__LINE__} parameters are userEmail = #{userEmail}, agentPhone = #{agentPhone}, amount = #{amount}, timeStampz = #{timeStampz} and hashData = #{hashData}")

		currentUser = Utility.getUserFromEmail(userEmail)
		response = validateAPICashDeposit(currentUser, agentPhone, amount, timeStampz, hashData, request)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	      	return response
	    end 
	    Log.logger.info("At line #{__LINE__} this request is from user id = #{currentUser.id}, name = #{currentUser.name} #{currentUser.last_name} and phone = #{currentUser.phone}")

	    params[:agentID] = response.Response_Msg
		
		response = agentCashDeposit(params, currentUser, request)

		return response

	end	

	def self.validateAPICashDeposit(currentUser, agentPhone, amount, timeStampz, hashData, request)
		Log.logger.info("At line #{__LINE__} inside the Cash Deposit Manager class and validateAPICashDeposit method is called")
		response = Response.new

			requestedIP = Utility.getUserRemoteIP(request)
			Log.logger.info("At line #{__LINE__} request come from the IP = #{requestedIP}")
			unless requestedIP.eql? Properties::KIMALI_IP_ADDRESS
					Log.logger.info("At line #{__LINE__} and this request is not come from the authorize IP address which is = #{Properties::KIMALI_IP_ADDRESS}")
	        		response.Response_Code=(Error::IP_NOT_AUTHORIZE)
	        		response.Response_Msg=(I18n.t :IP_NOT_AUTHORIZE_MSG) 
	      		return response
			end	

			unless currentUser.present?
	        		Log.logger.info("At line #{__LINE__} and user is not present in our database")
	        		response.Response_Code=(Error::USER_NOT_EXIST)
	        		response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
	      		return response
	    	end

			if !agentPhone.present? || !amount.present? || !timeStampz.present? || !hashData.present?
					Log.logger.info("At line #{__LINE__} and some values are come nil in the request")
					response.Response_Code=(Error::INVALID_DATA)
	    	    	response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	       		return response
			end 

			if isDuplicateRequest(timeStampz, agentPhone)
					Log.logger.info("At line #{__LINE__} and duplicate request is come in less then 1 min")
					response.Response_Code=(Error::DUPLICATE_REQUEST)
	    	    	response.Response_Msg=(I18n.t :DUPLICATE_REQUEST_MSG) 
	       		return response
			end	

	      	if amount.to_f <= 0
	      			Log.logger.info("At line #{__LINE__} and amount to be deposit may be zero or the less then zero")
		        	response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
	    	    	response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
	       		return response
	      	end

	      	currentAgent = Utility.getCurrentAgentByPhone(agentPhone)
	      	unless currentAgent.present?
	        		Log.logger.info("At line #{__LINE__} and agent is not present with this phone")
	        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	      		return response
	    	end

	      	userAPIKey = ApiKey.getKey(currentUser.id)
          	unless userAPIKey.present?
          			Log.logger.info("At line #{__LINE__} and currentUser does not have any API Key")
		        	response.Response_Code=(Error::API_KEY_NOT_EXIST)
	    	    	response.Response_Msg=(I18n.t :API_KEY_NOT_EXIST_MSG) 
	       		return response
	      	end

            apiKey = Utility.decryptedData(userAPIKey.api_key)
            Log.logger.info("At line #{__LINE__} and our DB API Key = #{apiKey}")
	      	requestData = agentPhone.to_s + "|" + amount.to_s + "|" + timeStampz.to_s
	      	requestHashData = Utility.generateHashData(requestData, apiKey)
	      	Log.logger.info("At line #{__LINE__} and our requestHashData = #{requestHashData}")

	      	unless requestHashData.eql? hashData
	      			Log.logger.info("At line #{__LINE__} and requestHashData is not matched with out hashData")
		        	response.Response_Code=(Error::REQUEST_HASH_DATA_NOT_MATCH)
	    	    	response.Response_Msg=(I18n.t :REQUEST_HASH_DATA_NOT_MATCH_MSG) 
	       		return response
	      	end	

	      	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	      	response.Response_Msg=(currentAgent.id) 
        return response 
	end	

	def self.isDuplicateRequest(timeStamp, agentPhone)
		minutesDiff = ((Time.now - Time.parse(timeStamp))/(1.minute)).round
		Log.logger.info("At line #{__LINE__} and time difference in minutes of request is = #{minutesDiff}")
		if minutesDiff.to_i <= 1
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end			
	end	

end	