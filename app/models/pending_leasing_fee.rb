class PendingLeasingFee < ApplicationRecord

	def self.getLastMonthDays
			endDate = getCustomDate()
	      	lastMonthDays = endDate.strftime('%d')	
	    return lastMonthDays	
	end	

	def self.getEndPeriod
			endDate = getCustomDate()
	      	endDate = endDate.strftime('%d-%m-%Y')
      	return endDate
	end	


	def self.getCustomDate()
			currentMonth = Utility.getCurrentMonthNumber
	      	currentYear  = Utility.getCurrentYearNumber

	      	lastMonth = currentMonth.to_i - 1

		    if lastMonth <= 0
		        lastMonth = 12
		        lastYear = currentYear.to_i - 1
		    else
		        lastYear = currentYear
		    end  

	      	date = Date.new(lastYear.to_i, lastMonth.to_i)
	      	monthEnding = date.end_of_month

	      	secondDate = Date.parse(monthEnding.to_s)
	      	endDate = secondDate.end_of_day

	    return endDate	
	end


	def self.getAgentRegDaysDiff(currentAgent)
		
			registeredDate = currentAgent.created_at
			Log.logger.info("At line #{__LINE__} and inside the getAgentRegDaysDiff method and registeredDate is = #{registeredDate}")

			differnce = DateTime.now.mjd - DateTime.parse(registeredDate.to_s).mjd
			Log.logger.info("At line #{__LINE__} and inside the getAgentRegDaysDiff method and differnce of the currentDate and registeredDate is = #{differnce}")

		return 	differnce
	end	


	def self.getPendingFeeAgentsList
			
			agentsList = PendingLeasingFee.where('status = ?', Constants::LEASING_FEE_PENDING_STATUS).order('agent_id, end_period ASC')

		return agentsList

	end	

end
