class Announcement < ApplicationRecord

	def self.createAnnouncement(params, loggedInAdmin)

		Log.logger.info("At line #{__LINE__} inside the Announcement Model class and createAnnouncement method is called")

		announcement = params[:announcement]

		response = validate(announcement, nil, false)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
	        	saveAnnouncement(announcement, loggedInAdmin)
	        	ActivityLog.saveActivity((I18n.t :ANNOUNCEMENT_CREATE_ACTIVITY_MSG), loggedInAdmin.id, nil, params)
	        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        	response.Response_Msg=(I18n.t :ANNOUNCEMENT_SUCCESS_CREATE_MSG) 
	        return response
        end

	end	


	def self.saveAnnouncement(announcement, loggedInAdmin)

		ActiveRecord::Base.transaction do
			announcementData = Announcement.new
				announcementData.announcement = announcement
				announcementData.created_by = loggedInAdmin.id
				announcementData.txn_date = Time.now
			announcementData.save	
		end

	end	


	def self.getLatestAnnouncement(agent)

		response = validate(nil, agent, true)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
	        lastAnnouncement = Announcement.all.last
	        
	        if lastAnnouncement.present?	
	        		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        		response.Response_Msg=(lastAnnouncement.announcement) 
	        	return response
	        else
	        	response.Response_Code=(Error::NO_HISTORY)
	        		response.Response_Msg=(I18n.t :NO_ANNOUNCENT_EXIST_MSG) 
	        	return response
	        end	
        end

	end	

	def self.validate(announcement, currentAgent, isAgentCheck)
      
        response = Response.new
        
        if !isAgentCheck  
	        if announcement.eql? Constants::BLANK_VALUE
	                Log.logger.info("At line #{__LINE__} and announcement data is come nil from the web")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response
	        end    
	    else
	    	unless currentAgent.present?
                    AppLog.logger.info("At line #{__LINE__} and agent is not present")
                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
                return response
            end
	    end    

        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        response.Response_Msg=("") 
      return response  
    end
end
