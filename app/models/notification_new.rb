class NotificationNew < ApplicationRecord

	def self.getNotificationsList(currentAgent)
		AppLog.logger.info("At line #{__LINE__} inside the NotificationNew's and getNotificationsList method is called")
	    response = Response.new
	    unless currentAgent.present?
	        AppLog.logger.info("At line #{__LINE__} and agent is not present")
	        response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	      return response
	    end
	    AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")
	    
	    notificationsList = NotificationNew.where("user_id = #{currentAgent.id}").last(AppConstants::TOTAL_NOTIFICATIONS_DISPLAY_VALUE).reverse
        AppLog.logger.info("At line #{__LINE__} and check that the agent has notifications or not = #{notificationsList.present?}")	

        if notificationsList.present?
        		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.object=(notificationsList)
      		return response
        else
        		response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
        end	
	end	

	def self.updateNotificationReadFlag(currentAgent)
		AppLog.logger.info("At line #{__LINE__} inside the NotificationNew's and updateNotificationReadFlag method is called")
	    response = Response.new
	    unless currentAgent.present?
	        AppLog.logger.info("At line #{__LINE__} and agent is not present")
	        response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	      return response
	    end
	    AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")
	    
	    notificationsList = NotificationNew.where("user_id = #{currentAgent.id}")
        AppLog.logger.info("At line #{__LINE__} and check that the agent has notifications or not = #{notificationsList.present?}")	

        if notificationsList.present?
        	notificationsList.each do |notification|
          		notification.update_attribute(:read_flag, AppConstants::NOTIFICATION_READ_FLAG_VALUE)
        	end
        		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.Response_Msg=(I18n.t :SUCCESS_READ_NOTIFICATION_MSG)
      		return response
        else
        		response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
        end	
	end	

end
