class CreditFeeConfig < ApplicationRecord

	def self.getCreditFeeDetalis(params, loggedInAdmin)
		response = Response.new
		Log.logger.info("At line #{__LINE__} and inside the CreditFeeConfig's class getCreditFeeDetalis method is called")
		
		whichMF = params[:whichMF]
		mfPhone = params[:mfPhone]
		Log.logger.info("At line #{__LINE__} and parameters are as micro franchisee type is = #{whichMF} and mfPhone = #{mfPhone}")

		if whichMF.eql? Constants::SINGLE_MF_RADIO_LABEL
			if !mfPhone.present?
	        		Log.logger.info("At line = #{__LINE__} and mfPhone is come nil from the web")
	        		response.Response_Code=(Error::INVALID_DATA)
	        		response.Response_Msg=(I18n.t :INVALID_DATA_MSG)
	      		return response
	  		end

	  		currentAgent = Utility.getCurrentAgentFromPhone(mfPhone)
	  		unless currentAgent.present?
	        		Log.logger.info("At line #{__LINE__} and agent is not present with this phone = #{mfPhone}")
	        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG)
	      		return response
	    	end
	    	return getMFCreditFeeDetails(currentAgent)
	    else
	    		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.user=("")
        		response.object=(getDefaultCreditDetails) 
      		return response
	    end	
	end

	def self.getMFCreditFeeDetails(currentAgent)
		response = Response.new
		mfCreditFeeConfig = getAgentCreditFeeDetails(currentAgent.id)
		
		if mfCreditFeeConfig.present?
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.user=(currentAgent)
        		response.object=(mfCreditFeeConfig) 
      		return response
		else
				defaultCreditDetails = getDefaultCreditDetails
				defaultCreditDetails.each do |creditFee|
					creditFee.approval_req = Constants::YES_LABEL
					creditFee.fee = 0
				end	
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.status_flag=(Constants::BOOLEAN_TRUE_LABEL)
				response.user=(currentAgent)
        		response.object=(defaultCreditDetails) 
      		return response
		end	
	end

	def self.getDefaultCreditDetails
			defaultCreditDetails = CreditFeeConfig.where("user_id = #{Constants::ANONYMOUS_LOGIN_USER_ID}").order('amount asc')
		return 	defaultCreditDetails
	end

	def self.getAgentCreditFeeDetails(agentID)
			agentCreditFeeDetails = CreditFeeConfig.where("user_id = #{agentID}").order('amount asc')
		return 	agentCreditFeeDetails
	end


	def self.updateCreditFee(params, loggedInAdmin)
		response = Response.new
		Log.logger.info("Inside the Credit Fee Config's updateCreditFee method at line #{__LINE__}")

		counter = params[:count]
		whichMF = params[:whichMF]
		agentID = params[:agentID]
		Log.logger.info("At line = #{__LINE__} and the total number of records for the setup =#{counter}, whichMF = #{whichMF} and agentID = #{agentID}")	
		
		ActiveRecord::Base.transaction do
			if whichMF.eql? Constants::ALL_MF_RADIO_LABEL
				for i in 1..counter.to_i
					response = updateCreditFeeDetails(params['amount'+i.to_s], params['approvalReq'+i.to_s], params['creditFee'+i.to_s], Constants::ANONYMOUS_LOGIN_USER_ID)
					if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
						break
					end	
				end	
				return response
			else
				unless agentID.present?
		        		Log.logger.info("At line #{__LINE__} agentID is come nil from the software")	
		                response.Response_Code=(Error::INVALID_DATA)
		                response.Response_Msg=(I18n.t :FIRST_GET_DETAILS_MSG) 
		            return response    
		 		end
				mfCreditFeeConfig = getAgentCreditFeeDetails(agentID)
				if mfCreditFeeConfig.present?
					for i in 1..counter.to_i
						response = updateCreditFeeDetails(params['amount'+i.to_s], params['approvalReq'+i.to_s], params['creditFee'+i.to_s], agentID)
						if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
							break
						end	
					end	
					return response
				else
					for i in 1..counter.to_i
						response = insertCreditFeeDetails(params['amount'+i.to_s], params['approvalReq'+i.to_s], params['creditFee'+i.to_s], agentID)
					end	
					return response
				end	
			end	

			ActivityLog.saveActivity((I18n.t :MF_CREDIT_FEE_UPDATE_ACTIVITY_MSG), loggedInAdmin.id, agentID, params)
		end	
	end	

	def self.insertCreditFeeDetails(amount, approvalReq, creditFee, agentID)
		response = Response.new	
			creditFeeConfig = CreditFeeConfig.new
			creditFeeConfig.amount = amount
			creditFeeConfig.approval_req = approvalReq
			creditFeeConfig.fee = creditFee
			creditFeeConfig.status = 0
			creditFeeConfig.user_id = agentID
			creditFeeConfig.save

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.Response_Msg=(I18n.t :CREDIT_FEE_UPDATE_MSG)
		return response	
	end

	def self.updateCreditFeeDetails(amount, approvalReq, creditFee, agentID)
		response = Response.new
			query = "update credit_fee_configs set approval_req = '#{approvalReq}', fee = #{creditFee} where amount = #{amount} AND user_id = #{agentID}"
			isSuccessExecute = Utility.executeSQLQuery(query)
			if isSuccessExecute == Constants::BOOLEAN_FALSE_LABEL
				response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	    		response.Response_Msg=(I18n.t :INTERNAL_SERVER_ERROR_MSG)
			else
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.Response_Msg=(I18n.t :CREDIT_FEE_UPDATE_MSG)
			end	
	 	return response  
	end

	def self.getColor(approvalReq)
		if approvalReq.eql? Constants::LOAN_APPROVAL_REQ_VALUE # if the approval is true
			return 'background: #d1f3ec'
		end	
	end

	def self.getCreditConfig(amount, agentID)
		mfCreditFeeConfig = getAgentCreditFeeDetails(agentID)
		if mfCreditFeeConfig.present?
			addQuery = "AND user_id = #{agentID}"		
		else
			addQuery = "AND user_id = #{Constants::ANONYMOUS_LOGIN_USER_ID}"
		end	
		query = "select approval_req, fee from credit_fee_configs where amount = #{amount} #{addQuery}"
		queryResult = Utility.executeSQLQuery(query)
		if queryResult.present?
			return queryResult[0]
		else
			return nil
		end	
	end	

	def self.isCreditAutoApproval(creditConfig)
		if creditConfig.present?
			if creditConfig['approval_req'].eql? Constants::LOAN_APPROVAL_REQ_VALUE
				return Constants::BOOLEAN_TRUE_LABEL
			else
				return Constants::BOOLEAN_FALSE_LABEL
			end	
		else
			return Constants::BOOLEAN_TRUE_LABEL
		end	
	end	

	def self.getCreditFee(creditConfig)
		if creditConfig.present?
			return creditConfig['fee']
		else
			return Constants::USER_INITIAL_LOAN_AMOUNT # This is for the zero
		end	
	end	

end