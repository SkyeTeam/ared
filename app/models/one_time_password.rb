class OneTimePassword < ApplicationRecord

	validates :otp, length: { minimum: 6 }, if: -> { new_record? || changes[:otp] }
	
	def self.getOTPID

		otp = OneTimePassword.all.last

		if otp.present?
			lastOTPId = otp.otp_id

			splitArray = lastOTPId.split("LOGINOTP-",2)

			intID = splitArray[1]

			intID = intID.to_i + 1

			nextOTPId = "LOGINOTP-" + intID.to_s

			return nextOTPId

		else
			return Constants::OTP_START_ID
		end	

	end	

end
