class AgentCreditLimit < ApplicationRecord
	has_one :user
  require 'constants_helper'
	
  def self.expirechallenge

    Log.logger.info("Agent Credit Limit function of schedular is called at line #{__LINE__}")

    Log.logger.info("At line = #{__LINE__} and today date is = #{Date.today}")
		
    two_months_old_date = Date.today - 2.month
		two_months_old_date_beginning = two_months_old_date.beginning_of_day

    Log.logger.info("At line = #{__LINE__} and two months old date from now = #{two_months_old_date_beginning}")

    three_months_old_date = Date.today - 3.month
    three_months_old_date_beginning = three_months_old_date.beginning_of_day

    Log.logger.info("At line = #{__LINE__} and three months old date from now = #{three_months_old_date_beginning}")

    four_months_old_date = Date.today - 4.month
    four_months_old_date_beginning = four_months_old_date.beginning_of_day

    Log.logger.info("At line = #{__LINE__} and four months old date from now = #{four_months_old_date_beginning}")

    five_months_old_date = Date.today - 5.month
    five_months_old_date_beginning = five_months_old_date.beginning_of_day

    Log.logger.info("At line = #{__LINE__} and five months old date from now = #{five_months_old_date_beginning}")
                              
    agent_credit_limit2=AgentCreditLimit.where("loan_capacity_date <= ? AND loan_amount = 5000 AND status = 0",two_months_old_date_beginning)
    	if agent_credit_limit2 != nil
     	  agent_credit_limit2.each do |a|
          getagentcreditlimit(a,a.user_id,ConstantsHelper::TWO_MONTHS_OLD_LOAN_LIMIT)
	 	    end	
      end

    agent_credit_limit3=AgentCreditLimit.where("loan_capacity_date <= ? AND loan_amount = 10000 AND status = 0",three_months_old_date_beginning)
      if agent_credit_limit3 != nil
        agent_credit_limit3.each do |a|
          getagentcreditlimit(a,a.user_id,ConstantsHelper::THREE_MONTHS_OLD_LOAN_LIMIT)
        end 
      end

    agent_credit_limit4=AgentCreditLimit.where("loan_capacity_date <= ? AND loan_amount = 20000 AND status = 0",four_months_old_date_beginning)
      if agent_credit_limit4 != nil
        agent_credit_limit4.each do |a|
          getagentcreditlimit(a,a.user_id,ConstantsHelper::FOUR_MONTHS_OLD_LOAN_LIMIT)
        end
      end

    agent_credit_limit5=AgentCreditLimit.where("loan_capacity_date <= ? AND loan_amount = 50000 AND status = 0",five_months_old_date_beginning )
      if agent_credit_limit5 != nil
        agent_credit_limit5.each do |a|
          getagentcreditlimit(a,a.user_id,ConstantsHelper::FIVE_MONTHS_OLD_LOAN_LIMIT)
        end 
      end

		end

    def self.getagentcreditlimit(user,user_id,amount)
      
      ActiveRecord::Base.transaction do
        
       agent_credit_limit = AgentCreditLimit.new
       agent_credit_limit.user_id=user_id
       agent_credit_limit.loan_capacity_date = getFirstLoanCapacityDate(user_id)
       agent_credit_limit.loan_amount = amount
       agent_credit_limit.status = 0
    
       agent_credit_limit.save
    
       user.update_attribute(:status, 1)
    
      end  
    end

    def self.getFirstLoanCapacityDate(user_id)
      agent = AgentCreditLimit.where('user_id = ?',user_id).first
      if agent.present?
        return agent.loan_capacity_date
      else 
        return Date.today
      end  
    end  

  end 

