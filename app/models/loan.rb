class Loan < ApplicationRecord
  has_one :user

  def self.loannotification
    Log.logger.info("At line #{__LINE__} inside the Loan class and loannotification function of schedular is called for send the notification to all the agents whose loan balance is pending")
  	
    activeAgents = User.where("role = #{Constants::MF_USER_ROLE} AND status = #{Constants::USER_ACTIVE_STATUS}")
    activeAgents.each do |currentAgent|
          
      loanBalance = Utility.getAgentLoanBalance(currentAgent)
      if loanBalance.to_f > 0
        Log.logger.info("At line #{__LINE__} and details of the agent whose loan is pending are agentID = #{currentAgent.id}, agentName = #{currentAgent.name}, phone = #{currentAgent.phone} and loan balance = #{loanBalance}")
        agentLastLoanDueDateTime = DateTime.parse(Utility.getAgentLastLoanDueDate(currentAgent).to_s)

        sendLoanNotification(currentAgent.id, agentLastLoanDueDateTime, loanBalance)
        Log.logger.info("At line #{__LINE__} and  loan taken notification is send successfully to the agent")
      end
  			
  	end
  end	

  def self.sendLoanNotification(agentID, agentLastLoanDueDateTime, loanBalance)
    notified = "Your credit amount  RWF " + (Utility.getFormatedAmount(loanBalance)).to_s + " is pending and "
    notified << "you can pay till the due date " + agentLastLoanDueDateTime.to_s
    title = "Credit Due notification"

    Log.logger.info("At line #{__LINE__} and notification details are as notification is = #{notified} and title is = #{title}")

    Utility.sendNotification(agentID, notified, title)
  end

  def self.loanunpaidnotification
    Log.logger.info("At line #{__LINE__} inside the Loan class and loanunpaidnotification function of schedular is called for apply penalty")
    
    activeAgents = User.where("role = #{Constants::MF_USER_ROLE} AND status = #{Constants::USER_ACTIVE_STATUS}")
    
    activeAgents.each do |currentAgent|
          
      loanBalance = Utility.getAgentLoanBalance(currentAgent)


      if loanBalance.to_f > 0
        Log.logger.info("At line #{__LINE__} and details of the agent whose loan is pending are agentID = #{currentAgent.id}, agentName = #{currentAgent.name}, phone = #{currentAgent.phone} and loan balance = #{loanBalance}")
             
        agentLastLoanDueDateTime = DateTime.parse(Utility.getAgentLastLoanDueDate(currentAgent).to_s)
        todayDateTime =  DateTime.now
        differnceOfDays = (todayDateTime - agentLastLoanDueDateTime).to_i
        Log.logger.info("At line #{__LINE__} agent agentLastLoanDueDateTime is = #{agentLastLoanDueDateTime}, todayDateTime is = #{todayDateTime} and differnceOfDays are = #{differnceOfDays}")
            
        if todayDateTime > agentLastLoanDueDateTime
          differenceOfHours = ((Time.parse(todayDateTime.to_s) -  Time.parse(agentLastLoanDueDateTime.to_s))/3600).to_i
          Log.logger.info("At line #{__LINE__} and differenceOfHours are = #{differenceOfHours}")

          if differenceOfHours.to_i >= 12 && differenceOfHours.to_i <= 48
            appliedPenaltyAmount = ConstantsHelper::PENALTY_CHARGES
            Log.logger.info("At line #{__LINE__} and inside the Penalty apply block and #{appliedPenaltyAmount} charges are applied to the agent")
            appliedPenalty(currentAgent, loanBalance, agentLastLoanDueDateTime, appliedPenaltyAmount)
          elsif differenceOfHours.to_i > 48
            Log.logger.info("At line #{__LINE__} and inside the agent suspended account block")
            suspendAgentAccount(currentAgent, loanBalance)
          end
        end
      end  
          
    end 
  end
 
  def self.appliedPenalty(currentAgent, loanBalance, agentLastLoanDueDate, appliedPenaltyAmount)
    Log.logger.info("At line #{__LINE__} inside the Loan class and appliedPenalty method is called")
    agentPendingPenalty = Utility.getAgentPenaltyBalance(currentAgent)
    totalPenaltyAmount = TxnHeader.my_money(agentPendingPenalty).to_f + TxnHeader.my_money(appliedPenaltyAmount).to_f
    
    ActiveRecord::Base.transaction do
      agentPenaltyDetail = AgentPenaltyDetail.new
        agentPenaltyDetail.user_id = currentAgent.id
        agentPenaltyDetail.loan_amount = loanBalance
        agentPenaltyDetail.penalty_amount = TxnHeader.my_money(appliedPenaltyAmount).to_f
        agentPenaltyDetail.pending_penalty_amount = TxnHeader.my_money(totalPenaltyAmount).to_f
      agentPenaltyDetail.save
      Log.logger.info("At line #{__LINE__} and agent penalty is saved in AgentPenaltyDetail table successfully")

      agentName = currentAgent.name.to_s + " " + currentAgent.last_name.to_s 
      ActivityLog.saveActivity((I18n.t :SCHEDULAR_PENALTY_APPLY_ACTIVITY_MSG, :penalty => Utility.getFormatedAmount(appliedPenaltyAmount), :agentName => agentName), ConstantsHelper::ANONYMOUS_USER_ID, currentAgent.id, nil)
      Log.logger.info("At line #{__LINE__} and penalty apply by the schedular's activity is saved in ActivityLog table successfully")
         
      sendPenalityApplyNotification(loanBalance, totalPenaltyAmount, agentLastLoanDueDate, currentAgent.id, agentName)
      Log.logger.info("At line #{__LINE__} and  penalty apply notification is send successfully to the agent")
    end   
  end  

  def self.suspendAgentAccount(currentAgent, loanBalance)
    Log.logger.info("At line #{__LINE__} inside the Loan class and suspendAgentAccount method is called to suspend the agent because of non payment of the credit")

    if currentAgent.status.eql? Constants::ACTIVE_DB_LABEL
      currentAgent.update_attribute(:status, Constants::USER_SUSPEND_STATUS)
      Log.logger.info("At line #{__LINE__} and agent is suspend successfully and update his status #{Constants::USER_SUSPEND_STATUS} in the user table")

      agentName = currentAgent.name.to_s + " " + currentAgent.last_name.to_s
      ActivityLog.saveActivity((I18n.t :SCHEDULAR_SUSPEND_AGENT_ACTIVITY_MSG, :agentName => agentName, :loanBalance => Utility.getFormatedAmount(loanBalance)), ConstantsHelper::ANONYMOUS_USER_ID, currentAgent.id, nil)
      Log.logger.info("At line #{__LINE__} and agent suspended by the schedular's activity is saved in ActivityLog table successfully")
      
      sendAgentSuspendNotification(loanBalance, currentAgent.id)
      Log.logger.info("At line #{__LINE__} and agent suspended notification is send successfully to the agent")
      
      message = (I18n.t :AGENT_SUSPEND_EMAIL_MSG, :agentName => (currentAgent.name.to_s + ' ' + currentAgent.last_name.to_s), :agentPhone => currentAgent.phone, :amount => Utility.getFormatedAmount(loanBalance))
      Utility.sendEmail(Properties::SUPERADMIN_EMAIL, Properties::LOAN_UNPAID_EMAIL, (I18n.t :AGENT_SUSPEND_EMAIL_TITLE), message)
      Log.logger.info("At line #{__LINE__} and agent suspended email is send successfully to the admin")
    end  
  end  

  def self.sendPenalityApplyNotification(balance, pending_penalty, loan_due_date, user_id, agent_name)
    notified = "Dear " + agent_name +          
                " . You have an outstanding credit of RWF "+ (Utility.getFormatedAmount(balance)).to_s + " and its due date was " + loan_due_date.to_s +
                ".We request you to kindly deposit this amount with late charges of RWF " + (Utility.getFormatedAmount(pending_penalty)).to_s
    title = "Penalty Notification"

    Log.logger.info("At line #{__LINE__} and notification details are as notification is = #{notified} and title is = #{title}")
   
    Utility.sendNotification(user_id,notified,title)
  end

  def self.sendAgentSuspendNotification(loanBalance, agentID)
    notified = "Your account has been suspended due to non payment of credit amount = "+(Utility.getFormatedAmount(loanBalance)).to_s
    title = "Account Suspend Notification"

    Log.logger.info("At line #{__LINE__} and notification details are as notification is = #{notified} and title is = #{title}")

    Utility.sendNotification(agentID, notified, title)
  end

end