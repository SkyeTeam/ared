class Role < ApplicationRecord

    def self.getAssignedRolesName(userID)
        Log.logger.info("At line #{__LINE__} inside the Role class and getAssignedRolesName method is called")

        unless userID.present?
          Log.logger.info("At line #{__LINE__} userID is empty")
          return ''
        end  

        assignedRoleName = Array.new

        assignedRoles = getAssignedRolesIDs(userID)
      	assignedRoles.each do |role|
      		assignedRoleName.push(getRoleName(role.role_id))
      	end	

      return assignedRoleName
    end 

	  def self.getAssignedRolesIDs(userID)
    	  Log.logger.info("At line #{__LINE__} inside the Role class and getAssignedRolesIDs method is called")

    	  unless userID.present?
          Log.logger.info("At line #{__LINE__} userID is empty")
          return ''
        end  

        assignedRolesIDs = RoleHistory.where('user_id = ?',userID).select('role_id')
      return assignedRolesIDs
    end    

    def self.getRoleName(roleID)
        Log.logger.info("At line #{__LINE__} inside the Role class and roleID method is called")
        
        unless roleID.present?
          Log.logger.info("At line #{__LINE__} and no role exist with this id = "+roleID)
          return ''
        end  
        
        role = Role.find(roleID)
      return role.role_name
    end 

    def self.getUniqueRoleCategory(loggedUserID)
    	
      roleIdResult = getRoleID(loggedUserID)
     
      unless roleIdResult.present?
        Log.logger.info("Inside the Role class at line #{__LINE__} and no roleid's exist in the RoleHistory table.")
        return ""
      end  

      assignedRoleIds = getAssignedRolesArray(roleIdResult)

      Log.logger.info("Inside the Role class at line #{__LINE__} and roleid's exist in the RoleHistory table are = #{assignedRoleIds}")

      result  = Utility.executeSQLQuery(getSQLQuery(assignedRoleIds))
    	return result
    
    end	


    def self.getAssignedRolesArray(roleIdResult)
        @assignedRoleIds = Array.new

        roleIdResult.each do |roleId|
            @assignedRoleIds.push(roleId['role_id'])
        end  
      return @assignedRoleIds 
    end  


    def self.getSQLQuery(assignedRoleIds)

        isSuperAdmin = checkSuperAdminStatus(assignedRoleIds)

        if isSuperAdmin 
          query = "SELECT DISTINCT role_type, order_view FROM roles WHERE role_type != 'Super Admin' ORDER BY order_view ASC"
        else  
          query = "SELECT DISTINCT role_type, order_view FROM roles WHERE " + getAddSQLQuery(assignedRoleIds) +  " ORDER BY order_view ASC"
        end

      return query  

    end  


    def self.checkSuperAdminStatus(assignedRoleIds)

        isSuperAdmin = false
        # If the user is superadmin then he has only one role with role id = 0
        if assignedRoleIds.size == 1
          if assignedRoleIds.first == Constants::SUPER_ADMIN_ROLE_ID
            isSuperAdmin = true
          end   
        end  
      return  isSuperAdmin 
    end  

    def self.isSuperAdminRoleAssign(adminID)
        isSuperAdmin = false
        roleHistory = RoleHistory.find_by_user_id(adminID)
        
        if roleHistory.present?
          if roleHistory.role_id == Constants::SUPER_ADMIN_ROLE_ID
            isSuperAdmin = true
          end  
        end
      return isSuperAdmin  
    end    

    
    def self.getRoleID(loggedUserID)
      query = "SELECT role_id FROM role_histories WHERE user_id = '"+loggedUserID+"'"
      result  = Utility.executeSQLQuery(query)
      Log.logger.info("Inside the Role class at line #{__LINE__} and logged user id = #{loggedUserID} and check is query result is present or not = #{result.present?}")
      return result
    end  


    def self.getCategoryRoles(categoryName)
      assignedRoles = @assignedRoleIds
    	isSuperAdmin = checkSuperAdminStatus(assignedRoles)
      
      if isSuperAdmin
        query = "SELECT role_name,controller,action,role_sub_name,route FROM roles WHERE role_type = '"+categoryName+"' AND status = 0 ORDER BY role_name ASC"
      else
        query = "SELECT role_name,controller,action,role_sub_name,route FROM roles WHERE " + getAddRoleSQLQuery(assignedRoles,categoryName) + " ORDER BY role_name ASC"  
      end 

      result  = Utility.executeSQLQuery(query)
    
    	return result
    
    end	


    def self.getAddSQLQuery(assignedRoles)
        orStatus = ""
        queryAddId = ""
          
          for i in 0..assignedRoles.size.to_i - 1
            if i < assignedRoles.size-1
              orStatus = " OR "
            else
              orStatus = ""
            end  
            queryAddId = queryAddId.to_s + "id = " + assignedRoles[i].to_s + orStatus 
          end
      return  queryAddId   
    end  

    def self.getAddRoleSQLQuery(assignedRoles,serviceName)
        orStatus = ""
        queryAddId = ""
          
          for i in 0..assignedRoles.size.to_i - 1
            if i < assignedRoles.size-1
              orStatus = " OR "
            else
              orStatus = ""
            end  
            queryAddId = queryAddId.to_s + "role_type = '" + serviceName.to_s + "' AND id = " + assignedRoles[i].to_s + orStatus 
          end
      return  queryAddId   
    end  


    def self.getRoleCategoryIconClass(categoryName)
      if categoryName.eql? Constants::USER_ROLE_CAT_LABEL
        return 'fa fa-user'
      elsif categoryName.eql? Constants::FLOAT_ROLE_CAT_LABEL
        return 'fa fa-money'
      elsif categoryName.eql? Constants::SERVICES_ROLE_CAT_LABEL
        return 'fa fa-gears'  
      elsif categoryName.eql? Constants::ANALYTIC_ROLE_CAT_LABLE
        return 'fa fa-bar-chart-o' 
      elsif categoryName.eql? Constants::KIOSK_ROLE_CAT_LABEL
        return 'fa fa-shopping-cart'
      else
        return ''
      end 
    end  
    
end
