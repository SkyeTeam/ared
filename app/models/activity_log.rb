class ActivityLog < ApplicationRecord

	def self.saveActivity(action, initiator, description, params)
		activityLog = ActivityLog.new
			activityLog.action = action
			activityLog.initiator = initiator
			activityLog.description = description
			activityLog.params = params #.to_json
		activityLog.save
	end

end
