class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.search(search)
    where("title LIKE ?", "%#{search}%")
  end
end
