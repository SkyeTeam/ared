class AccessLog < ApplicationRecord

	def self.getAccessID

		accessLog = AccessLog.all.last

		if accessLog.present?

			lastAccessId = accessLog.access_id

			splitArray = lastAccessId.split("ACCESS-",2)

			intID = splitArray[1]

			intID = intID.to_i + 1

			nextAccessId = "ACCESS-" + intID.to_s

			return nextAccessId

		else
			return Constants::ACCESS_LOG_START_ID
		end	

	end	

	def self.fetchDetails(params, loggedInAdmin, request)
		
		Log.logger.info("Inside the Access Log class and fetchDetails method is called at line = #{__LINE__}")
			
			logType 	= params[:reportType]
			user 		= params[:user]
			userType 	= params[:userType]
			userPhone 	= params[:userPhone]
			fromDate 	= params[:from_date]
			toDate 	= params[:to_date]

			Log.logger.info("At line #{__LINE__} and parametes are logType = #{logType}, user = #{user}, userType = #{userType}, userPhone = #{userPhone}, fromDate = #{fromDate} and toDate = #{toDate}")

			response = validate(logType, user, userType, userPhone, fromDate, toDate)
		
			if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	        	return response
	        end

	        userID = response.Response_Msg
	       	fromDate = Date.parse(fromDate.to_s).beginning_of_day
		    toDate = Date.parse(toDate.to_s).end_of_day

		    detailsHash = getDetails(logType, user, userType, userID, fromDate, toDate)

        	if detailsHash.present?
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.detailsHash=(detailsHash)
			else
				response.Response_Code=(Error::NO_HISTORY)
			   	response.Response_Msg=(I18n.t :NO_HISTORY_MSG) 
			end	

      	return response  

	end	

	def self.getDetails(logType, user, userType, userID, fromDate, toDate)

			if logType.eql? Constants::ACCESS_LOGS_REPORT_LABEL
				query = getSQLQueryAccessLog(user, userType, userID, fromDate, toDate)
			elsif logType.eql? Constants::ACTIVITY_LOG_REPORT_LABEL
				query = getSQLQueryActivityLog(user, userType, userID, fromDate, toDate)
			else
				query = getSQLQueryTxnLog(user, userType, userID, fromDate, toDate)
			end	

			detailsHash = Utility.executeSQLQuery(query)

		return detailsHash
	
	end	

	def self.getSQLQueryAccessLog(user, userType, userID, fromDate, toDate) 
			
		if user.eql? Constants::ALL_USER_LABEL
			roleQuery = "a.role IN (#{Constants::ADMIN_USER_ROLE}, #{Constants::MF_USER_ROLE})"
		elsif user.eql? Constants::ADMIN_DB_LABEL
			roleQuery = "a.role = #{Constants::ADMIN_USER_ROLE}"
		else
			roleQuery = "a.role = #{Constants::MF_USER_ROLE}"	
		end	

		if  userType.eql? Constants::SINGLE_USER_RADIO_LABEL and !(user.eql? Constants::ALL_USER_LABEL)
			singleUserQuery = " AND user_id = #{userID}"
		else
			singleUserQuery = ""
		end	

		query = "SELECT a.created_at AT TIME ZONE '#{Properties::TIME_ZONE}' AS created_at, a.user_id, u.name, u.last_name, u.phone, a.role, a.is_successful, a.remote_ip, a.use_web "
		query << "FROM access_logs a, users u "
		query << "WHERE u.id = a.user_id AND  #{roleQuery} #{singleUserQuery} AND "
		query << "a.created_at >= '#{fromDate}' AND a.created_at <= '#{toDate}' "
		query << "ORDER BY a.created_at DESC"

		if !Properties::IS_PRODUCTION_SERVER
			Log.logger.info("At line = #{__LINE__} and SQL Query is = #{query}")
		end	

		return query	
	
	end	

	def self.getSQLQueryActivityLog(user, userType, userID, fromDate, toDate)

		if userType.eql? Constants::SINGLE_USER_RADIO_LABEL and !(user.eql? Constants::ALL_USER_LABEL)
			singleUserQuery = " AND activity.initiator::INT = #{userID}"
		else
			singleUserQuery = ""
		end	

		query = "SELECT activity.created_at AT TIME ZONE '#{Properties::TIME_ZONE}' AS activitydate, activity.action, whoDid.name AS whodidfname, whoDid.last_name AS whodidlname, whoDid.phone AS whodidphone, "
		query << "onWhich.name AS onwhichfname, onWhich.last_name AS onwhichlname, onWhich.phone AS onwhichphone "
		query << "FROM users whoDid, activity_logs activity LEFT JOIN users onWhich ON activity.description::INT = onWhich.id "
		query << "WHERE activity.initiator::INT = whoDid.id #{singleUserQuery} "
		query << "AND activity.created_at >= '#{fromDate}' AND activity.created_at <= '#{toDate}' "
		query << "ORDER BY activity.created_at DESC"

		if !Properties::IS_PRODUCTION_SERVER
			Log.logger.info("At line = #{__LINE__} and SQL Query is = #{query}")
		end	

		return query	
		
	end

	def self.getSQLQueryTxnLog(user, userType, userID, fromDate, toDate)
	
		if  userType.eql? Constants::SINGLE_USER_RADIO_LABEL and !(user.eql? Constants::ALL_USER_LABEL)
			if user.eql? Constants::ADMIN_DB_LABEL
				singleUserQuery = "AND user_id = #{userID}"
			else
				singleUserQuery = "AND agent_id = #{userID}"
			end	
		else
			singleUserQuery = ""
		end	

		if user.eql? Constants::ALL_USER_LABEL
			subQuery = getAllUserTxnLogQuery(user, userType, userID, fromDate, toDate)
			return subQuery
		elsif user.eql? Constants::ADMIN_DB_LABEL
			subQuery =  "WHERE u.id = p.user_id AND  p.role = #{Constants::ADMIN_USER_ROLE} #{singleUserQuery}"
		else
			subQuery =  "WHERE u.id = p.agent_id AND  p.role = #{Constants::MF_USER_ROLE} #{singleUserQuery}"
		end	
		
		query = "SELECT p.agent_id, u.name, u.last_name, u.phone, p.service_type, p.sub_type, p.client_number, p.amount, p.status, p.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, p.remote_ip, p.role "
		query << "FROM pending_requests p, users u #{subQuery} "
		query << "AND p.txn_date >= '#{fromDate}' AND p.txn_date <= '#{toDate}' "
		query << "ORDER BY p.txn_date DESC"

		if !Properties::IS_PRODUCTION_SERVER
			Log.logger.info("At line = #{__LINE__} and SQL Query is = #{query}")
		end	

		return query
	
	end	

	def self.getAllUserTxnLogQuery(user, userType, userID, fromDate, toDate)

		adminQuery = "SELECT p.agent_id, u.name, u.last_name, u.phone, p.service_type, p.sub_type, p.client_number, p.amount, p.status, p.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, p.remote_ip, p.role "
		adminQuery << "FROM pending_requests p, users u "
		adminQuery << "WHERE u.id = p.agent_id AND  p.role = #{Constants::MF_USER_ROLE} "
		adminQuery << "AND p.txn_date >= '#{fromDate}' AND p.txn_date <= '#{toDate}'"

		mfQuery = "SELECT p.agent_id, u.name, u.last_name, u.phone, p.service_type, p.sub_type, p.client_number, p.amount, p.status, p.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, p.remote_ip, p.role "
		mfQuery << "FROM pending_requests p, users u "
		mfQuery << "WHERE u.id = p.user_id AND p.role = #{Constants::ADMIN_USER_ROLE} "
		mfQuery << "AND p.txn_date >= '#{fromDate}' AND p.txn_date <= '#{toDate}' "
		mfQuery << "ORDER BY txn_date DESC"			

		query = adminQuery + " \n UNION ALL \n" + mfQuery

		if !Properties::IS_PRODUCTION_SERVER
			Log.logger.info("At line = #{__LINE__} and SQL Query is = #{query}")
		end	

		return query
	
	end	
	
	def self.validate(logType, user, userType, userPhone, fromDate, toDate)
		response = Response.new
		userID = Constants::BLANK_VALUE
        
	        if !logType.present? || !fromDate.present? || !toDate.present?
                	Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		if userType.eql? Constants::SINGLE_USER_RADIO_LABEL and !(user.eql? Constants::ALL_USER_LABEL)
	 			if !userPhone.present? || !user.present? || !userType.present? 
                		Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
	                	response.Response_Code=(Error::INVALID_DATA)
	                	response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            	return response  
	            end	

	            currentUser = Utility.getCurrentAgentByPhone(userPhone)
	            if currentUser.present?
	             	if currentUser.role.eql? user 
	            		userID = currentUser.id
	            	else
	            			response.Response_Code=(Error::OTHER_USER_PHONE)
		   					response.Response_Msg=(I18n.t :OTHER_USER_PHONE_MSG, :userRole => User.getRoleDisValue(user)) 
		           		return response
	            	end	
	            else	
	            		Log.logger.info("At line = #{__LINE__} and User does not exist with this phone = #{userPhone}")
	                	response.Response_Code=(Error::USER_NOT_EXIST)
		   				response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
		           return response
	            end	
	 		end

		 	fromDate = Date.parse(fromDate.to_s).beginning_of_day
		    toDate = Date.parse(toDate.to_s).end_of_day

            if fromDate > toDate
            		Log.logger.info("At line = #{__LINE__} and From Date is greater then the To Date")
	       			response.Response_Code=(Error::CHECK_DATES_AGAIN)
		    		response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
		    	return response
			end	

			if !Utility.isValidYear(fromDate.to_s)
        			Log.logger.info("At line = #{__LINE__} and from date has not a valid year ")
       				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

			if !Utility.isValidYear(toDate.to_s)
        			Log.logger.info("At line = #{__LINE__} and to date has not a valid year ")
       				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=(userID) 

      	return response  
	end

	def self.getLoginStatus(intLoginStatus)
		if intLoginStatus == Constants::SUCCESS_LOGIN_STATUS
			return Constants::SUCCESS_LABEL
		else	
			return Constants::FAILED_LABEL
		end	
	end

	def self.getPlatform(intWebStatus)
		if intWebStatus == Constants::LOGIN_THROUGH_WEB_STATUS
			return Constants::WEB_LABEL
		else	
			return Constants::APP_LABEL
		end	
	end

	def self.getSubUserDetails(activity)
			if activity['onwhichfname'].present?
	            subUserDetails = "(" + activity['onwhichfname'] + " " + activity['onwhichlname'] + "," +  activity['onwhichphone'] + ")"
	        else
	        	subUserDetails = ""    
	        end	
	    return subUserDetails    
	end	

end