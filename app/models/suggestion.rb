class Suggestion < ApplicationRecord

	def self.saveSuggestion(currentAgent, params, request)
		AppLog.logger.info("At line = #{__LINE__} and inside the Suggestion manager class and saveSuggestion method is called")

		suggestionType = params[""+AppConstants::SUGGESTION_TYPE_PARAM_LABEL+""]
		suggestionComments = params[""+AppConstants::SUGGESTION_COMMENTS_PARAM_LABEL+""]

		AppLog.logger.info("At line = #{__LINE__} and parameters are suggestionType = #{suggestionType} and suggestionComments = #{suggestionComments}")

		response = validate(currentAgent, suggestionType, suggestionComments, nil, nil, true)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
        	saveUserSuggestions(currentAgent, suggestionType, suggestionComments, params)
        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=(I18n.t :USER_SUGGESTION_SAVE_SUCCESS_MSG) 
      		return response  
        end
	end	

	def self.saveUserSuggestions(currentAgent, suggestionType, suggestionComments, params)
		ActiveRecord::Base.transaction do
			suggestion = Suggestion.new
				suggestion.user_id = currentAgent.id
				suggestion.type = suggestionType
				suggestion.user_comments = suggestionComments
			suggestion.save	
			AppLog.logger.info("At line = #{__LINE__} and agent suggestion is saved successfully into the suggestions table")

			ActivityLog.saveActivity((I18n.t :USER_SUGGESTION_ACTIVITY_MSG), currentAgent.id, nil, params)
			AppLog.logger.info("At line = #{__LINE__} and agent's save suggestion activity saved successfully into the ActivityLog table")
		end		
	end	

    def self.fetchDetails(params)
		Log.logger.info("At line = #{__LINE__} and inside the Suggestion manager class and fetchDetails method is called")

		suggestionType = params[:suggestionType]
		fromDate = params[:from_date]
		toDate = params[:to_date]

		Log.logger.info("At line = #{__LINE__} and parameters are suggestionType = #{suggestionType}, fromDate = #{fromDate} and toDate = #{toDate}")

		response = validate(nil, suggestionType, nil, fromDate, toDate, false)

		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end

	    suggestionList = getDetails(suggestionType, fromDate, toDate)

		if suggestionList.present?
	          response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	          response.txn_history=(suggestionList)
          	return response
        else
          		response.Response_Code=(Error::NO_SUGGESTIONS_OCCUR)
	   	  		response.Response_Msg=(I18n.t :NO_SUGGESTIONS_OCCUR_MSG) 
	   	  	return response
        end
	end	

	def self.getDetails(suggestionType, fromDate, toDate)
			fromDate = Date.parse(fromDate.first.to_s).beginning_of_day
		    toDate = Date.parse(toDate.first.to_s).end_of_day

			query = "SELECT u.name, u.last_name, u.phone, s.type, s.user_comments, s.created_at "
			query << "FROM suggestions s, users u "
			query << "WHERE u.id = s.user_id AND s.created_at >= '#{fromDate}' AND s.created_at <= '#{toDate}' AND type = '#{suggestionType}' "
			query << "ORDER BY s.created_at DESC"

			suggestionList = Utility.executeSQLQuery(query)

		return suggestionList
	end

	def self.validate(currentAgent, suggestionType, suggestionComments, fromDate, toDate, isSuggestionSave)
        response = Response.new

            if isSuggestionSave
	            unless currentAgent.present?
	                    AppLog.logger.info("At line = #{__LINE__} and inside the Suggestion manager's  validate method and agent is not present")
	                    response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	                    response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	                return response
	            end

	            if !suggestionType.present? || !suggestionComments.present?
	                    AppLog.logger.info("At line = #{__LINE__} and some data is come nil from the app")
	                    response.Response_Code=(Error::INVALID_DATA)
	                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	                return response
	        	end
	        else
	        	if !suggestionType.present? || !fromDate.present? || !toDate.present?
	                    Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
	                    response.Response_Code=(Error::INVALID_DATA)
	                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	                return response
	        	end

	        	if fromDate.first > toDate.first
	        			Log.logger.info("At line = #{__LINE__} and from date is greater then the to date")
	       				response.Response_Code=(Error::CHECK_DATES_AGAIN)
		    			response.Response_Msg=(I18n.t :CHECK_DATES_AGAIN_MSG) 
		    		return response
				end

				if !Utility.isValidYear(fromDate.first)
	        			Log.logger.info("At line = #{__LINE__} and from date has not a valid year ")
	       				response.Response_Code=(Error::INVALID_DATE)
		    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
		    		return response
				end

				if !Utility.isValidYear(toDate.first)
	        			Log.logger.info("At line = #{__LINE__} and to date has not a valid year ")
	       				response.Response_Code=(Error::INVALID_DATE)
		    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
		    		return response
				end

	        end	
        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
    end

    def self.parseSuggestion(comments)
		return comments.gsub(/\</, "").gsub(/\>/, "")
    end	

end