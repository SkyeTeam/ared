class LeasingFee < ApplicationRecord

	def self.createLeasingFee(params, loggedInAdmin)
		leasingFeeAmount = params[:amount]
		applicableFrom   = params[:applicable_from].first
		Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and createLeasingFee method is called with parameters are amount is = #{leasingFeeAmount}, applicable from = #{applicableFrom}")

		response = validate(leasingFeeAmount, applicableFrom)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
	        	saveLeasingFee(leasingFeeAmount, applicableFrom)
	        	ActivityLog.saveActivity((I18n.t :LEASING_FEE_CREATE_ACTIVITY_MSG), loggedInAdmin.id, nil, params)
	        	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        	response.Response_Msg=(I18n.t :LEASING_FEE_SUCCESS_CREATE_MSG) 
	        return response
        end
	end	

	def self.saveLeasingFee(leasingFeeAmount, applicableFrom)
		ActiveRecord::Base.transaction do
			leasingFeeData = LeasingFee.new
				leasingFeeData.fee_amount = TxnHeader.my_money(leasingFeeAmount).to_f
				leasingFeeData.applicable_from = applicableFrom
				leasingFeeData.status = 0
			leasingFeeData.save	
		end
	end	

	def self.validate(leasingFeeAmount, applicableFrom)
        response = Response.new
        
	        if !leasingFeeAmount.present? || !applicableFrom.present?
	                Log.logger.info("At line #{__LINE__} and some data is come nil from the web")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response
	        end   

	        if leasingFeeAmount.to_f <= 0
	   				Log.logger.info("At line #{__LINE__} and leasingFeeAmount is less the zero")
	                response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
	                response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
	            return response
	        end 

			if !Utility.isValidYear(applicableFrom.to_s)
	    			Log.logger.info("At line #{__LINE__} and applicableFrom has not a valid year ")
	   				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

	        if Date.parse(applicableFrom.to_s) < Date.today
	       			response.Response_Code=(Error::APPLICABLE_DATE_IN_PAST)
		    		response.Response_Msg=(I18n.t :APPLICABLE_DATE_IN_PAST_MSG) 
		    	return response
			end
		   
		    response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        response.Response_Msg=("") 
      	return response  
    end

    def self.deductLeasingFee
    	begin
			
			Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and deductLeasingFee method is called")

			leasingFeeAmount = getLeasingFeeAmount

			Log.logger.info("At line #{__LINE__} and deduct leasingFee amount is = #{leasingFeeAmount}")

			applyLeasingFee(leasingFeeAmount)
		
		rescue => exception
	        Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and deductLeasingFee method Exception is = \n #{exception.message}")
	        Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and deductLeasingFee method Full Exception is = \n #{exception.backtrace.join("\n")}")
	    end
	end


	def self.getLeasingFeeAmount
		leasingFee = LeasingFee.where('applicable_from <= ?', Time.now).order('applicable_from DESC').first

		Log.logger.info("At line #{__LINE__} and check the leasing fee is present or not = #{leasingFee.present?}")
		
		if leasingFee.present?
			Log.logger.info("At line #{__LINE__} and applicable leasing fee id is = #{leasingFee.id}")
			return leasingFee.fee_amount
		else
			return 0
		end	
	end	


	def self.applyLeasingFee(leasingFeeAmount)

		activeMFs = Utility.getAllActiveMF
		
		ActiveRecord::Base.transaction do

			if activeMFs.present?

				activeMFs.each do |agent|
					
					Log.logger.info("At line #{__LINE__} and active agent id is = #{agent.id}")

					deductAbleLeasingFee = getDeductAbleLeasingFee(agent, leasingFeeAmount)

					agentAccountBalance = Utility.getAgentAccountBalance(agent)

					Log.logger.info("At line #{__LINE__} and agent account balance is = #{agentAccountBalance} and applicable leasing fee amount is = #{deductAbleLeasingFee}")

					if agentAccountBalance.to_f >= deductAbleLeasingFee
						deductLeasingFeeFromAccount(agent, deductAbleLeasingFee, PendingLeasingFee.getEndPeriod)
					else
						savePendingLeasingFee(agent, deductAbleLeasingFee)
					end	
				end	

			end	

		end	

	end

	def self.getDeductAbleLeasingFee(agent, leasingFeeAmount)
			noOfRegDays = PendingLeasingFee.getAgentRegDaysDiff(agent)

			lastMonthDays = PendingLeasingFee.getLastMonthDays

			if noOfRegDays.to_i < lastMonthDays.to_f
				deductAbleLeasingFee = TxnHeader.my_money((leasingFeeAmount.to_f * noOfRegDays.to_f)/lastMonthDays.to_f).to_f
			else
				deductAbleLeasingFee = leasingFeeAmount
			end
		return deductAbleLeasingFee
	end	


	def self.deductLeasingFeeFromAccount(currentAgent, leasingFeeAmount, endPeriod)

		Log.logger.info("At line #{__LINE__} and inside the deductLeasingFeeFromAccount method")

		txnHeader = Utility.getHeader(leasingFeeAmount, Constants::LEASING_FEE_DB_LABEL, Constants::LEASING_FEE_DB_LABEL, currentAgent.id, Constants::SUCCESS_RESPONSE_CODE, (I18n.t :LEASING_FEE_DEDUCT_DES, :endPeriod => endPeriod), nil)
		txnHeader.save
		Log.logger.info("At line #{__LINE__} and LeasingFee Deduct entry is saved in the TxnHeader table successfully")
		
		# And two entries of the credit and debit into the txn_item table
    	agentTxnItem  = Utility.getTxnItemNew(currentAgent, TxnHeader.my_money(leasingFeeAmount), true, Utility.getCurrentAgentAccount(currentAgent))
    	agentTxnItem.header_id = txnHeader.id
      	agentTxnItem.save
	    Log.logger.info("At line #{__LINE__} and Agent transaction item is saved in the TxnItem table successfully")


    	optFeeTxnItem  = Utility.getTxnItemNew(Utility.getFeeAdmin(), TxnHeader.my_money(leasingFeeAmount), false, Utility.getFeeAdminAccount(Utility.getFeeAdmin()))
	    optFeeTxnItem.header_id = txnHeader.id
	    optFeeTxnItem.save
        Log.logger.info("At line #{__LINE__} and operator(fee) transaction item is saved in the TxnItem table successfully")
          
        # After saving into the txn_item now update the account balances of the operator and the agent
    	TxnManager.update_account(Utility.getFeeAdminAccount(Utility.getFeeAdmin()), TxnHeader.my_money(leasingFeeAmount), false)
		TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), TxnHeader.my_money(leasingFeeAmount), true)  
	    Log.logger.info("At line #{__LINE__} and balances of Agent and Operator Fee Account are updated successfully")

	end


	def self.savePendingLeasingFee(currentAgent, leasingFeeAmount)
		Log.logger.info("At line #{__LINE__} and inside the savePendingLeasingFee method and agent id is = #{currentAgent.id}")
		pendingLeasingFee = PendingLeasingFee.new
			pendingLeasingFee.agent_id = currentAgent.id
			pendingLeasingFee.end_period = PendingLeasingFee.getEndPeriod
			pendingLeasingFee.payment_amount = TxnHeader.my_money(leasingFeeAmount).to_f
			pendingLeasingFee.status = Constants::LEASING_FEE_PENDING_STATUS
		pendingLeasingFee.save
	end	


	def self.deductPendingLeasingFee
		
		begin
			
			todayDaynumber = Utility.getCurrentDayNumber

			Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and deductPendingLeasingFee method is called and today day number is = #{todayDaynumber}")

			if todayDaynumber.to_i != 1

				ActiveRecord::Base.transaction do
					
					pendingFeeAgentList =  PendingLeasingFee.getPendingFeeAgentsList

				    if pendingFeeAgentList.present?

				        pendingFeeAgentList.each do |agent|

				        	agentID = agent.agent_id
				        	pendingLeasingFee = agent.payment_amount
				        	endPeriod = agent.end_period

				        	Log.logger.info("At line #{__LINE__} and pending Leasing Fee is = #{pendingLeasingFee} of agent whose id is = #{agentID}")
				    		
				    		agentAccountBalance = Utility.getUserAccountBalance(agentID)

				    		if agentAccountBalance.to_f >= pendingLeasingFee.to_f
								deductLeasingFeeFromAccount(User.find(agentID), pendingLeasingFee, endPeriod)
								agent.update_attribute(:status, Constants::LEASING_FEE_RECEIVE_STATUS)
							end
									
				        end

				    end  
				end

			end 	   
		rescue => exception
	        Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and deductPendingLeasingFee method Exception is = \n #{exception.message}")
	        Log.logger.info("At line #{__LINE__} and inside the LeasingFee Model class and deductPendingLeasingFee method Full Exception is = \n #{exception.backtrace.join("\n")}")
	    end

	end	

end
