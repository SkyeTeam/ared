# admin
class Upload < ApplicationRecord
  ATTRIBUTES_FOR_STATUS_CHECKSUM = ["id", "title", "description", "language", "category_id", "area_id"]

  mount_uploader :file, FileUploader
  validates_presence_of :file

  after_commit :attributes_add_file_size, on: :create
  after_commit :attributes_update_file_size, on: :update
  after_commit :attributes_remove_file_size, on: :destroy

  belongs_to :area
  belongs_to :category
  validates :area_id, :category_id, :title, presence: true, allow_blank: false

  def file_as_url
    Settings.host_uri + self.file.url
  end

  def attributes_for_status
    h = attributes
    h.delete_if{|k,v| !ATTRIBUTES_FOR_STATUS_CHECKSUM.include?(k)}
    h["file"] = self.file.url
    h
  end

  def attributes_for_synchronize
    h = attributes
    h.delete_if{|k,v| !ATTRIBUTES_FOR_STATUS_CHECKSUM.include?(k)}
    h["remote_file_url"] = file_as_url
    h
  end

  def attributes_add_file_size
    area = Area.where(id: area_id).first
    size = area.storage_size + file.file.size

    Area.where(id: area_id).update(storage_size: size)
  end

  def attributes_update_file_size
    area = Area.where(id: area_id).first
    size = area.storage_size - file.size
    size += file.file.size

    Area.where(id: area_id).update(storage_size: size)
  end

  def attributes_remove_file_size
    area = Area.where(id: area_id).first
    size = area.storage_size - file.file.size

    if size < 0
      size = 0
    end

    Area.where(id: area_id).update(storage_size: size)
  end

end
