class Issue < ApplicationRecord
  enum status: {
    open:     1,
    pending:  2,
    closed:   3
  }

  enum category: {
    technical_problem: 1,
    billing_problem:   2
  }

  enum priority: {
    info: 1,
    minor: 2,
    major: 3,
    blocker: 4,
  }

  belongs_to :kiosk

  validates :kiosk, :category, :kiosk, :description, :priority, presence: true, allow_blank: false

end
