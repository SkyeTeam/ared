class ServiceFee < ApplicationRecord

	def self.isUpdateDeleteEnable(effectiveDate)
	    currentTime = Time.new
        todayDate = currentTime.strftime("%Y-%m-%d")
        if effectiveDate >= todayDate
        	return Constants::BOOLEAN_TRUE_LABEL
        else
        	return Constants::BOOLEAN_FALSE_LABEL
        end	
	end	

	def self.getServiceFee(serviceType,subType)
		serviceFee = ServiceFee.where('service = ? and company = ? and effective_date <= ?',serviceType,subType,Date.today).order('effective_date ASC').last
		if serviceFee != nil
			return serviceFee
		else
			return nil
		end			
	end	


	def self.getAgentTotalCommOnService(service,rechargeAmount)
		Log.logger.info("Service Fee class is called at line = #{__LINE__} and inside the function getAgentTotalCommOnService ")
		
		agentCommPct = service.agent_comm_per.to_f
      	agentCommFixed = service.agent_comm_fix.to_f
      	Log.logger.info("At line #{__LINE__} and agent commission(%) value = #{agentCommPct} and agent commission fixed value= #{agentCommFixed}")
		
		agentCalcualtedPct = (rechargeAmount.to_f  *  agentCommPct.to_f) / 100
      	agentCommAmount = agentCalcualtedPct + agentCommFixed

      	Log.logger.info("At line #{__LINE__} and total agent commission value on amount = #{rechargeAmount} is = #{agentCommAmount}")
      return agentCommAmount 	
	end	


	def self.getOptTotalCommOnService(service,rechargeAmount)
		Log.logger.info("Service Fee class is called at line = #{__LINE__} and inside the function getOptTotalCommOnService ")
		
		optCommPct = service.our_comm_per.to_f
      	optCommFixed = service.our_comm_fix.to_f
      	Log.logger.info("At line #{__LINE__} and operator commission(%) value  = #{optCommPct} and operator commission fixed = #{optCommFixed}")

      	optCalculatedPct = (rechargeAmount.to_f  *  optCommPct.to_f) / 100
      	optTotalAmount = optCalculatedPct + optCommFixed #opt total_comsn
      	
      	Log.logger.info("At line #{__LINE__} and total operator(Ared) commission value on amount = #{rechargeAmount} is = #{optTotalAmount}")
      return optTotalAmount 	
	end	

	def self.getTigoRequestExtRef()
		expersion = [('0'..'9')].map { |i| i.to_a }.flatten
        transactions_digit = (0...5).map { expersion[rand(expersion.length)] }.join
        expersion1 = [('A'..'Z')].map { |i| i.to_a }.flatten
        transaction_alpha = (0...3).map { expersion1[rand(expersion1.length)] }.join
        external_reference =transaction_alpha + "-" +  transactions_digit
      return external_reference
	end	

	def self.getMTNRequestExtRef()
		expersion = [('0'..'9')].map { |i| i.to_a }.flatten
        external_reference = (0...8).map { expersion[rand(expersion.length)] }.join
      return external_reference
	end
	
	def self.getAirtelRequestExtRef()
	    expersion = [('a'..'z'), ('A'..'Z'), ('0'..'9')].map { |i| i.to_a }.flatten
        external_reference = (0...15).map { expersion[rand(expersion.length)] }.join
      return external_reference  
	end	


end
