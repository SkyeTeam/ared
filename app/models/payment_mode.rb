class PaymentMode < ApplicationRecord

	def self.saveNewPaymentMode(params, loggedInAdmin)

		response = Response.new
			paymentModeType = params[:paymentModeType]
			Log.logger.info("At line #{__LINE__} inside the PaymentMode class and saveNewPaymentMode method is called with paymentModeType = #{paymentModeType}")

			unless paymentModeType.present?
					Log.logger.info("At line #{__LINE__} the paymentModeType is come empty from the software")
					response.Response_Code=(Error::INVALID_DATA)
		            response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
		        return response    
			end

			ActiveRecord::Base.transaction do
				paymentMode = PaymentMode.new
					paymentMode.payment_mode_type = paymentModeType
				paymentMode.save	

				ActivityLog.saveActivity((I18n.t :PAYMENT_MODE_SUCCESS_CREATE_ACTIVITY_MSG, :paymentModeType => paymentModeType), loggedInAdmin.id, nil, params)
			end	

			Log.logger.info("At line #{__LINE__} the payment Mode is successfully added")

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.Response_Msg=(I18n.t :PAYMENT_MODE_SUCCESS_CREATE_MSG) 
		return response	
	
	end


	def self.deletePaymentMode(params, loggedInAdmin)
		
		response = Response.new
			paymentModeID = params[:paymentModeID]
			Log.logger.info("At line #{__LINE__} inside the PaymentMode class and deletePaymentMode method is called with paymentModeID = #{paymentModeID}")

			unless paymentModeID.present?
					Log.logger.info("At line #{__LINE__} the paymentModeID is come empty from the software")
					response.Response_Code=(Error::INVALID_DATA)
		            response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
		        return response    
			end

			ActiveRecord::Base.transaction do
			
				paymentMode = PaymentMode.find_by_id(paymentModeID)
				unless paymentMode.present?
						Log.logger.info("At line #{__LINE__} the paymentMode does not exist in the database with this id = #{paymentModeID}")
						response.Response_Code=(Error::NO_RECORD_FOUND)
		            	response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
		        	return response    
				end	

				paymentMode.update_attribute(:status, Constants::PAYMENT_MODE_DELETE_STATUS)

				ActivityLog.saveActivity((I18n.t :PAYMENT_MODE_SUCCESS_DELETE_ACTIVITY_MSG, :paymentModeType => paymentMode.payment_mode_type), loggedInAdmin.id, nil, params)

			end	

			Log.logger.info("At line #{__LINE__} the payment Mode is successfully deleted")

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.Response_Msg=(I18n.t :PAYMENT_MODE_SUCCESS_DELETED_MSG) 
		return response	

	end	

	def self.getPaymentModeStatus(intStatus)

		if intStatus == Constants::PAYMENT_MODE_ACTIVE_STATUS

			return Constants::PAYMENT_MODE_ACTIVE_STATUS_VALUE

		else

			return Constants::PAYMENT_MODE_DELETE_STATUS_VALUE

		end		

	end	

	def self.isDeleteBtnEnable(intStatus)

		if intStatus == Constants::PAYMENT_MODE_ACTIVE_STATUS

			return Constants::BOOLEAN_TRUE_LABEL

		else

			return Constants::BOOLEAN_FALSE_LABEL

		end		
	
	end	

	def self.getPaymentModes
		paymentModesList = Array.new
		
			paymentModeObj = PaymentMode.where("status = #{Constants::PAYMENT_MODE_ACTIVE_STATUS}")

			if paymentModeObj.present?

				paymentModeObj.each do |paymentMode|

					paymentModesList.push(paymentMode.payment_mode_type)
					
				end	

			end

		return paymentModesList	
	end	

end