class MfPenaltyFee < ApplicationRecord

	def self.createPenalty(params, loggedInAdmin, request)
		Log.logger.info("At line = #{__LINE__} and inside the MfPenaltyFee manager class and createPenalty method is called")

		penaltyID = params[:penaltyID]
		penaltyType = params[:penaltyType]
		penaltyCharges = params[:penaltyCharges]
		isPenaltyActive = params[:isPenaltyActive]

		Log.logger.info("At line = #{__LINE__} and parameters are penaltyType = #{penaltyType}, penaltyCharges = #{penaltyCharges} and isPenaltyActive = #{isPenaltyActive}")

		response = validate(penaltyType, penaltyCharges, isPenaltyActive)

        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
        	isNew = isNewPenaltyFeeSetup(penaltyID)
        	Log.logger.info("At line = #{__LINE__} and check is that the new penalty or not = #{isNew}")
        	
        	if isNew
        		return savePenaltyFeeSetup(penaltyType, penaltyCharges, isPenaltyActive, params, loggedInAdmin)
        	else
        		return updatePenaltyFeeSetup(penaltyID, penaltyType, penaltyCharges, isPenaltyActive, params, loggedInAdmin)
        	end	
        end
	end	

	def self.isNewPenaltyFeeSetup(penaltyID)
		if penaltyID.present?
			return Constants::BOOLEAN_FALSE_LABEL
		else
			return Constants::BOOLEAN_TRUE_LABEL
		end	
	end	

	def self.savePenaltyFeeSetup(penaltyType, penaltyCharges, isPenaltyActive, params, loggedInAdmin)
		Log.logger.info("At line = #{__LINE__} and inside the MfPenaltyFee manager class and savePenaltyFeeSetup method is called")
		response = Response.new
			ActiveRecord::Base.transaction do
				mfPenaltyFee = MfPenaltyFee.new
					mfPenaltyFee.penalty_type = penaltyType
					mfPenaltyFee.charges = TxnHeader.my_money(penaltyCharges).to_f
					mfPenaltyFee.is_active = isPenaltyActive
					mfPenaltyFee.status = Constants::MF_PENALTY_ACTIVE_STATUS
				mfPenaltyFee.save	
				ActivityLog.saveActivity((I18n.t :MF_NEW_PENALTY_FEE_SETUP_MSG), loggedInAdmin.id, nil, params)
			end
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        response.Response_Msg=(I18n.t :PENALTY_FEE_CREATE_SUCSESS_MSG) 
      	return response  
	end	

	def self.updatePenaltyFeeSetup(penaltyID, penaltyType, penaltyCharges, isPenaltyActive, params, loggedInAdmin)
		Log.logger.info("At line = #{__LINE__} and inside the MfPenaltyFee manager class and updatePenaltyFeeSetup method is called")
		response = Response.new
			ActiveRecord::Base.transaction do
				mfPenaltyFee = MfPenaltyFee.find(penaltyID)	
				mfPenaltyFee.update_attributes(:penalty_type => penaltyType, :charges => penaltyCharges, :is_active => isPenaltyActive)
				ActivityLog.saveActivity((I18n.t :MF_UDATE_PENALTY_FEE_SETUP_MSG, :penaltyID => penaltyID), loggedInAdmin.id, nil, params)
			end
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        response.Response_Msg=(I18n.t :PENALTY_FEE_UPDATE_SUCSESS_MSG) 
      	return response  
	end	

	def self.deletePenalty(params, loggedInAdmin, request)
			Log.logger.info("At line = #{__LINE__} and inside the MfPenaltyFee manager class and deletePenalty method is called")

			penaltyID = params[:penaltyID]

			Log.logger.info("At line = #{__LINE__} and penaltyID to delete is = #{penaltyID}")

			response = Response.new
			ActiveRecord::Base.transaction do
				mfPenaltyFee = MfPenaltyFee.find(penaltyID)	
				mfPenaltyFee.update_attribute(:status, Constants::MF_PENALTY_DISABLE_STATUS)
				ActivityLog.saveActivity((I18n.t :MF_DELETE_PENALTY_FEE_SETUP_MSG, :penaltyID => penaltyID), loggedInAdmin.id, nil, params)
			end
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        response.Response_Msg=(I18n.t :PENALTY_FEE_DELETE_SUCSESS_MSG) 
      	return response  
	end	

	def self.validate(penaltyType, penaltyCharges, isPenaltyActive)
        response = Response.new
            if !penaltyType.present? || !penaltyCharges.present? || !isPenaltyActive.present?
                    Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
        	end
        	
            if penaltyCharges.to_f <= 0
                    Log.logger.info("At line = #{__LINE__} and recharge amount is may be zero or less then zero")
                    response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
                    response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
                return response
            end

	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        response.Response_Msg=("") 
      	return response  
    end

    def self.validateApplyPenalty(agentID, applyPenalties)
        response = Response.new
            if !agentID.present? || !applyPenalties.present?
                    Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
                    response.Response_Code=(Error::INVALID_DATA)
                    response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                return response
        	end
        	
            if applyPenalties.size.to_i <= 0
	    			Log.logger.info("At line = #{__LINE__} and no penalty is selected")
	    			response.Response_Code=(Error::PENALTY_NOT_SELECT)
		    		response.Response_Msg=(I18n.t :PENALTY_NOT_SELECT_MSG) 
      			return response  
	    	end	

	        response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
	        response.Response_Msg=("") 
      	return response  
    end

    def self.getAllPenaltyTypes
    	penaltyTypeList = Array.new
    	mfPenaltyFee = MfPenaltyFee.where("status = #{Constants::MF_PENALTY_ACTIVE_STATUS} AND is_active = #{Constants::MF_PENALTY_ACTIVE_STATUS}").order('penalty_type ASC')
    	if mfPenaltyFee.present?
    		mfPenaltyFee.each do |penalty|
    			penaltyTypeList.push(penalty.penalty_type)
    		end	
    	end	
    	return penaltyTypeList	
    end	

    def self.applyPenalty(params, loggedInAdmin, request)
	    response = Response.new	
	    	Log.logger.info("At line = #{__LINE__} and inside the MfPenaltyFee manager class and applyPenalty method is called")

	    	agentID = params[:agentID]
	    	applyPenalties = params[:applyPenalties].tr('[]', '').split(',').map(&:to_s) #This come in array

	    	response = validateApplyPenalty(agentID, applyPenalties)
	        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	            return response
	        end

	    	Log.logger.info("At line = #{__LINE__} and parameters are agentID is = #{agentID} and applyPenalties are = #{applyPenalties}")

	    	currentAgent = Utility.getCurrentAgent(agentID)

	    	for i in 0..applyPenalties.size-1
	    		penaltyType = applyPenalties[i]
	    		penaltyDetails = getPenaltyDetails(penaltyType)
	    		penaltyID = penaltyDetails.id
	    		penaltyCharges = penaltyDetails.charges
	    		Log.logger.info("At line = #{__LINE__} and apply Penalty charges are = #{penaltyCharges} of penalty id = #{penaltyID}")

	    		applyPenaltyToMF(currentAgent, penaltyID, penaltyType, TxnHeader.my_money(penaltyCharges).to_f, loggedInAdmin, params)
	    	end	

	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		    response.Response_Msg=(I18n.t :PENALTY_APPLY_SUCSESS_MSG) 
      	return response  
    end

    def self.applyPenaltyToMF(currentAgent, penaltyID, penaltyType, penaltyCharges, loggedInAdmin, params)
	    ActiveRecord::Base.transaction do
	    	pendingPenaltyCharges = Utility.getAgentPenaltyBalance(currentAgent)
	    	agentPenalty = AgentPenaltyDetail.new
             	agentPenalty.user_id = currentAgent.id
	            agentPenalty.loan_amount = 0
	            agentPenalty.penalty_amount = penaltyCharges
	            agentPenalty.pending_penalty_amount = TxnHeader.my_money(pendingPenaltyCharges + penaltyCharges).to_f
	            agentPenalty.penalty_type_id = penaltyID
            agentPenalty.save
            Log.logger.info("At line = #{__LINE__} and penalty details are saved into the AgentPenaltyDetail table successfully")
			
			ActivityLog.saveActivity((I18n.t :MF_APPLY_PENALTY_MSG, :penaltyCharges => Utility.getFormatedAmount(penaltyCharges), :agentName => currentAgent.name.to_s + " " +currentAgent.last_name.to_s, :penaltyType => penaltyType), loggedInAdmin.id, currentAgent.id, params)
	    	Log.logger.info("At line = #{__LINE__} and activity of the admin is saved into the ActivityLog table successfully")
	    end	
    end	

    def self.getPenaltyDetails(penaltyType)
	    	penaltyDetails = MfPenaltyFee.where("penalty_type = '#{penaltyType}' AND status = #{Constants::MF_PENALTY_ACTIVE_STATUS} AND is_active = #{Constants::MF_PENALTY_ACTIVE_STATUS}").last
	    return penaltyDetails	
    end	

end