class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    elsif user.agent?
      can :manage, Kiosk, id: user.kiosks.pluck(:id)
      can [:read, :destroy, :edit], Issue, kiosk_id: user.kiosks.pluck(:id)
      can :create, Issue
      can :read, User, id: user.id
      can :manage, WifiBooking, kiosk_id: user.kiosks.pluck(:id)
      can :create, WifiBooking
    elsif user.sales?
      can :manage, Ad
    elsif user.maintainer?
      can :manage, Issue, id: user.kiosks.pluck(:id)
      can :read, Kiosk
      can :read, User
      can :read, Area
      can :manage, Firmware
    elsif user.content_manager?
      can :manage, Article
      can :manage, Area
      can :manage, Survey
      can :manage, Question
      can :manage, Answer
      can :manage, Category
      can :manage, Upload
      can :read, SurveyResult
      can :read, SurveyResultAnswer
    end

  end
end
