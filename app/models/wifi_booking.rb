class WifiBooking < ApplicationRecord
  before_create :create_code

  enum kind: {
    agent: 1,
    consumer: 2
  }

  belongs_to :user
  belongs_to :kiosk

  def create_code
    all_codes = WifiBooking.pluck(:code).compact # get all nonnull-codes in Array

    loop do
      # need a number with 10 digits, but not more than 0xFFFFFFFF = 4_294_967_295
      number_w_10digits = 1_000_000_000 + rand(3_294_967_296)
      self.code = number_w_10digits.to_s(16).upcase
      break if !all_codes.include?(self.code)
    end
  end

  # available_duration is calculatzed as follows:
  # lastPing - start
  def available_duration_in_seconds
    if agent?
      WifiBooking.consumer.where(kiosk_id: kiosk_id, user_id: user_id).pluck(:duration).sum
    elsif consumer?
      if start
        if last_internet_ping
          if last_internet_ping > (start + duration.minutes)
            0
          else
            (start + duration.minutes) - last_internet_ping
          end
        else
          0
        end
      else
        0
      end
    end
  end

  # @param amount_of_time e.g.: 6000.seconds
  def self.to_time(amount_of_time_in_seconds)
    if amount_of_time_in_seconds
      if amount_of_time_in_seconds > 86400
        days, amount_of_time_in_seconds = amount_of_time_in_seconds.divmod(86400)
        timestamp   = Time.at(amount_of_time_in_seconds).utc.strftime("%H:%M:%S")
        hours, minutes, seconds = timestamp.split(":").map{|i| i.to_i}
        hours   = hours + days * 24
        hours   = (  hours < 10) ?   "0#{hours}" : hours
        minutes = (minutes < 10) ? "0#{minutes}" : minutes
        seconds = (seconds < 10) ? "0#{seconds}" : seconds
        return "#{hours}:#{minutes}:#{seconds}"
      else
        Time.at(amount_of_time_in_seconds).utc.strftime("%H:%M:%S")
      end
    else
      "00:00:00"
    end
  end

  def self.search(search)
    where("code LIKE ?", "%#{search}%")
  end

end
