class Review < ApplicationRecord
  belongs_to :kiosk_master 
  belongs_to :agent
  belongs_to :officer

 

  def associated_agents
	koiskAssignedResponse = KioskAgent.getAssignedAgent(self.kiosk_master_id)
	agent = koiskAssignedResponse.user
	return agent
  end


  def date_string
  	created_at.strftime('%a,%e,%b,%Y,%H:%M')
  end

end
