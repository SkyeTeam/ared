class District < ApplicationRecord
  belongs_to :country

  has_many :towns, :dependent => :destroy
  accepts_nested_attributes_for :towns, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
  has_many :officers 
end
