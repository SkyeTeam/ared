class TxnHeader < ApplicationRecord

 	belongs_to :user

	def self.getTxnDetails(txnID)
    response = Response.new
  		Log.logger.info("At line #{__LINE__} inside the TxnHeader class getTxnDetails method is called")

  		unless txnID.present?
  				response.Response_Code=(Error::INVALID_DATA)
				  response.Response_Msg=(I18n.t :INVALID_DATA_MSG)
			 return response
  		end	
  		
      query = "SELECT u.name, u.last_name, u.phone, u.tin_no, t.id, t.service_type, t.sub_type, t.txn_date, t.amount, t.agent_id, t.status, t.is_rollback "
      query << "FROM txn_headers t, users u  WHERE u.id = t.agent_id::INT AND t.id = #{txnID}"
      txnHeader = Utility.executeSQLQuery(query)
  		
      Log.logger.info("At line #{__LINE__} and check that the details of the this ID is present or not = #{txnHeader.present?}")
  		unless txnHeader.present?
  				response.Response_Code=(Error::INVALID_TXN_ID)
				  response.Response_Msg=(I18n.t :INVALID_TXN_ID_MSG)
			  return response
  		end	

      txnType = txnHeader[0]['service_type']

      if txnType.eql? Constants::TXN_ROLLBACK_DB_LABEL
          response.Response_Code=(Error::TXN_NOT_ABLE_TO_ROLLBACK)
          response.Response_Msg=(I18n.t :TXN_NOT_ABLE_TO_ROLLBACK_MSG)
        return response
      end  

      if isSerivceInDisableList(txnType)
          response.Response_Code=(Error::TXN_NOT_ABLE_TO_ROLLBACK)
          response.Response_Msg=(I18n.t :TXN_NOT_ABLE_TO_ROLLBACK_MSG)
        return response
      end  

  		isTxnRollback = txnHeader[0]['is_rollback']
  		Log.logger.info("At line #{__LINE__} and rollback status of this transaction = #{isTxnRollback}")

  		if isTxnRollback == Constants::TXN_INITIATE_ROLLBACK_STATUS
				  response.Response_Code=(Error::TXN_ALREADY_INITIATE)
				  response.Response_Msg=(I18n.t :TXN_ALREADY_INITIATE_MSG)
			  return response
  		end	

      if isTxnRollback == Constants::TXN_APPROVED_ROLLBACK_STATUS
          response.Response_Code=(Error::TXN_ALREADY_ROLLBACK)
          response.Response_Msg=(I18n.t :TXN_ALREADY_ROLLBACK_MSG)
        return response
      end 

   		if isTxnRollback == Constants::TXN_REJECTED_ROLLBACK_STATUS
  				response.Response_Code=(Error::TXN_ALREADY_REJECT)
  				response.Response_Msg=(I18n.t :TXN_ALREADY_REJECT_MSG)
  			return response
    	end	

  		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
  		response.txnDetails=(txnHeader[0])

  	return response	
  end


	def self.initiatedTxnRollback(txnID, txnAmount, description, agentID, serviceType, loggedInAdmin)
		response = Response.new

      Log.logger.info("At line #{__LINE__} inside the TxnHeader class and initiatedTxnRollback method is called")
  		
      ActiveRecord::Base.transaction do
      
        txnHeaderObject = TxnHeader.find(txnID)

        updateRollbackStatus(txnHeaderObject, true, false) # Here true means transaction is only Initiate and false means transaction is not rollback

  			txnHeader = Utility.getHeader(txnAmount, Constants::TXN_ROLLBACK_DB_LABEL, Constants::TXN_ROLLBACK_DB_LABEL, agentID, Constants::INITIATE_TXN_CODE, description, txnID)
  			txnHeader.initated_by = loggedInAdmin.id
  			txnHeader.save

        txnItems = TxnItem.where("header_id = #{txnID}")
	  		txnItems.each do |txnItem|

	  			if isUserDebit(txnItem.debit_amount) # if isUserDebit is true and then reverse the entry mnz now the user is credit
            AppUtility.getTxnItemForRollback(txnHeader.id, txnItem.user_id, txnItem.account_id, getUserAccountType(txnItem.account_id), txnItem.debit_amount, txnItem.fee_amt, Constants::PAYEE_LABEL, true, isGovtAccountAccess(txnItem.user_id, serviceType)) #true indicate that the txn is initiate
	  			else
            AppUtility.getTxnItemForRollback(txnHeader.id, txnItem.user_id, txnItem.account_id, getUserAccountType(txnItem.account_id), txnItem.credit_amount, txnItem.fee_amt, Constants::PAYER_LABEL, true, isGovtAccountAccess(txnItem.user_id, serviceType)) #true indicate that the txn is initiate
	  			end	

	  		end	

        ActivityLog.saveActivity((I18n.t :TXN_INITIATE_ACTIVITY_MSG, :txnID => txnID), loggedInAdmin.id, nil, nil)

	  	end
 
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.Response_Msg=(I18n.t :TXN_SUCCESS_INITIATE_MSG)
		return response	
	end	


  def self.isGovtAccountAccess(userID, serviceType)
    if Utility.isMatchWithAnyOperatorID(userID)
      return Constants::BOOLEAN_FALSE_LABEL
    else
      if serviceType.eql? Constants::USER_GOVT_CASH_DEPOSIT_LABLE or serviceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
        return Constants::BOOLEAN_TRUE_LABEL
      else
        return Constants::BOOLEAN_FALSE_LABEL  
      end
    end  
  end  


	def self.isUserDebit(debitAmount)
		if debitAmount.present?
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end		
	end


  def self.getUserAccountType(accountID)
      userAccount = UserAccount.find_by_id(accountID)
    return userAccount.acc_type  
  end  


	def self.updateRollbackStatus(txnHeader, isInitiate, isApprove)
	  if isInitiate
		  txnHeader.update_attribute(:is_rollback, Constants::TXN_INITIATE_ROLLBACK_STATUS)
    else
      if isApprove
        txnHeader.update_attribute(:is_rollback, Constants::TXN_APPROVED_ROLLBACK_STATUS)
        txnHeader.update_attribute(:vat_status, Constants::ROLLBACK_TO_AGENT_VAT_STATUS)
      else
        txnHeader.update_attribute(:is_rollback, Constants::TXN_REJECTED_ROLLBACK_STATUS)
      end  
    end  
	end	


  def self.updateCurrentTxnStatus(txnID, isApprove)
    txnHeader = TxnHeader.find(txnID)
    if isApprove
      txnHeader.update_attribute(:status, Constants::SUCCESS_RESPONSE_CODE)
    else
      txnHeader.update_attribute(:status, Constants::REJECT_TXN_CODE)
    end  
  end  


	def self.getAllInitiatedTxns
  		# Here a.id is the id of the TRANSACTION_ROLLBACK entry b is used for that txn that become the success txn and rollback now.
  		# u is for the agent who did the txn and u2 for the admin who initiated the txn
  		query = "SELECT a.id, a.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, a.external_reference, a.amount, b.service_type, b.sub_type, "
      query << "b.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS done_txn_date, b.account_id AS client_phone, u.name, u.last_name, u.phone, "
      query << "u2.name AS initatorfname, u2.last_name AS initatorlname, u2.phone AS initatorphone, a.description "
      query << "FROM txn_headers a, txn_headers b, users u, users u2 "
      query << "WHERE a.external_reference = b.id::VARCHAR AND a.agent_id::INT=u.id "
      query << "AND a.initated_by = u2.id AND a.service_type = '#{Constants::TXN_ROLLBACK_DB_LABEL}' "
      query << "AND a.status = #{Constants::INITIATE_TXN_CODE} ORDER BY a.txn_date DESC"

      allInitiatedTxns = Utility.executeSQLQuery(query)
		return allInitiatedTxns
	end	
   

  def self.confrimApproveRejectTxn(userAction, txnID, rolledBackTxnId, amount, serviceType, loggedInAdmin, params) 
    Log.logger.info("At line #{__LINE__} inside the TxnHeader class and confrimApproveRejectTxn method is called")
    response = Response.new
    
      ActiveRecord::Base.transaction do

        txnHeader = TxnHeader.find(rolledBackTxnId) #rolledBackTxnId is the transaction ID which was inititate later

        if userAction.to_i == Constants::APPROVE_TXN_USER_ACTION_VALUE.to_i

          isAble = isTxnAbleToRollback(txnHeader, amount, serviceType)
          Log.logger.info("At line #{__LINE__} and first we check that this transaction is able to rollback or not = #{isAble}")
          
          if !isAble
              response.Response_Code=(Error::INSUFFICIENT_ACC_BALANCE)
              response.Response_Msg=(I18n.t :INSUFFICIENT_ACC_BALANCE_MSG)
            return response
          end  

          updateRollbackStatus(txnHeader, false, true) # This will update the status from 1 to 2 of that transaction that was initiated later

          updateCurrentTxnStatus(txnID, true) # This will update the status of that TRANSACTION_ROLLBACK entry transaction from 100 to 200
        
          txnItems = TxnItem.where("header_id = #{txnID}") # txnID is the transaction ID of TRANSACTION_ROLLBACK entry

          txnItems.each do |txnItem|
            
            updateBalances(txnItem, txnItem.user_id, isUserDebit(txnItem.debit_amount), serviceType)

          end

          updateUserOtherAcsounts(txnID, rolledBackTxnId, txnHeader)

          rollbackRelatedTxns(rolledBackTxnId, nil)

          ActivityLog.saveActivity((I18n.t :TXN_APPROVE_ACTIVITY_MSG, :txnID => rolledBackTxnId), loggedInAdmin.id, nil, params)

          response.Response_Msg=(I18n.t :TXN_SUCCESS_ROLLBACK_MSG)

        else
          
          updateRollbackStatus(txnHeader, false, false) # This will update the status from 1 to 3 of that transaction that was initiated later

          updateCurrentTxnStatus(txnID,false) # This will update the status from 100 to 150 of that TRANSACTION_ROLLBACK entry transaction 

          ActivityLog.saveActivity((I18n.t :TXN_REJECT_ACTIVITY_MSG, :txnID => rolledBackTxnId), loggedInAdmin.id, nil, params)

          response.Response_Msg=(I18n.t :TXN_SUCCESS_REJECTED_MSG)

        end 

      end
 
      response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    return response 
  end  


  def self.isTxnAbleToRollback(txnHeader, amount, serviceType)
    
    if serviceType.eql? Constants::USER_CASH_DEPOSIT_LABLE or serviceType.eql? Constants::USER_LOAN_REQUEST_LABLE or serviceType.eql? Constants::USER_COMMISSION_PAYMENT_LABLE

      agentAccountBalance = Utility.getUserAccountBalanceWithType(txnHeader.agent_id, Constants::USER_ACCOUNT_TYPE)
      Log.logger.info("At line #{__LINE__} the agentAccountBalance is = #{agentAccountBalance} and transaction amount is = #{amount}")
      
      if (TxnHeader.my_money(amount)).to_f > (TxnHeader.my_money(agentAccountBalance)).to_f
        return Constants::BOOLEAN_FALSE_LABEL
      else
        return Constants::BOOLEAN_TRUE_LABEL
      end 

    elsif serviceType.eql? Constants::USER_GOVT_CASH_DEPOSIT_LABLE
      
      agentGovtAccountBalance = Utility.getUserGovtAccountBalanceWithID(txnHeader.agent_id)
      Log.logger.info("At line #{__LINE__} the agentGovtAccountBalance is = #{agentGovtAccountBalance} and transaction amount is = #{amount}")

      if (TxnHeader.my_money(amount)).to_f > (TxnHeader.my_money(agentGovtAccountBalance)).to_f
        return Constants::BOOLEAN_FALSE_LABEL
      else
        return Constants::BOOLEAN_TRUE_LABEL
      end 

    else

      return Constants::BOOLEAN_TRUE_LABEL

    end 

  end 


  def self.updateBalances(txnItemObj, userID, isDebit, serviceType)

    if isDebit
      txnAmount = txnItemObj.debit_amount
    else
      txnAmount = txnItemObj.credit_amount
    end 
    Log.logger.info("At line #{__LINE__} inside the TxnHeader class and updateBalances method is called and txnAmount is = #{txnAmount}") 

    if isGovtAccountAccess(userID, serviceType)
      currentAccBalance = Utility.getUserGovtAccountBalanceWithID(userID)
      Utility.updateUserGovtAccountBalance(userID, txnAmount, isDebit)
      Log.logger.info("At line #{__LINE__} and User GOVT account balance is successfully updated")
    else
      accountType = getUserAccountType(txnItemObj.account_id)
      currentAccBalance = Utility.getUserAccountBalanceWithType(userID, accountType)
      Utility.updateUserAccountBalanceNew(userID, accountType, TxnHeader.my_money(txnAmount).to_f, isDebit)
      Log.logger.info("At line #{__LINE__} and User M-SHIRIKI account balance is successfully updated")
    end  

    upadatePrePostBalances(txnItemObj, currentAccBalance, txnAmount, isDebit)
    Log.logger.info("At line #{__LINE__} and previous balance and post balance are successfully updated")
  end 


  def self.upadatePrePostBalances(txnItemObj, currentAccBalance, txnAmount, isDebit)
    if isDebit
      txnItemObj.update_attributes(:previous_balance => TxnHeader.my_money(currentAccBalance).to_f, :post_balance => TxnHeader.my_money(TxnHeader.my_money(currentAccBalance).to_f - TxnHeader.my_money(txnAmount).to_f).to_f)
    else
      txnItemObj.update_attributes(:previous_balance => TxnHeader.my_money(currentAccBalance).to_f, :post_balance => TxnHeader.my_money(TxnHeader.my_money(currentAccBalance).to_f + TxnHeader.my_money(txnAmount).to_f).to_f)
    end  
  end 


  def self.updateUserOtherAcsounts(txnID, rolledBackTxnId, txnHeader)

    response = isCommissonRollback(txnHeader)

    if response.isCommRollback
    
      updateAgentCommTxnStatus(txnID, rolledBackTxnId)

      deductedCommAmount = TxnHeader.my_money(TxnHeader.my_money(txnHeader.agent_commission).to_f - TxnHeader.my_money(response.agentComm).to_f).to_f

      getAgentCommission(txnHeader.agent_id, deductedCommAmount, txnID, true)

    elsif txnHeader.service_type.eql? Constants::USER_LOAN_REQUEST_LABLE
     
      loanAccount = Utility.getAgentLoanRecord(txnHeader.agent_id, txnHeader.amount, true)
      loanAccount.save
    
    elsif txnHeader.service_type.eql? Constants::USER_LOAN_DEPOSIT_LABLE
    
      loanAccount = Utility.getAgentLoanRecord(txnHeader.agent_id, txnHeader.amount, false)
      loanAccount.save
    
    elsif txnHeader.service_type.eql? Constants::USER_COMMISSION_PAYMENT_LABLE
      
      getAgentCommission(txnHeader.agent_id, txnHeader.amount, txnID, false)

    elsif txnHeader.service_type.eql? Constants::USER_PENALTY_CHARGE_LABLE
      
      getAgentPenalty(txnHeader.agent_id, txnHeader.amount, txnID)  

    end  

  end 


  def self.isCommissonRollback(txnHeader)
    response = Response.new
    serviceType = txnHeader.service_type
    isCommRollback = Constants::BOOLEAN_FALSE_LABEL
    endCustomerCommission = 0

      if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
        availableServices = Constants::DASHBOARD_AVAILABLE_SERVICES
      elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]
        availableServices = UgandaConstants::DASHBOARD_AVAILABLE_SERVICES
      end
    
      for i in 0..availableServices.size.to_i - 1
        if serviceType.eql? availableServices[i] and !serviceType.eql? Constants::IREMBO_TAX_PAYMENT_DB_LABEL
          isCommRollback = Constants::BOOLEAN_TRUE_LABEL
          break
        end   
      end  

      if isCommRollback

        agentCommission = AgentCommission.where("header_id = #{txnHeader.id}")
        
        if agentCommission.size.to_i == 1
          agentCommission = agentCommission.first
          if TxnHeader.my_money(agentCommission.previous_balance).to_f == TxnHeader.my_money(agentCommission.post_balance).to_f
            isCommRollback = Constants::BOOLEAN_FALSE_LABEL
          end
        else
          agentCommission.each do |commission|
            if TxnHeader.my_money(commission.previous_balance).to_f == TxnHeader.my_money(commission.post_balance).to_f
              endCustomerCommission =  TxnHeader.my_money(TxnHeader.my_money(endCustomerCommission).to_f + TxnHeader.my_money(commission.total_comm).to_f).to_f
            end
          end  
          isCommRollback =  Constants::BOOLEAN_TRUE_LABEL
        end 
          
      end  

      response.isCommRollback=(isCommRollback)
      response.agentComm=(endCustomerCommission)
      
    return response 
  end  


  def self.updateAgentCommTxnStatus(txnID,rolledBackTxnId)
    agentCommission = AgentCommission.find_by_header_id(rolledBackTxnId)
    agentCommission.update_attributes(:status => Constants::COMM_PAID_STATUS,:payment_ref => txnID)
  end  


  def self.getAgentCommission(userID, amount, headerID, isDebit)
    Log.logger.info("At line #{__LINE__ } inside the TxnHeader Class and getAgentCommission method to settle the account of user #{userID} and amount is = #{amount}")

    commBalance = Utility.getAgentCommBalanceByID(userID)
   
    agentCommission = AgentCommission.new
      agentCommission.user_id          = userID
      agentCommission.service_type     = Constants::USER_COMM_ROLLBACK_LABLE
      agentCommission.sub_type         = Constants::USER_COMM_ROLLBACK_LABLE
      agentCommission.txn_date         = Time.now
      agentCommission.previous_balance = TxnHeader.my_money(commBalance).to_f
      if isDebit
        agentCommission.post_balance = TxnHeader.my_money(TxnHeader.my_money(commBalance).to_f - TxnHeader.my_money(amount).to_f).to_f
      else
        agentCommission.post_balance = TxnHeader.my_money(TxnHeader.my_money(commBalance).to_f + TxnHeader.my_money(amount).to_f).to_f
      end  
      agentCommission.header_id        = headerID
      agentCommission.status           = Constants::COMM_PAID_STATUS
      agentCommission.payment_ref      = headerID
    agentCommission.save 
  end


  def self.getAgentPenalty(userID, amount,headerID)
    Log.logger.info("At line #{__LINE__ } inside the TxnHeader Class and getAgentPenalty method to settle the account of user #{userID} and amount is = #{amount}")
      
    lastPenaltyRecord = Utility.getCurrentAgentPenaltyAccountByID(userID)
    agentPenalty = AgentPenaltyDetail.new
      agentPenalty.user_id = userID
      agentPenalty.loan_amount = lastPenaltyRecord.loan_amount  
      agentPenalty.penalty_amount = TxnHeader.my_money(amount).to_f
      agentPenalty.pending_penalty_amount = TxnHeader.my_money(TxnHeader.my_money(lastPenaltyRecord.pending_penalty_amount).to_f + TxnHeader.my_money(amount).to_f).to_f
      agentPenalty.header_id = headerID
    agentPenalty.save
  end  


  def self.rollbackRelatedTxns(txnID, description)

    txnHeaders = TxnHeader.where("external_reference = '#{txnID}' AND service_type != '#{Constants::TXN_ROLLBACK_DB_LABEL}'")

    if txnHeaders.present?

      ActiveRecord::Base.transaction do

        txnHeaders.each do |header|
        
          txnHeaderObject = TxnHeader.find(header.id)

          updateRollbackStatus(txnHeaderObject, false, true) # Here false means transaction is not Initiate and true means transaction is rollback successfully

          txnHeader = Utility.getHeader(header.amount, Constants::TXN_ROLLBACK_DB_LABEL, Constants::TXN_ROLLBACK_DB_LABEL, header.agent_id, Constants::SUCCESS_RESPONSE_CODE, description, header.id)
          txnHeader.save

          txnItems = TxnItem.where("header_id = #{header.id}")
          txnItems.each do |txnItem|

            isDebit = isUserDebit(txnItem.debit_amount)

            txnAmount = isDebit ? txnItem.debit_amount : txnItem.credit_amount

            if isDebit # if isDebit is true and then reverse the entry mnz now the user is credit
              AppUtility.getTxnItemForRollback(txnHeader.id, txnItem.user_id, txnItem.account_id, getUserAccountType(txnItem.account_id), txnItem.debit_amount, txnItem.fee_amt, Constants::PAYEE_LABEL, false, isGovtAccountAccess(txnItem.user_id, txnHeaderObject.service_type)) #false indicate that the txn is not initiate
            else
              AppUtility.getTxnItemForRollback(txnHeader.id, txnItem.user_id, txnItem.account_id, getUserAccountType(txnItem.account_id), txnItem.credit_amount, txnItem.fee_amt, Constants::PAYER_LABEL, false, isGovtAccountAccess(txnItem.user_id, txnHeaderObject.service_type)) #false indicate that the txn is not initiate
            end 

            if isGovtAccountAccess(txnItem.user_id, txnHeaderObject.service_type)
              Utility.updateUserGovtAccountBalance(txnItem.user_id, TxnHeader.my_money(txnAmount).to_f, !isDebit)
              Log.logger.info("At line #{__LINE__} and User GOVT account balance is successfully updated")
            else
              Utility.updateUserAccountBalanceNew(txnItem.user_id, getUserAccountType(txnItem.account_id), TxnHeader.my_money(txnAmount).to_f, !isDebit)
              Log.logger.info("At line #{__LINE__} and User M-SHIRIKI account balance is successfully updated")
            end  

          end 

        end

      end  

    end  

  end


  def self.isTxnRollbackEnable(txnHisObj)
    if isSerivceInDisableList(txnHisObj["service_type"])

      return Constants::BOOLEAN_FALSE_LABEL

    elsif txnHisObj['is_rollback'].to_i == Constants::TXN_INITIATE_ROLLBACK_STATUS

      return Constants::BOOLEAN_FALSE_LABEL

    elsif txnHisObj['is_rollback'].to_i == Constants::TXN_APPROVED_ROLLBACK_STATUS

      return Constants::BOOLEAN_FALSE_LABEL

    elsif txnHisObj['is_rollback'].to_i == Constants::TXN_REJECTED_ROLLBACK_STATUS
  
      return Constants::BOOLEAN_FALSE_LABEL
  
    else

      return Constants::BOOLEAN_TRUE_LABEL

    end    
  end  

  def self.isSerivceInDisableList(serviceType)

    disableDirectRollbackServices = Array.new
    disableDirectRollbackServices.push(Constants::COMMISSION_PAID_SERVICE_TYPE_DB_LABEL)
    disableDirectRollbackServices.push(Constants::COMMISSION_RECEIVABLE_SERVICE_TYPE_DB_LABEL)
    disableDirectRollbackServices.push(Constants::AGENT_VAT_DB_LABEL)
    disableDirectRollbackServices.push(Constants::OPT_VAT_DB_LABEL)
    disableDirectRollbackServices.push(Constants::TXN_ROLLBACK_DB_LABEL)
    disableDirectRollbackServices.push(Constants::AGGREGATOR_DEPOSIT_DB_LABEL)
    disableDirectRollbackServices.push(Constants::MTOP_UP_SALES_LABEL)
    disableDirectRollbackServices.push(Constants::ELECTRICITY_SALE_LABEL)
    disableDirectRollbackServices.push(Constants::DTH_SALE_LABEL)
    disableDirectRollbackServices.push(Constants::INTERNET_SALE_LABEL)
    disableDirectRollbackServices.push(Constants::IREMBO_TAX_PAYMENT_SALE_LABEL)
    disableDirectRollbackServices.push(UgandaConstants::NATIONAL_WATER_BILL_SALES_LABEL)

    if disableDirectRollbackServices.include? serviceType
      return Constants::BOOLEAN_TRUE_LABEL
    else
      return Constants::BOOLEAN_FALSE_LABEL  
    end  

  end  


  def self.my_money(money)
      if money != nil 
        formatedMoney = money.to_f.round(2)
        formatedMoney = '%.2f' % formatedMoney
      else
        formatedMoney = '%.2f' % 0
      end   
    return formatedMoney.to_s
  end

  def self.getYearsList()
      year = []
      lastYear = 2016
      for i in 0..100 
        year[i]=lastYear
        lastYear = lastYear.to_i + 1
      end   
    return year   
  end 

end