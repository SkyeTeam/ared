class UserAccount < ApplicationRecord
  	belongs_to :user

  	def self.createAggregatorsNewAccounts
  		allAggregators = User.where("role = #{Constants::AGGREGATOR_USER_ROLE}")
  		if allAggregators.present?
  			allAggregators.each do|aggregator|
  				UserAccount.create(user_id: aggregator.id, currency: Utility.getCurrencyLabel, balance: 0, acc_type: Constants::AGGREGATOR_COMM_PAID_ACCOUNT_TYPE)
  				UserAccount.create(user_id: aggregator.id, currency: Utility.getCurrencyLabel, balance: 0, acc_type: Constants::AGGREGATOR_COMM_RECV_ACCOUNT_TYPE)	
  			end	
  		end	
  	end	

end