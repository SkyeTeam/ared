class Visit < ApplicationRecord
  belongs_to :agent
  belongs_to :maintenance_officer
end
