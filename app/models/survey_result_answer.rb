class SurveyResultAnswer < ApplicationRecord
  belongs_to :survey_result
  belongs_to :answer
end
