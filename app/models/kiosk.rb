class Kiosk < ApplicationRecord
  has_paper_trail

  # Kiosk sends geocoordinates in one Field: lonlat
  # hence we need to convert this before we save
  attr_accessor :lonlat

  #has_many :wifi_bookings
  has_many :servery_results
  has_many :issues, dependent: :destroy
  has_many :ad_statistics, class_name: "Ad::Statistic", dependent: :nullify
  belongs_to :area

  has_and_belongs_to_many :users

  before_save :convert_lonlat_param
  #scope :for_category, ->(category = nil) { category ? where(category: category) : all }
  #scope :unowned, includes(:users).where(:users => {:id => nil})

  def convert_lonlat_param
    if lonlat.present?
      lon_, lat_ = lonlat.split(/[\s,']/)
      self.lon = lon_
      self.lat = lat_
    else
      self.lon = 30.082111
      self.lat = -1.935114
    end
  end

  # @param month 1..12
  # @param year e.g. 2016
  def get_month_data(ad_or_ad_id, month, year)
    ad_id     = (ad_or_ad_id.class == Ad) ? ad_or_ad_id.id : ad_or_ad_id
    start     = Time.mktime(year, month, 1)
    last_day  = Date.civil(year, month, -1).day
    finish    = Time.mktime(year, month, last_day, 23, 59, 59)
    range     = start..finish

    month_data = OpenStruct.new(ad_id: ad_id, kiosk_id: id, impressions: 0, clicks: 0)
    Ad::Statistic.where(ad_id: ad_id, action_at: range, kiosk_id: id).all.each do |s|
      if s.action == "impression"
        month_data.impressions = month_data.impressions + 1
      else s.action == "click"
        month_data.clicks = month_data.clicks + 1
      end
    end

    return month_data
  end



  # relations as methods
  #
  def ads
    Ad.where(area_id: self.area.subtree_ids) rescue []
  end

  def articles
    Article.where(area_id: self.area.subtree_ids) rescue []
  end

  def surveys
    Survey.where(area_id: self.area.subtree_ids).includes( questions: [ :answers ]) rescue []
  end

  def uploads
    Upload.all rescue []
  end

  def wifi_bookings(user_or_user_id)
    user_id = (user_or_user_id.class == User) ? user_or_user_id.id : user_or_user_id
    WifiBooking.where(kiosk_id: id, user_id: user_id)
  end

  def info(truncate = nil)
    description = self.description || self.identifier

    if description.present?
      if truncate && description
        description.truncate(truncate)
      else
       description
      end
    else
      self.identifier
    end
  end

  # returns md5 checksum for defined select fields
  def checksum_status(model_name)
    case model_name.class.to_s # unschön, aber notwendig
    when "Ad::ActiveRecord_Relation"
      _status = ads.map{|ad| ad.attributes_for_status}

    when "Article::ActiveRecord_Relation"
      _status = articles.select("id, title, text, language, file, area_id, category_id")

    when "Category::ActiveRecord_Relation"
      _status = Category.all.select("id, title")

    when "Survey::ActiveRecord_Relation"
      _status = Survey.get_nested_attributes_hash(model_name)

    when "Upload::ActiveRecord_Relation"
      _status = uploads.map{|upload| upload.attributes_for_status}

    else
      puts "Problem with checksum: #{model_name.class}"
    end

    return Digest::MD5.hexdigest(_status.to_json)
  end

  def wifi_status
    if self.wifi_info
      JSON.parse(self.wifi_info)["radio0"]["up"]
    else
      false
    end
  end

  def self.search(search)
    where("description LIKE ?", "%#{search}%")
  end

end
