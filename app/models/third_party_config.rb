class ThirdPartyConfig < ApplicationRecord

	def self.getThirdparty(thirdPartyName)
		thirdParty = ThirdPartyConfig.where("party_name = '#{thirdPartyName}'").first
 	end	

 	def self.updateConfigs(loggedInAdmin, params)

 		Log.logger.info("At line #{__LINE__} inside the ThirdPartyConfig class and method updateConfigs is called")

 		counter = params[:count]
 		Log.logger.info("At line = #{__LINE__} and total configurations are = #{counter}")
		
		response = Response.new
 		
 		ActiveRecord::Base.transaction do
			for i in 1..counter.to_i
				response = updateConfigsSingleParty(params['partyName'+i.to_s], params['username'+i.to_s], params['password'+i.to_s], params['connTime'+i.to_s])
				if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
					break
				end	
			end	
			ActivityLog.saveActivity((I18n.t :AGGREGATOR_CONFIG_UPDATE_ACTIVITY_MSG), loggedInAdmin.id, nil, params)
		end	

		return response
 	
 	end	

 	def self.updateConfigsSingleParty(partyName, username, password, connectionTimeout)
 		
 		Log.logger.info("At line = #{__LINE__} and parameters are partyName = #{partyName}, username = #{username}, password = #{password} and connectionTimeout = #{connectionTimeout}")
		
		response = Response.new
		
			query = "update third_party_configs set username = '#{username}', password = '#{password}', connection_timeout = #{connectionTimeout}, updated_at = '#{Time.now}' where party_name = '#{partyName}'"
		
			isSuccessExecute = Utility.executeSQLQuery(query)
		
			if isSuccessExecute == Constants::BOOLEAN_FALSE_LABEL
				response.Response_Code=(Constants::INTERNAL_SERVER_ERROR)
	    		response.Response_Msg=(I18n.t :INTERNAL_SERVER_ERROR_MSG)
			else
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				response.Response_Msg=(I18n.t :THIRD_PARTY_UPDATE_MSG)
			end	
	 	
	 	return response  
	
	end

end
