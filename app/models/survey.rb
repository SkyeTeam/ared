class Survey < ApplicationRecord
  has_many :questions, dependent: :destroy

  has_many :survey_results
  has_many :results, :through => :survey_results

  belongs_to :area

  validates :title, presence: true, allow_blank: false
  validates :language, presence: true, allow_blank: false
  validates :start_date, :end_date, presence: true
  validates_associated :questions, presence: true

  def handle_questions_by_params(params)
    # questions
    # self.questions.destroy_all if self.persisted?

    params[:questions].each_with_index do |(_, question), index|
      new_question = self.questions.find_or_initialize_by(order_id: index)
      new_question.title = question
      new_question.layout = params[:layout][index.to_s]

      if new_question.save
        params[:answers][index.to_s].each_with_index do |answer, i|
          if answer.present?
            tmp_answer = new_question.answers.find_or_create_by order_id: i
            tmp_answer.answer = answer
            tmp_answer.save
          end
        end
      end
    end
  end

  def self.get_nested_attributes_hash(surveys)
    _status = surveys.as_json(:include => {:questions => {:include => :answers}})
    # need to use questions_attributes and answers_attributes instead of questions, answers for the nested creation in the kiosk
    _status.each{|hash|
      hash["questions_attributes"] = hash["questions"];
      hash["questions_attributes"].each{|hash2|
        hash2["answers_attributes"] = hash2["answers"]
        hash2.delete "answers"
      }
      hash.delete "questions"
    }
    return _status
  end

end
