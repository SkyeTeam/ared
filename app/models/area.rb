class Area < ApplicationRecord
  has_ancestry

  has_many :articles
  has_many :surveys
  has_many :ads
  has_many :kiosks
end
