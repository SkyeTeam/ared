class Ad < ApplicationRecord
  ATTRIBUTES_FOR_STATUS_CHECKSUM = ["id", "title", "start_date", "end_date", "area_id", "active", "position", "link", "language"]

  mount_uploader :image, AdUploader
  validates_presence_of :image

  after_commit :attributes_add_file_size, on: :create
  after_commit :attributes_update_file_size, on: :update
  after_commit :attributes_remove_file_size, on: :destroy

  enum position: {
    left: 1,
    right: 2,
    top: 3,
    bottom: 4,
    inline: 5,
  }

  # relations
  belongs_to :area
  has_many :ad_statistics, class_name: Ad::Statistic, dependent: :nullify

  # validations
  validates :title, :area_id, :language, presence: true
  validates :clicks, numericality: { only_integer: true }, allow_blank: true

  def image_as_url
    Settings.host_uri + self.image.url
  end

  def attributes_for_status
    h = attributes
    h.delete_if{|k,v| !ATTRIBUTES_FOR_STATUS_CHECKSUM.include?(k)}
    h["image"] = self.image.url
    h
  end

  def attributes_for_synchronize
    h = attributes
    h.delete_if{|k,v| !ATTRIBUTES_FOR_STATUS_CHECKSUM.include?(k)}
    h["remote_image_url"] = image_as_url
    h
  end

  def attributes_add_file_size
    area = Area.where(id: area_id).first
    size = area.storage_size + image.file.size

    Area.where(id: area_id).update(storage_size: size)
  end

  def attributes_update_file_size
    area = Area.where(id: area_id).first
    size = area.storage_size - image.size
    size += image.file.size

    Area.where(id: area_id).update(storage_size: size)
  end

  def attributes_remove_file_size
    area = Area.where(id: area_id).first
    size = area.storage_size - image.file.size

    if size < 0
      size = 0
    end

    Area.where(id: area_id).update(storage_size: size)
  end

end
