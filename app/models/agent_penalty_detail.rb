class AgentPenaltyDetail < ApplicationRecord
	has_one :user

	def self.fetchDetails(currentAgent)
		AppLog.logger.info("At line #{__LINE__} inside the AgentPenaltyDetail's  and fetchDetails method is called")
		response = Response.new
		
		unless currentAgent.present?
        		AppLog.logger.info("At line #{__LINE__} and agent is not present")
        		response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
        		response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
      		return response
    	end
    	AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")

    	response = getAgentPenaltyDetails(currentAgent)
    	
    	return response
	end

	def self.getAgentPenaltyDetails(currentAgent)
		response = Response.new

		query = "SELECT penalty.id, penalty.penalty_amount, penalty.pending_penalty_amount, fees.penalty_type, penalty.created_at "
		query << "FROM agent_penalty_details penalty, mf_penalty_fees fees "
		query << "WHERE penalty.user_id = #{currentAgent.id} AND penalty.penalty_type_id = fees.id AND penalty.non_payment_status = #{Constants::PAYMENT_STATUS} "
		query << "ORDER BY penalty.created_at DESC"
	
		agentPenaltyDetails = Utility.executeSQLQuery(query)	
		AppLog.logger.info("At line #{__LINE__} and check that agent has penalty history or not = #{agentPenaltyDetails.present?}")

		if agentPenaltyDetails.present?
      			penaltyArray = Array.new

      			agentPenaltyDetails.each do |penalty|
      				penaltyHash = ActiveSupport::HashWithIndifferentAccess.new

      				penaltyHash[:id] = penalty["id"]
      				if penalty["penalty_amount"].to_f < 0
      					penaltyHash[:penalty_amount] = TxnHeader.my_money(-(penalty["penalty_amount"].to_f))
      					penaltyHash[:penalty_type] = Constants::USER_PENALTY_DEPOSIT_VIEW_LABLE
      				else
      					penaltyHash[:penalty_amount] = TxnHeader.my_money(penalty["penalty_amount"])
      					penaltyHash[:penalty_type] = penalty["penalty_type"]
      				end	
      				penaltyHash[:pending_penalty_amount] = TxnHeader.my_money(penalty["pending_penalty_amount"])
      				penaltyHash[:created_at] = penalty["created_at"]

      				penaltyArray << penaltyHash
      			end		

      			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.object=(penaltyArray)
      		return response
    	else  
       			response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
    	end
	end	

end