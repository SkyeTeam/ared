class KioskMaster < ApplicationRecord
 mount_uploader :image, ImageUploader

  has_many :reviews
  has_many :officers, :through => :reviews  # Edit :needs to be plural same as the has_many relationship  

	def self.getAllKiosks
		return KioskMaster.where("functional_status != #{Constants::KIOSK_TERMINATE_STATUS}").order("id ASC")
	end	

	def self.getNewKioskModel
		return Utility.getDefaultKioskModel.to_s + "-" + ((Date.today.year) % 100).to_s
	end	

	def self.getNewKioskSerialNumber
		lastKioskMaster = KioskMaster.all.order("id DESC").first
		if lastKioskMaster.present?
			lastKioskSerialNumber =	lastKioskMaster.serial_no.to_i + 1
			kioskSerialNo = Utility.convertToTenDigit(lastKioskSerialNumber.to_s) 
		else
			kioskSerialNo = Constants::DEFAULT_KIOSK_SERIAL_NUMBER
		end	
		Log.logger.info("At line #{__LINE__} inside the KioskMaster class and getNewKioskSerialNumber is called with kioskSerialNo = #{kioskSerialNo}")
		return kioskSerialNo
	end

	def self.setPageParams(params)
		kioskSerialNumber = getNewKioskSerialNumber
  		params[:purchaseDate] = Date.today
  		params[:kioskSerialNumber] = kioskSerialNumber
  		params[:kioskModel] = getNewKioskModel
  		params[:kioskAcctualModel] = getNewKioskModel.to_s + "-" + kioskSerialNumber.to_s
	end	

	def self.createKiosk(loggedInAdmin, params)
		Log.logger.info("At line #{__LINE__} inside the KioskMaster class and createKiosk method is called")
		kioskModel = params[:kioskModel]
		kioskSerialNumber = params[:kioskSerialNumber]
		purchaseDate = params[:purchaseDate]
		kiosksQuantity = params[:kiosksQuantity]
		isPrintQRCode = params[:isPrintQRCode]
		Log.logger.info("At line #{__LINE__} parameters are kioskModel = #{kioskModel}, kioskSerialNumber = #{kioskSerialNumber}, purchaseDate = #{purchaseDate}, kiosksQuantity = #{kiosksQuantity} and isPrintQRCode = #{isPrintQRCode}")

		response = validate(kioskModel, kioskSerialNumber, purchaseDate, kiosksQuantity, isPrintQRCode)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
        	return response
        else
        	return processSaveKiosk(kioskModel, kioskSerialNumber, purchaseDate, kiosksQuantity, isPrintQRCode, loggedInAdmin, params)
        end	
	end	

	def self.processSaveKiosk(kioskModel, kioskSerialNumber, purchaseDate, kiosksQuantity, isPrintQRCode, loggedInAdmin, params)
		response = Response.new	
			for i in 0..kiosksQuantity.to_i - 1
        		if i.to_i > 0
        			kioskNextSerialNumber =	kioskNextSerialNumber.to_i + 1
        		else
        			kioskNextSerialNumber = kioskSerialNumber.to_i
        		end 
        		actualkioskNextSerialNumber = Utility.convertToTenDigit(kioskNextSerialNumber.to_s)
        		
        		saveKiosk(kioskModel, actualkioskNextSerialNumber, purchaseDate, Utility.getCapAlphaNumRandomNumber(12), Constants::KIOSK_FUNCTIONAL_STATUS)

        	end	
			
			ActivityLog.saveActivity((I18n.t :KIOSK_ADD_ACTIVITY_MSG, :quantity => kiosksQuantity), loggedInAdmin.id, nil, params)

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=(I18n.t :KIOSK_SUCCESS_ADD_MSG, :quantity => kiosksQuantity) 
      	return response  
	end	

	def self.saveKiosk(kioskModelNo, kioskSerialNo, purchaseDate, qrCodeNo, functionalStatus)
		ActiveRecord::Base.transaction do
			kioskMaster = KioskMaster.new
				kioskMaster.model_no = kioskModelNo
				kioskMaster.serial_no = kioskSerialNo
				kioskMaster.purchase_date = purchaseDate
				kioskMaster.qr_code_no = qrCodeNo
				kioskMaster.functional_status = functionalStatus
			kioskMaster.save	
		end	
	end	

	def self.validate(kioskModel, kioskSerialNumber, purchaseDate, kiosksQuantity, isPrintQRCode)
		response = Response.new
	        
	        if !kioskModel.present? || !kioskSerialNumber.present? || !purchaseDate.present? || !kiosksQuantity.present?
	        		Log.logger.info("At line #{__LINE__} some data is come nil from the software")	
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		if !Utility.isValidYear(purchaseDate.to_s)
        			Log.logger.info("At line = #{__LINE__} and purchaseDate has not a valid year ")
       				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end

	def self.searchKiosk(loggedInAdmin, params)
		response = Response.new
		kioskID = params[:kioskID]
		Log.logger.info("At line #{__LINE__} inside the KioskMaster class and searchKiosk method is called to search the kiosk with id = #{kioskID}")
		
	 	unless kioskID.present?
        		Log.logger.info("At line #{__LINE__} kioskID is come nil from the software")	
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response    
 		end
 		
 		if kioskID.include? "-"
          	endVal = kioskID.rindex("-")
          	kioskModel = kioskID[0, endVal] 
          	kioskSerialNo = kioskID[endVal.to_i + 1, kioskID.length]
        else
        	kioskModel = kioskID
        	kioskSerialNo = kioskID	
        end

 		kioskMaster = KioskMaster.where("model_no = '#{kioskModel}' AND serial_no = '#{kioskSerialNo}'")
 		if kioskMaster.present?
 				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.Response_Msg=("") 
        		response.object=(kioskMaster)
      		return response
 		else
 				response.Response_Code=(Error::KIOSK_NOT_FOUND)
    	       	response.Response_Msg=(I18n.t :KIOSK_NOT_FOUND_MSG) 
        	return response 	
        end
	end	

	def self.getKioskStatus(intKioskStatus)
		if intKioskStatus.to_i == Constants::KIOSK_FUNCTIONAL_STATUS
			return Labels::KIOSK_FUNCTIONAL_STATUS_LABEL
		elsif intKioskStatus.to_i == Constants::KIOSK_NON_FUNCTIONAL_STATUS	
			return Labels::KIOSK_NON_FUNCTIONAL_STATUS_LABEL
		elsif intKioskStatus.to_i == Constants::KIOSK_TERMINATE_STATUS	
			return Labels::KIOSK_TERMINATE_STATUS_LABEL
		end		
	end

	def self.getKioskStatusColor(intKioskStatus)
		if intKioskStatus.to_i == Constants::KIOSK_FUNCTIONAL_STATUS
			return "green"
		elsif intKioskStatus.to_i == Constants::KIOSK_NON_FUNCTIONAL_STATUS	
			return "orange"
		elsif intKioskStatus.to_i == Constants::KIOSK_TERMINATE_STATUS	
			return "red"
		end		
	end	

	def self.getKioskMaster(koiskMasterID, kioskModel, kioskSerialNumber)
		koiskMaster = KioskMaster.where("id = #{koiskMasterID} AND model_no = '#{kioskModel}' AND serial_no = '#{kioskSerialNumber}'").first
		return koiskMaster
	end	

	def self.regenerateKioskQRCode(loggedInAdmin, params)
		koiskMasterID = params[:koiskMasterID]
		kioskModel = params[:kioskModel]
		kioskSerialNumber = params[:kioskSerialNumber]
		agentID = params[:agentID]
		Log.logger.info("At line #{__LINE__} inside the KioskMaster class and regenerateKioskQRCode method is called with parameters are koiskMasterID = #{koiskMasterID}, kioskModel = #{kioskModel}, kioskSerialNumber = #{kioskSerialNumber} and agentID = #{agentID}")

		response = Response.new
		if !koiskMasterID.present? || !kioskModel.present? || !kioskSerialNumber.present?
        		Log.logger.info("At line #{__LINE__} some data is come nil from the software")	
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response    
 		else
 				newQRCode = Utility.getCapAlphaNumRandomNumber(12)
 				kioskMaster = KioskMaster.getKioskMaster(koiskMasterID, kioskModel, kioskSerialNumber)
 				kioskMaster.update_attribute(:qr_code_no, newQRCode)
 					
 				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.Response_Msg=(I18n.t :QR_CODE_SUCCESS_REGENERATE_MSG) 
        		response.object=(setParamsList(kioskModel, kioskSerialNumber, agentID, newQRCode))

      		return response	
 		end
	end	

	def self.setParamsList(kioskModel, kioskSerialNumber, agentID, newQRCode)
		paramsList = "?downloadStatus=1&"
		paramsList << "kioskModel=#{kioskModel}&"
		paramsList << "kioskSerialNumber=#{kioskSerialNumber}&"
		paramsList << "agentName=#{Utility.getAgentFullName(Utility.getCurrentAgent(agentID))}&"
		paramsList << "qrCodeText=#{newQRCode}"
	
	end	

	def self.generateQRCode(text, size)
	  	require 'barby'
	  	require 'barby/barcode'
	  	require 'barby/barcode/qr_code'
	  	require 'barby/outputter/png_outputter'

		 barcode = Barby::QrCode.new(text, level: :q, size: size)
		 base64Output = Base64.encode64(barcode.to_png({ xdim: 5 }))
	     return "data:image/png;base64,#{base64Output}"
	end	

	def self.getTotalKiosks
		return KioskMaster.all.count
	end	
	
	def self.getTotalFunctionalKiosks
		return KioskMaster.where("functional_status = #{Constants::KIOSK_FUNCTIONAL_STATUS}").count
	end	

	def self.getTotalNonFunctionalKiosks
		return KioskMaster.where("functional_status = #{Constants::KIOSK_NON_FUNCTIONAL_STATUS}").count
	end	

	def self.getTotalTerminatedKiosks
		return KioskMaster.where("functional_status = #{Constants::KIOSK_TERMINATE_STATUS}").count
	end

	def self.getTotalOnFieldKiosks
		sqlQuery = "SELECT COUNT(k.id) FROM kiosk_masters k, kiosk_agents a "
		sqlQuery << "WHERE k.id = a.kiosk_master_id AND a.assign_status = #{Constants::KIOSK_ASSIGN_STATUS}"
		
		kioskAgent = Utility.executeSQLQuery(sqlQuery)
		if kioskAgent.present?
			return kioskAgent[0]["count"]
		else
			return 0
		end	
	end	

	def self.getAgentAssignKioskModel(agentID)
		kioskAgent = KioskAgent.getAgentAssignKioskID(agentID)
		if kioskAgent.present?
			koiskMaster = KioskMaster.find_by_id(kioskAgent.kiosk_master_id)
			if koiskMaster.present?
				return koiskMaster.model_no.to_s + "-" + koiskMaster.serial_no.to_s
			else
				return Constants::BLANK_VALUE	
			end	
		else
			return Constants::BLANK_VALUE
		end	
	end	
	
end
	