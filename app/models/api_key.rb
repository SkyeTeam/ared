class ApiKey < ApplicationRecord
	
	def self.getKey(loggedUserID)
			Log.logger.info("At line #{__LINE__} inside the ApiKey class and getKeys method is called with loggedUserID = #{loggedUserID}")

			keysList = ApiKey.where("user_id = #{loggedUserID}").order('created_at ASC').last

		return keysList
	end	

	def self.createAPIKey(params, loggedInUser)
		Log.logger.info("At line #{__LINE__} inside the ApiKey class and createAPIKey method is called with loggedUserID = #{loggedInUser.id}")
    	   
        response = Response.new

            ActiveRecord::Base.transaction do
             	key = ApiKey.new
            		apiKey = Utility.getCapAlphaNumRandomNumber(20)
            		key.user_id = loggedInUser.id
            		key.api_key = Utility.encryptedData(apiKey)
            	key.save	

                Utility.sendEmail(Properties::SUPERADMIN_EMAIL, loggedInUser.email, (I18n.t :API_KEY_GENERATE_EMAIL_TITLE), (I18n.t :API_KEY_GENERATE_EMAIL_MSG, :name => Utility.getAgentFullName(loggedInUser), :apiKey => apiKey)) 

                ActivityLog.saveActivity((I18n.t :API_KEY_CREATE_ACTIVITY_MSG), loggedInUser.id, nil, params)
            end    
            
		  	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
          	response.Response_Msg=(I18n.t :API_KEY_SUCCESSFULLY_CREATED_MSG)
		return response
 	end 

end
