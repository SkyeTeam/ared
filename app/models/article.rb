class Article < ApplicationRecord
  # relations
  belongs_to :area
  belongs_to :category

  # validations
  validates :title, presence: true, allow_blank: false
  validates :language, presence: true, allow_blank: false
  validates :area_id, presence: true
  validates :category_id, presence: true

end
