class Answer < ApplicationRecord
  belongs_to :question

  has_many :survey_result_answers
  has_many :survey_results, :through => :survey_result_answers, dependent: :destroy
end
