class ServiceSetting < ApplicationRecord
	require 'constants'

	def self.isEnableButtonShow(serviceStatus)
		if serviceStatus == 1
			return 'none'
		else
			return 'block'
		end	
	end	
	
	def self.isDisableButtonShow(serviceStatus)
		if serviceStatus == 0
			return 'none'
		else
			return 'block'
		end	
	end	

	def self.getServiceSettingStatus(serviceStatus)
	    if serviceStatus == 1
	      return 'Enable'
	    else
	      return 'Disable'
	    end  
  	end 

  	def self.enableService(params, loggedInAdmin)
		id = params[:id]
		service = params[:service]

		Log.logger.info("Service Setting  class is called and inside the method  enableService at  line #{__LINE__}  id is = #{id} , service is #{service}");

		serviceSetting = ServiceSetting.where('id = ? and service = ?',id,service).first
		serviceSetting.update(:status => 1)
		ActivityLog.saveActivity((I18n.t :SERVICE_ENABLE_ACTIVITY_MSG, :serviceName => service), loggedInAdmin.id, nil, params)
	end	

	def self.disableService(params, loggedInAdmin)
		id = params[:id]
		service = params[:service]

		Log.logger.info("Service Setting  class is called and inside the method  disableService at  line #{__LINE__}  id is = #{id} , service is #{service}");

		serviceSetting = ServiceSetting.where('id = ? and service = ?',id,service).first
		serviceSetting.update(:status => 0)
		ActivityLog.saveActivity((I18n.t :SERVICE_DISABLE_ACTIVITY_MSG, :serviceName => service), loggedInAdmin.id, nil, params)
	end	

	def self.updateServiceThreshold(params, loggedInAdmin)

		Log.logger.info("Service Setting  class is called and inside the method updateServiceThreshold at line #{__LINE__}")
		
		serviceID = params[:serviceID]
		serviceName = params[:serviceName]
		serviceThreshold = params[:thresholdAmt]
		notificationsEmails = params[:email]

		Log.logger.info("At  line #{__LINE__} service id = #{serviceID}, service name is = #{serviceName}, amount = #{serviceThreshold} and Emails are = #{notificationsEmails}");

		response = validate(serviceID, serviceName, serviceThreshold, notificationsEmails)

		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
        	return response
        end

        response = updateServiceThresholdDetails(serviceID, serviceName, serviceThreshold, notificationsEmails)

        ActivityLog.saveActivity((I18n.t :SERVICE_THRESHOLD_SET_ACTIVITY_MSG, :serviceName => serviceName), loggedInAdmin.id, nil, params)

        return response	

	end

	def self.validate(serviceID, serviceName, serviceThreshold, notificationsEmails)
		response = Response.new
        
	        if !serviceID.present? || !serviceName.present? || !serviceThreshold.present? || !notificationsEmails.present?
                	Log.logger.info("At line = #{__LINE__} and some data is come nil from the web")
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		#Now check that the amount is cannot be less then zero 
            if serviceThreshold.to_f <= 0
                    Log.logger.info("At line = #{__LINE__} and amount is may be zero or less then zero")
                    response.Response_Code=(Error::AMOUNT_LESS_THEN_ZERO)
                    response.Response_Msg=(I18n.t :AMOUNT_LESS_THEN_ZERO_MSG) 
                return response
            end

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end


	def self.updateServiceThresholdDetails(serviceID, serviceName, serviceThreshold, notificationsEmails)
		response = Response.new
		
		if serviceName.eql? Constants::FDI_DTH_LABEL or  serviceName.eql? Constants::FDI_TIGO_LABEL or serviceName.eql? Constants::FDI_ELECTRICITY_LABEL
				response  = updateFDIServicesThreshold(serviceThreshold, notificationsEmails)
			return response	
		else
			serviceSetting = getServiceDetails(serviceID,serviceName)
			if serviceSetting != nil
					serviceSetting.update(:threshold_amount => serviceThreshold)
					updateEmails(serviceSetting, notificationsEmails)
					response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    				response.Response_Msg=(I18n.t :THRESHOLD_UPDATED_SUCCESSFULLY)  
  				return response  
			else
					response.Response_Code=(Error::SERVICE_NOT_DEFINED)
    				response.Response_Msg=(I18n.t :SERVICE_NOT_DEFINED_MSG) 
  				return response  
			end	
		end
	
	end	


	def self.updateEmails(service, notificationsEmails)
		service.update(:emails => notificationsEmails)
	end	

	def self.getServiceDetails(serviceID,serviceName)
		serviceSetting = ServiceSetting.where('id = ? and service = ?',serviceID,serviceName).first
		if serviceSetting.present?
			return serviceSetting
		else
			return nil
		end	
	end

	def self.getServiceDetailsWithName(serviceName)
		serviceSetting = ServiceSetting.where('service = ?',serviceName).first
		if serviceSetting.present?
			return serviceSetting
		else
			return nil
		end	
	end

	def self.updateFDIServicesThreshold(thresholdAmount, notificationsEmails)
		response = Response.new
		
			services = ServiceSetting.where("service IN ('#{Constants::FDI_DTH_LABEL}', '#{Constants::FDI_TIGO_LABEL}', '#{Constants::FDI_ELECTRICITY_LABEL}')")
				
			services.each do |service|
				service.update(:threshold_amount => thresholdAmount)
				updateEmails(service, notificationsEmails)
			end	

			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    		response.Response_Msg=(I18n.t :THRESHOLD_UPDATED_SUCCESSFULLY)  
  		return response  
	end


	def self.isServiceAvaialable(serviceName)
		serviceSetting = getServiceDetailsWithName(serviceName)
		if serviceSetting != nil
			if serviceSetting.status == 1
				return true
			else
				return false
			end	
		else
			return false
		end
	end

	def self.getServiceThresholdAmount(serviceName)
		serviceSetting = getServiceDetailsWithName(serviceName)
		if serviceSetting != nil
			return serviceSetting.threshold_amount
		else
			return 0
		end	
	end	

	def self.getServiceAggregatorBalance(serviceName)
		serviceSetting = getServiceDetailsWithName(serviceName)
		if serviceSetting != nil
			return serviceSetting.balance
		else
			return 0
		end	
	end	


end
