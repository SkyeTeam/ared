class Deployment < ApplicationRecord
  belongs_to :country, optional: true
  belongs_to :district, optional: true
  belongs_to :town, optional: true
  belongs_to :officer, optional: true
  belongs_to :maintenance_officer, optional: true


  def location 
    "#{country.try(:name)} #{district.try(:name)} #{town.try(:name)}".titleize 
   end
end
