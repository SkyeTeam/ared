class User < ApplicationRecord

    mount_uploader :image, ImageUploader
    has_one :user_account
    has_one :user_govt_account
    has_many :loans
    has_many :txn_headers
    has_many :agent_credit_limits
    has_many :agent_commissions
    has_many :agent_penalty_details
    has_many :booths

    authenticates_with_sorcery!

    has_and_belongs_to_many :kiosks
    has_many :wifi_bookings

    # encrypt password
    has_secure_password

    validates_presence_of :name, :email, :password_digest

    validates :password, length: { minimum: 4 }, if: -> { new_record? || changes[:crypted_password] }
    validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
 
    def valid_password?(password)
        crypted_password == password || super(password)
    end

    def change_password!(new_password)
        self.send(:"#{sorcery_config.password_attribute_name}=", new_password)
        sorcery_adapter.save
    end

    def self.getRoleList(urlBarValue)
        if urlBarValue.present? && urlBarValue == 'agent'
            selectList = {'Micro Franchisee': 'agent'}
        else  
            selectList = {'Administrator': 'admin', 'Content Manager': 'content_manager' , 'Micro Franchisee': 'agent', 'Sales': 'sales', 'Technician': 'maintainer',  }
        end 
      return selectList  
    end  

    def self.getUserListQuery
        query = "id != #{ConstantsHelper::OPERATOR_ID} AND "
        query << "id != #{ConstantsHelper::COMMISSION_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_CASH_ACCOUNT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_PENALTY_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_PROFIT_AND_LOSE_ACCOUNT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_TAX_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_GOVT_TAX_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_FEE_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_SALES_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_PURCHASES_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::ANONYMOUS_USER_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_GOVT_BANK_ACCOUT_ID} AND "
        query << "id != #{ConstantsHelper::OPERATOR_COMMISSION_RECEIVABLE_ACCOUT_ID}"
        return query        
    end   

    def self.getNameDisValue(user)
        if user.role.eql? Constants::AGGREGATOR_DB_LABEL
            return user.business_name
        else
            return user.name.to_s + " " + user.last_name.to_s
        end    
    end    


    def self.getRoleDisValue(role)
        if role.eql? Constants::ADMIN_DB_LABEL
         
            return Constants::ADMIN_VIEW_LABEL
        
        elsif role.eql? Constants::AGENT_DB_LABEL
        
            return Constants::AGENT_VIEW_LABEL
        
        elsif role.eql? Constants::CONTENT_MANAGER_DB_LABEL
        
            return Constants::CONTENT_MANAGER_VIEW_LABEL
        
        elsif role.eql? Constants::AGGREGATOR_DB_LABEL
        
            return Constants::AGGREGATOR_VIEW_LABEL
        
        else
        
            return role
        
        end        
    end                


    def self.getUserRoleIntValue(user)
        if user.role.eql? Constants::ADMIN_DB_LABEL
        
            return Constants::ADMIN_USER_ROLE
        
        elsif user.role.eql? Constants::AGENT_DB_LABEL
        
            return Constants::MF_USER_ROLE  

        elsif user.role.eql? Constants::CONTENT_MANAGER_DB_LABEL

            return Constants::CONTENT_MANAGER_USER_ROLE  

        elsif user.role.eql? Constants::AGGREGATOR_DB_LABEL

            return Constants::AGGREGATOR_USER_ROLE  

        end    
    end   

    def self.getUserRole(intRole)
        if intRole == Constants::ADMIN_USER_ROLE
            
            return Constants::ADMIN_VIEW_LABEL
        
        elsif intRole == Constants::MF_USER_ROLE
            
            return Constants::AGENT_VIEW_LABEL

        elsif intRole == Constants::CONTENT_MANAGER_USER_ROLE
        
            return Constants::CONTENT_MANAGER_VIEW_LABEL

        elsif intRole == Constants::AGGREGATOR_USER_ROLE
    
            return Constants::AGGREGATOR_VIEW_LABEL                            

        else
            
            return intRole

        end    
    end    


    def self.getUserStatusList()
        statusList = {Active: 'active', Suspend: 'suspended'}
      return statusList  
    end  
    

    def self.getViewAdminRolesStatus(user,user_bar)
        status = 0

        #This case is new user
        if user.id == nil
            if user.role == 'admin'
                status = 1
            elsif user.role == 'agent'
                status = 0  
            end  
        end 

        #This case is for update 
        if user.id != nil
            if user.role == 'admin'
               status = 1
            elsif user.role == 'agent'
               status = 0  
            end  
        end 

        #This case is for assign admin roles
        if user_bar.present? 
            if user_bar == 'agent' 
              status = 0
            end
        end   
        
      return status
    end  

    def self.getPageHeaderForRegForm(user)
        if user.id != nil
           return "Modify User"
        else
           return "New User"
        end
    end    

    def self.getPageHeaderForNewForm(status)
        if status == 'update'
            return "Modify User"
        elsif status == 'suspend' || status == 'agent_suspend'
            return "Suspend User"
        elsif status == 'delete' || status == 'agent_delete'
            return "Terminate User"
        end
    end   

    def self.getCSSClass(userStatus)
        if userStatus == 'disabled'
            return "btn btn-danger btn-round"
        elsif userStatus == 'suspended'
            return "btn btn-warning btn-round"
        else
            return "btn btn-success btn-round"
        end
    end 

    def self.getTotalRolesCount
        totalroles = Role.all.count
      return totalroles  
    end   

    def self.compare(id,na)
        @uv= RoleHistory.where(user_id: id)
        @uv.each do |f|
            if f[:role_id]==na.to_i then
              return true
              break
            else
            end
        end
    end
    
    def self.search(details, dateRange)

        if dateRange.present?
            dateRangeArray = dateRange.split(" - ", 2)
            fromDate = Date.parse(dateRangeArray[0].to_s).beginning_of_day
            toDate = Date.parse(dateRangeArray[1].to_s).end_of_day
            Log.logger.info("At line = #{__LINE__} from date is = #{fromDate} and to date is = #{toDate}")
            response = getUsersListBWDates(fromDate, toDate)
            return response
        end  

        response = validate(details, nil, true)
        if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        end
         
        phone = getUserPhone(details)

        response = searchByPhone(phone)
        response.object=(false)
        
        return response
    end   


    def self.getUserPhone(details)
        if details.include? '-'
                detailsArray =  details.split(" - ", 2)
            return detailsArray[1]
        else
            return details
        end    
    end 

    def self.getUsersListBWDates(fromDate, toDate)
        response = Response.new
        user = User.where("created_at >= '#{fromDate}' and created_at <= '#{toDate}'").where(getUserListQuery).order('created_at DESC')
        Log.logger.info("Inside the User Manager's method getUsersListBWDates at line = #{__LINE__} and check user list is exist or not =#{user.present?}")
        
        if user.present?
            Log.logger.info("Inside the Cash Deposit Manager's method searchByName at line = #{__LINE__} and agent is present and information give back to the controller")
            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.User_Object=(user)
            response.object=(true)
          return response
        else
            response.Response_Code=(Error::USER_NOT_EXIST)
            response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
          return response
        end 
    end    
    

    def self.searchByPhone(phone)
            response = validate(phone, nil, true)
            if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
                return response
            end
            
            Log.logger.info("The User Manager's method searchByPhone is called at line = #{__LINE__} and search the phone in the database is = #{phone}")

            user = User.find_by_phone(phone)
            Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and check agent is exist or not =#{user.present?}")

            if user.present?
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.User_Object=(user)
                return response
            else
                Log.logger.info("Inside the Cash Deposit Manager's method searchByPhone at line = #{__LINE__} and agent is not exist.") 
                response.Response_Code=(Error::USER_NOT_EXIST)
                response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
                return response
            end
        return response    
    end

    def self.searchByPhoneName(params)
        response = Response.new
        phoneOrName = params[:phoneOrName]
        Log.logger.info("At line #{__LINE__} inside the User class and searchByPhoneName method is called with phoneOrName = #{phoneOrName}")

        if !phoneOrName.present?
                Log.logger.info("At line = #{__LINE__} and phoneOrName is come nil from the web")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
        end

        user = User.where('role = ?', Constants::MF_USER_ROLE).where('phone = ?', User.getUserPhone(phoneOrName)).first
        Log.logger.info("At line #{__LINE__} and check agent is exist or not with above phone number = #{user.present?}")

        if user.present?
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.User_Object=(user)
            return response
        else
                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
            return response
        end 
    end


    def self.searchByName(enteredName)
            response = Response.new
            response = validate(nil, enteredName, false)
           
            if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
                return response
            end

            Log.logger.info("The Cash Deposit Manager's method searchByName is called at line = #{__LINE__} and search the name in the database is = #{enteredName}")

            length = 0  
            query = ''  
            
            if enteredName.include? ' '
                x = enteredName.split(' ') #Split on the basis of the spaces
                length = x.length
            
                for i in (0..length-1)
                  query = query.to_s + " LOWER(name) LIKE '%" + x[i].downcase + "%' or "
                  query = query.to_s + " LOWER(last_name) LIKE '%" + x[i].downcase + "%' or"
                end  
                query = query.chomp('or')
            else
                query = "LOWER(name) LIKE '%" + enteredName.downcase + "%' or LOWER(last_name) LIKE '%" + enteredName.downcase + "%'"
            end 

            user = User.where(query)
            Log.logger.info("Inside the Cash Deposit Manager's method searchByName at line = #{__LINE__} and check agent is exist or not =#{user.present?}")
            
            if user.present?
                Log.logger.info("Inside the Cash Deposit Manager's method searchByName at line = #{__LINE__} and agent is present and information give back to the controller")
                response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
                response.User_Object=(user)
              return response
            else
                response.Response_Code=(Error::USER_NOT_EXIST)
                response.Response_Msg=(I18n.t :USER_NOT_EXIST_MSG) 
              return response
            end 
            
        return response    
    end

    def self.validate(phone, name, isSearchByPhone)
        response = Response.new

            if isSearchByPhone
                if !phone.present?
                        Log.logger.info("At line = #{__LINE__} and phone is come nil from the software")
                        response.Response_Code=(Error::INVALID_DATA)
                        response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                    return response
                end  
            else
                if !name.present?
                        Log.logger.info("At line = #{__LINE__} and name is come nil from the software")
                        response.Response_Code=(Error::INVALID_DATA)
                        response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
                    return response
                end  
            end

            response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
            response.Response_Msg=("") 
        return response      
    end   

    def self.getAdminRoleDisplayStyle(loggedinAdminId, updateUserId)
        if updateUserId.present?
            if loggedinAdminId.to_i == updateUserId.to_i
                return "none"
            else
                return "block"
            end    
        else
            return "block"
        end        
    end    

    def self.getLoggedinAdminImage(currentUser)
        if currentUser.present?
            if currentUser.image.present?
                return currentUser.image
            else
                return "user.png"
            end    
        else
            return "user.png"
        end
    end

    def self.getUserImage(currentUser)
        if currentUser.present?
            if currentUser.image.present?
                return currentUser.image
            else
                return "user.png"
            end    
        else
            return "user.png"
        end
    end

    def self.getOnClickEvent(user)
        if user.role.eql? Constants::AGENT_DB_LABEL
            return "onclick='viewProfile(#{user.id})'"
        else
            return Constants::BLANK_VALUE
        end    
    end    

    # User roles: If you change this order, please map the new order to the old one: http://api.rubyonrails.org/v4.1.0/classes/ActiveRecord/Enum.html
    enum status: {
        disabled: 1,
        active:   2,
        suspended: 3
    }
  
    enum role:  [ :admin, :agent, :sales, :maintainer, :content_manager, :aggregator ]
 
    enum rwandaDistt:  [
        :Bugesera,
        :Burera,
        :Gakenke,
        :Gasabo,
        :Gatsibo,
        :Gicumbi,
        :Gisagara,
        :Huye,
        :Kamonyi,
        :Karongi,
        :Kayonza,
        :Kicukiro,
        :Kirehe,
        :Muhanga,
        :Musanze,
        :Ngoma,
        :Ngororero,
        :Nyabihu,
        :Nyagatare,
        :Nyamagabe,
        :Nyamasheke,
        :Nyanza,
        :Nyarugenge,
        :Nyaruguru,
        :Rubavu,
        :Ruhango,
        :Rulindo,
        :Rusizi,
        :Rutsiro,
        :Rwamagana]

    enum ugandaDistt:  [
        :Abim,
        :Adjumani,
        :Agago,
        :Alebtong,
        :Amolatar,
        :Amudat,
        :Amuria,
        :Amuru,
        :Apac,
        :Arua,
        :Budaka,
        :Bududa,
        :Bugiri,
        :Buhweju,
        :Buikwe,
        :Bukedea,
        :Bukomansimbi,
        :Bukwa,
        :Bulambuli,
        :Buliisa,
        :Bundibugyo,
        :Bushenyi,
        :Busia,
        :Butaleja,
        :Butambala,
        :Buvuma,
        :Buyende,
        :Dokolo,
        :Gomba,
        :Gulu,
        :Hoima,
        :Ibanda,
        :Iganga,
        :Isingiro,
        :Jinja,
        :Kaabong,
        :Kabale,
        :Kabarole,
        :Kaberamaido,
        :Kalangala,
        :Kaliro,
        :Kalungu,
        :Kampala,
        :Kamuli,
        :Kamwenge,
        :Kanungu,
        :Kapchorwa,
        :Kasese,
        :Katakwi,
        :Kayunga,
        :Kibaale,
        :Kiboga,
        :Kibuku,
        :Kiruhura,
        :Kiryandongo,
        :Kisoro,
        :Kitgum,
        :Koboko,
        :Kole,
        :Kotido,
        :Kumi,
        :Kween,
        :Kyankwanzi,
        :Kyegegwa,
        :Kyenjojo,
        :Lamwo,
        :Lira,
        :Luuka,
        :Luweero,
        :Lwengo,
        :Lyantonde,
        :Manafwa,
        :Maracha,
        :Masaka,
        :Masindi,
        :Mayuge,
        :Mbale,
        :Mbarara,
        :Mitooma,
        :Mityana,
        :Moroto,
        :Moyo,
        :Mpigi,
        :Mubende,
        :Mukono,
        :Nakapiripirit,
        :Nakaseke,
        :Nakasongola,
        :Namayingo,
        :Namutumba,
        :Napak,
        :Nebbi,
        :Ngora,
        :Ntoroko,
        :Ntungamo,
        :Nwoya,
        :Otuke,
        :Oyam,
        :Pader,
        :Pallisa,
        :Rakai,
        :Rubirizi,
        :Rukungiri,
        :Serere,
        :Sheema,
        :Sironko,
        :Soroti,
        :Ssembabule,
        :Tororo,
        :Wakiso,
        :Yumbe,
        :Zombo]    
  
    enum cntry: [
        :Afghanistan,
        :Albania,
        :Algeria,
        :Andorra,
        :Angola,
        :Antigua_and_Barbuda,
        :Argentina,
        :Armenia,
        :Aruba,
        :Australia,
        :Austria,
        :Azerbaijan,
        :Bahamas_The,
        :Bahrain,
        :Bangladesh,
        :Barbados,
        :Belarus,
        :Belgium,
        :Belize,
        :Benin,
        :Bhutan,
        :Bolivia,
        :Bosnia_and_Herzegovina,
        :Botswana,
        :Brazil,
        :Brunei,
        :Bulgaria,
        :Burkina_Faso,
        :Burma,
        :Burundi,
        :Cambodia,
        :Cameroon,
        :Canada,
        :Cabo_Verde,
        :Central_African_Republic,
        :Chad,
        :Chile,
        :China,
        :Colombia,
        :Comoros,
        :Congo_Democratic_Republic_of_the,
        :Congo_Republic_of_the,
        :Costa_Rica,
        :Cote_dIvoire,
        :Croatia,
        :Cuba,
        :Curacao,
        :Cyprus,
        :Czechia,
        :Denmark,
        :Djibouti,
        :Dominica,
        :Dominican_Republic,
        :East_Timor,
        :Ecuador,
        :Egypt,
        :El_Salvador,
        :Equatorial_Guinea,
        :Eritrea,
        :Estonia,
        :Ethiopia,
        :Fiji,
        :Finland,
        :France,
        :Gabon,
        :Gambia_The,
        :Georgia,
        :Germany,
        :Ghana,
        :Greece,
        :Grenada,
        :Guatemala,
        :Guinea,
        :Guinea_Bissau,
        :Guyana,
        :Haiti,
        :Holy_See,
        :Honduras,
        :Hong_Kong,
        :Hungary,
        :Iceland,
        :India,
        :Indonesia,
        :Iran,
        :Iraq,
        :Ireland,
        :Israel,
        :Italy,
        :Jamaica,
        :Japan,
        :Jordan,
        :Kazakhstan,
        :Kenya,
        :Kiribati,
        :Korea_North,
        :Korea_South,
        :Kosovo,
        :Kuwait,
        :Kyrgyzstan,
        :Laos,
        :Latvia,
        :Lebanon,
        :Lesotho,
        :Liberia,
        :Libya,
        :Liechtenstein,
        :Lithuania,
        :Luxembourg,
        :Macau,
        :Macedonia,
        :Madagascar,
        :Malawi,
        :Malaysia,
        :Maldives,
        :Mali,
        :Malta,
        :Marshall_Islands,
        :Mauritania,
        :Mauritius,
        :Mexico,
        :Micronesia,
        :Moldova,
        :Monaco,
        :Mongolia,
        :Montenegro,
        :Morocco,
        :Mozambique,
        :Namibia,
        :Nauru,
        :Nepal,
        :Netherlands,
        :New_Zealand,
        :Nicaragua,
        :Niger,
        :Nigeria,
        :North_Korea,
        :Norway,
        :Oman,
        :Pakistan,
        :Palau,
        :Palestinian_Territories,
        :Panama,
        :Papua_New_Guinea,
        :Paraguay,
        :Peru,
        :Philippines,
        :Poland,
        :Portugal,
        :Qatar,
        :Romania,
        :Russia,
        :Rwanda,
        :Saint_Kitts_and_Nevis,
        :Saint_Lucia,
        :Saint_Vincent_and_the_Grenadines,
        :Samoa,
        :San_Marino,
        :Sao_Tome_and_Principe,
        :Saudi_Arabia,
        :Senegal,
        :Serbia,
        :Seychelles,
        :Sierra_Leone,
        :Singapore,
        :Sint_Maarten,
        :Slovakia,
        :Slovenia,
        :Solomon_Islands,
        :Somalia,
        :South_Africa,
        :South_Korea,
        :South_Sudan,
        :Spain,
        :Sri_Lanka,
        :Sudan,
        :Suriname,
        :Swaziland,
        :Sweden,
        :Switzerland,
        :Syria,
        :Taiwan,
        :Tajikistan,
        :Tanzania,
        :Thailand,
        :Timor_Leste,
        :Togo,
        :Tonga,
        :Trinidad_and_Tobago,
        :Tunisia,
        :Turkey,
        :Turkmenistan,
        :Tuvalu,
        :Uganda,
        :Ukraine,
        :United_Arab_Emirates,
        :United_Kingdom,
        :Uruguay,
        :Uzbekistan,
        :Vanuatu,
        :Venezuela,
        :Vietnam,
        :Yemen,
        :Zambia,
        :Zimbabwe]
end
