class Agent < ApplicationRecord
  has_many :visits
  has_many :maintenance_officers, :through => :visits# Edit :needs to be plural same as the has_many relationship   

  has_many :reviews,dependent: :destroy 
  belongs_to :officer
  before_save :set_photo
  validates :phone, :uniqueness => true, :presence => true
  validates :name, :presence => true


  def account_age 
    1
  end

 private 
  def set_photo 
    gravatar = Digest::MD5::hexdigest("random@gmail.com").downcase
    url = "http://gravatar.com/avatar/#{gravatar}.png?s=#{64}"
    self.photo = url 
  end
end
