class CreditSuspend < ApplicationRecord

	def self.getCurrnetCreditStatus(agentID)
		Log.logger.info("Inside the Credit Suspend class's method getCurrnetCreditStatus at line = #{__LINE__} and check the status of the agentID = #{agentID}")

		creditSuspend = getAgent(agentID)

		if creditSuspend.present?
			if creditSuspend.is_blocked == Constants::CREDIT_SUSPEND_STATUS
				creditStatus = Constants::SUSPEND__VIEW_LABEL
			else
				creditStatus = Constants::ACTIVE_VIEW_LABEL
			end	

		else
			creditStatus = Constants::ACTIVE_VIEW_LABEL
		end	
	  return creditStatus	
	end	

	def self.updateCreditStauts(params, loggedInAdmin)
		
		response = Response.new
		agentID = params[:agentID]
      	blockedStatus = params[:blockedStatus]
      
		Log.logger.info("Inside the Credit Suspend class's method updateCreditStauts at line = #{__LINE__} and check the status of the agentID = #{agentID} and status is = #{blockedStatus}")

		ActiveRecord::Base.transaction do
			creditSuspend = getAgent(agentID)
			if creditSuspend.present? # if agent is present then update his status 
				updateAgentCreditStauts(agentID,blockedStatus)
			else # if not then insert the agent
				saveAgentCreditStauts(agentID,blockedStatus)
			end	

			ActivityLog.saveActivity((I18n.t :MF_CREDIT_SUS_ACTIVITY_MSG, :status => getCurrentCreditLabel(blockedStatus)), loggedInAdmin.id, agentID, params)

		end	
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
			response.Response_Msg=(I18n.t :CREDIT_SUSPEND_STATUS_MSG)

		return response	
	end

	def self.getAgent(agentID)
		creditSuspend = CreditSuspend.find_by_agent_id(agentID)
		return creditSuspend
	end	

	def self.saveAgentCreditStauts(agentID,blockedStatus)
		creditSuspend = CreditSuspend.new
		creditSuspend.agent_id = agentID
		creditSuspend.is_blocked = blockedStatus
		creditSuspend.save
	end

	def self.updateAgentCreditStauts(agentID,blockedStatus)
		creditSuspend = getAgent(agentID)
		creditSuspend.update_attribute(:is_blocked,blockedStatus)
	end	

	def self.getColor(currentCreditStatus)
		if currentCreditStatus.eql? Constants::ACTIVE_VIEW_LABEL
        	return Constants::CREDIT_COLOR
        else
        	return Constants::DEDUCT_COLOR
        end
	end

	def self.isAgentAbleForCredit(agentID)
		creditSuspend = getAgent(agentID)
		if creditSuspend.present?
			if creditSuspend.is_blocked == Constants::CREDIT_SUSPEND_STATUS
				creditStatus = Constants::BOOLEAN_FALSE_LABEL
			else
				creditStatus = Constants::BOOLEAN_TRUE_LABEL
			end	
		else
			creditStatus = Constants::BOOLEAN_TRUE_LABEL
		end	
	  return creditStatus
	end	

	def self.getCurrentCreditLabel(status)
		if status.to_i == 0
			return Constants::ACTIVE_VIEW_LABEL
		else
			return Constants::SUSPEND__VIEW_LABEL
		end
	end		

end
