# This class used as a Manager in MVC
# :reek:ClassVariable { enabled: false }

class SymboxLog

  require 'properties'	 	 

  def self.logger
    @@logger ||= Logger.new(Properties::SYMBOX_LOGS_FILE_LOCATION , 'daily')
  end

end	