class MaintenanceOfficer < ApplicationRecord
  has_many :visits
  has_many :agents, :through => :visits# Edit :needs to be plural same as the has_many relationship  
  
  belongs_to :country
  belongs_to :district
  belongs_to :town

  belongs_to :country, optional: true
  belongs_to :town, optional: true
  belongs_to :district, optional: true
  has_many :deployments ,dependent: :destroy
 
  validates :email, :phone, :account, :uniqueness => true
  validates :password, :first_name,:last_name, :presence => true
  validates_length_of :phone, :minimum => 10

  before_save :set_account
  mount_uploader :photo, PhotoUploader

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  state_machine initial: :new do
     event :active do
        transition :new => :activated
      end
     event :deploy do
        transition :activated => :deployed
      end
      event :suspend do
        transition :deployed => :suspended
      end
      event :terminate do
        transition :suspended => :terminated
      end
  end



  def name 
    "#{first_name} #{middle_name} #{last_name}"
  end

   def location 
    "#{country.name} ,#{district.name}, #{town.name}".titleize 
   end
private 
  def set_account 
    self.account = phone.split(//).last(9).join
  end
  
end



  