class GatewayLog < ApplicationRecord

	def self.getGatewayLogID
		return Utility.getCapAlphaNumRandomNumber(20)
	end	

	def self.saveGatewayLog(logID, agentID, headerID, aggregatorName, aggregatorBalance, txnAmount, requestData, responseData)
		Log.logger.info("At line = #{__LINE__} and inside the GatewayLog log class and saveGatewayLog method is called with parameters as")
			parameters = "Log ID = #{logID}, Agent id  = #{agentID}, headerID = #{headerID}, aggregatorName = #{aggregatorName},"
			parameters << "txnAmount = #{txnAmount}, aggregatorBalance = #{aggregatorBalance},"
			parameters << "requestData = #{requestData}, \n responseData = #{responseData}"
		Log.logger.info(parameters)
		
		gatewayLog = GatewayLog.new
			gatewayLog.log_id = logID
			gatewayLog.agent_id = agentID
			gatewayLog.header_id = headerID
			gatewayLog.aggregator_name = aggregatorName
			gatewayLog.aggregator_balance = TxnHeader.my_money(aggregatorBalance).to_f
			gatewayLog.txn_amount = TxnHeader.my_money(txnAmount).to_f
			gatewayLog.request = requestData
			gatewayLog.response = responseData
		gatewayLog.save	
	end	

	def self.updateRequestData(logID, requestData)
		gatewayLog = GatewayLog.find_by_log_id(logID)
		gatewayLog.update_attribute(:request, requestData)
	end	

	def self.updateResponseData(logID, responseData)
		gatewayLog = GatewayLog.find_by_log_id(logID)
		gatewayLog.update_attribute(:response, responseData)
	end	

	def self.updatePendingAtt(logID, headerID, aggregatorBalance)
		gatewayLog = GatewayLog.find_by_log_id(logID)
		gatewayLog.update_attributes(:header_id => headerID, :aggregator_balance => aggregatorBalance)
	end	

	def self.getPreviousGatewayLog(aggregatorName, headerID)
			Log.logger.info("At line = #{__LINE__} and check the previous GatewayLog of aggregatorName = #{aggregatorName} and headerID = #{headerID}")
			previousGatewayLog = GatewayLog.where("aggregator_name = '#{aggregatorName}' AND header_id::INT < #{headerID}").order('id DESC').first
		return previousGatewayLog	
	end	

	def self.getCurrentGatewayLog(aggregatorName, headerID)
			Log.logger.info("At line = #{__LINE__} and check the current GatewayLog of aggregatorName = #{aggregatorName} and headerID = #{headerID}")
			nextGatewayLog = GatewayLog.where("aggregator_name = '#{aggregatorName}' AND header_id::INT = #{headerID}").first
		return nextGatewayLog	
	end	
	
	def self.getNextGatewayLog(aggregatorName, headerID)
			Log.logger.info("At line = #{__LINE__} and check the next GatewayLog of aggregatorName = #{aggregatorName} and headerID = #{headerID}")
			nextGatewayLog = GatewayLog.where("aggregator_name = '#{aggregatorName}' AND header_id::INT > #{headerID}").order('id ASC').first
		return nextGatewayLog	
	end	


	def self.getFormalRqstResData(requestData, responseData)
		response = Response.new
		if Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[1]
			if requestData.start_with?(Properties::FDI_TXNS_URL) #This means that request send to the FDI
					pdtType = getPdtType(requestData)
					response.requestTxnID=(getTxnRef(requestData))
					response.clientNo=(getCustomerPhone(requestData))
					response.txnAmount=(getTxnAmount(requestData))
					response.requestTime=(getRequestTime(requestData))

					response = setFDIResposeValues(pdtType, response, responseData)
	          	return response
			elsif requestData.start_with?("<?xml version=\"1.0\"?>") #This means that request send to the MTN or AIRTEL
				if isMTNRequest(requestData)
						response.requestTxnID=(getTagValue(requestData, 'seq'))
						response.clientNo=(getTagValue(requestData, 'dest'))
						response.txnAmount=(getTagValue(requestData, 'amount'))
						response.requestTime=('')
						
						response = setMTNResponseValues(response, responseData)
			        return response    
				else
						response.requestTxnID=(getTagValue(requestData, 'EXTREFNUM'))
						response.clientNo=(getTagValue(requestData, 'MSISDN2'))
						response.txnAmount=(getTagValue(requestData, 'AMOUNT'))
						response.requestTime=(getTagValue(requestData, 'DATE'))

						response = setAirtelResponseValue(response, responseData)
					return response
				end	
			end
		elsif Properties::COUNTRY_NAME.eql? Constants::LOGIN_COUNTRY_LIST[2]

		end	
	end	

	def self.getPdtType(requestData)
			requestDataArray = requestData.split('?pdt=', 2)
	        pdtArray = requestDataArray[1].split('&', 2)
	        pdtType = pdtArray[0]
	    return pdtType    
	end	

	def self.getTxnRef(requestData)
			requestDataArray = requestData.split('&trxref=', 2)
        	txnRefArray = requestDataArray[1].split('&', 2)
        	txnRef = txnRefArray[0]
        return txnRef	
	end	

	def self.getCustomerPhone(requestData)
			requestDataArray = requestData.split('&customer_msisdn=', 2)
        	customerPhoneArray = requestDataArray[1].split('&', 2)
        	customerPhone = customerPhoneArray[0]
        return customerPhone	
	end	

	def self.getTxnAmount(requestData)
			requestDataArray = requestData.split('&amount=', 2)
        	txnAmountArray = requestDataArray[1].split('&', 2)
        	txnAmount = txnAmountArray[0]
        return txnAmount	
	end	

	def self.getRequestTime(requestData)
			requestDataArray = requestData.split('&tstamp=', 2)
        	txnTimeArray = requestDataArray[1].split('&', 2)
        	requestTime = txnTimeArray[0]
        return requestTime	
	end	

	def self.isMTNRequest(requestData)
		if requestData.include? "<seq>" #This means that request send to the MTN
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end	
	end

	def self.getTagValue(requestData, tagName)
		 	firstArray = requestData.split("<#{tagName}>", 2)
        	secondArray = firstArray[1].split("</#{tagName}>", 2)
        	tagValue = secondArray[0]
        return tagValue	
	end	

	def self.setFDIResposeValues(pdtType, response, responseData)
			resData = JSON.parse(responseData)
			statusCode = resData['status_code']
			statusMsg = resData['status_msg']
		
			if pdtType.eql? Constants::AIRTIME_LOWER_CASE_LABEL
				firstSplit = statusMsg["Your combined wallet balance is: "]
	        elsif pdtType.eql? Constants::FDI_ELECTRICITY_LOWER_CASE_LABEL or pdtType.eql? Constants::FDI_STARTIMES_LOWER_CASE_LABEL
				firstSplit = statusMsg["Your balance is: "]
	    	end	
		
		  	balFirst = statusMsg.split(firstSplit,2)  
          	serviceBalance = balFirst[1]
		
			response.serviceTxnID=('')
			response.serviceBalance=(serviceBalance)
			response.serviceResponseCode=(statusCode)
	      	response.serviceResponseMsg=(statusMsg)
	    return response  	
	end	

	def self.setMTNResponseValues(response, responseData)
			responseCodeArray = responseData.split('responseCode=',2)
          	txnRefArray = responseCodeArray[0].split('txRefId=',2)

          	responseMsgArray = responseCodeArray[1].split('; responseMessage',2)
          	response.serviceResponseCode=(responseMsgArray[0])

          	serviceResponseMsg = responseMsgArray[1].gsub(/=/, '').gsub(/;/, '')
          	response.serviceResponseMsg=(serviceResponseMsg)

        	mtnTokkenArray = txnRefArray[1].split(';',2)
        	mtnToken = mtnTokkenArray[0]
        	response.serviceTxnID=(mtnToken)

        	firstBalanceSplit = mtnTokkenArray[1].split('origBalance=',2)
        	secondBalanceSplit = firstBalanceSplit[1].split('; destBalance',2) 
        	mtnServiceBalance =  secondBalanceSplit[0]
        	response.serviceBalance=(mtnServiceBalance)
        return response
	end	

	def self.setAirtelResponseValue(response, responseData)
			first_array = responseData.split("<TXNSTATUS>",2)
    		second_array = first_array[1].split("</TXNSTATUS>",2)
			
			response.serviceResponseCode=(second_array[0])

	        response_txnId = second_array[1].split("<TXNID>",2)
	        final_response_txnId = response_txnId[1].split("</TXNID>",2)
	        airtel_response_txnID = final_response_txnId[0]

	        response.serviceTxnID=(airtel_response_txnID)

	        first_mez_array = final_response_txnId[1].split("<MESSAGE>",2)
	        x = first_mez_array[1].split(":",3)
	        final_mez_array = x[2].split("</MESSAGE>",2) 

	        response.serviceResponseMsg=(final_mez_array[0])

	        balanceDetail = final_mez_array[0]
	        firstSplit = balanceDetail["Your new balance is "]
	        secondSplit = balanceDetail[" RWF"]
	          
	        balFirst = balanceDetail.split(firstSplit,2)  
	        balSecond = balFirst[1].split(secondSplit,2)
	        airtelServiceBalance = balSecond[0]

	        response.serviceBalance=(airtelServiceBalance)
	    return response    
	end	        

end