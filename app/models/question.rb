class Question < ApplicationRecord
  belongs_to :survey
  has_many :answers, dependent: :destroy

  enum layout: [:multi, :single] # multi = checkbox | single = radio

end
