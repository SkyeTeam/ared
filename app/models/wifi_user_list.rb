class WifiUserList < ApplicationRecord

	before_create :create_code

  # enum kind: {
  #   agent: 1,
  #   consumer: 2
  # }

  # belongs_to :user
  # belongs_to :kiosk

  def create_code
    all_codes = WifiUserList.pluck(:refer_code).compact # get all nonnull-codes in Array

    loop do
      # need a number with 10 digits, but not more than 0xFFFFFFFF = 4_294_967_295
      number_w_10digits = 1_000_000_000 + rand(3_294_967_296)
      self.refer_code = number_w_10digits.to_s(16).upcase
      break if !all_codes.include?(self.refer_code)
    end
  end
end
