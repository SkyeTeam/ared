class Firmware < ApplicationRecord
  ATTRIBUTES_FOR_STATUS_CHECKSUM = ["id", "title", "file", "version", "revision", "areas", "active", "areas"]

  mount_uploader :file, FirmwareUploader
  validates_presence_of :file

  has_and_belongs_to_many :areas

  validates :version, :revision, :title, presence: true
  validates :version, :revision, uniqueness: true

  def file_as_url
    Settings.host_uri + self.file.url
  end

  def attributes_for_status
    h = attributes
    h.delete_if{|k,v| !ATTRIBUTES_FOR_STATUS_CHECKSUM.include?(k)}
    h["file"] = self.file.url
    h["areas"] = area_ids
    h
  end

  def attributes_for_deployment
    h = attributes
    h.delete_if{|k,v| !ATTRIBUTES_FOR_STATUS_CHECKSUM.include?(k)}
    h["file"] = file_as_url
    h["areas"] = area_ids
    h
  end

end
