class TranscationCategory < ApplicationRecord

	def self.getOperatorsList(currentAgent, params)
		AppLog.logger.info("At line #{__LINE__} inside the TranscationCategory's  and getOperatorsList method is called")
	    response = Response.new
	    unless currentAgent.present?
	        AppLog.logger.info("At line #{__LINE__} and agent is not present")
	        response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	        response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	      return response
	    end
	    AppLog.logger.info("At line #{__LINE__} this request is done by the agent ID = #{currentAgent.id}, name = #{currentAgent.name} and phone = #{currentAgent.phone}")
	    
	    serviceType = params[""+AppConstants::TXN_SUB_CATEGORY_PARAM_LABEL+""]
        AppLog.logger.info("At line #{__LINE__} and serviceType to get the operators list is = #{serviceType}")

        if !serviceType.present?
                AppLog.logger.info("At line = #{__LINE__} and serviceType is come nil from the app")
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response
    	end

    	operatorList = TranscationCategory.where("category_sub_name = '#{serviceType}'").select(:id,:category_name).order('category_name ASC')
    	AppLog.logger.info("At line #{__LINE__} and check that this serviceType has operators or not = #{operatorList.present?}")

    	if operatorList.present?
    			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.object=(operatorList)
      		return response
    	else  
        		response.Response_Code=(Error::NO_RECORD_FOUND)
        		response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
      		return response
    	end
	end	

end
