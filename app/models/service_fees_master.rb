class ServiceFeesMaster < ApplicationRecord

	def self.createServiceFee(loggedInAdmin, params)
		Log.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and createServiceFee method is called")

		serviceType = params[:serviceType]
		subType = params[:subType]
		commPayer = params[:commPayer]
		vatType = params[:vatType]
		effectiveDate = params[:effectiveDate]
		effectiveTime = params[:effectiveTime]
		isUpdate = params[:isUpdate]
		serviceFeesMasterID = params[:serviceFeesMasterID]
		Log.logger.info("At line #{__LINE__} parameters are as serviceType = #{serviceType}, subType = #{subType}, commPayer = #{commPayer}, vatType = #{vatType}, effectiveDate = #{effectiveDate}, effectiveTime = #{effectiveTime} and check that is service fee is update or not = #{isUpdate}")

		aggregatorTotalSlabs = params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"]
 		endUserTotalSlabs = params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"]
 		Log.logger.info("At line #{__LINE__} aggregatorTotalSlabs are = #{aggregatorTotalSlabs} and endUserTotalSlabs are = #{endUserTotalSlabs}")

		response = validate(serviceType, subType, commPayer, vatType, effectiveDate, effectiveTime)
		if response.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return response
        else
        	return saveServiceFeeMaster(loggedInAdmin, params, serviceType, subType, commPayer, vatType, effectiveDate, effectiveTime, isUpdate, serviceFeesMasterID)
        end    
	end	

	def self.saveServiceFeeMaster(loggedInAdmin, params, serviceType, subType, commPayer, vatType, effectiveDate, effectiveTime, isUpdate, serviceFeesMasterID)
		response = Response.new
			
			ActiveRecord::Base.transaction do
				if !(isUpdate.present?)
					serviceFeesMaster = ServiceFeesMaster.new
						serviceFeesMaster.service_type = serviceType
						serviceFeesMaster.sub_type = getSubType(serviceType, subType)
						serviceFeesMaster.effective_date = Utility.getActualDateTime(effectiveDate, effectiveTime) 
						serviceFeesMaster.comm_payer = commPayer
						serviceFeesMaster.vat_type = vatType
					serviceFeesMaster.save

					ActivityLog.saveActivity((I18n.t :SERVICE_FEE_CREATE_ACTIVITY_MSG, :subType => getSubType(serviceType, subType)), loggedInAdmin.id, nil, params)
					
					response.Response_Msg=(I18n.t :SERVICE_FEE_SUCCESS_CREATE_MSG, :serviceType => Utility.getServiceTypeDisplayValue(subType))
				else
					Log.logger.info("At line #{__LINE__} serviceFeesMasterID for update is = #{serviceFeesMasterID}")
					serviceFeesMaster = ServiceFeesMaster.find_by_id(serviceFeesMasterID)
					serviceFeesMaster.update_attributes(:service_type => serviceType, :sub_type => getSubType(serviceType, subType), :effective_date => Utility.getActualDateTime(effectiveDate, effectiveTime), :comm_payer => commPayer, :vat_type => vatType)

					ActivityLog.saveActivity((I18n.t :SERVICE_FEE_UPDATE_ACTIVITY_MSG, :subType => getSubType(serviceType, subType)), loggedInAdmin.id, nil, params)
					
					response.Response_Msg=(I18n.t :SERVICE_FEE_SUCCESS_UDATE_MSG, :serviceType => Utility.getServiceTypeDisplayValue(subType))
				end	

				getRquestedSlabs(loggedInAdmin, params, serviceFeesMaster.id, isUpdate)

			end	
			
			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		return response	
	end	

	def self.getRquestedSlabs(loggedInAdmin, params, serviceFeesMasterID, isUpdate)
		Log.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and getSlabs method is called")
		
		commPayer = params[:commPayer]
		if commPayer.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL

			aggregatorTotalSlabs = params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"]
			sequence = 1
			getRquestedSlabsData(params, aggregatorTotalSlabs, commPayer, Constants::AGGREGATOR_HTML_NAME_LABEL, serviceFeesMasterID, sequence, isUpdate)

		elsif commPayer.eql? Constants::END_USER_COMM_PAYER_DB_LABEL

			endUserTotalSlabs = params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"]
			sequence = 1
			getRquestedSlabsData(params, endUserTotalSlabs, commPayer, Constants::END_USER_HTML_NAME_LABEL, serviceFeesMasterID, sequence, isUpdate)

		elsif commPayer.eql? Constants::BOTH_COMM_PAYER_DB_LABEL

			aggregatorTotalSlabs = params[Constants::AGGREGATOR_HTML_NAME_LABEL.to_s + "TotalSlabs"]
			sequence = 1
			lastSequence = getRquestedSlabsData(params, aggregatorTotalSlabs, Constants::AGGREGATOR_COMM_PAYER_DB_LABEL, Constants::AGGREGATOR_HTML_NAME_LABEL, serviceFeesMasterID, sequence, isUpdate)

			endUserTotalSlabs = params[Constants::END_USER_HTML_NAME_LABEL.to_s + "TotalSlabs"]
			getRquestedSlabsData(params, endUserTotalSlabs, Constants::END_USER_COMM_PAYER_DB_LABEL, Constants::END_USER_HTML_NAME_LABEL, serviceFeesMasterID, lastSequence, isUpdate)

		end	
	end

	def self.getRquestedSlabsData(params, totalSlabs, commPayer, slabFor, serviceFeesMasterID, sequence, isUpdate)
		
			for slab in 0..totalSlabs.to_i - 1
				minAmount = params[slabFor + "MinAmount" + slab.to_s + ""]
				maxAmount = params[slabFor + "MaxAmount" + slab.to_s + ""]
				commType = params[slabFor + "CommType" + slab.to_s + ""]
				totalComm = params[slabFor + "TotalComm" + slab.to_s + ""]
				ourComm = params[slabFor + "OurComm" + slab.to_s + ""]
				agentComm = params[slabFor + "AgentComm" + slab.to_s + ""]
				aggregatorComm = params[slabFor + "AggregatorComm" + slab.to_s + ""]
				
				if minAmount.present?
					if !(isUpdate.present?)
						ServiceFeesSlab.saveServiceFeeSlabs(serviceFeesMasterID, commPayer, minAmount, maxAmount, commType, totalComm, ourComm, agentComm, aggregatorComm, sequence)
					else
						Log.logger.info("At line #{__LINE__} inside the else block to update the slabs data")
						ServiceFeesSlab.updateServiceFeeSlabs(serviceFeesMasterID, commPayer, minAmount, maxAmount, commType, totalComm, ourComm, agentComm, aggregatorComm, sequence)
					end	
					sequence = sequence + 1
				end	
			end	
		return sequence	
	end	

	def self.validate(serviceType, subType, commPayer, vatType, effectiveDate, effectiveTime)
		response = Response.new
	        
	        if !serviceType.present? || !commPayer.present? || !vatType.present? || !effectiveDate.present? || !effectiveTime.present?
	        		Log.logger.info("At line #{__LINE__} some data is come nil from the software")	
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end

	 		if serviceType.eql? Constants::MTOPUP_LABEL and !subType.present?
	        		Log.logger.info("At line #{__LINE__} subType is come nil from the software")	
	                response.Response_Code=(Error::INVALID_DATA)
	                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
	            return response    
	 		end	

	 		if !Utility.isValidYear(effectiveDate.to_s)
        			Log.logger.info("At line = #{__LINE__} and from date has not a valid year ")
       				response.Response_Code=(Error::INVALID_DATE)
	    			response.Response_Msg=(I18n.t :INVALID_DATE_MSG) 
	    		return response
			end

			effectiveDate = Time.parse(Utility.getActualDateTime(effectiveDate, effectiveTime).to_s)
			todayDate = Time.now
            if effectiveDate <= todayDate
	       			response.Response_Code=(Error::APPLICABLE_DATE_IN_PAST)
		    		response.Response_Msg=(I18n.t :APPLICABLE_DATE_IN_PAST_MSG) 
		    	return response
			end

 	    	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        	response.Response_Msg=("") 
      	return response  
	end

	def self.getServiceFee(params)
		response = Response.new

		serviceFeesMasterID = params[:serviceFeesMasterID]
		Log.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and getServiceFee method is called with serviceFeesMasterID = #{serviceFeesMasterID}")

		if !serviceFeesMasterID.present?
        		Log.logger.info("At line #{__LINE__} serviceFeesMasterID is come nil from the software")	
                response.Response_Code=(Error::INVALID_DATA)
                response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
            return response    
 		end

		serviceFeesMaster = ServiceFeesMaster.find_by_id(serviceFeesMasterID)
		if serviceFeesMaster.present?
	 			params[:serviceType] = serviceFeesMaster.service_type
	 			params[:subType] = serviceFeesMaster.sub_type
	 			params[:commPayer] = serviceFeesMaster.comm_payer
	 			params[:vatType] = serviceFeesMaster.vat_type
	 			params[:effectiveDate] = Date.parse(serviceFeesMaster.effective_date.to_s)
	 			params[:effectiveTime] = Utility.getActualTime(serviceFeesMaster.effective_date.to_s)

	 			ServiceFeesSlab.getServiceFeeSlabs(params, serviceFeesMasterID)

	 			response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
      		return response
      	else
      			Log.logger.info("At line #{__LINE__} data is not present with serviceFeesMasterID = #{serviceFeesMasterID}")	
                response.Response_Code=(Error::NO_RECORD_FOUND)
                response.Response_Msg=(I18n.t :NO_RECORD_FOUND_MSG) 
            return response    
      	end	
	end	

	def self.getSubType(serviceType, subType)
		if subType.present?
			return subType
		else
			return serviceType
		end		
	end	

	def self.getCommPayer(dbCommPayer)
		if dbCommPayer.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL
			return Constants::AGGREGATOR_COMM_PAYER_VIEW_LABEL
		elsif dbCommPayer.eql? Constants::END_USER_COMM_PAYER_DB_LABEL
			return Constants::END_USER_COMM_PAYER_VIEW_LABEL
		else
			return Constants::BOTH_COMM_PAYER_VIEW_LABEL
		end			
	end

	def self.isFormDisable(params)
		effectiveDate = Time.parse(Utility.getActualDateTime(params[:effectiveDate], params[:effectiveTime]).to_s)
		todayDate = Time.now
        if params[:effectiveDate].present? && effectiveDate <= todayDate && params[:showDetails].present?
	    	return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end
	end	

	def self.getApplicableServiceFee(serviceType, subType, txnAmount)
		response = Response.new
		AppLog.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and getApplicableServiceFee method is called with serviceType = #{serviceType}, subType = #{subType} and txnAmount = #{txnAmount}")
		
		serviceFeesMaster = ServiceFeesMaster.where("service_type = '#{serviceType}' AND sub_type = '#{subType}' AND effective_date <= '#{Time.now}'").order('effective_date ASC').last
		AppLog.logger.info("At line #{__LINE__} check that the service fee for the above parameters is exist or not = #{serviceFeesMaster.present?}")

		if serviceFeesMaster.present?
			AppLog.logger.info("At line #{__LINE__} applicable serviceFeesMasterID is = #{serviceFeesMaster.id}, applicable from is = #{serviceFeesMaster.effective_date} and commission payer is = #{serviceFeesMaster.comm_payer}")

			serviceFeesSlabs = getApplicableServiceFeeSlabs(serviceFeesMaster.id)
			if serviceFeesSlabs.present?
					response = getApplicableServiceFeeSlabsObjects(serviceFeesSlabs, txnAmount, serviceFeesMaster.comm_payer)
					response.vatType=(serviceFeesMaster.vat_type)
            	return response
			else
					response.Response_Code=(Error::SERVICE_FEE_SLABS_NOT_DEFINED)
                	response.Response_Msg=(I18n.t :SERVICE_FEE_SLABS_NOT_DEFINED_MSG) 
            	return response
			end	
		else
				response.Response_Code=(Error::SERVICE_FEE_NOT_DEFINED)
                response.Response_Msg=(I18n.t :SERVICE_FEE_NOT_DEFINED_MSG) 
            return response
		end	
	end	

	def self.getApplicableServiceFeeSlabs(serviceFeesMasterID)
		AppLog.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and getApplicableServiceFeeSlabs method is called with serviceFeesMasterID = #{serviceFeesMasterID}")
		
		serviceFeesSlabs = ServiceFeesSlab.where("master_id = #{serviceFeesMasterID}").order("sequence ASC")
		AppLog.logger.info("At line #{__LINE__} check that the service fee slabs for the above serviceFeesMasterID are exist or not = #{serviceFeesSlabs.present?}")

		if serviceFeesSlabs.present?
			return serviceFeesSlabs
		else
			return nil
		end		
	end	

	def self.getApplicableServiceFeeSlabsObjects(serviceFeesSlabs, txnAmount, commPayerValue)

		response = Response.new
		response.commPayer=(commPayerValue)

		if commPayerValue.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL	
				
			aggregatorSlabObject = retrieveApplicableSlab(serviceFeesSlabs, txnAmount, Constants::AGGREGATOR_COMM_PAYER_DB_LABEL, response)
			if aggregatorSlabObject.Response_Code != Constants::SUCCESS_RESPONSE_CODE
				return aggregatorSlabObject
			else
					response.aggregatorSlabObject=(aggregatorSlabObject)
				return response
			end	
		
		elsif commPayerValue.eql? Constants::END_USER_COMM_PAYER_DB_LABEL	
				
			endUserSlabObject = retrieveApplicableSlab(serviceFeesSlabs, txnAmount, Constants::END_USER_COMM_PAYER_DB_LABEL, response)
			if endUserSlabObject.Response_Code != Constants::SUCCESS_RESPONSE_CODE
				return endUserSlabObject
			else
					response.endUserSlabObject=(endUserSlabObject)
				return response
			end	

		else

			aggregatorSlabObject = retrieveApplicableSlab(serviceFeesSlabs, txnAmount, Constants::AGGREGATOR_COMM_PAYER_DB_LABEL, response)
			if aggregatorSlabObject.Response_Code != Constants::SUCCESS_RESPONSE_CODE
				return aggregatorSlabObject
			else
				response = Response.new
				response.commPayer=(commPayerValue)
				endUserSlabObject = retrieveApplicableSlab(serviceFeesSlabs, txnAmount, Constants::END_USER_COMM_PAYER_DB_LABEL, response)
				if endUserSlabObject.Response_Code != Constants::SUCCESS_RESPONSE_CODE
					return endUserSlabObject
				else
						response.aggregatorSlabObject=(aggregatorSlabObject)
						response.endUserSlabObject=(endUserSlabObject)
					return response
				end	
			end	
	
		end			
		
	end	

	def self.retrieveApplicableSlab(serviceFeesSlabs, txnAmount, commPayerValue, slabObject)
		
		commPayer, minAmount, maxAmount, commType, totalComm, ourComm, agentComm, aggregatorComm = Array.new, Array.new, Array.new, Array.new, Array.new, Array.new, Array.new, Array.new
		applicableCounter = -1

		serviceFeesSlabs.each do |slab|
			commPayer.push(slab.comm_payer)
			minAmount.push(slab.min_amount)
			maxAmount.push(slab.max_amount)
			commType.push(slab.comm_type)
			totalComm.push(slab.total_comm)
			ourComm.push(slab.our_comm)
			agentComm.push(slab.agent_comm)
			aggregatorComm.push(slab.aggregator_comm)
		end	
		
		for i in 0..minAmount.size.to_i - 1
			if TxnHeader.my_money(txnAmount).to_f >= TxnHeader.my_money(minAmount[i]).to_f && TxnHeader.my_money(txnAmount).to_f <= TxnHeader.my_money(maxAmount[i]).to_f and commPayerValue.eql? commPayer[i]
				applicableCounter = i
				break
			end	
		end	

		if applicableCounter == -1
				AppLog.logger.info("At line #{__LINE__} the service fee slabs is not found for the amount = #{txnAmount}")
				slabObject.Response_Code=(Error::SERVICE_FEE_AMT_SLABS_NOT_DEFINED)
    	       	slabObject.Response_Msg=(I18n.t :SERVICE_FEE_AMT_SLABS_NOT_DEFINED_MSG, :commPayer => getCommPayer(commPayerValue)) 
          	return slabObject
		else
				slabObject.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
				slabObject.Response_Msg=("")	
			return setApplicableSlabData(slabObject, applicableCounter, commPayer, commType, totalComm, ourComm, agentComm, aggregatorComm)
		end	
	end	

	def self.setApplicableSlabData(slabObject, applicableCounter, commPayer, commType, totalComm, ourComm, agentComm, aggregatorComm)
			AppLog.logger.info("At line #{__LINE__} applicable slab details are commPayer = #{commPayer[applicableCounter]}, commType = #{commType[applicableCounter]}, totalComm = #{totalComm[applicableCounter]}, ourComm = #{ourComm[applicableCounter]}, agentComm = #{agentComm[applicableCounter]} and aggregatorComm is = #{aggregatorComm[applicableCounter]}")
			slabObject.commType=(commType[applicableCounter])
			slabObject.totalComm=(totalComm[applicableCounter])
			slabObject.ourComm=(ourComm[applicableCounter])
			slabObject.agentComm=(agentComm[applicableCounter])
			slabObject.aggregatorComm=(aggregatorComm[applicableCounter])
		return slabObject	
	end	

	def self.calculateServiceFee(serviceFeesMasterResponse, txnAmount)
		
		commPayer = serviceFeesMasterResponse.commPayer
		
		if commPayer.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL

			return calculatedServiceFee(serviceFeesMasterResponse.aggregatorSlabObject, txnAmount, commPayer)

		elsif commPayer.eql? Constants::END_USER_COMM_PAYER_DB_LABEL
			
			return calculatedServiceFee(serviceFeesMasterResponse.endUserSlabObject, txnAmount, commPayer)
		
		else
			
			response = Response.new
				aggregatorResponse = calculatedServiceFee(serviceFeesMasterResponse.aggregatorSlabObject, txnAmount, commPayer)
				endUserResponse = calculatedServiceFee(serviceFeesMasterResponse.endUserSlabObject, txnAmount, commPayer)
		
				response.commPayer=(commPayer)
				response.aggregatorSlabObject=(aggregatorResponse)
				response.endUserSlabObject=(endUserResponse)
			return response
				
		end	

	end	

	def self.calculatedServiceFee(slabObject, txnAmount, commPayer)

		response = Response.new
		response.commPayer=(commPayer)

		if slabObject.commType.eql? Constants::FIXED_DB_LABEL

				response.totalComm=(slabObject.totalComm)
				response.ourComm=(slabObject.ourComm)
				response.agentComm=(slabObject.agentComm)
				response.aggregatorComm=(slabObject.aggregatorComm)
			return response	

		elsif slabObject.commType.eql? Constants::PERCENTAGE_DB_LABEL
			
				response.totalComm=(getPercentageValue(txnAmount, slabObject.totalComm))
				response.ourComm=(getPercentageValue(txnAmount, slabObject.ourComm))
				response.agentComm=(getPercentageValue(txnAmount, slabObject.agentComm))
				response.aggregatorComm=(getPercentageValue(txnAmount, slabObject.aggregatorComm))
			return response	

		end		
	
	end	

	def self.getPercentageValue(txnAmount, percentage)
		return TxnHeader.my_money(((TxnHeader.my_money(txnAmount).to_f) * (TxnHeader.my_money(percentage).to_f))/100)
	end	

	def self.calculatedServiceFeeParams(serviceType, subType, rechargeAmount)
		AppLog.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and calculatedServiceFeeParams method is called")
		
		response = Response.new
		
		serviceFeesMasterResponse = ServiceFeesMaster.getApplicableServiceFee(serviceType, subType, rechargeAmount)
        if serviceFeesMasterResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
            return serviceFeesMasterResponse
        end
        vatType = serviceFeesMasterResponse.vatType

        response = ServiceFeesMaster.calculateServiceFee(serviceFeesMasterResponse, rechargeAmount)
        commPayer = response.commPayer
        

        if commPayer.eql? Constants::BOTH_COMM_PAYER_DB_LABEL
            
	          	aggregatorObject = response.aggregatorSlabObject
	          	aggregatorTotalComm = TxnHeader.my_money(aggregatorObject.totalComm).to_f
	         	aggregatorOurComm = TxnHeader.my_money(aggregatorObject.ourComm).to_f
	         	aggregatorAgentComm = TxnHeader.my_money(aggregatorObject.agentComm).to_f
	         	aggregatorAggComm = TxnHeader.my_money(aggregatorObject.aggregatorComm).to_f
	         	AppLog.logger.info("At line #{__LINE__} aggregator commission payer details are totalComm = #{aggregatorTotalComm}, ared commission = #{aggregatorOurComm}, agent commission = #{aggregatorAgentComm}, aggregator commission = #{aggregatorAggComm}")
	         	
	         	aggregatorParamsObject = calculateCreditDebitParams(rechargeAmount, Constants::AGGREGATOR_COMM_PAYER_DB_LABEL, aggregatorTotalComm, aggregatorOurComm, aggregatorAgentComm, aggregatorAggComm, response, vatType)
	         	aggregatorParamsObject.commPayer=(commPayer)
	         	aggregatorParamsObject.totalComm=(aggregatorTotalComm)
	         	aggregatorParamsObject.ourComm=(aggregatorOurComm)
	         	aggregatorParamsObject.agentComm=(aggregatorAgentComm)
	         	aggregatorParamsObject.aggregatorComm=(aggregatorAggComm)

				endUserObject = response.endUserSlabObject
				endUserTotalComm = TxnHeader.my_money(endUserObject.totalComm).to_f
				endUserOurComm = TxnHeader.my_money(endUserObject.ourComm).to_f
				endUserAgentComm = TxnHeader.my_money(endUserObject.agentComm).to_f
				endUserAggregatorComm = TxnHeader.my_money(endUserObject.aggregatorComm).to_f
				AppLog.logger.info("At line #{__LINE__} end user commission payer details are totalComm = #{endUserTotalComm}, ared commission = #{endUserOurComm}, agent commission = #{endUserAgentComm} and aggregator commission = #{endUserAggregatorComm}")
        	
        		response = Response.new
        		endUserParamsObject = calculateCreditDebitParams(rechargeAmount, Constants::END_USER_COMM_PAYER_DB_LABEL, endUserTotalComm, endUserOurComm, endUserAgentComm, endUserAggregatorComm, response, vatType)
        		endUserParamsObject.commPayer=(commPayer)
	         	endUserParamsObject.totalComm=(endUserTotalComm)
	         	endUserParamsObject.ourComm=(endUserOurComm)
	         	endUserParamsObject.agentComm=(endUserAgentComm)
	         	endUserParamsObject.aggregatorComm=(endUserAggregatorComm)

	         	response.aggregatorSlabObject=(aggregatorParamsObject)
				response.endUserSlabObject=(endUserParamsObject)
				response.vatType=(vatType)
				response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.Response_Msg=("") 
	        
	        return response	

        else
        
	          	totalComm = TxnHeader.my_money(response.totalComm).to_f
	          	ourComm = TxnHeader.my_money(response.ourComm).to_f
	          	agentComm = TxnHeader.my_money(response.agentComm).to_f
	          	aggregatorComm = TxnHeader.my_money(response.aggregatorComm).to_f
	         	AppLog.logger.info("At line #{__LINE__} #{commPayer} commission payer details are totalComm = #{totalComm}, ared commission = #{ourComm}, agent commission = #{agentComm} and aggregatorComm = #{aggregatorComm}")

	         	calculateCreditDebitParams(rechargeAmount, commPayer, totalComm, ourComm, agentComm, aggregatorComm, response, vatType)
	         	response.commPayer=(commPayer)
	         	response.totalComm=(totalComm)
	         	response.ourComm=(ourComm)
	         	response.agentComm=(agentComm)
	         	response.aggregatorComm=(aggregatorComm)
	         	response.vatType=(vatType)
	         	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
        		response.Response_Msg=("") 
	        
	        return response
	         	
        end  

	end	

	def self.calculateCreditDebitParams(rechargeAmount, commPayer, totalComm, ourComm, agentComm, aggregatorComm, response, vatType)
		AppLog.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and calculateCreditDebitParams method is called")
		
			if commPayer.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL

		        agentDebitAmount = TxnHeader.my_money(rechargeAmount).to_f
		        stockOptCreditAmt = TxnHeader.my_money(TxnHeader.my_money(rechargeAmount).to_f - TxnHeader.my_money(totalComm).to_f).to_f
		        commOptCreditAmt = TxnHeader.my_money(totalComm).to_f

		    elsif commPayer.eql? Constants::END_USER_COMM_PAYER_DB_LABEL    

		    	agentDebitAmount = TxnHeader.my_money(TxnHeader.my_money(rechargeAmount).to_f + TxnHeader.my_money(ourComm).to_f + TxnHeader.my_money(aggregatorComm).to_f).to_f
		        stockOptCreditAmt = TxnHeader.my_money(rechargeAmount).to_f
		        commOptCreditAmt = TxnHeader.my_money(TxnHeader.my_money(ourComm).to_f + TxnHeader.my_money(aggregatorComm).to_f).to_f
		    
		    end	

		    AppLog.logger.info("At line #{__LINE__} Agent is debit with amount = #{agentDebitAmount}")
		    AppLog.logger.info("At line #{__LINE__} Operator(Stock) is credit with amount  = #{stockOptCreditAmt}")
		    AppLog.logger.info("At line #{__LINE__} Operator(Commission) is credit with amount  = #{commOptCreditAmt}")

		    aggregatorTotalCommReceive = TxnHeader.my_money(TxnHeader.my_money(ourComm).to_f + TxnHeader.my_money(agentComm).to_f).to_f
		    AppLog.logger.info("At line #{__LINE__} total commission receivable from the aggregator is = #{aggregatorTotalCommReceive}")

		    if vatType.eql? Constants::VAT_INCLUDE_DB_LABEL
			    aredVATAmount = TaxConfiguration.calculateVATOnAmount(ourComm)
			    agentVATAmount = TaxConfiguration.calculateVATOnAmount(agentComm)
			else
				aredVATAmount = 0
				agentVATAmount = 0
			end	
		    AppLog.logger.info("At line #{__LINE__} ARED VAT amount is = #{aredVATAmount}") 
		    AppLog.logger.info("At line #{__LINE__} Agent VAT amount is = #{agentVATAmount}") 

		    response.agentDebitAmount=(agentDebitAmount)
		    response.stockOptCreditAmt=(stockOptCreditAmt)
		    response.commOptCreditAmt=(commOptCreditAmt)
		    response.aredVATAmount=(aredVATAmount)
		    response.agentVATAmount=(agentVATAmount)
		    response.aggregatorTotalCommReceive=(aggregatorTotalCommReceive)

		return response    
		
	end

	def self.calculateCreditDebitParamsForBoth(rechargeAmount, aggregatorTotalComm, aggregatorOurComm, aggregatorAgentComm, aggregatorAggComm, endUserTotalComm, endUserOurComm, endUserAgentComm, endUserAggregatorComm, vatType)

		response = Response.new

			agentDebitAmount = TxnHeader.my_money(TxnHeader.my_money(rechargeAmount).to_f + TxnHeader.my_money(endUserOurComm).to_f + TxnHeader.my_money(endUserAggregatorComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} Agent is debit with amount = #{agentDebitAmount}")

	        stockOptCreditAmt = TxnHeader.my_money(TxnHeader.my_money(rechargeAmount).to_f - TxnHeader.my_money(aggregatorTotalComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} Operator(Stock) is credit with amount  = #{stockOptCreditAmt}")        

	        commOptCreditAmt = TxnHeader.my_money(TxnHeader.my_money(aggregatorTotalComm).to_f + TxnHeader.my_money(endUserOurComm).to_f + TxnHeader.my_money(endUserAggregatorComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} Operator(Commission) is credit with amount  = #{commOptCreditAmt}") 

	        totalComm = TxnHeader.my_money(TxnHeader.my_money(aggregatorTotalComm).to_f + TxnHeader.my_money(endUserTotalComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} total commission is = #{totalComm}") 

	        ourComm = TxnHeader.my_money(TxnHeader.my_money(aggregatorOurComm).to_f + TxnHeader.my_money(endUserOurComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} ared commission is = #{ourComm}") 

	        agentComm = TxnHeader.my_money(TxnHeader.my_money(aggregatorAgentComm).to_f + TxnHeader.my_money(endUserAgentComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} agent commission is = #{agentComm}") 

	        aggregatorComm = TxnHeader.my_money(TxnHeader.my_money(aggregatorAggComm).to_f + TxnHeader.my_money(endUserAggregatorComm).to_f).to_f
	        AppLog.logger.info("At line #{__LINE__} aggregator commission is = #{aggregatorComm}")

	        if vatType.eql? Constants::VAT_INCLUDE_DB_LABEL
	        	aredVATAmount = TaxConfiguration.calculateVATOnAmount(endUserOurComm)
	        	agentVATAmount = TaxConfiguration.calculateVATOnAmount(endUserAgentComm)
	        else
	        	aredVATAmount = 0
	        	agentVATAmount = 0
	        end	
		    AppLog.logger.info("At line #{__LINE__} ARED VAT amount is = #{aredVATAmount}") 
		    AppLog.logger.info("At line #{__LINE__} Agent VAT amount is = #{agentVATAmount}")  

	        response.totalComm=(totalComm)
	        response.ourComm=(ourComm)
	        response.agentComm=(agentComm)
	        response.aggregatorComm=(aggregatorComm)
	        response.agentDebitAmount=(agentDebitAmount)
		    response.stockOptCreditAmt=(stockOptCreditAmt)
		    response.commOptCreditAmt=(commOptCreditAmt)
		    response.aredVATAmount=(aredVATAmount)
		    response.agentVATAmount=(agentVATAmount)
		    response.aggregatorTotalCommReceive=(TxnHeader.my_money(TxnHeader.my_money(aggregatorOurComm).to_f + TxnHeader.my_money(aggregatorAgentComm).to_f).to_f)

		return response   
	
	end	

	def self.isAgentCommCredit(commissionPayer)
		if commissionPayer.eql? Constants::AGGREGATOR_COMM_PAYER_DB_LABEL
			return Constants::BOOLEAN_TRUE_LABEL
		else	
			return Constants::BOOLEAN_FALSE_LABEL
		end
	end	

	def self.getServiceFeeCharges(currentAgent, params)
		response = Response.new
		
			serviceType = params[""+AppConstants::SERVICE_TYPE_PARAM_LABEL+""]
	        operator = params[""+AppConstants::OPERATOR_PARAM_LABEL+""]
	        rechargeAmount = params[""+AppConstants::AMOUNT_PARAM_LABEL+""]
	        AppLog.logger.info("At line #{__LINE__} inside the ServiceFeesMaster class and getServiceFeeCharges method is called with serviceType = #{serviceType}, operator = #{operator} and rechargeAmount = #{rechargeAmount}")

	        unless currentAgent.present?
	                AppLog.logger.info("At line #{__LINE__} and agent is not present")
	                response.Response_Code=(Error::MICRO_FRANCHISEE_NOT_EXIST)
	                response.Response_Msg=(I18n.t :MICRO_FRANCHISEE_NOT_EXIST_MSG) 
	            return response
	        end
	        AppLog.logger.info("At line #{__LINE__} and this request is done by the agent ID = #{currentAgent.id}, agent Phone = #{currentAgent.phone} and name is = #{Utility.getAgentFullName(currentAgent)}")

	        if !serviceType.present? || !operator.present? || !rechargeAmount.present?
		            AppLog.logger.info("At line #{__LINE__} and some data is come nil from the app")
		            response.Response_Code=(Error::INVALID_DATA)
		            response.Response_Msg=(I18n.t :INVALID_DATA_MSG) 
		        return response
			end

			calculatedResponse = ServiceFeesMaster.calculatedServiceFeeParams(serviceType, operator, rechargeAmount)
	        if calculatedResponse.Response_Code != Constants::SUCCESS_RESPONSE_CODE
	            return calculatedResponse
	       	end
	        
	        commissionPayer = calculatedResponse.commPayer
	        if commissionPayer.eql? Constants::BOTH_COMM_PAYER_DB_LABEL
		        endUserParamsObject = calculatedResponse.endUserSlabObject
	    	    totalCommAmount = TxnHeader.my_money(endUserParamsObject.totalComm).to_f
	        elsif commissionPayer.eql? Constants::END_USER_COMM_PAYER_DB_LABEL
	          	totalCommAmount = TxnHeader.my_money(calculatedResponse.totalComm).to_f
	        else
	        	totalCommAmount = 0
	        end  
        		
    		response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
    		response.Response_Msg=("") 
        	response.applicableCharges=(totalCommAmount)
        return response
	end	

end