class ClientCommand
  include ActiveModel::Validations

  VALID_COMMANDS = %w(
    toggle_wifi
    reboot
    wipe
  )

  def initialize(command)
    self.command = command
  end

  attr_accessor :command

  validates :command, inclusion: { in: VALID_COMMANDS }

end
