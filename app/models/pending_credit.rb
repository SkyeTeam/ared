class PendingCredit < ApplicationRecord

	def self.getPendingCredits

		Log.logger.info("Inside the Pending Credit's class getPendingCredits at line = #{__LINE__}")

		query = "SELECT c.agent_id, u.name, u.last_name, u.phone, u.tin_no, c.id, c.amount, c.txn_date AT TIME ZONE '#{Properties::TIME_ZONE}' AS txn_date, c.status, c.app_unique_id FROM pending_credits c, users u WHERE c.agent_id = u.id and c.status = #{Constants::INITIATE_TXN_CODE} ORDER BY txn_date desc"

		queryResult = Utility.executeSQLQuery(query)

		if queryResult.present?
			return queryResult
		else
			return nil
		end		
	end

	def self.confrimApproveRejectCredit(params, loggedInAdmin, request) 
		
		userAction = params[:userAction]
		agentID = params[:agentID]
		txnID = params[:txnID]
		approvedAmount = params[:amount]
		appTxnUniqueID = params[:appTxnUniqueID]
		txnUniqueID = Utility.getRandomNumber(15)

		Log.logger.info("Inside the PendingCredit class at line = #{__LINE__}, userAction is = #{userAction}, txnID is = #{txnID}, agentID is = #{agentID}, appTxnUniqueID = #{appTxnUniqueID} and requested amount is = #{approvedAmount}")

		response = Response.new

		isCreditProccessed = checkCreditProccessed(txnID)

		if isCreditProccessed
				response.Response_Code=(Error::CREDIT_ALREADY_PROCESSED)
		        response.Response_Msg=(I18n.t :CREDIT_ALREADY_PROCESSED_MSG)
		    return response  
		end	

  	    #First we add one entry in the pending request table before processing
        AppUtility.savePendingRequest(txnUniqueID, Constants::USER_LOAN_REQUEST_LABLE, Constants::USER_LOAN_REQUEST_LABLE, nil, approvedAmount, agentID, Utility.getUserRemoteIP(request), loggedInAdmin.id, Constants::ADMIN_USER_ROLE)
        Log.logger.info("At line = #{__LINE__} and add one entry in the pending request table before processing successfully")

			ActiveRecord::Base.transaction do

				currentAgent = Utility.getCurrentAgent(agentID)

		        if userAction.to_i == Constants::APPROVE_TXN_USER_ACTION_VALUE.to_i

		          	#This will update the status of that transaction that was requested for the credit
		          	updateReqCreditTxnStatus(txnID,true)

		          	creditFee = CreditFeeConfig.getCreditFee(CreditFeeConfig.getCreditConfig(approvedAmount, currentAgent.id))
	    			AppLog.logger.info("At line = #{__LINE__} and the credit fee is = #{creditFee} on the credit amount = #{approvedAmount}")

		          	txnheader = CreditManager.getHeader(approvedAmount)
		          	txnheader.agent_id= currentAgent.id
		          	txnheader.save
		          	AppLog.logger.info("At line = #{__LINE__} and data saved into the txn header table with header ID = #{txnheader.id}.")

		          	agentTxnItem  = CreditManager.getTxnItem(currentAgent, approvedAmount, false, Utility.getCurrentAgentAccount(currentAgent))
		          	agentTxnItem.header_id = txnheader.id
		          	agentTxnItem.save
		          	AppLog.logger.info("At line = #{__LINE__} and agent txn item data saved into the txn_item table.")

		          	optTxnItem = CreditManager.getTxnItem(Utility.getStockAdmin(), approvedAmount, true, Utility.getStockAdminAccount(Utility.getStockAdmin()))
		          	optTxnItem.header_id = txnheader.id;
		          	optTxnItem.save
		          	AppLog.logger.info("At line = #{__LINE__} and operator's stock txn item data saved into the txn_item table.")

		          	loanRecord = CreditManager.getLoanRecord(currentAgent, approvedAmount, creditFee)
		          	loanRecord.save
		          	AppLog.logger.info("At line = #{__LINE__} and agent loan details is saved into the loan table.")

		          	TxnManager.update_account(Utility.getStockAdminAccount(Utility.getStockAdmin()), approvedAmount, true);
		          	TxnManager.update_account(Utility.getCurrentAgentAccount(currentAgent), approvedAmount, false);
		          	AppLog.logger.info("At line = #{__LINE__} and balances of the agent and stock admin accounts are updated successfully.")

		          	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		          	response.Response_Msg=(I18n.t :MF_LOAN_APPROVE_MSG, :approvedAmount => Utility.getFormatedAmount(approvedAmount)) 

	  	        	AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
		            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{appTxnUniqueID}")

	  	        	AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::SUCCESS_RESPONSE_CODE)
		            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 200 of this transaction id #{txnUniqueID}")

		          	#Here we send the notification to the agent
	   				Utility.sendNotification(currentAgent.id, (I18n.t :MF_LOAN_CREDIT_MSG, :approvedAmount => Utility.getFormatedAmount(approvedAmount)), Constants::CASH_DEPOSIT_NOTIFICATION_TITLE)

		        else
		          
		          	#This will update the status of that transaction that was requested for the credit
		          	updateReqCreditTxnStatus(txnID,false)

		          	AppUtility.updatePendingRequestStatus(appTxnUniqueID, Constants::REJECT_TXN_CODE)
		            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of this transaction id #{appTxnUniqueID}")

					AppUtility.updatePendingRequestStatus(txnUniqueID, Constants::REJECT_TXN_CODE)
		            AppLog.logger.info("At line #{__LINE__} and successfuly update the status to 150 of this transaction id #{txnUniqueID}")

		         	response.Response_Code=(Constants::SUCCESS_RESPONSE_CODE)
		          	response.Response_Msg=(I18n.t :TXN_SUCCESS_REJECTED_MSG)
				
				   	#Here we send the notification to the agent
	   				Utility.sendNotification(currentAgent.id, (I18n.t :MF_LOAN_REJECT_MSG, :name=> (currentAgent.name.to_s + " " + currentAgent.last_name), :amount => Utility.getFormatedAmount(approvedAmount)), Constants::CASH_DEPOSIT_NOTIFICATION_TITLE)

		        end 

	      	end

	    return response  	

	end	

	def self.updateReqCreditTxnStatus(txnID,isApprove)
		pendingCredit = PendingCredit.find(txnID)
		if isApprove
			pendingCredit.update_attributes(:status => Constants::SUCCESS_RESPONSE_CODE, :approval_date => Time.now)
		else
			pendingCredit.update_attributes(:status => Constants::REJECT_TXN_CODE, :approval_date => Time.now)
		end	
	end	

	def self.checkCreditProccessed(txnID)
		pendingCredit = PendingCredit.find(txnID)
		if pendingCredit.status == Constants::SUCCESS_RESPONSE_CODE || pendingCredit.status == Constants::REJECT_TXN_CODE
			return Constants::BOOLEAN_TRUE_LABEL
		else
			return Constants::BOOLEAN_FALSE_LABEL
		end	
	end	
	

	def self.getAgentTotalSell(currentAgent)
			totalSell = TxnHeader.where(agent_id: currentAgent.id).where(service_type: [Constants::FDI_DTH_LABEL, Constants::FDI_ELECTRICITY_LABEL,Constants::MTOPUP_LABEL]).where.not(is_rollback: Constants::TXN_APPROVED_ROLLBACK_STATUS).sum(:amount)
		return 	totalSell
	end	

	def self.getAgentAvgSell(totalSell, totaldays)
			if totaldays != 0
				avgSell = TxnHeader.my_money(totalSell/totaldays).to_f
			else
				avgSell = totalSell
			end	
		return avgSell	
	end	


end
