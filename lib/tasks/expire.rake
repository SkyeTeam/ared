namespace :expire do 

	task challanges: :environment do
		Log.logger.info("Scheduler's Agent credit Limt function is called at line = #{__LINE__}")
		AgentCreditLimit.expirechallenge
	end

	task loan_notify: :environment do
		Log.logger.info("Scheduler's Loan notification function is called at line = #{__LINE__}")
		Loan.loannotification
	end

	task loan_unpaid: :environment do
		Log.logger.info("Scheduler's Loan Un paid function is called at line = #{__LINE__}")
		Loan.loanunpaidnotification
	end

	task demo: :environment do
		Log.logger.info("**********Development Scheduler is called *********")
	end

	task demo_prod: :environment do
		Log.logger.info("<<<<<<<<<<<<Updated Production Scheduler is called at time = #{Time.now}")
	end
	
	task demo_prod_test: :environment do
		Log.logger.info("New Production Scheduler is called at time = #{Time.now}")
	end

end