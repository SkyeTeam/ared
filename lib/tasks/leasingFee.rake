namespace :leasingFee do

	task deductLeasingFee: :environment do
		Log.logger.info("Scheduler's of deduct Leasing Fee is called at line = #{__LINE__} on time = #{Time.now}")
		LeasingFee.deductLeasingFee
	end 

	task deductPendingLeasingFee: :environment do
		Log.logger.info("Scheduler's of deduct Pending Leasing Fee is called at line = #{__LINE__} on time = #{Time.now}")
		LeasingFee.deductPendingLeasingFee
	end

end	