namespace :vat do

	task deductAgentsVAT: :environment do
		Log.logger.info("At line #{__LINE__} and Scheduler's of deduct Agents VAT is called on time = #{Time.now}")
		VatManager.deductAgentsVAT
	end 

	task deductAREDVAT: :environment do
		Log.logger.info("At line #{__LINE__} and Scheduler's of deduct ARED VAT is called on time = #{Time.now}")
		VatManager.deductAREDVAT
	end

end