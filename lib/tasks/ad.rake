namespace :ad do

  desc "Activate all ads which should start 'today'"
  task :activate_by_startdate => :environment do
    Ad.where("start_date <= ? and start_date IS NOT NULL", DateTime.now).update_all(active: true)
  end

  desc "Deactivate all ads which should end 'today'"
  task :deactivate_by_enddate => :environment do
    Ad.where("end_date <= ? and end_date IS NOT NULL", DateTime.now).update_all(active: false)
  end

end
