namespace :survey do

  desc "Activate all surveys which should start 'today'"
  task :activate_by_startdate => :environment do
    Survey.where("start_date <= ? and start_date IS NOT NULL", DateTime.now).update_all(active: true)
  end

  desc "Deactivate all surveys which should end 'today'"
  task :deactivate_by_enddate => :environment do
    Survey.where("end_date <= ? and end_date IS NOT NULL", DateTime.now).update_all(active: false)
  end

end
