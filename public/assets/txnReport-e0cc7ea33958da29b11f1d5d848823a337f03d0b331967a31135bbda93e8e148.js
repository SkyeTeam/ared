$(document).ready(function(){

    // these all varaiables are defined in the header file
    if(txnType == airtimeLabel){
        showMtopUpForm();
    } else if(txnType == taxReportLabel){
        taxReportForm();
    } else if(txnType == commPayReportLabel){
        commPayReportForm();
    } else if(txnType == penaltyReportLabel){
        penaltyReportForm();
    } else if(txnType == pandLreportForm){
        pandLForm();
    } else{
        otherForm();
    }

    managerUserPhoneDiv(whichUser);
	
	$("#serviceSelected").change(function(e){
        var serviceSelected = $(this).val();

        if(serviceSelected == airtimeLabel){
            showMtopUpForm();
        } else if(serviceSelected == taxReportLabel){
            taxReportForm();
        } else if(serviceSelected == commPayReportLabel){
            commPayReportForm();
        } else if(serviceSelected == penaltyReportLabel){
            penaltyReportForm();
        } else if(serviceSelected == pandLreportForm){
            pandLForm();
        } else{
            otherForm();
        }
    });

    $("#txnReportUserType").change(function(e){
        var userType = $(this).val();    
        managerUserPhoneDiv(userType);
    });

});

function validateTxnReportForm(){
    if(!formValidation('TxnReportForm'))return false;
}

function validateOtherForm(formID){
    if(!formValidation(formID))return false;
}

function managerUserPhoneDiv(userType){
    if( userType == singleUserType ){
        $('input[name=user_phone]').prop('required', 'true');
        $('input[name=user_phone]').removeAttr('disabled');
    } else {
        $('input[name=user_phone]').removeAttr('required');
        $('input[name=user_phone]').prop('disabled', 'true');
    }
}

function showMtopUpForm(){
    $("#previousForm").show();
    $("#operatorList").removeAttr('disabled');
    $("#taxReportFrom").hide();
    $("#commissionPaymentReportForm").hide();
    $("#penaltyReportForm").hide();
    $("#profitAndLossReportForm").hide();
}

function taxReportForm(){
    $("#previousForm").hide();
    $("#operatorList").prop('disabled', 'true');
    $("#taxReportFrom").show();
    $("#commissionPaymentReportForm").hide();
    $("#penaltyReportForm").hide();
    $("#profitAndLossReportForm").hide();  
}

function commPayReportForm(){
    $("#previousForm").hide();
    $("#operatorList").prop('disabled', 'true');
    $("#taxReportFrom").hide();
    $("#commissionPaymentReportForm").show();
    $("#penaltyReportForm").hide();
    $("#profitAndLossReportForm").hide();
}

function penaltyReportForm(){
    $("#previousForm").hide();
    $("#operatorList").prop('disabled', 'true');
    $("#taxReportFrom").hide();
    $("#commissionPaymentReportForm").hide();
    $("#penaltyReportForm").show();
    $("#profitAndLossReportForm").hide(); 
}

function pandLForm(){
    $("#previousForm").hide();
    $("#operatorList").prop('disabled', 'true');
    $("#taxReportFrom").hide();
    $("#commissionPaymentReportForm").hide();
    $("#penaltyReportForm").hide();
    $("#profitAndLossReportForm").show();
}

function otherForm(){
    $("#previousForm").show();
    $("#operatorList").prop('disabled', 'true');
    $("#taxReportFrom").hide();
    $("#commissionPaymentReportForm").hide();
    $("#penaltyReportForm").hide();
    $("#profitAndLossReportForm").hide();
}
;
