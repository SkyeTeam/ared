$(document).ready(function(){
		
		$('.number').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode
			if (keyValue == 8 || keyValue == 37 || keyValue == 39 || keyValue == 9 || keyValue == 118){
				return true;
			}
			else if(keyValue >= 48 && keyValue <= 57){
				return true;
			}
			else{
				return false;
			}
		});
		
		$('.number').focusout(function(e){
			var dest = $(this);
			dest.val($.trim(dest.val()));
			checkNumberInput(dest,'number');
		});
		
		$('.decimal').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode;
			if (keyValue == 8 || keyValue == 37 || keyValue == 39 || keyValue == 9 || keyValue == 118){
				return true;
			}
			else if(keyValue == 46 && $(this).val().indexOf('.') == -1){ // THis is used to enable the one decimal point in the text field
				return true;
			}
			else if(keyValue >= 48 && keyValue <= 57){
				return true;
			}
			else{
				return false;
			}
		});
		
		$('.decimal').focusout(function(e){
			var dest = $(this);
			dest.val($.trim(dest.val()));
			checkNumberInput(dest,'decimal');
		});
		
		$('.alpha').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode;
			if (keyValue == 8 || keyValue == 37 || keyValue == 39 || keyValue == 9 || keyValue == 46 || keyValue == 32){
				return true;
			} else if(keyValue >= 65 && keyValue <= 90){
				return true;
			} else if(keyValue >= 97 && keyValue <= 122){
				return true;
			} else {
				return false;
			}
		});
		
		$('.text').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode
			if (keyValue == 8 || keyValue == 127 || keyValue == 46|| keyValue == 37 || keyValue == 39 || keyValue == 9){
				return true;
			} else if(keyValue >= 48 || keyValue <= 57){
				return true;
			} else if(keyValue >=65 || keyValue <=90){
				return true;
			} else if(keyValue >= 97 || keyValue<=122){
				return true;
			} else {
				return false;
			}
		});
	
		//*********To trim the spaces in the text field
		$('.text').focusout(function(e){
			var dest = $(this);
	        dest.val($.trim(dest.val())); 
		});		
});

function checkNumberInput(dest,classType) {
    dest.val($.trim(dest.val())); 
	var inputValue = dest.val();
	var integerArray = [0,1,2,3,4,5,6,7,8,9];
	var decimalArray = [0,1,2,3,4,5,6,7,8,9,DECIMAL_SEPARATOR];
	var pickedChars;

	if(classType=="number"){
		pickedChars=integerArray;
	} else {
		pickedChars=decimalArray;
	}

	for(var i=0;i<inputValue.length; i++){
		var charMatched = false;
		for(var j=0; j<pickedChars.length; j++){
			if(inputValue.charAt(i)==pickedChars[j]){
				charMatched = true;
				break;
			}
		}
		if(charMatched==false){
			dest.val("");
			return false;
		}
	}
}

function getErrorAlert(error) {
	swal({
		title : "Error!",
		text : error,
		type : "error",
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : "OK",
		allowOutsideClick: false
	});
}

function getWarningAlert(warning) {
	swal({
		title : "Warning!",
		text : warning,
		type : "warning",
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : "OK" ,
		allowOutsideClick: false
	});
}


function getSuccessAlert(text) {
	swal({
		title : 'Done',
		text : text,
		type : 'success',
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : 'OK',
		allowOutsideClick: false
	})
}

function getSuccessForwardAlert(text) {
	swal({
		title : 'Done',
		text : text,
		type : 'success',
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : 'OK',
		allowOutsideClick: false
	}).then(function () {
		window.location.href = ctxPath;
	})
}

function getConfirmAlert(confirmMes,confirmCallBackFunction){
	swal({
		  text: confirmMes,
		  type: 'warning',
		  animation : false,
		  customClass : 'swal',
		  showCancelButton: true,
		  confirmButtonColor: '#1ABB9C',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No',
		  allowOutsideClick: false
		}).then(function () {
			confirmCallBackFunction(true);
		})
}

function formValidation(formName) {
	var isValid = true;

	$("#" + formName + " input[type=text]").each(function() {
		if ($.trim($(this).val()) == '') {
			isValid = false;
			this.style.setProperty('background-color', '#ffd2e4', 'important');
		} else {
			$(this).css('background-color', 'white');
		}
	});

	$("#" + formName + " input[type=password]").each(function() {
		if ($.trim($(this).val()) == '') {
			isValid = false;
			this.style.setProperty('background-color', '#ffd2e4', 'important');
		} else {
			$(this).css('background-color', 'white');
		}
	});

	$("#" + formName + " input[type=date]").each(function() {
		if ($.trim($(this).val()) == '') {
			isValid = false;
			this.style.setProperty('background-color', '#ffd2e4', 'important');
		} else {
			$(this).css('background-color', 'white');
		}
	});

	$("#" + formName + " input[type=radio]").each(function() {
		if ($.trim($(this).val()) == '') {
			isValid = false;
			this.style.setProperty('background-color', '#ffd2e4', 'important');
		} else {
			$(this).css('background-color', 'white');
		}
	});

	// This is used to check all the select tag inside the one particular form
	$("#" + formName + " select").each(function() {
		if ($.trim($(this).val()) == '') {
			isValid = false;
			this.style.setProperty('background-color', '#ffd2e4', 'important');
		} else {
			$(this).css('background-color', 'white');
		}
	});

	$("#" + formName + " textarea").each(function() {
		if ($.trim($(this).val()) == '') {
			isValid = false;
			this.style.setProperty('background-color', '#ffd2e4', 'important');
		} else {
			$(this).css('background-color', 'white');
		}
	});
	
	
	if (!isValid){
		getWarningAlert("Please provide valid data in the highlighted field(s).");
	 }
	return isValid;
}
;
