import AreaHelper from 'area_helper.es6';
import SurveyHelper from 'survey_helper.es6';

// initialize all the js
$(document).ready( function() {
  // area tree helper
  $('.js-new-area').each(function() {
    const areaHelper = new AreaHelper();
    areaHelper.initialize($(this));
  });

  // survey helper
  if($('.js-survey').length > 0) {
    const surveyHelper = new SurveyHelper();
   surveyHelper.initialize($('.js-survey'));
  }

});
