$(document).ready(function(){
    
  	var reportTypeValue = $("select[name=reportType]").val();
  	manageForms(reportTypeValue);
  
  	var serviceTypeValue = $("select[name=serviceTypeCommReport]").val();
  	manageSubType(serviceTypeValue);

    var userTypeCommReport = $("select[name=userTypeCommReport]").val();
    managerCommFormPhoneDiv(userTypeCommReport);

    var creditReportFormUType = $("select[name=userType]").val();
    managerCreditFormPhoneDiv(creditReportFormUType);

    var serviceTypeValueRev = $("select[name=serviceTypeRevReport]").val();
    manageSubTypeRev(serviceTypeValueRev);


    $("select[name=reportType]").change(function() {
    	manageForms($(this).val());
    });

    $("select[name=serviceTypeCommReport]").change(function() {
    	manageSubType($(this).val());
    });

    $("select[name=userTypeCommReport]").change(function() {
    	managerCommFormPhoneDiv($(this).val());
    });

    $("select[name=userType]").change(function() {
      managerCreditFormPhoneDiv($(this).val());
    });

    $("select[name=serviceTypeRevReport]").change(function() {
      manageSubTypeRev($(this).val());
    });
    
});



function manageForms(reportType){

	if (reportType == revenueReportLabel) { //This  variable(revenueReportLabel) defined in the layouts/header file
	      	
      	$("#revenueReportForm").show();
      	$("#commReportForm").hide();
       	$("#creditReportForm").hide();
    
    } else if(reportType == creditReportLabel) { //This  variable(creditReportLabel) defined in the layouts/header file
      	
      	$("#revenueReportForm").hide();
       	$("#creditReportForm").show();
        $("#commReportForm").hide();

    } else{
      	
      	$("#revenueReportForm").hide();
       	$("#commReportForm").show();
        $("#creditReportForm").hide();

    }

}

function managerCreditFormPhoneDiv(userType){

	if(userType == singleUserType){ //This is used for the Single User and variable(singleUserType) defined in the layouts/header file
         
      $("#phoneDiv").show();
      $('input[name=phoneCreditReport]').prop('required', 'true');
    
    } else {
    
      $("#phoneDiv").hide();
      $('input[name=phoneCreditReport]').removeAttr('required');
    
    }
}

function managerCommFormPhoneDiv(userType){
	
	if(userType == singleUserType){ //This is used for the Single User and variable(singleUserType) defined in the layouts/header file
         
      $("#phoneDivCommReport").show();
      $('input[name=phoneCommReport]').prop('required', 'true');
      $('#viewDiv').css("margin-top", "-10px");
    
    } else {
    
      $("#phoneDivCommReport").hide();
      $('input[name=phoneCommReport]').removeAttr('required');
       $('#viewDiv').css("margin-top", "0px");
    
    }
}


function manageSubType(serviceType){
  if (serviceType == airtimeLabel ){//This  variable(airtimeLabel) defined in the layouts/header file
    $("#subTypeDiv").show();
	} else {
		$("#subTypeDiv").hide();
	}
}

function manageSubTypeRev(serviceType){
  if (serviceType == airtimeLabel ){//This  variable(airtimeLabel) defined in the layouts/header file
    $("#subTypeRevDiv").show();
  } else {
    $("#subTypeRevDiv").hide();
  }
}


function validateForm(formID){
	if(!formValidation(formID)) return false;
}

;
