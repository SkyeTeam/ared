Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

/* global $ */

var SurveyHelper = (function () {
  function SurveyHelper() {
    _classCallCheck(this, SurveyHelper);
  }

  _createClass(SurveyHelper, [{
    key: 'initialize',

    // take care of new questions and answers
    value: function initialize($el) {
      this.$el = $el;

      // elements
      this.newQuestionTrigger = this.$el.find('.js-new-question');

      // listener
      this.newQuestionTrigger.on('click', this.observeNewQuestion.bind(this));
      this.$el.on('click', '.js-new-answer', this.insertNewAnswer);
    }
  }, {
    key: 'observeNewQuestion',
    value: function observeNewQuestion() {
      $.post('/surveys/new_question', { index: $('.js-question-card').length }).done(function (data) {
        $('.js-questions').append(data.html);
      }).error(function () {
        alert('Sorry - Something went wrong!');
      });
    }
  }, {
    key: 'insertNewAnswer',
    value: function insertNewAnswer() {
      var index = parseInt($(this).data('question-index'), 10);
      var answers = $('.js-answers').eq(index);

      $.post('/surveys/new_answer', { index: index, answersCount: answers.find('input').length }).done(function (data) {
        answers.append(data.html);
      }).error(function () {
        alert('Sorry - Something went wrong!');
      });
    }
  }]);

  return SurveyHelper;
})();

exports['default'] = SurveyHelper;
module.exports = exports['default'];
