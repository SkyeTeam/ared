(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _area_helperEs6 = require('area_helper.es6');

var _area_helperEs62 = _interopRequireDefault(_area_helperEs6);

var _survey_helperEs6 = require('survey_helper.es6');

var _survey_helperEs62 = _interopRequireDefault(_survey_helperEs6);

// initialize all the js
$(document).ready(function () {
  // area tree helper
  $('.js-new-area').each(function () {
    var areaHelper = new _area_helperEs62['default']();
    areaHelper.initialize($(this));
  });

  // survey helper
  if ($('.js-survey').length > 0) {
    var surveyHelper = new _survey_helperEs62['default']();
    surveyHelper.initialize($('.js-survey'));
  }
});

},{"area_helper.es6":2,"survey_helper.es6":3}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AreaHelper = function () {
  function AreaHelper() {
    _classCallCheck(this, AreaHelper);
  }

  _createClass(AreaHelper, [{
    key: 'initialize',
    value: function initialize($el) {
      this.$el = $el;

      this.$el.on('click', this.observeClick.bind(this));
    }
  }, {
    key: 'observeClick',
    value: function observeClick() {
      var _this = this;

      this.$el.fadeOut('slow', function () {
        _this.$el.next().removeClass('hidden-xs-up');
      });
    }
  }]);

  return AreaHelper;
}();

exports.default = AreaHelper;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SurveyHelper = function () {
  function SurveyHelper() {
    _classCallCheck(this, SurveyHelper);
  }

  _createClass(SurveyHelper, [{
    key: 'initialize',
    value: function initialize($el) {
      this.$el = $el;

      this.newQuestionTrigger = this.$el.find('.js-new-question');

      this.newQuestionTrigger.on('click', this.observeNewQuestion.bind(this));
      this.$el.on('click', '.js-new-answer', this.insertNewAnswer);
    }
  }, {
    key: 'observeNewQuestion',
    value: function observeNewQuestion() {
      $.post('/surveys/new_question', { index: $('.js-question-card').length }).done(function (data) {
        $('.js-questions').append(data.html);
      }).error(function () {
        alert('Sorry - Something went wrong!');
      });
    }
  }, {
    key: 'insertNewAnswer',
    value: function insertNewAnswer() {
      var index = parseInt($(this).data('question-index'), 10);
      var answers = $('.js-answers').eq(index);

      $.post('/surveys/new_answer', { index: index, answersCount: answers.find('input').length }).done(function (data) {
        answers.append(data.html);
      }).error(function () {
        alert('Sorry - Something went wrong!');
      });
    }
  }]);

  return SurveyHelper;
}();

exports.default = SurveyHelper;

},{}]},{},[1]);
