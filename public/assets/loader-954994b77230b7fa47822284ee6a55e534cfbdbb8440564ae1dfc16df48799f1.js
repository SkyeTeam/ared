function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _area_helperEs6 = require('area_helper.es6');

var _area_helperEs62 = _interopRequireDefault(_area_helperEs6);

var _survey_helperEs6 = require('survey_helper.es6');

var _survey_helperEs62 = _interopRequireDefault(_survey_helperEs6);

// initialize all the js
$(document).ready(function () {
  // area tree helper
  $('.js-new-area').each(function () {
    var areaHelper = new _area_helperEs62['default']();
    areaHelper.initialize($(this));
  });

  // survey helper
  if ($('.js-survey').length > 0) {
    var surveyHelper = new _survey_helperEs62['default']();
    surveyHelper.initialize($('.js-survey'));
  }
});
