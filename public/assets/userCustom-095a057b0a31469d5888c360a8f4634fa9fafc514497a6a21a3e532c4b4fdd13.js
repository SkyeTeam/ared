var DECIMAL_SEPARATOR = '.';

$(document).ready(function(){
		
		$('.number').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode
			if (keyValue == 8 || keyValue == 37 || keyValue == 39 || keyValue == 9 || keyValue == 118){
				return true;
			}
			else if(keyValue >= 48 && keyValue <= 57){
				return true;
			}
			else{
				return false;
			}
		});
		
		$('.number').focusout(function(e){
			var dest = $(this);
			dest.val($.trim(dest.val()));
			checkNumberInput(dest,'number');
		});
		
		$('.decimal').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode;
			if (keyValue == 8 || keyValue == 37 || keyValue == 39 || keyValue == 9 || keyValue == 118){
				return true;
			}
			else if(keyValue == 46 && $(this).val().indexOf('.') == -1){ // THis is used to enable the one decimal point in the text field
				return true;
			}
			else if(keyValue >= 48 && keyValue <= 57){
				return true;
			}
			else{
				return false;
			}
		});
		
		$('.decimal').focusout(function(e){
			var dest = $(this);
			dest.val($.trim(dest.val()));
			checkNumberInput(dest,'decimal');
		});
		
		$('.alpha').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode;
			//46 is used for the delete
			if (keyValue == 8 || keyValue == 37 || keyValue == 46 || keyValue == 9 || keyValue == 32){
				return true;
			} else if(keyValue >= 65 && keyValue <= 90){
				return true;
			} else if(keyValue >= 97 && keyValue <= 122){
				return true;
			} else {
				return false;
			}
		});

		//*********To trim the spaces in the text field
		$('.alpha').focusout(function(e){
			var dest = $(this);
	        dest.val($.trim(dest.val())); 
		});	

		
		$('.text').keypress(function(key) {
			var keyValue=key.charCode? key.charCode : key.keyCode
			if (keyValue == 8 || keyValue == 127 || keyValue == 46|| keyValue == 37 || keyValue == 39 || keyValue == 9){
				return true;
			} else if(keyValue >= 48 || keyValue <= 57){
				return true;
			} else if(keyValue >=65 || keyValue <=90){
				return true;
			} else if(keyValue >= 97 || keyValue<=122){
				return true;
			} else {
				return false;
			}
		});

		$('.readonly').prop("readonly", true);
		$('.readonly').css("cursor", "not-allowed");
	
		//*********To trim the spaces in the text field
		$('.text').focusout(function(e){
			alert(1);
			var dest = $(this);
	        dest.val($.trim(dest.val())); 
		});		
});

function parseFloatJS(value){
	var stringValue = Number(value).toFixed(2);
	return parseFloat(stringValue);
}

function checkNumberInput(dest,classType) {
    dest.val($.trim(dest.val())); 
	var inputValue = dest.val();
	var integerArray = [0,1,2,3,4,5,6,7,8,9];
	var decimalArray = [0,1,2,3,4,5,6,7,8,9,DECIMAL_SEPARATOR];
	var pickedChars;

	if(classType=="number"){
		pickedChars=integerArray;
	} else {
		pickedChars=decimalArray;
	}

	for(var i=0;i<inputValue.length; i++){
		var charMatched = false;
		for(var j=0; j<pickedChars.length; j++){
			if(inputValue.charAt(i)==pickedChars[j]){
				charMatched = true;
				break;
			}
		}
		if(charMatched==false){
			dest.val("");
			return false;
		}
	}
}

function getMessageAlert(message) {
	swal({
		html : message,
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : "OK" ,
		allowOutsideClick: false
	});
}

function getErrorAlert(error) {
	swal({
		title : "Error!",
		text : error,
		type : "error",
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : "OK",
		allowOutsideClick: false
	});
}

function getWarningAlert(warning) {
	swal({
		title : "Warning!",
		text : warning,
		type : "warning",
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : "OK" ,
		allowOutsideClick: false
	});
}


function getSuccessAlert(text) {
	swal({
		title : 'Done',
		text : text,
		type : 'success',
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : 'OK',
		allowOutsideClick: false
	})
}

function getSuccessForwardAlert(text) {
	swal({
		title : 'Done',
		text : text,
		type : 'success',
		customClass : 'swal',
		animation : false,
		confirmButtonColor : '#1ABB9C',
		confirmButtonText : 'OK',
		allowOutsideClick: false
	}).then(function () {
		window.location.href = ctxPath;
	})
}

function getConfirmAlert(confirmMes,confirmCallBackFunction){
	swal({
		  text: confirmMes,
		  type: 'warning',
		  animation : false,
		  customClass : 'swal',
		  showCancelButton: true,
		  confirmButtonColor: '#1ABB9C',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes',
		  cancelButtonText: 'No',
		  allowOutsideClick: false
		}).then(function () {
			confirmCallBackFunction(true);
		})
}

function openWindowWithPost(url, name, values) {
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", url);

	for (var i = 0; i < name.length; i++) {
		var input = document.createElement('input');
		input.type = 'hidden';
		input.name = name[i];
		input.value = values[i];
		form.appendChild(input);
	}

	document.body.appendChild(form);
	form.submit();
	document.body.removeChild(form);
}

function formValidation(formName) {
	var isValid = true;

	$("#" + formName + " input[type=text]").each(function(index,element) {
		if(element.required){
			if ($.trim($(this).val()) == '') {
				isValid = false;
				this.style.setProperty('background-color', '#ffd2e4', 'important');
			} else {
				$(this).css('background-color', 'white');
			}
		}
	});

	$("#" + formName + " input[type=password]").each(function(index,element) {
		if(element.required){
			if ($.trim($(this).val()) == '') {
				isValid = false;
				this.style.setProperty('background-color', '#ffd2e4', 'important');
			} else {
				$(this).css('background-color', 'white');
			}
		}
	});

	$("#" + formName + " input[type=date]").each(function(index,element) {
		if(element.required){
			if ($.trim($(this).val()) == '') {
				isValid = false;
				this.style.setProperty('background-color', '#ffd2e4', 'important');
			} else {
				$(this).css('background-color', 'white');
			}
		}
	});

	$("#" + formName + " input[type=radio]").each(function(index,element) {
		if(element.required){
			if ($.trim($(this).val()) == '') {
				isValid = false;
				this.style.setProperty('background-color', '#ffd2e4', 'important');
			} else {
				$(this).css('background-color', 'white');
			}
		}
	});

	// This is used to check all the select tag inside the one particular form
	$("#" + formName + " select").each(function(index,element) {
		if(element.required){
			if ($.trim($(this).val()) == '') {
				isValid = false;
				this.style.setProperty('background-color', '#ffd2e4', 'important');
			} else {
				$(this).css('background-color', 'white');
			}
		}
	});

	$("#" + formName + " textarea").each(function(index,element) {
		if(element.required){
			if ($.trim($(this).val()) == '') {
				isValid = false;
				this.style.setProperty('background-color', '#ffd2e4', 'important');
			} else {
				$(this).css('background-color', 'white');
			}
		}
	}); 
	
	if(formName == "commReportForm"){
		var formObj = document.commReportForm;
	    var userType = formObj.userTypeCommReport.value;
	    var viewType = formObj.viewTypeCommReport.value;
	    if(userType == singleUserType){
	      formObj.action = "/SingleMFCommissionReport"
	    } else {
	      formObj.action = "/MFCommissionReport"
	    }
	}
	
	if (!isValid){
		getWarningAlert("Please provide valid data in the highlighted field(s).");
	}
	return isValid;
}

function formDisbale(formName){
	
	$("#" + formName + " input[type=text]").each(function(index,element) {
	    $(this).prop('disabled', true);
	});
	
	$("#" + formName + " input[type=checkbox]").each(function(index,element) {
	    $(this).prop('disabled', true);
	});

	$("#" + formName + " input[type=date]").each(function(index,element) {
	    $(this).prop('disabled', true);
	});
	
	$("#" + formName + " input[type=submit]").each(function(index,element) {
	    $(this).prop('disabled', true);
	});
	 
	$("#" + formName + " input[type=button]").each(function(index,element) {
	    $(this).prop('disabled', true);
	});
	 
	$("#" + formName + " select").each(function(index,element) {
	    $(this).prop('disabled', true);
	}); 
	   
	$("#" + formName + " button").each(function(index,element) {
	    if(element.name!='backButton'){
		    $(this).prop('disabled', true);
	    }
	});

}
;
