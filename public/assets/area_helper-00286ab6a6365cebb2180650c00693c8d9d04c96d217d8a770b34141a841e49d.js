Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

/* global $ */

var AreaHelper = (function () {
  function AreaHelper() {
    _classCallCheck(this, AreaHelper);
  }

  _createClass(AreaHelper, [{
    key: 'initialize',

    // watch open/close
    value: function initialize($el) {
      this.$el = $el;

      // listener
      this.$el.on('click', this.observeClick.bind(this));
    }
  }, {
    key: 'observeClick',
    value: function observeClick() {
      var _this = this;

      this.$el.fadeOut('slow', function () {
        _this.$el.next().removeClass('hidden-xs-up');
      });
    }
  }]);

  return AreaHelper;
})();

exports['default'] = AreaHelper;
module.exports = exports['default'];
