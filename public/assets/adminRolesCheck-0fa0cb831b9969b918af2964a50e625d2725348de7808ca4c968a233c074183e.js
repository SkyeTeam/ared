$(document).ready(function(){

	// This is used if the admin is update and his role is superadmin
	if($("#puppy_commands_0").is(':checked')){
      	for(x=1; x < parseInt(totalRoles); x++){ //Here totalRoles are defined in the form.html.erb of the user
        	$('#puppy_commands_'+x).attr("disabled", "disabled");
      	}
    }

	$('#puppy_commands_0').click(function(){
	  	if($(this).is(':checked')){
          	for(x=1; x < parseInt(totalRoles); x++){ //Here totalRoles are defined in the form.html.erb of the user
            	$('#puppy_commands_'+x).attr("disabled", "disabled");
          	}
      	} else { 
          	for(x=1; x < parseInt(totalRoles); x++){
            	$('#puppy_commands_'+x).removeAttr("disabled");
          	}
      	}
    });

    //This is for Micro signup only
    $('#puppy_commands_1').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_2').attr("disabled", "disabled");
     	 } else{
        	$('#puppy_commands_2').removeAttr("disabled");
      	}
    }); 

    //This is for all signup
    $('#puppy_commands_2').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_1').attr("disabled", "disabled");
      	} else{
        	$('#puppy_commands_1').removeAttr("disabled");
      	}
    }); 

    //******************************************
    //This is for Micro suspend only
    $('#puppy_commands_3').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_4').attr("disabled", "disabled");
      	} else {
        	$('#puppy_commands_4').removeAttr("disabled");
      	}
    }); 

    //This is for all suspend
    $('#puppy_commands_4').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_3').attr("disabled", "disabled");
      	} else {
        	$('#puppy_commands_3').removeAttr("disabled");
      	}
    }); 


    //******************************************
    // This is for Micro Modify only
    $('#puppy_commands_5').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_6').attr("disabled", "disabled");
     	} else {
        	$('#puppy_commands_6').removeAttr("disabled");
      	}
    }); 

    // THis is for all modify
    $('#puppy_commands_6').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_5').attr("disabled", "disabled");
      	} else {
       	 	$('#puppy_commands_5').removeAttr("disabled");
      	}
    }); 

     //******************************************
    // This is for Micro Modify only
    $('#puppy_commands_7').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_8').attr("disabled", "disabled");
     	} else {
        	$('#puppy_commands_8').removeAttr("disabled");
      	}
    }); 

    // THis is for all modify
    $('#puppy_commands_8').click(function(){
      	if($(this).is(':checked')){
        	$('#puppy_commands_7').attr("disabled", "disabled");
      	} else {
       	 	$('#puppy_commands_7').removeAttr("disabled");
      	}
    }); 

});

function manageDivs(userRole){

  	if(userRole == "admin" ){
	    
	    document.getElementById('email-font').style.display = "block";
      
      if(document.getElementById('form-group-national-id') != null){
  	    document.getElementById('form-group-national-id').style.display = "none";
      }
	    if(document.getElementById('form-group-business') != null){
        document.getElementById('form-group-business').style.display = "none";
      } 
	    if(document.getElementById('form-group-tin') != null){
        document.getElementById('form-group-tin').style.display = "none";
      }
	    
      document.getElementById('form-group-role').style.display = "block";
  	
  	} else if(userRole == "agent") {
	
	    document.getElementById('form-group-role').style.display = "none";
	    document.getElementById('email-font').style.display = "none";
	    document.getElementById('form-group-national-id').style.display = "block";
	    document.getElementById('form-group-business').style.display = "block";
	    document.getElementById('form-group-tin').style.display = "block";
  	
  	} 
};
