/* global $ */
export default class AreaHelper {

  constructor() {
    // watch open/close
  }

  initialize ($el) {
    this.$el = $el;

    // listener
    this.$el.on('click', this.observeClick.bind(this));
  }

  observeClick () {
    this.$el.fadeOut('slow', () => {
      this.$el.next().removeClass('hidden-xs-up')
    })
  }
}
