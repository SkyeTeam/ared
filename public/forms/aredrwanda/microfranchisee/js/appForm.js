$(function() {
  $('.error').hide();
  $('.alert-info').hide();
  $('#file_error').hide();
  $('#invalid_email').hide();
  $('#invalid_phone').hide();
  $('label#checkme').hide();
  $('label#file_empty').hide();

  $("#savedata").click(function() {
     $('.error').hide();
	 
		<!--code validationf for house_name--->
	  var first_name = $("input#first_name").val();
		if (first_name == "") {
		$('input#first_name_error').fadeIn(1000);
      $("input#first_name_error").show();
      $("input#first_name").focus();
      return false;
    }
	
	
	<!--code for the appartment validation here-->
	
	   var last_name = $("input#last_name").val();
		if (last_name == "") {
		$('input#last_name_error').fadeIn(1000);
      $("input#last_name_error").show();
      $("input#last_name").focus();
      return false;
    }
	
		<!--code for the bathrooms here-->
	  var email = $("input#email").val();
		if (email == "") {
		$('input#email_error').fadeIn(1000);
      $("input#email_error").show();
      $("input#email").focus();
      return false;
    }
	
	//check for validility of email
	
	function validEmail(v) {
    var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    return (v.match(r) == null) ? false : true;
}

	if (!validEmail(email)) {
    $("label#invalid_email").show();  
    $("input#email").focus();  
    return false;  
	}
	
	  var phone = $("input#phone").val();
		if (phone == "") {
		$('input#phone_error').fadeIn(1000);
      $("input#phone_error").show();
      $("input#phone").focus();
      return false;
    }


  var id_number = $("input#id_number").val();
		if (id_number == "") {
		$('input#idnumber_error').fadeIn(1000);
      $("input#idnumber_error").show();
      $("input#id_number").focus();
      return false;
    }
	
	
	  var passport_number = $("input#passport_number").val();
		if (passport_number == "") {
		$('input#passportnumber_error').fadeIn(1000);
      $("input#passportnumber_error").show();
      $("input#passport_number").focus();
      return false;
		}
		
		var next_of_kin = $("input#next_of_kin").val();
		if (next_of_kin == "") {
	  $('input#next_of_kin_error').fadeIn(1000);
      $("input#next_of_kin_error").show();
      $("input#next_of_kin").focus();
      return false;
		}
		
		
	var kin_number = $("input#kin_number").val();
		if (kin_number == "") {
		$('input#kin_number_error').fadeIn(1000);
      $("input#kin_number_error").show();
      $("input#kin_number").focus();
      return false;
		}
		
				
	var address = $("input#address").val();
		if (address == "") {
		$('input#address_error').fadeIn(1000);
      $("input#address_error").show();
      $("input#address").focus();
      return false;
		}
		


	    var location = $("input#location").val();
		if (location == "") {
		$('input#location_error').fadeIn(1000);
      $("input#location_error").show();
      $("input#location").focus();
      return false;
		}
/*
	  var val = $("#document_attachment").val();
   		 if(val == ''){
		  	$('label#file_empty').show(1000);
			 return false;
    }
	*/

	var desclaimer = $("input#t_and_c").val();
   if($("#t_and_c").is(':checked')){
	  
	  }else{
		  
		  
		  	$('label#checkme').show(1000);
			 return false;
		  }


		<!--code for appartment location validation-->
      var gender = $("#gender :selected").val();
      var email = $("input#email").val();
	  var phone = $("input#phone").val();
	  var address = $("input#address").val();
	  var id_number = $("input#id_number").val();
	  var passport_number = $("input#passort_number").val();
	  var next_of_kin = $("input#next_of_kin").val();
      var kin_number = $("input#kin_number").val();
	  var police_letter = $('#police_letter').prop('files');
	  var file_passport = $('#file_passport').prop('files');
	  var location = $("input#location").val();


			$.post(
		"./appFormHelper.php",
    $("#upload").serialize(),
	 
      function(data){
	  if(data != 'Data saved successfully'){
	  $('.alert-info').html(data);
}
else{
$('.alert-info').show();
$('.alert-info').fadeIn(1000);
$('.alert-info').html(data);
$('.alert-info').fadeOut(10000); 
//window.location.reload(true);
window.setTimeout(function(){window.location.href = "http://www.a-r-e-d.com/forms/masterfranchisee/franchisee_registration_final_step.php"},800);

 }
 
     });
    return false;
	
	
	});
	
});


