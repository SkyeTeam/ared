<?php
session_start();
/*Microfranchise application Form*/
/*Code updated on 7/7/2017 8:57 pm
By Nayebare Micheal*/
include_once 'common.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
        <title><?php echo $lang['PAGE_TITLE']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
</head>

<body>

    
		<div class="container">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
 		<div class="row">
        <div id="logo"><img src="images/ARED-Logo-final.png" align="center" width="144" height="100"></div>
        <div class="top_row3">
			<h3 align="center"><?php echo $lang['HEADER_TITLE'];?></h3>
         </div>


            
           <hr class="colorgraph">
        	 <div id="language" class="col-xs-12">
         	<div class="col-xs-12 col-md-6"><a href="index.php?lang=en" class="btn btn-success btn-block btn-lg" style="color:#fff;">English Form</a></div>		
          	<div class="col-xs-12 col-md-6"><a href="index.php?lang=rw" class="btn btn-primary btn-block btn-lg" style="color:#fff;">Kinyarwanda Form</a></div>
          	</div>
          
      	 <div id="separator"></div>
       
           <hr class="colorgraph">
            <form id="upload" method="POST" action="upload.php" enctype="multipart/form-data">
            
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
                      	<input type="hidden" name="first_element" id="first_element_error">
                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="<?php echo $lang['FIRST_NAME']; ?>">
                        <input type="hidden" name="first_name_error" id="first_name_error" class="form-control">
					</div>
				</div>
                
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="last_name" id="last_name" class="form-control" placeholder="<?php echo $lang['LAST_NAME']; ?>">
                         <input type="hidden" name="first_name_error" id="first_name_error" class="form-control">
					</div>
				</div>
			
            <div class="form-group">
            <?php echo $lang['GENDER'];?>
    		<select id="gender" name="gender" class="form-control">
        	<option><?php echo $lang['MALE'];?></option>
        	<option><?php echo $lang['FEMALE']; ?></option>
            </select>
            </div>
        
        
          
        		<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
             				<input  type="email" class="form-control" id="email" name="email" placeholder="<?php print $lang['EMAIL'];?>"/>
                              <input type="hidden"  id="email_error" class="form-control">
                              <label id="invalid_email" style="color:#FF0000;">Invalid Email</label>
					</div>
				</div>
        
        	 <div class="col-xs-12 col-sm-6 col-md-6">
				<div class="form-group">
                        <input type="text" name="phone" id="phone" class="form-control" placeholder="<?php  print $lang['PHONE'];?>">
                        <input type="hidden" id="phone_error" class="form-control">
                        <label id="invalid_phone" style="color:#FF0000;">Enter Only Numbers</label>
					</div>
				</div>
                
				<div class="form-group">
					<div class="form-group">
						<input type="text" name="address" id="address" class="form-control" placeholder="<?php echo $lang['ADDRESS'];?>">
                        <input type="hidden" id="address_error" class="form-control">
					</div>
				</div>
		
      
                  
        		<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
                        <input type="text" name="id_number" id="id_number" class="form-control" placeholder="<?php print $lang['ID_NUMBER'];?>">
                         <input type="hidden" id="idnumber_error" class="form-control">
                         <label id="invalid_id" style="color:#FF0000;">Enter Only Numbers</label>
					</div>
				</div>

                  
        		<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
                        <input type="text" name="passport_number" id="passport_number" class="form-control" placeholder="<?php print $lang['PASSPORT_NUMBER'];?>">
                        <input type="hidden" id="passportnumber_error" class="form-control">
					</div>
				</div>
                
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="next_of_kin" id="next_of_kin" class="form-control" placeholder="<?php print $lang['NEXT_KIN_NAME']; ?>">
                        <input type="hidden" id="next_of_kin_error" class="form-control">
					</div>
				</div>


		     <div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="kin_number" id="kin_number" class="form-control" placeholder="<?php print $lang['KIN_NUMBER']; ?>">
                        <input type="hidden" id="kin_number_error" class="form-control">
                        
					</div>
				</div>
              
            
      
    			<div class="form-group">
				<input type="text" name="location" id="location" class="form-control" placeholder="<?php print $lang['LOCATION'];?>" >
                <input type="hidden" id="location_error" class="form-control">
				</div>
           

    
			<div id="drop" class="form-group">
				<p>Drag and Drop Here Passport/National ID and Police Letter</p>
                
                
				<a>Browse For Files</a>
				<input type="file" name="upl" multiple  id="document_attachment"/>
                <input type="hidden" id="file_error" class="form-control" value="Upload File Please">
              <ul>
				<!-- The file uploads will be shown here -->
			  </ul>
			</div>


				<div class="form-group">
					<span class="button-checkbox">
						<button type="button" class="btn" data-color="info" tabindex="7">I Agree</button>
                        <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
                        <label id="checkme">hey</label>
					</span>
				</div>

                <hr class="colorgraph">
				<div class="form-group">
			  <p><?php print $lang['DISCLAIMER'];?>
                </p>
				</div>
			
			
                   <div class="alert-info" style="height:34px;"></div>
                   
			<hr class="colorgraph">
          <div class="form-group"><button type="button" class="btn btn-success btn-block btn-lg" id="savedata">Send</button></div>
		</form>
	</div>
</div>
</div>
</div>

<!--get the js elements --->
    <script src="js/index.js"></script>
    <script src="js/selection.js"></script>
   	<script src="js/appForm.js"></script>
	<script src="js/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies -->
		<script src="js/jquery.ui.widget.js"></script>
		<script src="js/jquery.iframe-transport.js"></script>
		<script src="js/jquery.fileupload.js"></script>
		
		<!-- Our main JS file -->
		<script src="js/script.js"></script>
<script type="text/javascript">
$(function () {
$('#phone,#id_number,#kin_number').keydown(function (e) {
if (e.shiftKey || e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
e.preventDefault();
}
}
});
});
</script>
  
</body>
</html>