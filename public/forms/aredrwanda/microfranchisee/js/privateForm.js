$(function() {
  $('.error').hide();
  $('.alert-info').hide();
   $('#file_error').hide();
     $('#invalid_email').hide();

  $("#sendme").click(function() {
     $('.error').hide();
	 
	  var first_name = $("input#first_name").val();
		if (first_name == "") {
		$('label#first_name_error').fadeIn(1000);
      	$("label#first_name_error").show();
      	$("input#first_name").focus();
      	return false;
   	 }
	
	  var last_name = $("input#last_name").val();
		if (last_name == "") {
		$('label#last_name_error').fadeIn(1000);
      $("label#last_name_error").show();
      $("input#last_name").focus();
      return false;
    }


	  var address = $("input#address").val();
		if (address == "") {
		$('input#address_error').fadeIn(1000);
      	$("input#address_error").show();
     	 $("input#address").focus();
      return false;
    }
		
	  var city = $("input#city").val();
		if (city == "") {
		$('input#city_error').fadeIn(1000);
      	$("input#city_error").show();
     	 $("input#city").focus();
      return false;
    }
	
		  var country = $("input#country").val();
		if (country == "") {
		$('input#country_error').fadeIn(1000);
      	$("input#country_error").show();
     	 $("input#country").focus();
      return false;
    }
	
	var postal_code = $("input#postal_code").val();
		if (postal_code == "") {
		$('input#postal_code_error').fadeIn(1000);
      $("input#postal_code_error").show();
      $("input#postal_code").focus();
      return false;
    }
	
	
		var phone= $("input#phone").val();
		if (phone == "") {
		$('input#phone_error').fadeIn(1000);
      $("input#phone_error").show();
      $("input#phone").focus();
      return false;
    }
	
	var company_name = $("input#company_name").val();
		if (company_name == "") {
		$('input#company_name_error').fadeIn(1000);
      $("input#company_name_error").show();
      $("input#company_name").focus();
      return false;
    }
	
	
	var position = $("input#position").val();
		if (last_name == "") {
		$('input#position_error').fadeIn(1000);
      $("input#position_error").show();
      $("input#position").focus();
      return false;
    }
	
		var year_established = $("input#year_established").val();
		if (year_established == "") {
		$('input#year_established_error').fadeIn(1000);
      $("input#year_established_error").show();
      $("input#year_established").focus();
      return false;
    }
	
/*		var website = $("input#website_error").val();
		if (website == "") {
		$('input#website_error').fadeIn(1000);
      $("input#website_error").show();
      $("input#website").focus();
      return false;
    }
	*/
	var street_address = $("input#street_address").val();
		if (street_address == "") {
		$('input#street_address_error').fadeIn(1000);
      $("input#street_address_error").show();
      $("input#street_address").focus();
      return false;
    }
	
		var company_city = $("input#company_city").val();
		if (company_city == "") {
		$('input#company_city_error').fadeIn(1000);
      $("input#company_city_error").show();
      $("input#company_city").focus();
      return false;
    }
	
	var company_country = $("input#company_country").val();
		if (company_country == "") {
		$('input#company_country_error').fadeIn(1000);
      $("input#company_country_error").show();
      $("input#company_country").focus();
      return false;
    }
	
		var company_postal_code = $("input#company_postal_code").val();
		if (company_postal_code == "") {
		$('input#company_postal_code_error').fadeIn(1000);
      $("input#company_postal_code_error").show();
      $("input#company_postal_code").focus();
      return false;
    }
	
	
	var company_telephone = $("input#company_telephone").val();
		if (company_telephone == "") {
		$('input#company_telephone_error').fadeIn(1000);
      $("input#company_telephone_error").show();
      $("input#company_telephone").focus();
      return false;
    }
	
		var company_telephone = $("input#company_telephone").val();
		if (company_telephone == "") {
		$('input#company_telephone_error').fadeIn(1000);
      $("input#company_telephone_error").show();
      $("input#company_telephone").focus();
      return false;
    }
	
	
		var previous_business_details = $("input#previous_business_details").val();
		if (previous_business_details == "") {
		$('input#previous_business_details_error').fadeIn(1000);
      $("input#previous_business_details_error").show();
      $("input#previous_business_details").focus();
      return false;
    }
	
		var business_qualifications = $("input#business_qualifications").val();
		if (business_qualifications == "") {
		$('input#business_qualifications_error').fadeIn(1000);
      $("input#business_qualifications_error").show();
      $("input#business_qualifications").focus();
      return false;
    }
	
			var business_qualifications = $("input#business_qualifications").val();
		if (business_qualifications == "") {
		$('input#business_qualifications_error').fadeIn(1000);
      $("input#business_qualifications_error").show();
      $("input#business_qualifications").focus();
      return false;
    }
	
	var mobile = $("input#mobile").val();
		if (last_name == "") {
		$('label#mobile_error').fadeIn(1000);
      $("label#mobile_error").show();
      $("input#mobile").focus();
      return false;
    }
	
		var email = $("input#email").val();
		if (email == "") {
		$('label#email_error').fadeIn(1000);
      $("label#email_error").show();
      $("input#email").focus();
      return false;
    }
	
		//check for validility of email
	
	function validEmail(v) {
    var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    return (v.match(r) == null) ? false : true;
}

	if (!validEmail(email)) {
    $("label#invalid_email").show();  
    $("input#email").focus();  
    return false;  
	}
	
	
		var company_name = $("input#company_name").val();
		if (company_name == "") {
		$('label#company_name_error').fadeIn(1000);
      $("label#company_name_error").show();
      $("input#company_name").focus();
      return false;
    }
	
		
		var position = $("input#position").val();
		if (position == "") {
		$('label#position_error').fadeIn(1000);
      $("label#position_error").show();
      $("input#position").focus();
      return false;
    }
			
		var year_established = $("input#year_established").val();
		if (year_established == "") {
		$('label#year_established_error').fadeIn(1000);
      $("label#year_established_error").show();
      $("input#year_established").focus();
      return false;
    }
	
	var street_address = $("input#street_address").val();
		if (street_address == "") {
		$('label#street_address_error').fadeIn(1000);
      $("label#street_address_error").show();
      $("input#street_address").focus();
      return false;
    }
	
		var company_city = $("input#company_city").val();
		if (company_city == "") {
		$('label#company_city_error').fadeIn(1000);
      $("label#company_city_error").show();
      $("input#company_city").focus();
      return false;
    }
	
	var company_telephone= $("input#company_telephone").val();
		if (company_city == "") {
		$('label#company_telephone_error').fadeIn(1000);
      $("label#company_telephone_error").show();
      $("input#company_telephoner").focus();
      return false;
    }
	

			$.post(
		"./privateformHelper.php",
      $("#investor_form").serialize(),
      function(data){
	  if(data != 'Data saved successfully'){
	  $('.alert-info').html(data);

}
else{
$('.alert-info').show();
$('.alert-info').fadeIn(800);
$('.alert-info').html(data);
$('.alert-info').fadeOut(7000); 
window.setTimeout(function(){window.location.href = "http://www.a-r-e-d.com/"},900);
 }
     });
    return false;	
	});
	
});


