<?php
/* 
------------------
Language: Kinyrwanda
------------------
*/

$lang = array();

$lang['PAGE_TITLE'] = 'ARED MICROFRANCHISEE APPLICATION FORM';
$lang['HEADER_TITLE'] = 'ARED MICROFRANCHISEE APPLICATION FORM';
$lang['BUSINESS_TYPE'] = 'Select Type of Business';
$lang['INDIVIDUAL'] = 'Kuiti cye';
$lang['BUSINESS'] = 'Umucuruzi';
$lang['COMPANY'] = 'Kompanyi';
$lang['BUSINESS_NAME'] = 'Izina ry &acute;ubucuruzi';
$lang['FIRST_NAME'] = 'Amazina';
$lang['LAST_NAME'] = 'Amazina';
$lang['GENDER'] = 'Igitsina';
$lang['DATE_OF_BIRTH'] = 'Itariki wavukiyeho : MM/DD/YYYY';
$lang['MALE'] = 'Gabo';
$lang['FEMALE'] = 'Garo';
$lang['EMAIL'] = 'Imeyili';
$lang['PHONE'] = 'Nomero ya Telefone';
$lang['ADDRESS'] = 'Aho abarizwa:(Akarere/intara/Umurenge)';
$lang['ID_NUMBER'] = 'Nomero y&acute;irangamuntu';
$lang['PASSPORT_NUMBER'] = 'Pasiporo';
$lang['NEXT_KIN_NAME'] = 'Umwishingizi(Amazina)';
$lang['KIN_NUMBER'] = 'Telefoni ya Umishingizi';
$lang['TIN_NUMBER'] = 'Intumwa';
$lang['POLICE_LETTER'] = 'Upload Criminal Record Letter';
$lang['ID_PASSPORT'] = 'Upload ID/Pasiporo';
$lang['LOCATION'] = 'Andika aho wifuza gukoresha kiosk igendanwa (Akarere/ Umurenge/Akagari)';
$lang['DISCLAIMER'] = 
						'Nemeje ko : Amakuru ntanze kuri ururupapuro yuzuye neza nkurikije ubumenyi mfite kandi ntamakosa arimo nemeye kwirengera 
						ingaruka zayaturukaho.Nemeye gukurikiza amakuru ari muri iyi nyandiko. Amategeko azakurikirane ingaruka zizaturuka mubyo
					 	ntangiye ubuhamya muburenganzira bwanjye .Nshyize umukono kuri iyi nyandiko.Mubushishozi nasomye kandi nemeye amategeko n&acute;amabwiriza
				 		akubiye muri aya masezerano nk &acute;Intumwa.';
				 
				 
/*microfranchisee checklist sample*/

$lang['NUMBER_ONE'] = "Ese urajijutse kuburyo wowe ubwawe wakora igikorwa runaka?";
$lang['NUMBER_TWO'] = "Ese uziko aka kazi kadahembera umushahara,ahubwo gahemba comisiyo wakoreye kukwezi?";
$lang['NUMBER_THREE'] = "Ese uziko iyi bizinesi uko ukora cyane  aribyo bituma ubona komisiyo nyinshi?";
$lang['NUMBER_FOUR'] = "Ese witeguye kuba wafata icyemezo gikomeye kugiti cyawe?";
$lang['NUMBER_FIVE'] = "Ese ushobora kumenya igihe uri mubibazo bikaze kuburyo ukenera ko abandi bantu bagufasha?";
$lang['NUMBER_SIX'] = "Ese igihe ukeneye ubufasha? Ushobora gushaka uburyo wabubonamo?";
$lang['NUMBER_SEVEN'] = "Ese ushobora gukorana neza n誕bandi?";
$lang['NUMBER_EIGHT'] = "Ese ufite ubushobozi bwo kuba wayobora inshingano runaka, Gukundisha abantu ikintu no kubasobanurira neza??";
$lang['NUMBER_NINE'] = "Ese ushobora kwemera gusigira inshingano warufite n置burenganzira ukabiha undi muntu?";
$lang['NUMBER_TEN'] = "Ese ushobora kwemera ikosa igihe warikoze?";
$lang['NUMBER_ELEVEN'] = "Ese hari ishusho y置bunyamwuga ugaragariza abakugana?";
$lang['NUMBER_TWELVE'] = "Ese abantu bashobora kwizera ibyo uvuga?";
$lang['NUMBER_THIRTEEN'] = "Ese abantu bashobora kwizera ko uzakora ibyo ubabwiye?";
$lang['NUMBER_FOURTEEN'] = "Ese ufite uburambe mu kuyobora abantu no gucunga ibintu?";
$lang['NUMBER_FIFTEEN'] = "Ese ufite ubumenyi  uzakenera kugira ngo ukore ubu bucuruzi?";
$lang['NUMBER_SIXTEEN'] = "Ese uzi ingufu n段ntege nke byawe?";
$lang['NUMBER_SEVENTEEN'] = "Ese ufite abo mufatanyije mu bucuruzi cyangwa abajyanama bashobora kukunganira kubera intege nke zawe?";
$lang['NUMBER_EIGHTEEN'] = "Ese wigeze gukora umurimo umeze nk置wo ushaka gutangiza?";
$lang['NUMBER_NINETEEN'] = "Ese washatse amakuru yose ashoboka ku murimo ushaka gukora?";
$lang['NUMBER_TWENTY'] = "Ese uzi gutega amatwi neza?";
$lang['NUMBER_TWENTYONE'] = "Ese ufite ubushobozi no gukurikiza amabwiriza n段bizacyenerwa kugirango utangire iki gikorwa?";


?>