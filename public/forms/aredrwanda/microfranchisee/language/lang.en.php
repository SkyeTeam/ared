<?php
/* 
------------------
Language: English
------------------
*/

$lang = array();

$lang['PAGE_TITLE'] = 'ARED MICROFRANCHISEE APPLICATION FORM';
$lang['HEADER_TITLE'] = 'ARED MICROFRANCHISEE APPLICATION FORM';
$lang['BUSINESS_TYPE'] = 'Select Type of Business';
$lang['INDIVIDUAL'] = 'Individual';
$lang['BUSINESS'] = 'Business';
$lang['COMPANY'] = 'Company';
$lang['BUSINESS_NAME'] = 'Business Name';
$lang['FIRST_NAME'] = 'First Name';
$lang['LAST_NAME'] = 'Last Name';
$lang['GENDER'] = 'Select Gender';
$lang['DATE_OF_BIRTH'] = 'Date of Birth: MM/DD/YYYY';
$lang['MALE'] = 'Male';
$lang['FEMALE'] = 'Female';
$lang['EMAIL'] = 'Email';
$lang['PHONE'] = 'Phone';
$lang['ADDRESS'] = 'Address:(District/Province/Sector)';
$lang['ID_NUMBER'] = 'ID Number';
$lang['TIN_NUMBER'] = 'Tin Number';
$lang['PASSPORT_NUMBER'] = 'Passport Number';
$lang['NEXT_KIN_NAME'] = 'Next of Kin Names';
$lang['KIN_NUMBER'] = 'Next of Kin Telephone Number';
$lang['POLICE_LETTER'] = 'Upload Criminal Record Letter';
$lang['ID_PASSPORT'] = 'Upload ID/PassporT';
$lang['LOCATION'] = 'Location you intend to operate the ARED Kiosk(District/Sector And Cell)';
$lang['DISCLAIMER'] = '<b>I declare that</b>: The information provided on this form is correct to the best of my knowledge 
						and without misrepresentation deliberate or otherwise. 
                		I agree to update the information provided herein when it is no longer accurate. 
                		Any legal consequences arising from what I have started here in my responsibility.
               			By singing this form of acknowledge that I have read and agree to the term and conditions of the attached Agent&acute;s Agreement';
				
				
/*microfranchisee checklist sample*/
$lang['NUMBER_ONE'] = 	"Are you a self-starter?";
$lang['NUMBER_TWO'] = 	"Do you understand this is a commission based business and not a salary?";
$lang['NUMBER_THREE'] = "Do you understand that in this business the harder you work, the more you�ll earn?";
$lang['NUMBER_FOUR'] = 	"Are you ready to make tough decisions on your own?";
$lang['NUMBER_FIVE'] = 	"Do you know when you're in over your head and need outside help?";
$lang['NUMBER_SIX'] = 	"Are you willing to seek outside help? Do you know where to find it";
$lang['NUMBER_SEVEN'] = "Can you deal effectively with other people?";
$lang['NUMBER_EIGHT'] = "Are you an effective leader, motivator, and communicator?";
$lang['NUMBER_NINE'] = 	"Are you willing to delegate authority and responsibility to others?";
$lang['NUMBER_TEN'] = 	"Can you deal effectively with other people?";
$lang['NUMBER_ELEVEN'] = "Do you project a professional image to your clients and customers?";
$lang['NUMBER_TWELVE'] = "Can people trust what you say?";
$lang['NUMBER_THIRTEEN'] = "Can people trust you to do what you say you will do?";
$lang['NUMBER_FOURTEEN'] = "Do you have managerial experience?";
$lang['NUMBER_FIFTEEN'] = 	"Do you have the technical skills you will need to operate your particular business?";
$lang['NUMBER_SIXTEEN'] = 	"Do you have the business skills you need to run a business?";
$lang['NUMBER_SEVENTEEN'] = "Do you know your strengths and weaknesses?";
$lang['NUMBER_EIGHTEEN'] = 	"Do you have business partners or advisors who can compensate for your weaknesses?";
$lang['NUMBER_NINETEEN'] =	"Have you worked in a business like the one you want to start?";
$lang['NUMBER_TWENTY'] = 	"Have you researched your business thoroughly?";
$lang['NUMBER_TWENTYONE'] = "Are you a good listener?";


?>