<?php

/*Microfranchise application Form*/
/*Code updated on 7/7/2017 8:57 pm
By Nayebare Micheal*/
include_once 'common.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
        <title><?php echo $lang['PAGE_TITLE']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
</head>

	<body>
	<div class="container">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
 		<div class="row">
        <div id="logo"><img src="images/ARED-Logo-final.png" align="center" width="144" height="100"></div>
        <div class="top_row3">
			<h3 align="center"><?php echo $lang['HEADER_TITLE'];?></h3>
         </div>

               <form id="upload" method="POST"  enctype="multipart/form-data">
           <hr class="colorgraph">

         <div id="language" class="col-xs-12">
         	<div class="col-xs-12 col-md-6"><a href="checkboxes.php?lang=en" class="btn btn-success btn-block btn-lg" style="color:#fff;">English Form</a></div>		
          	<div class="col-xs-12 col-md-6"><a href="checkboxes.php?lang=rw" class="btn btn-primary btn-block btn-lg" style="color:#fff;">Kinyarwanda Form</a></div>
          </div>
          
       <div id="separator"></div>
       
           <hr class="colorgraph">


                   <div class="funkyradio-info">
          
                    
        <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">1.<?php  print $lang['NUMBER_ONE'];?></label>
    		<div class="form-group form-group">
    			<div class="form-group form-group">
    				<div id="radioBtn" class="btn-group">
    					    Yes <input type="radio" name="qns1" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns1" id="checkbox6" value="no" />
    			</div>
    		</div>
    	</div>
	<br />

        <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">2.<?php  print $lang['NUMBER_TWO'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns2" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns2" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />

        <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">3.<?php  print $lang['NUMBER_THREE'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns3" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns3" id="checkbox6" value="no"/>
    				</div>
    		
    			</div>
    		</div>
    	</div>
	<br />
    
          
            <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">4.<?php  print $lang['NUMBER_FOUR'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    					Yes <input type="radio" name="qns4" id="checkbox6" value="yes" />
            				No <input type="radio" name="qns4" id="checkbox6" value="no"/>
    				</div>
    			
    			</div>
    		</div>
    	</div>
	<br />
    
    
                <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">5.<?php  print $lang['NUMBER_FIVE'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    					Yes <input type="radio" name="qns5" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns5" id="checkbox6" value="no"/>
    				</div>
    			
    			</div>
    		</div>
    	</div>
	<br />
    
    
             <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">6.<?php  print $lang['NUMBER_SIX'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns6" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns6" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
    
                 <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">7.<?php  print $lang['NUMBER_SEVEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    					Yes <input type="radio" name="qns7" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns7" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
             <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">8.<?php  print $lang['NUMBER_EIGHT'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns8" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns8" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
           <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">9.<?php  print $lang['NUMBER_NINE'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    					Yes <input type="radio" name="qns9" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns9" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
               <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">10.<?php  print $lang['NUMBER_TEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns10" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns10" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
                   <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">11.<?php  print $lang['NUMBER_ELEVEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    					Yes <input type="radio" name="qns11" id="checkbox6" value="yes"/>
            			No <input type="radio" name="qns11" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
         <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">12.<?php  print $lang['NUMBER_TWELVE'];?></label>
    		<div class="form-group form-group">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns12" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns12" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
         <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">13.<?php  print $lang['NUMBER_THIRTEEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns13" id="checkbox6" value="yes" />
            				No <input type="radio" name="qns13" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
       <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">14.<?php  print $lang['NUMBER_FOURTEEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns14" id="checkbox6" value="yes" />
            				No <input type="radio" name="qns14" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
           <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">15.<?php  print $lang['NUMBER_FIFTEEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns15" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns15" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
       <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">16.<?php  print $lang['NUMBER_SIXTEEN'];?>?</label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns16" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns16" id="checkbox6" value="no"/>
    				</div>
    			
    			</div>
    		</div>
    	</div>
	<br />
    
    <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">17.<?php  print $lang['NUMBER_SEVENTEEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns17" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns17" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
     <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">18.<?php  print $lang['NUMBER_EIGHTEEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns18" id="checkbox6" value="yes" />
            				No <input type="radio" name="qns18" id="checkbox6" value="no"/>
    				</div>
    		
    			</div>
    		</div>
    	</div>
	<br />
    
    
        
         <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">19.<?php  print $lang['NUMBER_NINETEEN'];?></label>
    		<div class="col-sm-7 col-md-7">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns19" id="checkbox6" value="yes" />
            				No <input type="radio" name="qns19" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
             <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">20.<?php  print $lang['NUMBER_TWENTY'];?></label>
    		<div class="form-group form-group">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns20" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns20" id="checkbox6" value="no"/>
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
    
             <div class="form-group">
    		<label for="happy" class="form-group  control-label text-left">21.<?php  print $lang['NUMBER_TWENTYONE'];?></label>
    		<div class="form-group form-group">
    			<div class="input-group">
    				<div id="radioBtn" class="btn-group">
    						Yes <input type="radio" name="qns21" id="checkbox6" value="yes"/>
            				No <input type="radio" name="qns21" id="checkbox6"value="no" />
    				</div>
    			</div>
    		</div>
    	</div>
	<br />
    
     <label id="checkme" style="color:#FF0000;">Please answer all questions</label>
     
                       <div class="alert-info" style="height:34px;"></div>
			<hr class="colorgraph">
		          <div class="form-group"><button type="button" class="btn btn-success btn-block btn-lg" id="qnsbutton">Send</button></div>
		</form>
	</div>
</div>
</div>


<!--get the css elements --->
    <script src="js/index.js"></script>
    <script src="js/selection.js"></script>
     <script src="js/radio.js"></script>
</body>
</html>